﻿using System.Collections.Generic;
using System.Diagnostics;
using PreCom.Core.Communication;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Test.Mocks
{
    internal class MockDispatcher : DispatcherBase
    {
        public List<SentItem> SentItems { get; set; }

        public override void SendMessage(EntityBase entity, SynchronizationModule syncModule)
        {
            var sentItem = new SentItem() { Persistence = Persistence.ConnectionSession };

            // Must create a clone otherwise might get error with updating the same reference.
            // Probably we want to fix this in the sync module instead to avoid future confusion and errors.
            var entityType = entity.GetType();
            if (entityType == typeof(PushResponse))
            {
                sentItem.Entity = Clone((PushResponse)entity);
            }
            else if (entityType == typeof(PullResponse))
            {
                sentItem.Entity = Clone((PullResponse)entity);
            }
            else
            {
                sentItem.Entity = entity;
            }

            SentItems.Add(sentItem);
        }

        private PushResponse Clone(PushResponse pushResponse)
        {
            var clone = new PushResponse();
            foreach (var item in pushResponse.AcceptedObjects)
            {
                clone.AcceptedObjects.Add(item);
            }

            foreach (var item in pushResponse.ResolvedObjects)
            {
                clone.ResolvedObjects.Add(item);
            }

            clone.IsSplit = pushResponse.IsSplit;
            clone.ObjectType = pushResponse.ObjectType;
            clone.PacketNumber = pushResponse.PacketNumber;
            clone.SessionGuid = pushResponse.SessionGuid;
            clone.TotalNumberOfPackets = pushResponse.TotalNumberOfPackets;

            return clone;
        }

        private PullResponse Clone(PullResponse pullResponse)
        {
            var clone = new PullResponse();
            clone.CurrentEntityCount = pullResponse.CurrentEntityCount;

            foreach (var item in pullResponse.DeletedObjects)
            {
                clone.DeletedObjects.Add(item);
            }

            clone.IsSplit = pullResponse.IsSplit;
            clone.ObjectType = pullResponse.ObjectType;
            clone.PacketNumber = pullResponse.PacketNumber;
            clone.SessionGuid = pullResponse.SessionGuid;

            foreach (var item in pullResponse.SynchronizedObjects)
            {
                clone.SynchronizedObjects.Add(item);
            }

            clone.SyncType = pullResponse.SyncType;
            clone.TotalEntityCount = pullResponse.TotalEntityCount;
            clone.TotalNumberOfPackets = pullResponse.TotalNumberOfPackets;

            return clone;
        }

        [DebuggerDisplay("Entity = {Entity}, CredentialId = {CredentialId}, Persistence = {Persistence}")]
        internal class SentItem
        {
            public EntityBase Entity { get; set; }
            public Persistence Persistence { get; set; }
        }
    }
}
