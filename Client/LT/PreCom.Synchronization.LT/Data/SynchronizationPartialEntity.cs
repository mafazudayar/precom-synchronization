﻿using System;

namespace PreCom.Synchronization.Data
{
    internal class SynchronizationPartialEntity
    {
        public Guid SessionId { get; set; }
        public Guid EntityId { get; set; }
        public int PacketNumber { get; set; }
        public string PartialEntity { get; set; }
        public DateTime InsertionDate { get; set; }
    }
}
