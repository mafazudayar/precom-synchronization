﻿using System;

namespace PreCom.Synchronization.Data
{
    internal class SynchronizationPropertyMetadata
    {
        public string PropertyName { get; set; }
        public int IsChanged { get; set; }
    }
}
