﻿using System;

namespace PreCom.Synchronization.Data
{
    internal class SynchronizationMetadata
    {
        public string EntityType { get; set; }
        public Guid EntityId { get; set; }
        public int EntityVersion { get; set; }
        public int EntityStatus { get; set; }
        public Guid SessionId { get; set; }
        public Guid ChangeId { get; set; }
    }
}
