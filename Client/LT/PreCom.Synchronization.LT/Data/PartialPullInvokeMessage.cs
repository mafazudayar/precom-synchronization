﻿namespace PreCom.Synchronization.Data
{
    class PartialPullInvokeMessage
    {
        public int PacketNumber { get; set; }
        public string Knowledge { get; set; }
    }
}
