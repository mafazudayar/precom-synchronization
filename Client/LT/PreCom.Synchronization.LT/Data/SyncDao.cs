﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Data
{
    internal class SyncDao : ISyncDao
    {
        private readonly object _syncMetadataLock = new object();
        private readonly Dictionary<Guid, SynchronizationMetadata> _synchronizationMetadata =
            new Dictionary<Guid, SynchronizationMetadata>();

        private readonly Dictionary<Guid, List<SynchronizationPropertyMetadata>> _synchronizationPropertyMetadata =
            new Dictionary<Guid, List<SynchronizationPropertyMetadata>>();

        private readonly Dictionary<Guid, List<PartialPullInvokeMessage>> _partialPullInvokeMessage =
            new Dictionary<Guid, List<PartialPullInvokeMessage>>();

        private readonly List<SynchronizationPartialEntity> _partialEntities = new List<SynchronizationPartialEntity>();

        #region Constructor

        internal SyncDao()
        {
            CreateStorage();
        }

        #endregion

        #region Methods

        public void CreateStorage()
        {
        }

        public SyncMetadata GetMetadata(Guid entityId)
        {
            int version = -1;
            int status = -1;

            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    version = metadata.EntityVersion;
                    status = metadata.EntityStatus;
                }
            }

            return new SyncMetadata { Guid = entityId, Status = status, Version = version };
        }

        public List<Guid> GetMetadataIds(string entityType)
        {
            lock (_syncMetadataLock)
            {
                List<Guid> ids = (from metadata in _synchronizationMetadata
                    where metadata.Value.EntityType == entityType
                    select metadata.Value.EntityId).ToList();

                return ids;
            }
        }

        public void UpdateSessionIds(ICollection<Guid> entityIds, Guid sessionId)
        {
            if (entityIds == null || entityIds.Count <= 0)
            {
                return;
            }

            lock (_syncMetadataLock)
            {
                foreach (Guid guid in entityIds)
                {
                    SynchronizationMetadata metadata;
                    if (_synchronizationMetadata.TryGetValue(guid, out metadata))
                    {
                        metadata.SessionId = sessionId;
                    }
                }
            }
        }

        public Guid GetSessionId(Guid entityId)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    return metadata.SessionId;
                }
            }

            return Guid.Empty;
        }

        public bool IsExistInMetadata(Guid entityId)
        {
            lock (_syncMetadataLock)
            {
                return _synchronizationMetadata.ContainsKey(entityId);
            }
        }

        public void DeleteMetadata(Guid entityId, string entityType)
        {
            lock (_syncMetadataLock)
            {
                _synchronizationMetadata.Remove(entityId);
            }
        }

        public void InsertPartialEntity(Guid entityId, Guid sessionId, int packetNumber, SyncEntityData syncObject)
        {
            SynchronizationPartialEntity partialEntity = new SynchronizationPartialEntity();
            partialEntity.EntityId = entityId;
            partialEntity.SessionId = sessionId;
            partialEntity.PacketNumber = packetNumber;
            partialEntity.PartialEntity = syncObject.Object;
            partialEntity.InsertionDate = DateTime.Now;

            lock (_partialEntities)
            {
                _partialEntities.Add(partialEntity); 
            }
        }

        public int GetPartialEntityCount(Guid entityId, Guid sessionId)
        {
            int count = 0;

            lock (_partialEntities)
            {
                foreach (SynchronizationPartialEntity partialEntity in _partialEntities)
                {
                    if (entityId == partialEntity.EntityId && sessionId == partialEntity.SessionId)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public List<string> GetPartialEntities(Guid entityId, Guid sessionId)
        {
            List<string> partialEntities = new List<string>();
            List<SynchronizationPartialEntity> entityList = new List<SynchronizationPartialEntity>();

            lock (_partialEntities)
            {
                foreach (SynchronizationPartialEntity partialEntity in _partialEntities)
                {
                    if (entityId == partialEntity.EntityId && sessionId == partialEntity.SessionId)
                    {
                        entityList.Add(partialEntity);
                    }
                }
            }

            var newList = entityList.OrderBy(x => x.PacketNumber).ToList();

            foreach (SynchronizationPartialEntity entity in newList)
            {
                partialEntities.Add(entity.PartialEntity);
            }

            return partialEntities;
        }

        public void InsertMetadata(string entityType, Guid entityId, int version, SyncStatus syncStatus)
        {
            SynchronizationMetadata metadata = new SynchronizationMetadata();
            metadata.EntityType = entityType;
            metadata.EntityId = entityId;
            metadata.EntityVersion = version;
            metadata.EntityStatus = (int)syncStatus;
            metadata.SessionId = Guid.Empty;

            lock (_syncMetadataLock)
            {
                _synchronizationMetadata.Add(entityId, metadata);
            }
        }

        public List<string> GetChangedProperties(Guid entityId)
        {
            List<string> changedProperties = new List<string>();

            lock (_syncMetadataLock)
            {
                List<SynchronizationPropertyMetadata> propertyMetadataList;
                if (_synchronizationPropertyMetadata.TryGetValue(entityId, out propertyMetadataList))
                {
                    foreach (var propertyMetadata in propertyMetadataList)
                    {
                        if (propertyMetadata.IsChanged == (int)PropertySyncStatus.IsChanged)
                        {
                            changedProperties.Add(propertyMetadata.PropertyName);
                        }
                    }
                }
            }

            return changedProperties;
        }

        public void UpdateMetadata(string entityType, Guid entityId, int version, SyncStatus status)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    metadata.EntityVersion = version;
                    metadata.EntityStatus = (int)status;
                }
            }
        }

        public void UpdateMetadata(string entityType, Guid entityId, SyncStatus status, Guid changeId)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    metadata.EntityStatus = (int)status;
                }
            }
        }

        public void MarkPropertiesAsSynchronized(Guid entityId, List<string> properties)
        {
            UpdatePropertyMetadata(entityId, properties, PropertySyncStatus.Synchronized);
        }

        private void UpdatePropertyMetadata(Guid entityId, List<string> properties, PropertySyncStatus syncStatus)
        {
            if (properties == null || properties.Count <= 0)
            {
                return;
            }

            lock (_syncMetadataLock)
            {
                List<SynchronizationPropertyMetadata> propertyMetadataList;
                if (_synchronizationPropertyMetadata.TryGetValue(entityId, out propertyMetadataList))
                {
                    foreach (var property in properties)
                    {
                        var propertyMeta = propertyMetadataList.FirstOrDefault(p => p.PropertyName == property);
                        if (propertyMeta != null)
                        {
                            propertyMeta.IsChanged = (int)syncStatus;
                        }
                    }
                }
            }
        }

        public void MarkAllPropertiesAsSynchronized(Guid entityId)
        {
            MarkAllProperties(entityId, PropertySyncStatus.Synchronized);
        }

        private void MarkAllProperties(Guid entityId, PropertySyncStatus propertySyncStatus)
        {
            lock (_syncMetadataLock)
            {
                List<SynchronizationPropertyMetadata> propertyMetadataList;
                if (_synchronizationPropertyMetadata.TryGetValue(entityId, out propertyMetadataList))
                {
                    foreach (var propertyMetadata in propertyMetadataList)
                    {
                        propertyMetadata.IsChanged = (int)propertySyncStatus;
                    }
                }
            }
        }

        public List<SyncEntityData> GetModifiedEntities(string entityType)
        {
            List<SyncEntityData> metadataCollection = new List<SyncEntityData>();

            lock (_syncMetadataLock)
            {
                foreach (SynchronizationMetadata metadata in _synchronizationMetadata.Values)
                {
                    if (metadata.EntityStatus != (int)SyncStatus.Synchronized &&
                        metadata.EntityType == entityType)
                    {
                        SyncEntityData syncEntityData = new SyncEntityData();
                        syncEntityData.Version = metadata.EntityVersion;
                        syncEntityData.Status = metadata.EntityStatus;
                        syncEntityData.Guid = metadata.EntityId;

                        metadataCollection.Add(syncEntityData);
                    }
                }
            }

            return metadataCollection;
        }

        public int GetStatusForId(Guid entityId)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    return metadata.EntityStatus;
                }
            }

            return -1;
        }

        public void RemovePartialEntities(Guid entityId, Guid sessionId)
        {
            lock (_partialEntities)
            {
                _partialEntities.RemoveAll(s => s.EntityId == entityId && s.SessionId == sessionId);
            }
        }

        public void MarkPropertiesAsChanged(Guid entityId, List<string> changedProperties)
        {
            UpdatePropertyMetadata(entityId, changedProperties, PropertySyncStatus.IsChanged);
        }

        public void DeleteAllPropertyMetadata(Guid entityId)
        {
            lock (_syncMetadataLock)
            {
                _synchronizationPropertyMetadata.Remove(entityId);
            }
        }

        public Guid GetChangeId(Guid entityId)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    return metadata.ChangeId;
                }
            }

            return Guid.Empty;
        }

        public void InsertPropertyMetadata(Guid entityId, IEnumerable<PropertyInfo> properties, PropertySyncStatus propertySyncStatus)
        {
            lock (_syncMetadataLock)
            {
                List<SynchronizationPropertyMetadata> propertyMetadataList;
                if (!_synchronizationPropertyMetadata.TryGetValue(entityId, out propertyMetadataList))
                {
                    propertyMetadataList = new List<SynchronizationPropertyMetadata>();
                    _synchronizationPropertyMetadata.Add(entityId, propertyMetadataList);
                }

                foreach (PropertyInfo propertyInfo in properties)
                {
                    // Check to avoid adding duplicates of a property.
                    var property = propertyMetadataList.FirstOrDefault(p => p.PropertyName == propertyInfo.Name);
                    if (property == null)
                    {
                        propertyMetadataList.Add(new SynchronizationPropertyMetadata()
                        {
                            PropertyName = propertyInfo.Name,
                            IsChanged = (int)propertySyncStatus
                        });
                    }
                    else
                    {
                        property.IsChanged = (int)propertySyncStatus;
                    }
                }
            }
        }

        public void UpdateMetadata(string entityType, Guid entityId, int version)
        {
            lock (_syncMetadataLock)
            {
                SynchronizationMetadata metadata;
                if (_synchronizationMetadata.TryGetValue(entityId, out metadata))
                {
                    metadata.EntityVersion = version;
                }
            }
        }

        public bool InsertPartialInvokeMessage(Guid sessionId, int packetNumber, string knowledge)
        {
            lock (_partialPullInvokeMessage)
            {
                List<PartialPullInvokeMessage> partialPullInvokeList;
                if (!_partialPullInvokeMessage.TryGetValue(sessionId, out partialPullInvokeList))
                {
                    partialPullInvokeList = new List<PartialPullInvokeMessage>();
                    _partialPullInvokeMessage.Add(sessionId, partialPullInvokeList);
                }

                partialPullInvokeList.Add(new PartialPullInvokeMessage
                {
                    PacketNumber = packetNumber,
                    Knowledge = knowledge
                });
            }

            return true;
        }

        public void DeletePartialInvokeMessages(Guid sessionId)
        {
            lock (_syncMetadataLock)
            {
                var toDelete = _synchronizationMetadata.Values.Where(m => m.SessionId == sessionId);

                foreach (var item in toDelete)
                {
                    _synchronizationMetadata.Remove(item.EntityId);
                }
            }
        }

        public List<string> GetPartialInvokeMessages(Guid sessionId)
        {
            lock (_partialPullInvokeMessage)
            {
                List<PartialPullInvokeMessage> partialPullInvokeList;
                if (_partialPullInvokeMessage.TryGetValue(sessionId, out partialPullInvokeList))
                {
                    var query = from p in partialPullInvokeList
                                orderby p.PacketNumber ascending
                                select p.Knowledge;

                    return query.ToList();
                }
            }

            return new List<string>();
        }

        public int GetPartialInvokeMessageCount(Guid sessionId)
        {
            lock (_partialPullInvokeMessage)
            {
                List<PartialPullInvokeMessage> partialPullInvokeList;
                if (_partialPullInvokeMessage.TryGetValue(sessionId, out partialPullInvokeList))
                {
                    return partialPullInvokeList.Count;
                }
            }

            return 0;
        }

        #endregion

        #region ISyncDao Members

        public List<EntityMetadata> GetMetadata(ICollection<Guid> ids)
        {
            var metadataList = new List<EntityMetadata>();

            foreach (var id in ids)
            {
                lock (_syncMetadataLock)
                {
                    SynchronizationMetadata metadata;
                    if (_synchronizationMetadata.TryGetValue(id, out metadata))
                    {
                        metadataList.Add(new EntityMetadata()
                        {
                            ChangeId = metadata.ChangeId,
                            Status = metadata.EntityStatus,
                            Guid = metadata.EntityId,
                            Version = metadata.EntityVersion
                        });
                    }
                }
            }

            return metadataList;
        }

        public void GetChangedProperties(ICollection<Guid> ids, Dictionary<Guid, EntityMetadata> currentData)
        {
            foreach (var entityId in ids)
            {
                lock (_syncMetadataLock)
                {
                    List<SynchronizationPropertyMetadata> propertyMetadataList;
                    if (_synchronizationPropertyMetadata.TryGetValue(entityId, out propertyMetadataList))
                    {
                        foreach (var propertyMetadata in propertyMetadataList)
                        {
                            if (propertyMetadata.IsChanged == (int)PropertySyncStatus.IsChanged)
                            {
                                EntityMetadata metadata;
                                currentData.TryGetValue(entityId, out metadata);

                                if (metadata != null)
                                {
                                    metadata.Properties.Add(propertyMetadata.PropertyName);
                                }
                                else
                                {
                                    metadata = new EntityMetadata();
                                    metadata.Properties.Add(propertyMetadata.PropertyName);
                                    currentData[entityId] = metadata;
                                }
                            }
                        }
                    }
                }
            }
        }

        public List<SyncMetadata> GetDeletedEntities(string entityType)
        {
            var metadataList = new List<SyncMetadata>();
            lock (_syncMetadataLock)
            {
                foreach (var metadata in _synchronizationMetadata.Values)
                {
                    if (metadata.EntityType == entityType && metadata.EntityStatus == (int)SyncStatus.Deleted)
                    {
                        metadataList.Add(new SyncMetadata
                        {
                            Guid = metadata.EntityId,
                            ChangeId = metadata.ChangeId,
                            Status = metadata.EntityStatus,
                            Version = metadata.EntityVersion
                        });
                    }
                }
            }

            return metadataList;
        }

        #endregion
    }
}
