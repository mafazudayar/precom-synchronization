﻿using System;
using System.Xml.Linq;
using PreCom.Core.Log;
using PreCom.Core.Modules;

namespace PreCom.Synchronization.Utils
{
    class LogFile : ILog
    {
        private object _lock = new object();

        public bool Write(LogItem item)
        {
            string message = string.Format("{0}: {1} : {2} : {3} : {4}",
                item.Level,
                item.ID,
                item.Header,
                item.Body,
                item.Dump);

            //just in case: we protect code with try.
            try
            {
                lock (_lock)
                {
                    string filename = AppDomain.CurrentDomain.BaseDirectory +
                    AppDomain.CurrentDomain.RelativeSearchPath + GetFilenameYYYMMDD("_LOG", ".log");

                    System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                    XElement xmlEntry = new XElement("logEntry",
                        new XElement("Date", System.DateTime.Now.ToString()),
                        new XElement("Message", message));
                    sw.WriteLine(xmlEntry);
                    sw.Close();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public IAsyncResult BeginWrite(LogItem item, AsyncCallback callback)
        {
            throw new NotImplementedException();
        }

        private string GetFilenameYYYMMDD(string suffix, string extension)
        {
            return DateTime.Now.ToString("yyyy_MM_dd") + suffix + extension;
        }
    }
}
