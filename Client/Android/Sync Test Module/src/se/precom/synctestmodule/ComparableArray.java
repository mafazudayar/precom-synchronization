package se.precom.synctestmodule;

import java.util.Arrays;

public class ComparableArray {

	public ComparableArray() {
	}
	
	public ComparableArray(byte[] content) {
		super();
		this.content = content;
	}

	private byte[] content;

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(content);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableArray other = (ComparableArray) obj;
		if (!Arrays.equals(content, other.content))
			return false;
		return true;
	}
}
