package se.precom.synctestmodule;

import java.util.List;

import se.precom.core.communication.CommunicationBase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MarketOrderListActivity extends Activity {

    CommunicationBase _communication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_display_market_orders);

        ListView _listView = (ListView) findViewById(R.id.marketOrderListView);
        MarketOrderDao dao = new MarketOrderDao();

        List<MarketOrder> orderList = dao.getOrders();

        MarketOrderAdapter adaptor = new MarketOrderAdapter(this, R.layout.activity_sync_order_list_item,
                android.R.id.text1, orderList);

        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                MarketOrder order = (MarketOrder) arg0.getItemAtPosition(arg2);

                Intent intent = new Intent(MarketOrderListActivity.this, UpdateActivity.class);
                intent.putExtra("Type", "Market");
                intent.putExtra("ID", order.Id);
                intent.putExtra("Creator", order.Creator);
                intent.putExtra("Description", order.Description);
                intent.putExtra("Assign", order.IsAssigned.toString());
                // startActivity(intent);
                startActivityForResult(intent, 1);

            }

        });

        _listView.setAdapter(adaptor);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if (resultCode == RESULT_OK) {
        this.onCreate(null);
        // }
    }
}
