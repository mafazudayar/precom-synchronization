package se.precom.synctestmodule;

import se.precom.core.communication.CommunicationBase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SyncTestActivity extends Activity {

    private CommunicationBase _communication;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_test);

        Button syncTest = (Button) findViewById(R.id.synctest_btn_sync_test);
        syncTest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SyncTestActivity.this, SyncApiTestActivity.class);
                startActivity(intent);
            }
        });

        Button displayMarketOrders = (Button) findViewById(R.id.synctest_display_market_orders);
        displayMarketOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SyncTestActivity.this, MarketOrderListActivity.class);
                startActivity(intent);
            }
        });

        Button displayLimitOrders = (Button) findViewById(R.id.synctest_display_limit_orders);
        displayLimitOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SyncTestActivity.this, LimitOrderListActivity.class);
                startActivity(intent);
            }
        });

        Button manageRecords = (Button) findViewById(R.id.sync_main_btn_manage_records);
        manageRecords.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SyncTestActivity.this, ActivityManageRecords.class);
                startActivity(intent);
            }
        });

    }

    private String GetCreator(int i) {

        if (i % 3 == 1) {
            return "Kaushalya";
        } else if (i % 3 == 2) {
            return "Asanka";
        } else if (i % 3 == 0) {
            return "Prasad";
        } else {
            return "Amalka";
        }
    }

    private String GetDescription(int i) {

        if (i % 3 == 1) {
            return "Computer Parts";
        } else if (i % 3 == 2) {
            return "Food items";
        } else if (i % 3 == 0) {
            return "Vehicals Parts";
        } else {
            return "Books";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_sync_test, menu);
        return true;
    }
}
