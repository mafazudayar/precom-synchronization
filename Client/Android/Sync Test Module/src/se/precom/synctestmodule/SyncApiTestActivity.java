package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import se.precom.core.Application;
import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.SendPriority;
import se.precom.core.communication.TypeReference;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.Filter;
import se.precom.synchronization.FilterOperator;
import se.precom.synchronization.ISynchronizable;
import se.precom.synchronization.SynchronizationClientBase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class SyncApiTestActivity extends Activity {

    private CommunicationBase _communication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_request_tester);

        Button btnSync = (Button) findViewById(R.id.synctest_btn_pull_request);
        btnSync.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SyncApiTestActivity.this, ActivityPullRequest.class);
                startActivity(intent);
            }
        });

        Button pushRequest = (Button) findViewById(R.id.synctest_btn_push_request);
        pushRequest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SyncApiTestActivity.this, ActivityPushRequest.class);
                startActivity(intent);
            }
        });

        Button serverPushRequest = (Button) findViewById(R.id.synctest_btn_server_push_request);
        serverPushRequest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = 0;

                    List<Integer> users = new ArrayList<Integer>();
                    users.add(1);
                    users.add(2);
                    users.add(3);

                    EntityFilter filter = new EntityFilter();
                    List<Filter> filterlist = new ArrayList<Filter>();
                    Filter f = new Filter();
                    f.setOperator(FilterOperator.EQUAL_TO);
                    f.setPropertyName("Creator");
                    f.setValue("Kaushalya");

                    filter.getFilterList().add(f);

                    orders.Credentials = users;
                    orders.OrderFilter = filter;

                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "ServerPush";
                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }
            }
        });

        Button deleteLocally = (Button) findViewById(R.id.btn_delete_locally);
        deleteLocally.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {

                    List<UUID> entityInfo = new ArrayList<UUID>();

                    LimitOrderDao dao = new LimitOrderDao();
                    EntityFilter ef = new EntityFilter();
                    se.precom.synchronization.Filter f = new se.precom.synchronization.Filter();
                    f.setOperator(FilterOperator.EQUAL_TO);
                    f.setPropertyName("Creator");
                    f.setValue("Prasad");
                    ef.getFilterList().add(f);

                    List<ISynchronizable> orderList = dao.getOrders(ef);

                    for (ISynchronizable o : orderList) {
                        entityInfo.add(o.getGuid());
                    }

                    if (entityInfo.size() == 0) {
                        Toast.makeText(SyncApiTestActivity.this,
                                "Nothing to delete", Toast.LENGTH_SHORT).show();
                    } else {
                        SynchronizationClientBase syncModule = Application.getInstance().getModules()
                                .get(SynchronizationClientBase.class);
                        syncModule.deleteLocally(LimitOrder.class, entityInfo);
                        Toast.makeText(SyncApiTestActivity.this,
                                " records deleted locally", Toast.LENGTH_SHORT)
                                .show();
                    }

                } catch (Exception ex) {
                    Toast.makeText(SyncApiTestActivity.this,
                            "Fail to delete records", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

    }
}
