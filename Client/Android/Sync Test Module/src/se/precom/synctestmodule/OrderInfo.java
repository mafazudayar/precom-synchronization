package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.List;

import se.precom.core.communication.EntityBase;
import se.precom.synchronization.EntityFilter;

public class OrderInfo extends EntityBase {
    public int NumberOfOrders;
    public List<Integer> Credentials;
    public EntityFilter OrderFilter;
    public String OrderType;
    public String Action;

    public OrderInfo()
    {
        Credentials = new ArrayList<Integer>();
        OrderFilter = new EntityFilter();
    }
}
