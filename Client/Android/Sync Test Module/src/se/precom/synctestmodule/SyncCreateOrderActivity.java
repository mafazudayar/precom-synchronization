package se.precom.synctestmodule;

import java.util.List;

import se.precom.core.Application;
import se.precom.synchronization.CreateArgs;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.ISynchronizable;
import se.precom.synchronization.SynchronizedStorageException;
import se.precom.synchronization.UpdateArgs;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SyncCreateOrderActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_create);

        final EditText txtCreator = (EditText) findViewById(R.id.editTextCreator);
        final EditText txtDescription = (EditText) findViewById(R.id.editTextDescription);
        final CheckBox chkIsAssign = (CheckBox) findViewById(R.id.checkBoxAssign);

        Button btnInsertMarket = (Button) findViewById(R.id.sync_btn_insert_market_order);
        btnInsertMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String creator = txtCreator.getText().toString();
                String decription = txtDescription.getText().toString();

                if (creator.equals("") != true && decription.equals("") != true) {
                    MarketOrder order = MarketOrderManager.CreateOrder(creator, decription, chkIsAssign.isChecked());

                    SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                    try {
                        module.getMarketManager().create(order, new CreateArgs(getClass().getSimpleName()));
                        clear();
                        Toast.makeText(SyncCreateOrderActivity.this, "New market order created", Toast.LENGTH_SHORT)
                                .show();
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(SyncCreateOrderActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SyncCreateOrderActivity.this, "Please fill creator and description",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnInsertLimit = (Button) findViewById(R.id.sync_btn_insert_limit_order);
        btnInsertLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String creator = txtCreator.getText().toString();
                String decription = txtDescription.getText().toString();

                if (creator.equals("") != true && decription.equals("") != true) {
                    LimitOrder order = LimitOrderManager.CreateOrder(creator, decription, chkIsAssign.isChecked());

                    SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                    try {
                        module.getLimitManager().create(order, new CreateArgs(getClass().getSimpleName()));
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(SyncCreateOrderActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT)
                                .show();
                    }
                    clear();
                    Toast.makeText(SyncCreateOrderActivity.this, "New limit order created", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SyncCreateOrderActivity.this, "Please fill creator and description",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void clear() {
        final EditText txtCreator = (EditText) findViewById(R.id.editTextCreator);
        final EditText txtDescription = (EditText) findViewById(R.id.editTextDescription);
        final CheckBox chkIsAssign = (CheckBox) findViewById(R.id.checkBoxAssign);
        txtCreator.setText("");
        txtDescription.setText("");
        chkIsAssign.setChecked(false);
    }

    private void UpdateBulkRecords() {
        final EditText txtDescription = (EditText) findViewById(R.id.editTextDescription);
        SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
        String decription = "444";
        try {
            List<ISynchronizable> list = module.getLimitManager().read(new EntityFilter());
            decription = txtDescription.getText().toString();
            int count = 0;
            for (ISynchronizable iSynchronizable : list) {
                count = count + 1;
                LimitOrder order = (LimitOrder) iSynchronizable;

                order.setDescription(decription);
                module.getLimitManager().update(order, new UpdateArgs(getClass().getSimpleName()));
            }

            Toast.makeText(SyncCreateOrderActivity.this, count + " updated",
                    Toast.LENGTH_SHORT).show();

        } catch (SynchronizedStorageException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
