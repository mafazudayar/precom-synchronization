package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.List;

import se.precom.core.Application;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.Filter;
import se.precom.synchronization.FilterOperator;
import se.precom.synchronization.PullArgs;
import se.precom.synchronization.SynchronizationClientBase;
import se.precom.synchronization.SynchronizationModule;
import se.precom.synchronization.SynchronizedStorageException;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ActivityPullRequest extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        Button pullAllMarket = (Button) findViewById(R.id.synctest_pull_btn_pull_all_market);
        pullAllMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationModule syncModule = Application.getInstance().getModules()
                        .get(SynchronizationModule.class);
                PullArgs pullargs = new PullArgs(new EntityFilter());

                try {
                    syncModule.pull(MarketOrder.class, pullargs);
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPullRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button pullAllLimit = (Button) findViewById(R.id.synctest_pull_btn_pull_all_limit);
        pullAllLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationModule syncModule = Application.getInstance().getModules()
                        .get(SynchronizationModule.class);
                PullArgs pullargs = new PullArgs(new EntityFilter());

                try {
                    syncModule.pull(LimitOrder.class, pullargs);
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPullRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button pullKaushalya = (Button) findViewById(R.id.synctest_btn_pull_market_kaushalya);
        pullKaushalya.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                EntityFilter filter = new EntityFilter();
                List<Filter> filterlist = new ArrayList<Filter>();
                Filter f = new Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Kaushalya");

                filter.getFilterList().add(f);
                PullArgs pullargs = new PullArgs(filter);

                try {
                    syncModule.pull(MarketOrder.class, pullargs);
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPullRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button pullAsanka = (Button) findViewById(R.id.synctest_btn_pull_market_asanka);
        pullAsanka.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                EntityFilter filter = new EntityFilter();
                List<Filter> filterlist = new ArrayList<Filter>();

                Filter f = new Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Asanka");

                filter.getFilterList().add(f);
                PullArgs pullargs = new PullArgs(filter);

                try {
                    syncModule.pull(MarketOrder.class, pullargs);
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPullRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button pullPrasad = (Button) findViewById(R.id.synctest_btn_pull_limit_prasad);
        pullPrasad.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                EntityFilter filter = new EntityFilter();
                List<Filter> filterlist = new ArrayList<Filter>();

                Filter f = new Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Prasad");

                filter.getFilterList().add(f);
                PullArgs pullargs = new PullArgs(filter);

                try {
                    syncModule.pull(LimitOrder.class, pullargs);
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPullRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
