package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.List;

import se.precom.core.Application;
import se.precom.core.communication.MaxMessageSizeException;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LimitOrderAdapter extends ArrayAdapter<LimitOrder> {

    public LimitOrderAdapter(Context context, int resource, int textViewRecourceId, List<LimitOrder> orders) {

        super(context, resource, textViewRecourceId, orders.subList(0, Math.min(200, orders.size())));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) Application.getInstance().getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.activity_sync_order_list_item, parent, false);

        TextView orderID = (TextView) row.findViewById(R.id.sync_text_oid);
        TextView orderName = (TextView) row.findViewById(R.id.sync_text_creator);
        TextView orderVersion = (TextView) row.findViewById(R.id.sync_text_version);

        LimitOrder order = new LimitOrder();
        order = getItem(position);
        orderID.setText(order.Id);
        orderName.setText(order.getCreator());

        LimitOrderDao dao = new LimitOrderDao();
        String version = dao.readVersionForId(order.Id);
        int status = dao.readStatusForId(order.Id);

        orderVersion.setText("Version: " + version + " , Status: " + status);

        return row;
    }
}
