package se.precom.synctestmodule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import se.precom.synchronization.CreateArgs;
import se.precom.synchronization.DeleteArgs;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.ISynchronizable;
import se.precom.synchronization.SynchronizedStorageBase;
import se.precom.synchronization.SynchronizedStorageException;
import se.precom.synchronization.UpdateArgs;

public class MarketOrderManager extends SynchronizedStorageBase<MarketOrder> {

    List<OnEntityChangedListener> _list = new ArrayList<SynchronizedStorageBase.OnEntityChangedListener>();
    private boolean _isThrowing;

    public boolean is_isThrowing() {
        return _isThrowing;
    }

    public void set_isThrowing(boolean _isThrowing) {
        this._isThrowing = _isThrowing;
    }

    public static MarketOrder CreateOrder(String creator, String description, Boolean isAssigned) {

        MarketOrder order = new MarketOrder();
        order.Id = UUID.randomUUID().toString();
        order.Creator = creator;
        order.Description = description;
        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            order.Date = sdf.parse("2012-12-12 06:00:00");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        order.IsAssigned = isAssigned;
        return order;
    }

    @Override
    protected void onCreate(List<ISynchronizable> entities, CreateArgs args)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        for (ISynchronizable entity : entities) {
            MarketOrder order = (MarketOrder) entity;
            MarketOrderDao dao = new MarketOrderDao();
            dao.insertData(order);
        }
    }

    @Override
    protected void onUpdate(ISynchronizable entity, UpdateArgs args)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        MarketOrder order = (MarketOrder) entity;
        MarketOrderDao dao = new MarketOrderDao();
        dao.updateData(order);
    }

    @Override
    protected void onDelete(UUID entityId, DeleteArgs args)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        MarketOrderDao dao = new MarketOrderDao();
        dao.deleteOrderRecord(entityId);
    }

    @Override
    protected List<ISynchronizable> onRead(List<UUID> entityIds)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        if (entityIds != null && entityIds.size() > 0) {
            String ids = "";
            for (UUID syncEntityInfo : entityIds) {
                ids = ids + "'" + syncEntityInfo + "',";
            }
            ids = ids.substring(0, ids.length() - 1);
            MarketOrderDao dao = new MarketOrderDao();
            return dao.getOrders(ids);
        }
        return null;
    }

    @Override
    protected List<ISynchronizable> onRead(EntityFilter filter)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        MarketOrderDao dao = new MarketOrderDao();
        return dao.getOrders(filter);
    }

    @Override
    protected List<UUID> onReadIds(EntityFilter filter)
            throws SynchronizedStorageException {

        if (_isThrowing) {
            throw new SynchronizedStorageException();
        }

        MarketOrderDao dao = new MarketOrderDao();
        return dao.getOrderIds(filter);
    }
}
