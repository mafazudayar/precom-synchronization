package se.precom.synctestmodule;

import java.util.EnumSet;
import java.util.UUID;

import se.precom.core.Application;
import se.precom.core.ModuleBase;
import se.precom.core.PreComBase;
import se.precom.synchronization.ConflictNotificationInfo;
import se.precom.synchronization.ConflictedPropertyInfo;
import se.precom.synchronization.Operation;
import se.precom.synchronization.SyncInfo;
import se.precom.synchronization.SynchronizationClientBase;
import se.precom.synchronization.SynchronizationClientBase.OnConflictListener;
import se.precom.synchronization.SynchronizationClientBase.OnSyncEventListener;
import se.precom.ui.MainMenuItem;
import se.precom.ui.MainMenuItem.OnMainMenuItemClickListener;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SyncTestModule extends ModuleBase {
    private MainMenuItem _syncTestMenuItem;
    private SynchronizationClientBase _module;
    private MarketOrderManager _marketManager;
    private LimitOrderManager _limitManager;

    private boolean enableSyncEventsForMarketOrder = false;
    private boolean enableSyncEventsForLimitOrder = true;

    private boolean registerAutoPushForLimitOrder = true;

    private boolean registerConflictNotifierForMarkerOrder = false;
    private boolean registerConflictNotifierForLimitOrder = false;

    public MarketOrderManager getMarketManager() {
        return _marketManager;
    }

    public LimitOrderManager getLimitManager() {
        return _limitManager;
    }

    @Override
    public void initialize() throws Exception {
        _syncTestMenuItem = new MainMenuItem(R.string.hello_world, R.drawable.ic_launcher,
                new OnMainMenuItemClickListener() {

                    @Override
                    public void onMainMenuItemClick(Context context, MainMenuItem item) {
                        Intent intent = new Intent(context, SyncTestActivity.class);
                        context.startActivity(intent);
                    }
                });

        Application.getInstance().getUIManager().getMainMenu().add(_syncTestMenuItem);

        _marketManager = new MarketOrderManager();
        MarketOrderDao dao = new MarketOrderDao();
        dao.CreateTable();

        _limitManager = new LimitOrderManager();
        LimitOrderDao cdao = new LimitOrderDao();
        cdao.CreateTable();

        _module = Application.getInstance().getModules().get(SynchronizationClientBase.class);

        Log.d("Sync", "Sync module version " + ((PreComBase) _module).getVersion());

        _module.registerStorage(MarketOrder.class, _marketManager);
        _module.registerStorage(LimitOrder.class, _limitManager);

        if (enableSyncEventsForMarketOrder) {
            _module.addOnSyncEventListener(MarketOrder.class, new OnSyncEventListener() {

                @Override
                public void onSyncEvent(UUID entityId, SyncInfo syncInfo) {
                    Log.d("Sync", "MarketOrder " + entityId.toString() + ":" + syncInfo.getReason().toString() + ":"
                            + syncInfo.getSyncEventType().toString());

                }
            });
        }

        if (enableSyncEventsForLimitOrder) {
            _module.addOnSyncEventListener(LimitOrder.class, new OnSyncEventListener() {

                @Override
                public void onSyncEvent(UUID entityId, SyncInfo syncInfo) {
                    try {
                        Log.d("Sync", "LimitOrder " +
                                "Id:" + (entityId != null ? entityId.toString() : "empty") + ":" +
                                "Session:"
                                + (syncInfo.getSessionId() != null ? syncInfo.getSessionId().toString() : "empty")
                                + ":" +
                                "Reason:" + syncInfo.getReason().toString() + ":" +
                                "Type:" + syncInfo.getSyncEventType().toString() + ":" +
                                "Total Entity Count : " + syncInfo.getTotalEntityCount() + ":" +
                                "Synchronized Entity Count : " + syncInfo.getSynchronizedEntityCount() + ":" +
                                "Information:" + (syncInfo.getInformation() != null ? syncInfo.getInformation() : ""));

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
        }

        if (registerAutoPushForLimitOrder) {
            _module.registerAutomaticPush(LimitOrder.class,
                    EnumSet.of(Operation.DELETED, Operation.CREATED, Operation.UPDATED));
        }

        if (registerConflictNotifierForMarkerOrder) {
            _module.setOnConflictListener(MarketOrder.class, new OnConflictListener() {

                @Override
                public void onConflict(ConflictNotificationInfo conflictEventArgs) {
                    for (ConflictedPropertyInfo info : conflictEventArgs.getConflicts()) {

                        try {
                            Log.d("Sync", "Property " + info.getPropertyName() + " has been modified from "
                                    + (info.getOldValue() != null ? info.getOldValue().toString() : "") + " to "
                                    + (info.getNewValue() != null ? info.getNewValue().toString() : ""));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        if (registerConflictNotifierForLimitOrder) {
            _module.setOnConflictListener(LimitOrder.class, new OnConflictListener() {

                @Override
                public void onConflict(ConflictNotificationInfo conflictEventArgs) {
                    for (ConflictedPropertyInfo info : conflictEventArgs.getConflicts()) {

                        try {
                            Log.d("Sync", "Property " + info.getPropertyName() + " has been modified from "
                                    + (info.getOldValue() != null ? info.getOldValue().toString() : "") + " to "
                                    + (info.getNewValue() != null ? info.getNewValue().toString() : ""));
                        } catch (Exception e) {

                        }
                    }
                }
            });
        }

        // Test client pull #38
//        _limitManager
//            .addOnEntityChangedListener(new se.precom.synchronization.SynchronizedStorageBase.OnEntityChangedListener() {
        // @Override
//                public void onEntityChanged(se.precom.synchronization.ChangeEventArgs eventArgs) {
        // if (eventArgs.getEntityChange() == Operation.CREATED) {
        // LimitOrder o = (LimitOrder) eventArgs.getNewEntity();
        // o.setDescription(o.getDescription() + " 1");
        // try {
//                            _limitManager.update(o,
//                                    new se.precom.synchronization.UpdateArgs(getClass().getSimpleName()));
//                        } catch (se.precom.synchronization.SynchronizedStorageException e) {
        // e.printStackTrace();
        // }
        //
        // }
        // }
        // });

        setInitialized(true);
    }

    @Override
    public void dispose() {
        Application.getInstance().getUIManager().getMainMenu().remove(_syncTestMenuItem);
        setInitialized(false);
    }

}
