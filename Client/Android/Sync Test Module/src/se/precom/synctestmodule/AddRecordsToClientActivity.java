package se.precom.synctestmodule;

import java.util.Random;

import se.precom.core.Application;
import se.precom.core.communication.CommunicationBase;
import se.precom.synchronization.CreateArgs;
import se.precom.synchronization.SynchronizedStorageException;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddRecordsToClientActivity extends Activity {

    CommunicationBase _communication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_add_records_to_client);

        final EditText txtRecords = (EditText) findViewById(R.id.editTextRecords);

        Button btnAddMarket = (Button) findViewById(R.id.synctest_btn_add_market_orders_to_client);
        btnAddMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);

                for (int i = 0; i < a; i++) {
                    MarketOrder order = MarketOrderManager.CreateOrder(GetCreator(), GetDescription(i), false);
                    try {
                        module.getMarketManager().create(order, new CreateArgs(getClass().getSimpleName()));
                        Toast.makeText(AddRecordsToClientActivity.this, a + " Market Orders Added", Toast.LENGTH_SHORT)
                                .show();
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(AddRecordsToClientActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT)
                                .show();
                    }
                }

            }
        });

        Button btnInsertLimit = (Button) findViewById(R.id.synctest_btn_insert_limit_orders_to_client);
        btnInsertLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);

                for (int i = 0; i < a; i++) {
                    LimitOrder order = LimitOrderManager.CreateOrder(GetCreator(), GetDescription(i), false);
                    try {
                        module.getLimitManager().create(order, new CreateArgs(getClass().getSimpleName()));
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(AddRecordsToClientActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                Toast.makeText(AddRecordsToClientActivity.this, a + " Limit Orders Added", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnAddLargeMarket = (Button) findViewById(R.id.synctest_btn_add_large_market_order_to_client);
        btnAddLargeMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);

                for (int i = 0; i < a; i++) {
                    MarketOrder order = MarketOrderManager.CreateOrder(GetCreator(),
                            generateString(new Random(), "A", 1024 * 1000),
                            false);
                    try {
                        module.getMarketManager().create(order, new CreateArgs(getClass().getSimpleName()));
                        Toast.makeText(AddRecordsToClientActivity.this, "Large Market Order Added", Toast.LENGTH_SHORT)
                                .show();
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(AddRecordsToClientActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT)
                                .show();
                    }
                }

            }
        });

        Button btnAddLargeLimit = (Button) findViewById(R.id.synctest_btn_add_large_limit_order_to_client);
        btnAddLargeLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);

                for (int i = 0; i < a; i++) {
                    LimitOrder order = LimitOrderManager.CreateOrder(GetCreator(),
                            generateString(new Random(), "��", 1024 * 600),
                            false);
                    try {
                        module.getLimitManager().create(order, new CreateArgs(getClass().getSimpleName()));
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(AddRecordsToClientActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                Toast.makeText(AddRecordsToClientActivity.this, "Large Limit Order Added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String GetCreator() {

        Random rn = new Random();
        int i = rn.nextInt(10);

        if (i % 3 == 1) {
            return "Kaushalya";
        } else if (i % 3 == 2) {
            return "Asanka";
        } else if (i % 3 == 0) {
            return "Prasad";
        } else {
            return "Kaushalya";
        }
    }

    private String GetDescription(int i) {

        if (i % 3 == 1) {
            return "Computer Parts";
        } else if (i % 3 == 2) {
            return "Food items";
        } else if (i % 3 == 0) {
            return "Vehicals Parts";
        } else {
            return "Books";
        }
    }

    public String generateString(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }

        return new String(text);
    }
}
