package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import se.precom.synchronization.CreateArgs;
import se.precom.synchronization.DeleteArgs;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.ISynchronizable;
import se.precom.synchronization.SynchronizedStorageBase;
import se.precom.synchronization.SynchronizedStorageException;
import se.precom.synchronization.UpdateArgs;

public class LimitOrderManager extends SynchronizedStorageBase<LimitOrder>
{
    List<OnEntityChangedListener> _list = new ArrayList<SynchronizedStorageBase.OnEntityChangedListener>();

    public static LimitOrder CreateOrder(String creator, String description, Boolean isAssigned) {

        LimitOrder order = new LimitOrder();
        order.Id = UUID.randomUUID().toString();
        order.setCreator(creator);
        order.setDescription(description);
        Date d = new Date();
        order.setDate(d);
        order.setIsAssigned(isAssigned);
        order.Report = new Report();
        order.Report.Title = "Limit Order Report";
        order.Report.Content = "Limit Order Report Content";
        List<Editor> editors = new ArrayList<Editor>();
        Editor e1 = new Editor();
        e1.EditorName = "AAA";
        Editor e2 = new Editor();
        e2.EditorName = "BBB";
        Editor e3 = new Editor();
        e3.EditorName = "CCC";
        editors.add(e1);
        editors.add(e2);
        editors.add(e3);
        return order;
    }

    @Override
    protected void onCreate(List<ISynchronizable> entities, CreateArgs args)
            throws SynchronizedStorageException {

        for (ISynchronizable entity : entities) {
            LimitOrder order = (LimitOrder) entity;
            LimitOrderDao dao = new LimitOrderDao();
            dao.insertData(order);
        }
    }

    @Override
    protected void onUpdate(ISynchronizable entity, UpdateArgs args)
            throws SynchronizedStorageException {

        LimitOrder order = (LimitOrder) entity;
        LimitOrderDao dao = new LimitOrderDao();
        dao.updateData(order);
    }

    @Override
    protected void onDelete(UUID entityId, DeleteArgs args)
            throws SynchronizedStorageException {

        LimitOrderDao dao = new LimitOrderDao();
        dao.deleteOrderRecord(entityId);
    }

    @Override
    protected List<ISynchronizable> onRead(List<UUID> entityIds)
            throws SynchronizedStorageException {

        if (entityIds != null && entityIds.size() > 0) {
            String ids = "";
            for (UUID syncEntityInfo : entityIds) {
                ids = ids + "'" + syncEntityInfo + "',";
            }
            ids = ids.substring(0, ids.length() - 1);
            LimitOrderDao dao = new LimitOrderDao();

            List<ISynchronizable> orders = dao.getOrders(ids);
            
            // Uncomment to test PC-2279 
//            if (orders.size() > 0) {
//                orders.remove(0);
//            }
            
            return orders;
        }
        return null;
    }

    @Override
    protected List<ISynchronizable> onRead(EntityFilter filter)
            throws SynchronizedStorageException {

        LimitOrderDao dao = new LimitOrderDao();
        return dao.getOrders(filter);
    }

    @Override
    protected List<UUID> onReadIds(EntityFilter filter)
            throws SynchronizedStorageException {

        LimitOrderDao dao = new LimitOrderDao();
        return dao.getOrderIds(filter);
    }
}
