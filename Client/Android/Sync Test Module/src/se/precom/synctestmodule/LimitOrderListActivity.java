package se.precom.synctestmodule;

import java.util.List;

import se.precom.core.communication.CommunicationBase;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class LimitOrderListActivity extends Activity {
    CommunicationBase _communication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_display_limit_orders);
        
        ListView _listView = (ListView) findViewById(R.id.limitOrderListView);
        LimitOrderDao dao = new LimitOrderDao();

        List<LimitOrder> orderList = dao.getOrders();
        
        Toast.makeText(this, "" + orderList.size() + " orders", Toast.LENGTH_SHORT).show();

        LimitOrderAdapter adaptor = new LimitOrderAdapter(this, R.layout.activity_sync_order_list_item,
                android.R.id.text1, orderList);

        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                LimitOrder order = (LimitOrder) arg0.getItemAtPosition(arg2);

                Intent intent = new Intent(LimitOrderListActivity.this, UpdateActivity.class);
                intent.putExtra("Type", "Limit");
                intent.putExtra("ID", order.Id);
                intent.putExtra("Creator", order.getCreator());
                intent.putExtra("Description", order.getDescription());
                intent.putExtra("Assign", order.getIsAssigned().toString());
                UpdateActivity.setLimitOrder(order);
                // startActivity(intent);
                startActivityForResult(intent, 1);

            }

        });

        _listView.setAdapter(adaptor);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if (resultCode == RESULT_OK) {
        this.onCreate(null);
        // }
    }
}
