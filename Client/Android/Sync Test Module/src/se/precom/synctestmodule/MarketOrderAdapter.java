package se.precom.synctestmodule;

import java.util.List;

import se.precom.core.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MarketOrderAdapter extends ArrayAdapter<MarketOrder> {

    private List<MarketOrder> _orderList;

    public MarketOrderAdapter(Context context, int resource, int textViewRecourceId, List<MarketOrder> orders) {

        super(context, resource, textViewRecourceId, orders);

        _orderList = orders;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) Application.getInstance().getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.activity_sync_order_list_item, parent, false);

        TextView orderID = (TextView) row.findViewById(R.id.sync_text_oid);
        TextView orderName = (TextView) row.findViewById(R.id.sync_text_creator);
        TextView orderVersion = (TextView) row.findViewById(R.id.sync_text_version);

        MarketOrder order = new MarketOrder();
        order = _orderList.get(position);
        orderID.setText(order.Id);
        orderName.setText(order.Creator);

        MarketOrderDao dao = new MarketOrderDao();
        String version = dao.readVersionForId(order.Id);
        int status = dao.readStatusForId(order.Id);

        orderVersion.setText("Version: " + version + " , Status: " + status);

        return row;
    }

}
