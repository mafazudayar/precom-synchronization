package se.precom.synctestmodule;

import java.util.UUID;

public class MarketOrder extends Order implements se.precom.synchronization.ISynchronizable {
    public String Id;
    public Boolean IsAssigned;
    public java.util.Date Date;

    @Override
    public UUID getGuid() {
        UUID uid = UUID.fromString(Id);
        return uid;
    }

    @Override
    public void setGuid(UUID uid) {
        Id = uid.toString();
    }

    public String getCreator() {
        return super.Creator;
    }

    public void setCreator(String creator) {
        super.Creator = creator;
    }

    public String getDescription() {
        return super.Description;
    }

    public void setDescription(String description) {
        super.Description = description;
    }

    public Boolean getIsAssigned() {
        return IsAssigned;
    }

    public void setIsAssigned(Boolean isAssigned) {
        IsAssigned = isAssigned;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }
}
