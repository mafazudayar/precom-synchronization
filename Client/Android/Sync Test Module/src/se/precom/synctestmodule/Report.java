package se.precom.synctestmodule;

import java.util.List;

public class Report {

    public String Title;
    public String Content;
    public List<Editor> Editors;
    public int Id;

    public Report() {
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Content == null) ? 0 : Content.hashCode());
        result = prime * result + ((Title == null) ? 0 : Title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Report other = (Report) obj;
        if (Content == null) {
            if (other.Content != null)
                return false;
        } else if (!Content.equals(other.Content))
            return false;
        if (Title == null) {
            if (other.Title != null)
                return false;
        } else if (!Title.equals(other.Title))
            return false;
        return true;
    }

}
