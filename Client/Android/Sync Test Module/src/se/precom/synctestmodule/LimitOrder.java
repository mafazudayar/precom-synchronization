package se.precom.synctestmodule;

import java.util.UUID;

import se.precom.synchronization.Synchronizable;

public class LimitOrder implements se.precom.synchronization.ISynchronizable {

    @Synchronizable
    private String Creator;
    @Synchronizable
    private String Description;
    @Synchronizable
    public String Id;

    private Boolean IsAssigned;
    @Synchronizable
    private java.util.Date Date;
    @Synchronizable
    public ComparableArray Image;

    @Synchronizable
    public Report Report;

    @Override
    public UUID getGuid() {
        UUID uid = UUID.fromString(Id);
        return uid;
    }

    @Override
    public void setGuid(UUID uid) {
        Id = uid.toString();
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String creator) {
        Creator = creator;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Boolean getIsAssigned() {
        return IsAssigned;
    }

    public void setIsAssigned(Boolean isAssigned) {
        IsAssigned = isAssigned;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }
}
