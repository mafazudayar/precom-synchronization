package se.precom.synctestmodule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import se.precom.core.Application;
import se.precom.core.storage.ObjectType;
import se.precom.core.storage.StorageBase;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.Filter;
import se.precom.synchronization.FilterOperator;
import se.precom.synchronization.ISynchronizable;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class MarketOrderDao {

    private StorageBase _storage;
    public static final String DB_TABLE_NAME = "SyncTest_MarketOrder";
    public static final String DB_AUTO_ID = "AutoId";
    public static final String DB_ORDER_ID = "OrderId";
    public static final String DB_CREATOR = "Creator";
    public static final String DB_DESCRIPTION = "Description";
    public static final String DB_DATE = "Date";
    public static final String DB_IS_ASSIGNED = "IsAssigned";

    private String META_TABLE_NAME = "PMC_Synchronization_MetaData";
    private String META_PROPERTY_TABLE_NAME = "PMC_Synchronization_Property_MetaData";
    private String ID_COLUMN = "EntityId";
    private String SERVER_VERSION_COLUMN = "EntityVersion";
    private String STATUS_COLUMN = "Status";

    public MarketOrderDao() {
        _storage = Application.getInstance().getStorage();
    }

    public void CreateTable() {

        if (!_storage.objectExists(DB_TABLE_NAME, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE " + DB_TABLE_NAME + " (");
            sb.append(DB_AUTO_ID + " integer PRIMARY KEY AUTOINCREMENT,");
            sb.append(DB_ORDER_ID + " TEXT,");
            sb.append(DB_CREATOR + " TEXT,");
            sb.append(DB_DESCRIPTION + " TEXT,");
            sb.append(DB_DATE + " TEXT,");
            sb.append(DB_IS_ASSIGNED + " BOOLEAN");
            sb.append(")");

            _storage.execSQL(sb.toString());

        }
    }

    public void insertData(MarketOrder order) {
        try {
            _storage.beginTransaction();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = formatter.format(order.Date);

            ContentValues values = new ContentValues();
            values.putNull(DB_AUTO_ID);
            values.put(DB_ORDER_ID, order.Id);
            values.put(DB_CREATOR, order.Creator);
            values.put(DB_DESCRIPTION, order.Description);
            values.put(DB_DATE, date);
            values.put(DB_IS_ASSIGNED, order.IsAssigned);
            _storage.insert(DB_TABLE_NAME, values);

            _storage.setTransactionSuccessful();
        } finally {
            _storage.endTransaction();
        }
    }

    public void updateData(MarketOrder order) {
        try {
            _storage.beginTransaction();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = formatter.format(order.Date);

            ContentValues values = new ContentValues();
            values.put(DB_CREATOR, order.Creator);
            values.put(DB_DESCRIPTION, order.Description);
            values.put(DB_DATE, date);
            values.put(DB_IS_ASSIGNED, order.IsAssigned);

            _storage.update(DB_TABLE_NAME, values, DB_ORDER_ID + "='" + order.Id + "'", null);
            _storage.setTransactionSuccessful();
        } finally {
            _storage.endTransaction();
        }
    }

    public List<MarketOrder> getOrders() {

        List<MarketOrder> orders = new ArrayList<MarketOrder>();
        Cursor cursor = _storage.query(false, DB_TABLE_NAME, null, null, null, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    MarketOrder order = new MarketOrder();
                    order.Id = cursor.getString(cursor.getColumnIndex(DB_ORDER_ID));
                    order.Creator = cursor.getString(cursor.getColumnIndex(DB_CREATOR));
                    order.Description = cursor.getString(cursor.getColumnIndex(DB_DESCRIPTION));
                    order.Date = getDate(cursor.getString(cursor.getColumnIndex(DB_DATE)));
                    order.IsAssigned = cursor.getInt(cursor.getColumnIndex(DB_IS_ASSIGNED)) == 1;
                    orders.add(order);

                } while (cursor.moveToNext());
            }
        }

        return orders;
    }

    private Date getDate(String input) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date t = new Date();

        try {
            t = formatter.parse(input);
        } catch (ParseException e) {

        }

        return t;
    }

    public MarketOrder getOrder(String id) {

        Cursor cursor = _storage.query(false, DB_TABLE_NAME, null, DB_ORDER_ID + "='" + id + "'", null, null, null,
                null, null);
        MarketOrder order = new MarketOrder();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    order.Id = cursor.getString(cursor.getColumnIndex(DB_ORDER_ID));
                    order.Creator = cursor.getString(cursor.getColumnIndex(DB_CREATOR));
                    order.Description = cursor.getString(cursor.getColumnIndex(DB_DESCRIPTION));
                    order.Date = getDate(cursor.getString(cursor.getColumnIndex(DB_DATE)));
                    order.IsAssigned = cursor.getInt(cursor.getColumnIndex(DB_IS_ASSIGNED)) == 1;

                } while (cursor.moveToNext());
            }
        }

        return order;
    }

    public List<ISynchronizable> getOrders(String commaSeparetedIds) {
        Cursor cursor = _storage.query(false, DB_TABLE_NAME, null, DB_ORDER_ID + " IN(" + commaSeparetedIds + ")",
                null, null, null, null, null);
        List<ISynchronizable> orders = new ArrayList<ISynchronizable>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    MarketOrder order = new MarketOrder();
                    order.Id = cursor.getString(cursor.getColumnIndex(DB_ORDER_ID));
                    order.Creator = cursor.getString(cursor.getColumnIndex(DB_CREATOR));
                    order.Description = cursor.getString(cursor.getColumnIndex(DB_DESCRIPTION));
                    order.Date = getDate(cursor.getString(cursor.getColumnIndex(DB_DATE)));
                    order.IsAssigned = cursor.getInt(cursor.getColumnIndex(DB_IS_ASSIGNED)) == 1;
                    orders.add(order);
                    Log.i("SyncTest", "Date: " + order.Date.getTime());
                } while (cursor.moveToNext());
            }
        }

        return orders;
    }

    public List<ISynchronizable> getOrders(EntityFilter filter) {

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * from [" + DB_TABLE_NAME + "] ");

        if (filter != null && filter.getFilterList() != null && filter.getFilterList().size() > 0)
        {
            sb.append("WHERE ");
            int a = 0;
            for (Filter f : filter.getFilterList())
            {
                a = a + 1;
                sb.append("[" + GetTableColumnName(f.getPropertyName()) + "]" + GetOparator(f.getOperator())
                        + GetValue(f.getPropertyName(), f.getValue()));
                if (a < filter.getFilterList().size())
                {
                    sb.append(" AND ");
                }
            }
        }
        sb.append(";");
        Cursor cursor = _storage.rawQuery(sb.toString(), null);
        List<ISynchronizable> orders = new ArrayList<ISynchronizable>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    MarketOrder order = new MarketOrder();
                    order.Id = cursor.getString(cursor.getColumnIndex(DB_ORDER_ID));
                    order.Creator = cursor.getString(cursor.getColumnIndex(DB_CREATOR));
                    order.Description = cursor.getString(cursor.getColumnIndex(DB_DESCRIPTION));
                    order.Date = getDate(cursor.getString(cursor.getColumnIndex(DB_DATE)));
                    order.IsAssigned = cursor.getInt(cursor.getColumnIndex(DB_IS_ASSIGNED)) == 1;
                    orders.add(order);
                } while (cursor.moveToNext());
            }
        }

        return orders;
    }

    public List<UUID> getOrderIds(EntityFilter filter) {

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT OrderId from [" + DB_TABLE_NAME + "] ");

        if (filter != null && filter.getFilterList() != null && filter.getFilterList().size() > 0)
        {
            sb.append("WHERE ");
            int a = 0;
            for (Filter f : filter.getFilterList())
            {
                a = a + 1;
                sb.append("[" + GetTableColumnName(f.getPropertyName()) + "]" + GetOparator(f.getOperator())
                        + GetValue(f.getPropertyName(), f.getValue()));
                if (a < filter.getFilterList().size())
                {
                    sb.append(" AND ");
                }
            }
        }
        sb.append(";");
        Cursor cursor = _storage.rawQuery(sb.toString(), null);

        List<UUID> orders = new ArrayList<UUID>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String oid = cursor.getString(cursor.getColumnIndex(DB_ORDER_ID));
                    orders.add(UUID.fromString(oid));
                } while (cursor.moveToNext());
            }
        }

        return orders;
    }

    public void deleteOrderRecord(UUID orderId) {
        String whereClause = DB_ORDER_ID + "='" + orderId.toString() + "'";
        _storage.delete(DB_TABLE_NAME, whereClause, null);
    }

    public void deleteOrderReords() {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM " + DB_TABLE_NAME + "");
        _storage.execSQL(sb.toString());
    }

    public void deleteSyncReords() {
        StringBuilder sb1 = new StringBuilder();
        sb1.append("DELETE FROM " + META_TABLE_NAME);
        _storage.execSQL(sb1.toString());

        StringBuilder sb2 = new StringBuilder();
        sb2.append("DELETE FROM " + META_PROPERTY_TABLE_NAME);
        _storage.execSQL(sb2.toString());

    }

    public Date ConvertDate(String date) {
        Date convertedDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            convertedDate = (Date) formatter.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return convertedDate;
    }

    private String GetValue(String propertyName, Object value)
    {
        if (propertyName.equals("Id"))
        {
            return "'" + value + "'";
        }
        else if (propertyName.equals("Creator"))
        {
            return "'" + value + "'";
        }
        else if (propertyName.equals("Description"))
        {
            return "'" + value + "'";
        }
        else if (propertyName.equals("IsAssigned"))
        {
            return value.toString();
        }
        else if (propertyName.equals("Date"))
        {
            return value.toString();
        } else {
            return value.toString();
        }
    }

    private String GetTableColumnName(String propertyName)
    {
        if (propertyName.equals("Id"))
        {
            return "OrderId";
        }
        else if (propertyName.equals("Creator"))
        {
            return "Creator";
        }
        else if (propertyName.equals("Description"))
        {
            return "Description";
        }
        else if (propertyName.equals("IsAssigned"))
        {
            return "IsAssigned";
        }
        else if (propertyName.equals("Date"))
        {
            return "Date";
        } else {
            return "";
        }
    }

    private String GetOparator(FilterOperator oparator)
    {
        if (oparator == FilterOperator.EQUAL_TO)
        {
            return "=";
        }
        else if (oparator == FilterOperator.NOT_EQUAL_TO)
        {
            return "!=";
        }
        else if (oparator == FilterOperator.GREATER_THAN)
        {
            return ">";
        }
        else if (oparator == FilterOperator.LESS_THAN)
        {
            return "<";
        }
        else
        {
            return "=";
        }
    }

    public String readVersionForId(String objectId) {
        String version = null;
        if (_storage.objectExists(META_TABLE_NAME, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT ");
            sb.append(SERVER_VERSION_COLUMN);
            sb.append(" FROM " + META_TABLE_NAME);
            sb.append(" WHERE " + ID_COLUMN + "='" + objectId + "'");
            Cursor cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                version = cursor.getString(cursor.getColumnIndex(SERVER_VERSION_COLUMN));
            }
            cursor.close();
        }
        return version;
    }

    public int readStatusForId(String objectId) {
        int status = -1;
        if (_storage.objectExists(META_TABLE_NAME, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT ");
            sb.append(STATUS_COLUMN);
            sb.append(" FROM " + META_TABLE_NAME);
            sb.append(" WHERE " + ID_COLUMN + "='" + objectId + "'");
            Cursor cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                status = cursor.getInt(cursor.getColumnIndex(STATUS_COLUMN));
            }
            cursor.close();
        }
        return status;
    }

}
