package se.precom.synctestmodule;

import se.precom.core.Application;
import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.SendPriority;
import se.precom.core.communication.TypeReference;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddRecordsToServerActivity extends Activity {

    CommunicationBase _communication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_add_records_to_server);

        final EditText txtRecords = (EditText) findViewById(R.id.editTextRecords);

        Button btnAddMarket = (Button) findViewById(R.id.synctest_btn_add_market_orders_to_server);
        btnAddMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                try {

                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "MarketOrder";
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "Insert";
                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this, a + " Market Orders sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }
            }
        });

        Button btnAddLimit = (Button) findViewById(R.id.synctest_btn_add_limit_orders_to_server);
        btnAddLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                try {

                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "LimitOrder";
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "Insert";

                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this, a + " Limit Orders sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }
            }
        });

        Button btnUpdateMarket = (Button) findViewById(R.id.synctest_btn_update_last_market_order_server);
        btnUpdateMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                try {
                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "MarketOrder";
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "Update";

                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this,
                            "Update latest market order command sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }
            }
        });
        Button btnUpdateLimit = (Button) findViewById(R.id.synctest_btn_update_last_limit_order_server);
        btnUpdateLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }
                try {
                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "LimitOrder";
                    
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "Update";

                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this,
                            "Update latest limit order command sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }
            }
        });

        Button btnDeleteMarket = (Button) findViewById(R.id.synctest_btn_delete_last_market_order_server);
        btnDeleteMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                try {
                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "MarketOrder";
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "DeleteLast";
                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this,
                            "Delete latest market order command sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }

            }
        });
        Button btnDeleteLimit = (Button) findViewById(R.id.synctest_btn_delete_last_limit_order_server);
        btnDeleteLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int a = 1;
                try {
                    a = Integer.parseInt(txtRecords.getText().toString());
                } catch (Exception e) {

                }

                try {
                    _communication = Application.getInstance().getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = a;
                    orders.OrderType = "LimitOrder";
                    RequestArgs ra = new RequestArgs(new TypeReference<OrderInfo>() {
                    });

                    orders.Action = "DeleteLast";
                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);

                    Toast.makeText(AddRecordsToServerActivity.this,
                            "Delete latest limit order command sent to server",
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
