package se.precom.synctestmodule;

import java.util.Date;
import java.util.UUID;

import se.precom.core.Application;
import se.precom.synchronization.DeleteArgs;
import se.precom.synchronization.SynchronizedStorageException;
import se.precom.synchronization.UpdateArgs;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateActivity extends Activity {

    private static LimitOrder limitOrder;

    public static LimitOrder getLimitOrder() {
        return limitOrder;
    }

    public static void setLimitOrder(LimitOrder order) {
        UpdateActivity.limitOrder = order;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_update);

        final TextView typeId = (TextView) findViewById(R.id.synctest_textview_order_type);
        final EditText txtId = (EditText) findViewById(R.id.editTextOrderId);
        final EditText txtCreator = (EditText) findViewById(R.id.editTextCreator);
        final EditText txtDescription = (EditText) findViewById(R.id.editTextDescription);
        final CheckBox chkIsAssign = (CheckBox) findViewById(R.id.checkBoxAssign);

        if (getIntent().hasExtra("Type")) {
            String type = getIntent().getExtras().getString("Type");
            typeId.setText(type + " OrderId");
        }

        if (getIntent().hasExtra("ID")) {
            String id = getIntent().getExtras().getString("ID");
            txtId.setText(id);
        }

        if (getIntent().hasExtra("Creator")) {
            txtCreator.setText(getIntent().getExtras().getString("Creator"));
        }

        if (getIntent().hasExtra("Description")) {
            txtDescription.setText(getIntent().getExtras().getString("Description"));
        }

        if (getIntent().hasExtra("Assign")) {
            String assign = getIntent().getExtras().getString("Assign");

            if (assign.equals("true")) {
                chkIsAssign.setChecked(true);
            } else {
                chkIsAssign.setChecked(false);
            }
        }

        Button btnSave = (Button) findViewById(R.id.buttonUpdate);
        btnSave.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String creator = txtCreator.getText().toString();
                String decription = txtDescription.getText().toString();
                String type = typeId.getText().toString();

                if (creator.equals("") != true) {

                    if (type.equals("Market OrderId")) {
                        MarketOrder order = new MarketOrder();
                        order.Id = txtId.getText().toString();
                        order.Creator = creator;
                        if (decription.equals("") == true) {
                            order.Description = null;
                        } else {
                            order.Description = decription;
                        }
                        Date date = new Date();
                        order.Date = date;
                        order.IsAssigned = chkIsAssign.isChecked();
                        SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                        try {
                            module.getMarketManager().update(order, new UpdateArgs(getClass().getSimpleName()));
                            Toast.makeText(UpdateActivity.this, "Market Order Updated", Toast.LENGTH_SHORT).show();
                        } catch (SynchronizedStorageException e) {
                            Toast.makeText(UpdateActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (type.equals("Limit OrderId")) {
                        LimitOrder order = getLimitOrder();

                        // LimitOrder order = new LimitOrder();
                        // order.Id = txtId.getText().toString();
                        order.setCreator(creator);
                        order.setDescription(decription);
                        // Date date = new Date();
                        // order.setDate(date);
                        order.setIsAssigned(chkIsAssign.isChecked());
                        // order.Report.Content = "Updated";
                        // order.Report.Title = "Updated Title";
                        SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                        try {
                            module.getLimitManager().update(order, new UpdateArgs(getClass().getSimpleName()));
                        } catch (SynchronizedStorageException e) {
                            Toast.makeText(UpdateActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                        }

                        Toast.makeText(UpdateActivity.this, "Limit Order Updated", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(UpdateActivity.this, "Please fill creator and description",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        final Button btnDelete = (Button) findViewById(R.id.buttonDelete);
        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String type = typeId.getText().toString();
                if (type.equals("Market OrderId")) {
                    String Id = txtId.getText().toString();

                    SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                    try {
                        module.getMarketManager().delete(UUID.fromString(Id),
                                new DeleteArgs(getClass().getSimpleName()));
                        Toast.makeText(UpdateActivity.this, "Market Order deleted", Toast.LENGTH_SHORT).show();
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(UpdateActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                    }

                }

                if (type.equals("Limit OrderId")) {
                    String Id = txtId.getText().toString();

                    SyncTestModule module = Application.getInstance().getModules().get(SyncTestModule.class);
                    try {
                        module.getLimitManager()
                                .delete(UUID.fromString(Id), new DeleteArgs(getClass().getSimpleName()));
                        Toast.makeText(UpdateActivity.this, "Limit Order deleted", Toast.LENGTH_SHORT).show();
                    } catch (SynchronizedStorageException e) {
                        Toast.makeText(UpdateActivity.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                    }

                }

                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
    }
}
