package se.precom.synctestmodule;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import se.precom.core.Application;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.synchronization.EntityFilter;
import se.precom.synchronization.FilterOperator;
import se.precom.synchronization.ISynchronizable;
import se.precom.synchronization.PushArgs;
import se.precom.synchronization.PushResult;
import se.precom.synchronization.SynchronizationClientBase;
import se.precom.synchronization.SynchronizedStorageException;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ActivityPushRequest extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_push_request);

        Button pushKaushalya = (Button) findViewById(R.id.synctest_btn_push_market_kaushalya);
        pushKaushalya.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                List<UUID> entitiInfo = new ArrayList<UUID>();

                MarketOrderDao dao = new MarketOrderDao();
                EntityFilter ef = new EntityFilter();
                se.precom.synchronization.Filter f = new se.precom.synchronization.Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Kaushalya");
                ef.getFilterList().add(f);

                List<ISynchronizable> orderList = dao.getOrders(ef);
                PushArgs args = new PushArgs();
                for (ISynchronizable o : orderList) {
                    args.getEntityIdList().add(o.getGuid());
                }

                try {
                    PushResult pushResult = syncModule.push(MarketOrder.class, args);
                    Toast.makeText(ActivityPushRequest.this, "Market " + pushResultToString(pushResult),
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPushRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button pushasanka = (Button) findViewById(R.id.synctest_btn_push_market_asanka);
        pushasanka.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                List<UUID> entitiInfo = new ArrayList<UUID>();

                MarketOrderDao dao = new MarketOrderDao();
                EntityFilter ef = new EntityFilter();
                se.precom.synchronization.Filter f = new se.precom.synchronization.Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Asanka");
                ef.getFilterList().add(f);

                List<ISynchronizable> orderList = dao.getOrders(ef);
                PushArgs args = new PushArgs();
                for (ISynchronizable o : orderList) {
                    args.getEntityIdList().add(o.getGuid());
                }

                try {
                    PushResult pushResult = syncModule.push(MarketOrder.class, args);
                    Toast.makeText(ActivityPushRequest.this, "Market " + pushResultToString(pushResult),
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPushRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button pushprasad = (Button) findViewById(R.id.synctest_btn_push_limit_prasad);
        pushprasad.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                List<UUID> entitiInfo = new ArrayList<UUID>();

                LimitOrderDao dao = new LimitOrderDao();
                EntityFilter ef = new EntityFilter();
                se.precom.synchronization.Filter f = new se.precom.synchronization.Filter();
                f.setOperator(FilterOperator.EQUAL_TO);
                f.setPropertyName("Creator");
                f.setValue("Prasad");
                ef.getFilterList().add(f);

                List<ISynchronizable> orderList = dao.getOrders(ef);
                PushArgs args = new PushArgs();
                for (ISynchronizable o : orderList) {
                    args.getEntityIdList().add(o.getGuid());
                }

                try {
                    PushResult pushResult = syncModule.push(LimitOrder.class, args);
                    Toast.makeText(ActivityPushRequest.this, "Limit " + pushResultToString(pushResult),
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPushRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button pushAllMarket = (Button) findViewById(R.id.synctest_push_btn_push_all_market);
        pushAllMarket.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);
                try {
                    PushResult pushResult = syncModule.push(MarketOrder.class);
                    Toast.makeText(ActivityPushRequest.this, "Market " + pushResultToString(pushResult),
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    Toast.makeText(ActivityPushRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button pushAllLimit = (Button) findViewById(R.id.synctest_push_btn_push_all_limit);
        pushAllLimit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                SynchronizationClientBase syncModule = Application.getInstance().getModules()
                        .get(SynchronizationClientBase.class);

                try {
                    PushResult pushResult = syncModule.push(LimitOrder.class);
                    Toast.makeText(ActivityPushRequest.this, "Limit" + pushResultToString(pushResult),
                            Toast.LENGTH_SHORT).show();
                } catch (NotLoggedInException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SynchronizedStorageException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(ActivityPushRequest.this, e.getClass().getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private static String pushResultToString(PushResult pushResult) {
        String str = "PushResult: " + pushResult.getPushedEntityIds().size() + " entities";

        if (pushResult.getSyncSessionId() != null) {
            str += ", session id " + pushResult.getSyncSessionId();
        }

        return str;
    }
}
