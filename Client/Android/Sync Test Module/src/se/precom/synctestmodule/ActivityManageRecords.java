package se.precom.synctestmodule;

import se.precom.core.Application;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

public class ActivityManageRecords extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_manage_records);

        Button btncreate = (Button) findViewById(R.id.synctest_btn_create_order);
        btncreate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityManageRecords.this, SyncCreateOrderActivity.class);
                startActivity(intent);
            }
        });

        Button btnInsertBulk = (Button) findViewById(R.id.synctest_add_records_to_client);
        btnInsertBulk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityManageRecords.this, AddRecordsToClientActivity.class);
                startActivity(intent);
            }
        });

        Button btnInsertBulkToServer = (Button) findViewById(R.id.sync_add_records_to_server);
        btnInsertBulkToServer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityManageRecords.this, AddRecordsToServerActivity.class);
                startActivity(intent);
            }
        });

        Button resetTables = (Button) findViewById(R.id.sync_main_btn_reset_tables);
        resetTables.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityManageRecords.this, DeleteRecordsActivity.class);
                startActivity(intent);
            }
        });

        Button bulkUpdate = (Button) findViewById(R.id.synctest_update_all_limit_orders);
        bulkUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    LimitOrderDao limitdao = new LimitOrderDao();
                    limitdao.markAllLimitOrdersAsUpdated();

                    Toast.makeText(ActivityManageRecords.this,
                            "All records updated", Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(ActivityManageRecords.this,
                            "Fail to update records", Toast.LENGTH_SHORT)
                            .show();

                }
            }
        });

        Button bulkDelete = (Button) findViewById(R.id.synctest_delete_all_limit_orders);
        bulkDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    LimitOrderDao limitdao = new LimitOrderDao();
                    limitdao.deleteOrderReords();
                    limitdao.markAllLimitOrdersAsDeleted();

                    Toast.makeText(ActivityManageRecords.this,
                            "All records deleted", Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(ActivityManageRecords.this,
                            "Fail to delete records", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });

        CheckBox marketStorageThrows = (CheckBox) findViewById(R.id.sync_check_market_throws);
        marketStorageThrows.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked) {
                Application.getInstance().getModules()
                        .get(SyncTestModule.class).getMarketManager()
                        .set_isThrowing(isChecked);
            }
        });

        marketStorageThrows.setChecked(Application.getInstance().getModules()
                .get(SyncTestModule.class).getMarketManager().is_isThrowing());
    }
}
