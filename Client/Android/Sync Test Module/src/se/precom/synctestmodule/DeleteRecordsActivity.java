package se.precom.synctestmodule;

import se.precom.core.Application;
import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.SendPriority;
import se.precom.core.communication.TypeReference;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class DeleteRecordsActivity extends Activity {

    CommunicationBase _communication;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_delete_records);

        Button deleteClientRecords = (Button) findViewById(R.id.btn_delete_client_records);
        deleteClientRecords.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {

                    // This method simply delete clear all the client records.
                    MarketOrderDao dao = new MarketOrderDao();
                    dao.deleteOrderReords();
                    dao.deleteSyncReords();

                    LimitOrderDao limitdao = new LimitOrderDao();
                    limitdao.deleteOrderReords();

                    Toast.makeText(DeleteRecordsActivity.this,
                            "All records deleted", Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Toast.makeText(DeleteRecordsActivity.this,
                            "Fail to delete records", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });

        Button deleteServerRecords = (Button) findViewById(R.id.btn_delete_server_records);
        deleteServerRecords.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {

                    _communication = Application.getInstance()
                            .getCommunication();
                    OrderInfo orders = new OrderInfo();
                    orders.NumberOfOrders = 0;
                    RequestArgs ra = new RequestArgs(
                            new TypeReference<OrderInfo>() {
                            });

                    orders.Action = "Delete";
                    _communication.send(orders, ra, SendPriority.WHEN_POSSIBLE);
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (MaxMessageSizeException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
