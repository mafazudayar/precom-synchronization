package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Contains information about the synchronization process started by calling
 * {@link SynchronizationClientBase#push(Class, PushArgs)} or
 * {@link SynchronizationClientBase#push(Class, java.util.List, PushArgs)}
 */
public class PushResult {

    private UUID _syncSessionId;
    private List<UUID> _pushedEntityIds = new ArrayList<UUID>();

    /**
     * Returns the synchronization session Id. This may be null if no synchronization process has started, for example
     * if there were no entities to synchronize.
     * 
     * @return the synchronization session Id.
     */
    public UUID getSyncSessionId() {
        return _syncSessionId;
    }

    /**
     * Sets the synchronization session Id.
     * 
     * @param syncSessionId
     *            the synchronization session Id.
     */
    void setSyncSessionId(UUID syncSessionId) {
        _syncSessionId = syncSessionId;
    }

    /**
     * Returns a list of identifiers of the entities for which the synchronization process has started.
     * 
     * @return a list of identifiers.
     */
    public List<UUID> getPushedEntityIds() {
        return _pushedEntityIds;
    }
}
