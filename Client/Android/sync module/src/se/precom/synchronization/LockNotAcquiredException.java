package se.precom.synchronization;

/**
 * Thrown if the caller were supposed to acquire the lock but the lock has not been acquired.
 * 
 */
public class LockNotAcquiredException extends Exception {

    private static final long serialVersionUID = 7905867930201246339L;

    /**
     * Constructs a new LockNotAcquiredException with the current stack trace.
     */
    public LockNotAcquiredException() {
    }

    /**
     * Constructs a new LockNotAcquiredException with the current stack trace and the specified detail message.
     * 
     * @param detailMessage
     *            the detail message for this exception.
     */
    public LockNotAcquiredException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new LockNotAcquiredException with the current stack trace and the specified cause.
     * 
     * @param throwable
     *            the cause of this exception.
     */
    public LockNotAcquiredException(Throwable throwable) {
        super(throwable);
    }

    /**
     * Constructs a new LockNotAcquiredException with the current stack trace and the specified cause and detail
     * message.
     * 
     * @param detailMessage
     *            the detail message for this exception.
     * @param throwable
     *            the cause of this exception.
     */
    public LockNotAcquiredException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
