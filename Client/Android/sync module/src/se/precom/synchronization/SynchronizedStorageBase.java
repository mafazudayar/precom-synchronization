package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

/**
 * Base class for custom storage implementations. This class performs CRUD operations. It contains a number of abstract
 * methods that the subclass needs to implement where the actual CRUD operations of the storage will be performed.
 * 
 * @param <T>
 */
public abstract class SynchronizedStorageBase<T extends ISynchronizable> {

    private List<OnEntityChangedListener> _entityChangedListeners = new ArrayList<OnEntityChangedListener>();
    private List<OnEntityChangedListener> _internalEntityChangedListeners = new ArrayList<OnEntityChangedListener>();
    private Lock _lock;

    Lock getLock() {
        return _lock;
    }

    void setLock(Lock _lock) {
        this._lock = _lock;
    }

    /**
     * Adds a {@link OnEntityChangedListener} which needs to be notified on entity changes.
     */
    public void addOnEntityChangedListener(
            OnEntityChangedListener onEntityChangedListener) {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            _entityChangedListeners.add(onEntityChangedListener);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Removes a {@link OnEntityChangedListener} which will no longer be notified.
     */
    public void removeOnEntityChangedListener(
            OnEntityChangedListener onEntityChangedListener) {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            _entityChangedListeners.remove(onEntityChangedListener);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    void addInternalOnEntityChangedListener(
            OnEntityChangedListener onEntityChangedListener) {
        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            _internalEntityChangedListeners.add(onEntityChangedListener);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    void removeInternalOnEntityChangedListener(
            OnEntityChangedListener onEntityChangedListener) {
        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            _internalEntityChangedListeners.remove(onEntityChangedListener);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Creates new entities.
     * 
     * @param entities
     *            list of entities which should be created.
     * @param createArgs
     *            containing information of creation.
     * 
     * @throws SynchronizedStorageException
     */
    public void create(List<ISynchronizable> entities, CreateArgs createArgs)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            onCreate(entities, createArgs);
            for (ISynchronizable entity : entities) {
                notifyOnEntityChangedListeners(Operation.CREATED, createArgs.getOriginatorType(), null,
                        entity);
            }
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a new entity
     * 
     * @param entity
     *            entity which should be created.
     * @param createArgs
     *            containing information of creation.
     * 
     * @throws SynchronizedStorageException
     */
    public void create(ISynchronizable entity, CreateArgs createArgs)
            throws SynchronizedStorageException {

        List<ISynchronizable> entities = new ArrayList<ISynchronizable>();
        entities.add(entity);
        create(entities, createArgs);
    }

    /**
     * Returns a list of {@link ISynchronizable}.
     * 
     * @param entityIds
     *            list if UUIDsS of the entities which need to be read.
     * @return list of {@link ISynchronizable}.
     * 
     * @throws SynchronizedStorageException
     */
    public List<ISynchronizable> read(List<UUID> entityIds)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            return onRead(entityIds);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Returns a list of {@link ISynchronizable} which match the filter. If the EntityFilter is empty, should return all
     * entities.
     * 
     * @param filter
     *            filter used to identify the entities which need to be read.
     * @return list of {@link ISynchronizable}.
     * 
     * @throws SynchronizedStorageException
     */
    public List<ISynchronizable> read(EntityFilter filter)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            return onRead(filter);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Returns a list of entity IDs which match the filter. If the EntityFilter is empty, should return all entity ids.
     * 
     * @param filter
     *            filter used to identify the entities which need to be read.
     * @return a list of entity IDs.
     * 
     * @throws SynchronizedStorageException
     */
    public List<UUID> readIds(EntityFilter filter)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            return onReadIds(filter);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Updates an existing entity.
     * 
     * @param entity
     *            entity which needs to be updated.
     * @param updateArgs
     *            containing information about the modification.
     * @throws SynchronizedStorageException
     */
    public void update(ISynchronizable entity, UpdateArgs updateArgs)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            ISynchronizable oldEntity = getOldEntity(entity.getGuid());
            onUpdate(entity, updateArgs);
            notifyOnEntityChangedListeners(Operation.UPDATED, updateArgs.getOriginatorType(),
                    oldEntity, entity);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Deletes an existing entity.
     * 
     * @param entityId
     *            Id of the entity which needs to be deleted.
     * @param deleteArgs
     *            containing information about the deletion.
     * @throws SynchronizedStorageException
     */
    public void delete(UUID entityId, DeleteArgs deleteArgs)
            throws SynchronizedStorageException {

        Lock lock = getLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            ISynchronizable oldEntity = getOldEntity(entityId);
            onDelete(entityId, deleteArgs);
            notifyOnEntityChangedListeners(Operation.DELETED, deleteArgs.getOriginatorType(),
                    oldEntity, null);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    /**
     * Called by the base class when objects shall be created. Implement the create functionality in this method.
     * 
     * @param entities
     *            list of entities to be created
     * @param createArgs
     *            containing information of creation.
     * @throws SynchronizedStorageException
     */
    protected abstract void onCreate(List<ISynchronizable> entities, CreateArgs createArgs)
            throws SynchronizedStorageException;

    /**
     * Called by the base class when an object shall be updated. Implement the update functionality in this method.
     * 
     * @param entity
     *            the entity to be updated
     * @param updateArgs
     *            containing information about the modification.
     * @throws SynchronizedStorageException
     */
    protected abstract void onUpdate(ISynchronizable entity, UpdateArgs updateArgs)
            throws SynchronizedStorageException;

    /**
     * Called by the base class when an object shall be deleted. Implement the delete functionality in this method.
     * 
     * @param entityId
     *            the id of the entity to be deleted
     * @param deleteArgs
     *            containing information about the deletion.
     * @throws SynchronizedStorageException
     */
    protected abstract void onDelete(UUID entityId, DeleteArgs deleteArgs)
            throws SynchronizedStorageException;

    /**
     * Called by the base class when entities should be read given a list of entity Ids. Implement the read
     * functionality in this method.
     * 
     * @param entityIds
     *            the list of entity id:s
     * @return list of ISynchronizable entities
     * @throws SynchronizedStorageException
     */
    protected abstract List<ISynchronizable> onRead(List<UUID> entityIds)
            throws SynchronizedStorageException;

    /**
     * Called by the base class when entities should be read given a filter. Implement the read functionality in this
     * method. If the EntityFilter is empty, should return all entities.
     * 
     * @param filter
     *            the filter to use
     * @return list of ISynchronizable entities
     * @throws SynchronizedStorageException
     */
    protected abstract List<ISynchronizable> onRead(EntityFilter filter)
            throws SynchronizedStorageException;

    /**
     * Called by the base class when entity Id:s should be read given a filter. Implement the read functionality in this
     * method. If the EntityFilter is empty, should return all entity ids.
     * 
     * @param filter
     *            the filter to use
     * @return List of Id:s selected by the filter
     * @throws SynchronizedStorageException
     */
    protected abstract List<UUID> onReadIds(EntityFilter filter)
            throws SynchronizedStorageException;

    private void notifyOnEntityChangedListeners(Operation operation,
            String originatorType, ISynchronizable oldEntity,
            ISynchronizable newEntity) {

        ISynchronizable entity;
        if (newEntity != null) {
            entity = newEntity;
        } else {
            entity = oldEntity;
        }

        final ChangeEventArgs args = new ChangeEventArgs();
        args.setEntityChange(operation);
        args.setId(entity.getGuid());
        args.setEntityType(entity.getClass().getSimpleName());
        args.setOriginatorType(originatorType);
        args.setOldEntity(oldEntity);
        args.setNewEntity(newEntity);

        if (_internalEntityChangedListeners != null) {
            for (OnEntityChangedListener onEntityChangedListener : _internalEntityChangedListeners) {
                onEntityChangedListener.onEntityChanged(args);
            }
        }

        Thread notifyThread = new Thread() {
            @Override
            public void run() {
                if (_entityChangedListeners != null) {
                    for (OnEntityChangedListener onEntityChangedListener : _entityChangedListeners) {
                        onEntityChangedListener.onEntityChanged(args);
                    }
                }
            }
        };
        notifyThread.start();
    }

    private ISynchronizable getOldEntity(UUID id)
            throws SynchronizedStorageException {
        List<UUID> entityList = new ArrayList<UUID>();
        entityList.add(id);
        List<ISynchronizable> result = read(entityList);

        if (result == null || result.size() == 0) {
            throw new SynchronizedStorageException(String.format(
                    "No object with id %s was found", id));
        }
        if (result.size() != 1) {
            throw new SynchronizedStorageException(String.format(
                    "%s objects with id %i was found, expecting one object",
                    Integer.toString(result.size()), id));
        }
        return result.get(0);
    }

    /**
     * Interface definition for a callback to be invoked when an entity is changed.
     * 
     */
    public interface OnEntityChangedListener {
        /**
         * Callback that is invoked when an entity is changed.
         * 
         * @param eventArgs
         *            {@link ChangeEventArgs}.
         * @throws LockNotAcquiredException
         */
        void onEntityChanged(ChangeEventArgs eventArgs);
    }
}
