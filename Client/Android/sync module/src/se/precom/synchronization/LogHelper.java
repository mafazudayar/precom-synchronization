package se.precom.synchronization;

import se.precom.core.log.LogBase;
import se.precom.core.log.LogItem;
import se.precom.core.log.LogLevel;

class LogHelper {
    
    private LogBase _log;
    
    LogHelper(LogBase log) {
        _log = log;
    }

    void logError(String source, LogId id, String message, Exception exception) {
        _log.write(new LogItem(source, LogLevel.ERROR, id.toString(), message,
                        exception == null || exception.getMessage() == null ? "" : exception.getMessage(),
                        exception == null ? "" : android.util.Log.getStackTraceString(exception)));
    }

    void logError(String source, LogId id, String message, String body) {
        _log.write(new LogItem(source, LogLevel.ERROR, id.toString(), message, body));
    }

    void logDebug(String source, LogId id, String message, String body) {
        _log.write(new LogItem(source, LogLevel.DEBUG, id.toString(), message, body));
    }

    void logDebug(String source, LogId id, String message, String body, String dump) {
        _log.write(new LogItem(source, LogLevel.DEBUG, id.toString(), message, body, dump));
    }
}
