package se.precom.synchronization;

import java.util.UUID;

/**
 * Contains metadata related to sync object.
 */
public class SyncMetaObject {

    private UUID Guid;
    private int Version;
    private int Status;
    private UUID ChangeId;

    /**
     * Creates a new SyncMetaObject
     * 
     * @param objectId
     *            the object Id.
     * @param version
     *            the version for the object.
     */
    public SyncMetaObject(UUID objectId, int version) {
        Guid = objectId;
        Version = version;
    }

    /**
     * Returns the object Id.
     * 
     * @return the object Id.
     */
    public UUID getGuid() {
        return Guid;
    }

    /**
     * Sets the change Id.
     * 
     * @param guid
     *            the change Id.
     */
    public void setChangeId(UUID changeId) {
        ChangeId = changeId;
    }

    /**
     * Returns the change Id.
     * 
     * @return the change Id.
     */
    public UUID getChangeId() {
        return ChangeId;
    }

    /**
     * Sets the object Id.
     * 
     * @param guid
     *            the object Id.
     */
    public void setGuid(UUID guid) {
        Guid = guid;
    }

    /**
     * Returns the version of the object.
     * 
     * @return the version of the object.
     */
    public int getVersion() {
        return Version;
    }

    /**
     * Sets the version of the object.
     * 
     * @param version
     *            the version of the object
     */
    public void setVersion(int version) {
        Version = version;
    }

    /**
     * Returns the status of the object.
     * 
     * @return the status of the object.
     */
    public int getStatus() {
        return Status;
    }

    /**
     * Sets the status of the object.
     * 
     * @param status
     *            the status of the object.
     */
    public void setStatus(int status) {
        Status = status;
    }
}
