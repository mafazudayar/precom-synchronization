package se.precom.synchronization;

import java.util.UUID;

/**
 * Argument used to notify {@link OnEntityChangedListener}s on entity changed event.
 */
public class ChangeEventArgs {

    private UUID _id;
    private Operation _change;
    private String _entityType;
    private ISynchronizable _oldEntity;
    private ISynchronizable _newEntity;
    private String _originatorType;

    /**
     * Returns the Id of the changed entity.
     * 
     * @return the Id of the changed entity.
     */
    public UUID getId() {
        return _id;
    }

    /**
     * Sets the Id of the changed entity.
     * 
     * @param id
     *            The id of the changed entity.
     */
    public void setId(UUID id) {
        _id = id;
    }

    /**
     * Returns the status of the changed entity.
     * 
     * @return the status of the changed entity.
     */
    public Operation getEntityChange() {
        return _change;
    }

    /**
     * Sets the status of the changed entity.
     * 
     * @param change
     *            the status of the changed entity.
     */
    public void setEntityChange(Operation change) {
        _change = change;
    }

    /**
     * Returns the type of the changed entity.
     * 
     * @return the type of the changed entity.
     */
    public String getEntityType() {
        return _entityType;
    }

    /**
     * Sets the type of the changed entity.
     * 
     * @param entityType
     *            the type of the changed entity.
     */
    public void setEntityType(String entityType) {
        _entityType = entityType;
    }

    /**
     * Returns the old version of the entity which existed before the update.
     * 
     * @return the old version of the entity.
     */
    public ISynchronizable getOldEntity() {
        return _oldEntity;
    }

    /**
     * Sets the old version of the entity which existed before the update.
     * 
     * @param oldEntity
     *            the old version of the entity.
     */
    public void setOldEntity(ISynchronizable oldEntity) {
        this._oldEntity = oldEntity;
    }

    /**
     * Returns the new version of the entity which got updated.
     * 
     * @return the new version of the entity.
     */
    public ISynchronizable getNewEntity() {
        return _newEntity;
    }

    /**
     * Sets the new version of the entity which got updated.
     * 
     * @param newEntity
     *            the new version of the entity.
     */
    public void setNewEntity(ISynchronizable newEntity) {
        this._newEntity = newEntity;
    }

    /**
     * Gets the type of the caller that created, updated or deleted the entity.
     * 
     * @return The originator type of the change
     */
	public Object getOriginatorType() {
		return _originatorType;
    }

    /**
	 * Sets the type of the caller that created, updated or deleted the entity.
     * 
	 * @param originatorType
	 * 				the originator type of the change
     */
	public void setOriginatorType(String originatorType) {
		this._originatorType = originatorType;
    }

}
