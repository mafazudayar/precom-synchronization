package se.precom.synchronization;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import se.precom.core.storage.ObjectType;
import se.precom.core.storage.StorageBase;
import se.precom.core.storage.StorageQueryParameter;
import android.content.ContentValues;
import android.database.Cursor;

class SynchronizationDao {

    private final String METADATA_TABLE = "PMC_Synchronization_Metadata";
    private final String TYPE_COLUMN = "EntityType";
    private final String ENTITY_ID_COLUMN = "EntityId";
    private final String SERVER_VERSION_COLUMN = "EntityVersion";
    private final String STATUS_COLUMN = "Status";
    private final String SESSION_ID_COLUMN = "SessionId";
    private final String Change_ID_COLUMN = "ChangeId";

    private final String PARTIAL_ENTITY_TABLE = "PMC_Synchronization_Partial_Entity";
    private final String PACKET_NUMBER_COLUMN = "PacketNumber";
    private final String PARTIAL_ENTITY_COLUMN = "PartialEntity";
    private final String INSERTED_ON_COLUMN = "InsertionDate";

    private final String PROPERTY_METADATA_TABLE = "PMC_Synchronization_Property_Metadata";
    private final String PROPERTY_NAME_COLUMN = "PropertyName";
    private final String IS_CHANGED_COLUMN = "IsChanged";

    private final String PARTIAL_PULL_INVOKE_MESSAGE_TABLE = "PMC_Synchronization_Partial_PullInvokeMessage";
    private final String PARTIAL_PULL_INVOKE_MESSAGE_COLUMN = "PartialPullInvokeMessage";

    private final StorageBase _storage;

    SynchronizationDao(StorageBase storage) {
        _storage = storage;
    }

    void createDataBaseTables() {
        this.updateTableSchemas();
        this.createMetadataTable();
        this.createPartialEntityTable();
        this.createPropertyMetadataTable();
        this.createPartialPullInvokeTable();
        this.createTableIndexes();
    }

    private void updateTableSchemas()
    {
        if (_storage.objectExists(METADATA_TABLE, ObjectType.Table))
        {
            StorageQueryParameter queryParameter = new StorageQueryParameter();
            queryParameter.setParentName(METADATA_TABLE);
            queryParameter.setObjectType(ObjectType.Column);
            queryParameter.setObjectName(Change_ID_COLUMN);

            if (!_storage.objectExists(queryParameter)) {

                StringBuilder sb = new StringBuilder();
                sb.append("ALTER TABLE " + METADATA_TABLE);
                sb.append(" ADD ");
                sb.append(Change_ID_COLUMN + " TEXT");

                _storage.execSQL(sb.toString());
            }
        }
    }

    private void createMetadataTable() {
        if (!_storage.objectExists(METADATA_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE " + METADATA_TABLE + " (");
            sb.append(ENTITY_ID_COLUMN + " TEXT NOT NULL ,");
            sb.append(TYPE_COLUMN + " TEXT NOT NULL ,");
            sb.append(SERVER_VERSION_COLUMN + " INTEGER NOT NULL ,");
            sb.append(STATUS_COLUMN + " INTEGER NOT NULL,");
            sb.append(SESSION_ID_COLUMN + " TEXT,");
            sb.append(Change_ID_COLUMN + " TEXT,");
            sb.append("PRIMARY KEY (" + ENTITY_ID_COLUMN + ")");
            sb.append(")");
            _storage.execSQL(sb.toString());
        }
    }

    private void createPartialEntityTable() {
        if (!_storage.objectExists(PARTIAL_ENTITY_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE " + PARTIAL_ENTITY_TABLE + " (");
            sb.append(ENTITY_ID_COLUMN + " TEXT NOT NULL ,");
            sb.append(SESSION_ID_COLUMN + " TEXT NOT NULL ,");
            sb.append(PACKET_NUMBER_COLUMN + " INTEGER NOT NULL ,");
            sb.append(PARTIAL_ENTITY_COLUMN + " TEXT NOT NULL ,");
            sb.append(INSERTED_ON_COLUMN + " DATETIME NOT NULL)");
            _storage.execSQL(sb.toString());
        }
    }

    private void createPartialPullInvokeTable() {
        if (!_storage.objectExists(PARTIAL_PULL_INVOKE_MESSAGE_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE " + PARTIAL_PULL_INVOKE_MESSAGE_TABLE + " (");
            sb.append(SESSION_ID_COLUMN + " TEXT NOT NULL ,");
            sb.append(PACKET_NUMBER_COLUMN + " INTEGER NOT NULL ,");
            sb.append(PARTIAL_PULL_INVOKE_MESSAGE_COLUMN + " TEXT NOT NULL)");
            _storage.execSQL(sb.toString());
        }
    }

    private void createPropertyMetadataTable() {
        if (!_storage.objectExists(PROPERTY_METADATA_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE " + PROPERTY_METADATA_TABLE + " (");
            sb.append(ENTITY_ID_COLUMN + " TEXT NOT NULL ,");
            sb.append(PROPERTY_NAME_COLUMN + " TEXT NOT NULL ,");
            sb.append(IS_CHANGED_COLUMN + " INTEGER NOT NULL )");
            _storage.execSQL(sb.toString());
        }
    }

    private void createTableIndexes() {

        if (!isIndexExist("idx_PMC_Synchronization_Partial_Entity")) {

            StringBuilder sb = new StringBuilder();
            sb.append("CREATE INDEX idx_PMC_Synchronization_Partial_Entity ON ");
            sb.append(PARTIAL_ENTITY_TABLE);
            sb.append(" ( " + ENTITY_ID_COLUMN + " )");
            _storage.execSQL(sb.toString());

        }

        if (!isIndexExist("idx_PMC_Synchronization_Property_Metadata")) {

            StringBuilder sb = new StringBuilder();
            sb.append("CREATE INDEX idx_PMC_Synchronization_Property_Metadata ON ");
            sb.append(PROPERTY_METADATA_TABLE);
            sb.append(" ( " + ENTITY_ID_COLUMN + " )");
            _storage.execSQL(sb.toString());

        }

        if (!isIndexExist("idx_PMC_Synchronization_Partial_PullInvokeMessage")) {

            StringBuilder sb = new StringBuilder();
            sb.append("CREATE INDEX idx_PMC_Synchronization_Partial_PullInvokeMessage ON ");
            sb.append(PARTIAL_PULL_INVOKE_MESSAGE_TABLE);
            sb.append(" ( " + SESSION_ID_COLUMN + " )");
            _storage.execSQL(sb.toString());

        }
    }

    private boolean isIndexExist(String name) {

        boolean result = false;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT count(Name) FROM sqlite_master WHERE type='index' and Name = '" + name + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return result;

    }

    String getVersionForId(UUID objectId) {
        String version = null;
        if (_storage.objectExists(METADATA_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT ");
            sb.append(SERVER_VERSION_COLUMN);
            sb.append(" FROM " + METADATA_TABLE);
            sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + objectId.toString() + "'");
            Cursor cursor = null;
            try {
                cursor = _storage.rawQuery(sb.toString(), null);
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    version = cursor.getString(cursor.getColumnIndex(SERVER_VERSION_COLUMN));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return version;
    }

    int getStatusForId(UUID objectId) {
        int status = -1;
        if (_storage.objectExists(METADATA_TABLE, ObjectType.Table)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT ");
            sb.append(STATUS_COLUMN);
            sb.append(" FROM " + METADATA_TABLE);
            sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + objectId.toString() + "'");
            Cursor cursor = null;
            try {
                cursor = _storage.rawQuery(sb.toString(), null);
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    status = cursor.getInt(cursor.getColumnIndex(STATUS_COLUMN));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

        }
        return status;
    }

    boolean isExistingInMetadata(UUID objectId) {
        boolean result = false;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(SERVER_VERSION_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + objectId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                result = true;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return result;
    }

    private void update(String tableName, ContentValues contentValues, String whereClause) {
        try {
            _storage.beginTransaction();
            _storage.update(tableName, contentValues, whereClause, null);
            _storage.setTransactionSuccessful();
        } finally {
            _storage.endTransaction();
        }
    }

    void updateMetadataRecord(String objectType, UUID objectId, SyncStatus status) {
        // When the client updates the record, version is not updated. Only the
        // status is set to Updated. So this overload is used in such a
        // scenario.
        String whereClause = TYPE_COLUMN + "='" + objectType + "' AND " + ENTITY_ID_COLUMN + "='" + objectId.toString()
                + "'";
        ContentValues cv = new ContentValues();

        cv.put(STATUS_COLUMN, Integer.valueOf(status.getValue()));

        this.update(METADATA_TABLE, cv, whereClause);
    }

    void updateMetadataRecord(String objectType, UUID objectId, int version, SyncStatus status) {

        String whereClause = TYPE_COLUMN + "='" + objectType + "' AND " + ENTITY_ID_COLUMN + "='" + objectId.toString()
                + "'";

        ContentValues cv = new ContentValues();
        cv.put(SERVER_VERSION_COLUMN, Integer.valueOf(version));
        cv.put(STATUS_COLUMN, Integer.valueOf(status.getValue()));

        this.update(METADATA_TABLE, cv, whereClause);
    }

    void updateMetadataRecord(String objectType, UUID objectId, int version) {

        String whereClause = TYPE_COLUMN + "='" + objectType + "' AND " + ENTITY_ID_COLUMN + "='" + objectId.toString()
                + "'";

        ContentValues cv = new ContentValues();
        cv.put(SERVER_VERSION_COLUMN, Integer.valueOf(version));

        this.update(METADATA_TABLE, cv, whereClause);
    }

    void updateMetadataRecord(String objectType, UUID objectId, SyncStatus status, UUID changeId) {

        String whereClause = TYPE_COLUMN + "='" + objectType + "' AND " + ENTITY_ID_COLUMN + "='" + objectId.toString()
                + "'";

        ContentValues cv = new ContentValues();
        cv.put(STATUS_COLUMN, Integer.valueOf(status.getValue()));
        cv.put(Change_ID_COLUMN, changeId.toString());

        this.update(METADATA_TABLE, cv, whereClause);
    }

    void updateSessionId(List<UUID> idList, UUID syncSessionId) {
        if (idList != null && idList.size() > 0) {
            StringBuilder whereClause = new StringBuilder();
            whereClause.append(ENTITY_ID_COLUMN + " IN (");

            for (UUID id : idList) {
                whereClause.append("'" + id.toString() + "',");
            }
            whereClause.deleteCharAt(whereClause.length() - 1);
            whereClause.append(")");
            ContentValues cv = new ContentValues();
            cv.put(SESSION_ID_COLUMN, syncSessionId.toString());
            this.update(METADATA_TABLE, cv, whereClause.toString());
        }
    }

    private void insert(String tableName, ContentValues cv) {
        try {
            _storage.beginTransaction();
            _storage.insert(tableName, cv);
            _storage.setTransactionSuccessful();
        } finally {
            _storage.endTransaction();
        }
    }

    private void insertMetadataRecord(String objectType, UUID objectId, int version, SyncStatus status) {

        ContentValues cv = new ContentValues();
        cv.put(TYPE_COLUMN, objectType);
        cv.put(ENTITY_ID_COLUMN, objectId.toString());
        cv.put(SERVER_VERSION_COLUMN, Integer.valueOf(version));
        cv.put(STATUS_COLUMN, Integer.valueOf(status.getValue()));
        cv.put(Change_ID_COLUMN, UUID.randomUUID().toString());

        _storage.insert(METADATA_TABLE, cv);
    }

    void deleteMetadataRecord(String objectType, UUID objectId) {
        String whereClause = ENTITY_ID_COLUMN + "='" + objectId.toString() + "' AND " + TYPE_COLUMN + "='" + objectType
                + "'";
        _storage.delete(METADATA_TABLE, whereClause, null);
    }

    SyncMetaObject getMetadata(String objectType, UUID objectId) {
        int status = -1;
        int version = -1;
        UUID changeId = UUID.randomUUID();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(SERVER_VERSION_COLUMN + "," + STATUS_COLUMN + "," + Change_ID_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + TYPE_COLUMN + "='" + objectType + "' AND " + ENTITY_ID_COLUMN + "='"
                + objectId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                status = cursor.getInt(cursor.getColumnIndex(STATUS_COLUMN));
                version = cursor.getInt(cursor.getColumnIndex(SERVER_VERSION_COLUMN));
                String change = cursor.getString(cursor.getColumnIndex(Change_ID_COLUMN));
                if (change != null && !change.equals("")) {
                    changeId = UUID.fromString(change);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        SyncMetaObject syncMeta = new SyncMetaObject(objectId, version);
        syncMeta.setStatus(status);
        syncMeta.setChangeId(changeId);
        return syncMeta;
    }

    List<SyncObject> getSyncObjectsForModifiedEntities(String objectType) {
        List<SyncObject> metadata = new ArrayList<SyncObject>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(ENTITY_ID_COLUMN + "," + SERVER_VERSION_COLUMN + "," + STATUS_COLUMN + "," + Change_ID_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + STATUS_COLUMN + "!=" + SyncStatus.SYNCHRONIZED.getValue() + " AND " + TYPE_COLUMN + "='"
                + objectType + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    UUID objectId = UUID.fromString(cursor.getString(0));
                    SyncObject syncMetadata = new SyncObject(objectId, cursor.getInt(cursor
                            .getColumnIndex(SERVER_VERSION_COLUMN)));
                    syncMetadata.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS_COLUMN)));
                    String change = cursor.getString(cursor.getColumnIndex(Change_ID_COLUMN));
                    UUID changeId = UUID.randomUUID();
                    if (change != null && !change.equals("")) {
                        changeId = UUID.fromString(change);
                    }
                    syncMetadata.setChangeId(changeId);
                    metadata.add(syncMetadata);
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return metadata;
    }

    List<SyncMetaObject> getSyncObjectsForDeletedEntities(String objectType) {
        List<SyncMetaObject> metadata = new ArrayList<SyncMetaObject>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(ENTITY_ID_COLUMN + "," + SERVER_VERSION_COLUMN + "," + STATUS_COLUMN + "," + Change_ID_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + STATUS_COLUMN + "=" + SyncStatus.DELETED.getValue() + " AND " + TYPE_COLUMN + "='"
                + objectType + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    UUID objectId = UUID.fromString(cursor.getString(0));
                    SyncMetaObject syncMetadata = new SyncMetaObject(objectId,
                            cursor.getInt(cursor.getColumnIndex(SERVER_VERSION_COLUMN)));
                    syncMetadata.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS_COLUMN)));
                    String change = cursor.getString(cursor.getColumnIndex(Change_ID_COLUMN));
                    UUID changeId = UUID.randomUUID();
                    if (change != null && !change.equals("")) {
                        changeId = UUID.fromString(change);
                    }
                    syncMetadata.setChangeId(changeId);
                    metadata.add(syncMetadata);
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return metadata;
    }

    String getMostRecentSessionId(UUID objectId) {
        // If there is no metadata for the given object, this methods return a empty string.
        // If there is a metadata for the given object and if it doesn't have a sync session, this method returns a
        // null.
        String sessionId = "";
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(SESSION_ID_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + objectId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                sessionId = cursor.getString(0);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return sessionId;
    }

    UUID getChangeId(UUID objectId) {
        // If there is no metadata for the given object, this cursor return a empty string.
        // If there is a metadata for the given object and if it doesn't have a changeId, cursor returns a
        // null. (Here in this both cases this method returns a new UUID)
        UUID changeId = UUID.randomUUID();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(Change_ID_COLUMN);
        sb.append(" FROM " + METADATA_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + objectId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                String change = cursor.getString(0);
                if (change != null && !change.equals("")) {
                    changeId = UUID.fromString(change);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return changeId;
    }

    void insertPartialEntity(UUID objectId, UUID sessionId, int packetNumber, String partialEntity) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateAndTime = sdf.format(new Date(System.currentTimeMillis()));

        ContentValues cv = new ContentValues();
        cv.put(ENTITY_ID_COLUMN, objectId.toString());
        cv.put(SESSION_ID_COLUMN, sessionId.toString());
        cv.put(PACKET_NUMBER_COLUMN, Integer.valueOf(packetNumber));
        cv.put(PARTIAL_ENTITY_COLUMN, partialEntity);
        cv.put(INSERTED_ON_COLUMN, currentDateAndTime);

        insert(PARTIAL_ENTITY_TABLE, cv);
    }

    int getPartialEntityCount(UUID entityId, UUID sessionId) {
        int count = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(PACKET_NUMBER_COLUMN);
        sb.append(" FROM " + PARTIAL_ENTITY_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + entityId.toString() + "' AND " + SESSION_ID_COLUMN + "='"
                + sessionId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            count = cursor.getCount();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return count;
    }

    List<String> getPartialEntities(UUID entityId, UUID sessionId) {
        List<String> partialObjects = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(PARTIAL_ENTITY_COLUMN);
        sb.append(" FROM " + PARTIAL_ENTITY_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + entityId.toString() + "' AND " + SESSION_ID_COLUMN + "='"
                + sessionId.toString() + "'");
        sb.append(" ORDER BY " + PACKET_NUMBER_COLUMN + " ASC");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    partialObjects.add(cursor.getString(cursor.getColumnIndex(PARTIAL_ENTITY_COLUMN)));
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return partialObjects;
    }

    void removePartialEntities(UUID entityId, UUID sessionId) {
        String whereClause = ENTITY_ID_COLUMN + "='" + entityId.toString() + "' AND " + SESSION_ID_COLUMN + "='"
                + sessionId.toString() + "'";
        _storage.delete(PARTIAL_ENTITY_TABLE, whereClause, null);
    }

    void insertPropertyMetadataRecords(UUID entityId, List<String> properties, PropertySyncStatus propertySyncStatus) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO " + PROPERTY_METADATA_TABLE);
        sb.append(" SELECT '" + entityId.toString() + "' AS " + ENTITY_ID_COLUMN + ", '" + properties.get(0) + "' AS "
                + PROPERTY_NAME_COLUMN + ", " + propertySyncStatus.getValue() + " AS " + IS_CHANGED_COLUMN);
        for (int i = 1; i < properties.size(); i++) {
            sb.append(" UNION SELECT '" + entityId.toString() + "', '" + properties.get(i) + "', "
                    + propertySyncStatus.getValue());
        }

        _storage.execSQL(sb.toString());
    }

    void insertMetadataAndPropertyMetadataRecords(String objectType, List<SyncObject> syncObjects,
            List<String> properties, SyncStatus syncStatus, boolean isPropertySyncEnabled) {

        try {

            _storage.beginTransaction();

            for (SyncObject syncObject : syncObjects) {

                insertMetadataRecord(objectType, syncObject.getGuid(), syncObject.getVersion(), syncStatus);

                if (isPropertySyncEnabled) {
                    PropertySyncStatus propertySyncStatus = null;
                    if (syncStatus == SyncStatus.CREATED) {
                        propertySyncStatus = PropertySyncStatus.IS_CHANGED;
                    } else if (syncStatus == SyncStatus.SYNCHRONIZED) {
                        propertySyncStatus = PropertySyncStatus.SYNCHRONIZED;
                    }
                    insertPropertyMetadataRecords(syncObject.getGuid(), properties, propertySyncStatus);
                }
            }

            _storage.setTransactionSuccessful();
        } finally {
            _storage.endTransaction();
        }
    }

    void markPropertiesAsChanged(UUID entityId, List<String> changedProperties) {
        updatePropertyMetadata(entityId, changedProperties, PropertySyncStatus.IS_CHANGED);
    }

    void markPropertiesAsSynchronized(UUID entityId, List<String> properties) {
        updatePropertyMetadata(entityId, properties, PropertySyncStatus.SYNCHRONIZED);
    }

    private void updatePropertyMetadata(UUID entityId, List<String> properties, PropertySyncStatus syncStatus) {
        if (properties != null && properties.size() > 0) {
            StringBuilder whereClause = new StringBuilder();
            whereClause.append(ENTITY_ID_COLUMN + "='" + entityId.toString() + "' AND " + PROPERTY_NAME_COLUMN
                    + " IN (");

            for (String property : properties) {
                whereClause.append("'" + property + "',");
            }
            whereClause.deleteCharAt(whereClause.length() - 1);
            whereClause.append(")");
            ContentValues cv = new ContentValues();
            cv.put(IS_CHANGED_COLUMN, Integer.valueOf(syncStatus.getValue()));
            this.update(PROPERTY_METADATA_TABLE, cv, whereClause.toString());
        }
    }

    void markAllPropertiesAsChanged(UUID entityId) {
        markAllProperties(entityId, PropertySyncStatus.IS_CHANGED);
    }

    void markAllPropertiesAsSynchronized(UUID entityId) {
        markAllProperties(entityId, PropertySyncStatus.SYNCHRONIZED);
    }

    private void markAllProperties(UUID entityId, PropertySyncStatus propertySyncStatus) {
        StringBuilder whereClause = new StringBuilder();
        whereClause.append(ENTITY_ID_COLUMN + "='" + entityId.toString() + "'");
        ContentValues cv = new ContentValues();
        cv.put(IS_CHANGED_COLUMN, Integer.valueOf(propertySyncStatus.getValue()));
        this.update(PROPERTY_METADATA_TABLE, cv, whereClause.toString());
    }

    List<String> getChangedProperties(UUID entityId) {
        List<String> changedProperties = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(PROPERTY_NAME_COLUMN);
        sb.append(" FROM " + PROPERTY_METADATA_TABLE);
        sb.append(" WHERE " + ENTITY_ID_COLUMN + "='" + entityId.toString() + "' AND " + IS_CHANGED_COLUMN + "="
                + PropertySyncStatus.IS_CHANGED.getValue());
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    changedProperties.add(cursor.getString(cursor.getColumnIndex(PROPERTY_NAME_COLUMN)));
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return changedProperties;
    }

    void deleteAllPropertyMetadataRecords(UUID entityId) {
        String whereClause = ENTITY_ID_COLUMN + "='" + entityId.toString() + "'";
        _storage.delete(PROPERTY_METADATA_TABLE, whereClause, null);
    }

    void insertPartialPullInvokeMessage(UUID sessionId, int packetNumber, String knowledge) {

        ContentValues cv = new ContentValues();
        cv.put(SESSION_ID_COLUMN, sessionId.toString());
        cv.put(PACKET_NUMBER_COLUMN, Integer.valueOf(packetNumber));
        cv.put(PARTIAL_PULL_INVOKE_MESSAGE_COLUMN, knowledge);

        insert(PARTIAL_PULL_INVOKE_MESSAGE_TABLE, cv);
    }

    int getPartialPullInvokeMessageCount(UUID sessionId) {
        int count = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(PACKET_NUMBER_COLUMN);
        sb.append(" FROM " + PARTIAL_PULL_INVOKE_MESSAGE_TABLE);
        sb.append(" WHERE " + SESSION_ID_COLUMN + "='"
                + sessionId.toString() + "'");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            count = cursor.getCount();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return count;
    }

    List<String> getPartialPullInvokeMessages(UUID sessionId) {
        List<String> partialObjects = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(PARTIAL_PULL_INVOKE_MESSAGE_COLUMN);
        sb.append(" FROM " + PARTIAL_PULL_INVOKE_MESSAGE_TABLE);
        sb.append(" WHERE " + SESSION_ID_COLUMN + "='" + sessionId.toString() + "'");
        sb.append(" ORDER BY " + PACKET_NUMBER_COLUMN + " ASC");
        Cursor cursor = null;
        try {
            cursor = _storage.rawQuery(sb.toString(), null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    partialObjects.add(cursor.getString(cursor.getColumnIndex(PARTIAL_PULL_INVOKE_MESSAGE_COLUMN)));
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return partialObjects;
    }

    void removePartialPullInvokeMessages(UUID sessionId) {
        String whereClause = SESSION_ID_COLUMN + "='" + sessionId.toString() + "'";
        _storage.delete(PARTIAL_PULL_INVOKE_MESSAGE_TABLE, whereClause, null);
    }
}
