package se.precom.synchronization;

enum PropertySyncStatus {

	SYNCHRONIZED(0), IS_CHANGED(1);

	private int _value;

	private PropertySyncStatus(int value) {
		this._value = value;
	}

	public int getValue() {
		return _value;
	}
}
