package se.precom.synchronization;

import java.util.ArrayList;

import com.google.gson.reflect.TypeToken;

/**
 * Entity sent by the client synchronization module to initiate a client pull process.
 * 
 */
public class PullRequest extends SynchronizationPacket {

    private EntityFilter Filter;
    private String Knowledge;
    private int SyncType;
    private String PlatformType = "Android";

    /**
     * Returns the entity filter.
     * 
     * @return {@link EntityFilter}.
     */
    public EntityFilter getFilter() {
        return Filter;
    }

    /**
     * Sets the entity filter.
     * 
     * @param filter
     *            {@link EntityFilter}.
     */
    public void setFilter(EntityFilter filter) {
        Filter = filter;
    }

    /**
     * Returns the Knowledge
     * 
     * @return Knowledge
     */
    public String getKnowledge() {
        return Knowledge;
    }

    /**
     * Sets the Knowledge.
     * 
     * @param knowledge
     * 
     */
    public void setKnowledge(String knowledge) {
        Knowledge = knowledge;
    }

    /**
     * Returns the PlatFormType
     * 
     * @return PlatFormType
     */
    public String getPlatFormType() {
        return PlatformType;
    }

    /**
     * Returns the type of the synchronization process.
     * 
     * @return {@link SyncEventType}.
     */
    public SyncEventType getSyncEventType() {
        return SyncEventType.fromInt(SyncType);
    }

    /**
     * Sets the type of the synchronization process.
     * 
     * @param syncEventType
     *            {@link SyncEventType}.
     */
    public void setSyncEventType(SyncEventType syncEventType) {
        SyncType = syncEventType.getValue();
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString()
    {
        ArrayList<SyncMetaObject> _knowledge = null;
        if (super.isSplit() != true && Knowledge != null && !Knowledge.isEmpty()) {
            _knowledge = JsonSerializer.deserialize(Knowledge, new TypeToken<ArrayList<SyncMetaObject>>() {
            }.getType());
        }

        return super.getDebugString() + String.format(", Filter(Count): %s, SyncType: %s, Knowledge(Count): %s",
                getFilter() != null && getFilter().getFilterList() != null ? getFilter().getFilterList().size() : "0",
                getSyncEventType(),
                _knowledge != null ? _knowledge.size() : "N/A");

    }
}
