package se.precom.synchronization;

/**
 * 
 * Represents the type of the synchronization process.
 * 
 */
public enum SyncEventType {
    /**
     * Client initiated pull.
     */
    CLIENT_PULL(0),
    /**
     * Client initiated push.
     */
    CLIENT_PUSH(1),
    /**
     * Server initiated push.
     */
    SERVER_PUSH(2);

    private final int _value;

    SyncEventType(int value) {
        this._value = value;
    }

    int getValue() {
        return _value;
    }

    static SyncEventType fromInt(int syncType) {

        for (SyncEventType syncEventType : SyncEventType.values()) {
            if (syncType == syncEventType.getValue()) {
                return syncEventType;
            }
        }

        throw new IllegalArgumentException("Invalid SyncEventType");
    }
}
