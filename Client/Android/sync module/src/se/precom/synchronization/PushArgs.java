package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Argument used when starting a push via {@link SynchronizationClientBase#push(Class, PushArgs)}
 * 
 */
public class PushArgs {

    private List<UUID> _entityIds;

    /**
     * Initializes a new instance of the {@link PushArgs}.
     */
    public PushArgs() {
        _entityIds = new ArrayList<UUID>();
    }

    /**
     * Initializes a new instance of the {@link PushArgs}.
     * 
     * @param entityIds
     *            list of entityIds.
     */
    public PushArgs(List<UUID> entityIds) {
        _entityIds = entityIds;
    }

    /**
     * Returns the list of entityIds.
     * 
     * @return the list of entityId.
     */
    public List<UUID> getEntityIdList() {
        return _entityIds;
    }
}
