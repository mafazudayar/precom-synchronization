package se.precom.synchronization;

/**
 * Contains information about a given property, which is used to build the filter query.
 * 
 */
public class Filter {

    private Object Value;
    private String PropertyName;
    private int Operator;

    /**
     * Initializes a new instance of the {@link Filter}.
     */
    public Filter() {
    }

    /**
     * Initializes a new instance of the {@link Filter}.
     * 
     * @param filterOperator
     *            {@link FilterOperator}.
     * @param propertyName
     *            The name of the property.
     * @param value
     *            Which is to be compared with the property.
     */
    public Filter(FilterOperator filterOperator, String propertyName, Object value)
    {
        setOperator(filterOperator);
        PropertyName = propertyName;
        Value = value;
    }

    /**
     * Returns the value which is to be compared with the property.
     * 
     * @return the value which is to be compared with the property.
     */
    public Object getValue() {
        return Value;
    }

    /**
     * Sets the value which is to be compared with the property.
     * 
     * @param value
     *            the value which is to be compared with the property.
     */
    public void setValue(Object value) {
        this.Value = value;
    }

    /**
     * Returns the name of the property.
     * 
     * @return the name of the property.
     */
    public String getPropertyName() {
        return PropertyName;
    }

    /**
     * Sets the name of the property.
     * 
     * @param propertyName
     *            the name of the property.
     */
    public void setPropertyName(String propertyName) {
        this.PropertyName = propertyName;
    }

    /**
     * Returns the {@link FilterOperator}.
     * 
     * @return the {@link FilterOperator}.
     */
    public FilterOperator getOperator() {
        int position = 0;
        for (FilterOperator filterOperator : FilterOperator.values()) {
            if (Operator == filterOperator.getValue()) {
                position = filterOperator.ordinal();
            }
        }
        return FilterOperator.values()[position];
    }

    /**
     * Sets the value for the given {@link FilterOperator}.
     * 
     * @param operator
     *            the operator.
     */
    public void setOperator(FilterOperator operator) {
        this.Operator = operator.getValue();
    }

}
