package se.precom.synchronization;

/**
 * Thrown when an error happens in an implementation of
 * {@link SynchronizedStorageBase}.
 */
public class SynchronizedStorageException extends Exception {

	private static final long serialVersionUID = 3758574621838659838L;

	/**
	 * Constructs a new SynchronizedStorageException with the current stack
	 * trace.
	 */
	public SynchronizedStorageException() {
	}

	/**
	 * Constructs a new SynchronizedStorageException with the current stack
	 * trace and the specified detail message.
	 * 
	 * @param detailMessage
	 *            the detail message for this exception.
	 */
	public SynchronizedStorageException(String detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructs a new SynchronizedStorageException with the current stack
	 * trace and the specified cause.
	 * 
	 * @param throwable
	 *            the cause of this exception.
	 */
	public SynchronizedStorageException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * Constructs a new SynchronizedStorageException with the current stack
	 * trace and the specified cause and detail message.
	 * 
	 * @param detailMessage
	 *            the detail message for this exception.
	 * @param throwable
	 *            the cause of this exception.
	 */
	public SynchronizedStorageException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
}
