package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;

/**
 * Response for the push request. This contains the information about accepted objects and resolved objects.
 * 
 */
public class PushResponse extends SynchronizationPacket {

    private List<SyncMetaObject> AcceptedObjects = new ArrayList<SyncMetaObject>();
    private List<SyncObject> ResolvedObjects = new ArrayList<SyncObject>();

    /**
     * Returns a list of {@link SyncMetaObject}s which contains information about the entities accepted by the server.
     * 
     * @return list of {@link SyncMetaObject}s.
     */
    public List<SyncMetaObject> getAcceptedObjects() {
        return AcceptedObjects;
    }

    /**
     * Returns a list of {@link SyncObject}s which contains the server versions of the entities resolved by the server.
     * 
     * @return list of {@link SyncObject}s.
     */
    public List<SyncObject> getResolvedObjects() {
        return ResolvedObjects;
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString()
    {
        return super.getDebugString() + String.format(", AcceptedObjects(Count): %s, ResolvedObjects(Count): %s",
                getAcceptedObjects() != null ? getAcceptedObjects().size() : "0",
                getResolvedObjects() != null ? getResolvedObjects().size() : "0");
    }
}
