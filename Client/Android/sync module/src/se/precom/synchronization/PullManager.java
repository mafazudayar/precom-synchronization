package se.precom.synchronization;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.CommunicationBase.OnReceiveListener;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.TypeReference;
import se.precom.core.log.LogBase;
import se.precom.core.storage.StorageBase;

import com.google.gson.reflect.TypeToken;

class PullManager extends SynchronizationManager {

    private final OnReceiveListener<PullInvokeMessage> _pullInvokeMessageReceiveListener;
    private final OnReceiveListener<PullResponse> _pullResponseReceiveListener;
    private final String EMPTY_GUID = "00000000-0000-0000-0000-000000000000";
    final int EMPTY_PULL_REQUEST_SIZE = 300;
    private final Hashtable<UUID, Integer> PullResponsesSessions = new Hashtable<UUID, Integer>();

    PullManager(TypeManager typeManager, JsonSerializer jsonSerializer, SynchronizationModule syncModule,
            int maximumSizeForTransferObject, CommunicationBase communication, StorageBase storage, LogBase log) {

        super(typeManager, jsonSerializer, syncModule,
                maximumSizeForTransferObject, communication, storage, log);

        _pullResponseReceiveListener = new OnReceiveListener<PullResponse>() {
            @Override
            public void receive(PullResponse entity) {
                PullManager.this.processPullResponse(entity);
            }
        };

        getCommunication().addOnReceiveListener(new TypeReference<PullResponse>() {
                }, _pullResponseReceiveListener);

        _pullInvokeMessageReceiveListener = new OnReceiveListener<PullInvokeMessage>() {

            @Override
            public void receive(PullInvokeMessage entity) {
                PullManager.this.processPullInvoke(entity);
            }
        };

        getCommunication().addOnReceiveListener(new TypeReference<PullInvokeMessage>() {
                }, _pullInvokeMessageReceiveListener);

    }

    private void processPullResponse(final PullResponse entity) {
        Thread pullResponceHandler = new Thread() {
            @Override
            public void run() {
                try {
                    PullManager.this.onPullResponseReceived(entity);
                } catch (IllegalArgumentException e) {
                    // For ELT-1030, don't log this as error right now. They
                    // register objects in server that are not
                    // registered in client
                    getLogHelper().logDebug(
                                    getModuleName(), LogId.SYNC_PULL_E_2,
                                    "IllegalArgumentException when processing pull response",
                                    e.getMessage());
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_2,
                            "Exception when processing pull response", e);
                }
            }
        };

        pullResponceHandler.start();
    }

    private void processPullInvoke(final PullInvokeMessage entity) {
        Thread pullInvokeHandler = new Thread() {
            @Override
            public void run() {
                try {
                    PullManager.this.onPullInvokeMessageReceived(entity);
                } catch (IllegalArgumentException e) {
                    // For ELT-1030, don't log this as error right now. They
                    // register objects in server that are not
                    // registered in client
                    getLogHelper().logDebug(
                                    getModuleName(), LogId.SYNC_PULL_E_2,
                                    "IllegalArgumentException when processing pull invoke message",
                                    e.getMessage());
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_2,
                            "Exception when processing pull invoke message", e);
                }
            }
        };

        pullInvokeHandler.start();
    }

    void dispose() {

        getCommunication().removeOnReceiveListener(new TypeReference<PullResponse>() {
                }, _pullResponseReceiveListener);

        getCommunication().removeOnReceiveListener(
                        new TypeReference<PullInvokeMessage>() {
                        }, _pullInvokeMessageReceiveListener);
    }

    PullResult pull(String objectType, PullArgs pullArgs,
            SyncEventType syncEventType) throws NotLoggedInException,
            MaxMessageSizeException, SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            PullResult pullResult = new PullResult();
            UUID syncSessionId = UUID.randomUUID();
            pullResult.setSyncSession(syncSessionId);

            pullDeletedEntities(objectType, syncSessionId, syncEventType);

            if (isEnablePerformanceLogs()) {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_1, "Performance Logs", "Pull request initiated",
                        "Object Type: "
                                + objectType + ", Sync Session Id: " + syncSessionId);

                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_2, "Performance Logs", "Calling custom storage",
                        "Method: readIds(EntityFilter filter), Object Type: " + objectType + ", Sync Session Id: "
                                + syncSessionId);
            }

            SynchronizedStorageBase<ISynchronizable> storage = getTypeManager().getStorage(objectType);

            List<UUID> listOfEntityIds = storage.readIds(pullArgs.getFilter());

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_3, "Performance Logs",
                        "Custom storage responded",
                        "Object Type: " + objectType + ", Sync Session Id: " + syncSessionId);
            }

            List<UUID> listOfEntityIdsToPull = new ArrayList<UUID>();
            ArrayList<SyncMetaObject> listOfSyncObjects = createSyncMetaObjectsForIds(
                    listOfEntityIds, objectType, listOfEntityIdsToPull);
            pull(objectType, listOfSyncObjects, syncEventType, syncSessionId,
                    pullArgs, listOfEntityIdsToPull);

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_4, "Performance Logs", "Pull requests sent",
                        "Object Type: "
                                + objectType + ", Sync Session Id: " + syncSessionId);
            }

            return pullResult;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void pullDeletedEntities(String objectType, UUID syncSessionId, SyncEventType syncEventType)
            throws MaxMessageSizeException, NotLoggedInException
    {
        PullArgs pullArgs = new PullArgs(null);
        ArrayList<SyncMetaObject> listOfSyncObjects = new ArrayList<SyncMetaObject>();
        List<UUID> listOfEntityIdsToPull = new ArrayList<UUID>();

        List<SyncMetaObject> deletedList = getSynchronizationDao().getSyncObjectsForDeletedEntities(objectType);

        if (deletedList.size() > 0)
        {
            for (SyncMetaObject syncEntityData : deletedList)
            {
                listOfSyncObjects.add(syncEntityData);
                listOfEntityIdsToPull.add(syncEntityData.getGuid());
            }

            pull(objectType, listOfSyncObjects, syncEventType, syncSessionId, pullArgs, listOfEntityIdsToPull);
        }
    }

    PullResult pull(String objectType, List<UUID> listOfEntityIds, PullArgs pullArgs,
            SyncEventType syncEventType) throws MaxMessageSizeException, NotLoggedInException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            PullResult pullResult = new PullResult();
            UUID syncSessionId = UUID.randomUUID();
            pullResult.setSyncSession(syncSessionId);
            ArrayList<SyncMetaObject> listOfSyncObjects = new ArrayList<SyncMetaObject>();
            for (UUID objectId : listOfEntityIds) {
                SyncMetaObject syncMetaObject = getSynchronizationDao()
                        .getMetadata(objectType, objectId);
                if (syncMetaObject.getVersion() == -1) {
                    syncMetaObject.setVersion(0);
                }
                listOfSyncObjects.add(syncMetaObject);
            }
            pull(objectType, listOfSyncObjects, syncEventType, syncSessionId,
                    pullArgs, listOfEntityIds);
            return pullResult;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void pull(String objectType,
            ArrayList<SyncMetaObject> listOfSyncObjects,
            SyncEventType syncEventType, UUID syncSessionId, PullArgs pullArgs,
            List<UUID> listOfEntityIds) throws MaxMessageSizeException,
            NotLoggedInException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            if (listOfSyncObjects.size() != 0) {
                sendPullRequests(objectType, listOfSyncObjects, syncEventType,
                        syncSessionId, pullArgs);
            } else {
                sendNewPullRequest("", syncSessionId, pullArgs, objectType,
                        syncEventType);
            }
            getSynchronizationDao().updateSessionId(listOfEntityIds,
                    syncSessionId);

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void sendPullRequests(String objectType,
            ArrayList<SyncMetaObject> listOfSyncObjects,
            SyncEventType syncEventType, UUID syncSessionId, PullArgs pullArgs)
            throws NotLoggedInException, MaxMessageSizeException {

        String serializedObjects = getJsonSerializer().serialize(
                listOfSyncObjects);

        // Fixed - PC-2506, we need to leave some space for the characters
        // created due to double serialization.
        int possibleSizeIncrement = (getCharacterOccurences(serializedObjects) * 2) + 2;

        // We deduct 300 (EMPTY_PULL_REQUEST_SIZE) to leave some space for the
        // pull request.
        int allowedMaxPacketSize = getMaximumSizeForTransferObject()
                - EMPTY_PULL_REQUEST_SIZE - possibleSizeIncrement;
        if (serializedObjects.length() > allowedMaxPacketSize) {
            String[] entitySplits = splitSerializedEntity(serializedObjects,
                    allowedMaxPacketSize);

            for (int x = 0; x < entitySplits.length; x++) {
                sendNewPullRequest(entitySplits[x], syncSessionId, pullArgs,
                        entitySplits.length, x + 1, objectType, syncEventType);
            }

        } else {
            sendNewPullRequest(serializedObjects, syncSessionId, pullArgs,
                    objectType, syncEventType);
        }
    }

    private void sendNewPullRequest(String objectsToBeSent, UUID sessionId,
            PullArgs pullArgs, String tEntitySimpleName,
            SyncEventType syncEventType) throws NotLoggedInException,
            MaxMessageSizeException {

        PullRequest pullRequest = new PullRequest();
        pullRequest.setKnowledge(objectsToBeSent);
        pullRequest.setObjectType(tEntitySimpleName);
        pullRequest.setSessionGuid(sessionId);
        pullRequest.setFilter(pullArgs.getFilter());
        pullRequest.setSyncEventType(syncEventType);
        pullRequest.setIsSplit(false);

        RequestArgs reqArgs = new RequestArgs(new TypeReference<PullRequest>() {
        });

        sendRequest(pullRequest, reqArgs);

        notifyOnSyncEventListeners(tEntitySimpleName,
                UUID.fromString(EMPTY_GUID), SyncEventReason.STARTED,
                syncEventType, sessionId);
    }

    private void sendNewPullRequest(String objectsToBeSent, UUID sessionId,
            PullArgs pullArgs, int totalNumberOfPackets, int packetNumber,
            String tEntitySimpleName, SyncEventType syncEventType)
            throws NotLoggedInException, MaxMessageSizeException {

        PullRequest pullRequest = new PullRequest();
        pullRequest.setKnowledge(objectsToBeSent);
        pullRequest.setObjectType(tEntitySimpleName);
        pullRequest.setSessionGuid(sessionId);
        pullRequest.setFilter(pullArgs.getFilter());
        pullRequest.setTotalNumberOfPackets(totalNumberOfPackets);
        pullRequest.setPacketNumber(packetNumber);
        pullRequest.setIsSplit(true);
        pullRequest.setSyncEventType(syncEventType);

        RequestArgs reqArgs = new RequestArgs(new TypeReference<PullRequest>() {
        });

        sendRequest(pullRequest, reqArgs);
        // invoke notifyOnSyncEventListener, only when starting to send the
        // first packet.
        if (packetNumber == 1) {
            notifyOnSyncEventListeners(tEntitySimpleName,
                    UUID.fromString(EMPTY_GUID), SyncEventReason.STARTED,
                    syncEventType, sessionId);
        }
    }

    private void onPullInvokeMessageReceived(PullInvokeMessage message) {

        String objectType = message.getObjectType();

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_10, "PullInvokeMessage received",
                    ((IDebugPrintable) message).getDebugString());

            List<UUID> receivedIdList = new ArrayList<UUID>();

            if (message.isSplit()) {

                getSynchronizationDao().insertPartialPullInvokeMessage(
                        message.getSessionGuid(), message.getPacketNumber(),
                        message.getKnowledge());

                int receivedCount = getSynchronizationDao()
                        .getPartialPullInvokeMessageCount(
                                message.getSessionGuid());

                if (receivedCount == message.getTotalNumberOfPackets()) {
                    List<String> partialKnowledge = getSynchronizationDao()
                            .getPartialPullInvokeMessages(
                                    message.getSessionGuid());
                    String knowledge = combinePartialItems(partialKnowledge);

                    receivedIdList = JsonSerializer.deserialize(knowledge,
                            new TypeToken<ArrayList<UUID>>() {
                            }.getType());

                    getSynchronizationDao().removePartialPullInvokeMessages(
                            message.getSessionGuid());
                } else {
                    return;
                }
            } else {

                if (message.getKnowledge() != null
                        && !message.getKnowledge().isEmpty()) {
                    if (message.getPullInvokeReason() == PullInvokeReason.CLIENT_LOGIN) {
                        receivedIdList = JsonSerializer.deserialize(
                                message.getKnowledge(),
                                new TypeToken<ArrayList<UUID>>() {
                                }.getType());
                    } else if (message.getPullInvokeReason() == PullInvokeReason.AUTOMATIC_PUSH) {
                        receivedIdList.add(UUID.fromString(message
                                .getKnowledge()));
                    }
                }
            }

            if (message.getPullInvokeReason() == PullInvokeReason.CLIENT_LOGIN) {

                List<UUID> clientIdList = getTypeManager().getStorage(
                        objectType).readIds(new EntityFilter());

                for (UUID clientId : clientIdList) {

                    if (!receivedIdList.contains(clientId)) {
                        receivedIdList.add(clientId);
                    }
                }

                PullArgs args = new PullArgs(null);
                pull(objectType, receivedIdList, args,
                        SyncEventType.SERVER_PUSH);

            } else if (message.getPullInvokeReason() == PullInvokeReason.AUTOMATIC_PUSH) {

                PullArgs args = new PullArgs(null);
                pull(objectType, receivedIdList, args,
                        SyncEventType.SERVER_PUSH);

            } else if (message.getPullInvokeReason() == PullInvokeReason.MANUAL_PUSH) {

                PullArgs args = new PullArgs(message.getFilter());
                pull(objectType, args, SyncEventType.SERVER_PUSH);
            }

        } catch (SynchronizedStorageException e) {
            getLogHelper().logError(
                            getModuleName(), LogId.SYNC_PULL_E_1,
                            "SynchronizedStorageException when pull invoke message received",
                            e);
        } catch (NotLoggedInException e) {
            getLogHelper().logError(
                            getModuleName(), LogId.SYNC_PULL_E_2,
                            "NotLoggedInException when pull invoke message received",
                            e);
        } catch (MaxMessageSizeException e) {
            getLogHelper().logError(
                            getModuleName(), LogId.SYNC_PULL_E_2,
                            "MaxMessageSizeException when pull invoke message received",
                            e);
        }

        finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void onPullResponseReceived(PullResponse response) {

        UUID sessionId = response.getSessionGuid();
        String objectType = response.getObjectType();
        if (isEnablePerformanceLogs())
        {
            getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_5, "Performance Logs", "Pull response received",
                    ((IDebugPrintable) response).getDebugString());
        }

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            if (!PullResponsesSessions.containsKey(response.getSessionGuid())) {
                PullResponsesSessions.put(response.getSessionGuid(), 0);
            }

            if (!response.isSplit().booleanValue()) {

                List<SyncObject> objectsToCreate = new ArrayList<SyncObject>();
                for (SyncObject syncObject : response.getSynchronizedObjects()) {

                    UUID objectId = syncObject.getGuid();
                    String mostRecentSessionId = getSynchronizationDao()
                            .getMostRecentSessionId(objectId);

                    if (mostRecentSessionId == null) {
                        logNoSessionFound(sessionId, objectId);
                    } else if (mostRecentSessionId.equals(sessionId.toString())
                            || mostRecentSessionId.equals("")) {

                        if (getSynchronizationDao().isExistingInMetadata(
                                objectId)) {
                            try {
                                List<ConflictedPropertyInfo> conflictedProperties = resolveReceivedEntity(objectType,
                                        syncObject, true);
                                notifyOnConflictListeners(objectType, objectId, conflictedProperties);
                                int syncedEntityNumber = updateSessionDictionary(sessionId);
                                notifyOnSyncEventListeners(
                                        objectType,
                                        syncObject.getGuid(),
                                        SyncEventReason.SYNCHRONIZED,
                                        response.getSyncEventType(),
                                        sessionId, "", response.getTotalEntityCount(), syncedEntityNumber);
                            } catch (SynchronizedStorageException e) {
                                notifyOnSyncEventListeners(objectType,
                                        objectId, SyncEventReason.ERROR,
                                        response.getSyncEventType(), sessionId);
                                getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_1,
                                        "SynchronizedStorageException when pull response received, sessionId: "
                                                + sessionId.toString(), e);
                            }
                        } else {
                            objectsToCreate.add(syncObject);
                        }
                    }
                }

                if (objectsToCreate.size() > 0) {
                    try {
                        insertEntityReceivedFromServer(objectType,
                                objectsToCreate);

                        for (SyncObject syncObject : objectsToCreate) {
                            int syncedEntityNumber = updateSessionDictionary(sessionId);
                            notifyOnSyncEventListeners(
                                    objectType,
                                    syncObject.getGuid(),
                                    SyncEventReason.SYNCHRONIZED,
                                    response.getSyncEventType(),
                                    sessionId, "", response.getTotalEntityCount(), syncedEntityNumber);
                        }
                    } catch (SynchronizedStorageException e) {
                        notifyOnSyncEventListeners(objectType,
                                UUID.fromString(EMPTY_GUID),
                                SyncEventReason.ERROR,
                                response.getSyncEventType(), sessionId);
                        getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_1,
                                "SynchronizedStorageException when pull response received, sessionId: "
                                        + sessionId.toString(), e);
                    }
                }

                for (SyncMetaObject syncMetaObject : response.getDeletedObjects()) {
                    UUID objectId = syncMetaObject.getGuid();
                    String mostRecentSessionId = getSynchronizationDao().getMostRecentSessionId(objectId);
                    if (mostRecentSessionId == null) {
                        logNoSessionFound(sessionId, objectId);
                    } else {
                        try {
                            if (mostRecentSessionId
                                    .equals(sessionId.toString())
                                    || mostRecentSessionId.equals("")) {

                                deleteCustomEntity(objectType, objectId);
                                deleteMetadata(objectType, objectId);
                            }
                            int syncedEntityNumber = updateSessionDictionary(sessionId);
                            notifyOnSyncEventListeners(
                                    objectType,
                                    objectId,
                                    SyncEventReason.SYNCHRONIZED,
                                    response.getSyncEventType(),
                                    sessionId, "", response.getTotalEntityCount(), syncedEntityNumber);
                        } catch (SynchronizedStorageException e) {
                            notifyOnSyncEventListeners(objectType, objectId,
                                    SyncEventReason.ERROR,
                                    response.getSyncEventType(), sessionId);
                            getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_1,
                                    "SynchronizedStorageException when pull response received, sessionId: "
                                            + sessionId.toString(), e);
                        }
                    }
                }
            } else {
                int syncedEntityNumber = 0;
                if (response.getCurrentEntityCount() == 1) {
                    syncedEntityNumber = updateSessionDictionary(sessionId);
                }

                SyncObject syncObject = response.getSynchronizedObjects().get(0);

                UUID objectId = syncObject.getGuid();
                String mostRecentSessionId = getSynchronizationDao().getMostRecentSessionId(objectId);

                if (mostRecentSessionId == null) {
                    logNoSessionFound(sessionId, objectId);
                } else if (mostRecentSessionId.equals(sessionId.toString())
                        || mostRecentSessionId.equals("")) {

                    getSynchronizationDao().insertPartialEntity(objectId,
                            sessionId, response.getPacketNumber(),
                            syncObject.getObject());
                    int numberOfPacketsOnDb = getSynchronizationDao()
                            .getPartialEntityCount(objectId, sessionId);

                    if (numberOfPacketsOnDb == response
                            .getTotalNumberOfPackets()) {

                        List<String> partialEntityList = getSynchronizationDao()
                                .getPartialEntities(objectId, sessionId);
                        syncObject
                                .setObject(combinePartialItems(partialEntityList));

                        try {
                            if (getSynchronizationDao().isExistingInMetadata(
                                    objectId)) {

                                List<ConflictedPropertyInfo> conflictDetailList = resolveReceivedEntity(
                                        objectType, syncObject, true);
                                this.notifyOnConflictListeners(objectType,
                                        objectId, conflictDetailList);
                            }

                            else {
                                List<SyncObject> syncObjects = new ArrayList<SyncObject>();
                                syncObjects.add(syncObject);
                                insertEntityReceivedFromServer(objectType,
                                        syncObjects);
                            }
                            notifyOnSyncEventListeners(
                                    objectType,
                                    objectId,
                                    SyncEventReason.SYNCHRONIZED,
                                    response.getSyncEventType(),
                                    sessionId, "", response.getTotalEntityCount(), syncedEntityNumber);
                        } catch (SynchronizedStorageException e) {
                            notifyOnSyncEventListeners(objectType, objectId,
                                    SyncEventReason.ERROR,
                                    response.getSyncEventType(), sessionId);
                            getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_1,
                                    "SynchronizedStorageException when pull response received, sessionId: "
                                            + sessionId.toString(), e);
                        } finally {
                            getSynchronizationDao().removePartialEntities(
                                    objectId, sessionId);
                        }
                    }
                }
            }

            // Get the received entity count and check if all received, if so
            // fires the event.
            if (PullResponsesSessions.get(response.getSessionGuid()).equals(
                    response.getTotalEntityCount())) {
                int receivedNoOfEntities = PullResponsesSessions.remove(sessionId);
                // Fires the event.
                notifyOnSyncEventListeners(
                        objectType,
                        UUID.fromString(EMPTY_GUID),
                        SyncEventReason.Completed,
                        response.getSyncEventType(),
                        sessionId, "", response.getTotalEntityCount(), receivedNoOfEntities);

                if (isEnablePerformanceLogs())
                {
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_PULL_D_6, "Performance Logs",
                            "Pull response completed", "Object Type: "
                                    + objectType + ", Sync Session Id: " + sessionId);
                }
            }

        } catch (Exception e) {
            getLogHelper().logError(getModuleName(), LogId.SYNC_PULL_E_2,
                    "Exception when pull response is received, sessionId: "
                            + sessionId.toString(), e);
        }

        finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private int updateSessionDictionary(UUID sessionId)
    {
        int receivedNoOfEntities = PullResponsesSessions.remove(sessionId);
        receivedNoOfEntities = receivedNoOfEntities + 1;
        PullResponsesSessions.put(sessionId, receivedNoOfEntities);
        return receivedNoOfEntities;
    }

    private void logNoSessionFound(UUID sessionId, UUID objectId) {
        getLogHelper().logDebug(
                        getModuleName(), LogId.SYNC_PULL_D_9,
                        "No sync session id found",
                        "No sync session id found for entity "
                                + objectId
                                + " when processing pull response with session "
                                + sessionId
                                + ". This can be expected if two consecutive pull requests are sent very quickly");
    }

    private void insertEntityReceivedFromServer(String objectType,
            List<SyncObject> syncObjects) throws SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            insertEntity(objectType, syncObjects);
            insertBulkMetadata(objectType, syncObjects, SyncStatus.SYNCHRONIZED);

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    ArrayList<SyncMetaObject> createSyncMetaObjectsForIds(List<UUID> idList,
            String objectType, List<UUID> listOfEntityIdsToPull) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            ArrayList<SyncMetaObject> objects = new ArrayList<SyncMetaObject>();
            if (idList != null) {
                for (UUID id : idList) {
                    // TODO: A new query will be executed for each object Id.
                    // This
                    // could be a performance issue.
                    SyncMetaObject metaDataObject = getSynchronizationDao()
                            .getMetadata(objectType, id);
                    objects.add(metaDataObject);
                    listOfEntityIdsToPull.add(id);
                }
            }
            return objects;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }
}
