package se.precom.synchronization;

import java.util.UUID;

import se.precom.synchronization.SynchronizationClientBase.OnSyncEventListener;

/**
 * Used to send information about the synchronization process to {@link OnSyncEventListener}s
 * 
 */
public class SyncInfo {

    private SyncEventReason _reason;
    private SyncEventType _type;
    private UUID _sessionId;
    private String _information;
    private int _totalEntityCount;
    private int _synchronizedEntityCount;

    SyncInfo(SyncEventReason reason, SyncEventType syncEventType, UUID sessionId, String information,
            int totalEntityCount, int synchronizedEntityCount) {
        _reason = reason;
        _type = syncEventType;
        _sessionId = sessionId;
        _information = information;
        _totalEntityCount = totalEntityCount;
        _synchronizedEntityCount = synchronizedEntityCount;
    }

    /**
     * Returns the sync event reason.
     * 
     * @return the sync event reason.
     */
    public SyncEventReason getReason() {
        return _reason;
    }

    /**
     * Returns the type of the synchronization process.
     * 
     * @return the type of the synchronization process.
     */
    public SyncEventType getSyncEventType() {
        return _type;
    }

    /**
     * Returns the session id of the synchronization process.
     * 
     * @return the session id of the synchronization process.
     */
    public UUID getSessionId() {
        return _sessionId;
    }

    /**
     * Returns additional information.
     * 
     * @return the additional information.
     */
    public String getInformation() {
        return _information;
    }

    /**
     * Returns the total entity count to be received.
     * 
     * @return Total entity count.
     */
    public int getTotalEntityCount()
    {
        return _totalEntityCount;
    }

    /**
     * Returns the synchronized entity count.
     * 
     * @return Synchronized entity count.
     */
    public int getSynchronizedEntityCount()
    {
        return _synchronizedEntityCount;
    }
}
