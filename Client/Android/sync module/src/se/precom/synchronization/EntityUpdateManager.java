package se.precom.synchronization;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.log.LogBase;
import se.precom.core.storage.StorageBase;
import se.precom.synchronization.SynchronizedStorageBase.OnEntityChangedListener;
import android.util.Log;

class EntityUpdateManager extends SynchronizationManager {

    private final HashMap<String, OnEntityChangedListener> _entityChangedListeners = new HashMap<String, OnEntityChangedListener>();

    EntityUpdateManager(TypeManager typeManager, JsonSerializer jsonSerializer,
            SynchronizationModule syncModule, int maximumSizeForTransferObject,
            CommunicationBase communication, StorageBase storage,
            LogBase log) {
        super(typeManager, jsonSerializer, syncModule,
                maximumSizeForTransferObject, communication,
                storage, log);

    }

    void dispose() {

        for (Entry<String, OnEntityChangedListener> entry : _entityChangedListeners.entrySet()) {
            SynchronizedStorageBase<ISynchronizable> storage = getTypeManager().getStorage(entry.getKey());
            storage.removeOnEntityChangedListener(entry.getValue());
        }
        _entityChangedListeners.clear();
    }

    <T extends ISynchronizable> void addOnEntityChangedListenerToStorage(Class<T> objectType,
            SynchronizedStorageBase<T> storage) {

        OnEntityChangedListener entityChangedListener = new OnEntityChangedListener() {

            @Override
            public void onEntityChanged(ChangeEventArgs eventArgs) {
                EntityUpdateManager.this.onEntityChanged(eventArgs);
            }
        };

        storage.addInternalOnEntityChangedListener(entityChangedListener);
        _entityChangedListeners.put(objectType.getName(), entityChangedListener);
    }

    void deleteLocally(String objectType, List<UUID> entityIds) throws SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        for (UUID entityId : entityIds) {
            try
            {
                getTypeManager().getStorage(objectType).delete(entityId,
                        new DeleteArgs(SynchronizationModule.class.getSimpleName()));

            } catch (Exception e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_3,
                        "Entity " + entityId.toString() + " delete failed", e);
            }

            try
            {
                deleteMetadata(objectType, entityId);
            } catch (Exception e)
            {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_3,
                        "Entity " + entityId.toString() + " metadata deletion failed", e);
            }
        }
        if (lock != null) {
            lock.unlock();
        }
    }

    private void onEntityChanged(ChangeEventArgs eventArgs) {

        if (!eventArgs.getOriginatorType().equals(SynchronizationModule.class.getSimpleName())) {

            String objectTypeName = eventArgs.getEntityType();
            updateSyncMetadata(eventArgs, objectTypeName);
            initiateAutomaticSync(eventArgs, objectTypeName);
        }
    }

    private void updateSyncMetadata(ChangeEventArgs eventArgs, String objectTypeName) {
        switch (eventArgs.getEntityChange()) {
            case CREATED:

                // In this scenario we are adding a new record at the client end. So the server version is
                // set to 0.
                // The server version will be updated for this entity once the new entity has been pushed to
                // the server,.
                insertMetadata(objectTypeName, eventArgs.getId(), 0, SyncStatus.CREATED);
                break;

            case UPDATED:
                List<String> changedProperties = findChangedProperties(eventArgs.getEntityType(),
                        eventArgs.getNewEntity(), eventArgs.getOldEntity());
                if (changedProperties.size() > 0) {
                    getSynchronizationDao().updateMetadataRecord(objectTypeName,
                            eventArgs.getId(),
                            SyncStatus.UPDATED, UUID.randomUUID());
                    if (isEnablePropertySynchronization()) {
                        getSynchronizationDao().markPropertiesAsChanged(eventArgs.getId(),
                                changedProperties);
                    }
                }
                break;

            case DELETED:
                UUID objectId = eventArgs.getId();
                if (getSynchronizationDao().getMetadata(objectTypeName, objectId).getVersion() == 0
                        && getSynchronizationDao().getMostRecentSessionId(objectId) == null) {
                    deleteMetadata(objectTypeName, objectId);
                } else {
                    getSynchronizationDao().updateMetadataRecord(objectTypeName, objectId,
                            SyncStatus.DELETED, UUID.randomUUID());
                }
                break;

            default:
                Log.d("Sync", "Object " + eventArgs.getId().toString() + " " + objectTypeName + ": "
                        + "entity change not specified");
                break;
        }
    }

    private void initiateAutomaticSync(ChangeEventArgs eventArgs, String objectTypeName) {
        if (getTypeManager().isRegisteredForAutomaticPush(objectTypeName)
                && getTypeManager().getAutomaticPushOperation(objectTypeName).contains(eventArgs.getEntityChange())) {

            PushArgs pushArgs = new PushArgs();
            pushArgs.getEntityIdList().add(eventArgs.getId());

            try {
                getSyncModule().push(getTypeManager().getType(objectTypeName), pushArgs);
            } catch (NotLoggedInException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_2,
                        "onEntityChanged : NotLoggedInException when sending push request", e);
            } catch (MaxMessageSizeException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_2,
                        "onEntityChanged : MaxMessageSizeException when sending push request", e);
            } catch (SynchronizedStorageException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_1,
                        "onEntityChanged : SynchronizedStorageException when sending push request", e);
            }
        }
    }

    private List<String> findChangedProperties(String objectType, ISynchronizable newObject,
            ISynchronizable oldObject) {
        List<String> changedProperties = new ArrayList<String>();
        Class type = getTypeManager().getType(objectType);
        for (String propertyName : getTypeManager().getProperties(objectType)) {

            try {
                Field field = getTypeManager().getField(type, propertyName);
                field.setAccessible(true);
                Object newValue = field.get(newObject);
                Object oldValue = field.get(oldObject);
                if (!equals(newValue, oldValue)) {
                    changedProperties.add(propertyName);
                }
            } catch (IllegalArgumentException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_2,
                        "IllegalArgumentException when finding changed properties", e);
            } catch (IllegalAccessException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_EU_E_2,
                        "IllegalAccessException when finding changed properties", e);
            }
        }
        return changedProperties;
    }
}
