package se.precom.synchronization;

import java.util.ArrayList;
import java.util.UUID;

import com.google.gson.reflect.TypeToken;

/**
 * Entity sent by the server synchronization module to initiate the server push process.
 */
public class PullInvokeMessage extends SynchronizationPacket {

    private EntityFilter Filter;
    private String Knowledge;
    private int InvokeReason;

    /**
     * Returns the entity filter.
     * 
     * @return {@link EntityFilter}.
     */
    public EntityFilter getFilter() {
        return Filter;
    }

    /**
     * Sets the entity filter.
     * 
     * @param filter
     *            {@link EntityFilter}.
     */
    public void setFilter(EntityFilter filter) {
        Filter = filter;
    }

    /**
     * Returns the Knowledge
     * 
     * @return Knowledge
     */
    public String getKnowledge() {
        return Knowledge;
    }

    /**
     * Sets the Knowledge.
     * 
     * @param knowledge
     * 
     */
    public void setKnowledge(String knowledge) {
        Knowledge = knowledge;
    }

    /**
     * Sets the reason for the server push.
     * 
     * @param invokeReason
     *            {@link PullInvokeReason}.
     */
    public void setPullInvokeReason(PullInvokeReason invokeReason) {
        InvokeReason = invokeReason.getValue();
    }

    /**
     * Returns the reason for the server push.
     * 
     * @return PullInvokeReason.
     */
    public PullInvokeReason getPullInvokeReason() {
        return PullInvokeReason.fromInt(InvokeReason);
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString() {

        ArrayList<UUID> _knowledge = null;

        if (super.isSplit() != true && Knowledge != null && !Knowledge.isEmpty()) {
            if (getPullInvokeReason() == PullInvokeReason.CLIENT_LOGIN) {

                _knowledge = JsonSerializer.deserialize(Knowledge, new TypeToken<ArrayList<UUID>>() {
                }.getType());

            } else if (getPullInvokeReason() == PullInvokeReason.AUTOMATIC_PUSH) {

                _knowledge = new ArrayList<UUID>();
                _knowledge.add(UUID.fromString(Knowledge));
            }
        }

        return super.getDebugString()
                + String.format(", Knowledge(Count): %s, Filter(Count): %s, ObjectType: %s",
                        _knowledge != null ? _knowledge.size() : "N/A",
                        getFilter() != null && getFilter().getFilterList() != null ? getFilter().getFilterList().size()
                                : "0", getObjectType());
    }
}
