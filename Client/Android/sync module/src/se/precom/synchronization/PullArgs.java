package se.precom.synchronization;

/**
 * Argument used when starting a pull via {@link SynchronizationClientBase#pull(Class, PullArgs)}.
 * 
 */
public class PullArgs {

    private EntityFilter _filter;

    /**
     * Initializes a new instance of the {@link PullArgs}.
     * 
     * @param filter
     *            the filter.
     */
    public PullArgs(EntityFilter filter)
    {
        _filter = filter;
    }

    /**
     * Returns the filter.
     * 
     * @return the filter.
     */
    public EntityFilter getFilter() {
        return _filter;
    }

    /**
     * Sets the filter.
     * 
     * @param filter
     *            the filter.
     */
    public void setFilter(EntityFilter filter) {
        _filter = filter;
    }
}
