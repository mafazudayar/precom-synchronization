package se.precom.synchronization;

enum SyncStatus {
    SYNCHRONIZED(0),
    CREATED(1),
    UPDATED(2),
    DELETED(4);
    private int _value;

    private SyncStatus(int value) {
        _value = value;
    }

    int getValue() {
        return _value;
    }
}
