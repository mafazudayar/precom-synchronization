package se.precom.synchronization;

/**
 * Contains the information related to the updated entity.
 */
public class UpdateArgs {

    private String _originatorType;
    private int _credentialId;

    /**
     * Initializes a new instance of the {@link UpdateArgs}.
     * 
     * @param originatorType
     *            the originator type.
     */
    public UpdateArgs(String originatorType)
    {
        _originatorType = originatorType;
    }

    /**
     * Initializes a new instance of the {@link UpdateArgs}.
     * 
     * @param originatorType
     *            the originator type.
     * @param credentialId
     *            the credential id.
     */
    public UpdateArgs(String originatorType, int credentialId)
    {
        _originatorType = originatorType;
        _credentialId = credentialId;
    }

    /**
     * Returns the originator type.
     * 
     * @return the originator type.
     */
    public String getOriginatorType() {
        return _originatorType;
    }

    /**
     * Sets the originator type.
     * 
     * @param originatorType
     *            The originator type.
     */
    public void setOriginatorType(String originatorType) {
        _originatorType = originatorType;
    }

    /**
     * Returns the credential id.
     * 
     * @return the credential id.
     */
    public int getCredentialId() {
        return _credentialId;
    }

    /**
     * Sets the credential id.
     * 
     * @param credentialId
     *            The credential id.
     */
    public void setCredentialId(int credentialId) {
        _credentialId = credentialId;
    }
}
