package se.precom.synchronization;

/**
 * Contains information about a conflicted property on the client side.
 * 
 */
public class ConflictedPropertyInfo {

    private String _property;
    private Object _oldValue;
    private Object _newValue;

    /**
     * Initializes a new instance of the {@link ConflictedPropertyInfo}.
     * 
     * @param propertyName
     *            name of the property.
     * @param newValue
     *            new value of the property.
     * @param oldValue
     *            old value of the property.
     */
    ConflictedPropertyInfo(String propertyName, Object newValue, Object oldValue) {
        _property = propertyName;
        _newValue = newValue;
        _oldValue = oldValue;
    }

    /**
     * Returns the name of the property.
     * 
     * @return name of the property.
     */
    public String getPropertyName() {
        return _property;
    }

    /**
     * Returns the old value of the property before being overwritten by the new value.
     * 
     * @return old value of the property.
     */
    public Object getOldValue() {
        return _oldValue;
    }

    /**
     * Returns the new value of the property which was sent by the server.
     * 
     * @return new value of the property.
     */
    public Object getNewValue() {
        return _newValue;
    }

}
