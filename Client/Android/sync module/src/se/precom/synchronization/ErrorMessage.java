package se.precom.synchronization;

import java.util.UUID;

import se.precom.core.communication.EntityBase;

/**
 * Entity containing error message sent from server.
 * 
 */
public class ErrorMessage extends EntityBase {

    private String ObjectType;
    private UUID EntityId;
    private int Type;
    private UUID SessionId;
    private String Information;

    /**
     * Returns the type of the object which error occurred.
     * 
     * @return object type.
     */
    public String getObjectType() {
        return ObjectType;
    }

    /**
     * Returns the id of the entity.
     * 
     * @return entity id.
     */
    public UUID getEntityId() {
        return EntityId;
    }

    /**
     * Returns the {@link SyncEventType}.
     * 
     * @return sync event type.
     */
    public SyncEventType getType() {
        return SyncEventType.fromInt(Type);
    }

    /**
     * Returns the session id.
     * 
     * @return session id.
     */
    public UUID getSessionId() {
        return SessionId;
    }

    /**
     * Returns the information related to the error.
     * 
     * @return information.
     */
    public String getInformation() {
        return Information;
    }

}
