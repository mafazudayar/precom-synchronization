package se.precom.synchronization;

import java.util.ArrayList;

/**
 * The response sent by the synchronization server for the pull request.
 * 
 */
public class PullResponse extends SynchronizationPacket {

    private final ArrayList<SyncObject> SynchronizedObjects = new ArrayList<SyncObject>();
    private final ArrayList<SyncMetaObject> DeletedObjects = new ArrayList<SyncMetaObject>();
    private int SyncType;
    private int TotalEntityCount;
    private int CurrentEntityCount;

    /**
     * Returns a list of {@link SyncObject}.
     * 
     * @return list of {@link SyncObject}.
     */
    public ArrayList<SyncObject> getSynchronizedObjects() {
        return SynchronizedObjects;
    }

    /**
     * Returns the type of the synchronization process.
     * 
     * @return {@link SyncEventType}.
     */
    public SyncEventType getSyncEventType() {
        return SyncEventType.fromInt(SyncType);
    }

    /**
     * Sets the type of the synchronization process.
     * 
     * @param syncEventType
     *            {@link SyncEventType}.
     */
    public void setSyncEventType(SyncEventType syncEventType) {
        SyncType = syncEventType.getValue();
    }

    /**
     * Returns a list of {@link SyncMetaObject} for entities deleted on server.
     * 
     * @return list of {@link SyncMetaObject}.
     */
    public ArrayList<SyncMetaObject> getDeletedObjects() {
        return DeletedObjects;
    }

    /**
     * Returns the total number of entities the client should receive against the given pull request
     * 
     * @return TotalEntityCount.
     */
    public int getTotalEntityCount()
    {
        return TotalEntityCount;
    }

    /**
     * Returns the number of entities contains in a given pull response
     * 
     * @return CurrentEntityCount.
     */
    public int getCurrentEntityCount()
    {
        return CurrentEntityCount;
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString()
    {
        return super.getDebugString()
                +
                String.format(
                        ", SynchronizedObjects(Count): %s, DeletedObjects(Count): %s, TotalEntityCount: %d, CurrentEntityCount: %d, SyncType: %s",
                        getSynchronizedObjects() != null ? getSynchronizedObjects().size() : "0",
                        getDeletedObjects() != null ? getDeletedObjects().size() : "0", getTotalEntityCount(),
                        getCurrentEntityCount(),
                        getSyncEventType());
    }
}
