package se.precom.synchronization;

import java.util.UUID;

/**
 * Contains information about the synchronization process started by calling
 * {@link SynchronizationClientBase#pull(Class, PullArgs)}
 * 
 */
public class PullResult {
    private UUID _syncSession;

    /**
     * Returns the synchronization session Id.
     * 
     * @return the synchronization session Id.
     */
    public UUID getSyncSession() {
        return _syncSession;
    }

    /**
     * Sets the synchronization session Id.
     * 
     * @param syncSession
     *            the synchronization session Id.
     */
    void setSyncSession(UUID syncSession) {
        this._syncSession = syncSession;
    }

}
