package se.precom.synchronization;

import java.util.ArrayList;

/**
 * Entity sent by the client synchronization module to initiate a client push process.
 */
public class PushRequest extends SynchronizationPacket {

    private ArrayList<SyncObject> SynchronizedObjects = new ArrayList<SyncObject>();

    /**
     * Returns a list of {@link SyncObject} containing entities which are modified at the client end.
     * 
     * @return list of {@link SyncObject}.
     */
    public ArrayList<SyncObject> getSynchronizedObjects() {
        return SynchronizedObjects;
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString()
    {
        return super.getDebugString() +
                String.format(", SynchronizedObjects(Count): %s",
                        getSynchronizedObjects() != null ? getSynchronizedObjects().size() : "0");
    }
}
