package se.precom.synchronization;

import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

import se.precom.core.ModuleBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;

/**
 * Base class which use by the synchronization module.
 * 
 */
public abstract class SynchronizationClientBase extends ModuleBase {

    /**
     * Registers an {@link SynchronizedStorageBase} instance for a certain object type.
     * 
     * @param objectType
     *            object type
     * @param storage
     *            {@link SynchronizedStorageBase} instance for the object type.
     */
    public abstract <T extends ISynchronizable> void registerStorage(Class<T> objectType,
    		SynchronizedStorageBase<T> storage);
    
    /**
     * Unregisters an {@link SynchronizedStorageBase} instance for the given object type.
     * 
     * @param objectType
     *            object type
     * @param storage
     *            {@link SynchronizedStorageBase} instance for the object type.
     */
    public abstract <T extends ISynchronizable> void unregisterStorage(Class<T> objectType,
            SynchronizedStorageBase<T> storage);

    /**
     * Adds a SyncEventListener for the specified object type.
     * 
     * @param objectType
     *            object type
     * @param onSyncEventListener
     *            {@link OnSyncEventListener}
     */
    public abstract <T extends ISynchronizable> void addOnSyncEventListener(Class<T> objectType,
            OnSyncEventListener onSyncEventListener);

    /**
     * Removes the specified SyncEventListener for the given object type.
     * 
     * @param objectType
     *            object type
     * @param onSyncEventListener
     *            {@link OnSyncEventListener}
     */
    public abstract <T extends ISynchronizable> void removeOnSyncEventListener(Class<T> objectType,
            OnSyncEventListener onSyncEventListener);

    /**
     * Register for automatic synchronization of the given object type, which need to be automatically pushed to the
     * server.
     * 
     * @param objectType
     *            object type.
     * @param entityChange
     *            {@link EnumSet} containing {@link Operation} values.
     */
    public abstract <T extends ISynchronizable> void registerAutomaticPush(Class<T> objectType,
            EnumSet<Operation> entityChange);
    
    /**
     * Unregister for automatic synchronization of the given object type, which need to be automatically pushed to the
     * server.
     * 
     * @param objectType
     *            object type.
     * @param entityChange
     *            {@link EnumSet} containing {@link Operation} values.
     */
    public abstract <T extends ISynchronizable> void unregisterAutomaticPush(Class<T> objectType,
            EnumSet<Operation> entityChange);

    /**
     * Sets a callback to be triggered when a conflict has occurred in an entity of the specified object type.
     * 
     * @param objectType
     *            object type.
     * @param listener
     *            {@link OnConflictListener}
     * 
     */
    public abstract <T extends ISynchronizable> void setOnConflictListener(Class<T> objectType,
            OnConflictListener onConflictListener);
    
    /**
     * Removes the specified conflictListener for the given object type.
     * 
     * @param objectType
     *            object type.
     * @param listener
     *            {@link OnConflictListener}
     * 
     */
    public abstract <T extends ISynchronizable> void removeOnConflictListener(Class<T> objectType);

    /**
     * Starts a new client initiated push request.
     * 
     * @param objectType
     *            The object type to be pushed to the server.
     * 
     * @return a {@link PushResult} object containing information about the sync session.
     * @throws MaxMessageSizeException
     * @throws NotLoggedInException
     * @throws SynchronizedStorageException
     */
    public abstract <T extends ISynchronizable> PushResult push(Class<T> objectType)
            throws NotLoggedInException, MaxMessageSizeException, SynchronizedStorageException;

    /**
     * Starts a new client initiated push request.
     * 
     * @param objectType
     *            The object type to be pushed to the server.
     * @param PushArgs
     *            Information related to the push request.
     * @return a {@link PushResult} object containing information about the sync session.
     * @throws NotLoggedInException
     * @throws MaxMessageSizeException
     * @throws SynchronizedStorageException
     */
    public abstract <T extends ISynchronizable> PushResult push(Class<T> objectType,
            PushArgs pushArgs) throws NotLoggedInException,
            MaxMessageSizeException, SynchronizedStorageException;

    /**
     * Starts a new client initiated pull.
     * 
     * @param objectType
     *            The object type to be pulled from server.
     * @param pullArgs
     *            Information related to the pull request.
     * @return A {@link PullResult} object containing information about the sync session.
     * @throws NotLoggedInException
     * @throws MaxMessageSizeException
     * @throws SynchronizedStorageException
     */
    public abstract <T extends ISynchronizable> PullResult pull(Class<T> objectType, PullArgs pullArgs)
            throws NotLoggedInException, MaxMessageSizeException, SynchronizedStorageException;

    /**
     * Removes entities from local client store without deleting the entities from the server.
     * 
     * @param objectType
     *            The object type to be removed from client.
     * @param entityIds
     *            List of {@link UUID} of the objects which are to be removed.
     * @throws SynchronizedStorageException
     */
    public abstract <T extends ISynchronizable> void deleteLocally(Class<T> objectType, List<UUID> entityIds)
            throws SynchronizedStorageException;

    /**
     * 
     * Interface used to notify sync events for an object type, to modules registered by calling
     * {@link SynchronizationClientBase#registerStorage(Class, OnSyncEventListener) }
     * 
     */
    public interface OnSyncEventListener {
        /**
         * Callback which is triggered when a synchronization process has been started,completed,aborted or error
         * occurred.
         * 
         * @param entityId
         *            Id of the entity.
         * @param syncInfo
         *            {@link SyncInfo}
         */
        void onSyncEvent(UUID entityId, SyncInfo syncInfo);
    }

    /**
     * Interface used to notify conflicts for an object type, to the module registered by calling
     * {@link SynchronizationClientBase#setOnConflictListener(Class, OnConflictListener)}
     * 
     */
    public interface OnConflictListener {
        /**
         * Callback which is triggered when a conflict has occurred.
         * 
         * @param conflictInfo
         *            {@link ConflictNotificationInfo}
         */
        void onConflict(ConflictNotificationInfo conflictInfo);
    }
}
