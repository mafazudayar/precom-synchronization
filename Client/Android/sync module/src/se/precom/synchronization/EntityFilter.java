package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of {@link Filter} objects that is used to create a filter query.
 * 
 */
public class EntityFilter {

    private List<Filter> FilterList;

    /**
     * Initializes a new instance of the {@link EntityFilter}.
     */
    public EntityFilter() {
        FilterList = new ArrayList<Filter>();
    }

    /**
     * Returns the list of {@link Filter} objects.
     * 
     * @return the list of {@link Filter} objects.
     */
    public List<Filter> getFilterList() {
        return FilterList;
    }

}
