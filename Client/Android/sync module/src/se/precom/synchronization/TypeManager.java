package se.precom.synchronization;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import se.precom.synchronization.SynchronizationClientBase.OnConflictListener;
import se.precom.synchronization.SynchronizationClientBase.OnSyncEventListener;

class TypeManager {

    private Map<String, TypeInformation> _registeredEntityTypeInformation;

    class TypeInformation {

        private Class<? extends ISynchronizable> _entityType;
        private List<OnSyncEventListener> _syncEventListeners;
        private SynchronizedStorageBase<ISynchronizable> _storage;
        private OnConflictListener _conflictListener;
        private EnumSet<Operation> _automaticSync;
        private Lock _lock;

        TypeInformation() {
            _syncEventListeners = new ArrayList<OnSyncEventListener>();
        }

        void setEntityType(Class<? extends ISynchronizable> entity) {
            _entityType = entity;
        }

        Class<? extends ISynchronizable> getEntityType() {
            return _entityType;
        }

        List<OnSyncEventListener> getOnSyncEventListeners() {
            return _syncEventListeners;
        }

        void setStorage(SynchronizedStorageBase<ISynchronizable> storage) {
            _storage = storage;
        }

        SynchronizedStorageBase<ISynchronizable> getStorage() {
            return _storage;
        }

        void setOnConflictListener(OnConflictListener conflictListener) {
            _conflictListener = conflictListener;
        }

        OnConflictListener getOnConflictListener() {
            return _conflictListener;
        }

        void setAutoPushOperation(EnumSet<Operation> automaticSync) {
            _automaticSync = automaticSync;
        }

        EnumSet<Operation> getAutoPushOperation() {
            return _automaticSync;
        }

		public Lock getLock() {
            return _lock;
        }

		public void setLock(Lock _lock) {
            this._lock = _lock;
        }
    }

    TypeManager() {
        _registeredEntityTypeInformation = new HashMap<String, TypeInformation>();
    }

    private TypeInformation getRegisteredTypeInformation(String entityType, boolean checkEntityType) {
        synchronized (_registeredEntityTypeInformation) {
            TypeInformation typeInformation = _registeredEntityTypeInformation.get(entityType);

            if (typeInformation == null) {
                if (checkEntityType) {
                    throw new IllegalArgumentException("Please register the entity : " + entityType);
                }
                typeInformation = new TypeInformation();
                _registeredEntityTypeInformation.put(entityType, typeInformation);
            }
            return typeInformation;
        }
    }

    <T extends ISynchronizable> boolean typeExists(Class<T> entityType) {
        String entitySimpleName = entityType.getSimpleName();

        TypeInformation typeInformation = getRegisteredTypeInformation(entitySimpleName, false);

        return typeInformation.getEntityType() != null;
    }

    <T extends ISynchronizable> Class<T> getType(String objectType) {
        return (Class<T>) getRegisteredTypeInformation(objectType, true).getEntityType();
    }

    void addSyncEventListeners(String entityType, OnSyncEventListener onSyncEventListener) {

        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        List<OnSyncEventListener> list = typeInformation.getOnSyncEventListeners();

        if (!list.contains(onSyncEventListener)) {
            typeInformation.getOnSyncEventListeners().add(onSyncEventListener);
        }
    }

    void removeSyncEventListeners(String entityType, OnSyncEventListener onSyncEventListener) {

        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        List<OnSyncEventListener> list = typeInformation.getOnSyncEventListeners();

        if (list.contains(onSyncEventListener)) {
            typeInformation.getOnSyncEventListeners().remove(onSyncEventListener);
        }
    }

    List<OnSyncEventListener> getSyncEventListeners(String entityType) {
        return getRegisteredTypeInformation(entityType, true).getOnSyncEventListeners();
    }

    <T extends ISynchronizable> void addStorage(Class<T> entityType, SynchronizedStorageBase<T> storage) {

        TypeInformation typeInformation = getRegisteredTypeInformation(entityType.getSimpleName(), false);

        if (typeInformation.getEntityType() == null) {
            typeInformation.setEntityType(entityType);
        }
        if (typeInformation.getStorage() == null) {
            typeInformation.setStorage((SynchronizedStorageBase<ISynchronizable>) storage);
        }
    }
    
    <T extends ISynchronizable> void removeStorage(Class<T> entityType) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType.getSimpleName(), false);               
        typeInformation.setStorage(null);        
    }

    SynchronizedStorageBase<ISynchronizable> getStorage(String entityType) {
        return getRegisteredTypeInformation(entityType, true).getStorage();
    }

    void addOnConflictListener(String entityType, OnConflictListener onConflictListener) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        if (typeInformation.getOnConflictListener() == null) {
            typeInformation.setOnConflictListener(onConflictListener);
        }
    }
    
    void removeOnConflictListeners(String entityType) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);        
            typeInformation.setOnConflictListener(null);
       
    }

    OnConflictListener getOnConflictListener(String entityType) {
        return getRegisteredTypeInformation(entityType, true).getOnConflictListener();
    }

    void addAutomaticPushOperation(String entityType, EnumSet<Operation> entityChange) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        if (typeInformation.getAutoPushOperation() == null) {
            typeInformation.setAutoPushOperation(entityChange);
        } else if (!typeInformation.getAutoPushOperation().contains(entityChange)) {
            typeInformation.getAutoPushOperation().addAll(entityChange);
        }
    }
    
    void removeAutomaticPushOperation(String entityType, EnumSet<Operation> entityChange) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);        
        if (typeInformation.getAutoPushOperation().containsAll(entityChange)) {
           typeInformation.getAutoPushOperation().removeAll(entityChange);           
        }
    }

    EnumSet<Operation> getAutomaticPushOperation(String entityType) {
        return getRegisteredTypeInformation(entityType, true).getAutoPushOperation();
    }

    boolean isRegisteredForAutomaticPush(String entityType) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        if (typeInformation != null && typeInformation.getAutoPushOperation() != null) {
            return true;
        }
        return false;
    }

    void setLock(String entityType, Lock lock) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        typeInformation.setLock(lock);
    }

    Lock getLock(String entityType) {
        TypeInformation typeInformation = getRegisteredTypeInformation(entityType, true);
        return typeInformation.getLock();
    }

    Field getField(Class objectType, String fieldName) {
        Field field = null;
        Class classToExamine = objectType;
        while (true) {
            for (Field aField : classToExamine.getDeclaredFields()) {
                if (aField.getName().equals(fieldName)) {
                    field = aField;
                    break;
                }
            }
            if (field != null) {
                break;
            }
            classToExamine = classToExamine.getSuperclass();
            if (classToExamine.getName().equals(Object.class.getName())) {
                break;
            }
        }
        return field;
    }

    List<String> getProperties(String objectType) {

        boolean syncAllProperties = true;

        List<String> allProperties = new ArrayList<String>();
        List<String> annotationProperties = new ArrayList<String>();

        Class classToExamine = this.getType(objectType);
        while (true) {
            for (Field field : classToExamine.getDeclaredFields()) {
                if (field.isAnnotationPresent(Synchronizable.class)) {
                    annotationProperties.add(field.getName());
                    syncAllProperties = false;
                }
                allProperties.add(field.getName());
            }
            classToExamine = classToExamine.getSuperclass();
            if (classToExamine.getName().equals(Object.class.getName())) {
                break;
            }
        }

        if (syncAllProperties) {
            return allProperties;
        }
        return annotationProperties;
    }
}
