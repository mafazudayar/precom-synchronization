package se.precom.synchronization;

/**
 * Represents the status of the synchronization process.
 * 
 */
public enum SyncEventReason {
    /**
     * Synchronization process is started.
     */
    STARTED(0),
    /**
     * Entity is synchronized.
     */
    SYNCHRONIZED(1),
    /**
     * Synchronization process is aborted.
     */
    ABORTED(2),
    /**
     * Error occurred in the synchronization process.
     */
    ERROR(3),
    /**
     * An error occurred in server in synchronization process.
     */
    SERVER_ERROR(4),
    /**
     * Synchronization process is Completed.
     */
    Completed(5);

    private final int _value;

    SyncEventReason(int value) {
        this._value = value;
    }

    int getValue() {
        return _value;
    }
}
