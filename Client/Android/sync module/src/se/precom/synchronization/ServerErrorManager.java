package se.precom.synchronization;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.CommunicationBase.OnReceiveListener;
import se.precom.core.communication.TypeReference;
import se.precom.core.log.LogBase;
import se.precom.core.storage.StorageBase;

public class ServerErrorManager extends SynchronizationManager {

    private OnReceiveListener<ErrorMessage> _errorMessageReceiveListener;

    public ServerErrorManager(TypeManager typeManager,
            JsonSerializer jsonSerializer, SynchronizationModule syncModule,
            int maximumSizeForTransferObject, CommunicationBase communication,
            StorageBase storage, LogBase log) {

        super(typeManager, jsonSerializer, syncModule,
                maximumSizeForTransferObject, communication, storage, log);

        _errorMessageReceiveListener = new OnReceiveListener<ErrorMessage>() {
            @Override
            public void receive(ErrorMessage entity) {
                try {
                    ServerErrorManager.this.onErrorMessageReceived(entity);
                } catch (IllegalArgumentException e) {
                    // For ELT-1030, don't log this as error right now. They register objects in server that are not
                    // registered in client
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_D_2,
                            "IllegalArgumentException when processing received error message",
                            e.getMessage());
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_D_2,
                            "Exception when processing received error message", e);
                }
            }
        };

        getCommunication().addOnReceiveListener(new TypeReference<ErrorMessage>() {
        }, _errorMessageReceiveListener);
    }

    void dispose() {

        getCommunication().removeOnReceiveListener(new TypeReference<ErrorMessage>() {
        }, _errorMessageReceiveListener);
    }

    private void onErrorMessageReceived(ErrorMessage errorMessage) {

        notifyOnSyncEventListeners(errorMessage.getObjectType(), errorMessage.getEntityId(),
                SyncEventReason.SERVER_ERROR, errorMessage.getType(), errorMessage.getSessionId(),
                errorMessage.getInformation(), 0, 0);
    }

}
