package se.precom.synchronization;

import java.lang.reflect.Type;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import se.precom.core.communication.EntitySerializer.SerializationTypeHandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;

class JsonSerializer {

    private ArrayList<SimpleEntry<Type, com.google.gson.JsonSerializer<?>>> _customSerializers =
            new ArrayList<SimpleEntry<Type, com.google.gson.JsonSerializer<?>>>();

    private ArrayList<SimpleEntry<Type, com.google.gson.JsonDeserializer<?>>> _customDeserializers =
            new ArrayList<SimpleEntry<Type, com.google.gson.JsonDeserializer<?>>>();

    JsonSerializer() {
        this(true);
    }

    JsonSerializer(boolean addDefaultTypeHandlers) {
        if (addDefaultTypeHandlers) {
            createDefaultTypeHandlers();
        }
    }

    private Gson createGson(Class<?> type, List<String> propertiesToInclude) {
        GsonBuilder gsonBuilder;
        if (propertiesToInclude != null) {
            gsonBuilder = new GsonBuilder().setExclusionStrategies(new SynchronizationExclusionStrategy(type,
                    propertiesToInclude));
        } else {
            gsonBuilder = new GsonBuilder();
        }

        for (SimpleEntry<Type, com.google.gson.JsonSerializer<?>> entry : _customSerializers) {
            gsonBuilder.registerTypeAdapter(entry.getKey(), entry.getValue());
        }

        for (SimpleEntry<Type, com.google.gson.JsonDeserializer<?>> entry : _customDeserializers) {
            gsonBuilder.registerTypeAdapter(entry.getKey(), entry.getValue());
        }

        return gsonBuilder.create();
    }

    private void createDefaultTypeHandlers() {
    	addTypeHandler(Date.class, new DateSerializationTypeHandler());
    }

    private <T> void addTypeHandler(Type type, final SerializationTypeHandler<T> serializer) {

        // Create wrappers that will call our serialization handler.
        com.google.gson.JsonSerializer<T> serializeWrapper = new com.google.gson.JsonSerializer<T>() {

            @Override
            public JsonElement serialize(T object, Type typeOfT, JsonSerializationContext context) {
                return new JsonPrimitive(serializer.serialize(object, typeOfT));
            }
        };

        com.google.gson.JsonDeserializer<T> deserializeWrapper = new com.google.gson.JsonDeserializer<T>() {

            @Override
            public T deserialize(JsonElement object, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                return serializer.deserialize(object.getAsString(), typeOfT);
            }
        };

        _customSerializers.add(
                new SimpleEntry<Type, com.google.gson.JsonSerializer<?>>(
                        type, serializeWrapper));

        _customDeserializers.add(
                new SimpleEntry<Type, com.google.gson.JsonDeserializer<?>>(
                        type, deserializeWrapper));
    }

    String serialize(Object object) {
        Gson gson = createGson(object.getClass(), null);
        return gson.toJson(object);
    }

    String serialize(Object object, List<String> propertiesToInclude) {
        Gson gson = createGson(object.getClass(), propertiesToInclude);
        String serializedEntity = gson.toJson(object);
        return serializedEntity;
    }

    <T> T deserialize(String serializedEntity, Class<T> objectType) {
        Gson gson = createGson(objectType, null);
        return gson.fromJson(serializedEntity, objectType);
    }

    static <T> T deserialize(String serializedEntity, Type typeOfT) {
        Gson gson = new Gson();
        return gson.fromJson(serializedEntity, typeOfT);
    }
}