package se.precom.synchronization;

import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.inject.Inject;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.log.LogBase;
import se.precom.core.settings.SettingsBase;
import se.precom.core.storage.StorageBase;

/**
 * Synchronization module used to sync data between the client and the server.
 * 
 */
public class SynchronizationModule extends SynchronizationClientBase {

    private final int _DEFAULT_SYNC_PACKET_SIZE_IN_KB = 700;
    private final int _DEFAULT_MAXIMUM_SYNC_PACKET_SIZE_IN_KB = 700;
    private final int _DEFAULT_MINIMUM_SYNC_PACKET_SIZE_IN_KB = 10;

    private TypeManager _typeManager;

    boolean _enablePerformanceLogs = false;
    boolean _enablePropertySynchronization = true;

    CommunicationBase _communication;
    SettingsBase _settings;
    LogBase _log;
    StorageBase _storage;
    
    PushManager _pushManager;
    PullManager _pullManager;
    EntityUpdateManager _updateManager;
    ServerErrorManager _serverErrorManager;
    LogHelper _logHelper;
    
    @Inject
    public SynchronizationModule(CommunicationBase communication, SettingsBase settings, LogBase log, StorageBase storage) {
        _communication = communication;
        _settings = settings;
        _log = log;
        _storage = storage;
        _logHelper = new LogHelper(_log);
    }

    @Override
    public void initialize() throws Exception {

        _typeManager = new TypeManager();

        new SynchronizationDao(_storage).createDataBaseTables();

        JsonSerializer jsonSerializer = new JsonSerializer();

        int definedSyncPacketSizeInKb = _settings.read("Synchronization", "MaximumSyncPacketSizeInKb", _DEFAULT_SYNC_PACKET_SIZE_IN_KB, false);

        _enablePerformanceLogs = _settings.read("Synchronization", "EnablePerformanceLogs", false, false);

        // Removed the possibility to turn off the delta synchronization
        // _enablePropertySynchronization = Application.getInstance().getSettings().read("Synchronization",
        // "EnablePropertySynchronization", true, true);

        int definedSyncPacketSizeInBytes = setSyncPacketSize(definedSyncPacketSizeInKb) * 1024;

        _pushManager = new PushManager(_typeManager, jsonSerializer, this, definedSyncPacketSizeInBytes, _communication, _storage, _log);
        _pullManager = new PullManager(_typeManager, jsonSerializer, this, definedSyncPacketSizeInBytes, _communication, _storage, _log);
        _updateManager = new EntityUpdateManager(_typeManager, jsonSerializer, this, definedSyncPacketSizeInBytes, _communication, _storage, _log);
        _serverErrorManager = new ServerErrorManager(_typeManager, jsonSerializer, this, definedSyncPacketSizeInBytes, _communication, _storage, _log);

        setInitialized(true);
    }

    @Override
    public void dispose() {
        _pushManager.dispose();
        _pullManager.dispose();
        _updateManager.dispose();
        _serverErrorManager.dispose();
        setInitialized(false);
    }

    @Override
    public String getVersion() {
        return ReleaseInformation.VERSION_NAME;
    }

    private int setSyncPacketSize(int definedSyncPacketSizeInKb) {

        if (definedSyncPacketSizeInKb > _DEFAULT_MAXIMUM_SYNC_PACKET_SIZE_IN_KB) {
            _logHelper.logError(this.getName(), LogId.SYNC_E_1, "MaximumSyncPacketSizeInKb setting is out of range",
                    "The value of the setting \"MaximumSyncPacketSizeInKb\" is ("
                            + definedSyncPacketSizeInKb + "), Maximum supported value "
                            + _DEFAULT_MAXIMUM_SYNC_PACKET_SIZE_IN_KB + " will be used");
            return _DEFAULT_MAXIMUM_SYNC_PACKET_SIZE_IN_KB;
        } else if (definedSyncPacketSizeInKb < _DEFAULT_MINIMUM_SYNC_PACKET_SIZE_IN_KB) {
            _logHelper.logError(this.getName(), LogId.SYNC_E_1, "MaximumSyncPacketSizeInKb setting is out of range",
                    "The value of the setting \"MaximumSyncPacketSizeInKb\" is ("
                            + definedSyncPacketSizeInKb + "), Minimum supported value "
                            + _DEFAULT_MINIMUM_SYNC_PACKET_SIZE_IN_KB + " will be used");
            return _DEFAULT_MINIMUM_SYNC_PACKET_SIZE_IN_KB;
        }

        return definedSyncPacketSizeInKb;
    }

    @Override
    public <T extends ISynchronizable> void registerStorage(Class<T> objectType,
            SynchronizedStorageBase<T> storage) {

        checkObjectType(objectType);
        if (storage == null) {
            throw new IllegalArgumentException("storage cannot be null");
        }

        if (_typeManager.typeExists(objectType)) {
            throw new IllegalArgumentException(objectType.getSimpleName() + " entity is already registered");
        }

        _typeManager.addStorage(objectType, storage);
        _typeManager.setLock(objectType.getSimpleName(), new ReentrantLock());
        storage.setLock(getLock(objectType));
        _updateManager.addOnEntityChangedListenerToStorage(objectType, storage);
    }

    @Override
    public <T extends ISynchronizable> void unregisterStorage(Class<T> objectType,
            SynchronizedStorageBase<T> storage) {

        checkObjectType(objectType);
        if (storage == null) {
            throw new IllegalArgumentException("storage cannot be null");
        }

        if (!_typeManager.typeExists(objectType)) {
            throw new IllegalArgumentException(objectType.getSimpleName() + " entity is not registered");
        }

        _typeManager.removeStorage(objectType);        
    }

    @Override
    public <T extends ISynchronizable> void addOnSyncEventListener(Class<T> objectType,
            OnSyncEventListener onSyncEventListener) {

        checkObjectType(objectType);
        if (onSyncEventListener == null) {
            throw new IllegalArgumentException("onSyncEventListener cannot be null");
        }

        _typeManager.addSyncEventListeners(objectType.getSimpleName(), onSyncEventListener);
    }

    @Override
    public <T extends ISynchronizable> void removeOnSyncEventListener(Class<T> objectType,
            OnSyncEventListener onSyncEventListener) {

        checkObjectType(objectType);
        if (onSyncEventListener == null) {
            throw new IllegalArgumentException("onSyncEventListener cannot be null");
        }

        _typeManager.removeSyncEventListeners(objectType.getSimpleName(), onSyncEventListener);
    }

    @Override
    public <T extends ISynchronizable> void registerAutomaticPush(Class<T> objectType,
            EnumSet<Operation> entityChange) {

        checkObjectType(objectType);
        if (entityChange == null) {
            throw new IllegalArgumentException("entityChange cannot be null");
        }
        _typeManager.addAutomaticPushOperation(objectType.getSimpleName(), entityChange);
    }
    
    @Override
    public <T extends ISynchronizable> void unregisterAutomaticPush(Class<T> objectType,
            EnumSet<Operation> entityChange) {

        checkObjectType(objectType);
        if (entityChange == null) {
            throw new IllegalArgumentException("entityChange cannot be null");
        }
        _typeManager.removeAutomaticPushOperation(objectType.getSimpleName(), entityChange);
    }

    @Override
    public <T extends ISynchronizable> void setOnConflictListener(Class<T> objectType,
            OnConflictListener onConflictListener) {

        checkObjectType(objectType);
        if (onConflictListener == null) {
            throw new IllegalArgumentException("onConflictListener cannot be null");
        }

        _typeManager.addOnConflictListener(objectType.getSimpleName(), onConflictListener);
    }
    
    @Override
    public <T extends ISynchronizable> void removeOnConflictListener(Class<T> objectType) {

        checkObjectType(objectType);
        _typeManager.removeOnConflictListeners(objectType.getSimpleName());
    }

    @Override
    public <T extends ISynchronizable> PushResult push(Class<T> objectType)
            throws NotLoggedInException, MaxMessageSizeException, SynchronizedStorageException {

        checkObjectType(objectType);

        return _pushManager.push(objectType);
    }

    @Override
    public <T extends ISynchronizable> PushResult push(Class<T> objectType,
            PushArgs pushArgs) throws NotLoggedInException,
            MaxMessageSizeException, SynchronizedStorageException {

        checkObjectType(objectType);
        if (pushArgs == null || pushArgs.getEntityIdList().size() == 0) {
            throw new IllegalArgumentException("entityIds cannot be null or empty");
        }

        return _pushManager.push(objectType, pushArgs.getEntityIdList());
    }

    @Override
    public <T extends ISynchronizable> PullResult pull(Class<T> objectType, PullArgs pullArgs)
            throws NotLoggedInException, MaxMessageSizeException, SynchronizedStorageException {

        checkObjectType(objectType);
        if (pullArgs == null) {
            throw new IllegalArgumentException("pullArgs cannot be null");
        } else if (pullArgs.getFilter() == null) {
            throw new IllegalArgumentException("filter cannot be null");
        }

        String objectTypeSimpleName = objectType.getSimpleName();
        return _pullManager.pull(objectTypeSimpleName, pullArgs, SyncEventType.CLIENT_PULL);
    }

    @Override
    public <T extends ISynchronizable> void deleteLocally(Class<T> objectType, List<UUID> entityIds)
            throws SynchronizedStorageException {
        checkObjectType(objectType);
        if (entityIds == null || entityIds.size() == 0) {
            throw new IllegalArgumentException("entityIds cannot be null or empty");
        }

        String objectTypeSimpleName = objectType.getSimpleName();
        _updateManager.deleteLocally(objectTypeSimpleName, entityIds);
    }

    <T extends ISynchronizable> Lock getLock(Class<T> objectType) {
        checkObjectType(objectType);

        return _typeManager.getLock(objectType.getSimpleName());
    }

    private static <T extends ISynchronizable> void checkObjectType(Class<T> objectType) {
        if (objectType == null) {
            throw new IllegalArgumentException("objectType cannot be null");
        }
    }
}
