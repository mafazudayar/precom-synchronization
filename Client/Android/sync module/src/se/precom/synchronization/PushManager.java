package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.CommunicationBase.OnReceiveListener;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.TypeReference;
import se.precom.core.log.LogBase;
import se.precom.core.storage.StorageBase;

class PushManager extends SynchronizationManager {

	CopyOnWriteArrayList<SyncObject> _sendingSyncObjectList = new CopyOnWriteArrayList<SyncObject>();

    private final OnReceiveListener<PushResponse> _pushResponseReceiveListener;
    final int EMPTY_PUSH_REQUEST_SIZE = 152;

    PushManager(TypeManager typeManager, JsonSerializer jsonSerializer, SynchronizationModule syncModule,
            int maximumSizeForTransferObject, CommunicationBase communication, StorageBase storage,
            LogBase log) {

        super(typeManager, jsonSerializer, syncModule, maximumSizeForTransferObject,
                communication, storage, log);

        _pushResponseReceiveListener = new OnReceiveListener<PushResponse>() {

            @Override
            public void receive(PushResponse entity) {
                PushManager.this.processPushResponce(entity);
            }
        };

        getCommunication().addOnReceiveListener(new TypeReference<PushResponse>() {
        }, _pushResponseReceiveListener);
    }

    private void processPushResponce(final PushResponse entity) {

        Thread pushResponceHandler = new Thread() {
            @Override
            public void run() {
                try {
                    PushManager.this.onPushResponseReceived(entity);
                } catch (IllegalArgumentException e) {
                    // For ELT-1030, don't log this as error right now. They register objects in server that are not
                    // registered in client
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_E_2,
                            "IllegalArgumentException when processing pull invoke message",
                            e.getMessage());
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_2,
                            "Exception when processing pull invoke message", e);
                }
            }
        };

        pushResponceHandler.start();
    }

    void dispose() {

        getCommunication().removeOnReceiveListener(new TypeReference<PushResponse>() {
        }, _pushResponseReceiveListener);
    }

    <T extends ISynchronizable> PushResult push(Class<T> objectType, List<UUID> entityIdList)
            throws NotLoggedInException, MaxMessageSizeException, SynchronizedStorageException {

        String objectTypeSimpleName = objectType.getSimpleName();
        PushResult pushResult;

        Lock lock = getTypeManager().getLock(objectTypeSimpleName);
        if (lock != null) {
            lock.lock();
        }
        try {

            if (entityIdList.size() < 1) {

                pushResult = push(objectType);

            } else {

                pushResult = new PushResult();
                UUID syncSessionId = UUID.randomUUID();

                if (isEnablePerformanceLogs())
                {
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_1, "Performance Logs",
                            "Push request initiated",
                            "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_2, "Performance Logs",
                            "Calling custom storage",
                            "Method: Read(entityIds), Object Type: " + objectTypeSimpleName +
                                    ", Sync Session Id: " + syncSessionId);
                }

                SynchronizedStorageBase<ISynchronizable> storage = getTypeManager().getStorage(objectTypeSimpleName);
                List<ISynchronizable> entities = storage.read(entityIdList);

                if (isEnablePerformanceLogs())
                {
                    getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_3, "Performance Logs",
                            "Custom storage responded",
                            "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
                }

                List<SyncObject> syncObjects = new ArrayList<SyncObject>();
                List<UUID> entitiesNotInStorage = new ArrayList<UUID>();

                for (UUID syncEntityId : entityIdList) {

                    SyncMetaObject syncMetaObject = getSynchronizationDao().getMetadata(objectTypeSimpleName,
                            syncEntityId);

                    if (syncMetaObject.getStatus() != -1 && syncMetaObject.getVersion() != -1
                            && syncMetaObject.getStatus() != 0) {

                        SyncObject syncObject = new SyncObject(syncMetaObject);
                        syncObject.setChangeId(syncMetaObject.getChangeId());

                        if (IsSyncEntityNotSent(syncObject))
                        {
                            _sendingSyncObjectList.add(syncObject);
                            boolean found = false;
                            for (ISynchronizable tEntity : entities) {

                                if (tEntity != null && syncMetaObject.getGuid().equals(tEntity.getGuid())) {

                                    List<String> changedProperties = null;

                                    if (isEnablePropertySynchronization()) {
                                        changedProperties = getSynchronizationDao()
                                            .getChangedProperties(tEntity.getGuid());
                                    }
                                    else
                                    {
                                        changedProperties = getTypeManager().getProperties(
                                                objectType.getSimpleName());
                                    }

                                    for (String changedProperty : changedProperties) {
                                        syncObject.getChangedPropertyNames().add(
                                                changedProperty);
                                    }
                                    syncObject.setObject(getJsonSerializer().serialize(
                                            tEntity, changedProperties));
                                    found = true;
                                    break;
                                }
                            }

                            syncObjects.add(syncObject);
                            pushResult.getPushedEntityIds().add(syncObject.getGuid());

                            if (!found && syncObject.getStatus() != SyncStatus.DELETED.getValue()) {
                                entitiesNotInStorage.add(syncEntityId);
                                logObjectNotFound(syncEntityId);
                            }
                        }
                    }
                }

                for (UUID id : entitiesNotInStorage) {
                    removeFirstSyncObjectWithId(id, syncObjects);
                }

                if (syncObjects.size() > 0) {
                    pushResult.setSyncSessionId(syncSessionId);
                    sendPushRequests(objectTypeSimpleName, syncObjects, syncSessionId);

                    if (isEnablePerformanceLogs())
                    {
                        getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_4, "Performance Logs",
                                "Push requests sent",
                                "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
                    }
                }
            }

            return pushResult;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    <T extends ISynchronizable> PushResult push(Class<T> objectType) throws NotLoggedInException,
            MaxMessageSizeException, SynchronizedStorageException {

        String objectTypeSimpleName = objectType.getSimpleName();

        Lock lock = getTypeManager().getLock(objectTypeSimpleName);
        if (lock != null) {
            lock.lock();
        }
        try {

            PushResult pushResult;
            UUID syncSessionId = UUID.randomUUID();

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_1, "Performance Logs", "Push request initiated",
                        "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
            }

            List<SyncObject> syncMetadataCollection = GetSyncMetadataCollection(objectTypeSimpleName);

            if (syncMetadataCollection == null || syncMetadataCollection.size() == 0) {
                return new PushResult();
            }

            pushResult = new PushResult();
            pushResult.setSyncSessionId(syncSessionId);
            List<UUID> syncEntityGuidList = new ArrayList<UUID>();

            for (SyncMetaObject syncMetaObject : syncMetadataCollection) {
                syncEntityGuidList.add(syncMetaObject.getGuid());
            }

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_2, "Performance Logs", "Calling custom storage",
                        "Method: Read(entityIds), Object Type: " + objectTypeSimpleName +
                                ", Sync Session Id: " + syncSessionId);
            }

            SynchronizedStorageBase<ISynchronizable> storage = getTypeManager().getStorage(objectTypeSimpleName);
            List<ISynchronizable> entities = storage.read(syncEntityGuidList);

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_3, "Performance Logs",
                        "Custom storage responded",
                        "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
            }

            List<UUID> entitiesNotInStorage = new ArrayList<UUID>();

            for (SyncObject syncObject : syncMetadataCollection) {

                boolean found = false;

                for (ISynchronizable entity : entities) {

                    if (syncObject.getGuid().equals(entity.getGuid())) {

                        List<String> changedProperties = null;

                        if (isEnablePropertySynchronization())
                        {
                            changedProperties = getSynchronizationDao().getChangedProperties(entity.getGuid());
                        }
                        else
                        {
                            changedProperties = getTypeManager().getProperties(objectType.getSimpleName());
                        }

                        for (String changedProperty : changedProperties) {
                            syncObject.getChangedPropertyNames().add(changedProperty);
                        }

                        syncObject.setObject(getJsonSerializer().serialize(entity, changedProperties));
                        found = true;
                        break;
                    }
                }

                pushResult.getPushedEntityIds().add(syncObject.getGuid());

                if (!found && syncObject.getStatus() != SyncStatus.DELETED.getValue()) {
                    entitiesNotInStorage.add(syncObject.getGuid());
                    logObjectNotFound(syncObject.getGuid());
                }
            }

            for (UUID guid : entitiesNotInStorage) {
                removeFirstSyncObjectWithId(guid, syncMetadataCollection);
            }

            sendPushRequests(objectTypeSimpleName, syncMetadataCollection, syncSessionId);

            if (isEnablePerformanceLogs())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_4, "Performance Logs", "Push requests sent",
                        "Object Type: " + objectTypeSimpleName + ", Sync Session Id: " + syncSessionId);
            }

            return pushResult;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void sendPushRequests(String objectType, List<SyncObject> syncObjects, UUID syncSessionId)
            throws NotLoggedInException, MaxMessageSizeException {

        if (syncObjects.size() <= 0) {
            return;
        }

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            ArrayList<SyncObject> objectsToBeSent = new ArrayList<SyncObject>();
            List<UUID> idList = new ArrayList<UUID>();
            int filledSizeOfTheCurrentBucket = 0;
            for (SyncObject syncObject : syncObjects) {

                idList.add(syncObject.getGuid());
                notifyOnSyncEventListeners(objectType, syncObject.getGuid(), SyncEventReason.STARTED,
                        SyncEventType.CLIENT_PUSH, syncSessionId);

                String serializedSyncObject = getJsonSerializer().serialize(syncObject);
                int lengthOfSerializedSyncObject = serializedSyncObject.length();

                // Fixed - PC-2506, we need to leave some space for the characters created due to double serialization.
                int possibleSizeIncrement = (getCharacterOccurences(serializedSyncObject) * 2) + 2;

                // If the size of the entity is larger than the maximum size allowed, the entity is send as multiple
                // PushRequests to the server.
                // We deduct 152 (EMPTY_PUSH_REQUEST_SIZE) to leave some space for the push request.
                int allowedMaxPacketSize = getMaximumSizeForTransferObject() - EMPTY_PUSH_REQUEST_SIZE
                        - possibleSizeIncrement;
                if (lengthOfSerializedSyncObject > allowedMaxPacketSize) {

                    String[] entitySplits = splitSerializedEntity(syncObject.getObject(), allowedMaxPacketSize);

                    // Functionality to send multiple push requests for a single
                    // object when the object size is larger than
                    // maximumSizeForTransferObject.
                    for (int i = 0; i < entitySplits.length; i++) {

                        SyncObject syncObjectForObjectPartition = new SyncObject(syncObject.getGuid(),
                                syncObject.getVersion(), entitySplits[i]);
                        syncObjectForObjectPartition.setStatus(syncObject.getStatus());
                        syncObjectForObjectPartition.setChangeId(syncObject.getChangeId());

                        for (String changedPropertyName : syncObject.getChangedPropertyNames()) {
                            syncObjectForObjectPartition.getChangedPropertyNames().add(changedPropertyName);
                        }

                        ArrayList<SyncObject> syncObjectList = new ArrayList<SyncObject>();
                        syncObjectList.add(syncObjectForObjectPartition);

                        sendPushRequest(syncObjectList, syncSessionId, objectType, Boolean.TRUE, entitySplits.length, i);

                    }
                } else {
                    if (filledSizeOfTheCurrentBucket
                            + lengthOfSerializedSyncObject > allowedMaxPacketSize) {
                        sendPushRequest(objectsToBeSent, syncSessionId,
                                objectType, Boolean.FALSE, 0, 0);
                        objectsToBeSent.clear();
                        objectsToBeSent.add(syncObject);
                        filledSizeOfTheCurrentBucket = lengthOfSerializedSyncObject;
                    } else {
                        objectsToBeSent.add(syncObject);
                        filledSizeOfTheCurrentBucket += lengthOfSerializedSyncObject;
                    }
                }
            }
            if (objectsToBeSent.size() > 0) {
                sendPushRequest(objectsToBeSent, syncSessionId, objectType,
                        Boolean.FALSE, 0, 0);
            }
            getSynchronizationDao().updateSessionId(idList, syncSessionId);

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    void sendPushRequest(ArrayList<SyncObject> objectsToBeSent, UUID sessionId, String objectType,
            Boolean isSplit, int totalNumberOfPackets, int packetNumber) throws NotLoggedInException,
            MaxMessageSizeException {

        PushRequest pushRequest = new PushRequest();
        pushRequest.setObjectType(objectType);
        pushRequest.setSessionGuid(sessionId);
        pushRequest.setIsSplit(isSplit);
        pushRequest.setPacketNumber(packetNumber);
        pushRequest.setTotalNumberOfPackets(totalNumberOfPackets);

        for (SyncObject syncObject : objectsToBeSent) {
            pushRequest.getSynchronizedObjects().add(syncObject);
        }

        RequestArgs reqArgs = new RequestArgs(new TypeReference<PushRequest>() {
        });
        sendRequest(pushRequest, reqArgs);
    }

    private void onPushResponseReceived(PushResponse pushResponse) {

        String objectType = pushResponse.getObjectType();
        UUID sessionId = pushResponse.getSessionGuid();

        if (isEnablePerformanceLogs())
        {
            getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_5, "Performance Logs", "PushResponse received",
                    ((IDebugPrintable) pushResponse).getDebugString());
        }

        // remove sent items from sent entities
        RemoveObjectFromSendingList(pushResponse);

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            if (pushResponse.isSplit().booleanValue()) {

                SyncObject syncObject = pushResponse.getResolvedObjects().get(0);
                UUID objectId = syncObject.getGuid();

                String mostRecentSessionId = getSynchronizationDao().getMostRecentSessionId(objectId);

                if (mostRecentSessionId == null) {
                    logNoSessionFound(sessionId, objectId);
                }
                else if (mostRecentSessionId.equals(sessionId.toString()) || mostRecentSessionId.equals("")) {

                    getSynchronizationDao().insertPartialEntity(objectId, sessionId,
                            pushResponse.getPacketNumber(), syncObject.getObject());
                    int numberOfPacketsOnDb = getSynchronizationDao().getPartialEntityCount(objectId, sessionId);

                    if (numberOfPacketsOnDb == pushResponse.getTotalNumberOfPackets()) {

                        List<String> partialEntityList = getSynchronizationDao().getPartialEntities(objectId,
                                sessionId);

                        syncObject.setObject(combinePartialItems(partialEntityList));

                        try {

                            manageConflicts(objectId, objectType, syncObject);
                            notifyOnSyncEventListeners(objectType, syncObject.getGuid(),
                                    SyncEventReason.SYNCHRONIZED, SyncEventType.CLIENT_PUSH, sessionId);

                        } catch (SynchronizedStorageException e) {

                            notifyOnSyncEventListeners(objectType, syncObject.getGuid(), SyncEventReason.ERROR,
                                    SyncEventType.CLIENT_PUSH, sessionId);
                            getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_1,
                                    "SynchronizedStorageException when push response received, sessionId: "
                                            + sessionId.toString(), e);
                        } finally {
                            getSynchronizationDao().removePartialEntities(objectId, sessionId);
                        }

                        if (isEnablePerformanceLogs())
                        {
                            getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_6, "Performance Logs",
                                    "Push response completed",
                                    "Object Type: " + objectType + ", Sync Session Id: " + sessionId);
                        }
                    }
                }

            } else {

                for (SyncObject syncObject : pushResponse.getResolvedObjects()) {

                    UUID id = syncObject.getGuid();
                    String mostRecentSessionId = getSynchronizationDao().getMostRecentSessionId(id);

                    if (mostRecentSessionId == null) {
                        logNoSessionFound(sessionId, id);
                    }
                    else if (mostRecentSessionId.equals(sessionId.toString())) {

                        try {

                            if (syncObject.getStatus() != SyncStatus.DELETED.getValue()) {
                                manageConflicts(id, objectType, syncObject);
                            } else {
                                deleteCustomEntity(objectType, id);
                                deleteMetadata(objectType, id);
                            }

                            notifyOnSyncEventListeners(objectType, id, SyncEventReason.SYNCHRONIZED,
                                    SyncEventType.CLIENT_PUSH, sessionId);

                        } catch (SynchronizedStorageException e) {
                            notifyOnSyncEventListeners(objectType, id, SyncEventReason.ERROR,
                                    SyncEventType.CLIENT_PUSH, sessionId);
                            getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_1,
                                    "SynchronizedStorageException when push response received, sessionId: "
                                            + sessionId.toString(), e);
                        }
                    }
                }

                for (SyncMetaObject syncMetaObject : pushResponse.getAcceptedObjects()) {

                    UUID id = syncMetaObject.getGuid();
                    String mostRecentSessionId = getSynchronizationDao().getMostRecentSessionId(id);
                    if (mostRecentSessionId == null) {
                        logNoSessionFound(sessionId, id);
                    }
                    else if (mostRecentSessionId.equals(sessionId.toString())) {
                        if (syncMetaObject.getStatus() != SyncStatus.DELETED.getValue()) {

                            updateMetadataAsSynchronized(objectType, syncMetaObject, false);
                            notifyOnSyncEventListeners(objectType, id, SyncEventReason.SYNCHRONIZED,
                                    SyncEventType.CLIENT_PUSH, sessionId);
                        } else {
                            deleteMetadata(objectType, id);
                            notifyOnSyncEventListeners(objectType, id, SyncEventReason.SYNCHRONIZED,
                                    SyncEventType.CLIENT_PUSH, sessionId);
                        }
                    }
                }
            }

            if (isEnablePerformanceLogs() && !pushResponse.isSplit().booleanValue())
            {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_PUSH_D_6, "Performance Logs", "Push response completed",
                        "Object Type: " + objectType + ", Sync Session Id: " + sessionId);
            }

        } catch (Exception e) {
            getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_2,
                    "Exception when push response is received, sessionId: "
                            + sessionId.toString(), e);
        }

        finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    private void logNoSessionFound(UUID sessionId, UUID objectId) {
        getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_4, "No sync session id found",
                "No sync session id found for entity " + objectId
                        + " when processing push response with session " + sessionId + ".");
    }

    private void logObjectNotFound(UUID id) {
        getLogHelper().logError(getModuleName(), LogId.SYNC_PUSH_E_3, "Object not found when pushing",
                String.format("Object with Guid %s was not found in storage", id.toString()));
    }

    private static void removeFirstSyncObjectWithId(UUID id, List<SyncObject> syncObjects) {
        SyncObject objectToRemove = null;
        for (SyncObject syncObject : syncObjects) {
            if (syncObject.getGuid().compareTo(id) == 0) {
                objectToRemove = syncObject;
            }
        }
        if (objectToRemove != null) {
            syncObjects.remove(objectToRemove);
        }
    }

    private void manageConflicts(UUID entityId, String objectType, SyncObject syncObject) throws Exception {

        if (getSynchronizationDao().getStatusForId(entityId) != SyncStatus.DELETED.getValue()) {

            List<ConflictedPropertyInfo> conflictedProperties = resolveReceivedEntity(objectType, syncObject, false);
            this.notifyOnConflictListeners(objectType, entityId, conflictedProperties);

        } else {
            rollBackDeleteOperation(objectType, syncObject);
        }
    }

    private List<SyncObject> GetSyncMetadataCollection(String objectTypeSimpleName)
    {
        List<SyncObject> filteredModifiedEntities = new ArrayList<SyncObject>();
        List<SyncObject> modifiedEntities = getSynchronizationDao().getSyncObjectsForModifiedEntities(objectTypeSimpleName);
        for (int syncObjectIndex = modifiedEntities.size() - 1; syncObjectIndex >= 0; syncObjectIndex--) {
            SyncObject syncObject = modifiedEntities.get(syncObjectIndex);
            if (IsSyncEntityNotSent(syncObject))
            {
                filteredModifiedEntities.add(syncObject);
                _sendingSyncObjectList.add(syncObject);
            }
        }
        return filteredModifiedEntities;
    }

    private boolean IsSyncEntityNotSent(SyncObject syncObjectToCheck)
    {
        boolean notSent = true;
        for (SyncObject syncObject : _sendingSyncObjectList) {
            if (syncObject.getGuid().compareTo(syncObjectToCheck.getGuid()) == 0
                    && syncObject.getChangeId().compareTo(syncObjectToCheck.getChangeId()) == 0)
            {
                notSent = false;
                break;
            }
        }
        return notSent;
    }

    private void RemoveObjectFromSendingList(PushResponse pushResponse)
    {
        for (int sendingObjectIndex = _sendingSyncObjectList.size() - 1; sendingObjectIndex >= 0; sendingObjectIndex--)
        {
            // remove if in any list
            SyncObject sendingEntity = _sendingSyncObjectList.get(sendingObjectIndex);
            boolean existsInAcceptedObjects = false;
            for (SyncMetaObject syncMetaObject : pushResponse.getAcceptedObjects()) {
                if ((syncMetaObject.getGuid().compareTo(sendingEntity.getGuid()) == 0)
                        && (syncMetaObject.getChangeId().compareTo(sendingEntity.getChangeId()) == 0))
                {
                    existsInAcceptedObjects = true;
                    break;
                }
            }
            boolean existsInResolvedObjects = false;
            for (SyncMetaObject syncMetaObject : pushResponse.getResolvedObjects()) {
                if ((syncMetaObject.getGuid().compareTo(sendingEntity.getGuid()) == 0)
                        && (syncMetaObject.getChangeId().compareTo(sendingEntity.getChangeId()) == 0))
                {
                    existsInResolvedObjects = true;
                    break;
                }
            }

            if (existsInAcceptedObjects || existsInResolvedObjects)
            {
                _sendingSyncObjectList.remove(sendingEntity);
            }
        }
    }
}
