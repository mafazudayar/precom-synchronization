package se.precom.synchronization;

/**
 * Interface to be implemented by the entities which need detailed debug string.
 */
interface IDebugPrintable {

    /**
     * Returns the detailed debug string.
     * 
     * @return the debug string.
     */
    String getDebugString();
}
