package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import se.precom.synchronization.SynchronizationClientBase.OnConflictListener;

/**
 * Used to send conflicted property information of an entity on the client side to {@link OnConflictListener}s.
 * 
 */
public class ConflictNotificationInfo {

    private UUID _entityId;
    private List<ConflictedPropertyInfo> _conflicts;

    /**
     * Initializes a new instance of the {@link ConflictNotificationInfo}.
     * 
     * @param entityId
     *            Id of the entity.
     * @param conflictedPropertyList
     *            list of {@link ConflictedPropertyInfo}.
     */
    ConflictNotificationInfo(UUID entityId,
            List<ConflictedPropertyInfo> conflictedPropertyList) {
        this._entityId = entityId;
        _conflicts = new ArrayList<ConflictedPropertyInfo>();
        for (ConflictedPropertyInfo conflictDetail : conflictedPropertyList) {
            _conflicts.add(conflictDetail);
        }
    }

    /**
     * Returns the Id of the entity which has conflicted properties.
     * 
     * @return Id of the entity.
     */
    public UUID getEntityId() {
        return _entityId;
    }

    /**
     * Returns a list of {@link ConflictedPropertyInfo} for the properties, which were modified at both client and
     * server side.
     * 
     * @return list of {@link ConflictedPropertyInfo}.
     */
    public List<ConflictedPropertyInfo> getConflicts() {
        return _conflicts;
    }

}
