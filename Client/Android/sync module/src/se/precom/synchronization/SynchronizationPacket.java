package se.precom.synchronization;

import java.util.UUID;

import se.precom.core.communication.EntityBase;

/**
 * Entity Containing the basic information required for the synchronization process.
 */
public class SynchronizationPacket extends EntityBase implements IDebugPrintable {

    private UUID SessionGuid;
    private String ObjectType;
    private int TotalNumberOfPackets;
    private int PacketNumber;
    private Boolean IsSplit;

    /**
     * Returns the synchronization session Id.
     * 
     * @return session GUID.
     */
    public UUID getSessionGuid() {
        return SessionGuid;
    }

    /**
     * Sets the synchronization session Id.
     * 
     * @param sessionGuid
     *            session GUID.
     */
    public void setSessionGuid(UUID sessionGuid) {
        SessionGuid = sessionGuid;
    }

    /**
     * Returns the object type.
     * 
     * @return object type.
     */
    public String getObjectType() {
        return ObjectType;
    }

    /**
     * Sets the object type.
     * 
     * @param objectType
     *            the object type.
     */
    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    /**
     * Returns the total number of packets, if the same object is broken into several parts.
     * 
     * @return the total number of packets.
     */
    public int getTotalNumberOfPackets() {
        return TotalNumberOfPackets;
    }

    /**
     * Sets the total number of packets, if the same object is broken into several parts.
     * 
     * @param totalNumberOfPackets
     *            the total number of packets.
     */
    public void setTotalNumberOfPackets(int totalNumberOfPackets) {
        TotalNumberOfPackets = totalNumberOfPackets;
    }

    /**
     * Returns the packet number, for parts of large objects sent in multiple packets.
     * 
     * @return packet number.
     */
    public int getPacketNumber() {
        return PacketNumber;
    }

    /**
     * Sets the packet number for parts of large objects sent in multiple packets.
     * 
     * @param packetNumber
     *            the packet number.
     */
    public void setPacketNumber(int packetNumber) {
        PacketNumber = packetNumber;
    }

    /**
     * Returns whether the {@link SynchronizationPacket} contains a partial entity.
     * 
     * @return true if contains a partial entity, else false.
     */
    public Boolean isSplit() {
        return IsSplit;
    }

    /**
     * Sets whether the {@link SynchronizationPacket} contains a partial entity.
     * 
     * @param isSplit
     *            true if contains a partial entity, else false.
     */
    public void setIsSplit(Boolean isSplit) {
        IsSplit = isSplit;
    }

    /**
     * Returns the string for debug logging.
     * 
     * @return String.
     */
    @Override
    public String getDebugString() {
        return String.format(
                "SessionGuid: %s, TotalNumberOfPackets: %s, PacketNumber: %s, ObjectType: %s, IsSplit: %s",
                getSessionGuid(), getTotalNumberOfPackets(), getPacketNumber(), getObjectType(), isSplit());
    }
}
