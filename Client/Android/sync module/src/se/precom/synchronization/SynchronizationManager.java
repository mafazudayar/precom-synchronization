package se.precom.synchronization;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.EntityBase;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.SendPriority;
import se.precom.core.log.LogBase;
import se.precom.core.storage.StorageBase;
import se.precom.synchronization.SynchronizationClientBase.OnConflictListener;
import se.precom.synchronization.SynchronizationClientBase.OnSyncEventListener;

class SynchronizationManager {

    private final TypeManager _typeManager;
    private final JsonSerializer _jsonSerializer;
    private final SynchronizationModule _syncModule;
    private final int _maximumSizeForTransferObject;
    private final SynchronizationDao _synchronizationDao;
    private final CommunicationBase _communication;
    private final LogHelper _logHelper;

    SynchronizationManager(TypeManager typeManager, JsonSerializer jsonSerializer,
            SynchronizationModule syncModule, int maximumSizeForTransferObject,
            CommunicationBase communication, StorageBase storage, LogBase log) {

        _typeManager = typeManager;
        _jsonSerializer = jsonSerializer;
        _syncModule = syncModule;
        _maximumSizeForTransferObject = maximumSizeForTransferObject;
        _communication = communication;
        _synchronizationDao = new SynchronizationDao(storage);
        _logHelper = new LogHelper(log);
    }

    protected boolean isEnablePerformanceLogs() {
        return _syncModule._enablePerformanceLogs;
    }

    protected boolean isEnablePropertySynchronization() {
        return _syncModule._enablePropertySynchronization;
    }

    protected int getMaximumSizeForTransferObject() {
        return _maximumSizeForTransferObject;
    }

    protected TypeManager getTypeManager() {
        return _typeManager;
    }

    protected JsonSerializer getJsonSerializer() {
        return _jsonSerializer;
    }

    protected String getModuleName() {
        return _syncModule.getName();
    }

    protected SynchronizationModule getSyncModule() {
        return _syncModule;
    }

    protected SynchronizationDao getSynchronizationDao() {
        return _synchronizationDao;
    }

    protected CommunicationBase getCommunication() {
        return _communication;
    }

    protected LogHelper getLogHelper() {
        return _logHelper;
    }

    protected List<ConflictedPropertyInfo> resolveReceivedEntity(String objectType, SyncObject syncObject,
            boolean ignoreChangeId)
            throws Exception {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            SynchronizedStorageBase<ISynchronizable> storage = getTypeManager()
                    .getStorage(objectType);
            List<ConflictedPropertyInfo> conflictDetailList = new ArrayList<ConflictedPropertyInfo>();
            UUID objectId = syncObject.getGuid();
            ISynchronizable receivedSyncEntity = getJsonSerializer()
                    .deserialize(syncObject.getObject(),
                            getTypeManager().getType(objectType));

            // TODO: If the received entity has been already deleted on the
            // client,but the change is not pushed to the
            // server, existing Entity could be null. This scenario doesn't occur
            // with the current implementation because of
            // the session ID mismatch.
            ISynchronizable existingSyncEntity = readEntityFromStorage(objectType, objectId, storage);
            Class type = getTypeManager().getType(objectType);

            if (existingSyncEntity == null) {

                List<ISynchronizable> syncEntities = new ArrayList<ISynchronizable>();
                receivedSyncEntity.setGuid(objectId);
                syncEntities.add(receivedSyncEntity);
                getTypeManager().getStorage(objectType).create(syncEntities,
                        new CreateArgs(SynchronizationModule.class.getSimpleName()));
                getSynchronizationDao().updateMetadataRecord(objectType, objectId, syncObject.getVersion(),
                        SyncStatus.SYNCHRONIZED);

                List<String> properties = getTypeManager().getProperties(objectType);
                for (String property : properties)
                {
                    Field field = getTypeManager().getField(type, property);
                    if (field != null) {
                        field.setAccessible(true);
                        Object newValue = field.get(receivedSyncEntity);
                        ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo(
                                property, newValue, null);

                        conflictDetailList.add(conflictedProperty);
                    }
                }
                getSynchronizationDao().insertPropertyMetadataRecords(syncObject.getGuid(), properties,
                        PropertySyncStatus.SYNCHRONIZED);

            } else {
                SyncMetaObject metaData = getSynchronizationDao().getMetadata(objectType, objectId);
                if (metaData.getVersion() == 0) {

                    // In case the object is received even though it has still not being properly pushed.
                    // This can happen if a push response is lost. Server will return empty changed properties
                    // so we need to handle this separately.

                    if (metaData.getStatus() == SyncStatus.UPDATED.getValue()) {
                        // Add conflicts for the properties that have been changed at client
                        // since they will be overwritten.
                        List<String> changedPropertiesAtClient = getSynchronizationDao().getChangedProperties(objectId);
                        for (String propertyName : changedPropertiesAtClient) {
                            Field field = getTypeManager().getField(type, propertyName);
                            if (field != null) {
                                field.setAccessible(true);
                                Object newValue = field.get(receivedSyncEntity);
                                Object oldValue = field.get(existingSyncEntity);
                                ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo(
                                        propertyName, newValue, oldValue);
                                conflictDetailList.add(conflictedProperty);
                            }
                        }
                    }

                    // If the GUID property is not marked as Synchronizable, it can be null. So need to set it from
                    // syncObject.
                    receivedSyncEntity.setGuid(objectId);
                    storage.update(receivedSyncEntity, new UpdateArgs(SynchronizationModule.class.getSimpleName()));
                    updateMetadataAsSynchronized(objectType, syncObject, ignoreChangeId);
                } else {
                    List<String> changedPropertiesAtClient = getSynchronizationDao().getChangedProperties(objectId);
                    List<String> changedProperties = syncObject.getChangedPropertyNames();
                    try {
                        for (String propertyName : changedProperties) {

                            Field field = getTypeManager().getField(type, propertyName);
                            if (field != null) {
                                field.setAccessible(true);
                                Object newValue = field.get(receivedSyncEntity);
                                Object oldValue = field.get(existingSyncEntity);
                                if (changedPropertiesAtClient.contains(propertyName)) {
                                    ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo(
                                            propertyName, newValue, oldValue);
                                    conflictDetailList.add(conflictedProperty);
                                }

                                field.set(existingSyncEntity, newValue);
                            }
                        }
                    } catch (Exception e) {
                        getLogHelper().logError(getModuleName(), LogId.SYNC_SM_E_1,
                                "Exception when resolving received entity", e);
                    }
                    if (changedProperties.size() > 0)
                    {
                        storage.update(existingSyncEntity, new UpdateArgs(SynchronizationModule.class.getSimpleName()));
                        // Update meta data records.
                        // If the number of conflicts are equal to the number of properties
                        // changed at the client side, then all the
                        // changed properties have been overwritten by server values.
                        // Update the meta data record as SyncStatus.SYNCHRONIZED.
                        if (conflictDetailList.size() == changedPropertiesAtClient.size()) {
                            updateMetadataAsSynchronized(objectType, syncObject, ignoreChangeId);
                        }
                        // If the number of conflicts are less than the number of properties
                        // changed at the client side, mark the
                        // conflicted properties as SYNCHRONIZED, but keep the status of the
                        // object as UPDATED.
                        else {
                            List<String> conflictedPropertyNames = new ArrayList<String>();
                            for (ConflictedPropertyInfo conflictedProperty : conflictDetailList) {
                                conflictedPropertyNames.add(conflictedProperty
                                        .getPropertyName());
                            }
                            getSynchronizationDao().updateMetadataRecord(objectType,
                                    objectId, syncObject.getVersion(), SyncStatus.UPDATED);
                            getSynchronizationDao().markPropertiesAsSynchronized(objectId,
                                    conflictedPropertyNames);
                        }
                    }
                }
            }
            return conflictDetailList;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void notifyOnConflictListeners(String objectType, UUID objectId,
            List<ConflictedPropertyInfo> conflictedProperties) {

        List<ConflictedPropertyInfo> conflictedPropertiesToNotify = new ArrayList<ConflictedPropertyInfo>();

        for (ConflictedPropertyInfo info : conflictedProperties) {
            if (!equals(info.getNewValue(), info.getOldValue())) {
                conflictedPropertiesToNotify.add(info);
            }
        }

        if (conflictedPropertiesToNotify.size() > 0) {
            ConflictNotificationInfo conflictInfo = new ConflictNotificationInfo(objectId, conflictedPropertiesToNotify);

            OnConflictListener listener = getTypeManager().getOnConflictListener(objectType);
            if (listener != null) {
                try {
                    listener.onConflict(conflictInfo);
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_SM_E_1,
                            "Exception when calling conflict listeners",
                            e);
                }
            }
        }
    }

    protected static boolean equals(Object x, Object y) {
        return (x == null ? y == null : x.equals(y));
    }

    protected void notifyOnSyncEventListeners(String objectType, UUID entityId, SyncEventReason reason,
            SyncEventType type, UUID sessionId) {

        notifyOnSyncEventListeners(objectType, entityId, reason, type, sessionId, "", 0, 0);
    }

    protected void notifyOnSyncEventListeners(String objectType, UUID entityId, SyncEventReason reason,
            SyncEventType type, UUID sessionId, String information, int totalEntityCount, int synchronizedEntityCount) {

        List<OnSyncEventListener> listenerList = getTypeManager().getSyncEventListeners(objectType);
        if (listenerList != null) {
            for (OnSyncEventListener onSyncEventListener : listenerList) {
                try {
                    SyncInfo syncInfo = new SyncInfo(reason, type, sessionId, information, totalEntityCount,
                            synchronizedEntityCount);
                    onSyncEventListener.onSyncEvent(entityId, syncInfo);
                } catch (Exception e) {
                    getLogHelper().logError(getModuleName(), LogId.SYNC_SM_E_1,
                            "Exception when calling sync event listeners", e);
                }
            }
        }
    }

    protected void rollBackDeleteOperation(String objectType, SyncObject syncObject)
            throws SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            // Fix for the issue - PC-2229
            if (syncObject.getChangeId().equals(getSynchronizationDao().getChangeId(syncObject.getGuid())))
            {
                List<SyncObject> syncObjects = new ArrayList<SyncObject>();
                syncObjects.add(syncObject);
                insertEntity(objectType, syncObjects);
                getSynchronizationDao().updateMetadataRecord(objectType, syncObject.getGuid(), syncObject.getVersion(),
                        SyncStatus.SYNCHRONIZED);
            }

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void insertEntity(String objectType, List<SyncObject> syncObjects) throws SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            List<ISynchronizable> syncEntities = new ArrayList<ISynchronizable>();
            for (SyncObject syncObject : syncObjects) {
                ISynchronizable syncEntity = getJsonSerializer().deserialize(syncObject.getObject(),
                        getTypeManager().getType(objectType));
                syncEntity.setGuid(syncObject.getGuid());
                syncEntities.add(syncEntity);
            }
            getTypeManager().getStorage(objectType).create(syncEntities,
                    new CreateArgs(SynchronizationModule.class.getSimpleName()));

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void updateMetadataAsSynchronized(String objectType, SyncMetaObject syncObject, boolean ignoreChangeId) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            // Fix for the issue - PC-2229
            if (syncObject.getChangeId().equals(getSynchronizationDao().getChangeId(syncObject.getGuid()))
                    || ignoreChangeId)
            {
                getSynchronizationDao().updateMetadataRecord(objectType, syncObject.getGuid(), syncObject.getVersion(),
                        SyncStatus.SYNCHRONIZED);

                getSynchronizationDao().markAllPropertiesAsSynchronized(syncObject.getGuid());
            } else {
                // If the changeId of the syncObject is not matched with the changeId in the DB
                // we do not update the status as synchronized, keep it as it is.
                getSynchronizationDao().updateMetadataRecord(objectType, syncObject.getGuid(),
                        syncObject.getVersion());
            }

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void deleteMetadata(String objectType, UUID objectId) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            getSynchronizationDao().deleteMetadataRecord(objectType, objectId);
            getSynchronizationDao().deleteAllPropertyMetadataRecords(objectId);

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void deleteCustomEntity(String objectType, UUID objectId) throws SynchronizedStorageException {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            SyncMetaObject metaObject = getSynchronizationDao().getMetadata(objectType, objectId);
            // If the status is deleted, we cannot call the Storage.Delete(), because entity is deleted from the custom
            // tables.
            // If the status is -1, we cannot call the Storage.Delete(), because entity never exist.
            if (metaObject.getStatus() != SyncStatus.DELETED.getValue() && metaObject.getStatus() != -1) {
                getTypeManager().getStorage(objectType).delete(objectId,
                        new DeleteArgs(SynchronizationModule.class.getSimpleName()));
            }

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void insertMetadata(String objectType, UUID objectId, int serverVersion, SyncStatus syncStatus) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            List<SyncObject> syncObjects = new ArrayList<SyncObject>();
            SyncObject syncObject = new SyncObject(objectId, serverVersion);
            syncObjects.add(syncObject);

            insertBulkMetadata(objectType, syncObjects, syncStatus);

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void insertBulkMetadata(String objectType, List<SyncObject> syncObjects, SyncStatus syncStatus) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            getSynchronizationDao().insertMetadataAndPropertyMetadataRecords(objectType, syncObjects,
                    getTypeManager().getProperties(objectType), syncStatus, isEnablePropertySynchronization());

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected void sendRequest(EntityBase syncRequest, RequestArgs requestArgs) throws NotLoggedInException,
            MaxMessageSizeException {

        try {
            getCommunication().send(syncRequest, requestArgs, SendPriority.WHEN_POSSIBLE);

            if (syncRequest instanceof IDebugPrintable) {
                getLogHelper().logDebug(getModuleName(), LogId.SYNC_COM_D_1, syncRequest.getClass().getSimpleName()
                        + " Sent",
                        ((IDebugPrintable) syncRequest).getDebugString());
            }
        } catch (MaxMessageSizeException e) {
            getLogHelper().logError(getModuleName(), LogId.SYNC_COM_E_1, "Exception when calling Communication.send", e);
            throw e;
        }
    }

    private <T extends ISynchronizable> T readEntityFromStorage(String objectType, UUID entityId,
            SynchronizedStorageBase<T> storage) {

        Lock lock = getTypeManager().getLock(objectType);
        if (lock != null) {
            lock.lock();
        }
        try {

            T entity = null;
            List<UUID> syncEntityInfoList = new ArrayList<UUID>();
            syncEntityInfoList.add(entityId);
            try {
                List<ISynchronizable> entities = storage
                        .read(syncEntityInfoList);
                if (entities != null && entities.size() > 0) {
                    entity = (T) entities.get(0);
                }
            } catch (SynchronizedStorageException e) {
                getLogHelper().logError(getModuleName(), LogId.SYNC_SM_E_2,
                        "Exception when reading entity from storage", e);
            }
            return entity;

        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    protected static String combinePartialItems(List<String> partialItemList) {
        StringBuilder combinedItem = new StringBuilder();
        for (String partialEntity : partialItemList) {
            combinedItem.append(partialEntity);
        }
        return combinedItem.toString();
    }

    protected String[] splitSerializedEntity(String entity, int allowedMaxDataPacketSize) {

        int lengthOfEntity = entity.length();
        int numberOfSplits = lengthOfEntity / allowedMaxDataPacketSize;
        if (lengthOfEntity % allowedMaxDataPacketSize > 0) {
            numberOfSplits += 1;
        }

        String[] splits = new String[numberOfSplits];
        for (int i = 0; i < splits.length; i++) {
            int subStringStart = allowedMaxDataPacketSize * i;
            int subStringEnd = allowedMaxDataPacketSize * (i + 1);
            if (subStringEnd < entity.length()) {
                splits[i] = entity.substring(subStringStart, subStringEnd);
            } else {
                splits[i] = entity.substring(subStringStart);
            }
        }
        return splits;
    }

    protected int getCharacterOccurences(String serializedObject) {

        int counter = 0;
        for (int i = 0; i < serializedObject.length(); i++) {
            if (serializedObject.charAt(i) == '"') {
                counter++;
            }
        }
        return counter;
    }
}
