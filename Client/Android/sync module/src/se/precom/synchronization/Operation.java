package se.precom.synchronization;

/**
 * Represents the status of the entity.
 */
public enum Operation {
    /**
     * Entity is not changed.
     */
    NONE,
    /**
     * Entity is created.
     */
    CREATED,
    /**
     * Entity is updated.
     */
    UPDATED,
    /**
     * Entity is deleted.
     */
    DELETED
}
