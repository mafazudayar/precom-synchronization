package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * Strategy used to decide whether a property should be serialized as a part of the JSON output, when the corresponding
 * entity is serialized by the Synchronization Module.
 * 
 */
public class SynchronizationExclusionStrategy implements ExclusionStrategy {

    private ArrayList<String> _propertiesToInclude;
    private Class<?> _type;

    public SynchronizationExclusionStrategy(Class<?> type, List<String> propertiesToInclude) {
    	_type = type;
        _propertiesToInclude = new ArrayList<String>();
        for (String property : propertiesToInclude) {
            _propertiesToInclude.add(property);
        }
    }

    @Override
    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

    @Override
	public boolean shouldSkipField(FieldAttributes f) {
		return _type == f.getDeclaringClass()
				&& !_propertiesToInclude.contains(f.getName());
	}
}
