package se.precom.synchronization;

enum LogId {

    /**
     * Maximum sync packet size setting is out of range.
     */
    SYNC_E_1,
    /**
     * Exception when processing received error message.
     */
    SYNC_D_2,
    /**
     * Communication exception when sending messages.
     */
    SYNC_COM_E_1,
    /**
     * Message sent.
     */
    SYNC_COM_D_1,
    /**
     * Message received.
     */
    SYNC_COM_D_2,
    /**
     * Exception in synchronization manager.
     */
    SYNC_SM_E_1,
    /**
     * Storage exception in synchronization manager.
     */
    SYNC_SM_E_2,
    /**
     * Storage exception in entity update manager.
     */
    SYNC_EU_E_1,
    /**
     * Exception in entity update manager.
     */
    SYNC_EU_E_2,
    /**
     * Exception when an entity is deleted locally.
     */
    SYNC_EU_E_3,
    /**
     * Pull request initiated.
     */
    SYNC_PULL_D_1,
    /**
     * Calling custom storage.
     */
    SYNC_PULL_D_2,
    /**
     * Custom storage responded.
     */
    SYNC_PULL_D_3,
    /**
     * Pull requests sent.
     */
    SYNC_PULL_D_4,
    /**
     * Pull response received.
     */
    SYNC_PULL_D_5,
    /**
     * Pull response completed.
     */
    SYNC_PULL_D_6,
    /**
     * No sync session id found.
     */
    SYNC_PULL_D_9,
    /**
     * Pull invoke received.
     */
    SYNC_PULL_D_10,
    /**
     * Storage exception in pull manager.
     */
    SYNC_PULL_E_1,
    /**
     * Exception in pull manager.
     */
    SYNC_PULL_E_2,
    /**
     * Push request initiated.
     */
    SYNC_PUSH_D_1,
    /**
     * Calling custom storage.
     */
    SYNC_PUSH_D_2,
    /**
     * Custom storage responded.
     */
    SYNC_PUSH_D_3,
    /**
     * Push requests sent.
     */
    SYNC_PUSH_D_4,
    /**
     * Push response received.
     */
    SYNC_PUSH_D_5,
    /**
     * Push response completed.
     */
    SYNC_PUSH_D_6,
    /**
     * Storage exception in push manager.
     */
    SYNC_PUSH_E_1,
    /**
     * Exception in push manager.
     */
    SYNC_PUSH_E_2,
    /**
     * Object not found when pushing.
     */
    SYNC_PUSH_E_3,
    /**
     * No sync session id found.
     */
    SYNC_PUSH_E_4
}
