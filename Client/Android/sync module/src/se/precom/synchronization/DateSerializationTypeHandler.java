package se.precom.synchronization;

import java.lang.reflect.Type;
import java.util.Date;

import se.precom.core.communication.EntitySerializer;

public class DateSerializationTypeHandler implements EntitySerializer.SerializationTypeHandler<Date> {
    @Override
    public String serialize(Date object, Type typeOfT) {
        if (object == null) {
            return null;
        }

        // It seems it is not possible to get time zone from Date.
        // It only supports getting the default time zone of the system so lets not include it.
        // return "/Date(" + source.getTime() + getDefaultTimeZoneInfo() + ")/";
        return "/Date(" + object.getTime() + ")/";
    }

    @Override
    public Date deserialize(String object, Type typeOfT) {
        if (object == null || object.length() == 0) {
            return null;
        }

        long epochMilliseconds;

        int tzIndex = object.indexOf("+");

        if (tzIndex != -1) {
            epochMilliseconds = Long.parseLong(object.substring(6, tzIndex));
        } else {
            tzIndex = object.lastIndexOf("-");
            if (tzIndex > 6) {
                epochMilliseconds = Long.parseLong(object.substring(6, tzIndex));
            } else {
                epochMilliseconds = Long.parseLong(object.substring(6, object.length() - 2));
            }
        }

        return new Date(epochMilliseconds);
    }
}
