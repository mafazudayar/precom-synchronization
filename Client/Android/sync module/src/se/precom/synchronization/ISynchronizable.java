package se.precom.synchronization;

import java.util.UUID;

/**
 * Interface to be implemented by the object types which should be synchronized by the Synchronization Module.
 */
public interface ISynchronizable {

    /**
     * Returns the object Id.
     * 
     * @return the object Id.
     */
    UUID getGuid();

    /**
     * Sets the object Id.
     * 
     * @param guid
     *            object Id.
     */
    void setGuid(UUID guid);

}
