package se.precom.synchronization;

/**
 * Represents the operators used to compare the properties when building the filter query.
 * 
 */
public enum FilterOperator {
    /**
     * Property equal to the value.
     */
    EQUAL_TO(0),
    /**
     * Property not equal to the value.
     */
    NOT_EQUAL_TO(1),
    /**
     * Property greater than the value.
     */
    GREATER_THAN(2),
    /**
     * Property less than the value.
     */
    LESS_THAN(3);

    private int _value;

    private FilterOperator(int value) {
        this._value = value;
    }

    /**
     * Returns the numerical value that this enumeration represents.
     * 
     * @return the numerical value that this enumeration represents.
     */
    int getValue() {
        return _value;
    }
}
