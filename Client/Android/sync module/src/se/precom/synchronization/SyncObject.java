package se.precom.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Contains the metadata and the object to be synchronized.
 */
public class SyncObject extends SyncMetaObject {

	private String Object;
	private List<String> ChangedPropertyNames = new ArrayList<String>();

	/**
	 * Creates a new sync object.
	 * 
	 * @param objectId
	 *            the object Id.
	 * @param version
	 *            the version of the object.
	 * @param object
	 *            the serialized object.
	 */
	public SyncObject(UUID objectId, int version, String object) {
		super(objectId, version);
		setObject(object);
	}

	/**
	 * Creates a new sync object.
	 * 
	 * @param objectId
	 *            the object Id.
	 * @param version
	 *            the version of the object.
	 */
	public SyncObject(UUID objectId, int version) {
		super(objectId, version);
	}

	/**
	 * Creates a new sync object
	 * 
	 * @param syncMetaObject
	 *            the {@link SyncMetaObject} containing the GUID , version and
	 *            the status of the object.
	 */
	public SyncObject(SyncMetaObject syncMetaObject) {
		super(syncMetaObject.getGuid(), syncMetaObject.getVersion());
		setStatus(syncMetaObject.getStatus());
	}

	/**
	 * Returns the serialized object.
	 * 
	 * @return the serialized object.
	 */
	public String getObject() {
		return Object;
	}

	/**
	 * Sets the serialized object.
	 * 
	 * @param object
	 *            the serialized object.
	 */
	public void setObject(String object) {
		Object = object;
	}

	/**
	 * Returns the list of changed property names.
	 * 
	 * @return the list of changed property names.
	 */
	public List<String> getChangedPropertyNames() {
		return ChangedPropertyNames;
	}
}
