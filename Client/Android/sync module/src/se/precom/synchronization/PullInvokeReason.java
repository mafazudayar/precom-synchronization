package se.precom.synchronization;

/**
 * 
 * Represents the reason for the server push.
 * 
 */
public enum PullInvokeReason {
    /**
     * Server initiates a push due to a client login.
     */
    CLIENT_LOGIN(0),
    /**
     * Server initiates a push due to a automatic push.
     */
    AUTOMATIC_PUSH(1),
    /**
     * Server initiates a push due to a manual push.
     */
    MANUAL_PUSH(2);

    private final int _value;

    PullInvokeReason(int value) {
        this._value = value;
    }

    int getValue() {
        return _value;
    }

    static PullInvokeReason fromInt(int invokeReason) {

        for (PullInvokeReason reason : PullInvokeReason.values()) {
            if (invokeReason == reason.getValue()) {
                return reason;
            }
        }

        throw new IllegalArgumentException("Invalid PullInvokeReason");
    }
}
