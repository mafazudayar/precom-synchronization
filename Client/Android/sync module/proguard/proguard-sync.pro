# When building with ANT the output is named classes.jar instead of <project name>.jar for some reason.
-injars precom-synchronization-android.jar
-outjars precom-synchronization-android-obfuscated.jar

-libraryjars 'C:\Program Files\Android\android-sdk\platforms\android-16\android.jar'
-libraryjars ..\..\..\..\ext\PreCom-Android-Framework-3.8.0.1\PreCom\libs\gson-2.1.jar
-libraryjars ..\..\..\..\ext\PreCom-Android-Framework-3.8.0.1\PreCom\libs\precom-android-3.8.0.1.jar
-libraryjars ..\..\..\..\ext\PreCom-Android-Framework-3.8.0.1\PreComUI\libs\precom-ui-android-3.8.0.1.jar

-printmapping sync_mapping.txt

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontpreverify
-dontoptimize
-dontshrink
-verbose
-keepparameternames
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,
                SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

# Saves class name and method names for public methods, everything else is obfuscated.
-keep public class se.precom.synchronization.** {
     public protected *;
}

-keep enum se.precom.synchronization.** {
     *;
}

# We need to keep all classes that are sent over communication. It is easy to
# keep all EntityBase classes as below, but we must also find all other classes
# that are used as fields by the EntityBase classes.

# Important to add new classes that are used for communication here!!

-keep class se.precom.synchronization.** extends se.precom.core.communication.EntityBase {
	*;
}

-keep class se.precom.synchronization.SyncObject {
	*;
}

-keep class se.precom.synchronization.EntityFilter {
	*;
}

-keep class se.precom.synchronization.Filter {
	*;
}

-keep class se.precom.synchronization.SyncMetaObject {
	*;
}

-keep class se.precom.synchronization.SyncObject {
	*;
}

# Might need this if we have interface in seperate files.
-keep public interface se.precom.synchronization.** {
      *;
}
