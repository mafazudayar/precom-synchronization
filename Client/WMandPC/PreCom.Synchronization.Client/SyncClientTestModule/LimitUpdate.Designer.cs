﻿namespace SyncClientTestModule
{
    partial class LimitUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLinitId = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.chkAssign = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtReportID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtEditors = new System.Windows.Forms.TextBox();
            this.txtAddEditor = new System.Windows.Forms.TextBox();
            this.btnAddEditor = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.txtCreator = new PreCom.Controls.PreComInput2();
            this.txtDesc = new PreCom.Controls.PreComInput2();
            this.txtReportTitle = new PreCom.Controls.PreComInput2();
            this.txtReportContent = new PreCom.Controls.PreComInput2();
            this.SuspendLayout();
            // 
            // lblLinitId
            // 
            this.lblLinitId.Location = new System.Drawing.Point(0, 0);
            this.lblLinitId.Name = "lblLinitId";
            this.lblLinitId.Size = new System.Drawing.Size(240, 20);
            this.lblLinitId.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(193, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 20);
            this.button1.TabIndex = 1;
            this.button1.Text = "Close";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.Text = "Creator";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.Text = "Descrip";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(4, 106);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(148, 22);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // chkAssign
            // 
            this.chkAssign.Location = new System.Drawing.Point(170, 108);
            this.chkAssign.Name = "chkAssign";
            this.chkAssign.Size = new System.Drawing.Size(67, 20);
            this.chkAssign.TabIndex = 8;
            this.chkAssign.Text = "Assign";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 20);
            this.label3.Text = "R-Title";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 21);
            this.label4.Text = "Content";
            // 
            // txtReportID
            // 
            this.txtReportID.Location = new System.Drawing.Point(60, 245);
            this.txtReportID.Name = "txtReportID";
            this.txtReportID.Size = new System.Drawing.Size(44, 21);
            this.txtReportID.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(14, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 18);
            this.label5.Text = "R - ID";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(107, 246);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 20);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtEditors
            // 
            this.txtEditors.Location = new System.Drawing.Point(71, 206);
            this.txtEditors.Name = "txtEditors";
            this.txtEditors.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtEditors.Size = new System.Drawing.Size(166, 21);
            this.txtEditors.TabIndex = 19;
            // 
            // txtAddEditor
            // 
            this.txtAddEditor.Location = new System.Drawing.Point(98, 183);
            this.txtAddEditor.Name = "txtAddEditor";
            this.txtAddEditor.Size = new System.Drawing.Size(139, 21);
            this.txtAddEditor.TabIndex = 20;
            // 
            // btnAddEditor
            // 
            this.btnAddEditor.Location = new System.Drawing.Point(4, 182);
            this.btnAddEditor.Name = "btnAddEditor";
            this.btnAddEditor.Size = new System.Drawing.Size(88, 20);
            this.btnAddEditor.TabIndex = 21;
            this.btnAddEditor.Text = "Add Editor";
            this.btnAddEditor.Click += new System.EventHandler(this.btnAddEditor_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(4, 208);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(61, 20);
            this.btnRemove.TabIndex = 22;
            this.btnRemove.Text = "Remove";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // txtCreator
            // 
            this.txtCreator.Location = new System.Drawing.Point(61, 21);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.Size = new System.Drawing.Size(176, 21);
            this.txtCreator.TabIndex = 29;
            this.txtCreator.TextTranslation = false;
            this.txtCreator.Click += new System.EventHandler(this.txtCreator_Click);
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(61, 46);
            this.txtDesc.Multiline = true;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDesc.Size = new System.Drawing.Size(176, 56);
            this.txtDesc.TabIndex = 30;
            this.txtDesc.TextTranslation = false;
            this.txtDesc.Click += new System.EventHandler(this.txtDesc_Click);
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.Location = new System.Drawing.Point(61, 132);
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Size = new System.Drawing.Size(176, 21);
            this.txtReportTitle.TabIndex = 31;
            this.txtReportTitle.TextTranslation = false;
            this.txtReportTitle.Click += new System.EventHandler(this.txtReportTitle_Click);
            // 
            // txtReportContent
            // 
            this.txtReportContent.Location = new System.Drawing.Point(60, 155);
            this.txtReportContent.Name = "txtReportContent";
            this.txtReportContent.Size = new System.Drawing.Size(177, 21);
            this.txtReportContent.TabIndex = 32;
            this.txtReportContent.TextTranslation = false;
            this.txtReportContent.Click += new System.EventHandler(this.txtReportContent_Click);
            // 
            // LimitUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.txtReportContent);
            this.Controls.Add(this.txtReportTitle);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.txtCreator);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAddEditor);
            this.Controls.Add(this.txtAddEditor);
            this.Controls.Add(this.txtEditors);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtReportID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkAssign);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblLinitId);
            this.Name = "LimitUpdate";
            this.Size = new System.Drawing.Size(240, 280);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLinitId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox chkAssign;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtReportID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtEditors;
        private System.Windows.Forms.TextBox txtAddEditor;
        private System.Windows.Forms.Button btnAddEditor;
        private System.Windows.Forms.Button btnRemove;
        private PreCom.Controls.PreComInput2 txtCreator;
        private PreCom.Controls.PreComInput2 txtDesc;
        private PreCom.Controls.PreComInput2 txtReportTitle;
        private PreCom.Controls.PreComInput2 txtReportContent;
    }
}