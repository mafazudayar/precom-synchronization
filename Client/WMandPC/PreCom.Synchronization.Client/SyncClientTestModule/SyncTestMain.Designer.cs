﻿namespace SyncClientTestModule
{
    partial class SyncTestMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPullMarket = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDeleteLimitOrderServer = new System.Windows.Forms.Button();
            this.textBox1 = new PreCom.Controls.PreComInput2();
            this.btnUpdateLimitOrderServer = new System.Windows.Forms.Button();
            this.chkLarge = new System.Windows.Forms.CheckBox();
            this.btnDeleteAllServer = new System.Windows.Forms.Button();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.btnCreateMarketOrderServer = new System.Windows.Forms.Button();
            this.btnCreateLimitOrderServer = new System.Windows.Forms.Button();
            this.btnCreateMarketOrderClient = new System.Windows.Forms.Button();
            this.btnCreateLimitOrder = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnServerPushAPIArgumentTest = new System.Windows.Forms.Button();
            this.chkFilterClientPull = new System.Windows.Forms.CheckBox();
            this.chkFilterServerPush = new System.Windows.Forms.CheckBox();
            this.btnDeleteLocallyLimit = new System.Windows.Forms.Button();
            this.btnPushMarket = new System.Windows.Forms.Button();
            this.btnPush = new System.Windows.Forms.Button();
            this.btnServerPushLimit = new System.Windows.Forms.Button();
            this.btnServerPushMarket = new System.Windows.Forms.Button();
            this.btnPullAllLimit = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnLoadMarket = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ID = new System.Windows.Forms.ColumnHeader();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelCount = new System.Windows.Forms.Label();
            this.btnDeleteLimit = new System.Windows.Forms.Button();
            this.btnUpdateLimit = new System.Windows.Forms.Button();
            this.btnDisplayLimit = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnBulkDelete = new System.Windows.Forms.Button();
            this.btnBulkUpdate = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPullMarket
            // 
            this.btnPullMarket.Location = new System.Drawing.Point(78, 7);
            this.btnPullMarket.Name = "btnPullMarket";
            this.btnPullMarket.Size = new System.Drawing.Size(144, 20);
            this.btnPullMarket.TabIndex = 0;
            this.btnPullMarket.Text = "Pull - Market";
            this.btnPullMarket.Click += new System.EventHandler(this.buttonPull_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 266);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDeleteLimitOrderServer);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.btnUpdateLimitOrderServer);
            this.tabPage1.Controls.Add(this.chkLarge);
            this.tabPage1.Controls.Add(this.btnDeleteAllServer);
            this.tabPage1.Controls.Add(this.btnDeleteAll);
            this.tabPage1.Controls.Add(this.btnCreateMarketOrderServer);
            this.tabPage1.Controls.Add(this.btnCreateLimitOrderServer);
            this.tabPage1.Controls.Add(this.btnCreateMarketOrderClient);
            this.tabPage1.Controls.Add(this.btnCreateLimitOrder);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 243);
            this.tabPage1.Text = "Operations";
            // 
            // btnDeleteLimitOrderServer
            // 
            this.btnDeleteLimitOrderServer.Location = new System.Drawing.Point(17, 191);
            this.btnDeleteLimitOrderServer.Name = "btnDeleteLimitOrderServer";
            this.btnDeleteLimitOrderServer.Size = new System.Drawing.Size(196, 20);
            this.btnDeleteLimitOrderServer.TabIndex = 10;
            this.btnDeleteLimitOrderServer.Text = "Delete Limit Order - Server";
            this.btnDeleteLimitOrderServer.Click += new System.EventHandler(this.btnDeleteLimitOrderServer_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(17, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(196, 21);
            this.textBox1.TabIndex = 9;
            this.textBox1.TextTranslation = false;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // btnUpdateLimitOrderServer
            // 
            this.btnUpdateLimitOrderServer.Location = new System.Drawing.Point(17, 165);
            this.btnUpdateLimitOrderServer.Name = "btnUpdateLimitOrderServer";
            this.btnUpdateLimitOrderServer.Size = new System.Drawing.Size(196, 20);
            this.btnUpdateLimitOrderServer.TabIndex = 8;
            this.btnUpdateLimitOrderServer.Text = "Update Limit Order - Server";
            this.btnUpdateLimitOrderServer.Click += new System.EventHandler(this.btnUpdateLimitOrderServer_Click);
            // 
            // chkLarge
            // 
            this.chkLarge.Location = new System.Drawing.Point(17, 33);
            this.chkLarge.Name = "chkLarge";
            this.chkLarge.Size = new System.Drawing.Size(100, 20);
            this.chkLarge.TabIndex = 7;
            this.chkLarge.Text = "Large Objects";
            // 
            // btnDeleteAllServer
            // 
            this.btnDeleteAllServer.Location = new System.Drawing.Point(123, 220);
            this.btnDeleteAllServer.Name = "btnDeleteAllServer";
            this.btnDeleteAllServer.Size = new System.Drawing.Size(110, 20);
            this.btnDeleteAllServer.TabIndex = 6;
            this.btnDeleteAllServer.Text = "Clear - Server";
            this.btnDeleteAllServer.Click += new System.EventHandler(this.btnDeleteAllServer_Click);
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Location = new System.Drawing.Point(7, 220);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(110, 20);
            this.btnDeleteAll.TabIndex = 5;
            this.btnDeleteAll.Text = "Clear - Client";
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            // 
            // btnCreateMarketOrderServer
            // 
            this.btnCreateMarketOrderServer.Location = new System.Drawing.Point(17, 113);
            this.btnCreateMarketOrderServer.Name = "btnCreateMarketOrderServer";
            this.btnCreateMarketOrderServer.Size = new System.Drawing.Size(196, 20);
            this.btnCreateMarketOrderServer.TabIndex = 3;
            this.btnCreateMarketOrderServer.Text = "Create Market Order - Server";
            this.btnCreateMarketOrderServer.Click += new System.EventHandler(this.btnCreateMarketOrderServer_Click);
            // 
            // btnCreateLimitOrderServer
            // 
            this.btnCreateLimitOrderServer.Location = new System.Drawing.Point(17, 139);
            this.btnCreateLimitOrderServer.Name = "btnCreateLimitOrderServer";
            this.btnCreateLimitOrderServer.Size = new System.Drawing.Size(196, 20);
            this.btnCreateLimitOrderServer.TabIndex = 2;
            this.btnCreateLimitOrderServer.Text = "Create Limit Order - Server";
            this.btnCreateLimitOrderServer.Click += new System.EventHandler(this.btnCreateLimitOrderServer_Click);
            // 
            // btnCreateMarketOrderClient
            // 
            this.btnCreateMarketOrderClient.Location = new System.Drawing.Point(17, 58);
            this.btnCreateMarketOrderClient.Name = "btnCreateMarketOrderClient";
            this.btnCreateMarketOrderClient.Size = new System.Drawing.Size(182, 20);
            this.btnCreateMarketOrderClient.TabIndex = 1;
            this.btnCreateMarketOrderClient.Text = "Create Market Order - Client";
            this.btnCreateMarketOrderClient.Click += new System.EventHandler(this.btnCreateMarketOrderClient_Click);
            // 
            // btnCreateLimitOrder
            // 
            this.btnCreateLimitOrder.Location = new System.Drawing.Point(17, 83);
            this.btnCreateLimitOrder.Name = "btnCreateLimitOrder";
            this.btnCreateLimitOrder.Size = new System.Drawing.Size(182, 20);
            this.btnCreateLimitOrder.TabIndex = 0;
            this.btnCreateLimitOrder.Text = "Create Limit Order - Client";
            this.btnCreateLimitOrder.Click += new System.EventHandler(this.btnCreateLimitOrder_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnServerPushAPIArgumentTest);
            this.tabPage2.Controls.Add(this.chkFilterClientPull);
            this.tabPage2.Controls.Add(this.chkFilterServerPush);
            this.tabPage2.Controls.Add(this.btnDeleteLocallyLimit);
            this.tabPage2.Controls.Add(this.btnPushMarket);
            this.tabPage2.Controls.Add(this.btnPush);
            this.tabPage2.Controls.Add(this.btnServerPushLimit);
            this.tabPage2.Controls.Add(this.btnServerPushMarket);
            this.tabPage2.Controls.Add(this.btnPullAllLimit);
            this.tabPage2.Controls.Add(this.btnPullMarket);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(240, 243);
            this.tabPage2.Text = "API";
            // 
            // btnServerPushAPIArgumentTest
            // 
            this.btnServerPushAPIArgumentTest.Location = new System.Drawing.Point(7, 187);
            this.btnServerPushAPIArgumentTest.Name = "btnServerPushAPIArgumentTest";
            this.btnServerPushAPIArgumentTest.Size = new System.Drawing.Size(213, 20);
            this.btnServerPushAPIArgumentTest.TabIndex = 9;
            this.btnServerPushAPIArgumentTest.Text = "Server Push  API Arugument Test";
            this.btnServerPushAPIArgumentTest.Click += new System.EventHandler(this.btnServerPushAPIArgumentTest_Click);
            // 
            // chkFilterClientPull
            // 
            this.chkFilterClientPull.Location = new System.Drawing.Point(7, 33);
            this.chkFilterClientPull.Name = "chkFilterClientPull";
            this.chkFilterClientPull.Size = new System.Drawing.Size(63, 20);
            this.chkFilterClientPull.TabIndex = 8;
            this.chkFilterClientPull.Text = "Filter";
            // 
            // chkFilterServerPush
            // 
            this.chkFilterServerPush.Location = new System.Drawing.Point(7, 164);
            this.chkFilterServerPush.Name = "chkFilterServerPush";
            this.chkFilterServerPush.Size = new System.Drawing.Size(63, 20);
            this.chkFilterServerPush.TabIndex = 7;
            this.chkFilterServerPush.Text = "Filter";
            // 
            // btnDeleteLocallyLimit
            // 
            this.btnDeleteLocallyLimit.Location = new System.Drawing.Point(78, 213);
            this.btnDeleteLocallyLimit.Name = "btnDeleteLocallyLimit";
            this.btnDeleteLocallyLimit.Size = new System.Drawing.Size(144, 20);
            this.btnDeleteLocallyLimit.TabIndex = 6;
            this.btnDeleteLocallyLimit.Text = "Delete Locally - Limit";
            this.btnDeleteLocallyLimit.Click += new System.EventHandler(this.btnDeleteLocallyLimit_Click);
            // 
            // btnPushMarket
            // 
            this.btnPushMarket.Location = new System.Drawing.Point(78, 69);
            this.btnPushMarket.Name = "btnPushMarket";
            this.btnPushMarket.Size = new System.Drawing.Size(144, 20);
            this.btnPushMarket.TabIndex = 5;
            this.btnPushMarket.Text = "Push - Market";
            this.btnPushMarket.Click += new System.EventHandler(this.btnPushMarket_Click);
            // 
            // btnPush
            // 
            this.btnPush.Location = new System.Drawing.Point(78, 95);
            this.btnPush.Name = "btnPush";
            this.btnPush.Size = new System.Drawing.Size(144, 20);
            this.btnPush.TabIndex = 4;
            this.btnPush.Text = "Push - Limit";
            this.btnPush.Click += new System.EventHandler(this.btnPush_Click);
            // 
            // btnServerPushLimit
            // 
            this.btnServerPushLimit.Location = new System.Drawing.Point(78, 163);
            this.btnServerPushLimit.Name = "btnServerPushLimit";
            this.btnServerPushLimit.Size = new System.Drawing.Size(144, 20);
            this.btnServerPushLimit.TabIndex = 3;
            this.btnServerPushLimit.Text = "Server Push - Limit";
            this.btnServerPushLimit.Click += new System.EventHandler(this.btnServerPushLimit_Click);
            // 
            // btnServerPushMarket
            // 
            this.btnServerPushMarket.Location = new System.Drawing.Point(78, 137);
            this.btnServerPushMarket.Name = "btnServerPushMarket";
            this.btnServerPushMarket.Size = new System.Drawing.Size(144, 20);
            this.btnServerPushMarket.TabIndex = 2;
            this.btnServerPushMarket.Text = "Server Push - Market";
            // 
            // btnPullAllLimit
            // 
            this.btnPullAllLimit.Location = new System.Drawing.Point(78, 33);
            this.btnPullAllLimit.Name = "btnPullAllLimit";
            this.btnPullAllLimit.Size = new System.Drawing.Size(144, 20);
            this.btnPullAllLimit.TabIndex = 1;
            this.btnPullAllLimit.Text = "Pull - Limit";
            this.btnPullAllLimit.Click += new System.EventHandler(this.btnPullAllLimit_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnLoadMarket);
            this.tabPage3.Controls.Add(this.listView1);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(232, 240);
            this.tabPage3.Text = "Market";
            // 
            // btnLoadMarket
            // 
            this.btnLoadMarket.Location = new System.Drawing.Point(8, 4);
            this.btnLoadMarket.Name = "btnLoadMarket";
            this.btnLoadMarket.Size = new System.Drawing.Size(112, 20);
            this.btnLoadMarket.TabIndex = 1;
            this.btnLoadMarket.Text = "Display - Market";
            this.btnLoadMarket.Click += new System.EventHandler(this.btnLoadMarket_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.Add(this.ID);
            this.listView1.Columns.Add(this.columnHeader1);
            this.listView1.Columns.Add(this.columnHeader2);
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(7, 26);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(226, 225);
            this.listView1.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.Text = "GUID";
            this.ID.Width = 120;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Version";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Staus";
            this.columnHeader2.Width = 50;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.labelCount);
            this.tabPage4.Controls.Add(this.btnDeleteLimit);
            this.tabPage4.Controls.Add(this.btnUpdateLimit);
            this.tabPage4.Controls.Add(this.btnDisplayLimit);
            this.tabPage4.Controls.Add(this.listView2);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(232, 240);
            this.tabPage4.Text = "Limit";
            // 
            // labelCount
            // 
            this.labelCount.Location = new System.Drawing.Point(0, 4);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(62, 20);
            // 
            // btnDeleteLimit
            // 
            this.btnDeleteLimit.Location = new System.Drawing.Point(181, 4);
            this.btnDeleteLimit.Name = "btnDeleteLimit";
            this.btnDeleteLimit.Size = new System.Drawing.Size(52, 20);
            this.btnDeleteLimit.TabIndex = 5;
            this.btnDeleteLimit.Text = "Delete";
            this.btnDeleteLimit.Click += new System.EventHandler(this.btnDeleteLimit_Click);
            // 
            // btnUpdateLimit
            // 
            this.btnUpdateLimit.Location = new System.Drawing.Point(122, 4);
            this.btnUpdateLimit.Name = "btnUpdateLimit";
            this.btnUpdateLimit.Size = new System.Drawing.Size(56, 20);
            this.btnUpdateLimit.TabIndex = 4;
            this.btnUpdateLimit.Text = "Update";
            this.btnUpdateLimit.Click += new System.EventHandler(this.btnUpdateLimit_Click);
            // 
            // btnDisplayLimit
            // 
            this.btnDisplayLimit.Location = new System.Drawing.Point(64, 4);
            this.btnDisplayLimit.Name = "btnDisplayLimit";
            this.btnDisplayLimit.Size = new System.Drawing.Size(56, 20);
            this.btnDisplayLimit.TabIndex = 3;
            this.btnDisplayLimit.Text = "Refresh";
            this.btnDisplayLimit.Click += new System.EventHandler(this.btnDisplayLimit_Click);
            // 
            // listView2
            // 
            this.listView2.Columns.Add(this.columnHeader3);
            this.listView2.Columns.Add(this.columnHeader4);
            this.listView2.Columns.Add(this.columnHeader5);
            this.listView2.FullRowSelect = true;
            this.listView2.Location = new System.Drawing.Point(7, 26);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(226, 215);
            this.listView2.TabIndex = 2;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "GUID";
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Version";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Staus";
            this.columnHeader5.Width = 50;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnBulkDelete);
            this.tabPage5.Controls.Add(this.btnBulkUpdate);
            this.tabPage5.Location = new System.Drawing.Point(0, 0);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(232, 240);
            this.tabPage5.Text = "Performance";
            // 
            // btnBulkDelete
            // 
            this.btnBulkDelete.Location = new System.Drawing.Point(42, 76);
            this.btnBulkDelete.Name = "btnBulkDelete";
            this.btnBulkDelete.Size = new System.Drawing.Size(144, 20);
            this.btnBulkDelete.TabIndex = 2;
            this.btnBulkDelete.Text = "Bulk Delete - Limit";
            this.btnBulkDelete.Click += new System.EventHandler(this.btnBulkDelete_Click);
            // 
            // btnBulkUpdate
            // 
            this.btnBulkUpdate.Location = new System.Drawing.Point(42, 30);
            this.btnBulkUpdate.Name = "btnBulkUpdate";
            this.btnBulkUpdate.Size = new System.Drawing.Size(144, 20);
            this.btnBulkUpdate.TabIndex = 1;
            this.btnBulkUpdate.Text = "Bulk Update - Limit";
            this.btnBulkUpdate.Click += new System.EventHandler(this.btnBulkUpdate_Click);
            // 
            // SyncTestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tabControl1);
            this.Name = "SyncTestMain";
            this.Size = new System.Drawing.Size(240, 280);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPullMarket;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnCreateLimitOrder;
        private System.Windows.Forms.Button btnCreateMarketOrderServer;
        private System.Windows.Forms.Button btnCreateLimitOrderServer;
        private System.Windows.Forms.Button btnCreateMarketOrderClient;
        private System.Windows.Forms.Button btnDeleteAll;
        private System.Windows.Forms.Button btnPullAllLimit;
        private System.Windows.Forms.Button btnDeleteAllServer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnLoadMarket;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnDisplayLimit;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.CheckBox chkLarge;
        private System.Windows.Forms.Button btnServerPushLimit;
        private System.Windows.Forms.Button btnServerPushMarket;
        private System.Windows.Forms.Button btnPush;
        private System.Windows.Forms.Button btnDeleteLimit;
        private System.Windows.Forms.Button btnUpdateLimit;
        private System.Windows.Forms.Button btnPushMarket;
        private System.Windows.Forms.Button btnDeleteLocallyLimit;
        private System.Windows.Forms.Button btnUpdateLimitOrderServer;
        private PreCom.Controls.PreComInput2 textBox1;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnBulkUpdate;
        private System.Windows.Forms.Button btnBulkDelete;
        private System.Windows.Forms.Button btnDeleteLimitOrderServer;
        private System.Windows.Forms.CheckBox chkFilterServerPush;
        private System.Windows.Forms.CheckBox chkFilterClientPull;
        private System.Windows.Forms.Button btnServerPushAPIArgumentTest;
    }
}
