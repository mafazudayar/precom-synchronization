﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using PreCom.Controls;
using PreCom.Core.Communication;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;
using SyncTestModule;
using Application = PreCom.Application;

namespace SyncClientTestModule
{
    public partial class SyncTestMain : PreCom.Controls.PreComForm
    {
        #region Fields

        /// <summary>
        /// The PreCom module instance.
        /// </summary>
        private PreComModule _module;

        #endregion Fields

        #region Constructor(s)

        /// <summary>
        /// Initializes a new instance of the <see cref="PreComModuleForm"/> class.
        /// </summary>
        /// <param name="module">The PreCom module.</param>
        public SyncTestMain(PreComModule module)
        {
            _module = module;

            InitializeComponent();
        }

        #endregion Constructor(s)

        private void buttonPull_Click(object sender, EventArgs e)
        {
            PullArgs pullargs = new PullArgs(new EntityFilter());
            Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };

            //pullargs.Filter.FilterList.Add(f);
            var sw = Stopwatch.StartNew();
            var result = _module.SynchronizationModule.Pull<MarketOrder>(pullargs);
            sw.Stop();
            var args = new ModalMessageBoxArgument("Pull Market",
                                                   string.Format("Pull sync id: {0} {1} ms",
                                                                 result.SyncSession,
                                                                 sw.ElapsedMilliseconds),
                                                   new ModalMessageBoxButton("OK"));
            args.AutoCloseAfterTimeout = new TimeSpan(0, 0, 0, 4);
            ModalMessageBox.Show(args);
        }

        private void btnPullAllLimit_Click(object sender, EventArgs e)
        {
            PullArgs pullargs = new PullArgs(new EntityFilter());

            if (chkFilterClientPull.Checked)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Kaushalya" };
                Filter f2 = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Description", Value = "AAA" };

                pullargs.Filter.FilterList.Add(f);
                pullargs.Filter.FilterList.Add(f2);
            }
            else
            {
                pullargs.Filter = new EntityFilter();
            }

            var sw = Stopwatch.StartNew();
            var result = _module.SynchronizationModule.Pull<LimitOrder>(pullargs);
            sw.Stop();
            var args = new ModalMessageBoxArgument("Pull Limit",
                                                   string.Format("Pull sync id: {0} {1} ms",
                                                                 result.SyncSession,
                                                                 sw.ElapsedMilliseconds),
                                                   new ModalMessageBoxButton("OK"));
            args.AutoCloseAfterTimeout = new TimeSpan(0, 0, 0, 4);
            ModalMessageBox.Show(args);
        }

        private LimitOrder CreateLimitOrder(string creater, string description)
        {
            LimitOrder mo = new LimitOrder();

            mo.Guid = Guid.NewGuid();
            mo.IsAssigned = true;
            mo.Date = DateTime.Now;
            mo.Description = description;
            mo.Creator = creater;
            mo.Image = new ComparableArray();
            mo.Report = new Report();
            mo.Report.Content = "Report Content";
            mo.Report.Title = "Report Title";
            mo.Report.Id = 1;
            mo.Report.Editors = new List<Editor>();
            mo.Report.Editors.Add(new Editor { EditorName = "AAA" });
            mo.Report.Editors.Add(new Editor { EditorName = "BBB" });
            mo.Report.Editors.Add(new Editor { EditorName = "CCC" });
            return mo;
        }

        private LimitOrder CreateLimitOrder(string creater, string description, byte[] img)
        {
            LimitOrder mo = new LimitOrder();

            mo.Guid = Guid.NewGuid();
            mo.IsAssigned = true;
            mo.Date = DateTime.Now;
            mo.Description = description;
            mo.Creator = creater;
            mo.Report = new Report();
            mo.Report.Content = "Report Content";
            mo.Report.Title = "Report Title";
            mo.Report.Id = 1;
            mo.Image = new ComparableArray { Content = img };
            mo.Report.Editors = new List<Editor>();
            mo.Report.Editors.Add(new Editor { EditorName = "AAA" });
            mo.Report.Editors.Add(new Editor { EditorName = "BBB" });
            mo.Report.Editors.Add(new Editor { EditorName = "CCC" });
            return mo;
        }

        private void btnCreateLimitOrder_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            LimitOrder order;

            for (int i = 0; i < a; i++)
            {
                if (chkLarge.Checked)
                {
                    StringBuilder sb = new StringBuilder();

                    while (sb.Length < 50001)
                    {
                        //sb.Append((char) new Random().Next(65, 90));
                        sb.Append("XLLLYYYYYYYYYYYMMMMMMMMMMBBBBBBBBBBBOOOOOOOOOOOCCCCCCCCCCCPPPPPPPPPPPDDDDDDDDDDDQQQQQQQQQQQEEEEEEEEERRRRRRRRRRRFFFFFFFSSSSSSSSSGGGGGGGGGGGTTTTTTTTTT" +
                            "HHHHHHHUUUUUUUUUUUIIIIIIIIIIVVVVVVVVVVVJJJJJJJJJJWWWWWWWWWWWLLLLLLLLLLLYYYYYYYYYYMMMMMMMMMMMAAAAAAAAAAANNNNNNNNNNBBBBBBBOOOOOOOOOOOOCCCCCCCCCCPPPPPPPPPPP" +
                            "DDDDDDDDDDDQQQQQQQQQQQEEEEEEEEEEERRRRRRRRRRRFFFFFFFFFFFSSSSSSSSSSSGGGGGGGGGGUUUUUUUUUUUIIIIIIIIIIVVVVVVVVVVVJJJJJJJJJJJWWWWWWWWWKKKKKKKKKKKXXXXXXXXXXLLLLL" +
                            "LLLLLLYYYYYYYYYYYYMMMMMMMMMMAAAAAAAAAAANNNNNNNNNNNBBBBBBBBBBBOOOOOOOOOOOCCCCCCCCCCCPPPPPPPPPPPDDDDDDDDDDDRRRRRRRRRRFFFFFFFFFFFSSSSSSSSSSSGGGGGGGGGGGTTTTTT" +
                            "TTTTTHHHHHHHHHUUUUUUUUUUUIIIIIIIIIVVVVVVVVVVVJJJJJJJJJJWWWWWWWWWWWKKKKKKKKKKKXXXXXXXXXXXLLLLLLLLLLLYYYYYYYYYYYMMMMMMMMMMMBBBBBBBBBBBOOOOOOOOOOOCCCCCCCCCC");
                    }


                    order = CreateLimitOrder(GetCreator(), "Hello", Encoding.UTF8.GetBytes(sb.ToString()));
                }

                else
                {
                    order = CreateLimitOrder(GetCreator(), GetDescription(i));
                }
                //try {
                _module.LimitOrderManager.Create(order, new PreCom.Synchronization.CreateArgs(GetType().Name));
                //} catch (SynchronizedStorageException e) {

                //}
            }
        }

        private void btnCreateLimitOrderServer_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            OrderInfo orderinf = new OrderInfo { NumberOfOrders = a, OrderType = "LimitOrder", Action = "Insert", IsLarge = chkLarge.Checked };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void btnCreateMarketOrderClient_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            for (int i = 0; i < a; i++)
            {
                MarketOrder order = CreateMarketOrder(GetCreator(), GetDescription(i));
                //try {
                _module.MarketOrderManager.Create(order, new PreCom.Synchronization.CreateArgs(GetType().Name));
                //} catch (SynchronizedStorageException e) {

                //}
            }
        }

        public MarketOrder CreateMarketOrder(string creater, string description)
        {
            MarketOrder order = new MarketOrder();
            Guid id = Guid.NewGuid();
            order.Guid = id;
            order.Creator = creater;
            order.Description = description;
            order.Date = DateTime.Now;
            order.IsAssigned = false;

            return order;
        }

        private String GetCreator()
        {

            Random rn = new Random();
            int i = rn.Next(10);

            if (i % 3 == 1)
            {
                return "Kaushalya";
            }
            else if (i % 3 == 2)
            {
                return "Asanka";
            }
            else if (i % 3 == 0)
            {
                return "Prasad";
            }
            else
            {
                return "Kaushalya";
            }
        }

        private String GetDescription(int i)
        {

            if (i % 3 == 1)
            {
                return "Computer Parts";
            }
            else if (i % 3 == 2)
            {
                return "Food items";
            }
            else if (i % 3 == 0)
            {
                return "Vehicals Parts";
            }
            else
            {
                return "Books";
            }
        }

        private void btnCreateMarketOrderServer_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            OrderInfo orderinf = new OrderInfo { NumberOfOrders = a, OrderType = "MarketOrder", Action = "Insert" };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void btnDeleteAllServer_Click(object sender, EventArgs e)
        {
            OrderInfo orderinf = new OrderInfo { Action = "Delete" };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            try
            {

                // This method simply delete clear all the client records.
                LimitOrderDao dao = new LimitOrderDao();
                dao.Delete();
                dao.DeleteMetadata();
                dao.DeletePropertyMetadata();

                _module.MarketOrderManager.Delete();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnLoadMarket_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView1.View = View.Details;

            foreach (MarketOrder o in _module.MarketOrderManager.Read(new EntityFilter()))
            {
                ListViewItem item1 = new ListViewItem();

                VersionStatus vs = GetStausVersion(o.Guid.ToString());
                item1.Text = o.Guid.ToString();
                item1.SubItems.Add(vs.Version.ToString());
                item1.SubItems.Add(vs.Staus.ToString());
                listView1.Items.Add(item1);
            }
        }

        private VersionStatus GetStausVersion(string id)
        {
            string commandString = " SELECT [EntityVersion],[EntityStatus] from [PMC_Synchronization_Metadata] "
           + " WHERE [EntityId] = '" + id + "'; ";

            VersionStatus vs = new VersionStatus();

            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                try
                {
                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(commandString))
                    {
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            try
                            {
                                if (reader.Read())
                                {
                                    vs.Staus = Convert.ToInt32(reader["EntityStatus"]);
                                    vs.Version = Convert.ToInt32(reader["EntityVersion"]);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogErrorMessage("PMC_Synchronization_Metadata", "GetStausVersion(string commaSeparetedIds)"
                                                       , "Failed to Read Table: PMC_Synchronization_Metadata", ex.StackTrace);

                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogErrorMessage("PMC_Synchronization_Metadata", "GetStausVersion(string commaSeparetedIds)"
                                                      , "Failed to Read Table: PMC_Synchronization_Metadata", ex.StackTrace);

                }
            }

            return vs;
        }

        private void btnDisplayLimit_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            listView2.View = View.Details;

            LimitOrderDao dao = new LimitOrderDao();
            ICollection<Guid> orders = dao.GetOrderIds(new EntityFilter());
            listView2.SelectedIndexChanged += new EventHandler(listView2_SelectedIndexChanged);

            labelCount.Text = orders.Count.ToString();

            int i = 0;
            foreach (Guid o in orders)
            {
                ListViewItem item1 = new ListViewItem();
                VersionStatus vs = GetStausVersion(o.ToString());
                item1.Text = o.ToString();
                item1.SubItems.Add(vs.Version.ToString());
                item1.SubItems.Add(vs.Staus.ToString());
                listView2.Items.Add(item1);
                if (++i > 200)
                {
                    break;
                }
            }
        }

        private String selectedLimitId;
        void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var SelectedItem = listView2.SelectedIndices[0];
                ListViewItem item1 = listView2.Items[SelectedItem];
                selectedLimitId = item1.Text;
            }
            catch (Exception erx)
            {
            }
        }

        private void btnServerPushLimit_Click(object sender, EventArgs e)
        {
            OrderInfo orderinf = new OrderInfo { Action = "ServerPush" };
            orderinf.Credentials = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            if (chkFilterServerPush.Checked)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };
                Filter f1 = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Description", Value = "AAA" };
                orderinf.OrderFilter.FilterList.Add(f);
                 orderinf.OrderFilter.FilterList.Add(f1);
            }
            else
            {
                orderinf.OrderFilter = new EntityFilter();
            }


            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void btnPush_Click(object sender, EventArgs e)
        {
            //Collection<Guid> ids = (Collection<Guid>)_module.LimitOrderManager.ReadIds(new EntityFilter());
            // if (ids.Count > 0)
            //{
            var result = _module.SynchronizationModule.Push<LimitOrder>();

            var args = new ModalMessageBoxArgument("Push Limit",
                                                   string.Format("Pushed {0} objects. Push sync id: {1}",
                                                                 result.PushedEntityIds.Count,
                                                                 result.SyncSessionId),
                                                   new ModalMessageBoxButton("OK"));
            args.AutoCloseAfterTimeout = new TimeSpan(0, 0, 0, 4);
            ModalMessageBox.Show(args);
            //}
        }

        private void btnUpdateLimit_Click(object sender, EventArgs e)
        {
            LimitUpdate lu = new LimitUpdate(selectedLimitId, _module);
            lu.Show(this);
        }

        private void btnDeleteLimit_Click(object sender, EventArgs e)
        {
            try
            {
                _module.LimitOrderManager.Delete(new Guid(selectedLimitId), new DeleteArgs(GetType().Name));

            }
            catch (Exception)
            {

            }
        }

        private void btnPushMarket_Click(object sender, EventArgs e)
        {
            try
            {
                MarketOrderManager storage = _module.MarketOrderManager;

                //Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };
                //EntityFilter filter = new EntityFilter();
                //filter.FilterList.Add(f);

                //ICollection<Guid> ids = storage.ReadIds(filter);
                //Collection<Guid> guids = new Collection<Guid>();
                //foreach (Guid guid in ids)
                //{
                //guids.Add(guid);
                //}

                //var result = _module.SynchronizationModule.Push<MarketOrder>(guids);
                var result = _module.SynchronizationModule.Push<MarketOrder>();

                var args = new ModalMessageBoxArgument("Push Market",
                                                       string.Format("Pushed {0} objects. Push sync id: {1}",
                                                                     result.PushedEntityIds.Count,
                                                                     result.SyncSessionId),
                                                       new ModalMessageBoxButton("OK"));
                args.AutoCloseAfterTimeout = new TimeSpan(0, 0, 0, 4);
                ModalMessageBox.Show(args);

            }
            catch (Exception exception)
            {

            }
        }

        private void btnDeleteLocallyLimit_Click(object sender, EventArgs e)
        {
            LimitOrderManager storage = _module.LimitOrderManager;
            Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };
            EntityFilter filter = new EntityFilter();
            filter.FilterList.Add(f);

            ICollection<Guid> ids = storage.ReadIds(filter);
            _module.SynchronizationModule.DeleteLocally<LimitOrder>(ids);
        }

        private void btnUpdateLimitOrderServer_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            OrderInfo orderinf = new OrderInfo { OrderType = "LimitOrder", Action = "Update", NumberOfOrders = a };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            PreCom.Controls.PreComInputPanel2.Show(new PreCom.Controls.InputPanelItem((System.Windows.Forms.Control)sender, ""));
        }

        private void btnBulkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LimitOrderDao dao = new LimitOrderDao();
                dao.Delete();
                dao.UpdateMetadataAsDeleted();
                dao.DeletePropertyMetadata();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnBulkUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                LimitOrderDao dao = new LimitOrderDao();
                dao.BulkUpdate();
                dao.UpdateMetadataAsUpdated();
                dao.UpdatePropertyMetadata();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnDeleteLimitOrderServer_Click(object sender, EventArgs e)
        {
            int a = GetNumberOfOrders();

            OrderInfo orderinf = new OrderInfo { OrderType = "LimitOrder", Action = "DeleteLast", NumberOfOrders = a };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private void btnServerPushAPIArgumentTest_Click(object sender, EventArgs e)
        {
            OrderInfo orderinf = new OrderInfo { Action = "ServerPushArgumentTest" };

            try
            {
                Application.Instance.Communication.Send(orderinf, new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession));
            }
            catch (Exception ex)
            {

            }
        }

        private int GetNumberOfOrders()
        {
            int a = 1;
            try
            {
                a = int.Parse(textBox1.Text);
            }
            catch (Exception)
            {
            }

            return a;
        }
    }
}
