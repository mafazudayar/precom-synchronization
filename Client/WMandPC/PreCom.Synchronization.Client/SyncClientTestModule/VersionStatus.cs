﻿namespace SyncClientTestModule
{
    internal class VersionStatus
    {
        internal VersionStatus()
        {
            Version = -1;
            Staus = -1;
        }
        internal int Version { get; set; }
        internal int Staus { get; set; }
    }
}
