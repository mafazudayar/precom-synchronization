﻿using PreCom.Core.Modules;

namespace SyncClientTestModule
{
    public class Logger
    {
        public static void LogErrorMessage(string modulename, string headerMessage, string bodyMessage,
                string dumpMessage)
        {
            WriteToLog(modulename, headerMessage, bodyMessage, dumpMessage, LogLevel.Error);
        }

        public static void LogErrorMessage(string message)
        {
            WriteToLog(null, null, message, null, LogLevel.Error);
        }

        public static void WriteToLog(string moduleName, string headerMessage, string bodyMessage,
                string dumpMessage, LogLevel logLevel)
        {
            ILog log = PreCom.Application.Instance.CoreComponents.Get<ILog>();

            log.Write(new PreCom.Core.Log.LogItem()
            {
                Source = PreCom.Application.Instance,
                ID = GetModulename(moduleName),
                Level = logLevel,
                CustomLevel = 0,
                Header = GetHeaderMessage(headerMessage),
                Body = GetBodyMessage(bodyMessage),
                Dump = GetDumpMessage(dumpMessage)
            }
            );
        }

        private static string GetModulename(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "<Module - None>";
            }
            return name;
        }

        private static string GetHeaderMessage(string headerMessage)
        {
            if (string.IsNullOrEmpty(headerMessage))
            {
                headerMessage = "<Header - None>";
            }

            return headerMessage;
        }

        private static string GetBodyMessage(string bodyMessage)
        {
            if (string.IsNullOrEmpty(bodyMessage))
            {
                bodyMessage = "<Body - None>";
            }

            return bodyMessage;
        }

        private static string GetDumpMessage(string dumpMessage)
        {
            if (string.IsNullOrEmpty(dumpMessage))
            {
                dumpMessage = "<Dump - None>";
            }

            return dumpMessage;
        }
    }
}
