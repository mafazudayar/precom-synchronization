﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using PreCom.Core;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;

namespace SyncClientTestModule
{
    internal class LimitOrderDao : IOrderDao<LimitOrder>
    {
        private StorageBase _storage;

        internal LimitOrderDao()
        {
            _storage = PreCom.Application.Instance.Storage;
        }

        internal void CreateTables()
        {
            if (!_storage.ObjectExists("SyncTest_LimitOrder", ObjectType.Table))
            {
                using (IDbCommand command = _storage.CreateCommand())
                {
                    StringBuilder queryString = new StringBuilder();

                    queryString.Append("CREATE TABLE [SyncTest_LimitOrder]( ");
                    queryString.Append("[AutoId] [bigint] NOT NULL identity (1,1) PRIMARY KEY, ");
                    queryString.Append("[OrderId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[Creator] [nvarchar](50) NULL, ");
                    queryString.Append("[Description] [nvarchar] (4000) NULL, ");
                    queryString.Append("[Date] [datetime] NULL, ");
                    queryString.Append("[Image] [image] NULL, ");
                    queryString.Append("[ReportTitle] [nvarchar](50) NULL, ");
                    queryString.Append("[ReportContent] [nvarchar](50) NULL, ");
                    queryString.Append("[ReportId] [int] NULL, ");
                    queryString.Append("[Editors] [nvarchar](50) NULL, ");
                    queryString.Append("[IsAssigned] [bit] NOT NULL)");

                    command.CommandText = queryString.ToString();
                    try
                    {
                        _storage.ExecuteNonCommand(command);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "CreateTable()"
                       , "Failed to create Table: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
        }

        public bool Insert(LimitOrder order)
        {
            bool inserted = false;
            const string commandString = "INSERT INTO  [SyncTest_LimitOrder] "
                             + " ([OrderId],[Creator],[Description],[Date],[Image],[ReportTitle],[ReportContent],[ReportId],[Editors],[IsAssigned]) "
                             + " VALUES (@OrderID, @Creator, @Description, @Date,@Image, @ReportTitle, @ReportContent,@ReportId, @Editors,@IsAssigned); ";

            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand insertCommand = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@OrderId", order.Id));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Creator", order.Creator));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Description", order.Description));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Date", order.Date));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@IsAssigned", order.IsAssigned));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Image", order.Image == null ? null : order.Image.Content));


                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportTitle", order.Report == null ? null : order.Report.Title));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportContent",
                                                                                order.Report == null ? null : order.Report.Content));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportId", order.Report == null ? -1 : order.Report.Id));



                        string editors = null;
                        if (order.Report != null && order.Report.Editors != null)
                        {
                            foreach (Editor editor in order.Report.Editors)
                            {
                                editors = editor.EditorName + "-" + editors;
                            }

                            if (editors != null) editors = editors.TrimEnd('-');
                        }
                        insertCommand.Parameters.Add(
                                PreCom.Application.Instance.Storage.CreateParameter("@Editors", editors));

                        int rows = insertCommand.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            inserted = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Insert(Order order)"
                       , "Failed to Insert: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }

            return inserted;
        }

        public bool Update(LimitOrder order)
        {
            bool updated = false;
            const string commandString = "UPDATE [SyncTest_LimitOrder] "
                             + " SET [Creator]= @Creator,[Description]=@Description,[Date]=@Date,[Image]=@Image,"
                             + "[IsAssigned]=@IsAssigned,[ReportTitle]=@ReportTitle,[ReportContent]=@ReportContent,"
                             + "[ReportId]=@ReportId,[Editors]=@Editors "
                             + " WHERE [OrderId]= @OrderId; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand insertCommand = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@OrderId", order.Id));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Creator", order.Creator));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Description", order.Description));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Date", order.Date));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@IsAssigned", order.IsAssigned));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@Image", order.Image == null ? null : order.Image.Content));


                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportTitle", order.Report == null ? null : order.Report.Title));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportContent",
                                                                                order.Report == null ? null : order.Report.Content));
                        insertCommand.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@ReportId", order.Report == null ? -1 : order.Report.Id));

                        string editors = null;
                        if (order.Report != null && order.Report.Editors != null)
                        {
                            foreach (Editor editor in order.Report.Editors)
                            {
                                editors = editor.EditorName + "-" + editors;
                            }

                            if (editors != null) editors = editors.TrimEnd('-');
                        }
                        insertCommand.Parameters.Add(
                                PreCom.Application.Instance.Storage.CreateParameter("@Editors", editors));
                        int rows = insertCommand.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            updated = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Update(Order order)"
                       , "Failed to Update record: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return updated;
        }

        public bool Delete(string orderId)
        {
            bool deleted = false;
            const string commandString = "DELETE FROM [SyncTest_LimitOrder] "
                             + " WHERE [OrderId]= @OrderId; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        command.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@OrderId", orderId));

                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Delete(Order order)"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        public bool DeleteAll()
        {
            Delete();
            return true;
        }

        internal Collection<ISynchronizable> GetOrders(string commaSeparetedIds)
        {
            Collection<ISynchronizable> orders = null;

            string commandString = " SELECT * from [SyncTest_LimitOrder] "
                       + " WHERE [OrderId] IN (" + commaSeparetedIds + "); ";

            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    using (IDataReader reader = PreCom.Application.Instance.Storage.ExecuteQueryCommand(command))
                    {
                        orders = new Collection<ISynchronizable>();

                        try
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogErrorMessage("SyncTest_LimitOrder", "GetOrders(string commaSeparetedIds)"
                                , "Failed to Read Table: SyncTest_LimitOrder", ex.StackTrace);

                        }

                    }
                }
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(ICollection<Guid> entities)
        {
            Collection<ISynchronizable> orders = new Collection<ISynchronizable>();
            if (entities != null && entities.Count > 0)
            {
                foreach (Guid syncEntityInfo in entities)
                {
                    LimitOrder order = GetOrder(syncEntityInfo);
                    if (order != null )
                    {
                        orders.Add(order); 
                    }
                }
                return orders;
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(EntityFilter filter)
        {
            Collection<ISynchronizable> orders = null;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * from [SyncTest_LimitOrder] ");

            if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
            {
                int a = 0;
                foreach (Filter f in filter.FilterList)
                {
                    sb.Append("WHERE ");
                    a = a + 1;
                    sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                    if (a < filter.FilterList.Count)
                    {
                        sb.Append(" AND ");
                    }
                }
            }
            sb.Append(";");
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(sb.ToString()))
                {
                    using (IDataReader reader = PreCom.Application.Instance.Storage.ExecuteQueryCommand(command))
                    {
                        orders = new Collection<ISynchronizable>();
                        try
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }

                        }
                        catch (Exception ex)
                        {
                            Logger.LogErrorMessage("SyncTest_LimitOrder", "GetOrders(EntityFilter filter)"
                                                   , "Failed to Read Table: SyncTest_LimitOrder", ex.StackTrace);
                        }
                    }
                }
            }
            return orders;
        }

        public ICollection<Guid> GetOrderIds(EntityFilter filter)
        {
            Collection<Guid> orders = null;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT OrderId from [SyncTest_LimitOrder] ");
            if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
            {
                sb.Append("WHERE ");
                int a = 0;
                foreach (Filter f in filter.FilterList)
                {
                    a = a + 1;
                    sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                    if (a < filter.FilterList.Count)
                    {
                        sb.Append(" AND ");
                    }
                }
            }

            sb.Append(";");
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(sb.ToString()))
                {
                    using (IDataReader reader = PreCom.Application.Instance.Storage.ExecuteQueryCommand(command))
                    {
                        orders = new Collection<Guid>();
                        try
                        {
                            while (reader.Read())
                            {
                                Guid oid = new Guid(Convert.ToString(reader["OrderId"]));
                                orders.Add(oid);
                            }

                        }
                        catch (Exception ex)
                        {
                            Logger.LogErrorMessage("SyncTest_LimitOrder", "GetOrderIds(EntityFilter filter)"
                                                   , "Failed to Read Table: SyncTest_LimitOrder", ex.StackTrace);
                        }
                    }
                }
            }

            return orders;
        }

        public ICollection<LimitOrder> GetOrders()
        {
            Collection<LimitOrder> orders = null;

            string commandString = " SELECT * from [SyncTest_LimitOrder]; ";

            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    using (IDataReader reader = PreCom.Application.Instance.Storage.ExecuteQueryCommand(command))
                    {
                        orders = new Collection<LimitOrder>();

                        try
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogErrorMessage("SyncTest_MarketOrder", "GetOrders()"
                                , "Failed to Read Table: SyncTest_MarketOrder", ex.StackTrace);
                        }

                    }
                }
            }

            return orders;
        }

        public LimitOrder GetOrder(Guid id)
        {
            string commandString = "SELECT * from [SyncTest_LimitOrder] WHERE OrderId = '" + id + "'; ";
            LimitOrder order = null;
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    using (IDataReader reader = PreCom.Application.Instance.Storage.ExecuteQueryCommand(command))
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogErrorMessage("SyncTest_MarketOrder", "GetOrders()"
                                , "Failed to Read Table: SyncTest_MarketOrder", ex.StackTrace);
                        }
                    }
                }
            }

            return order;
        }

        private string GetValue(string propertyName, object value)
        {
            switch (propertyName)
            {
                case "Id":
                    return "'" + value + "'";
                case "Creator":
                    return "'" + value + "'";
                case "Description":
                    return "'" + value + "'";
                case "IsAssigned":
                    return value.ToString();
                case "Date":
                    return value.ToString();
                case "Report Title":
                    return "'" + value + "'";
                default:
                    return value.ToString();
            }
        }

        private string GetTableColumnName(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return "OrderId";
                case "Creator":
                    return "Creator";
                case "Description":
                    return "Description";
                case "IsAssigned":
                    return "IsAssigned";
                case "Date":
                    return "Date";
                case "Report Title":
                    return "ReportTitle";
                default:
                    return "";
            }
        }

        private string GetOperator(FilterOperator filterOperator)
        {
            string op = "=";
            switch (filterOperator)
            {
                case FilterOperator.EqualTo:
                    op = "=";
                    break;
                case FilterOperator.NotEqualTo:
                    op = "!=";
                    break;
                case FilterOperator.GreaterThan:
                    op = ">";
                    break;
                case FilterOperator.LessThan:
                    op = "<";
                    break;
            }
            return op;
        }

        internal bool Delete()
        {
            bool deleted = false;
            const string commandString = "DELETE FROM [SyncTest_LimitOrder]; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Delete()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool BulkUpdate()
        {
            bool deleted = false;
            Random r = new Random();
            string description = "XXXXXXXX" + r.Next(1000);
            string commandString = "UPDATE [SyncTest_LimitOrder] set Description = '" + description + "'; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Delete()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool DeleteMetadata()
        {
            bool deleted = false;
            const string commandString = "DELETE FROM [PMC_Synchronization_Metadata]; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "DeleteMetadata()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool UpdateMetadataAsDeleted()
        {
            bool deleted = false;
            const string commandString = "UPDATE [PMC_Synchronization_Metadata] SET EntityStatus=4; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "DeleteMetadata()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool UpdateMetadataAsUpdated()
        {
            bool deleted = false;
            const string commandString = "UPDATE [PMC_Synchronization_Metadata] SET EntityStatus=2; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "DeleteMetadata()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool DeletePropertyMetadata()
        {
            bool deleted = false;
            const string commandString = "DELETE FROM [PMC_Synchronization_Property_Metadata]; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "DeletePropertyMetadata()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        internal bool UpdatePropertyMetadata()
        {
            bool deleted = false;
            const string commandString = "UPDATE[PMC_Synchronization_Property_Metadata] set IsChanged=1 where PropertyName='Description'; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "DeletePropertyMetadata()"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        #region IOrderDao<LimitOrder> Members


        public bool Delete(Guid orderId)
        {
            bool deleted = false;
            const string commandString = "DELETE FROM [SyncTest_LimitOrder] "
                             + " WHERE [OrderId]= @OrderId; ";
            if (PreCom.Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = PreCom.Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        command.Parameters.Add(
                            PreCom.Application.Instance.Storage.CreateParameter("@OrderId", orderId.ToString()));

                        int rows = command.ExecuteNonQuery();

                        if (rows > 0)
                        {
                            deleted = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogErrorMessage("SyncTest_LimitOrder", "Delete(Order order)"
                            , "Failed to delete order: SyncTest_LimitOrder", ex.StackTrace);
                    }
                }
            }
            return deleted;
        }

        #endregion
    }
}
