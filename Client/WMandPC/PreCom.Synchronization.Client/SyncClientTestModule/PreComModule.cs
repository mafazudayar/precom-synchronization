﻿using System;
using System.Text;
using System.Threading;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;

namespace SyncClientTestModule
{
    public class PreComModule : PreCom.Core.ModuleBase, PreCom.Core.IForm
    {
        #region Private fields

        /// <summary>
        /// Indicates if the module has been initialized or not.
        /// </summary>
        private bool _isInitialized;

        internal SynchronizationClientBase SynchronizationModule;

        internal MarketOrderManager MarketOrderManager = new MarketOrderManager(new MarketOrderMemoryDao());

        internal LimitOrderManager LimitOrderManager;

        /// <summary>
        /// The PreCom form.
        /// </summary>
        private SyncTestMain _form;

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                // TODO: Change module name.
                return "SyncTest";
            }
        }

        #endregion Public properties

        [Inject]
        public PreComModule(ILog log, SynchronizationClientBase synchronization)
        {
            LimitOrderManager = new LimitOrderManager(new LimitOrderDao(), log, synchronization);
            //LimitOrderManager = new LimitOrderManager(new LimitOrderDao());
        }


        #region ModuleBase members

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">Cancel the dispose of the module</param>
        /// <returns>
        /// true if disposing was OK, false otherwise.
        /// </returns>
        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;

            return true;
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>
        /// true if initializing was OK, false otherwise.
        /// </returns>
        public override bool Initialize()
        {
            LimitOrderDao dao = new LimitOrderDao();
            dao.CreateTables();

            SynchronizationModule = PreCom.Application.Instance.Components.Get<SynchronizationClientBase>();

            SynchronizationModule.RegisterStorage<MarketOrder>(MarketOrderManager);
            SynchronizationModule.RegisterStorage(LimitOrderManager);

            SynchronizationModule.RegisterSyncEventNotifier<LimitOrder>(LimitOrderNotifier);
            //SynchronizationModule.RegisterSyncEventNotifier<MarketOrder>(MarketOrderNotifier);
            SynchronizationModule.RegisterConflictNotifier<LimitOrder>(LimitOrderConflictNotifier);
            //SynchronizationModule.RegisterConflictNotifier<MarketOrder>(LimitOrderConflictNotifier);

            //SynchronizationModule.RegisterAutomaticPush<LimitOrder>(Operation.Deleted | Operation.Created | Operation.Updated);
            //SynchronizationModule.RegisterAutomaticPush<MarketOrder>(Operation.Created);

            // Test client pull #38
            //LimitOrderManager.EntityChanged += (sender, args) =>
            //    {
            //        if (args.Change == Operation.Created)
            //        {
            //            var order = (LimitOrder) args.New;
            //            order.Description += "1";
            //            LimitOrderManager.Update(order,
            //                                     new UpdateArgs { CredentialId = args.CredentialId, OriginatorType = GetType().Name });
            //        }
            //    };

            _isInitialized = true;
            return true;
        }

        private void LimitOrderNotifier(Guid id, SyncInfo syncInfo)
        {
            Logger.WriteToLog("Sync", "SyncEvent - LimitOrder ",
                              id.ToString() + " - " + syncInfo.Reason.ToString() + " - " + syncInfo.Type.ToString() +
                              " - " + syncInfo.SessionId.ToString(),
                              syncInfo.Information + " - Synchronized Entity Count : " + syncInfo.SynchronizedEntityCount + " - Total Entity Count : "+syncInfo.TotalEntityCount,
                              LogLevel.Information);
        }

        private void MarketOrderNotifier(Guid id, SyncInfo syncInfo)
        {
            Logger.WriteToLog("Sync", "SyncEvent - MarketOrder ",
                              id.ToString() + " - " + syncInfo.Reason.ToString() + " - " + syncInfo.Type.ToString() +
                              " - " + syncInfo.SessionId.ToString(), syncInfo.Information ?? "", LogLevel.Information);
        }

        private void LimitOrderConflictNotifier(ConflictNotificationInfo info)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ConflictedPropertyInfo c in info.Conflicts)
            {
                sb.Append(c.Name + " " + c.OldValue + " > " + c.NewValue + "     ");
            }
            Logger.WriteToLog("Sync",
                  string.Format(
                      "SyncConflictNotifier - LimitOrder EntityId: {0}",
                      info.EntityId), sb.ToString(), "", LogLevel.Information);
        }

        public MarketOrder CreateMarketOrder(string creater, string description, string guid)
        {
            MarketOrder order = new MarketOrder();
            Guid id = new Guid(guid);
            order.Guid = id;
            order.Creator = creater;
            order.Description = description;
            order.Date = DateTime.Now;
            order.IsAssigned = false;

            return order;
        }

        /// <summary>
        /// Check if instance is initialized or not.
        /// </summary>
        /// <value>True if initialized, else false</value>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        #endregion ModuleBase members

        #region IForm Members

        /// <summary>
        /// The activate function gets called by the framework before the Form is displayed
        /// </summary>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false
        /// </returns>
        public bool Activate()
        {
            _form = new SyncTestMain(this);

            return true;
        }

        /// <summary>
        /// The deactivate function gets called by the framework before the Form is hidden
        /// </summary>
        /// <param name="cancel">A return parameter (out) to accept to be deactivated or not. By setting cancel to true the component can force to stay activated</param>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false
        /// </returns>
        public bool Deactivate(out bool cancel)
        {
            if (_form != null)
            {
                _form.Dispose();
                _form = null;
            }

            cancel = false;

            return true;
        }

        /// <summary>
        /// Gets the main form of the component (read only)
        /// </summary>
        /// <value>The PreComForm</value>
        public PreCom.Controls.PreComForm Form
        {
            get
            {
                return _form;
            }
        }

        /// <summary>
        /// Gets the image (32x32 with the top left (1,1) pixel color used as transparent color when drawing the image) of the IForm to be displayed on a menu button (read only)
        /// </summary>
        /// <value>Image used as icon</value>
        public System.Drawing.Image Icon
        {
            get
            {
                return Properties.Resources.icon_32x32;
            }
        }

        /// <summary>
        /// The event to invoke to request to  be shown (see ShowRequestDelegate)
        /// </summary>
        public event PreCom.Core.ShowRequestDelegate ShowRequest;

        #endregion IForm Members
    }
}
