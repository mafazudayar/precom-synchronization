﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;

namespace SyncClientTestModule
{
    public partial class LimitUpdate : PreCom.Controls.PreComForm
    {
        private string _id;
        private LimitOrder _order;
        private PreComModule _module;
        public LimitUpdate(string id, PreComModule module)
        {
            _id = id;
            InitializeComponent();
            lblLinitId.Text = id;
            _module = module;
            try
            {
                LimitOrderDao dao = new LimitOrderDao();
                _order = dao.GetOrder(new Guid(id));

                txtCreator.Text = _order.Creator;
                txtDesc.Text = _order.Description;
                if (_order.Report != null) txtReportContent.Text = _order.Report.Content;
                if (_order.Report != null) txtReportID.Text = _order.Report.Id.ToString();
                if (_order.Report != null) txtReportTitle.Text = _order.Report.Title;

                chkAssign.Checked = _order.IsAssigned;
                dateTimePicker1.Value = _order.Date;

                txtEditors.Text = GenerateEditorString();
            }
            catch (Exception e)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose(true);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _order.Creator = txtCreator.Text;
                _order.Description = txtDesc.Text;
                _order.IsAssigned = chkAssign.Checked;
                _order.Date = dateTimePicker1.Value;
                if (!string.IsNullOrEmpty(txtReportContent.Text))
                {
                    if (_order.Report != null)
                    {
                        _order.Report.Content = txtReportContent.Text;
                    }
                    else
                    {
                        _order.Report = new Report();
                        _order.Report.Content = txtReportContent.Text;
                    }
                }

                if (!string.IsNullOrEmpty(txtReportID.Text))
                {
                    if (_order.Report != null)
                    {
                        _order.Report.Id = Convert.ToInt32(txtReportID.Text);
                    }
                    else
                    {
                        _order.Report = new Report();
                        _order.Report.Id = Convert.ToInt32(txtReportID.Text);
                    }
                }

                if (!string.IsNullOrEmpty(txtReportTitle.Text))
                {
                    if (_order.Report != null)
                    {
                        _order.Report.Title = txtReportTitle.Text;
                    }
                    else
                    {
                        _order.Report = new Report();
                        _order.Report.Title = txtReportTitle.Text;
                    }
                }

                _module.LimitOrderManager.Update(_order, new UpdateArgs(GetType().Name));
            }
            catch (Exception exception)
            {

            }
        }

        private string GenerateEditorString()
        {
            string editors = string.Empty;
            if (_order.Report != null && _order.Report.Editors != null)
            {
                foreach (Editor editor in _order.Report.Editors)
                {
                    editors = editor.EditorName + "-" + editors;
                }

                editors = editors.TrimEnd('-');
            }
            return editors;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (_order.Report != null && _order.Report.Editors != null)
            {
                _order.Report.Editors.Clear();
            }
            txtEditors.Text = string.Empty;
        }

        private void btnAddEditor_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAddEditor.Text))
            {
                if (_order.Report == null)
                {
                    _order.Report = new Report();
                }

                if (_order.Report.Editors == null)
                {
                    _order.Report.Editors = new List<Editor>();
                }

                _order.Report.Editors.Add(new Editor() { EditorName = txtAddEditor.Text });

                txtEditors.Text = GenerateEditorString();
            }

        }

        private void txtCreator_Click(object sender, EventArgs e)
        {
            PreCom.Controls.PreComInputPanel2.Show(new PreCom.Controls.InputPanelItem((System.Windows.Forms.Control)sender, ""));
        }

        private void txtDesc_Click(object sender, EventArgs e)
        {
            PreCom.Controls.PreComInputPanel2.Show(new PreCom.Controls.InputPanelItem((System.Windows.Forms.Control)sender, ""));
        }

        private void txtReportTitle_Click(object sender, EventArgs e)
        {
            PreCom.Controls.PreComInputPanel2.Show(new PreCom.Controls.InputPanelItem((System.Windows.Forms.Control)sender, ""));
        }

        private void txtReportContent_Click(object sender, EventArgs e)
        {
            PreCom.Controls.PreComInputPanel2.Show(new PreCom.Controls.InputPanelItem((System.Windows.Forms.Control)sender, ""));
        }
    }
}