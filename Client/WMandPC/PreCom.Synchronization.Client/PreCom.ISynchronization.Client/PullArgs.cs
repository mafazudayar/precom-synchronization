﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Argument used when starting a pull via Synchronization.pull()
    /// </summary>
    public class PullArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PullArgs"/> class.
        /// </summary>
        /// <param name="filter">The filter.</param>
        public PullArgs(EntityFilter filter)
        {
            Filter = filter;
        }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public EntityFilter Filter{ get; set; }
    }
}
