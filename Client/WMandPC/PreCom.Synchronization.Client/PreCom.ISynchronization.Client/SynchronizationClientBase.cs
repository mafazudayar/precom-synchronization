﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Notify the custom module when a conflict occurs.
    /// </summary>
    /// <param name="conflictInfo">The conflict info.</param>
    public delegate void ConflictNotifyDelegate(ConflictNotificationInfo conflictInfo);

    /// <summary>
    /// Notify the current sync status of a given entity to custom module.
    /// </summary>
    /// <param name="entityId">The entity id.</param>
    /// <param name="syncInfo">The sync info.</param>
    public delegate void SyncEventDelegate(Guid entityId, SyncInfo syncInfo);

    /// <summary>
    /// Base class which use by the synchronization module.
    /// </summary>
    public abstract class SynchronizationClientBase : Core.ModuleBase
    {
        /// <summary>
        /// The custom module needs to register the storage through this method in order to use the synchronization module.
        /// </summary>
        /// <typeparam name="T">The type of the entity that the custom module is using to synchronize.</typeparam>
        /// <param name="storage">An instance of the storage that the custom module uses.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if storage is null.</exception>
        public abstract void RegisterStorage<T>(SynchronizedStorageBase<T> storage)
            where T : ISynchronizable;

        /// <summary>
        /// Unregisters a storage through this method for disposing registered object.
        /// </summary>
        /// <typeparam name="T">The type of the entity that the custom module is using to synchronize.</typeparam>
        /// <param name="storage">An instance of the storage that the custom module uses.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if storage is null.</exception>
        public abstract void UnregisterStorage<T>(SynchronizedStorageBase<T> storage)
            where T : ISynchronizable;

        /// <summary>
        /// Registers the automatic push.
        /// Pushes automatically, when an entity of the specified type changed.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="changeOperation">The change operation.</param>
        public abstract void RegisterAutomaticPush<T>(Operation changeOperation) where T : ISynchronizable;

        /// <summary>
        /// Unregisters the automatic push.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        public abstract void UnregisterAutomaticPush<T>() where T : ISynchronizable;

        /// <summary>
        /// Starts a new client initiated push request.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <returns>Push Result</returns>
        public abstract PushResult Push<T>() where T : ISynchronizable;

        /// <summary>
        /// Pushes the specified entity ids.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="pushArgs">Information related to the push request.</param>
        /// <returns>Push Result</returns>
        public abstract PushResult Push<T>(PushArgs pushArgs) where T : ISynchronizable;

        /// <summary>
        ///  Starts a new client initiated pull.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="pullArgs">Information related to the pull request.</param>
        /// <returns>Pull Result</returns>
        public abstract PullResult Pull<T>(PullArgs pullArgs) where T : ISynchronizable;

        /// <summary>
        /// Removes entities from local client store without deleting the entities from the server.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="entityIds">The entity ids.</param>
        public abstract void DeleteLocally<T>(ICollection<Guid> entityIds) where T : ISynchronizable;

        /// <summary>
        /// Registers the conflict notify method.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="conflictNotifier">The conflict notify method.</param>
        public abstract void RegisterConflictNotifier<T>(ConflictNotifyDelegate conflictNotifier) where T : ISynchronizable;

        /// <summary>
        /// Unregisters the conflict notify method.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        public abstract void UnregisterConflictNotifier<T>() where T : ISynchronizable;

        /// <summary>
        /// Registers the sync event notify method.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="syncEventNotifier">The sync event notify method.</param>
        public abstract void RegisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier) where T : ISynchronizable;

        /// <summary>
        /// Unregisters the sync event notify method.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="syncEventNotifier">The sync event notify method.</param>
        public abstract void UnregisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier) where T : ISynchronizable;
    }
}
