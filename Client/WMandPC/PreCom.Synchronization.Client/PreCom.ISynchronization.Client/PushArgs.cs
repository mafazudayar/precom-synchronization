﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Argument used when starting a push via Synchronization.push()
    /// </summary>
    public class PushArgs
    {
        private ICollection<Guid> _entityIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushArgs"/> class.
        /// </summary>
        /// <param name="entityIds">The entity ids.</param>
        public PushArgs(ICollection<Guid> entityIds)
        {
            _entityIds = entityIds;
        }

        /// <summary>
        /// Gets the collection of entityIds.
        /// </summary>
        public ICollection<Guid> EntityIds
        {
            get
            {
                if (_entityIds == null)
                {
                    _entityIds = new Collection<Guid>();
                }
                return _entityIds;
            }
        }
    }
}
