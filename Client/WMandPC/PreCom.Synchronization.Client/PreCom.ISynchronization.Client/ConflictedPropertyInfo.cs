﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains information about a conflicted property on the client side.
    /// </summary>
    public class ConflictedPropertyInfo
    {
        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the old value.
        /// </summary>
        public object OldValue { get; set; }
        /// <summary>
        /// Gets or sets the new value.
        /// </summary>
        public object NewValue { get; set; }
    }
}
