﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Used to send conflicted property information of an entity on the 
    /// client side to custom modules
    /// </summary>
    public class ConflictNotificationInfo
    {
        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        public Guid EntityId { get; set; }
        /// <summary>
        /// Gets or sets the conflicted properties.
        /// </summary>
        public List<ConflictedPropertyInfo> Conflicts { get; set; }
    }
}
