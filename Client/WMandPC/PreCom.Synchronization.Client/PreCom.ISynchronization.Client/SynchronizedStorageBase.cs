﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Base class for custom storage implementations. This class implements the contract of ISynchronizedStorage
    /// to perform CRUD operations. It will acquire the lock when needed and also fire the EntityChanged event.
    /// It contains a number of abstract methods that the subclass needs to implement where the actual CRUD operations
    /// of the storage will be performed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SynchronizedStorageBase<T> : ISynchronizedStorage<T> where T : ISynchronizable
    {
        public void Create(ISynchronizable entity, CreateArgs args)
        {
            lock (GetLock())
            {
                OnCreate(entity, args);
                FireChangedEvent(entity.Guid, Operation.Created, args.Originator, args.CredentialId, null, entity);
            }
        }

        public void Update(ISynchronizable entity, UpdateArgs args)
        {
            lock (GetLock())
            {
                ISynchronizable oldEntity = GetOldEntity(entity.Guid);

                OnUpdate(entity, args);
                FireChangedEvent(entity.Guid, Operation.Updated, args.Originator, args.CredentialId, oldEntity, entity);
            }
        }

        public void Delete(Guid id, DeleteArgs args)
        {
            lock (GetLock())
            {
                ISynchronizable oldEntity = GetOldEntity(id);

                OnDelete(id, args);
                FireChangedEvent(id, Operation.Deleted, args.Originator, args.CredentialId, oldEntity, null);
            }
        }

        public ICollection<ISynchronizable> Read(ICollection<Guid> entityIds)
        {
            return OnRead(entityIds);
        }

        public ICollection<ISynchronizable> Read(EntityFilter filter)
        {
            return OnRead(filter);
        }

        public ICollection<Guid> ReadIds(EntityFilter filter)
        {
            return OnReadIds(filter);
        }

        public event EventHandler<ChangeEventArgs> EntityChanged;

        /// <summary>
        /// Called by the base class when an object shall be created. Implement the create functionality in this method.
        /// </summary>
        /// <param name="entity">The entity to be created.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entity.</exception>
        protected abstract void OnCreate(ISynchronizable entity, CreateArgs args);

        /// <summary>
        /// Called by the base class when an object shall be updated. Implement the update functionality in this method.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        /// <param name="args">The argument containing information about the modification.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when updating the entity.</exception>
        protected abstract void OnUpdate(ISynchronizable entity, UpdateArgs args);

        /// <summary>
        /// Called by the base class when an object shall be deleted. Implement the delete functionality in this method.
        /// </summary>
        /// <param name="id">The id of the entity to be deleted.</param>
        /// <param name="args">The argument containing information about the deletion.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when deleting the entity.</exception>
        protected abstract void OnDelete(Guid id, DeleteArgs args);

        /// <summary>
        /// Called by the base class when entities should be read given a list of entity Ids. Implement the read functionality in this method.
        /// </summary>
        /// <param name="entityIds">The collection of entity ids</param>
        /// <returns>Collection of ISynchronizable entities.</returns>
        protected abstract ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds);

        /// <summary>
        /// Called by the base class when entities should be read given a filter. Implement the read functionality in this method.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Collection of ISynchronizable entities.</returns>
        protected abstract ICollection<ISynchronizable> OnRead(EntityFilter filter);

        /// <summary>
        /// Called by the base class when entity ids should be read given a filter. Implement the read functionality in this method.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Collection of ids selected by the filter</returns>
        protected abstract ICollection<Guid> OnReadIds(EntityFilter filter);

        private void FireChangedEvent(Guid id, Operation operation, object originator, int credentialId,
                                      ISynchronizable oldEntity, ISynchronizable newEntity)
        {
            ChangeEventArgs args = new ChangeEventArgs();
            args.Change = operation;
            args.Id = id;
            args.New = newEntity;
            args.Old = oldEntity;
            args.EntityType = typeof (T).Name;
            args.Originator = originator;
            args.CredentialId = credentialId;
            if (EntityChanged != null)
            {
                EntityChanged.Invoke(this, args);
            }
        }

        private object GetLock()
        {
            SynchronizationClientBase synchronization = Application.Instance.Modules.Get<SynchronizationClientBase>();
            return synchronization.GetLock<T>();
        }

        private ISynchronizable GetOldEntity(Guid entityId)
        {
            ICollection<ISynchronizable> result = Read(new Guid[] {entityId});
            if (result == null || result.Count == 0)
            {
                throw new SynchronizedStorageException(string.Format("No object with id {0} was found", entityId));
            }
            if (result.Count != 1)
            {
                throw new SynchronizedStorageException(string.Format("{0} objects with id {1} was found, expecting one object", result.Count, entityId));
            }
            return result.First();
        }
    }
}