﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains information about the synchronization process started by calling ISynchronization.pull()
    /// </summary>
    public class PullResult
    {
        /// <summary>
        /// Gets or sets the sync session.
        /// </summary>
        public Guid SyncSession { get; set; }
    }
}
