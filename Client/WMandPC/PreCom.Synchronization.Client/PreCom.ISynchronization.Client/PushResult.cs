﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains information about the synchronization process started by calling ISynchronization.push().
    /// </summary>
    public class PushResult
    {
        private Collection<Guid> _pushedEntityIds;

        /// <summary>
        /// Gets or sets the sync session id.
        /// </summary>
        public Guid SyncSessionId { get; set; }

        /// <summary>
        /// Gets the pushed entity ids.
        /// </summary>
        public ICollection<Guid> PushedEntityIds
        {
            get
            {
                if (_pushedEntityIds == null)
                {
                    _pushedEntityIds = new Collection<Guid>();
                }
                return _pushedEntityIds;
            }
        }
    }
}
