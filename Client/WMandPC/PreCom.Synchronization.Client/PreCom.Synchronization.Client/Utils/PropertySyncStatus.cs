﻿namespace PreCom.Synchronization.Utils
{
    internal enum PropertySyncStatus
    {
        Synchronized = 0,
        IsChanged = 1
    }
}
