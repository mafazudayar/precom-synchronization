﻿using System;
using PreCom.Core;
using PreCom.Core.Modules;

namespace PreCom.Synchronization.Utils
{
    internal class LogHelper
    {
        private readonly ILog _logger;

        internal LogHelper(ILog log)
        {
            _logger = log;
        }

        internal void LogError(PreComBase preComBase, Exception exception, LogId id, string header)
        {
            _logger.Write(new PreCom.Core.Log.LogItem(preComBase,
                LogLevel.Error,
                id.ToString(),
                header,
                exception.Message,
                exception.ToString()));
        }

        internal void LogError(PreComBase preComBase, LogId id, string header, string body, string dump)
        {
            _logger.Write(new PreCom.Core.Log.LogItem(preComBase, LogLevel.Error, id.ToString(), header, body, dump));
        }

        internal void LogWarning(PreComBase preComBase, LogId id, string header, string body, string dump)
        {
            if (_logger != null)
            {
                _logger.Write(new PreCom.Core.Log.LogItem(preComBase, LogLevel.Warning, id.ToString(), header, body, dump));
            }
        }

        internal void LogDebug(PreComBase preComBase, LogId id, string header, string body, string dump)
        {
            _logger.Write(new PreCom.Core.Log.LogItem(preComBase, LogLevel.Debug, id.ToString(), header, body, dump));
        }

        internal void LogInformation(PreComBase preComBase, LogId id, string header, string body, string dump)
        {
            _logger.Write(new PreCom.Core.Log.LogItem(preComBase,
                LogLevel.Information,
                id.ToString(),
                header,
                body,
                dump));
        }
    }
}
