﻿namespace PreCom.Synchronization.Utils
{
    public enum SyncEventType
    {
        ClientPull,
        ClientPush,
        ServerPush
    }
}
