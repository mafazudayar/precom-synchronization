﻿using System.Collections.Generic;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Data
{
    internal class EntityMetadata : SyncMetadata
    {
        private List<string> _properties;
        public List<string> Properties
        {
            get { return _properties ?? (_properties = new List<string>()); }
        }
    }
}
