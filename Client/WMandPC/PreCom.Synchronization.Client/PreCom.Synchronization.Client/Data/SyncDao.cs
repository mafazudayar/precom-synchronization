﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using PreCom.Core;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Data
{
    internal class SyncDao : ISyncDao
    {
        #region Fields

        private const string MetadataTableName = "PMC_Synchronization_Metadata";
        private const string EntityTypeColumn = "EntityType";
        private const string EntityIdColumn = "EntityId";
        private const string EntityVersionColumn = "EntityVersion";
        private const string EntityStatusColumn = "EntityStatus";
        private const string SessionIdColumn = "SessionId";
        private const string ChangeIdColumn = "ChangeId";

        private const string PartialEntityTableName = "PMC_Synchronization_Partial_Entity";
        private const string PacketNumberColumn = "PacketNumber";
        private const string PartialEntityColumn = "PartialEntity";
        private const string InsertedOnColumn = "InsertionDate";

        private const string PropertyMetadataTableName = "PMC_Synchronization_Property_Metadata";
        private const string PropertyNameColumn = "PropertyName";
        private const string IsChangedColumn = "IsChanged";

        private const string PartialInvokeMessageTableName = "PMC_Synchronization_Partial_PullInvokeMessage";
        private const string PartialInvokeMessageColumn = "PartialPullInvokeMessage";

        private const string PartialEntityIndex = "idxPartialEntity_EntityId";
        private const string PropertyMetadataEntityIndex = "idxPropertyMetadata_EntityId";
        private const string PropertyMetadataEntityUniqueIndex = "idxPropertyMetadata_EntityId_PropertyName";
        private const string PartialInvokeMessageEntityIndex = "idxPartialInvokeMessage_SessionId";


        #endregion

        public SyncDao(StorageBase storage)
        {
            Storage = storage;
        }

        #region Properties

        private StorageBase Storage
        {
            get;
            set;
        }

        #endregion

        #region Internal Methods

        public void CreateStorage()
        {
            UpdateTableSchemas();
            CreateTables();
            CreateTableIndexes();
        }

        #endregion

        #region Private Methods

        private void CreateTables()
        {
            if (!Storage.ObjectExists(PropertyMetadataTableName, ObjectType.Table))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    StringBuilder queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE ");
                    queryString.Append(PropertyMetadataTableName + " ( ");
                    queryString.Append(EntityIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL, ");
                    queryString.Append(PropertyNameColumn);
                    queryString.Append(" nvarchar(512) NOT NULL, ");
                    queryString.Append(IsChangedColumn);
                    queryString.Append(" bit NOT NULL )");
                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }

            if (!Storage.ObjectExists(PartialEntityTableName, ObjectType.Table))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    StringBuilder queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE ");
                    queryString.Append(PartialEntityTableName + " ( ");
                    queryString.Append(EntityIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL, ");
                    queryString.Append(SessionIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL, ");
                    queryString.Append(PacketNumberColumn);
                    queryString.Append(" [int] NOT NULL, ");
                    queryString.Append(PartialEntityColumn);
                    queryString.Append(" ntext NOT NULL, ");
                    queryString.Append(InsertedOnColumn);
                    queryString.Append(" DateTime NOT NULL )");
                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }

            if (!Storage.ObjectExists(MetadataTableName, ObjectType.Table))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    StringBuilder queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE ");
                    queryString.Append(MetadataTableName + " ( ");
                    queryString.Append(EntityTypeColumn);
                    queryString.Append(" nvarchar(512) NOT NULL, ");
                    queryString.Append(EntityIdColumn);
                    queryString.Append(" [uniqueidentifier] PRIMARY KEY, ");
                    queryString.Append(EntityVersionColumn);
                    queryString.Append(" [int] NOT NULL, ");
                    queryString.Append(EntityStatusColumn);
                    queryString.Append(" [int] NOT NULL, ");
                    queryString.Append(SessionIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL, ");
                    queryString.Append(ChangeIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL ) ");
                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }

            if (!Storage.ObjectExists(PartialInvokeMessageTableName, ObjectType.Table))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    StringBuilder queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE ");
                    queryString.Append(PartialInvokeMessageTableName + " ( ");
                    queryString.Append(SessionIdColumn);
                    queryString.Append(" [uniqueidentifier] NOT NULL, ");
                    queryString.Append(PacketNumberColumn);
                    queryString.Append(" [int] NOT NULL, ");
                    queryString.Append(PartialInvokeMessageColumn);
                    queryString.Append(" ntext NOT NULL ) ");
                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }
        }

        private void UpdateTableSchemas()
        {
            try
            {
                if (Storage.ObjectExists(MetadataTableName, ObjectType.Table))
                {
                    if (!Storage.ObjectExists(new StorageQueryParameter
                    {
                        ParentName = MetadataTableName,
                        ObjectName = ChangeIdColumn,
                        Type = ObjectType.Column
                    }))
                    {
                        using (IDbCommand command = Storage.CreateCommand())
                        {
                            StringBuilder queryString = new StringBuilder();
                            queryString.Append("ALTER TABLE ");
                            queryString.Append(MetadataTableName);
                            queryString.Append(" ADD ");
                            queryString.Append(ChangeIdColumn);
                            queryString.Append(" [uniqueidentifier] NULL ");
                            command.CommandText = queryString.ToString();
                            Storage.ExecuteNonCommand(command);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception when updating the table schemas", ex);
            }
        }

        private void CreateTableIndexes()
        {
            if (!IndexExists(PartialEntityTableName, PartialEntityIndex))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE INDEX ");
                    queryString.Append(PartialEntityIndex);
                    queryString.Append(" ON ");
                    queryString.Append(PartialEntityTableName);
                    queryString.Append(" ( ");
                    queryString.Append(EntityIdColumn);
                    queryString.Append(" )");

                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }

            if (IndexExists(PropertyMetadataTableName, PropertyMetadataEntityIndex))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("DROP INDEX ");
                    queryString.Append(PropertyMetadataTableName);
                    queryString.Append(".");
                    queryString.Append(PropertyMetadataEntityIndex);

                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }
            if (!IndexExists(PropertyMetadataTableName, PropertyMetadataEntityUniqueIndex))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE UNIQUE INDEX ");
                    queryString.Append(PropertyMetadataEntityUniqueIndex);
                    queryString.Append(" ON ");
                    queryString.Append(PropertyMetadataTableName);
                    queryString.Append(" ( ");
                    queryString.Append(EntityIdColumn);
                    queryString.Append(" , ");
                    queryString.Append(PropertyNameColumn);
                    queryString.Append(" )");

                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }

            if (!IndexExists(PartialInvokeMessageTableName, PartialInvokeMessageEntityIndex))
            {
                using (IDbCommand command = Storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE INDEX ");
                    queryString.Append(PartialInvokeMessageEntityIndex);
                    queryString.Append(" ON ");
                    queryString.Append(PartialInvokeMessageTableName);
                    queryString.Append(" ( ");
                    queryString.Append(SessionIdColumn);
                    queryString.Append(" )");

                    command.CommandText = queryString.ToString();
                    Storage.ExecuteNonCommand(command);
                }
            }
        }

        private bool IndexExists(string table, string indexName)
        {
            var cmdText = "SELECT INDEX_NAME FROM INFORMATION_SCHEMA.INDEXES WHERE TABLE_NAME  = @table AND INDEX_Name = @indexName";

            using (var cmd = Storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(Storage.CreateParameter("@table", table));
                cmd.Parameters.Add(Storage.CreateParameter("@indexName", indexName));

                var result = Storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        #endregion

        #region Internal/Public Methods

        public SyncMetadata GetMetadata(Guid entityId)
        {
            int version = -1;
            int status = -1;
            Guid changeId = Guid.Empty;

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" , ");
            queryString.Append(EntityVersionColumn);
            queryString.Append(" , ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId.ToString());
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            version = int.Parse(reader[EntityVersionColumn].ToString(), CultureInfo.CurrentCulture);
                            status = int.Parse(reader[EntityStatusColumn].ToString());
                            changeId = reader[ChangeIdColumn] == DBNull.Value ? Guid.Empty : new Guid(reader[ChangeIdColumn].ToString());
                        }
                    }
                }
            }

            return new SyncMetadata { Guid = entityId, Status = status, Version = version, ChangeId = changeId };
        }

        public List<EntityMetadata> GetMetadata(ICollection<Guid> ids)
        {
            var metadataList = new List<EntityMetadata>();

            if (ids != null && !ids.Any())
            {
                return metadataList;
            }

            var queryString = new StringBuilder();
            string query;
            queryString.Append("SELECT ");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" , ");
            queryString.Append(EntityVersionColumn);
            queryString.Append(" , ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" , ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            if (ids != null)
            {
                queryString.Append(" WHERE ");
                queryString.Append(EntityIdColumn);
                queryString.Append(" IN ");
                queryString.Append("( {0} )");
                query = string.Format(queryString.ToString(), CreateIdString(ids));
            }
            else
            {
                query = queryString.ToString();
            }

            using (var command = Storage.CreateCommand(query))
            {
                using (var reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadataList.Add(new EntityMetadata
                            {
                                Version = int.Parse(reader[EntityVersionColumn].ToString(), CultureInfo.CurrentCulture),
                                Status = int.Parse(reader[EntityStatusColumn].ToString()),
                                ChangeId = reader[ChangeIdColumn] == DBNull.Value ? Guid.Empty : new Guid(reader[ChangeIdColumn].ToString()),
                                Guid = new Guid(reader[EntityIdColumn].ToString())
                            });
                        }
                    }
                }
            }

            return metadataList;
        }

        public List<Guid> GetMetadataIds(string entityType)
        {
            var idList = new List<Guid>();

            var queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityTypeColumn);
            queryString.Append(" = ");
            queryString.Append("'{0}'");
            var query = string.Format(queryString.ToString(), entityType);

            using (var command = Storage.CreateCommand(query))
            {
                using (var reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            idList.Add(new Guid(reader[EntityIdColumn].ToString()));
                        }
                    }
                }
            }

            return idList;
        }

        private static string CreateIdString(IEnumerable<Guid> ids)
        {
            StringBuilder idStringBuilder = new StringBuilder();
            foreach (var id in ids)
            {
                idStringBuilder.Append("'" + id + "',");
            }

            string idString = idStringBuilder.ToString().Remove(idStringBuilder.ToString().Length - 1, 1);
            return idString;
        }

        public void UpdateSessionIds(ICollection<Guid> entityIds, Guid sessionId)
        {
            if (entityIds == null || entityIds.Count <= 0) return;

            StringBuilder queryString = new StringBuilder();
            queryString.Append(" UPDATE ");
            queryString.Append(MetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" = '");
            queryString.Append(sessionId);
            queryString.Append("' WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append("{0}'");

            List<IDbCommand> commands = new List<IDbCommand>();

            try
            {
                foreach (var id in entityIds)
                {
                    IDbCommand command =
                        Storage.CreateCommand(string.Format(queryString.ToString(), id.ToString()));

                    commands.Add(command);
                }

                Storage.ExecuteNonCommands(commands.ToArray());
            }
            finally
            {
                foreach (IDbCommand dbCommand in commands)
                {
                    dbCommand.Dispose();
                }
            }
        }

        public Guid GetSessionId(Guid entityId)
        {
            Guid sessionId = Guid.Empty;
            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append("='");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            sessionId = new Guid(reader[SessionIdColumn].ToString());
                        }
                    }
                }
            }

            return sessionId;
        }

        public Guid GetChangeId(Guid entityId)
        {
            Guid changedId = Guid.Empty;
            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append("='");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            changedId = reader[ChangeIdColumn] == DBNull.Value ? Guid.Empty : new Guid(reader[ChangeIdColumn].ToString());
                        }
                    }
                }
            }

            return changedId;
        }

        public bool IsExistInMetadata(Guid entityId)
        {
            bool result = false;

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityVersionColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append("='");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        public void DeleteMetadata(Guid entityId, string entityType)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("DELETE ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" ='");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append(" ='");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void InsertPartialEntity(Guid entityId, Guid sessionId, int packetNumber, SyncEntityData syncObject)
        {
            string commandString = "INSERT INTO  [" + PartialEntityTableName + "] "
                 + " ([" + EntityIdColumn + "],[" + SessionIdColumn + "],[" + PacketNumberColumn + "],[" + PartialEntityColumn + "],[" + InsertedOnColumn + "]) "
                 + " VALUES (@EntityIdColumn, @SessionIdColumn, @PacketNumberColumn, @PartialEntityColumn,@InsertedOnColumn); ";

            using (IDbCommand command = Storage.CreateCommand(commandString))
            {
                command.Parameters.Add(Storage.CreateParameter("@EntityIdColumn", entityId));
                command.Parameters.Add(Storage.CreateParameter("@SessionIdColumn", sessionId));
                command.Parameters.Add(Storage.CreateParameter("@PacketNumberColumn", packetNumber));
                command.Parameters.Add(Storage.CreateParameter("@PartialEntityColumn", syncObject.Object));
                command.Parameters.Add(Storage.CreateParameter("@InsertedOnColumn", DateTime.Now));
                Storage.ExecuteNonCommand(command);
            }
        }

        public int GetPartialEntityCount(Guid entityId, Guid sessionId)
        {
            int count = 0;

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT COUNT(");
            queryString.Append(PacketNumberColumn);
            queryString.Append(") AS NoOfPackets FROM ");
            queryString.Append(PartialEntityTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" ='");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" ='");
            queryString.Append(sessionId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            count = int.Parse(reader["NoOfPackets"].ToString(), CultureInfo.CurrentCulture);
                        }
                    }
                }
            }
            return count;
        }

        public List<string> GetPartialEntities(Guid entityId, Guid sessionId)
        {
            List<string> partialEntities = new List<string>();

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(PartialEntityColumn);
            queryString.Append(" FROM ");
            queryString.Append(PartialEntityTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" ='");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" ='");
            queryString.Append(sessionId);
            queryString.Append("'");
            queryString.Append(" ORDER BY ");
            queryString.Append(PacketNumberColumn);
            queryString.Append(" ASC");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            partialEntities.Add(reader[PartialEntityColumn].ToString());
                        }
                    }
                }
            }

            return partialEntities;
        }

        public void InsertMetadata(string entityType, Guid entityId, int version, SyncStatus syncStatus)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("INSERT INTO {0} VALUES ('{1}','{2}',{3},{4},'{5}','{6}')");
            string sqlCommand = String.Format(queryString.ToString(), MetadataTableName, entityType, entityId,
                version, ((int)syncStatus).ToString(CultureInfo.CurrentCulture), Guid.Empty, Guid.NewGuid());

            using (IDbCommand command = Storage.CreateCommand(sqlCommand))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void InsertPropertyMetadata(Guid entityId, IEnumerable<PropertyInfo> properties, PropertySyncStatus propertySyncStatus)
        {
            List<IDbCommand> commands = new List<IDbCommand>();

            StringBuilder queryString = new StringBuilder();
            queryString.Append("INSERT INTO ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" VALUES ('{0}','{1}',{2})");

            try
            {
                foreach (PropertyInfo propertyInfo in properties)
                {
                    using (IDbCommand command = Storage.CreateCommand(string.Format(queryString.ToString(), entityId, propertyInfo.Name, (int)propertySyncStatus)))
                    {
                        Storage.ExecuteNonCommand(command);
                    }
                    //commands.Add(Storage.CreateCommand(string.Format(queryString.ToString(), entityId, propertyInfo.Name,
                    //                                        (int)propertySyncStatus)));
                }

                //Storage.ExecuteNonCommands(commands.ToArray());
            }
            finally
            {
                //foreach (IDbCommand dbCommand in commands)
                //{
                //    dbCommand.Dispose();
                //}
            }
        }

        public List<string> GetChangedProperties(Guid entityId)
        {
            List<String> changedProperties = new List<String>();

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(PropertyNameColumn);
            queryString.Append(" FROM ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" ='");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(IsChangedColumn);
            queryString.Append(" = ");
            queryString.Append((int)PropertySyncStatus.IsChanged);

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            changedProperties.Add(reader[PropertyNameColumn].ToString());
                        }
                    }
                }
            }

            return changedProperties;
        }

        public void GetChangedProperties(ICollection<Guid> ids, Dictionary<Guid, EntityMetadata> currentData)
        {
            string idString = CreateIdString(ids);

            var queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(PropertyNameColumn);
            queryString.Append(" , ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(IsChangedColumn);
            queryString.Append(" = '");
            queryString.Append((int)PropertySyncStatus.IsChanged);
            queryString.Append("' AND ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" IN ");
            queryString.Append("( {0} )");

            var query = String.Format(queryString.ToString(), idString);

            using (var command = Storage.CreateCommand(query))
            {
                using (var reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            var id = new Guid(reader[EntityIdColumn].ToString());
                            EntityMetadata metadata;
                            currentData.TryGetValue(id, out metadata);

                            if (metadata != null)
                            {
                                metadata.Properties.Add(reader[PropertyNameColumn].ToString());
                            }
                            else
                            {
                                if (!currentData.ContainsKey(id))
                                {
                                    metadata = new EntityMetadata();
                                    metadata.Properties.Add(reader[PropertyNameColumn].ToString());
                                    currentData[id] = metadata;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void UpdateMetadata(string entityType, Guid entityId, int version)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append(" UPDATE ");
            queryString.Append(MetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(EntityVersionColumn);
            queryString.Append(" = ");
            queryString.Append(version);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append(" = '");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void UpdateMetadata(string entityType, Guid entityId, int version, SyncStatus status)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append(" UPDATE ");
            queryString.Append(MetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(EntityVersionColumn);
            queryString.Append(" = ");
            queryString.Append(version);
            queryString.Append(",");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" = ");
            queryString.Append((int)status);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append(" = '");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void UpdateMetadata(string entityType, Guid entityId, SyncStatus status, Guid changeId)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append(" UPDATE ");
            queryString.Append(MetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" = ");
            queryString.Append((int)status);
            queryString.Append(" , ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" = '");
            queryString.Append(changeId);
            queryString.Append("' WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append(" = '");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void MarkPropertiesAsSynchronized(Guid entityId, List<string> properties)
        {
            UpdatePropertyMetadata(entityId, properties, PropertySyncStatus.Synchronized);
        }

        private void UpdatePropertyMetadata(Guid entityId, List<String> properties, PropertySyncStatus syncStatus)
        {
            if (properties == null || properties.Count <= 0) return;

            StringBuilder queryString = new StringBuilder();

            queryString.Append(" UPDATE ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(IsChangedColumn);
            queryString.Append(" = ");
            queryString.Append((int)syncStatus);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(PropertyNameColumn);
            queryString.Append(" = '{0}'");

            List<IDbCommand> commands = new List<IDbCommand>();

            try
            {
                foreach (var property in properties)
                {
                    IDbCommand command =
                        Storage.CreateCommand(string.Format(queryString.ToString(), property));

                    commands.Add(command);
                }

                Storage.ExecuteNonCommands(commands.ToArray());
            }
            finally
            {
                foreach (IDbCommand dbCommand in commands)
                {
                    dbCommand.Dispose();
                }
            }
        }

        public void MarkAllPropertiesAsSynchronized(Guid entityId)
        {
            MarkAllProperties(entityId, PropertySyncStatus.Synchronized);
        }

        private void MarkAllProperties(Guid entityId, PropertySyncStatus propertySyncStatus)
        {
            StringBuilder queryString = new StringBuilder();

            queryString.Append(" UPDATE ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" SET ");
            queryString.Append(IsChangedColumn);
            queryString.Append(" = ");
            queryString.Append((int)propertySyncStatus);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" = '");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public List<SyncEntityData> GetModifiedEntities(string entityType)
        {
            List<SyncEntityData> metadataCollection = new List<SyncEntityData>();
            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityIdColumn);
            queryString.Append(",");
            queryString.Append(EntityVersionColumn);
            queryString.Append(",");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" , ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityStatusColumn);
            queryString.Append("!=");
            queryString.Append((int)SyncStatus.Synchronized);
            queryString.Append(" AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append("='");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            SyncEntityData metadata = new SyncEntityData();
                            metadata.Version = int.Parse(reader[EntityVersionColumn].ToString(), CultureInfo.CurrentCulture);
                            metadata.Status = int.Parse(reader[EntityStatusColumn].ToString());
                            metadata.Guid = new Guid(reader[EntityIdColumn].ToString());
                            metadata.ChangeId = reader[ChangeIdColumn] == DBNull.Value ? Guid.Empty : new Guid(reader[ChangeIdColumn].ToString());
                            metadataCollection.Add(metadata);
                        }
                    }
                }
            }

            return metadataCollection;
        }

        public List<SyncMetadata> GetDeletedEntities(string entityType)
        {
            List<SyncMetadata> metadataCollection = new List<SyncMetadata>();
            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityIdColumn);
            queryString.Append(",");
            queryString.Append(EntityVersionColumn);
            queryString.Append(",");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" , ");
            queryString.Append(ChangeIdColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityStatusColumn);
            queryString.Append("=");
            queryString.Append((int)SyncStatus.Deleted);
            queryString.Append(" AND ");
            queryString.Append(EntityTypeColumn);
            queryString.Append("='");
            queryString.Append(entityType);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            SyncMetadata metadata = new SyncMetadata();
                            metadata.Version = int.Parse(reader[EntityVersionColumn].ToString(), CultureInfo.CurrentCulture);
                            metadata.Status = int.Parse(reader[EntityStatusColumn].ToString());
                            metadata.Guid = new Guid(reader[EntityIdColumn].ToString());
                            metadata.ChangeId = reader[ChangeIdColumn] == DBNull.Value ? Guid.Empty : new Guid(reader[ChangeIdColumn].ToString());
                            metadataCollection.Add(metadata);
                        }
                    }
                }
            }

            return metadataCollection;
        }

        public int GetStatusForId(Guid entityId)
        {
            int status = -1;

            StringBuilder queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(EntityStatusColumn);
            queryString.Append(" FROM ");
            queryString.Append(MetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append("='");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                using (IDataReader reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            status = int.Parse(reader[EntityStatusColumn].ToString(), CultureInfo.CurrentCulture);
                        }
                    }
                }
            }

            return status;
        }

        public void RemovePartialEntities(Guid entityId, Guid sessionId)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("DELETE FROM ");
            queryString.Append(PartialEntityTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append("='");
            queryString.Append(entityId);
            queryString.Append("' AND ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" ='");
            queryString.Append(sessionId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public void MarkPropertiesAsChanged(Guid entityId, List<String> changedProperties)
        {
            UpdatePropertyMetadata(entityId, changedProperties, PropertySyncStatus.IsChanged);
        }

        public void DeleteAllPropertyMetadata(Guid entityId)
        {
            StringBuilder queryString = new StringBuilder();
            queryString.Append("DELETE ");
            queryString.Append(PropertyMetadataTableName);
            queryString.Append(" WHERE ");
            queryString.Append(EntityIdColumn);
            queryString.Append(" ='");
            queryString.Append(entityId);
            queryString.Append("'");

            using (IDbCommand command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public bool InsertPartialInvokeMessage(Guid sessionId, int packetNumber, string knowledge)
        {
            try
            {
                string commandString = "INSERT INTO  [" + PartialInvokeMessageTableName + "] "
                        + " ([" + SessionIdColumn + "],[" + PacketNumberColumn + "],[" + PartialInvokeMessageColumn + "]) "
                        + " VALUES (@SessionIdColumn, @PacketNumberColumn, @PartialInvokeMessageColumn); ";

                using (var command = Storage.CreateCommand(commandString))
                {
                    command.Parameters.Add(Storage.CreateParameter("@SessionIdColumn", sessionId));
                    command.Parameters.Add(Storage.CreateParameter("@PacketNumberColumn", packetNumber));
                    command.Parameters.Add(Storage.CreateParameter("@PartialInvokeMessageColumn", knowledge));
                    Storage.ExecuteNonCommand(command);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void DeletePartialInvokeMessages(Guid sessionId)
        {
            var queryString = new StringBuilder();
            queryString.Append("DELETE FROM ");
            queryString.Append(PartialInvokeMessageTableName);
            queryString.Append(" WHERE ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" ='");
            queryString.Append(sessionId);
            queryString.Append("'");

            using (var command = Storage.CreateCommand(queryString.ToString()))
            {
                Storage.ExecuteNonCommand(command);
            }
        }

        public List<string> GetPartialInvokeMessages(Guid sessionId)
        {
            var partialInvokeMessages = new List<string>();

            var queryString = new StringBuilder();
            queryString.Append("SELECT ");
            queryString.Append(PartialInvokeMessageColumn);
            queryString.Append(" FROM ");
            queryString.Append(PartialInvokeMessageTableName);
            queryString.Append(" WHERE ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" = '");
            queryString.Append(sessionId.ToString());
            queryString.Append("'");
            queryString.Append(" ORDER BY ");
            queryString.Append(PacketNumberColumn);
            queryString.Append(" ASC ");

            using (var command = Storage.CreateCommand(queryString.ToString()))
            {
                using (var reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            partialInvokeMessages.Add(reader[PartialInvokeMessageColumn].ToString());
                        }
                    }
                }
            }

            return partialInvokeMessages;
        }

        public int GetPartialInvokeMessageCount(Guid sessionId)
        {
            int count = 0;

            var queryString = new StringBuilder();
            queryString.Append("SELECT COUNT (");
            queryString.Append(PacketNumberColumn);
            queryString.Append(") AS NoOfPackets FROM ");
            queryString.Append(PartialInvokeMessageTableName);
            queryString.Append(" WHERE ");
            queryString.Append(SessionIdColumn);
            queryString.Append(" = '");
            queryString.Append(sessionId.ToString());
            queryString.Append("'");

            using (var command = Storage.CreateCommand(queryString.ToString()))
            {
                using (var reader = Storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            count = int.Parse(reader["NoOfPackets"].ToString(), CultureInfo.CurrentCulture);
                        }
                    }
                }
            }

            return count;
        }

        #endregion
    }
}
