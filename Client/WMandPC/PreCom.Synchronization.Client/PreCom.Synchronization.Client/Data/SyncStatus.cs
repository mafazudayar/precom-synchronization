﻿namespace PreCom.Synchronization.Data
{

    internal enum SyncStatus
    {
        /// <summary>
        /// Entity is Synchronized.
        /// </summary>
        Synchronized = 0,
        /// <summary>
        /// Entity is Created. 
        /// </summary>
        Created = 1,
        /// <summary>
        /// Entity is Updated. 
        /// </summary>
        Updated = 2,
        /// <summary>
        /// Entity is Deleted.
        /// </summary>
        Deleted = 4
    }
}
