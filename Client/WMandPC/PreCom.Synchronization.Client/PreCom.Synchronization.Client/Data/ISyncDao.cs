﻿using System;
using System.Collections.Generic;
using System.Reflection;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Data
{
    internal interface ISyncDao
    {
        void CreateStorage();
        SyncMetadata GetMetadata(Guid entityId);
        List<EntityMetadata> GetMetadata(ICollection<Guid> idList);
        List<Guid> GetMetadataIds(string entityType);
        void UpdateSessionIds(ICollection<Guid> entityIds, Guid sessionId);
        Guid GetSessionId(Guid entityId);
        Guid GetChangeId(Guid entityId);
        bool IsExistInMetadata(Guid entityId);
        void DeleteMetadata(Guid entityId, string entityType);
        void InsertPartialEntity(Guid entityId, Guid sessionId, int packetNumber, SyncEntityData syncObject);
        int GetPartialEntityCount(Guid entityId, Guid sessionId);
        List<string> GetPartialEntities(Guid entityId, Guid sessionId);
        void InsertMetadata(string entityType, Guid entityId, int version, SyncStatus syncStatus);
        void InsertPropertyMetadata(Guid entityId, IEnumerable<PropertyInfo> properties, PropertySyncStatus propertySyncStatus);
        List<string> GetChangedProperties(Guid entityId);
        void GetChangedProperties(ICollection<Guid> ids, Dictionary<Guid, EntityMetadata> currentData);
        void UpdateMetadata(string entityType, Guid entityId, int version);
        void UpdateMetadata(string entityType, Guid entityId, int version, SyncStatus status);
        void UpdateMetadata(string entityType, Guid entityId, SyncStatus status, Guid changeId);
        void MarkPropertiesAsSynchronized(Guid entityId, List<string> properties);
        void MarkAllPropertiesAsSynchronized(Guid entityId);
        List<SyncEntityData> GetModifiedEntities(string entityType);
        List<SyncMetadata> GetDeletedEntities(string entityType);
        int GetStatusForId(Guid entityId);
        void RemovePartialEntities(Guid entityId, Guid sessionId);
        void MarkPropertiesAsChanged(Guid entityId, List<String> changedProperties);
        void DeleteAllPropertyMetadata(Guid entityId);
        bool InsertPartialInvokeMessage(Guid sessionId, int packetNumber, string knowledge);
        void DeletePartialInvokeMessages(Guid sessionId);
        List<string> GetPartialInvokeMessages(Guid sessionId);
        int GetPartialInvokeMessageCount(Guid sessionId);
    }
}
