﻿using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using Synchronization.Shared.All.Utils;

namespace PreCom.Synchronization.Core
{
    internal class ServerErrorManager<T> : SynchronizationManager<T>, IServerErrorManager where T : ISynchronizable
    {
        public ServerErrorManager(ISyncDao syncDao, SynchronizedStorageBase<T> storage, SynchronizationModule syncModule, DispatcherBase dispatcher,
                                  int maxDataPacketSize, ILog log)
            : base(syncDao, dispatcher, storage, syncModule, maxDataPacketSize, log)
        {
            dispatcher.ErrorMessageReceived += ErrorMessageHandler;
        }

        private void ErrorMessageHandler(object sender, ErrorMessageEventArgs errorMessageEventArgs)
        {
            ErrorMessage errorMessage = errorMessageEventArgs.ErrorMessage;

            if (typeof(T).Name != errorMessage.ObjectType)
            {
                return;
            }

            NotifyOnSyncEvent(errorMessage.ObjectType,
                errorMessage.EntityId,
                SyncEventReason.ServerError,
                SyncTypeHelper.ToSyncEventType(errorMessage.Type),
                errorMessage.SessionId,
                errorMessage.Information,
                0,
                0);
        }
    }
}
