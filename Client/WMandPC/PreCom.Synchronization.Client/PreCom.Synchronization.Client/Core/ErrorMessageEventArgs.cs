﻿using System;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class ErrorMessageEventArgs : EventArgs
    {
        internal ErrorMessage ErrorMessage { get; set; }
    }
}
