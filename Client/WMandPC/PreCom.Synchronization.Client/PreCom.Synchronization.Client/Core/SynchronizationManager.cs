﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal abstract class SynchronizationManager<T> where T : ISynchronizable
    {
        private readonly SynchronizedStorageBase<T> _storage;
        private readonly SynchronizationModule _syncModule;
        private readonly int _maxDataPacketSize;
        private readonly TypeManager _typeManager;
        private readonly ISyncDao _syncDao;
        private readonly DispatcherBase _dispatcher;
        private readonly LogHelper _logHelper;

        protected ISyncDao SyncDao
        {
            get { return _syncDao; }
        }

        protected SynchronizedStorageBase<T> Storage
        {
            get { return _storage; }
        }

        protected SynchronizationModule SyncModule
        {
            get { return _syncModule; }
        }

        protected TypeManager TypeManager
        {
            get { return _typeManager; }
        }

        protected int MaxDataPacketSize
        {
            get { return _maxDataPacketSize; }
        }

        public DispatcherBase Dispatcher
        {
            get { return _dispatcher; }
        }

        public LogHelper LogHelper
        {
            get { return _logHelper; }
        }

        protected SynchronizationManager(
            ISyncDao syncDao,
            DispatcherBase dispatcher,
            SynchronizedStorageBase<T> storage,
            SynchronizationModule syncModule,
            int maxDataPacketSize,
            ILog log)
        {
            _syncDao = syncDao;
            _dispatcher = dispatcher;
            _storage = storage;
            _syncModule = syncModule;
            _typeManager = syncModule.TypeManager;
            _maxDataPacketSize = maxDataPacketSize;
            _logHelper = new LogHelper(log);
        }

        protected void NotifyOnSyncEvent(string entityType, Guid entityId, SyncEventReason reason,
                                         SyncEventType type, Guid sessionId, string information, int totalEntityCount, int syncedEntityCount)
        {
            try
            {
                SyncInfo syncInfo = new SyncInfo
                    {
                        Reason = reason,
                        Type = type,
                        SessionId = sessionId,
                        Information = information,
                        TotalEntityCount = totalEntityCount,
                        SynchronizedEntityCount = syncedEntityCount
                    };

                if (TypeManager != null)
                {
                    SyncEventDelegate syncEventDelegate = TypeManager.GetSyncEventHandler(entityType);
                    if (syncEventDelegate != null)
                    {
                        syncEventDelegate.Invoke(entityId, syncInfo);
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.LogError(SyncModule, e, LogId.SYNC_SM_E_1, "Exception when invoking the syncEventDelegate");
            }
        }

        protected List<ConflictedPropertyInfo> ResolveReceivedEntity(SyncEntityData syncObject, bool ignoreChangeId)
        {
            lock (SyncModule.GetLock<T>())
            {
                List<ConflictedPropertyInfo> conflictDetailList = new List<ConflictedPropertyInfo>();
                Guid objectId = syncObject.Guid;

                ISynchronizable receivedSyncEntity = (ISynchronizable)XSerializer.Deserialize(syncObject.Object, typeof(T));

                ISynchronizable existingSyncEntity = ReadEntityFromStorage(objectId);

                // If the metadata exist, but not the actual entity, that means, object is deleted at the client. 
                // So we need to insert the entity, property metadata and update the metadata.
                if (existingSyncEntity == null)
                {
                    Storage.Create(new Collection<ISynchronizable> { receivedSyncEntity },
                                   new CreateArgs(typeof(SynchronizationModule).Name));
                    SyncDao.UpdateMetadata(typeof(T).Name, objectId, syncObject.Version, SyncStatus.Synchronized);

                    if (SyncModule.EnablePropertySynchronization)
                    {
                        IEnumerable<PropertyInfo> propertyNames = PropertyHelper.GetProperties(typeof(T)).ToList();
                        foreach (PropertyInfo property in propertyNames)
                        {
                            ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo
                            {
                                Name = property.Name,
                                NewValue = property.GetValue(receivedSyncEntity, null),
                                OldValue = null
                            };

                            conflictDetailList.Add(conflictedProperty);
                        }

                        SyncDao.InsertPropertyMetadata(objectId, propertyNames, PropertySyncStatus.Synchronized);
                    }
                }
                else
                {
                    var metaData = SyncDao.GetMetadata(syncObject.Guid);

                    if (!metaData.IsValid())
                    {
                        foreach (var property in PropertyHelper.GetProperties(typeof(T)))
                        {
                            object newValue = property.GetValue(receivedSyncEntity, null);
                            object oldValue = property.GetValue(existingSyncEntity, null);

                            if (!Equals(newValue, oldValue))
                            {
                                ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo
                                {
                                    Name = property.Name,
                                    NewValue = newValue,
                                    OldValue = oldValue
                                };
                                conflictDetailList.Add(conflictedProperty);
                            }
                        }

                        Storage.Update(receivedSyncEntity, new UpdateArgs(typeof(SynchronizationModule).Name));
                    }
                    else if (metaData.Version == 0)
                    {
                        // In case the object is received even though it has still not being properly pushed.
                        // This can happen if a push response is lost. Server will return empty changed properties
                        // so we need to handle this separately.
                        if (metaData.GetStatus == Operation.Updated)
                        {
                            // Add conflicts for the properties that have been changed at client
                            // since they will be overwritten.
                            List<string> changedPropertiesAtClient = SyncDao.GetChangedProperties(objectId);
                            foreach (string propertyName in changedPropertiesAtClient)
                            {
                                PropertyInfo property = typeof(T).GetProperty(propertyName);

                                if (property != null)
                                {
                                    object newValue = property.GetValue(receivedSyncEntity, null);
                                    object oldValue = property.GetValue(existingSyncEntity, null);

                                    ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo
                                    {
                                        Name = propertyName,
                                        NewValue = newValue,
                                        OldValue = oldValue
                                    };

                                    conflictDetailList.Add(conflictedProperty);
                                }
                            }
                        }

                        Storage.Update(receivedSyncEntity, new UpdateArgs(typeof(SynchronizationModule).Name));
                        UpdateMetadataAsSynchronized(syncObject, ignoreChangeId);
                    }
                    else
                    {
                        List<string> changedPropertiesAtClient = SyncDao.GetChangedProperties(objectId);

                        try
                        {
                            foreach (string propertyName in syncObject.ChangedPropertyNames)
                            {
                                PropertyInfo property = typeof(T).GetProperty(propertyName);

                                if (property != null)
                                {
                                    object newValue = property.GetValue(receivedSyncEntity, null);
                                    object oldValue = property.GetValue(existingSyncEntity, null);

                                    if (changedPropertiesAtClient.Contains(propertyName))
                                    {
                                        ConflictedPropertyInfo conflictedProperty = new ConflictedPropertyInfo
                                        {
                                            Name = propertyName,
                                            NewValue = newValue,
                                            OldValue = oldValue
                                        };

                                        conflictDetailList.Add(conflictedProperty);
                                    }

                                    property.SetValue(existingSyncEntity, newValue, null);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            LogHelper.LogError(SyncModule, e, LogId.SYNC_SM_E_1, "Exception when resolving received entity");
                        }

                        if (syncObject.ChangedPropertyNames.Count > 0)
                        {
                            Storage.Update(existingSyncEntity, new UpdateArgs(typeof(SynchronizationModule).Name));

                            // Update meta data records.
                            // If the number of conflicts are equal to the number of properties
                            // changed at the client side, then all the
                            // changed properties have been overwritten by server values.
                            // Update the meta data record as SyncStatus.SYNCHRONIZED.
                            if (conflictDetailList.Count == changedPropertiesAtClient.Count)
                            {
                                UpdateMetadataAsSynchronized(syncObject, ignoreChangeId);
                            }
                            else
                            {
                                // If the number of conflicts are less than the number of properties
                                // changed at the client side, mark the
                                // conflicted properties as SYNCHRONIZED, but keep the status of the
                                // object as UPDATED.
                                List<string> conflictedPropertyNames = new List<string>();
                                foreach (ConflictedPropertyInfo conflictedProperty in conflictDetailList)
                                {
                                    conflictedPropertyNames.Add(conflictedProperty.Name);
                                }

                                SyncDao.UpdateMetadata(typeof(T).Name, objectId, syncObject.Version, SyncStatus.Updated);
                                SyncDao.MarkPropertiesAsSynchronized(objectId, conflictedPropertyNames);
                            }
                        }
                    }
                }

                return conflictDetailList;
            }
        }

        private ISynchronizable ReadEntityFromStorage(Guid entityId)
        {
            ISynchronizable entity = null;
            List<Guid> syncEntityInfoList = new List<Guid>();
            syncEntityInfoList.Add(entityId);

            try
            {
                ICollection<ISynchronizable> entities = Storage.Read(syncEntityInfoList);
                if (entities != null)
                {
                    foreach (ISynchronizable syncEntity in entities)
                    {
                        entity = syncEntity;
                        break;
                    }
                }
            }
            catch (SynchronizedStorageException e)
            {
                LogHelper.LogError(SyncModule, e, LogId.SYNC_SM_E_2, "Exception when reading entity from storage");
            }

            return entity;
        }

        protected void UpdateMetadataAsSynchronized(SyncMetadata syncObject, bool ignoreChangeId)
        {
            lock (SyncModule.GetLock<T>())
            {
                // Fix for the issue - PC-2229
                if (syncObject.ChangeId == SyncDao.GetChangeId(syncObject.Guid)
                    || ignoreChangeId)
                {
                    SyncDao.UpdateMetadata(typeof(T).Name, syncObject.Guid, syncObject.Version, SyncStatus.Synchronized);

                    SyncDao.MarkAllPropertiesAsSynchronized(syncObject.Guid);
                }
                else
                {
                    // If the changeId of the syncObject is not matched with the changeId in the DB
                    // we do not update the status as synchronized, keep it as it is. 
                    SyncDao.UpdateMetadata(typeof(T).Name, syncObject.Guid, syncObject.Version);
                }
            }
        }

        protected void InsertEntities(List<SyncEntityData> syncObjects)
        {
            lock (SyncModule.GetLock<T>())
            {
                List<ISynchronizable> syncEntities = new List<ISynchronizable>();
                foreach (SyncEntityData syncObject in syncObjects)
                {
                    ISynchronizable syncEntity = (ISynchronizable)XSerializer.Deserialize(syncObject.Object, typeof(T));
                    syncEntity.Guid = syncObject.Guid;
                    syncEntities.Add(syncEntity);
                }

                Storage.Create(syncEntities, new CreateArgs(typeof(SynchronizationModule).Name));
            }
        }

        protected void DeleteMetadata(Guid objectId)
        {
            lock (SyncModule.GetLock<T>())
            {
                SyncDao.DeleteMetadata(objectId, typeof(T).Name);
                SyncDao.DeleteAllPropertyMetadata(objectId);
            }
        }

        protected void DeleteCustomEntity(Guid entityId)
        {
            lock (SyncModule.GetLock<T>())
            {
                SyncMetadata metaObject = SyncDao.GetMetadata(entityId);

                // If the status is deleted, we cannot call the Storage.Delete(), because entity is deleted from the custom
                // tables.
                // If the status is -1, we cannot call the Storage.Delete(), because entity never exist.
                if (metaObject.GetStatus != Operation.Deleted && metaObject.Status != -1)
                {
                    Storage.Delete(entityId, new DeleteArgs(typeof(SynchronizationModule).Name));
                }
            }
        }

        protected string CombinePartialEntities(IEnumerable<string> partialEntityList)
        {
            StringBuilder completeObject = new StringBuilder();
            foreach (string partialEntity in partialEntityList)
            {
                completeObject.Append(partialEntity);
            }

            return completeObject.ToString();
        }

        protected void NotifyConflict(Guid entityId, List<ConflictedPropertyInfo> conflictedProperties)
        {
            if (conflictedProperties == null)
            {
                return;
            }

            // Don't report properties where value has not changed
            List<ConflictedPropertyInfo> conflictedPropertiesToNotify =
                new List<ConflictedPropertyInfo>(from p in conflictedProperties
                                                 where !Equals(p.OldValue, p.NewValue)
                                                 select p);

            if (conflictedPropertiesToNotify.Count <= 0)
            {
                return;
            }

            ConflictNotificationInfo conflictInfo = new ConflictNotificationInfo { EntityId = entityId, Conflicts = conflictedPropertiesToNotify };

            TypeInformation typeInfo = SyncModule.TypeManager.GetTypeInformation<T>();

            if (typeInfo != null && typeInfo.ConflictNotifier != null)
            {
                try
                {
                    typeInfo.ConflictNotifier.Invoke(conflictInfo);
                }
                catch (Exception e)
                {
                    LogHelper.LogError(SyncModule, e, LogId.SYNC_SM_E_1, "Exception when calling conflict notifiers");
                }
            }
        }

        protected void InsertMetadata(string objectType, Guid entityId, int serverVersion, SyncStatus syncStatus)
        {
            lock (SyncModule.GetLock<T>())
            {
                SyncDao.InsertMetadata(objectType, entityId, serverVersion, syncStatus);

                if (SyncModule.EnablePropertySynchronization)
                {
                    PropertySyncStatus propertySyncStatus = PropertySyncStatus.Synchronized;

                    if (syncStatus == SyncStatus.Created)
                    {
                        propertySyncStatus = PropertySyncStatus.IsChanged;
                    }
                    else if (syncStatus == SyncStatus.Synchronized)
                    {
                        propertySyncStatus = PropertySyncStatus.Synchronized;
                    }

                    SyncDao.InsertPropertyMetadata(entityId, PropertyHelper.GetProperties(typeof(T)), propertySyncStatus);
                }
            }
        }

        protected string[] SplitSerializedEntity(string entity, int allowedMaxDataPacketSize)
        {
            int lengthOfEntity = entity.Length;
            int numberOfSplits = lengthOfEntity / allowedMaxDataPacketSize;

            if (lengthOfEntity % allowedMaxDataPacketSize > 0)
            {
                numberOfSplits += 1;
            }

            string[] splits = new string[numberOfSplits];

            for (int i = 0; i < numberOfSplits - 1; i++)
            {
                splits[i] = entity.Substring(i * allowedMaxDataPacketSize, allowedMaxDataPacketSize);
            }

            splits[numberOfSplits - 1] = entity.Substring((numberOfSplits - 1) * allowedMaxDataPacketSize,
                                                entity.Length - ((numberOfSplits - 1) * allowedMaxDataPacketSize));

            return splits;
        }

        protected Dictionary<Guid, EntityMetadata> GetEntityMetadataList(ICollection<Guid> idCollection, bool getProperties)
        {
            var idDictionary = new Dictionary<Guid, EntityMetadata>();
            const int chunkSize = 500;
            var chunk = new List<Guid>();

            var metadataList = new List<EntityMetadata>();
            var idList = new List<Guid>(idCollection);

            // We need to decide which size of args, the sqlce supports. for the moment it is divided into 500 chunks
            if (idList.Count > chunkSize)
            {
                var noOfChunks = idList.Count / chunkSize;
                var y = 0;
                var x = 0;

                for (; x < noOfChunks; x++)
                {
                    chunk.AddRange(idList.GetRange(y, chunkSize));
                    y = y + chunkSize;

                    metadataList.Clear();

                    // Lock only the DB reading segment
                    lock (SyncModule.GetLock<T>())
                    {
                        metadataList.AddRange(SyncDao.GetMetadata(chunk));
                    }

                    foreach (var syncMetadata in metadataList)
                    {
                        if (!idDictionary.ContainsKey(syncMetadata.Guid))
                        {
                            idDictionary.Add(syncMetadata.Guid, syncMetadata);
                        }
                    }

                    if (getProperties)
                    {
                        // Lock only the DB reading segment
                        lock (SyncModule.GetLock<T>())
                        {
                            SyncDao.GetChangedProperties(chunk, idDictionary);
                        }
                    }

                    chunk.Clear();
                }

                if (idList.Count % chunkSize > 0)
                {
                    chunk.AddRange(idList.GetRange(y, idList.Count - y));

                    metadataList.Clear();

                    // Lock only the DB reading segment
                    lock (SyncModule.GetLock<T>())
                    {
                        metadataList.AddRange(SyncDao.GetMetadata(chunk));
                    }

                    foreach (var syncMetadata in metadataList)
                    {
                        if (!idDictionary.ContainsKey(syncMetadata.Guid))
                        {
                            idDictionary.Add(syncMetadata.Guid, syncMetadata);
                        }
                    }

                    if (getProperties)
                    {
                        // Lock only the DB reading segment
                        lock (SyncModule.GetLock<T>())
                        {
                            SyncDao.GetChangedProperties(chunk, idDictionary);
                        }
                    }
                }
            }
            else
            {
                if (idList.Count > 0)
                {
                    chunk.AddRange(idList.GetRange(0, idList.Count));

                    metadataList.Clear();

                    // Lock only the DB reading segment
                    lock (SyncModule.GetLock<T>())
                    {
                        metadataList.AddRange(SyncDao.GetMetadata(chunk));
                    }

                    foreach (var syncMetadata in metadataList)
                    {
                        if (!idDictionary.ContainsKey(syncMetadata.Guid))
                        {
                            idDictionary.Add(syncMetadata.Guid, syncMetadata);
                        }
                    }

                    if (getProperties)
                    {
                        // Lock only the DB reading segment
                        lock (SyncModule.GetLock<T>())
                        {
                            SyncDao.GetChangedProperties(chunk, idDictionary);
                        }
                    }
                }
            }

            return idDictionary;
        }

        protected Dictionary<Guid, EntityMetadata> GetChangedProperties(ICollection<Guid> idCollection)
        {
            var idDictionary = new Dictionary<Guid, EntityMetadata>();

            var chunk = new List<Guid>();
            var i = 0;
            const int chunkSize = 500;
            var idList = new List<Guid>(idCollection);

            // We need to decide which size of args, the sqlce supports. for the moment it is divided into 100 chunks
            if (idList.Count > chunkSize)
            {
                var x = 0;
                var limit = chunkSize;
                var noOfChunks = idList.Count / chunkSize;

                for (; x < noOfChunks; x++)
                {
                    for (; i < limit; i++)
                    {
                        chunk.Add(idList[i]);
                    }

                    lock (SyncModule.GetLock<T>())
                    {
                        SyncDao.GetChangedProperties(chunk, idDictionary);
                    }

                    limit += chunkSize;
                }

                if (idList.Count % chunkSize > 0)
                {
                    for (var j = i; j < idList.Count; j++)
                    {
                        chunk.Add(idList[i]);
                    }

                    lock (SyncModule.GetLock<T>())
                    {
                        SyncDao.GetChangedProperties(chunk, idDictionary);
                    }
                }
            }
            else
            {
                for (; i < idList.Count; i++)
                {
                    chunk.Add(idList[i]);
                }

                lock (SyncModule.GetLock<T>())
                {
                    SyncDao.GetChangedProperties(chunk, idDictionary);
                }
            }

            return idDictionary;
        }
    }
}
