﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class PushManager<T> : SynchronizationManager<T>, IClientPushManager where T : ISynchronizable
    {
private List<SyncEntityData> _sendingSyncEntities = new List<SyncEntityData>();

public PushManager(ISyncDao syncDao, SynchronizedStorageBase<T> storage, SynchronizationModule syncModule, DispatcherBase dispatcher,
            int maxDataPacketSize, ILog log)
            : base(syncDao, dispatcher, storage, syncModule, maxDataPacketSize, log)
        {
            dispatcher.PushResponseReceived += PushResponseHandler;
        }

        #region Internal Methods

        internal PushResult Push(ICollection<Guid> entityIds)
        {
            PushResult result = null;

            lock (SyncModule.GetLock<T>())
            {
                try
                {
                    if (entityIds.Count < 1)
                    {
                        result = Push();
                    }
                    else
                    {
                        result = new PushResult();
                        Guid sessionId = Guid.NewGuid();

                        if (SyncModule.EnablePerformanceLogs)
                        {
                            LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_1, "Performance Logs", "Push request initiated",
                                               "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                            LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_2, "Performance Logs", "Calling custom storage",
                                               string.Format("Method: Read(entityIds), Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, sessionId));
                        }

                        ICollection<ISynchronizable> entities;

                        lock (SyncModule.GetLock<T>())
                        {
                            entities = Storage.Read(entityIds);
                        }

                        if (SyncModule.EnablePerformanceLogs)
                        {
                            LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_3, "Performance Logs", "Custom storage responded",
                                               "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                        }

                        List<SyncEntityData> syncObjects = new List<SyncEntityData>();
                        List<Guid> entitiesNotInStorage = new List<Guid>();

                        // get the synchronizable properties only
                        List<PropertyInfo> propertiesToSync = PropertyHelper.GetProperties(typeof(T));

                        // Get all properties 
                        var allProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();

                        foreach (var entityId in entityIds)
                        {
                            var changedProperties = new List<string>();
                            SyncMetadata syncMetadata = null;
                            bool found = false;

                            if (SyncModule.EnablePropertySynchronization)
                            {
                                // this method should be changed to get only one instead of a collection
                                var metadataCollection = GetEntityMetadataList(new Collection<Guid> { entityId }, true);

                                EntityMetadata metadata;
                                metadataCollection.TryGetValue(entityId, out metadata);

                                if (metadata != null)
                                {
                                    syncMetadata = metadata;
                                    changedProperties.AddRange(metadata.Properties);
                                }
                            }
                            else
                            {
                                syncMetadata = SyncDao.GetMetadata(entityId);
                                changedProperties.AddRange(propertiesToSync.Select(item => item.Name));
                            }

                            if (syncMetadata != null)
                            {
                                if (syncMetadata.Status != (int)Operation.None && syncMetadata.Version != -1 &&
                                    syncMetadata.Status != -1)
                                {
                                    var syncEntityData = new SyncEntityData
                                    {
                                        Guid = syncMetadata.Guid,
                                        Version = syncMetadata.Version,
                                        Status = syncMetadata.Status,
                                        ChangeId = syncMetadata.ChangeId
                                    };
                                    if (IsSyncEntityNotSent(syncEntityData))
                                    {
                                        _sendingSyncEntities.Add(syncEntityData);
                                        foreach (var syncEntity in entities)
                                        {
                                            if (syncEntityData.Guid.Equals(syncEntity.Guid))
                                            {
                                                foreach (var property in changedProperties)
                                                {
                                                    syncEntityData.ChangedPropertyNames.Add(property);
                                                }

                                                syncEntityData.Object = XSerializer.SerializeSyncEntity(syncEntity,
                                                    PropertyHelper.GetExcludedProperties(allProperties,
                                                        changedProperties));

                                                found = true;
                                                break;
                                            }
                                        }

                                        syncObjects.Add(syncEntityData);
                                        result.PushedEntityIds.Add(syncEntityData.Guid);

                                        if (!found && syncMetadata.Status != (int)Operation.Deleted)
                                        {
                                            LogObjectNotFound(syncEntityData.Guid);
                                            entitiesNotInStorage.Add(syncEntityData.Guid);
                                        }
                                    }
                                }
                            }
                        }

                        foreach (var id in entitiesNotInStorage)
                        {
                            Guid id1 = id;
                            syncObjects.RemoveAll(data => data.Guid == id1);
                        }

                        if (syncObjects.Count > 0)
                        {
                            result.SyncSessionId = sessionId;
                            SendPushRequests(syncObjects, sessionId);

                            if (SyncModule.EnablePerformanceLogs)
                            {
                                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_4, "Performance Logs", "Push requests sent",
                                                   "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                            }
                        }
                    }
                }
                catch (SynchronizedStorageException storageException)
                {
                    LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PUSH_E_1, "Storage exception when Pushing");
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(SyncModule, ex, LogId.SYNC_PUSH_E_2, "Exception when Pushing");
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        private PushResult Push()
        {
            PushResult pushResult = new PushResult { SyncSessionId = Guid.NewGuid() };

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_1, "Performance Logs", "Push request initiated",
                                   string.Format("Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, pushResult.SyncSessionId));
            }

            List<SyncEntityData> syncMetadataCollection = GetSyncMetadataCollection();

            if (syncMetadataCollection == null || syncMetadataCollection.Count == 0)
            {
                return new PushResult();
            }

            List<Guid> syncEntityGuidList = new List<Guid>();

            // get the synchronizable properties
            List<PropertyInfo> propertiesToSync = PropertyHelper.GetProperties(typeof(T));

            // Get all properties 
            var allProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();

            foreach (SyncEntityData syncMetaObject in syncMetadataCollection)
            {
                syncEntityGuidList.Add(syncMetaObject.Guid);
            }

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_2, "Performance Logs", "Calling custom storage",
                                   string.Format("Method: Read(entityIds), Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, pushResult.SyncSessionId));
            }

            ICollection<ISynchronizable> entities;
            lock (SyncModule.GetLock<T>())
            {
                entities = Storage.Read(syncEntityGuidList);
            }

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_3, "Performance Logs", "Custom storage responded",
                                   string.Format("Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, pushResult.SyncSessionId));
            }

            var entitiesNotInStorage = new List<Guid>();

            foreach (SyncEntityData syncObject in syncMetadataCollection)
            {
                bool found = false;
                foreach (ISynchronizable entity in entities)
                {
                    if (entity == null)
                    {
                        continue;
                    }

                    if (syncObject.Guid.Equals(entity.Guid))
                    {
                        if (SyncModule.EnablePropertySynchronization)
                        {
                            IEnumerable<string> changedProperties = SyncDao.GetChangedProperties(entity.Guid);
                            foreach (string changedProperty in changedProperties)
                            {
                                syncObject.ChangedPropertyNames.Add(changedProperty);
                            }

                            syncObject.Object = XSerializer.SerializeSyncEntity(entity,
                                PropertyHelper.GetExcludedProperties(allProperties, syncObject.ChangedPropertyNames.ToList()));
                            found = true;
                        }
                        else
                        {
                            foreach (PropertyInfo property in propertiesToSync)
                            {
                                syncObject.ChangedPropertyNames.Add(property.Name);
                            }

                            syncObject.Object = XSerializer.SerializeSyncEntity(entity,
                                PropertyHelper.GetExcludedProperties(allProperties, syncObject.ChangedPropertyNames.ToList()));
                            found = true;
                            break;
                        }
                    }
                }

                pushResult.PushedEntityIds.Add(syncObject.Guid);

                if (!found && syncObject.Status != (int)Operation.Deleted)
                {
                    entitiesNotInStorage.Add(syncObject.Guid);
                    LogObjectNotFound(syncObject.Guid);
                }
            }

            foreach (var id in entitiesNotInStorage)
            {
                Guid id1 = id;
                syncMetadataCollection.RemoveAll(data => data.Guid == id1);
            }

            SendPushRequests(syncMetadataCollection, pushResult.SyncSessionId);

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_4, "Performance Logs", "Push requests sent",
                                   string.Format("Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, pushResult.SyncSessionId));
            }

            return pushResult;
        }

        private List<SyncEntityData> GetSyncMetadataCollection()
        {
            var modifiedEntities = SyncDao.GetModifiedEntities(typeof(T).Name);
            var newModifiedEntities = modifiedEntities.Where(IsSyncEntityNotSent).ToList();
            foreach (var newModifiedEntity in newModifiedEntities)
            {
                _sendingSyncEntities.Add(newModifiedEntity);
            }
            return newModifiedEntities;
        }

        private bool IsSyncEntityNotSent(SyncEntityData syncEntityDataToCheck)
        {
            bool notSent = true;
            var sendingSyncObject = _sendingSyncEntities.Where(k => k.Guid == syncEntityDataToCheck.Guid).FirstOrDefault();
            if (sendingSyncObject != null)
            {
                if (sendingSyncObject.ChangeId == syncEntityDataToCheck.ChangeId)
                {
                    notSent = false;
                }
            }
            return notSent;
        }

        private void SendPushRequests(List<SyncEntityData> syncObjects, Guid sessionId)
        {
            if (syncObjects.Count <= 0)
            {
                return;
            }

            List<SyncEntityData> objectsToBeSent = new List<SyncEntityData>();
            List<Guid> idList = new List<Guid>();
            int filledSizeOfTheCurrentBucket = 0;

            foreach (SyncEntityData syncObject in syncObjects)
            {
                idList.Add(syncObject.Guid);
                NotifyOnSyncEvent(typeof(T).Name, syncObject.Guid, SyncEventReason.Started, SyncEventType.ClientPush,
                                  sessionId, string.Empty, 0, 0);

                string serializedSyncObject = XSerializer.SerializeSyncEntity(syncObject);
                int lengthOfSerializedSyncObject = serializedSyncObject.Length;

                // If the size of the entity is larger than the maximum size
                // allowed, the entity is send as multiple PushRequests to the
                // server.
                if (lengthOfSerializedSyncObject > MaxDataPacketSize)
                {
                    string[] entitySplits = SplitSerializedEntity(syncObject.Object, MaxDataPacketSize);
                    //// Functionality to send multiple push requests for a single
                    //// object when the object size is larger than
                    //// MaxDataPacketSize.

                    for (int i = 0; i < entitySplits.Length; i++)
                    {
                        SyncEntityData syncObjectForObjectPartition = new SyncEntityData
                            {
                                Guid = syncObject.Guid,
                                Version = syncObject.Version,
                                ChangeId = syncObject.ChangeId,
                                Object = entitySplits[i]
                            };

                        syncObjectForObjectPartition.Status = syncObject.Status;

                        foreach (string changedPropertyName in syncObject.ChangedPropertyNames)
                        {
                            syncObjectForObjectPartition.ChangedPropertyNames.Add(changedPropertyName);
                        }

                        List<SyncEntityData> syncObjectList = new List<SyncEntityData>();
                        syncObjectList.Add(syncObjectForObjectPartition);

                        SendPushRequest(syncObjectList, sessionId, typeof(T).Name, true,
                                        entitySplits.Length, i);
                    }
                }
                else
                {
                    if (filledSizeOfTheCurrentBucket + lengthOfSerializedSyncObject > MaxDataPacketSize)
                    {
                        SendPushRequest(objectsToBeSent, sessionId, typeof(T).Name,
                                        false, 0, 0);
                        objectsToBeSent.Clear();
                        objectsToBeSent.Add(syncObject);
                        filledSizeOfTheCurrentBucket = lengthOfSerializedSyncObject;
                    }
                    else
                    {
                        objectsToBeSent.Add(syncObject);
                        filledSizeOfTheCurrentBucket += lengthOfSerializedSyncObject;
                    }
                }
            }

            if (objectsToBeSent.Count > 0)
            {
                SendPushRequest(objectsToBeSent, sessionId, typeof(T).Name, false, 0, 0);
            }

            lock (SyncModule.GetLock<T>())
            {
                SyncDao.UpdateSessionIds(idList, sessionId);
            }
        }

        private void SendPushRequest(IEnumerable<SyncEntityData> objectsToBeSent, Guid sessionId, string objectType,
            bool isSplit, int totalNumberOfPackets, int packetNumber)
        {
            PushRequest pushRequest = new PushRequest();
            pushRequest.ObjectType = objectType;
            pushRequest.SessionGuid = sessionId;
            pushRequest.IsSplit = isSplit;
            pushRequest.PacketNumber = packetNumber;
            pushRequest.TotalNumberOfPackets = totalNumberOfPackets;

            foreach (SyncEntityData syncObject in objectsToBeSent)
            {
                pushRequest.SynchronizedObjects.Add(syncObject);
            }

            Dispatcher.SendMessage(pushRequest, SyncModule);
        }

        private void LogObjectNotFound(Guid id)
        {
            string body = string.Format("Object with Guid {0} was not found in storage", id);
            LogHelper.LogError(SyncModule, LogId.SYNC_PUSH_E_3, "Object not found when pushing", body, null);
        }

        private void PushResponseHandler(object sender, PushResponseEventArgs pushResponseArgs)
        {
            PushResponse pushResponse = pushResponseArgs.PushResponse;

            if (typeof(T).Name != pushResponse.ObjectType)
            {
                return;
            }

            Guid sessionId = pushResponse.SessionGuid;

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_5, "Performance Logs", "Push response received",
                                   pushResponse.GetDebugString());
            } 

            //remove sent items from sent entities
            RemoveObjectFromSendingList(pushResponse);

            lock (SyncModule.GetLock<T>())
            {
                try
                {
                    if (pushResponse.IsSplit)
                    {
                        if (pushResponse.ResolvedObjects.Count > 0)
                        {
                            SyncEntityData syncObject = pushResponse.ResolvedObjects[0];
                            Guid entityId = syncObject.Guid;

                            Guid mostRecentSessionId = SyncDao.GetSessionId(entityId);

                            if (mostRecentSessionId.Equals(sessionId) || mostRecentSessionId.Equals(Guid.Empty))
                            {
                                SyncDao.InsertPartialEntity(entityId, sessionId,
                                                            pushResponse.PacketNumber,
                                                            syncObject);

                                int numberOfPacketsOnDb = SyncDao.GetPartialEntityCount(entityId, sessionId);

                                if (numberOfPacketsOnDb == pushResponse.TotalNumberOfPackets)
                                {
                                    List<string> partialEntityList = SyncDao.GetPartialEntities(entityId, sessionId);

                                    syncObject.Object = CombinePartialEntities(partialEntityList);

                                    try
                                    {
                                        ManageConflicts(entityId, syncObject);

                                        NotifyOnSyncEvent(typeof(T).Name, syncObject.Guid, SyncEventReason.Synchronized,
                                                          SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                    }
                                    catch (SynchronizedStorageException e)
                                    {
                                        NotifyOnSyncEvent(typeof(T).Name, syncObject.Guid, SyncEventReason.Error,
                                                          SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                        LogHelper.LogError(SyncModule, e, LogId.SYNC_PUSH_E_1,
                                                           string.Format("SynchronizedStorageException when push response received, sessionId: {0}", sessionId));
                                    }
                                    finally
                                    {
                                        SyncDao.RemovePartialEntities(entityId, sessionId);
                                    }

                                    if (SyncModule.EnablePerformanceLogs)
                                    {
                                        LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_6, "Performance Logs", "Push response completed",
                                                           string.Format("Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, sessionId));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (SyncEntityData syncObject in pushResponse.ResolvedObjects)
                        {
                            Guid id = syncObject.Guid;
                            Guid mostRecentSessionId = SyncDao.GetSessionId(id);

                            if (mostRecentSessionId.Equals(sessionId))
                            {
                                try
                                {
                                    if (syncObject.Status != (int)SyncStatus.Deleted)
                                    {
                                        ManageConflicts(id, syncObject);
                                    }
                                    else
                                    {
                                        // here we delete the data with out considering the enablepropertysynchronization
                                        DeleteCustomEntity(id);
                                        DeleteMetadata(id);
                                    }

                                    NotifyOnSyncEvent(typeof(T).Name, syncObject.Guid, SyncEventReason.Synchronized,
                                                      SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                }
                                catch (SynchronizedStorageException e)
                                {
                                    NotifyOnSyncEvent(typeof(T).Name, syncObject.Guid, SyncEventReason.Error,
                                                      SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                    LogHelper.LogError(SyncModule, e, LogId.SYNC_PUSH_E_1,
                                                       string.Format("SynchronizedStorageException when push response received, sessionId: {0}", sessionId));
                                }
                            }
                        }

                        foreach (SyncMetadata syncEntityDatObject in pushResponse.AcceptedObjects)
                        {
                            Guid id = syncEntityDatObject.Guid;
                            Guid mostRecentSessionId = SyncDao.GetSessionId(id);
                            if (mostRecentSessionId.Equals(sessionId))
                            {
                                if (syncEntityDatObject.Status != (int)SyncStatus.Deleted)
                                {
                                    UpdateMetadataAsSynchronized(syncEntityDatObject, false);
                                    NotifyOnSyncEvent(typeof(T).Name, id, SyncEventReason.Synchronized,
                                                      SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                }
                                else
                                {
                                    DeleteMetadata(id);
                                    NotifyOnSyncEvent(typeof(T).Name, id, SyncEventReason.Synchronized,
                                                      SyncEventType.ClientPush, sessionId, string.Empty, 0, 0);
                                }
                            }
                        }
                    }

                    if (SyncModule.EnablePerformanceLogs && !pushResponse.IsSplit)
                    {
                        LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_6, "Performance Logs", "Push response completed",
                                           "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(SyncModule, ex, LogId.SYNC_PUSH_E_2, string.Format("Exception when push response is received, sessionId: {0}", sessionId.ToString()));
                }
            }
        }

        private void ManageConflicts(Guid entity, SyncEntityData syncObject)
        {
            if (SyncDao.GetStatusForId(entity) != (int)SyncStatus.Deleted)
            {
                List<ConflictedPropertyInfo> conflictedProperties = ResolveReceivedEntity(syncObject, false);
                NotifyConflict(entity, conflictedProperties);
            }
            else
            {
                RollBackDeleteOperation(syncObject);
            }
        }

        private void RollBackDeleteOperation(SyncEntityData syncObject)
        {
            lock (SyncModule.GetLock<T>())
            {
                // Fix for the issue - PC-2229
                if (syncObject.ChangeId == SyncDao.GetChangeId(syncObject.Guid))
                {
                    InsertEntities(new List<SyncEntityData>() { syncObject });
                    SyncDao.UpdateMetadata(typeof(T).Name, syncObject.Guid, syncObject.Version, SyncStatus.Synchronized);
                }
            }
        }


        private void RemoveObjectFromSendingList(PushResponse pushResponse)
        {
            for (int sendingObjectIndex = _sendingSyncEntities.Count - 1; sendingObjectIndex >= 0; sendingObjectIndex--)
            {
                //remove if in any list
                var sendingEntity = _sendingSyncEntities[sendingObjectIndex];
                bool existsInAcceptedObjects = pushResponse.AcceptedObjects.Count(e =>
                {
                    if (e.Guid == sendingEntity.Guid
                        && e.ChangeId == sendingEntity.ChangeId)
                    {
                        return true;
                    }
                    return false;
                }) > 0;
                bool existsInResolvedObjects = pushResponse.ResolvedObjects.Count(e =>
                {
                    if (e.Guid == sendingEntity.Guid
                        && e.ChangeId == sendingEntity.ChangeId)
                    {
                        return true;
                    }
                    return false;
                }) > 0;

                if (existsInAcceptedObjects || existsInResolvedObjects)
                {
                    _sendingSyncEntities.Remove(sendingEntity);
                }
            }
        }

        #endregion
    }
}
