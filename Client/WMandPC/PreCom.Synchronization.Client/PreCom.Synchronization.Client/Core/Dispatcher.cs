﻿using System;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Modules;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class Dispatcher : DispatcherBase
    {
        private readonly CommunicationBase _communication;
        private readonly LogHelper _logHelper;

        // Callback currently intended for PreComLT to get send / receive activity.
        private readonly Action _messageSentCallback;

        protected Dispatcher()
        {
        }

        public Dispatcher(CommunicationBase communication, Action messageSentCallback, ILog log)
        {
            _communication = communication;
            _communication.Register<PullResponse>(OnPullResponseReceived);
            _communication.Register<PullInvokeMessage>(OnPullInvokeReceived);
            _communication.Register<PushResponse>(OnPushResponseReceived);
            _communication.Register<ErrorMessage>(OnErrorMessageReceived);

            _messageSentCallback = messageSentCallback;
            _logHelper = new LogHelper(log);
        }

        public override void SendMessage(EntityBase entity, SynchronizationModule syncModule)
        {
            if (_communication.IsLoggedIn)
            {
                try
                {
#if LT
                    if (!_communication.IsConnected)
                    {
                        _logHelper.LogError(syncModule, LogId.SYNC_COM_E_1, "SendMessage", "Communication not ready for client " + _communication.GetClient().ClientID, "");
                    }

                    var requestArgs = new RequestArgs(QueuePriority.Low, Persistence.ConnectionSession);
#else
                    var requestArgs = new RequestArgs(QueuePriority.Low, Persistence.UntilDelivered);
#endif

                    _communication.Send(entity, requestArgs);

                    if (_messageSentCallback != null)
                    {
                        _messageSentCallback();
                    }
                }
                catch (Exception ex)
                {
                    _logHelper.LogError(syncModule, ex, LogId.SYNC_COM_E_1, "Exception in SendMessage");
                }
            }
        }
    }
}
