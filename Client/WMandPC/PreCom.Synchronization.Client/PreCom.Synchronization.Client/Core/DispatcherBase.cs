using System;
using System.Threading;
using PreCom.Core.Communication;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal abstract class DispatcherBase
    {
        public event EventHandler<PullResponseEventArgs> PullResponseReceived;
        public event EventHandler<PushResponseEventArgs> PushResponseReceived;
        public event EventHandler<PullInvokeEventArgs> PullInvokeReceived;
        public event EventHandler<ErrorMessageEventArgs> ErrorMessageReceived;

        protected virtual void OnPullResponseReceived(PullResponse pullResponse)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                if (PullResponseReceived != null)
                {
                    PullResponseReceived.Invoke(this, new PullResponseEventArgs
                    {
                        PullResponse = pullResponse
                    });
                }
            });
        }

        protected virtual void OnPullInvokeReceived(PullInvokeMessage pullInvokeMessage)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                if (PullInvokeReceived != null)
                {
                    PullInvokeReceived.Invoke(this, new PullInvokeEventArgs
                    {
                        PullInvokeMessage = pullInvokeMessage
                    });
                }
            });
        }

        protected virtual void OnPushResponseReceived(PushResponse pushResponse)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                if (PushResponseReceived != null)
                {
                    PushResponseReceived.Invoke(this, new PushResponseEventArgs
                    {
                        PushResponse = pushResponse
                    });
                }
            });
        }

        protected virtual void OnErrorMessageReceived(ErrorMessage errorMessage)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                if (ErrorMessageReceived != null)
                {
                    ErrorMessageReceived.Invoke(this, new ErrorMessageEventArgs
                    {
                        ErrorMessage = errorMessage
                    });
                }
            });
        }

        public abstract void SendMessage(EntityBase entity, SynchronizationModule syncModule);
    }
}