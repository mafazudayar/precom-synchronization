﻿using System;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class PullInvokeEventArgs : EventArgs
    {
        internal PullInvokeMessage PullInvokeMessage { get; set; }
    }
}
