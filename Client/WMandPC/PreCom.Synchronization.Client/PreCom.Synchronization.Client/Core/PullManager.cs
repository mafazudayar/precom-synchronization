﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class PullManager<T> : SynchronizationManager<T>, IClientPullManager where T : ISynchronizable
    {
        private readonly Dictionary<Guid, int> _pullResponsesSessions = new Dictionary<Guid, int>();

        #region Constructor

        public PullManager(ISyncDao syncDao, SynchronizedStorageBase<T> storage, SynchronizationModule syncModule, DispatcherBase dispatcher,
            int maxDataPacketSize, ILog log)
            : base(syncDao, dispatcher, storage, syncModule, maxDataPacketSize, log)
        {
            dispatcher.PullResponseReceived += PullResponseHandler;
            dispatcher.PullInvokeReceived += PullInvokeMessageHandler;
        }

        #endregion

        #region Private fields

        private int _partialMsgCount;

        #endregion

        #region Public Methods

        public PullResult Pull(PullArgs pullArgs, SyncEventType eventType)
        {
            lock (SyncModule.GetLock<T>())
            {
                var clientIdList = new List<Guid>();
                clientIdList.AddRange(Storage.ReadIds(new EntityFilter()));
                DeleteMetadataNotInList(clientIdList);
            }

            return PullImpl(pullArgs, eventType);
        }

        private PullResult PullImpl(PullArgs pullArgs, SyncEventType eventType)
        {
            PullResult result = new PullResult();

            try
            {
                Guid sessionId = Guid.NewGuid();
                result.SyncSession = sessionId;

                PullDeletedEntities(sessionId, eventType);

                if (SyncModule.EnablePerformanceLogs)
                {
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_1, "Performance Logs", "Pull request initiated",
                                       "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_2, "Performance Logs", "Calling custom storage",
                                       string.Format("Method: ReadIds(EntityFilter filter), Object Type: {0}, Sync Session Id: {1}", typeof(T).Name, sessionId));
                }

                ICollection<Guid> entityIdList;

                lock (SyncModule.GetLock<T>())
                {
                    entityIdList = Storage.ReadIds(pullArgs.Filter);
                }

                if (SyncModule.EnablePerformanceLogs)
                {
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_3, "Performance Logs", "Custom storage responded",
                                       "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                }

                List<SyncMetadata> metadataList = new List<SyncMetadata>();
                ICollection<Guid> entityIdListToPull = new Collection<Guid>();

                if (entityIdList != null)
                {
                    var metadataCollection = GetEntityMetadataList(entityIdList, false);

                    foreach (var entityId in entityIdList)
                    {
                        EntityMetadata metadata;
                        metadataCollection.TryGetValue(entityId, out metadata);

                        if (metadata != null)
                        {
                            metadataList.Add(new SyncMetadata
                               {
                                   ChangeId = metadata.ChangeId,
                                   Guid = metadata.Guid,
                                   Status = metadata.Status,
                                   Version = metadata.Version
                               });

                            entityIdListToPull.Add(entityId);
                        }
                    }

                    Pull(metadataList, eventType, sessionId, pullArgs, entityIdListToPull);
                }

                if (SyncModule.EnablePerformanceLogs)
                {
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_4, "Performance Logs", "Pull requests sent",
                                       "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                }
            }
            catch (SynchronizedStorageException storageException)
            {
                LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1, "Storage exception when pulling");
            }
            catch (Exception ex)
            {
                LogHelper.LogError(SyncModule, ex, LogId.SYNC_PULL_E_2, "Exception when when pulling");
            }

            return result;
        }

        #endregion

        #region Private methods

        private static void CreateSyncEntityCollection(IEnumerable<Guid> chunkList,
                                               IDictionary<Guid, EntityMetadata> metadataCollection,
                                               ICollection<Guid> receivedIdList,
                                              ICollection<SyncMetadata> listOfSyncObjects)
        {
            foreach (var clientEntityId in chunkList)
            {
                EntityMetadata entityMetadata;
                metadataCollection.TryGetValue(clientEntityId, out entityMetadata);

                if (entityMetadata != null)
                {
                    if (!receivedIdList.Contains(clientEntityId))
                    {
                        receivedIdList.Add(clientEntityId);
                    }

                    var syncMetadata = new SyncMetadata
                    {
                        Guid = entityMetadata.Guid,
                        Version = entityMetadata.Version,
                        Status = entityMetadata.Status,
                        ChangeId = entityMetadata.ChangeId
                    };

                    listOfSyncObjects.Add(syncMetadata);
                }
                else
                {
                    entityMetadata = new EntityMetadata { Version = 0, Status = -1, Guid = clientEntityId };

                    var syncMetadata = new SyncMetadata
                    {
                        Guid = entityMetadata.Guid,
                        Version = entityMetadata.Version,
                        Status = entityMetadata.Status,
                        ChangeId = entityMetadata.ChangeId
                    };

                    listOfSyncObjects.Add(syncMetadata);
                }
            }
        }

        private void PullDeletedEntities(Guid sessionId, SyncEventType eventType)
        {
            PullArgs args = new PullArgs(null);
            List<SyncMetadata> metadataList = new List<SyncMetadata>();
            ICollection<Guid> entityIdListToPull = new Collection<Guid>();

            List<SyncMetadata> deletedList = SyncDao.GetDeletedEntities(typeof(T).Name);

            if (deletedList.Count > 0)
            {
                foreach (SyncMetadata syncEntityData in deletedList)
                {
                    metadataList.Add(syncEntityData);
                    entityIdListToPull.Add(syncEntityData.Guid);
                }

                Pull(metadataList, eventType, sessionId, args, entityIdListToPull);
            }
        }

        private void Pull(ICollection<SyncMetadata> listOfSyncObjects, SyncEventType syncEventType, Guid syncSessionId, PullArgs pullArgs, ICollection<Guid> listOfEntityIds)
        {
            if (listOfSyncObjects.Count != 0)
            {
                SendPullRequest(listOfSyncObjects, syncEventType, syncSessionId, pullArgs);

                lock (SyncModule.GetLock<T>())
                {
                    SyncDao.UpdateSessionIds(listOfEntityIds, syncSessionId);
                }
            }
            else
            {
                SendNewPullRequest(string.Empty, syncSessionId, pullArgs, syncEventType);
            }
        }

        private void SendNewPullRequest(string objectsToBeSent, Guid sessionId, PullArgs pullArgs, SyncEventType syncEventType)
        {
            var pullRequest = new PullRequest
                {
                    Knowledge = objectsToBeSent,
                    ObjectType = typeof(T).Name,
                    SessionGuid = sessionId,
                    Filter = pullArgs.Filter,
                    SyncType = (int)syncEventType,
                    PlatformType = "WindowsMobileOrPc" // we need to get the platform type from the framework
                };

            Dispatcher.SendMessage(pullRequest, SyncModule);

            NotifyOnSyncEvent(typeof(T).Name, Guid.Empty, SyncEventReason.Started,
                              syncEventType, sessionId, string.Empty, 0, 0);
        }

        private void SendNewPullRequest(string objectsToBeSent, Guid sessionId, PullArgs pullArgs,
                                        int totalNumberOfPackets, int packetNumber, SyncEventType syncEventType)
        {
            var pullRequest = new PullRequest
                {
                    Knowledge = objectsToBeSent,
                    ObjectType = typeof(T).Name,
                    SessionGuid = sessionId,
                    SyncType = (int)syncEventType,
                    Filter = pullArgs.Filter,
                    TotalNumberOfPackets = totalNumberOfPackets,
                    PacketNumber = packetNumber,
                    IsSplit = true,
                    PlatformType = "WindowsMobileOrPc" // we need to get the platform type from the framework
                };

            Dispatcher.SendMessage(pullRequest, SyncModule);

            // since the pull request is split, for a given series of 
            // requests with the same session id, only one notify event is fired at the first packet dispatch
            if (packetNumber == 1)
            {
                NotifyOnSyncEvent(typeof(T).Name, Guid.Empty, SyncEventReason.Started,
                                  syncEventType, sessionId, string.Empty, 0, 0);
            }
        }

        private void SendPullRequest(ICollection<SyncMetadata> listOfSyncObjects, SyncEventType syncEventType, Guid syncSessionId, PullArgs pullArgs)
        {
            string serializedObjects = XSerializer.SerializeSyncEntity(listOfSyncObjects, new List<PropertyInfo>());

            if (serializedObjects.Length > MaxDataPacketSize)
            {
                var entitySplits = SplitSerializedEntity(serializedObjects, MaxDataPacketSize);

                for (int x = 0; x < entitySplits.Length; x++)
                {
                    SendNewPullRequest(entitySplits[x], syncSessionId, pullArgs, entitySplits.Length, x + 1, syncEventType);
                }
            }
            else
            {
                SendNewPullRequest(serializedObjects, syncSessionId, pullArgs, syncEventType);
            }
        }

        private void PullResponseHandler(object sender, PullResponseEventArgs pullResponseArg)
        {
            PullResponse pullResponse = pullResponseArg.PullResponse;

            if (pullResponse.ObjectType != typeof(T).Name)
            {
                return;
            }

            Guid sessionId = pullResponse.SessionGuid;

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_5, "Performance Logs", "Pull response received", pullResponse.GetDebugString());
            }

            lock (SyncModule.GetLock<T>())
            {
                try
                {
                    if (!_pullResponsesSessions.ContainsKey(sessionId))
                    {
                        _pullResponsesSessions.Add(sessionId, 0); // initial CurrentEntityCount for the session id is 0
                    }

                    if (!pullResponse.IsSplit)
                    {
                        List<SyncEntityData> objectsToCreate = new List<SyncEntityData>();
                        foreach (var syncObject in pullResponse.SynchronizedObjects)
                        {
                            Guid entityId = syncObject.Guid;
                            Guid entitySessionId = SyncDao.GetSessionId(entityId);

                            if (entitySessionId.Equals(sessionId) || entitySessionId.Equals(Guid.Empty))
                            {
                                if (SyncDao.IsExistInMetadata(entityId))
                                {
                                    try
                                    {
                                        List<ConflictedPropertyInfo> conflictedProperties =
                                            ResolveReceivedEntity(syncObject, true);
                                        NotifyConflict(entityId, conflictedProperties);
                                        var syncedEntityNumber = UpdateSessionDictionary(sessionId);
                                        NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Synchronized,
                                                          (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, pullResponse.TotalEntityCount, syncedEntityNumber);
                                    }
                                    catch (SynchronizedStorageException storageException)
                                    {
                                        NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Error,
                                                          (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, 0, 0);
                                        LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1,
                                                           "Storage exception in PullResponseHandler");
                                    }
                                }
                                else
                                {
                                    objectsToCreate.Add(syncObject);
                                }
                            }
                        }

                        if (objectsToCreate.Count > 0)
                        {
                            try
                            {
                                InsertReceivedEntitiesFromServer(objectsToCreate);

                                foreach (SyncEntityData syncEntityData in objectsToCreate)
                                {
                                    var syncedEntityNumber = UpdateSessionDictionary(sessionId);
                                    NotifyOnSyncEvent(typeof(T).Name, syncEntityData.Guid, SyncEventReason.Synchronized,
                                                      (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, pullResponse.TotalEntityCount, syncedEntityNumber);
                                }
                            }
                            catch (SynchronizedStorageException storageException)
                            {
                                NotifyOnSyncEvent(typeof(T).Name, Guid.Empty, SyncEventReason.Error,
                                                  (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, 0, 0);
                                LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1,
                                                   "Storage exception in PullResponseHandler");
                            }
                        }

                        foreach (var syncObject in pullResponse.DeletedObjects)
                        {
                            Guid entityId = syncObject.Guid;
                            Guid entitySessionId = SyncDao.GetSessionId(entityId);

                            // need to check if the server is sending the deleted entity id to the client
                            // the scenario is server is having new entity which matches the given filter and
                            // deleted at the server, seems to be server is sending that infor to client which is not needed.
                            if (entitySessionId.Equals(sessionId) || entitySessionId.Equals(Guid.Empty))
                            {
                                try
                                {
                                    // here we delete the properties without considering the enablepropertysynchronization property
                                    DeleteCustomEntity(syncObject.Guid);
                                    DeleteMetadata(syncObject.Guid);
                                    var syncedEntityNumber = UpdateSessionDictionary(sessionId);
                                    NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Synchronized,
                                                      (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, pullResponse.TotalEntityCount, syncedEntityNumber);
                                }
                                catch (SynchronizedStorageException storageException)
                                {
                                    NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Error,
                                                      (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, 0, 0);
                                    LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1, "Storage exception in PullResponseHandler");
                                }
                            }
                        }
                    }
                    else
                    {
                        int syncedEntityNumber = 0;

                        if (pullResponse.CurrentEntityCount == 1)
                        {
                            syncedEntityNumber = UpdateSessionDictionary(sessionId);
                        }

                        // need to checked null 
                        SyncEntityData syncObject = pullResponse.SynchronizedObjects[0];
                        Guid entityId = syncObject.Guid;
                        Guid entitySessionId = SyncDao.GetSessionId(entityId);

                        if (entitySessionId.Equals(sessionId) || entitySessionId.Equals(Guid.Empty))
                        {
                            SyncDao.InsertPartialEntity(entityId, sessionId, pullResponse.PacketNumber, syncObject);

                            int numberOfPackets = SyncDao.GetPartialEntityCount(entityId, sessionId);

                            if (numberOfPackets == pullResponse.TotalNumberOfPackets)
                            {
                                List<string> partialEntityList = SyncDao.GetPartialEntities(entityId, sessionId);
                                syncObject.Object = CombinePartialEntities(partialEntityList);

                                try
                                {
                                    if (SyncDao.IsExistInMetadata(entityId))
                                    {
                                        List<ConflictedPropertyInfo> conflictedProperties = ResolveReceivedEntity(syncObject, true);
                                        NotifyConflict(entityId, conflictedProperties);
                                    }
                                    else
                                    {
                                        InsertReceivedEntitiesFromServer(new List<SyncEntityData>() { syncObject });
                                    }

                                    NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Synchronized,
                                                      (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, pullResponse.TotalEntityCount, syncedEntityNumber);
                                }
                                catch (SynchronizedStorageException storageException)
                                {
                                    NotifyOnSyncEvent(typeof(T).Name, entityId, SyncEventReason.Error,
                                                      (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, 0, 0);
                                    LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1, "Storage exception in PullResponseHandler");
                                }
                                finally
                                {
                                    SyncDao.RemovePartialEntities(entityId, sessionId);
                                }
                            }
                        }
                    }

                    // Get the received entity count and check if all received, if so fires the event.
                    int receivedNoOfEntities;

                    _pullResponsesSessions.TryGetValue(sessionId, out receivedNoOfEntities);

                    if (receivedNoOfEntities == pullResponse.TotalEntityCount)
                    {
                        NotifyOnSyncEvent(typeof(T).Name, Guid.Empty, SyncEventReason.Completed,
                                                    (SyncEventType)pullResponse.SyncType, sessionId, string.Empty, receivedNoOfEntities, pullResponse.TotalEntityCount);

                        _pullResponsesSessions.Remove(pullResponse.SessionGuid);

                        if (SyncModule.EnablePerformanceLogs)
                        {
                            LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_6, "Performance Logs", "Pull response completed",
                                               "Object Type: " + typeof(T).Name + ", Sync Session Id: " + sessionId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(SyncModule, ex, LogId.SYNC_PULL_E_2, "Exception in PullResponseHandler");
                }
            }
        }

        private int UpdateSessionDictionary(Guid sessionId)
        {
            int receivedNoOfEntities;
            _pullResponsesSessions.TryGetValue(sessionId, out receivedNoOfEntities);
            receivedNoOfEntities = receivedNoOfEntities + 1;
            _pullResponsesSessions[sessionId] = receivedNoOfEntities;
            return receivedNoOfEntities;
        }

        private void InsertReceivedEntitiesFromServer(List<SyncEntityData> syncObjects)
        {
            lock (SyncModule.GetLock<T>())
            {
                // If the object for some reason already is in storage (metadata has been missing for the object)
                // then it is updated instead of inserted
                var syncObjectsNotInStorage = UpdateEntitiesAlreadyInStorage(syncObjects);

                InsertEntities(syncObjectsNotInStorage);

                foreach (SyncEntityData syncObject in syncObjects)
                {
                    InsertMetadata(typeof(T).Name, syncObject.Guid, syncObject.Version, SyncStatus.Synchronized);
                }
            }
        }

        private List<SyncEntityData> UpdateEntitiesAlreadyInStorage(IEnumerable<SyncEntityData> syncObjects)
        {
            lock (SyncModule.GetLock<T>())
            {
                var syncObjectsNotInStorage = new List<SyncEntityData>();
                var idsInStorage = Storage.ReadIds(new EntityFilter());

                foreach (var syncEntityData in syncObjects)
                {
                    if (idsInStorage.Contains(syncEntityData.Guid))
                    {
                        var conflictedProperties = ResolveReceivedEntity(syncEntityData, true);
                        NotifyConflict(syncEntityData.Guid, conflictedProperties);
                    }
                    else
                    {
                        syncObjectsNotInStorage.Add(syncEntityData);
                    }
                }

                return syncObjectsNotInStorage;
            }
        }

        private void PullInvokeMessageHandler(object sender, PullInvokeEventArgs pullInvokeArgs)
        {
            PullInvokeMessage pullInvokeMessage = pullInvokeArgs.PullInvokeMessage;

            if (pullInvokeMessage.ObjectType != typeof(T).Name)
            {
                return;
            }

            try
            {
                var receivedIdList = new List<Guid>();

                if (pullInvokeMessage.IsSplit)
                {
                    // In this section we don't lock the DB, for the moment there is no reason to lock the DB since this data is not shared.
                    // insert to DB 
                    if (SyncDao.InsertPartialInvokeMessage(pullInvokeMessage.SessionGuid,
                                                           pullInvokeMessage.PacketNumber,
                                                           pullInvokeMessage.Knowledge))
                    {
                        _partialMsgCount = SyncDao.GetPartialInvokeMessageCount(pullInvokeMessage.SessionGuid); 
                    }

                    if (_partialMsgCount == pullInvokeMessage.TotalNumberOfPackets)
                    {
                        var knowledge = new StringBuilder();

                        List<string> partialInvokeMessageList =
                            SyncDao.GetPartialInvokeMessages(pullInvokeMessage.SessionGuid);

                        foreach (var message in partialInvokeMessageList)
                        {
                            knowledge.Append(message);
                        }

                        receivedIdList =
                            (List<Guid>)XSerializer.Deserialize(knowledge.ToString(), typeof(List<Guid>));

                        // delete the partial PullInvokeMessages
                        SyncDao.DeletePartialInvokeMessages(pullInvokeMessage.SessionGuid);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(pullInvokeMessage.Knowledge))
                    {
                        if (pullInvokeMessage.InvokeReason == (int)PullInvokeReason.ClientLogin)
                        {
                            receivedIdList =
                                (List<Guid>)
                                XSerializer.Deserialize(pullInvokeMessage.Knowledge, typeof(List<Guid>));
                        }
                        else if (pullInvokeMessage.InvokeReason == (int)PullInvokeReason.AutomaticPush)
                        {
                            receivedIdList.Add(new Guid(pullInvokeMessage.Knowledge));
                        }
                    }
                }

                lock (SyncModule.GetLock<T>())
                {
                    var clientIdList = new List<Guid>();
                    clientIdList.AddRange(Storage.ReadIds(new EntityFilter()));

                    DeleteMetadataNotInList(clientIdList);

                    if (pullInvokeMessage.InvokeReason == (int)PullInvokeReason.ClientLogin)
                    {
                        var listOfSyncObjects = new List<SyncMetadata>();

                        if (clientIdList.Count == 0)
                        {
                            foreach (var receivedId in receivedIdList)
                            {
                                listOfSyncObjects.Add(new SyncMetadata { Guid = receivedId, Version = 0 });
                            }
                        }
                        else
                        {
                            if (receivedIdList.Count > 0)
                            {
                                foreach (var id in receivedIdList.Where(id => !clientIdList.Contains(id)))
                                {
                                    clientIdList.Add(id);
                                }
                            }

                            var chunkList = new List<Guid>();
                            const int ChunkSize = 500;

                            var noOfChunks = clientIdList.Count / ChunkSize;

                            // just for the moment we have used 100 as the limit
                            var y = 0;

                            for (var i = 0; i < noOfChunks; i++)
                            {
                                chunkList.AddRange(clientIdList.GetRange(y, ChunkSize));
                                y = y + ChunkSize;

                                var metadataCollection = GetEntityMetadataList(chunkList, false);

                                CreateSyncEntityCollection(
                                    chunkList, metadataCollection, receivedIdList, listOfSyncObjects);
                                chunkList.Clear();
                            }

                            if (clientIdList.Count % ChunkSize > 0)
                            {
                                chunkList.AddRange(clientIdList.GetRange(y, clientIdList.Count - y));

                                var metadataCollection = GetEntityMetadataList(chunkList, false);

                                CreateSyncEntityCollection(
                                    chunkList, metadataCollection, receivedIdList, listOfSyncObjects);
                            }
                        }

                        Guid newSession = Guid.NewGuid();
                        SendPullRequest(listOfSyncObjects, SyncEventType.ServerPush, newSession, new PullArgs(null));

                        var ids = listOfSyncObjects.Select(syncObject => syncObject.Guid).ToList();
                        SyncDao.UpdateSessionIds(ids, newSession);
                    }

                    if (pullInvokeMessage.InvokeReason == (int)PullInvokeReason.AutomaticPush)
                    {
                        // Even though there is a list of ids, actually it contains one id since this is an automatic push by the server
                        SyncMetadata metadata;

                        lock (SyncModule.GetLock<T>())
                        {
                            metadata = SyncDao.GetMetadata(receivedIdList[0]);
                        }

                        if (metadata.Version == -1)
                        {
                            metadata.Version = 0;
                        }

                        Guid newSession = Guid.NewGuid();
                        SendPullRequest(
                            new List<SyncMetadata>() { metadata },
                            SyncEventType.ServerPush,
                            newSession,
                            new PullArgs(null));
                        SyncDao.UpdateSessionIds(new List<Guid>() { metadata.Guid }, newSession);
                    }

                    if (pullInvokeMessage.InvokeReason == (int)PullInvokeReason.ManualPush)
                    {
                        PullImpl(new PullArgs(pullInvokeMessage.Filter), SyncEventType.ServerPush);
                    }
                }
            }
            catch (SynchronizedStorageException storageException)
            {
                LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PULL_E_1,
                                   "Storage exception in PullInvokeMessageHandler");
            }
            catch (Exception ex)
            {
                LogHelper.LogError(SyncModule, ex, LogId.SYNC_PULL_E_2, "Exception in PullInvokeMessageHandler");
            }
        }

        // This is to handle if for some reason there is metadata that has no data.
        private void DeleteMetadataNotInList(IEnumerable<Guid> keepIds)
        {
            var metaDataIds = SyncDao.GetMetadataIds(typeof(T).Name);

            foreach (var id in metaDataIds.Except(keepIds))
            {
                var body = string.Format(
                    "Entity with Id {0}, Type {1} is missing, but there is metadata available. Metadata will be deleted.",
                    id.ToString(), typeof(T).Name);
                LogHelper.LogWarning(SyncModule, LogId.SYNC_COM_D_1, "Entity missing", body, string.Empty);
                DeleteMetadata(id);
            }
        }

        #endregion  
    }
}
