﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class EntityUpdateManager<T> : SynchronizationManager<T>, IEntityUpdateManager where T : ISynchronizable
    {
        #region Properties

        internal Operation AutomaticPushOperations { get; set; }

        #endregion

        #region Constructor

        public EntityUpdateManager(ISyncDao syncDao, SynchronizedStorageBase<T> storage, SynchronizationModule syncModule, int maxDataPacketSize, ILog log)
            : base(syncDao, null, storage, syncModule, maxDataPacketSize, log)
        {
            storage.InternalEntityChanged += OnEntityChanged;
        }

        #endregion

        #region Methods

        private void OnEntityChanged(object sender, ChangeEventArgs eventArgs)
        {
            lock (SyncModule.GetLock<T>())
            {
                try
                {
                    if (eventArgs.OriginatorType != typeof(SynchronizationModule).Name)
                    {
                        string objectTypeName = typeof(T).Name;
                        UpdateSyncMetadata(eventArgs, objectTypeName);
                        InitiateAutomaticSync(eventArgs);
                    }
                }
                catch (SynchronizedStorageException storageException)
                {
                    LogHelper.LogError(SyncModule, storageException, LogId.SYNC_EU_E_1, "Storage exception in entity update manager");
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(SyncModule, ex, LogId.SYNC_EU_E_2, "Exception in entity update manager");
                }
            }
        }

        private void UpdateSyncMetadata(ChangeEventArgs eventArgs, string objectTypeName)
        {
            switch (eventArgs.Change)
            {
                case Operation.Created:

                    // In this scenario we are adding a new record at the client end. So the server version is
                    // set to 0.
                    // The server version will be updated for this entity once the new entity has been pushed to
                    // the server,.
                    InsertMetadata(objectTypeName, eventArgs.Id, 0, SyncStatus.Created);
                    break;

                case Operation.Updated:

                    List<string> changedProperties = FindChangedProperties(eventArgs.New, eventArgs.Old);
                    if (SyncModule.EnablePropertySynchronization)
                    {
                        if (changedProperties.Count > 0)
                        {
                            SyncDao.UpdateMetadata(objectTypeName, eventArgs.Id, SyncStatus.Updated, Guid.NewGuid());
                            SyncDao.MarkPropertiesAsChanged(eventArgs.Id, changedProperties);
                        }
                    }
                    else
                    {
                        if (changedProperties.Count > 0)
                        {
                            SyncDao.UpdateMetadata(objectTypeName, eventArgs.Id, SyncStatus.Updated, Guid.NewGuid());
                        }
                    }

                    break;

                case Operation.Deleted:
                    Guid objectId = eventArgs.Id;
                    if (SyncDao.GetMetadata(objectId).Version == 0
                        && SyncDao.GetSessionId(objectId) == Guid.Empty)
                    {
                        DeleteMetadata(objectId);
                    }
                    else
                    {
                        SyncDao.UpdateMetadata(objectTypeName, objectId, SyncStatus.Deleted, Guid.NewGuid());
                    }

                    break;

                default:
                    LogHelper.LogError(SyncModule, null, LogId.SYNC_EU_E_3,
                                       string.Format("Object {0} {1}: " + "entity change not specified", eventArgs.Id, objectTypeName));
                    break;
            }
        }

        private void InitiateAutomaticSync(ChangeEventArgs eventArgs)
        {
            if ((AutomaticPushOperations & eventArgs.Change) != Operation.None)
            {
                Collection<Guid> ids = new Collection<Guid> { eventArgs.Id };
                PushArgs pushArgs = new PushArgs(ids);
                SyncModule.Push<T>(pushArgs);
            }
        }

        internal void DeleteLocally(ICollection<Guid> entityIds)
        {
            lock (SyncModule.GetLock<T>())
            {
                foreach (Guid entityId in entityIds)
                {
                    try
                    {
                        Storage.Delete(entityId, new DeleteArgs(typeof(SynchronizationModule).Name));
                    }
                    catch (Exception e)
                    {
                        LogHelper.LogError(SyncModule, e, LogId.SYNC_EU_E_6, string.Format("Entity {0} delete failed", entityId));
                    }
                    try
                    {
                        DeleteMetadata(entityId);
                    }
                    catch (Exception e)
                    {
                        LogHelper.LogError(SyncModule, e, LogId.SYNC_EU_E_6, string.Format("Entity {0} metadata deletion failed", entityId));
                    }
                }
            }
        }

        private List<string> FindChangedProperties(object newEntity, object oldEntity)
        {
            List<string> changedProperties = new List<string>();

            Type entityType = typeof(T);

            foreach (PropertyInfo property in PropertyHelper.GetProperties(entityType))
            {
                try
                {
                    object newValue = property.GetValue(newEntity, null);
                    object oldValue = property.GetValue(oldEntity, null);
                    if (!Equals(newValue, oldValue))
                    {
                        changedProperties.Add(property.Name);
                    }
                }
                catch (ArgumentNullException e)
                {
                    LogHelper.LogError(SyncModule, e, LogId.SYNC_EU_E_2, "Null argument when searching for properties");
                }
                catch (ArgumentException e)
                {
                    LogHelper.LogError(SyncModule, e, LogId.SYNC_EU_E_2, "Illegal Argument when finding changed properties");
                }
            }

            return changedProperties;
        }

        #endregion
    }
}
