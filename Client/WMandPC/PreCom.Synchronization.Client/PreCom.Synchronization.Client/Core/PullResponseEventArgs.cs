﻿using System;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class PullResponseEventArgs : EventArgs
    {
        internal PullResponse PullResponse { get; set; }
    }
}
