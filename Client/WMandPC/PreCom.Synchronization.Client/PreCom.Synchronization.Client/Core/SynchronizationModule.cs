﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
#if LT
    /// <summary>
    /// Synchronization module for PreComLT. Use the constructor to create instance and then call Initialize method.
    /// </summary>
    public class SynchronizationModule : SynchronizationClientBase
#else
    internal class SynchronizationModule : SynchronizationClientBase
#endif
    {
        #region Fields

        private const int DefaultSyncPacketSizeInKb = 1024;
        private const int DefaultMaximumSyncPacketSizeInKb = 2048;
        private const int DefaultMinimumSyncPacketSizeInKb = 10;

        internal const int SerializedEmptyPushRequest = 375;
        internal const int SerializedEmptyPullRequest = 436;

        private readonly ApplicationBase _application;
        private readonly CommunicationBase _communication;
        private readonly ILog _log;
#if !LT
        private readonly SettingsBase _settings;
        private readonly StorageBase _storage;
#endif
        private readonly Action _messageSentCallback;
// ReSharper disable once NotAccessedField.Local - Used by PreComLT
        private readonly Action _messageReceivedCallback;

        /// <summary>
        /// Indicates if the module has been initialized or not.
        /// </summary>
        private bool _isInitialized;
        private int _definedSyncPacketSizeInBytes;
        private ISyncDao _syncDao;
        private LogHelper _logHelper;

        private DispatcherBase _dispatcher;
        private TypeManager _typeManager;

        internal bool EnablePerformanceLogs { get; set; }
        internal bool EnablePropertySynchronization { get; set; }

        internal TypeManager TypeManager
        {
            get { return _typeManager; }
        }

        #endregion

        #region Constructors

#if !LT
        [Inject]
        public SynchronizationModule(ApplicationBase application,
            CommunicationBase communication,
            SettingsBase settings,
            StorageBase storage,
            ILog log)
        {
            _application = application;
            _communication = communication;
            _settings = settings;
            _storage = storage;
            _log = log;
            _logHelper = new LogHelper(_log);
        }
#else

        // Used by LT client
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationModule"/> class.
        /// </summary>
        /// <param name="communication">CommunicationBase instance.</param>
        public SynchronizationModule(CommunicationBase communication)
            : this(communication, null, null, true)
        {
        }

        internal SynchronizationModule(CommunicationBase communication,
            Action messageSentCallback,
            Action messageReceivedCallback,
            bool deltaSync)
        {
            _communication = communication;
            _messageSentCallback = messageSentCallback;
            _messageReceivedCallback = messageReceivedCallback;

            EnablePropertySynchronization = deltaSync;
            _log = new LogFile();
            _logHelper = new LogHelper(_log);
        }
#endif

        #endregion

        #region ModuleBase members

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get { return "Synchronization"; }
        }

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">Cancel the dispose of the module</param>
        /// <returns>
        /// true if disposing was OK, false otherwise.
        /// </returns>
        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;
            return true;
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>
        /// true if initializing was OK, false otherwise.
        /// </returns>
        public override bool Initialize()
        {
            // We can assign dispatcher in the constructor when unittesting, but otherwise initialize it here.
            if (_dispatcher == null)
            {
                _dispatcher = new Dispatcher(_communication, _messageSentCallback, _log);
            }

            _typeManager = new TypeManager();

#if LT
            if (_messageReceivedCallback != null)
            {
                _dispatcher.ErrorMessageReceived += (s, e) => _messageReceivedCallback();
                _dispatcher.PullInvokeReceived += (s, e) => _messageReceivedCallback();
                _dispatcher.PullResponseReceived += (s, e) => _messageReceivedCallback();
                _dispatcher.PushResponseReceived += (s, e) => _messageReceivedCallback();
            }

            EnablePerformanceLogs = false;
            int definedSyncPacketSizeInKb = DefaultSyncPacketSizeInKb;
            _syncDao = new SyncDao();
#else
            EnablePerformanceLogs = _settings.Read(this, "EnablePerformanceLogs", false, false);
            int definedSyncPacketSizeInKb = _settings.Read(this, "MaximumSyncPacketSizeInKb", DefaultSyncPacketSizeInKb, true);

             // Removed the possibility to turn off the delta synchronization
            // EnablePropertySynchronization = Application.Instance.Settings.Read(this, "EnablePropertySynchronization", true, true);
            EnablePropertySynchronization = true;
            _syncDao = new SyncDao(_storage);
#endif

            _definedSyncPacketSizeInBytes = SetSyncPacketSize(definedSyncPacketSizeInKb) * 1024;

            _syncDao.CreateStorage();

            _isInitialized = true;
            return true;
        }

        /// <summary>
        /// Check if instance is initialized or not.
        /// </summary>
        /// <value>True if initialized, else false</value>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        #endregion

        #region Private methods

        private int SetSyncPacketSize(int definedSyncPacketSizeInKb)
        {
            if (definedSyncPacketSizeInKb > DefaultMaximumSyncPacketSizeInKb)
            {
                _logHelper.LogError(this, LogId.SYNC_E_1, "MaximumSyncPacketSizeInKb setting is out of range",
                        string.Format("The value of the setting \"MaximumSyncPacketSizeInKb\" is ({0}), Maximum supported value {1} will be used", definedSyncPacketSizeInKb, DefaultMaximumSyncPacketSizeInKb), string.Empty);
                return DefaultMaximumSyncPacketSizeInKb;
            }

            if (definedSyncPacketSizeInKb < DefaultMinimumSyncPacketSizeInKb)
            {
                _logHelper.LogError(this, LogId.SYNC_E_1, "MaximumSyncPacketSizeInKb setting is out of range",
                        string.Format("The value of the setting \"MaximumSyncPacketSizeInKb\" is ({0}), Minimum supported value {1} will be used", definedSyncPacketSizeInKb, DefaultMinimumSyncPacketSizeInKb), string.Empty);
                return DefaultMinimumSyncPacketSizeInKb;
            }

            return definedSyncPacketSizeInKb;
        }

        #endregion

        #region ISynchronization Members

        public override PullResult Pull<T>(PullArgs pullArgs)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            if (pullArgs == null)
            {
                throw new ArgumentNullException("pullArgs");
            }

            if (pullArgs.Filter == null)
            {
                throw new ArgumentException("pullArgs.Filter must be non null", "pullArgs");
            }

            return ((PullManager<T>)_typeManager.GetTypeInformation<T>().PullManager).Pull(pullArgs, SyncEventType.ClientPull);
        }

        public override PushResult Push<T>()
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            return ((PushManager<T>)_typeManager.GetTypeInformation<T>().PushManager).Push(new Collection<Guid>());
        }

        public override PushResult Push<T>(PushArgs pushArgs)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            if (pushArgs == null)
            {
                throw new ArgumentNullException("pushArgs");
            }

            if (pushArgs.EntityIds.Count < 1)
            {
                throw new ArgumentException("EntityIds cannot be empty.");
            }

            return ((PushManager<T>)_typeManager.GetTypeInformation<T>().PushManager).Push(pushArgs.EntityIds);
        }

        public override void DeleteLocally<T>(ICollection<Guid> entityIds)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            if (entityIds == null)
            {
                throw new ArgumentNullException("entityIds");
            }

            TypeInformation information = _typeManager.GetTypeInformation<T>();
            if (information != null)
            {
                ((EntityUpdateManager<T>)information.EntityUpdateManager).DeleteLocally(entityIds);
            }
        }

        public override void RegisterStorage<T>(SynchronizedStorageBase<T> storage)
        {
            if (storage == null)
            {
                throw new ArgumentNullException("storage");
            }

            _typeManager.AddType<T>(new TypeInformation
            {
                PullManager = new PullManager<T>(_syncDao, storage, this, _dispatcher, _definedSyncPacketSizeInBytes - SerializedEmptyPullRequest, _log),
                EntityUpdateManager = new EntityUpdateManager<T>(_syncDao, storage, this, _definedSyncPacketSizeInBytes, _log),
                PushManager = new PushManager<T>(_syncDao, storage, this, _dispatcher, _definedSyncPacketSizeInBytes - SerializedEmptyPushRequest, _log),
                ServerErrorManager = new ServerErrorManager<T>(_syncDao, storage, this, _dispatcher, _definedSyncPacketSizeInBytes, _log),
                Lock = new object()
            });

            storage.ThisLock = GetLock<T>();
        }

        public override void UnregisterStorage<T>(SynchronizedStorageBase<T> storage)
        {
            if (storage == null)
            {
                throw new ArgumentNullException("storage");
            }

            _typeManager.RemoveType<T>();
        }

        public override void RegisterAutomaticPush<T>(Operation changeOperation)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            _typeManager.AddAutomaticPushOperation<T>(changeOperation);
        }

        public override void UnregisterAutomaticPush<T>()
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            _typeManager.RemoveAutomaticPushOperation<T>();
        }

        public override void RegisterConflictNotifier<T>(ConflictNotifyDelegate conflictNotifier)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            _typeManager.AddConflictNotifier<T>(conflictNotifier);
        }

        public override void UnregisterConflictNotifier<T>()
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            _typeManager.RemoveConflictNotifier<T>();
        }

        public override void RegisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            if (syncEventNotifier == null)
            {
                throw new ArgumentNullException("syncEventNotifier");
            }

            _typeManager.RegisterSyncEventHandler(typeof(T).Name, syncEventNotifier);
        }

        public override void UnregisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier)
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new ArgumentException(string.Format("The type {0} is not registered", typeof(T).Name));
            }

            if (syncEventNotifier == null)
            {
                throw new ArgumentNullException("syncEventNotifier");
            }

            _typeManager.UnregisterSyncEventHandler(typeof(T).Name, syncEventNotifier);
        }

        internal object GetLock<T>() where T : ISynchronizable
        {
            if (!_typeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            return _typeManager.GetLock<T>();
        }

        #endregion
    }
}
