﻿namespace PreCom.Synchronization.Core
{
    internal class TypeInformation
    {
        internal IClientPullManager PullManager { get; set; }
        internal IEntityUpdateManager EntityUpdateManager { get; set; }
        internal IClientPushManager PushManager { get; set; }
        internal SyncEventDelegate SyncEventHandlers { get; set; }
        internal ConflictNotifyDelegate ConflictNotifier { get; set; }
        internal IServerErrorManager ServerErrorManager { get; set; }  
        internal object Lock { get; set; }
    }
}
