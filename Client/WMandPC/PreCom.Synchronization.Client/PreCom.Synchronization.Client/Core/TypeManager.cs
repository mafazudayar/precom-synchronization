using System.Collections.Generic;

namespace PreCom.Synchronization.Core
{
    internal class TypeManager
    {
        #region Fields

        private readonly Dictionary<string, TypeInformation> _typeDictionary;

        #endregion

        #region Constructor

        public TypeManager()
        {
            _typeDictionary = new Dictionary<string, TypeInformation>();
        }

        #endregion

        #region Internal Methods

        internal void AddType<T>(TypeInformation typeInformation) where T : ISynchronizable
        {
            _typeDictionary.Add(typeof(T).Name, typeInformation);
        }

        internal void RemoveType<T>() where T : ISynchronizable
        {
            _typeDictionary.Remove(typeof(T).Name);
        }

        internal bool TypeExists<T>() where T : ISynchronizable
        {
            return _typeDictionary.ContainsKey(typeof(T).Name);
        }

        internal TypeInformation GetTypeInformation<T>()
        {
            TypeInformation typeInfo;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInfo);

            return typeInfo;
        }

        internal void RegisterSyncEventHandler(string type, SyncEventDelegate syncEventHandler)
        {
            TypeInformation typeInfo;
            _typeDictionary.TryGetValue(type, out typeInfo);

            if (typeInfo != null)
            {
                typeInfo.SyncEventHandlers += syncEventHandler;
            }
        }

        internal void UnregisterSyncEventHandler(string type, SyncEventDelegate syncEventHandler)
        {
            TypeInformation typeInfo;
            _typeDictionary.TryGetValue(type, out typeInfo);

            if (typeInfo != null)
            {
// ReSharper disable once DelegateSubtraction
                typeInfo.SyncEventHandlers -= syncEventHandler;
            }
        }

        internal SyncEventDelegate GetSyncEventHandler(string type)
        {
            TypeInformation typeInfo;
            SyncEventDelegate syncEventDelegate = null;
            _typeDictionary.TryGetValue(type, out typeInfo);

            if (typeInfo != null && typeInfo.SyncEventHandlers != null)
            {
                syncEventDelegate = typeInfo.SyncEventHandlers;
            }

            return syncEventDelegate;
        }

        internal void AddAutomaticPushOperation<T>(Operation change) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((EntityUpdateManager<T>)typeInformation.EntityUpdateManager).AutomaticPushOperations = change;
            }
        }

        internal void RemoveAutomaticPushOperation<T>() where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((EntityUpdateManager<T>)typeInformation.EntityUpdateManager).AutomaticPushOperations = Operation.None;
            }
        }

        internal void AddConflictNotifier<T>(ConflictNotifyDelegate notifier) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                typeInformation.ConflictNotifier = notifier;
            }
        }

        internal void RemoveConflictNotifier<T>() where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);
            typeInformation.ConflictNotifier = null;
        }

        internal object GetLock<T>() where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                return typeInformation.Lock;
            }

            return null;
        }

        #endregion
    }
}
