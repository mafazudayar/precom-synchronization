﻿using System;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class PushResponseEventArgs : EventArgs
    {
        internal PushResponse PushResponse { get; set; }
    }
}
