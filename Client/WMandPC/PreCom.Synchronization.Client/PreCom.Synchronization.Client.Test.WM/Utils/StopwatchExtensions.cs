﻿using System.Diagnostics;

namespace PreCom.Synchronization.Test.Utils
{
    public static class StopwatchExtensions
    {
        public static void Restart(this Stopwatch watch)
        {
            watch.Reset();
            watch.Start();
        }
    }
}
