﻿using System.Collections;
using NUnit.Framework;

namespace PreCom.Synchronization.Test.Utils
{
    static class CollectionAssert
    {
        public static void AreEqual(IEnumerable expected, IEnumerable actual)
        {
            if (expected == null)
            {
                Assert.IsNull(actual);
            }

            Assert.IsNotNull(actual);

// ReSharper disable once PossibleNullReferenceException
            var expectedEnumerator = expected.GetEnumerator();
            var actualEnumerator = actual.GetEnumerator();

            while (expectedEnumerator.MoveNext())
            {
                if (actualEnumerator.MoveNext())
                {
                    Assert.AreEqual(expectedEnumerator.Current, actualEnumerator.Current);
                }
                else
                {
                    Assert.Fail("Expected contains more elements than actual");
                }
            }

            if (actualEnumerator.MoveNext())
            {
                Assert.Fail("Actual contains more elements than expected");
            }
        }
    }
}
