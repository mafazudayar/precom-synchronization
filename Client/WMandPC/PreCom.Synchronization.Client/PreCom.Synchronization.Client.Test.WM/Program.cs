﻿using NUnitLite.Runner;

namespace PreCom.Synchronization.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            new TextUI(new DebugAndConsoleWriter()).Execute(args);
        }
    }
}
