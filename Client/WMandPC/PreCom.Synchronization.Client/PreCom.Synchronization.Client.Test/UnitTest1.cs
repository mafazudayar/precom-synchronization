﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PreCom.Synchronization;

namespace ClientUnitTestProject1
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            try
            {
                //MarketOrder m = new MarketOrder { Creator = "test", Id = new Guid(), Date = DateTime.Now, Description = "testting", IsAssigned = false };


                XmlSerializer seria = new XmlSerializer(typeof(SyncEntityData));
                StringWriter sw = new StringWriter();

                seria.Serialize(sw, new SyncEntityData());

                string s = sw.ToString();


                StringReader rdr = new StringReader(s);

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(MarketOrder));


                MarketOrder deserializedObject = (MarketOrder)xmlSerializer.Deserialize(rdr);

                //MarketOrder deserializedObject = DeSerialize<MarketOrder>(s);
                //Encoding.UTF8.GetString(ms.ToArray());
                //ms.Dispose();
                //List<PropertyInfo> props = m.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();

                //foreach (var propertyInfo in props)
                //{
                //    //propertyInfo.SetValue(m, null, null);
                //    //m.GetType().GetProperty("").se
                //}

                Console.WriteLine(deserializedObject.Creator);

                //Type entityType = typeof(MarketOrder);
                //Type[] knownTypes = null;



                //XmlSerializer serializer = new XmlSerializer(entityType);
                //StringReader sr = new StringReader(s);
                //{
                //    deserializedObject = (MarketOrder)serializer.Deserialize(sr);

                //    Console.WriteLine(s);
                //}


            }
            catch (Exception e)
            {
                Console.WriteLine("\nAn exception occurred: {0}", e.Message);
            }
        }

        public static string Serialize<T>(T data)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(data.GetType());
            //Overridden to use UTF8 for compatability with Perl XML::DOM
            StringWriter sw = new StringWriter();
            xmlSerializer.Serialize(sw, data);
            return sw.ToString();
        }

        public static object DeSerialize<T>(string data)
        {

            StringReader rdr = new StringReader(data);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(MarketOrder));


            var result = (T)xmlSerializer.Deserialize(rdr);


            return result;



        }
    }

    public class MarketOrder : ISynchronizable
    {
        public Guid Id { get; set; }

        public string Creator { get; set; }

        public string Description { get; set; }

        public bool IsAssigned { get; set; }

        public DateTime Date { get; set; }

        public Guid Guid
        {
            get { return Id; }
            set { Id = value; }
        }
    }

    //public class SeriHelper : IXmlSerializable
    //{
    //    private object _instance;

    //    public SeriHelper()
    //    {

    //    }

    //    public SeriHelper(object instance)
    //    {
    //        _instance = instance;
    //    }

    //    #region IXmlSerializable Members

    //    public System.Xml.Schema.XmlSchema GetSchema()
    //    {
    //        return null;
    //    }

    //    public void ReadXml(System.Xml.XmlReader reader)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void WriteXml(System.Xml.XmlWriter writer)
    //    {
    //        //writer.WriteString(

    //        List<PropertyInfo> props = _instance.GetType().GetProperties().
    //            Where(prop => Attribute.IsDefined(prop, typeof(SynchronizableAttribute))).ToList();

    //        foreach (var propertyInfo in props)
    //        {
    //            writer.WriteString(propertyInfo.GetValue(_instance, null).ToString());
    //        }
    //    }

    //    #endregion
    //}

    public class SyncMetadata
    {
        private int _status;

        /// <summary>
        /// Gets or sets the unique id of the entity.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the version of the entity.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets the status of the entity.
        /// </summary>
        public int Status { get { return _status; } set { _status = value; } }

        internal Operation GetStatus { get { return (Operation)_status; } }
    }

    public class SyncEntityData : PreCom.Synchronization.Entity.SyncMetadata
    {
        private Collection<string> _changedPropertyNames;

        /// <summary>
        /// Gets or sets the serialized object.
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Gets or sets the changed property names.
        /// </summary>
        /// <value>
        /// The changed property names.
        /// </value>
        public Collection<string> ChangedPropertyNames
        {
            get
            {
                if (_changedPropertyNames == null)
                {
                    _changedPropertyNames = new Collection<string>();
                }

                return _changedPropertyNames;
            }
        }
    }
}
