param(
    [Parameter(Position=0,Mandatory=1)]
    [string]$Version,

    [switch]$AutoPush,

    [Parameter(Mandatory=0)]
    [string]$File = "VERSION.txt"
)

function Invoke-Git
{
param(
    [string[]]$cmdArgs
)

    $gitExe = "git"
    $test = Get-IsGitPath($gitExe)
    if ($test -eq $false) {
        $gitExe = "C:\Program Files (x86)\git\bin\git"
        $test = Get-IsGitPath($gitExe)
        if ($test -eq $false) {
            throw "Can not find git.exe, please install git and make sure it is in the PATH"
        }
    }

    & $gitExe $cmdArgs
}

function Get-IsGitPath
{
param(
    [string]$path
)

    try {
        & $path --version > $null
    } catch {
        return $false
    }
    return $true
}

$isDirty = if ((Invoke-Git @("status", "--porcelain"))) { $true } else { $false }
if ($isDirty) {
    Write-Host "Current working directory is dirty (uncomitted changes)" -ForegroundColor red -BackgroundColor black
    Write-Host "Commit changes or otherwise clean your working copy before you attempt this command again." -ForegroundColor red -BackgroundColor black
    throw
}

if (!($Version -match "^v([0-9]+\.[0-9]+(\.[0-9]+(\.[0-9]+)?)?)(-[\w\-]+)?$")) {
    Write-Host "Version must conform to pattern 'vA.B[.C[.D]][-NAME]" -ForegroundColor red -BackgroundColor black
    Write-Host "Example of valid strings are" -ForegroundColor red -BackgroundColor black
    Write-Host "    v3.8" -ForegroundColor red -BackgroundColor black
    Write-Host "    v3.8-RC-2" -ForegroundColor red -BackgroundColor black
    Write-Host "    v3.8.2.0" -ForegroundColor red -BackgroundColor black
    Write-Host "    v3.8.0.1-RC-2" -ForegroundColor red -BackgroundColor black
    throw
}

$parsedVersion = $matches[1]

Write-Host "Writing $Version to $File" -ForegroundColor yellow
echo $Version >$File

Write-Host "Update AssemblyInfo files..." -ForegroundColor yellow
$properties = '@{""""Version""""=""""'+$parsedVersion+'"""";""""InformationalVersion""""=""""'+$Version+' (Local custom build)""""}'
.\build.cmd -taskList Generate-AssemblyInfo -properties $properties
if ($LastExitCode -ne 0) {
    throw
}

if ($AutoPush) {
    Write-Host "Comitting changes..." -ForegroundColor yellow
    Invoke-Git @("commit", "-a", "-m", "Bump to version $Version")

    Write-Host "Creating annotaded git tag..." -ForegroundColor yellow
    Invoke-Git @("tag", "-a", "$Version", "-m", "Bump to version $Version")
    if ($LastExitCode -ne 0) {
        throw
    }

    Write-Host "Pushing local branch..." -ForegroundColor yellow
    Invoke-Git "push"
    if ($LastExitCode -ne 0) {
        throw
    }

    Write-Host "Pushing git tag..." -ForegroundColor yellow
    Invoke-Git @("push", "origin", "$Version")
    if ($LastExitCode -ne 0) {
        throw
    }
} else {
    Write-Host
    Write-Host "-AutoPush was not specified, leaving changes uncomitted." -ForegroundColor yellow
    Write-Host
}

Write-Host "All done!" -ForegroundColor green
