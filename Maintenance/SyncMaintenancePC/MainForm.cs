﻿using System;
using System.Collections.Generic;

using PreCom.Utils;
using System.Linq;

namespace SyncMaintenance.PC
{
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    using PreCom.Core.Modules;

    using Application = PreCom.Application;

    public partial class MainForm : PreCom.Controls.PreComForm
    {
        #region Fields

        /// <summary>
        /// Instance of the module.
        /// </summary>
        private SyncMaintenance _module;
        private readonly ILog _log;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        /// <param name="module">The module.</param>
        public MainForm(SyncMaintenance module)
        {
            _module = module;
            _log = Application.Instance.CoreComponents.Get<ILog>();
            InitializeComponent();
            label1.Text = "";
            button1.Text = Language.Translate("Delete orders");
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(state => { DeleteOrders(); });
        }

        private void DeleteOrders()
        {
            _module.DeleteOrders();

            this.Invoke(
                (MethodInvoker)delegate
                    { label1.Text = Language.Translate("Orders deleted"); });

        }

        private void btnEntityTypes_Click(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    Analize();
                });
        }

        private void Analize()
        {
            List<string> entities = _module.GetEntityTypes();

            this.Invoke(
                (MethodInvoker)delegate
                    {
                        lblMessage.Text = "Analizing.....";
                    });

            foreach (string entityType in entities)
            {
                if (entityType == "EltelOrderEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("EltelOrderEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("EltelOrderEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("EltelOrderEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "EltelResourceStateEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("EltelResourceStateEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("EltelResourceStateEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("EltelResourceStateEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "EltelTimeMaterialEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("EltelTimeMaterialEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("EltelTimeMaterialEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("EltelTimeMaterialEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "FieldValueEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("FieldValueEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("FieldValueEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("FieldValueEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "MwlPxoAnswerEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("MwlPxoAnswerEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("MwlPxoAnswerEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("MwlPxoAnswerEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "MwlPxoEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("MwlPxoEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("MwlPxoEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("MwlPxoEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }

                if (entityType == "OrderBroadcastEntity")
                {
                    List<Guid> customObjects = _module.GetEntityCount("OrderBroadcastEntity");
                    List<Guid> metaObjects = _module.GetMetadataCount("OrderBroadcastEntity");
                    List<Guid> matchingObjects = _module.GetMatchingCount("OrderBroadcastEntity");

                    MatchCount(entityType, customObjects.Count, metaObjects.Count, matchingObjects.Count);
                    LogMissingData(entityType, customObjects, metaObjects, matchingObjects);
                }
            }

            this.Invoke(
                (MethodInvoker)delegate
                    {
                        lblMessage.Text = "Analize Completed";
                    });
        }

        private void MatchCount(string entityType, int customObjects, int metaObjects, int matchingObjects)
        {
            this.Invoke((MethodInvoker)delegate
            {
                dataGridView1.Rows.Add(entityType, customObjects, metaObjects, matchingObjects);

                int index = dataGridView1.Rows.Count;
                if (matchingObjects == customObjects && matchingObjects == metaObjects)
                {
                    dataGridView1.Rows[index - 1].Cells[3].Style.BackColor = Color.Green;
                }
                else
                {
                    dataGridView1.Rows[index - 1].Cells[3].Style.BackColor = Color.Red;
                }
            });
        }

        private void LogMissingData(string entityType, List<Guid> customObjects, List<Guid> metaObjects, List<Guid> matchingObjects)
        {
            // Objects that does not have meta data
            List<Guid> onlyCustomObjects = customObjects.Except(matchingObjects).ToList();

            foreach (Guid onlyCustomObject in onlyCustomObjects)
            {
                var logItem = new PreCom.Core.Log.LogItem(_module, LogLevel.Information, string.Empty, string.Empty, "Entity Id: " + onlyCustomObject + ", EntityType: " + entityType + " does not have meta data");
                _log.Write(logItem);
            }

            // Metadata that does not have Objects.
            List<Guid> onlyMetaObjects = metaObjects.Except(matchingObjects).ToList();

            foreach (Guid onlyMetaObject in onlyMetaObjects)
            {
                var logItem = new PreCom.Core.Log.LogItem(_module, LogLevel.Information, string.Empty, string.Empty, "Entity Id: " + onlyMetaObject + ", EntityType: " + entityType + " does not have the object, but metadata exist");
                _log.Write(logItem);
            }
        }
    }
}
