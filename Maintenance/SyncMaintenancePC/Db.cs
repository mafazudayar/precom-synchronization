﻿using System;
using System.Collections.Generic;
using System.Data;

using PreCom.Core;
using PreCom.Core.Modules;

namespace SyncMaintenance.PC
{
    class Db
    {
        private readonly StorageBase _storage;
        private readonly ILog _log;
        private readonly SyncMaintenance _syncReset;

        //private readonly Type _eltelOrderEntityType;
        //private readonly Type _eltelResourceStateEntityType;
        //private readonly Type _eltelTimeMaterialEntityType;
        //private readonly Type _fieldValueEntityType;
        //private readonly Type _fileEntityType;
        //private readonly Type _mwlPxoAnswerEntityType;
        //private readonly Type _mwlPxoEntityType;
        //private readonly Type _orderBroadcastEntityType;
        //private readonly Type _orderSearchEntityType;
        //private readonly Type _orderSearchQueryEntityType;
        //private readonly Type _resourceStateEntityType;
        //private readonly Type _statusEntityType;

        public Db(StorageBase storage, ILog log, SyncMaintenance syncReset)
        {
            _storage = storage;
            _log = log;
            _syncReset = syncReset;

            //_eltelOrderEntityType = typeof(EltelOrderEntity);
            //_eltelResourceStateEntityType = typeof(EltelResourceStateEntity);
            //_eltelTimeMaterialEntityType = typeof(EltelTimeMaterialEntity);
            //_fieldValueEntityType = typeof(FieldValueEntity);
            //_mwlPxoAnswerEntityType = typeof(MwlPxoAnswerEntity);
            //_mwlPxoEntityType = typeof(MwlPxoEntity);
            //_orderBroadcastEntityType = typeof(OrderBroadcastEntity);

            
            //_fileEntityType = typeof(FileEntity);
            //_orderSearchEntityType = typeof(OrderSearchEntity);
            //_orderSearchQueryEntityType = typeof(OrderSearchQueryEntity);
            //_resourceStateEntityType = typeof(ResourceStateEntity);
            //_statusEntityType = typeof(StatusEntity);
        }

        private string GetDBName(string entityType)
        {
            if (entityType == "EltelOrderEntity")
            {
                return "MWL_Order";
            }

            if (entityType == "EltelResourceStateEntity")
            {
                return "MWL_Resource_State";
            }

            if (entityType == "EltelTimeMaterialEntity")
            {
                return "MWL_TimeMaterial";
            }

            if (entityType == "FieldValueEntity")
            {
                return "ELT_FieldValue";
            }

            if (entityType == "MwlPxoAnswerEntity")
            {
                return "MWL_PxoAnswer";
            }

            if (entityType == "MwlPxoEntity")
            {
                return "MWL_Pxo";
            }

            if (entityType == "OrderBroadcastEntity")
            {
                return "ELT_OrderBroadcast";
            }

            return string.Empty;
        }

        internal List<string> GetEntityTypes()
        {
            var entityTypes = new List<string>();
            const string input = @"select EntityType 
                                   from PMC_Synchronization_Metadata 
                                   group by EntityType";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        string entityType = reader.GetString(0);
                        var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, string.Empty, string.Empty, "SyncReset.Db.GetEntityTypes: " + entityType);
                        _log.Write(logItem);
                        entityTypes.Add(entityType);
                    }
                }
            }

            return entityTypes;
        }

        internal List<Guid> GetEntityCount(string entityType)
        {
            var entityIds = new List<Guid>();
            string input = string.Empty;

            string dbName = this.GetDBName(entityType);

            input = "select Id from " + dbName;


            if (input == string.Empty)
            {
                return entityIds;
            }

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        Guid entityId = reader.GetGuid(0);
                        entityIds.Add(entityId);
                    }
                }
            }

            return entityIds;
        }

        internal List<Guid> GetMetadataCount(string entityType)
        {
            var entityIds = new List<Guid>();

            string input = "select EntityId from PMC_Synchronization_Metadata where EntityType = @entityType";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                var param = _storage.CreateParameter("@entityType", entityType);
                command.Parameters.Add(param);
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        Guid entityId = reader.GetGuid(0);
                        entityIds.Add(entityId);
                    }
                }
            }

            return entityIds;
        }

        internal List<Guid> GetMatchingCount(string entityType)
        {
            var entityIds = new List<Guid>();

            string input = string.Empty;

            if (entityType == "EltelOrderEntity")
            {
                input =
                    @"select PMC_Synchronization_Metadata.EntityId from MWL_Order inner join PMC_Synchronization_Metadata on MWL_Order.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='EltelOrderEntity'";
            }

            if (entityType == "EltelResourceStateEntity")
            {
                input =
                    @"select PMC_Synchronization_Metadata.EntityId from MWL_Resource_State inner join PMC_Synchronization_Metadata on MWL_Resource_State.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='EltelResourceStateEntity'";
            }

            if (entityType == "EltelTimeMaterialEntity")
            {
                input =
                    @"select PMC_Synchronization_Metadata.EntityId from MWL_TimeMaterial inner join PMC_Synchronization_Metadata on MWL_TimeMaterial.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='EltelTimeMaterialEntity'";
            }

            if (entityType == "FieldValueEntity")
            {
                input =
                    @"select PMC_Synchronization_Metadata.EntityId from ELT_FieldValue inner join PMC_Synchronization_Metadata on ELT_FieldValue.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='FieldValueEntity'";
            }

            if (entityType == "MwlPxoAnswerEntity")
            {
                input =
                    @"select PMC_Synchronization_Metadata.EntityId from MWL_PxoAnswer inner join PMC_Synchronization_Metadata on MWL_PxoAnswer.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='MwlPxoAnswerEntity'";
            }

            if (entityType == "MwlPxoEntity")
            {
                input =
                @"select PMC_Synchronization_Metadata.EntityId from MWL_Pxo inner join PMC_Synchronization_Metadata on MWL_Pxo.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='MwlPxoEntity'";
            }

            if (entityType == "OrderBroadcastEntity")
            {
                input =
                @"select PMC_Synchronization_Metadata.EntityId from ELT_OrderBroadcast inner join PMC_Synchronization_Metadata on ELT_OrderBroadcast.Id = PMC_Synchronization_Metadata.EntityId 
                          where PMC_Synchronization_Metadata.EntityType='OrderBroadcastEntity'";
            }

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        Guid entityId = reader.GetGuid(0);
                        entityIds.Add(entityId);
                    }
                }
            }

            return entityIds;
        }

        internal List<Guid> GetUnmodifiedEntities(string entityTypeName)
        {
            var entityIds = new List<Guid>();
            const string input = @"SELECT EntityId FROM PMC_Synchronization_Metadata 
                                   WHERE EntityType = @entityType 
                                   AND EntityStatus = 0;";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                var param = _storage.CreateParameter("@entityType", entityTypeName);
                command.Parameters.Add(param);
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        Guid entityId = reader.GetGuid(0);
                        entityIds.Add(entityId);
                    }
                    var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, string.Empty, string.Empty, "SyncReset.Db.GetUnmodifiedEntities: " + entityTypeName + ": " + entityIds.Count);
                    _log.Write(logItem);

                    //try
                    //{
                    //    Type t = Type.GetType(entityTypeName);
                    //    if (t == null)
                    //    {
                    //        logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, "", "", "GetType returned null for type: " + entityTypeName);
                    //    }
                    //    else
                    //    {
                    //        logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, "", "", "Found type: " + t.FullName);
                    //    }
                    //    _log.Write(logItem);
                    //}
                    //catch (Exception ex)
                    //{
                    //    logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", "Could not get type: " + entityTypeName, ex.ToString());
                    //    _log.Write(logItem);
                    //}
                }
            }

            return entityIds;
        }

        private void TruncateSyncCommunicationTables()
        {
            // TODO: test that this works

            string input = "Truncate table PMC_Synchronization_Partial_Entity";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                _storage.ExecuteNonCommand(command);

                //var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, "", "",
                //    "SyncReset.Db.GetUnmodifiedEntities: " + entityTypeName + ": " + entityIds.Count);
                //_log.Write(logItem);
            }

            input = "Truncate table PMC_Synchronization_Partial_PullInvokeMessage";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                _storage.ExecuteNonCommand(command);
            }
        }

        /// <summary>
        /// Returns any rows in the sync table that don't have a matching row in the order table
        /// </summary>
        /// <returns></returns>
        internal List<Guid> GetOrderIdsToRemove()
        {
            string entityTypeName = "EltelOrderEntity";

            var entityIds = new List<Guid>();
            const string input = @"SELECT EntityId
                                 FROM    PMC_Synchronization_Metadata l
                                 WHERE   l.EntityType = @entityType 
                                 AND    l.EntityId 
                                 NOT IN
                                 (
                                 SELECT  Id
                                 FROM    MWL_Order r
                                 );";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                var param = _storage.CreateParameter("@entityType", entityTypeName);
                command.Parameters.Add(param);
                using (IDataReader reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        Guid entityId = reader.GetGuid(0);
                        entityIds.Add(entityId);
                    }
                    var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Information, string.Empty, string.Empty, "SyncReset.Db.GetOrderIdsToRemove: " + entityTypeName + ": " + entityIds.Count);
                    _log.Write(logItem);
                }
            }

            return entityIds;
        }

        private void SelectAndDelete(Guid orderId, string selectInput, string deleteInput)
        {
            object result;
            using (IDbCommand command = _storage.CreateCommand(selectInput))
            {
                var param = _storage.CreateParameter("@orderId", orderId);
                command.Parameters.Add(param);
                result = _storage.ExecuteScalarCommand(command);
            }

            if (result != null)
            {
                var innerId = (Guid)result;
                DeleteFromSyncTables(innerId);

                using (IDbCommand command = _storage.CreateCommand(deleteInput))
                {
                    var param = _storage.CreateParameter("@orderId", orderId);
                    command.Parameters.Add(param);
                    _storage.ExecuteNonCommand(command);
                }
            }
        }

        internal void DeleteFromTimeMaterial(Guid orderId)
        {
            const string selectInput = "SELECT Id FROM MWL_TimeMaterial WHERE OrderId = @orderId";
            const string deleteInput = "DELETE FROM MWL_TimeMaterial WHERE OrderId = @orderId";

            SelectAndDelete(orderId, selectInput, deleteInput);
        }

        internal void DeleteFromOrderBroadcast(Guid orderId)
        {
            const string selectInput = "SELECT Id FROM ELT_OrderBroadcast WHERE OrderId = @orderId";
            const string deleteInput = "DELETE FROM ELT_OrderBroadcast WHERE OrderId = @orderId";

            SelectAndDelete(orderId, selectInput, deleteInput);
        }

        internal void DeleteFromMwlPxo(Guid parentId)
        {
            const string selectInput = "SELECT Id FROM MWL_Pxo WHERE ParentId = @orderId";
            const string deleteInput = "DELETE FROM MWL_Pxo WHERE ParentId = @orderId";

            SelectAndDelete(parentId, selectInput, deleteInput);
        }

        internal void DeleteFromMwlPxoAnswerAndProperty(Guid parentId)
        {
            const string selectInput = "SELECT Id FROM MWL_PxoAnswer WHERE ParentId = @orderId";
            const string deleteInput = "DELETE FROM MWL_PxoAnswer WHERE ParentId = @orderId";

            object result;
            using (IDbCommand command = _storage.CreateCommand(selectInput))
            {
                var param = _storage.CreateParameter("@orderId", parentId);
                command.Parameters.Add(param);
                result = _storage.ExecuteScalarCommand(command);
            }

            if (result != null)
            {
                var innerId = (Guid)result;

                string input = "DELETE FROM MWL_PxoAnswerProperty WHERE PxoAnswerId = @pxoAnswerId";
                using (IDbCommand command = _storage.CreateCommand(input))
                {
                    var param = _storage.CreateParameter("@pxoAnswerId", innerId);
                    command.Parameters.Add(param);
                    _storage.ExecuteNonCommand(command);
                }

                DeleteFromSyncTables(innerId);

                using (IDbCommand command = _storage.CreateCommand(deleteInput))
                {
                    var param = _storage.CreateParameter("@orderId", parentId);
                    command.Parameters.Add(param);
                    _storage.ExecuteNonCommand(command);
                }
            }
        }

        internal void DeleteFromMwlFile(Guid orderId)
        {
            const string selectInput = "SELECT Id FROM MWL_File WHERE OrderId = @orderId";
            const string deleteInput = "DELETE FROM MWL_File WHERE OrderId = @orderId";

            SelectAndDelete(orderId, selectInput, deleteInput);
        }

        internal void DeleteFromMwlStatus(Guid orderId)
        {
            const string selectInput = "SELECT Id FROM MWL_Status WHERE ParentId = @orderId";
            const string deleteInput = "DELETE FROM MWL_Status WHERE ParentId = @orderId";

            SelectAndDelete(orderId, selectInput, deleteInput);
        }

        internal void DeleteFromMwlTask(Guid orderId)
        {
            const string selectInput = "SELECT Id FROM MWL_Task WHERE OrderId = @orderId";
            const string deleteInput = "DELETE FROM MWL_Task WHERE OrderId = @orderId";

            SelectAndDelete(orderId, selectInput, deleteInput);
        }

        internal void DeleteFromMwlOrder(Guid orderId)
        {
            const string deleteInput = "DELETE FROM MWL_Order WHERE Id = @orderId";

            using (IDbCommand command = _storage.CreateCommand(deleteInput))
            {
                var param = _storage.CreateParameter("@orderId", orderId);
                command.Parameters.Add(param);
                _storage.ExecuteNonCommand(command);
            }
        }

        internal void DeleteFromSyncTables(Guid entityId)
        {
            string input = "DELETE FROM PMC_Synchronization_Property_Metadata WHERE EntityId = @entityId";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                var param = _storage.CreateParameter("@entityId", entityId);
                command.Parameters.Add(param);
                _storage.ExecuteNonCommand(command);
            }

            input = "DELETE FROM PMC_Synchronization_Metadata WHERE EntityId = @entityId";

            using (IDbCommand command = _storage.CreateCommand(input))
            {
                var param = _storage.CreateParameter("@entityId", entityId);
                command.Parameters.Add(param);
                _storage.ExecuteNonCommand(command);
            }
        }


        //internal void DeleteEntities(string entityTypeName, ICollection<Guid> entityIds)
        //{
        //    try
        //    {
        //        if (entityTypeName == _eltelOrderEntityType.Name)
        //        {
        //            DeleteLocally<EltelOrderEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _eltelResourceStateEntityType.Name)
        //        {
        //            DeleteLocally<EltelResourceStateEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _eltelTimeMaterialEntityType.Name)
        //        {
        //            DeleteLocally<EltelTimeMaterialEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _fieldValueEntityType.Name)
        //        {
        //            DeleteLocally<FieldValueEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _fileEntityType.Name)
        //        {
        //            DeleteLocally<FileEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _mwlPxoAnswerEntityType.Name)
        //        {
        //            DeleteLocally<MwlPxoAnswerEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _mwlPxoEntityType.Name)
        //        {
        //            DeleteLocally<MwlPxoEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _orderBroadcastEntityType.Name)
        //        {
        //            DeleteLocally<OrderBroadcastEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _orderSearchEntityType.Name)
        //        {
        //            DeleteLocally<OrderSearchEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _orderSearchQueryEntityType.Name)
        //        {
        //            DeleteLocally<OrderSearchQueryEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _resourceStateEntityType.Name)
        //        {
        //            DeleteLocally<ResourceStateEntity>(entityIds);
        //        }
        //        else if (entityTypeName == _statusEntityType.Name)
        //        {
        //            DeleteLocally<StatusEntity>(entityIds);
        //        }
        //        else
        //        {
        //            var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", "Could not delete entities because no type was found for entity type: " + entityTypeName);
        //            _log.Write(logItem);
        //        }
        //    }
        //    //catch (SynchronizedStorageException ex)
        //    //{
        //    //    var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", "Failed to call DeleteLocally for entity type: " + entityTypeName, ex.ToString());
        //    //    _log.Write(logItem);
        //    //}
        //    catch (Exception ex)
        //    {
        //        var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", "Failed to call DeleteLocally for entity type: " + entityTypeName, ex.ToString());
        //        _log.Write(logItem);
        //    }

        //}

        //private void DeleteLocally<T>(ICollection<Guid> entityIds) where T : ISynchronizable
        //{
        //    var failedDeletes = new List<Guid>();
        //    foreach (Guid entityId in entityIds)
        //    {
        //        var list = new List<Guid>(1);
        //        list.Add(entityId);

        //        try
        //        {
        //            _sync.DeleteLocally<T>(list);
        //        }
        //        catch (Exception ex)
        //        {
        //            var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", "Exception when caling DeleteLocally", ex.ToString());
        //            _log.Write(logItem);
        //            failedDeletes.Add(entityId);
        //        }
        //    }

        //    if (failedDeletes.Count > 0)
        //    {
        //        string dump = "Too many failed deletes, id's will not be logged.";
        //        if (failedDeletes.Count < 50)
        //        {
        //            dump = string.Join(",", failedDeletes);
        //        }

        //        var body = "Failed to delete {0} out of {1} entities for entity type: {2}";
        //        body = string.Format(body, failedDeletes.Count, entityIds.Count, typeof(T).Name);
        //        var logItem = new PreCom.Core.Log.LogItem(_syncReset, LogLevel.Error, "", "", body, dump);
        //        _log.Write(logItem);
        //    }
        //}
    }
}
