﻿using System;
using System.Collections.Generic;

using PreCom;
using PreCom.Core;
using PreCom.Core.Modules;

namespace SyncMaintenance.PC
{
    public class SyncMaintenance : ModuleBase, IForm
    {
        #region Fields
        
        private readonly StorageBase _storage;
        private readonly ILog _log;
        private readonly Db _db;

        /// <summary>
        /// The PreCom form.
        /// </summary>
        private MainForm _form;
        private bool _initialized;

        #endregion

        public SyncMaintenance()
        {
            _storage = Application.Instance.Storage;
            _log = Application.Instance.CoreComponents.Get<ILog>();
            _db = new Db(_storage, _log, this);
        }
        
        #region ModuleBase Members

        public override bool Initialize()
        {
            _initialized = true;
            return true;
        }

        internal void DeleteOrders()
        {
            DeleteUnmodifiedOrders();
            DeleteOrdersThatAreInSyncTablesButNotInOrderTable();
        }

        private void DeleteUnmodifiedOrders()
        {
            List<Guid> ordersToDelete = _db.GetUnmodifiedEntities("EltelOrderEntity");
            DeleteOrders(ordersToDelete);
        }

        private void DeleteOrdersThatAreInSyncTablesButNotInOrderTable()
        {
            List<Guid> ordersToDelete = _db.GetOrderIdsToRemove();
            DeleteOrders(ordersToDelete);
        }

        internal void DeleteOrders(List<Guid> ordersToDelete)
        {
            foreach (Guid orderId in ordersToDelete)
            {
                _db.DeleteFromTimeMaterial(orderId);
                _db.DeleteFromOrderBroadcast(orderId);
                _db.DeleteFromMwlPxo(orderId);
                _db.DeleteFromMwlPxoAnswerAndProperty(orderId);
                _db.DeleteFromMwlFile(orderId);
                _db.DeleteFromMwlStatus(orderId);
                _db.DeleteFromMwlTask(orderId);
                _db.DeleteFromMwlOrder(orderId);
                _db.DeleteFromSyncTables(orderId);
            }
        }

        internal List<string> GetEntityTypes()
        {
            return _db.GetEntityTypes();
        }

        internal List<Guid> GetEntityCount(string table)
        {
            return _db.GetEntityCount(table);
        }

        internal List<Guid> GetMetadataCount(string entityType)
        {
            return _db.GetMetadataCount(entityType);
        }

        internal List<Guid> GetMatchingCount(string entityType)
        {
            return _db.GetMatchingCount(entityType);
        }

        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _initialized = false;
            return true;
        }

        public override bool IsInitialized
        {
            get { return _initialized; }
        }

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                return "SyncMaintenance";
            }
        }

        #endregion

        #region IForm Members

        /// <summary>
        /// The activate function gets called by the framework before the Form is displayed.
        /// </summary>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false.
        /// </returns>
        public bool Activate()
        {
            _form = new MainForm(this);

            return true;
        }

        /// <summary>
        /// The deactivate function gets called by the framework before the Form is hidden.
        /// </summary>
        /// <param name="cancel">A return parameter (out) to accept to be deactivated or not. 
        /// By setting cancel to true the component can force to stay activated</param>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false.
        /// </returns>
        public bool Deactivate(out bool cancel)
        {
            _form.Dispose();
            _form = null;

            cancel = false;

            return true;
        }

        /// <summary>
        /// The main form of the component.
        /// </summary>
        /// <value>The PreCom form instance.</value>
        public PreCom.Controls.PreComForm Form
        {
            get
            {
                return _form;
            }
        }

        /// <summary>
        /// The image with the top left (1,1) pixel color used as transparent color when drawing the image
        /// of the IForm to be displayed on a menu button.
        /// </summary>
        /// <value></value>
        public System.Drawing.Image Icon
        {
            get
            {
                // TODO: Change icon.
                return Properties.Resources.icon_32x32;
            }
        }

        /// <summary>
        /// The event to invoke to request to be shown.
        /// </summary>
        public event ShowRequestDelegate ShowRequest;

        #endregion
    }
}
