﻿using System;

namespace SyncMonitor
{
    public class MonitorLogItem
    {
        #region Properties for the client log table

        public int ID { get; set; }
        public DateTime LogTime { get; set; }
        public int LogLevel { get; set; }
        public int LogCustomLevel { get; set; }
        public string LogID { get; set; }
        public string LogSource { get; set; }
        public string LogCaption { get; set; }
        public string LogDescription { get; set; }
        public string LogDump { get; set; }

        #endregion Properties for the client log table
    }
}
