﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PreCom.Core;

namespace SyncMonitor
{
    internal class Db
    {
        private readonly StorageBase _storage;
        private string _tableName;
        private string _statusTableName;

        public Db(StorageBase storage)
        {
            _storage = storage;
        }

        internal void CreateTable()
        {
            if (!_storage.ObjectExists(_tableName, ObjectType.Table))
            {
                var sql = string.Concat("CREATE TABLE [" + _tableName + "](",
                    "EntityId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,",
                    ")");

                using (var command = _storage.CreateCommand(sql))
                {
                    _storage.ExecuteNonCommand(command);
                }
            }
        }

        internal void CreateStatusTable()
        {
            if (!_storage.ObjectExists(_statusTableName, ObjectType.Table))
            {
                var sql = string.Concat("CREATE TABLE [" + _statusTableName + "](",
                    "Id INT NOT NULL PRIMARY KEY,",
                    "EntityId INT NOT NULL,",
                    "LastSendDateUtc DateTime NOT NULL,",
                    ")");

                using (var command = _storage.CreateCommand(sql))
                {
                    _storage.ExecuteNonCommand(command);
                }
            }
        }
    }
}
