﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using PreCom;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Modules;
using LogItem = PreCom.Core.Log.LogItem;

namespace SyncMonitor
{
    public class SyncMonitorModule : PreCom.Core.ModuleBase
    {
        #region Fields

        private readonly CommunicationBase _com;
        private readonly StorageBase _storage;
        private readonly SettingsBase _settings;
        private readonly ILog _log;

        /// <summary>
        /// Indicates if the module is initialized or not.
        /// </summary>
        private bool _isInitialized;

        private Thread _monitorThread;
        private int _numLogsToReadOnSyncError;
        private int _numPeriodicLogsToRead;
        private SemaphoreSlim _semaphore;
        private bool _monitorActive;
        private int _monitorInterval;
        private TimeSpan _periodicLogInterval;
        private List<Guid> _reportedSyncErrorGuids;
        private DateTime _lastPeriodicLogSendDateUtc;
        private int _lastPeriodicLogId;
        private int _initialMonitorDelay;

        #endregion

        ////public SyncMonitorModule(CommunicationBase com, StorageBase storage, SettingsBase settings, ILog log)
        ////{
        ////    _com = com;
        ////    _storage = storage;
        ////    _settings = settings;
        ////    _log = log;

        ////    _reportedSyncErrorGuids = new List<Guid>();
        ////    _lastPeriodicLogSendDateUtc = DateTime.MinValue;
        ////    _lastPeriodicLogId = 0;
        ////}

        public SyncMonitorModule()
        {
            _com = Application.Instance.Communication;
            _storage = Application.Instance.Storage;
            _settings = Application.Instance.Settings;
            _log = Application.Instance.CoreComponents.Get<ILog>();

            _reportedSyncErrorGuids = new List<Guid>();
            _lastPeriodicLogSendDateUtc = DateTime.MinValue;
            _lastPeriodicLogId = 0;
        }

        #region ModuleBase Members

        /// <summary>
        /// Gets a value indicating whether this instance is initialized.
        /// </summary>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                return "SyncMonitorModule";
            }
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>true if initialized successfully, otherwise false.</returns>
        public override bool Initialize()
        {
            ReadSettings();
            ////StartMonitoring();

            _com.LoginStatus += OnLoginStatusChanged;

            _isInitialized = true;
            return true;
        }

        private void OnLoginStatusChanged(LoginEventArgs e)
        {
            switch (e.Status)
            {
                case LoginStatusInfo.LoginOK:
                    StartMonitoring();
                    break;
                case LoginStatusInfo.LogoutOK:
                case LoginStatusInfo.LoginError:
                    StopMonitoring();
                    break;
            }
        }

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">true to cancel the dispose of the module, false if disposing is ok.</param>
        /// <returns>true if dispose was successful, otherwise false.</returns>
        public override bool Dispose(out bool cancel)
        {
            _com.LoginStatus -= OnLoginStatusChanged;
            StopMonitoring();

            cancel = false;
            _isInitialized = false;
            return true;
        }

        #endregion

        protected void ReadSettings()
        {
            _numLogsToReadOnSyncError = _settings.Read(this, "NumLogsToReadOnSyncError", 200, true);
            _numPeriodicLogsToRead = _settings.Read(this, "NumPeriodicLogsToRead", 200, true);

            // Use a default interval of 15 min (900 seconds)
            _monitorInterval = _settings.Read(this, "MonitorInterval", 900, true);

            // Use a default interval of 1 hour (3600 seconds)
            int periodicLogInterval = _settings.Read(this, "PeriodicLogInterval", 3600, true);
            _periodicLogInterval = new TimeSpan(0, 0, periodicLogInterval);

            //_monitorInterval = 5;
            //_periodicLogInterval = new TimeSpan(0, 0, 10);

            // TODO: change so this setting is in seconds instead of millis
            _initialMonitorDelay = _settings.Read(this, "InitialMonitorDelay", 0, true);
            // TODO: add setting to control if only error logs should be sent periodically.
        }

        protected int GetNumLogsToRead()
        {
            return _numLogsToReadOnSyncError;
        }

        protected void StartMonitoring()
        {
            if (!_monitorActive)
            {
                _monitorActive = true;
                _semaphore = new SemaphoreSlim(0, 1);

                _monitorThread = new Thread(SyncMonitor);
                _monitorThread.IsBackground = true;
                _monitorThread.Start();
            }
        }

        protected void StopMonitoring()
        {
            if (_monitorActive)
            {
                _monitorActive = false;
                _semaphore.Release();
                _monitorThread.Join(2000);
            }
        }

        /// <summary>
        /// Performs the sync status check.
        /// </summary>
        /// <returns>true if the sync seems to be working (no sync errors found), otherwise false</returns>
        protected List<Guid> ReadNewSyncErrors()
        {
            var newSyncErrorGuids = new List<Guid>();

            List<Guid> errorGuids = new List<Guid>();

            try
            {
                errorGuids = ReadSyncErrorGuids();
            }
            catch (Exception ex)
            {
                var body = "The sync status could not be determined because of a DB read error. Sync status is unknown.";
                LogE(ex, "SM_E_11", body);
            }

            ////List<Guid> errorGuids = new List<Guid>();
            ////errorGuids.Add(new Guid("096d1a1e-56c0-4881-aecb-03dcabb97746"));

            if (errorGuids.Count > 0)
            {
                var reportedSyncErrorGuids = GetReportedSyncErrorGuids();

                foreach (var guid in errorGuids)
                {
                    if (!reportedSyncErrorGuids.Contains(guid))
                    {
                        newSyncErrorGuids.Add(guid);
                    }
                }
            }

            return newSyncErrorGuids;
        }

        private List<Guid> GetReportedSyncErrorGuids()
        {
            // TODO: read guids from DB.
            // TODO: make it possible to clear the list of guids that have already been reported...
            return _reportedSyncErrorGuids;
        }

        private void AddReportedSyncErrorGuids(List<Guid> guids)
        {
            // TODO: write guids to db.
            _reportedSyncErrorGuids.AddRange(guids);
        }

        protected EntityList<MonitorLogItem> ReadLogs(int numLogsToRead)
        {
            var logs = new EntityList<MonitorLogItem>();
            string commandString = "Select top(" + numLogsToRead + ") * from PMC_Log_2 order by id desc";

            using (var command = _storage.CreateCommand(commandString))
            {
                using (var reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        var logItem = CreateLogItem(reader);

                        // TODO: increase performance by using add and then reversing the list.
                        // Use insert so the logs are sent in the correct order to the server.
                        logs.Insert(0, logItem);
                    }
                }
            }

            return logs;
        }

        protected EntityList<MonitorLogItem> ReadLogs(int numLogsToRead, int lastPeriodicLogId)
        {
            var logs = new EntityList<MonitorLogItem>();
            string sql = string.Concat("Select top(" + numLogsToRead + ") * ",
                "from PMC_Log_2 ",
                "where ID > @lastPeriodicLogId ",
                "AND (LogLevel = 1 OR LogLevel = 2) ",
                "order by id desc");

            using (var command = _storage.CreateCommand(sql))
            {
                var param = _storage.CreateParameter("@lastPeriodicLogId", lastPeriodicLogId);
                command.Parameters.Add(param);

                using (var reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        var logItem = CreateLogItem(reader);

                        // TODO: increase performance by using add and then reversing the list.
                        // Use insert so the logs are sent in the correct order to the server.
                        logs.Insert(0, logItem);
                    }
                }
            }

            return logs;
        }

        protected MonitorLogItem CreateLogItem(IDataReader reader)
        {
            var m = new MonitorLogItem();
            m.ID = (int)reader["ID"];
            m.LogID = reader["LogID"] as string;

            m.LogLevel = (int)reader["LogLevel"];
            m.LogDescription = reader["LogDescription"] as string;
            m.LogCustomLevel = (int)reader["LogCustomLevel"];
            m.LogDump = reader["LogDump"] as string;
            m.LogCaption = reader["LogCaption"] as string;
            m.LogSource = reader["LogSource"] as string;
            m.LogTime = (DateTime)reader["LogTime"];

            return m;
        }

        protected void SendLogs(EntityList<MonitorLogItem> logs)
        {
            var requestArgs = new RequestArgs(QueuePriority.Low, Persistence.UntilDelivered);
            _com.Send(logs, requestArgs);
        }

        private List<Guid> ReadSyncErrorGuids()
        {
            List<Guid> result = new List<Guid>();

            var sb = new StringBuilder();
            sb.AppendLine("SELECT ID");
            sb.AppendLine("FROM [MWL_TimeMaterial]");
            sb.AppendLine("WHERE statusTypeID = 200");
            sb.AppendLine("AND Number < 1000");
            sb.AppendLine("AND CreatedDateTime < dateadd(mi,-5,GETDATE())");

            using (var command = _storage.CreateCommand(sb.ToString()))
            {
                using (var reader = _storage.ExecuteQueryCommand(command))
                {
                    while (reader.Read())
                    {
                        result.Add((Guid)reader["ID"]);
                    }
                }
            }

            return result;
        }

        private void SyncMonitor()
        {
            if (_initialMonitorDelay > 0)
            {
                Thread.Sleep(_initialMonitorDelay);
            }

            while (_monitorActive)
            {
                // TODO: this try/catch should be unnecessary.
                try
                {
                    if (_com.IsLoggedIn)
                    {
                        SyncStatusJob();

                        // TODO: the periodic log job interval depends on the monitor interval, maybe we should have separate threads for the jobs?
                        PeriodicLogJob();
                    }
                }
                catch (Exception ex)
                {
                    var body = "Error when performing sync status job or periodic log job.";
                    LogE(ex, "SM_E_7", body);
                }

                _semaphore.Wait(_monitorInterval * 1000);
            }
        }

        private void SyncStatusJob()
        {
            List<Guid> newSyncErrors = ReadNewSyncErrors();

            if (newSyncErrors.Count > 0)
            {
                var header = "Sync error";
                var body = "Possible sync error found for id(s): {0}";
                body = string.Format(body, string.Join(", ", newSyncErrors));
                var logItem = new LogItem(this, LogLevel.Error, "SM_E_1", header, body);
                _log.Write(logItem);

                foreach (var guid in newSyncErrors)
                {
                    // Do sync specific checks with the guid here.
                    DoSyncTableReads(guid);
                }

                // A possible sync error has been found, read the logs and send them to the server.
                try
                {
                    var logs = ReadLogs(GetNumLogsToRead());

                    if (logs.Count > 0)
                    {
                        try
                        {
                            SendLogs(logs);

                            // add guids to a table so they aren't reported again
                            AddReportedSyncErrorGuids(newSyncErrors);
                        }
                        catch (Exception ex)
                        {
                            var body2 = "Error in sync status job. Failed to send sync error logs to server.";
                            LogE(ex, "SM_E_2", body2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var body3 = "Error in sync status job. Failed to read sync error logs from DB.";
                    LogE(ex, "SM_E_3", body3);
                }
            }
        }

        private void PeriodicLogJob()
        {
            try
            {
                DateTime lastLogReadDateUtc = GetLastPeriodicLogReadDate();
                if (DateTime.UtcNow > lastLogReadDateUtc.Add(_periodicLogInterval))
                {
                    int lastPeriodicLogId = GetLastPeriodicLogId();

                    // Get error logs
                    var logs = ReadLogs(_numPeriodicLogsToRead, lastPeriodicLogId);

                    if (logs.Count > 0)
                    {
                        try
                        {
                            // Send error logs
                            SendLogs(logs);

                            // Update last send date
                            SetLastPeriodicLogReadDate(DateTime.UtcNow);

                            // Get the last log id from the list.
                            lastPeriodicLogId = logs[logs.Count - 1].ID;
                            SetLastPeriodicLogId(lastPeriodicLogId);
                        }
                        catch (Exception ex)
                        {
                            var body = "Error when performing periodic log job. Failed to send logs to server.";
                            LogE(ex, "SM_E_8", body);
                        }
                    }
                    else
                    {
                        // Update the last send date even if there were no logs to send. We did the check...
                        SetLastPeriodicLogReadDate(DateTime.UtcNow);
                    }
                }
            }
            catch (Exception ex)
            {
                var body = "Error when performing periodic log job. Failed to read logs from DB.";
                LogE(ex, "SM_E_9", body);
            }
        }

        private DateTime GetLastPeriodicLogReadDate()
        {
            if (_lastPeriodicLogSendDateUtc == DateTime.MinValue)
            {
                // TODO: read last periodic send date from DB
                //_lastPeriodicLogSendDateUtc = GetFromDB();
            }

            return _lastPeriodicLogSendDateUtc;
        }

        private void SetLastPeriodicLogReadDate(DateTime lastSendDate)
        {
            _lastPeriodicLogSendDateUtc = lastSendDate;
            // TODO: persist last periodic send date to DB
        }

        private int GetLastPeriodicLogId()
        {
            if (_lastPeriodicLogId == 0)
            {
                // TODO: read last periodic log id from DB
                //_lastPeriodicLogId = GetFromDB();
            }

            return _lastPeriodicLogId;
        }

        private void SetLastPeriodicLogId(int id)
        {
            _lastPeriodicLogId = id;
            // TODO: persist last periodic id to DB
        }

        internal void DoSyncTableReads(Guid guid)
        {
            ReadAndLogSyncMetadata(guid);
            ReadAndLogSyncPartialEntity(guid);
            ReadAndLogSyncPropertyMetadata(guid);
        }

        private void ReadAndLogSyncMetadata(Guid guid)
        {
            try
            {
                string commandString = "select * from PMC_Synchronization_Metadata where EntityId = @guid";

                using (var command = _storage.CreateCommand(commandString))
                {
                    var param = _storage.CreateParameter("@guid", guid);
                    command.Parameters.Add(param);
                    using (var reader = _storage.ExecuteQueryCommand(command))
                    {
                        bool resultEmpty = true;
                        while (reader.Read())
                        {
                            resultEmpty = false;
                            var entityType = (string)reader["EntityType"];
                            var entityVersion = (int)reader["EntityVersion"];
                            var entityStatus = (int)reader["EntityStatus"];
                            var sessionId = (Guid)reader["SessionId"];
                            var changeId = (Guid)reader["ChangeId"];

                            var body = "Found sync metadata for id: {0}, entity type: {1}, entity version: {2}, entity status: {3}, session id: {4}, change id: {5}";
                            body = string.Format(body, guid, entityType, entityVersion, entityStatus, sessionId, changeId);
                            LogI("SM_I_1", "Sync metadata found", body);
                        }

                        if (resultEmpty)
                        {
                            var body = "Sync metadata not found for id: {0}";
                            body = string.Format(body, guid);
                            LogW("SM_W_1", "Sync metadata not found", body);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var body = "Failed to read sync metadata for id: {0}";
                body = string.Format(body, guid);
                LogE(ex, "SM_E_4", body);
            }
        }

        private void ReadAndLogSyncPartialEntity(Guid guid)
        {
            try
            {
                string commandString = "select * from PMC_Synchronization_Partial_Entity where EntityId = @guid";

                using (var command = _storage.CreateCommand(commandString))
                {
                    var param = _storage.CreateParameter("@guid", guid);
                    command.Parameters.Add(param);
                    using (var reader = _storage.ExecuteQueryCommand(command))
                    {
                        bool resultEmpty = true;
                        while (reader.Read())
                        {
                            resultEmpty = false;
                            var sessionId = (Guid)reader["SessionId"];
                            var packetNumber = (int)reader["PacketNumber"];
                            var partialEntity = (string)reader["PartialEntity"];
                            var insertionDate = (DateTime)reader["InsertionDate"];

                            var body =
                                "Found sync partial entity for id: {0}, session id: {1}, packet number: {2}, partial entity: {3}, insertion date: {4}";
                            body = string.Format(body, guid, sessionId, packetNumber, partialEntity, insertionDate);
                            LogI("SM_I_2", "Sync partial entity found", body);
                        }

                        if (resultEmpty)
                        {
                            var body = "Sync partial entity not found for id: {0}";
                            body = string.Format(body, guid);
                            LogW("SM_W_2", "Sync partial entity not found", body);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var body = "Failed to read sync partial entity for id: {0}";
                body = string.Format(body, guid);
                LogE(ex, "SM_E_5", body);
            }
        }

        private void ReadAndLogSyncPropertyMetadata(Guid guid)
        {
            try
            {
                bool resultEmpty = true;
                var sb = new StringBuilder();
                sb.AppendLine(string.Format("Found sync property metadata for id: {0}:", guid));

                string commandString = "select * from PMC_Synchronization_Property_Metadata where EntityId = @guid";

                using (var command = _storage.CreateCommand(commandString))
                {
                    var param = _storage.CreateParameter("@guid", guid);
                    command.Parameters.Add(param);
                    using (var reader = _storage.ExecuteQueryCommand(command))
                    {
                        while (reader.Read())
                        {
                            resultEmpty = false;
                            var propertyName = (string)reader["PropertyName"];
                            var isChanged = (bool)reader["IsChanged"];

                            sb.AppendLine(string.Format("property name: {0}, is changed: {1}", propertyName, isChanged));
                        }
                    }
                }

                if (resultEmpty)
                {
                    var body = "Sync property metadata not found for id: {0}";
                    body = string.Format(body, guid);
                    LogW("SM_W_3", "Sync property metadata not found", body);
                }
                else
                {
                    LogI("SM_I_3", "Sync property metadata found", sb.ToString());
                }
            }
            catch (Exception ex)
            {
                var body = "Failed to read sync property metadata for id: {0}";
                body = string.Format(body, guid);
                LogE(ex, "SM_E_6", body);
            }
        }

        private void LogE(Exception ex, string logId, string body)
        {
            var logItem = new LogItem(this, LogLevel.Error, logId, ex.GetType().Name, body, ex.ToString());
            _log.Write(logItem);
        }

        private void LogW(string logId, string header, string body)
        {
            var logItem = new LogItem(this, LogLevel.Warning, logId, header, body);
            _log.Write(logItem);
        }

        private void LogI(string logId, string header, string body)
        {
            var logItem = new LogItem(this, LogLevel.Information, logId, header, body);
            _log.Write(logItem);
        }
    }
}
