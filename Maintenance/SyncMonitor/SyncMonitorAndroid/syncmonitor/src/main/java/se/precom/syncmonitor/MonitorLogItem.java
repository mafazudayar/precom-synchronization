package se.precom.syncmonitor;

import java.util.Date;

import se.precom.core.communication.EntityBase;

/**
 * Created by Daniel Claesén on 2014-10-14.
 */
public class MonitorLogItem extends EntityBase {
    public int ID;
    public Date LogTime;
    public int LogLevel;
    public int LogCustomLevel;
    public String LogID;
    public String LogSource;
    public String LogCaption;
    public String LogDescription;
    public String LogDump;

    public String toString() {
        return String.format("%s; %s; %s; %s; %s; %s; %s; %s; %s; ", ID, LogTime, LogLevel, LogCustomLevel, LogID, LogSource, LogCaption, LogDescription, LogDump);
    }
}
