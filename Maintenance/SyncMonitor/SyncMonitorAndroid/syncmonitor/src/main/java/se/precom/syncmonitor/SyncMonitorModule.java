package se.precom.syncmonitor;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import se.precom.core.ModuleBase;
import se.precom.core.communication.CommunicationBase;
import se.precom.core.communication.EntityList;
import se.precom.core.communication.LoginStatusDetails;
import se.precom.core.communication.MaxMessageSizeException;
import se.precom.core.communication.NotLoggedInException;
import se.precom.core.communication.Persistence;
import se.precom.core.communication.QueuePriority;
import se.precom.core.communication.RequestArgs;
import se.precom.core.communication.TypeReference;
import se.precom.core.log.LogBase;
import se.precom.core.settings.SettingsBase;
import se.precom.core.storage.StorageBase;

/**
 * Created by Daniel Claesén on 2014-10-13.
 */
public class SyncMonitorModule extends ModuleBase {

    private final CommunicationBase _com;
    private final StorageBase _storage;
    private final SettingsBase _settings;
    private final LogBase _log;
    private final Db _db;

    private final String _logId;

    private final List<UUID> _reportedSyncErrorIds;
    private final Object _signalSyncMonitorLoop;

    private final CommunicationBase.OnLoginStatusListener _loginStatusListener;

    private long _initialMonitorDelay;
    private boolean _monitorActive;
    private long _monitorInterval;
    private Thread _syncMonitor;
    private int _numLogsToReadOnSyncError;
    private int _numPeriodicLogsToRead;
    private int _lastPeriodicLogId;
    private Date _lastPeriodicLogSendDateUtc;
    private int _periodicLogInterval;

    @Inject
    public SyncMonitorModule(CommunicationBase com, StorageBase storage, SettingsBase settings, LogBase log) {
        _com = com;
        _storage = storage;
        _settings = settings;
        _log = log;

        _logId = "SYNC_MON";
        _db = new Db(_storage, log, _logId);

        _reportedSyncErrorIds = new ArrayList<UUID>();

        _lastPeriodicLogSendDateUtc = new Date(0);
        _lastPeriodicLogId = 0;

        _signalSyncMonitorLoop = new Object();

        _loginStatusListener = new CommunicationBase.OnLoginStatusListener() {
            @Override
            public void onLoginStatus(LoginStatusDetails loginStatusDetails) {
                switch(loginStatusDetails.getStatus()) {
                    case LOGGED_IN:
                        startMonitor();
                        break;
                    case LOGGED_OUT:
                    case LOGIN_ERROR:
                        stopMonitor();
                        break;
                }
            }
        };
    }

    @Override
    public void initialize() throws Exception {
        readSettings();
        _com.addOnLoginStatusListener(_loginStatusListener);
    }

    @Override
    public void dispose() {
        _com.removeOnLoginStatusListener(_loginStatusListener);
        stopMonitor();
    }

    protected void readSettings() throws SettingsBase.ReadException {
        _numLogsToReadOnSyncError = _settings.read(getName(), "NumLogsToReadOnSyncError", 200, true);
        _numPeriodicLogsToRead = _settings.read(getName(), "NumPeriodicLogsToRead", 100, true);

        _initialMonitorDelay = _settings.read(getName(), "InitialMonitorDelay", 0, true);

        // Use a default interval of 1 hour (3600 seconds)
        _periodicLogInterval = _settings.read(getName(), "PeriodicLogInterval", 3600, true);

        // Use a default interval of 15 min (900 seconds)
        _monitorInterval = _settings.read(getName(), "MonitorInterval", 900, true);

        // hard coded values
//        _periodicLogInterval = 30;
//        _monitorInterval = 5;
    }

    private void startMonitor() {
        if (!_monitorActive)
        {
            _monitorActive = true;

            _syncMonitor = new Thread(new Runnable() {
                public void run() {
                    SyncMonitorModule.this.syncMonitor();

//                    synchronized (_signalMonitorStopped) {
//                        _signalMonitorStopped.notifyAll();
//                    }
                }
            }, "SyncMonitorThread");

            _syncMonitor.start();
        }
    }

    private void stopMonitor() {
        if (_monitorActive){
            _monitorActive = false;

            synchronized (_signalSyncMonitorLoop) {
                _signalSyncMonitorLoop.notify();
            }
        }
    }


    private void syncMonitor() {

        if (_initialMonitorDelay > 0)
        {
            try {
                Thread.sleep(_initialMonitorDelay * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        while (_monitorActive) {

            syncStatusJob();

            // TODO: the periodic log job interval depends on the monitor interval, maybe we should have separate threads for the jobs?
            periodicLogJob();

            synchronized (_signalSyncMonitorLoop) {
                try {
                    _signalSyncMonitorLoop.wait(_monitorInterval * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void syncStatusJob() {
        List<UUID> newSyncErrors = readNewSyncErrors();

        if (newSyncErrors.size() > 0) {
            String header = "Sync error";
            String body = "Possible sync error found for id(s): %s";
            body = String.format(body, TextUtils.join(", ", newSyncErrors));
            _log.e(getName(), _logId, header, body);

            for (int i = 0; i < newSyncErrors.size(); i++) {
                doSyncTableReads(newSyncErrors.get(i));
            }

            try {
                EntityList<MonitorLogItem> logs = _db.readLogs(_numLogsToReadOnSyncError);

                if(logs.size() > 0) {
                    try {
                        sendLogs(logs);

                        addReportedSyncErrorIds(newSyncErrors);
                    } catch(Exception ex) {
                        String body2 = "Error in sync status job. Failed to send sync error logs to server.";
                        _log.e(getName(), _logId, body2, ex);
                    }
                }
            } catch(Exception ex) {
                String body3 = "Error in sync status job. Failed to read sync error logs from DB.";
                _log.e(getName(), _logId, body3, ex);
            }
        }
    }

    private void doSyncTableReads(UUID id) {
        readAndLogSyncMetadata(id);
        readAndLogSyncPartialEntity(id);
        readAndLogSyncPropertyMetadata(id);
    }

    private void readAndLogSyncMetadata(UUID id) {
        try {
            _db.readAndLogSyncMetadata(id);
        } catch(Exception ex) {
            String body = "Failed to read sync metadata for id: %s";
            body = String.format(body, id);
            _log.e(getName(), _logId, body, ex);
        }
    }

    private void readAndLogSyncPartialEntity(UUID id) {
        try {
            _db.readAndLogSyncPartialEntity(id);
        } catch(Exception ex) {
            String body = "Failed to read sync partial entity for id: %s";
            body = String.format(body, id);
            _log.e(getName(), _logId, body, ex);
        }
    }

    private void readAndLogSyncPropertyMetadata(UUID id) {
        try {
            _db.readAndLogSyncPropertyMetadata(id);
        } catch(Exception ex) {
            String body = "Failed to read sync property metadata for id: %s";
            body = String.format(body, id);
            _log.e(getName(), _logId, body, ex);
        }
    }

    private void sendLogs(EntityList<MonitorLogItem> logs) throws NotLoggedInException, MaxMessageSizeException {
        RequestArgs requestArgs = new RequestArgs(new TypeReference<EntityList<MonitorLogItem>>() {}, QueuePriority.LOW, Persistence.UNTIL_DELIVERED);
        _com.send(logs, requestArgs);
    }

    private void addReportedSyncErrorIds(List<UUID> newSyncErrors) {
        // TODO: write ids to db.
        _reportedSyncErrorIds.addAll(newSyncErrors);
    }

    private List<UUID> getReportedSyncErrorIds() {
        // TODO: read ids from DB.
        // TODO: make it possible to clear the list of ids that have already been reported...
        return _reportedSyncErrorIds;
    }

    private void periodicLogJob() {
        Date lastLogReadDateUtc = getLastPeriodicLogReadDate();
        Date now = new Date();

        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(lastLogReadDateUtc); // sets calendar time/date
        cal.add(Calendar.SECOND, _periodicLogInterval);
        Date nextLogReadDateUtc = cal.getTime(); // returns new date object, with periodic log interval added

        // debug logs...
//        _log.d(getName(), _logId, "last read date", lastLogReadDateUtc.toString());
//        _log.d(getName(), _logId, "next read date", nextLogReadDateUtc.toString());
//        _log.d(getName(), _logId, "last periodic log id", String.valueOf(getLastPeriodicLogId()));

        if(nextLogReadDateUtc.before(now)) {
            int lastPeriodicLogId = getLastPeriodicLogId();
            EntityList<MonitorLogItem> logs = _db.readLogs(_numPeriodicLogsToRead, lastPeriodicLogId);

            if(logs.size() > 0) {
                try {
                    sendLogs(logs);

                    setLastPeriodicLogReadDate(new Date());
                    lastPeriodicLogId = logs.get(logs.size() - 1).ID;
                    setLastPeriodicLogId(lastPeriodicLogId);

                } catch(Exception ex) {
                    String body = "Error when performing periodic log job. Failed to send logs to server.";
                    _log.e(getName(), _logId, body, ex);
                }
            } else {
                // Update the last send date even if there were no logs to send. We did the check...
                setLastPeriodicLogReadDate(new Date());
            }
        }
    }

    private Date getLastPeriodicLogReadDate() {
        if (_lastPeriodicLogSendDateUtc.equals(new Date(0)))
        {
            // TODO: read last periodic send date from DB
            //_lastPeriodicLogSendDateUtc = GetFromDB();
        }

        return _lastPeriodicLogSendDateUtc;
    }

    private int getLastPeriodicLogId()
    {
        if (_lastPeriodicLogId == 0)
        {
            // TODO: read last periodic log id from DB
            //_lastPeriodicLogId = GetFromDB();
        }

        return _lastPeriodicLogId;
    }

    private void setLastPeriodicLogId(int id)
    {
        _lastPeriodicLogId = id;
        // TODO: persist last periodic id to DB
    }

    private void setLastPeriodicLogReadDate(Date lastSendDate)
    {
        _lastPeriodicLogSendDateUtc = lastSendDate;
        // TODO: persist last periodic send date to DB
    }

    private List<UUID> readNewSyncErrors() {
        List<UUID> newSyncErrorIds = new ArrayList<UUID>();
        List<UUID> syncErrorIds = new ArrayList<UUID>();

        try {
            syncErrorIds = _db.readSyncErrors();
        } catch(Exception ex) {
            String body = "The sync status could not be determined because of a DB read error. Sync status is unknown.";
            _log.e(getName(), _logId, body, ex);
        }

        // hard coded values
//        syncErrorIds.add(UUID.fromString("96b3ffc0-5834-11e4-8ed6-0800200c9a66"));
//        syncErrorIds.add(UUID.fromString("96b3ffc0-5834-11e4-8ed6-0800200c9a62"));

        if(syncErrorIds.size() > 0) {
            for (int i = 0; i < syncErrorIds.size(); i++) {
                UUID id = syncErrorIds.get(i);
                if(!getReportedSyncErrorIds().contains(id)) {
                    newSyncErrorIds.add(id);
                }
            }
        }

        return newSyncErrorIds;
    }
}
