package se.precom.syncmonitor;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import se.precom.core.communication.EntityList;
import se.precom.core.log.LogBase;
import se.precom.core.log.LogItem;
import se.precom.core.log.LogLevel;
import se.precom.core.storage.StorageBase;

/**
 * Created by Daniel Claesén on 2014-10-14.
 */
class Db {
    private static String MWL_TIME_MATERIAL_TABLE_NAME = "MWL_TimeMaterial_Item";
    private static String LOG_TABLE_NAME = "PMC_Log";
    private static String KEY_COLUMN = "_id";
    private static String TIME_COLUMN = "LogTime";
    private static String LEVEL_COLUMN = "LogLevel";
    private static String CUSTOM_LEVEL_COLUMN = "LogCustomLevel";
    private static String ID_COLUMN = "LogID";
    private static String SOURCE_COLUMN = "LogSource";
    private static String CAPTION_COLUMN = "LogCaption";
    private static String DESCRIPTION_COLUMN = "LogDescription";
    private static String DUMP_COLUMN = "LogDump";

    private static String SYNC_METADATA_TABLE_NAME = "PMC_Synchronization_Metadata";
    private static String SYNC_PARTIAL_ENTITY_TABLE_NAME = "PMC_Synchronization_Partial_Entity";
    private static String SYNC_PROPERTY_METADATA_TABLE_NAME = "PMC_Synchronization_Property_Metadata";
    private static String SYNC_ENTITY_ID_COLUMN = "EntityId";

    private final StorageBase _storage;
    private final LogBase _log;

    private final String _logName;
    private final String _logId;

    Db(StorageBase storage, LogBase log, String logId) {
        _storage = storage;
        _log = log;
        _logId = logId;
        _logName = "SyncMonitor.Db";
    }

    List<UUID> readSyncErrors() {
        List<UUID> syncErrors = new ArrayList<UUID>();

        // TODO: confirm that query works
        String where = "StatusId = 200 AND Number < 1000 AND CreatedDateTime < datetime('now','-5 minutes');";

        // Use this query when testing in a non Eltel environment, since the Number column only exists in Eltel solutions.
//        String where = "StatusId = 200 AND CreatedDateTime < datetime('now','-5 minutes');";

        Cursor cursor = _storage.query(false, MWL_TIME_MATERIAL_TABLE_NAME, null, where, null, null, null, null, null);

        if(cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        String idString = cursor.getString(cursor.getColumnIndex("_id"));
                        UUID id = UUID.fromString(idString);

                        syncErrors.add(id);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return syncErrors;
    }

    EntityList<MonitorLogItem> readLogs(int numLogsToRead) {
        Cursor cursor = _storage.query(false, LOG_TABLE_NAME, null, null, null, null, null, KEY_COLUMN + " desc", String.valueOf(numLogsToRead));

        return readLogs(cursor);
    }

    EntityList<MonitorLogItem> readLogs(int numLogsToRead, int lastPeriodicLogId) {
        String where = KEY_COLUMN + " > ? AND (" + LEVEL_COLUMN + " = 'Error' OR " + LEVEL_COLUMN + " = 'Warning')";
        String[] whereArgs = new String[] {String.valueOf(lastPeriodicLogId)};
        Cursor cursor = _storage.query(false, LOG_TABLE_NAME, null, where, whereArgs, null, null, KEY_COLUMN + " desc", String.valueOf(numLogsToRead));

        return readLogs(cursor);
    }

    private EntityList<MonitorLogItem> readLogs(Cursor cursor) {
        EntityList<MonitorLogItem> logs = new EntityList<MonitorLogItem>();

        if(cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        MonitorLogItem logItem = new MonitorLogItem();
                        logItem.ID = cursor.getInt(cursor.getColumnIndex(KEY_COLUMN));

                        String dateString = cursor.getString(cursor.getColumnIndex(TIME_COLUMN));

                        try {
                            logItem.LogTime = LogItem.getDateFormat().parse(dateString);
                        } catch(Exception ex) {
                            _log.e(_logName, _logId, "Error parsing date when reading log from DB. Row will be skipped.", ex);
                            continue;
                        }

                        String logLevelString = cursor.getString(cursor.getColumnIndex(LEVEL_COLUMN));
                        LogLevel logLevel = LogLevel.valueOf(logLevelString.toUpperCase());
                        logItem.LogLevel = logLevel.ordinal();
                        logItem.LogCustomLevel = cursor.getInt(cursor.getColumnIndex(CUSTOM_LEVEL_COLUMN));
                        logItem.LogID = cursor.getString(cursor.getColumnIndex(ID_COLUMN));
                        logItem.LogSource = cursor.getString(cursor.getColumnIndex(SOURCE_COLUMN));
                        logItem.LogCaption = cursor.getString(cursor.getColumnIndex(CAPTION_COLUMN));
                        logItem.LogDescription = cursor.getString(cursor.getColumnIndex(DESCRIPTION_COLUMN));
                        logItem.LogDump = cursor.getString(cursor.getColumnIndex(DUMP_COLUMN));

                        // TODO: remove this when the server module has been changed to accept null values for these columns.
                        if(logItem.LogDescription == null) {
                            logItem.LogDescription = "";
                        }
                        if(logItem.LogCaption == null) {
                            logItem.LogCaption = "";
                        }
                        if(logItem.LogID == null) {
                            logItem.LogID = "";
                        }
                        if(logItem.LogSource == null) {
                            logItem.LogSource = "";
                        }

                        // TODO: Do normal add (without index) and then reverse the list to increase performance.
                        logs.add(0, logItem);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return logs;
    }

    void readAndLogSyncMetadata(UUID id) {

        String[] selectionArgs = new String[] {String.valueOf(id)};
        Cursor cursor = _storage.query(false, SYNC_METADATA_TABLE_NAME, null, SYNC_ENTITY_ID_COLUMN + " = ?", selectionArgs, null, null, null, null);

        if(cursor != null) {
            try {
                boolean resultEmpty = true;
                if (cursor.moveToFirst()) {
                    do {
                        resultEmpty = false;

                        String entityType = cursor.getString(cursor.getColumnIndex("EntityType"));
                        String entityVersion = cursor.getString(cursor.getColumnIndex("EntityVersion"));
                        String entityStatus = cursor.getString(cursor.getColumnIndex("Status"));
//                        String entityStatus = cursor.getString(cursor.getColumnIndex("EntityStatus"));
                        String sessionId = cursor.getString(cursor.getColumnIndex("SessionId"));
                        String changeId = cursor.getString(cursor.getColumnIndex("ChangeId"));

                        String body = "Found sync metadata for id: %s, entity type: %s, entity version: %s, entity status: %s, session id: %s, change id: %s";
                        body = String.format(body, id, entityType, entityVersion, entityStatus, sessionId, changeId);
                        _log.i(_logName, _logId, "Sync metadata found", body);
                    } while (cursor.moveToNext());
                }

                if (resultEmpty) {
                    String body = "Sync metadata not found for id: %s";
                    body = String.format(body, id);
                    _log.w(_logName, _logId, "Sync metadata not found", body);
                }
            } finally {
                cursor.close();
            }
        }
    }

    void readAndLogSyncPartialEntity(UUID id) {

        String[] selectionArgs = new String[] {String.valueOf(id)};
        Cursor cursor = _storage.query(false, SYNC_PARTIAL_ENTITY_TABLE_NAME, null, SYNC_ENTITY_ID_COLUMN + " = ?", selectionArgs, null, null, null, null);

        if(cursor != null) {
            try {
                boolean resultEmpty = true;
                if (cursor.moveToFirst()) {
                    do {
                        resultEmpty = false;

                        String sessionId = cursor.getString(cursor.getColumnIndex("SessionId"));
                        String packetNumber = cursor.getString(cursor.getColumnIndex("PacketNumber"));
                        String partialEntity = cursor.getString(cursor.getColumnIndex("PartialEntity"));
                        String insertionDate = cursor.getString(cursor.getColumnIndex("InsertionDate"));

                        String body = "Found sync partial entity for id: %s, session id: %s, packet number: %s, partial entity: %s, insertion date: %s";
                        body = String.format(body, id, sessionId, packetNumber, partialEntity, insertionDate);
                        _log.i(_logName, _logId, "Sync partial entity found", body);
                    } while (cursor.moveToNext());
                }

                if (resultEmpty) {
                    String body = "Sync partial entity not found for id: %s";
                    body = String.format(body, id);
                    _log.w(_logName, _logId, "Sync partial entity not found", body);
                }
            } finally {
                cursor.close();
            }
        }
    }

    void readAndLogSyncPropertyMetadata(UUID id) {

        String[] selectionArgs = new String[] {String.valueOf(id)};
        Cursor cursor = _storage.query(false, SYNC_PROPERTY_METADATA_TABLE_NAME, null, SYNC_ENTITY_ID_COLUMN + " = ?", selectionArgs, null, null, null, null);

        if(cursor != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("Found sync property metadata for id: %s:", id));
                boolean resultEmpty = true;
                if (cursor.moveToFirst()) {
                    do {
                        resultEmpty = false;

                        String propertyName = cursor.getString(cursor.getColumnIndex("PropertyName"));
                        String isChanged = cursor.getString(cursor.getColumnIndex("IsChanged"));

                        sb.append(System.getProperty("line.separator"));
                        sb.append(String.format("property name: %s, is changed: %s", propertyName, isChanged));
                    } while (cursor.moveToNext());
                }

                if (resultEmpty) {
                    String body = "Sync property metadata not found for id: %s";
                    body = String.format(body, id);
                    _log.w(_logName, _logId, "Sync property metadata not found", body);
                } else {
                    _log.i(_logName, _logId, "Sync property metadata found", sb.toString());
                }
            } finally {
                cursor.close();
            }
        }
    }
}
