﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PreCom;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Modules;
using SyncMonitor;
using LogItem = PreCom.Core.Log.LogItem;

namespace SyncMonitorServer
{
    public class SyncMonitorServerModule : ModuleBase
    {
        #region Fields

        private string _syncMonitorTableName;
        private string _logTableTypeName;
        private StorageBase _storage;
        private SettingsBase _settings;
        private ILog _log;

        /// <summary>
        /// Indicates if the module is initialized or not.
        /// </summary>
        private bool _isInitialized;

        #endregion

        #region ModuleBase Members

        /// <summary>
        /// Gets a value indicating whether this instance is initialized.
        /// </summary>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                return "SyncMonitorServer";
            }
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>true if initialized successfully, otherwise false.</returns>
        public override bool Initialize()
        {
            var com = Application.Instance.Communication;
            _storage = Application.Instance.Storage;
            _settings = Application.Instance.Settings;
            _log = Application.Instance.CoreComponents.Get<ILog>();

            ReadSettings();
            var db = new Db(_storage, _syncMonitorTableName, _logTableTypeName);
            db.CreateDbTable();
            db.CreateLogTableType();
            com.Register<EntityList<MonitorLogItem>>(OnSyncMonitorReceive);
            _isInitialized = true;

            return _isInitialized;
        }

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">true to cancel the dispose of the module, false if disposing is ok.</param>
        /// <returns>true if dispose was successful, otherwise false.</returns>
        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;
            return true;
        }

        #endregion

        private void ReadSettings()
        {
            _syncMonitorTableName = _settings.Read(this, "LogTableName", "PMC_Sync_Monitor_Log", true);
            _logTableTypeName = _settings.Read(this, "LogTableTypeName", "PmcSyncMonitorLogTableType", true);
        }

        private void OnSyncMonitorReceive(EntityList<MonitorLogItem> logs, RequestInfo requestinfo)
        {
            var logTvp = CreateDataTable();
            
            string clientId = null;
            int credId = -1;

            try
            {
                clientId = requestinfo.Client.ClientID;
                credId = requestinfo.Client.CredentialId;
                var serverLogTime = DateTime.UtcNow;

                var sb = new StringBuilder();
                sb.AppendLine("INSERT INTO");
                sb.AppendLine(_syncMonitorTableName);
                sb.AppendLine("(ClientId, CredentialId, ClientLogId, ClientLogTime, LogLevel, LogCustomLevel, LogId, LogSource, LogCaption, LogDescription, LogDump, ServerLogTimeUtc)");
                sb.AppendLine("SELECT ClientId, CredentialId, ClientLogId, ClientLogTime, LogLevel, LogCustomLevel, LogId, LogSource, LogCaption, LogDescription, LogDump, ServerLogTimeUtc FROM @LogTvp");

                using (var command = _storage.CreateCommand(sb.ToString()))
                {
                    var param = new SqlParameter
                    {
                        ParameterName = "@LogTvp",
                        SqlDbType = SqlDbType.Structured,
                        TypeName = _logTableTypeName,
                        Value = logTvp,
                    };

                    foreach (var log in logs)
                    {
                        logTvp.Rows.Add(clientId, credId, log.ID, log.LogTime, log.LogLevel, log.LogCustomLevel, log.LogID,
                            log.LogSource, log.LogCaption, log.LogDescription, log.LogDump, serverLogTime);
                    }

                    command.Parameters.Add(param);

                    _storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                var body = "Failed to write client sync monitor logs to DB for credential id: {0}, client id: {1}. Logs will be discarded.";
                body = string.Format(body, credId, clientId);
                var logId = "SM_E_1";
                var logItem = new LogItem(this, LogLevel.Error, logId, ex.GetType().Name, body, ex.ToString());
                _log.Write(logItem);
            }
        }

        private DataTable CreateDataTable()
        {
            var logTvp = new DataTable();
            logTvp.Columns.Add("ClientId", typeof(string));
            logTvp.Columns.Add("CredentialId", typeof(int));
            logTvp.Columns.Add("ClientLogId", typeof(int));
            logTvp.Columns.Add("ClientLogTime", typeof(DateTime));
            logTvp.Columns.Add("LogLevel", typeof(int));
            logTvp.Columns.Add("LogCustomLevel", typeof(int));
            logTvp.Columns.Add("LogId", typeof(string));
            logTvp.Columns.Add("LogSource", typeof(string));
            logTvp.Columns.Add("LogCaption", typeof(string));
            logTvp.Columns.Add("LogDescription", typeof(string));
            logTvp.Columns.Add("LogDump", typeof(string));
            logTvp.Columns.Add("ServerLogTimeUtc", typeof(DateTime));

            return logTvp;
        }
    }
}
