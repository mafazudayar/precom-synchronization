﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PreCom.Core;

namespace SyncMonitorServer
{
    internal class Db
    {
        private readonly StorageBase _storage;
        private readonly string _syncMonitorTableName;
        private readonly string _logTableTypeName;

        public Db(StorageBase storage, string syncMonitorTableName, string logTableTypeName)
        {
            _storage = storage;
            _syncMonitorTableName = syncMonitorTableName;
            _logTableTypeName = logTableTypeName;
        }

        internal void CreateLogTableType()
        {
            var sb = new StringBuilder();
            sb.AppendLine("IF NOT EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = '" + _logTableTypeName + "')");
            sb.AppendLine("BEGIN");
            sb.AppendLine("CREATE TYPE " + _logTableTypeName + " AS TABLE");
            sb.AppendLine("( ClientId [nvarchar](256),");
            sb.AppendLine("CredentialId INT,");
            sb.AppendLine("ClientLogId INT,");
            sb.AppendLine("ClientLogTime [datetime2],");
            sb.AppendLine("LogLevel INT,");
            sb.AppendLine("LogCustomLevel INT,");
            sb.AppendLine("LogId [nvarchar](MAX),");
            sb.AppendLine("LogSource [nvarchar](MAX),");
            sb.AppendLine("LogCaption [nvarchar](MAX),");
            sb.AppendLine("LogDescription [nvarchar](MAX),");
            sb.AppendLine("LogDump [nvarchar](MAX),");
            sb.AppendLine("ServerLogTimeUtc [datetime2] );");
            sb.AppendLine("END");

            using (var command = _storage.CreateCommand(sb.ToString()))
            {
                _storage.ExecuteNonCommand(command);
            }
        }

        internal void CreateDbTable()
        {
            if (!_storage.ObjectExists(_syncMonitorTableName, ObjectType.Table))
            {
                // TODO: change so LogId, LogSource, LogCaption and LogDescription columns can contain null values since these
                var sb = new StringBuilder();
                sb.AppendLine("CREATE TABLE [" + _syncMonitorTableName + "]");
                sb.AppendLine("([Id] [int] IDENTITY(1,1) NOT NULL,");
                sb.AppendLine("[ClientId] [nvarchar](256) NOT NULL,");
                sb.AppendLine("[CredentialId] [int] NOT NULL,");
                sb.AppendLine("[ClientLogId] [int] NOT NULL,");
                sb.AppendLine("[ClientLogTime] [datetime] NOT NULL,");
                sb.AppendLine("[LogLevel] [int] NOT NULL,");
                sb.AppendLine("[LogCustomLevel] [int] NOT NULL,");
                sb.AppendLine("[LogId] [nvarchar](max) NOT NULL,");
                sb.AppendLine("[LogSource] [nvarchar](max) NOT NULL,");
                sb.AppendLine("[LogCaption] [nvarchar](max) NOT NULL,");
                sb.AppendLine("[LogDescription] [nvarchar](max) NOT NULL,");
                sb.AppendLine("[LogDump] [nvarchar](max) NULL,");
                sb.AppendLine("[ServerLogTimeUtc] [datetime2](7) NOT NULL,");
                sb.AppendLine("CONSTRAINT [PK_" + _syncMonitorTableName + "] PRIMARY KEY CLUSTERED");
                sb.AppendLine("([Id] ASC))");

                using (var command = _storage.CreateCommand(sb.ToString()))
                {
                    _storage.ExecuteNonCommand(command);
                }
            }
        }
    }
}
