﻿using System;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using LogItem = PreCom.Core.Log.LogItem;

// ReSharper disable once CheckNamespace
namespace PreCom.Synchronization.Utils
{
    public class LogUtil
    {
        private readonly PreComBase _parentModule;
        private readonly ILog _log;

        public LogUtil(PreComBase parentModule, ILog log)
        {
            _parentModule = parentModule;
            _log = log;
        }

        public void Error(string header, string body, string dump = null)
        {
            Log(LogLevel.Error, header, body, dump);
        }

        public void Error(string header, string body, Exception exception)
        {
            Log(LogLevel.Error, header, body, exception != null ? exception.ToString() : null);
        }

        public void Warn(string header, string body, string dump = null)
        {
            Log(LogLevel.Warning, header, body, dump);
        }

        public void Info(string header, string body, string dump = null)
        {
            Log(LogLevel.Information, header, body, dump);
        }

        public void Debug(string header, string body, string dump = null)
        {
            Log(LogLevel.Debug, header, body, dump);
        }

        private void Log(LogLevel level, string header, string body, string dump)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var item = new LogItem(_parentModule, level, "", header, body, dump, 0);
                _log.Write(item);

                scope.Complete();
            }
        }
    }
}
