﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using PreCom;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Login;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;
using PreCom.Synchronization.Utils;
using SyncTestModule;

namespace SyncServerTestModule
{
    public class SyncTestModule : ModuleBase
    {
        #region Private fields

        /// <summary>
        /// If the module is initialized or not.
        /// </summary>
        private bool _isInitialized;

        private MarketOrderDao _marketOrderDao;
        private LimitOrderDao _limitOrderDao;
        private SynchronizedStorageBase<MarketOrder> _marketOrderManager;
        private SynchronizedStorageBase<LimitOrder> _limitOrderManager;

        private Thread _updateThread;
        private SynchronizationServerBase _syncModule;
        private LogUtil _log;

        private byte[] _oneMbData;

        private bool _logAutomaticPushArgs = true;

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                return "SyncTestModule";
            }
        }

        #endregion Public properties

        [Inject]
        public SyncTestModule(ILog log, SynchronizationServerBase synchronization)
        {
            _log = new LogUtil(this, Application.Instance.CoreComponents.Get<ILog>());
            _limitOrderDao = new LimitOrderDao(_log);
            _limitOrderManager = new LimitOrderManager(_limitOrderDao, log, synchronization);
            // _limitOrderManager = new LimitOrderManager(_limitOrderDao);
        }

        #region Public methods

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">Out parameter. If returned as true dispose was cancelled by the application. If returned as false dispose was accepted by the application and is shutting down.</param>
        /// <returns>true if dispose was ok, false otherwise</returns>
        public override bool Dispose(out bool cancel)
        {
            // Set to true for aborting dispose. This will reinitialize modules that already has been disposed.
            cancel = false;
            _isInitialized = false;

            if (_updateThread != null)
            {
                if (!_updateThread.Join(100))
                {
                    _updateThread.Abort();
                }
                _updateThread = null;
            }

            return true;
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>
        /// true if initialition was ok, false otherwise
        /// </returns>
        public override bool Initialize()
        {
            _isInitialized = true;
            Application.Instance.Communication.Register<OrderInfo>(ReceiveOrders);
            _marketOrderDao = new MarketOrderDao(Application.Instance.Storage, _log);
            _marketOrderDao.CreateOrderTable();

            _limitOrderDao.CreateOrderTable();

            try
            {
                _oneMbData = File.ReadAllBytes(@"C:\Program Files\PocketMobile\PreCom Server Framework\1MB.jpg");
            }
            catch (Exception)
            {
            }

            _marketOrderManager = new MarketOrderManager(_marketOrderDao);

            //_limitOrderManager.EntityChanged += LimitOrderManagerOnEntityChanged;

            _syncModule = Application.Instance.Modules.Get<SynchronizationServerBase>();

            _syncModule.RegisterStorage<MarketOrder>(_marketOrderManager);
            _syncModule.RegisterStorage<LimitOrder>(_limitOrderManager);

           // _syncModule.RegisterLoginFilter<LimitOrder>(LoginFilterDelegate);
            //_syncModule.RegisterLoginFilter<MarketOrder>(MarketLoginFilterDelegate);


            //_syncModule.RegisterConflictResolver<MarketOrder>(ResolveConflict);
            //_syncModule.RegisterConflictResolver<LimitOrder>(ResolveConflict);

            //_syncModule.RegisterLoginFilter<LimitOrder>(IsAssignedToCredential);
            _syncModule.RegisterLoginFilter<LimitOrder>(LoginFilterDelegate,LoginStatus.Completed);
            //_syncModule.RegisterLoginFilter<MarketOrder>(MarketLoginFilterDelegate, LoginStatus.InProgress);
             //_syncModule.RegisterAutomaticPush2<LimitOrder>(OnLimitOrderChange,
            // Operation.Created | Operation.Updated | Operation.Deleted);

            //_syncModule.RegisterSyncEventNotifier<LimitOrder>(SyncEventNotifier);

            return _isInitialized;
        }

        private void LimitOrderManagerOnEntityChanged(object sender, ChangeEventArgs changeEventArgs)
        {
            var body = string.Format("{0} {1} Id {2} Originator {3} CredentialId {4}",
                changeEventArgs.EntityType,
                changeEventArgs.Change,
                changeEventArgs.Id,
                changeEventArgs.OriginatorType,
                changeEventArgs.CredentialId);
            _log.Info("Change event", body);
            Console.WriteLine(body);
        }

        private void SyncEventNotifier(Guid entityId, SyncInfo syncInfo)
        {
            var body = string.Format("{0} {1}, SyncSession {2}", syncInfo.Type, syncInfo.Reason, syncInfo.SessionId);
            _log.Info("Sync event", body, syncInfo.Information);
            Console.WriteLine(body);
            Console.WriteLine(syncInfo.Information);
        }

        private ICollection<Guid> MarketLoginFilterDelegate(LoginFilterArgs loginFilterArgs)
        {
            Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Prasad" };
            EntityFilter ef = new EntityFilter();
            ef.FilterList.Add(f);
            return _marketOrderManager.ReadIds(ef);
        }

        private ICollection<Guid> LoginFilterDelegate(LoginFilterArgs loginFilterArgs)
        {
            if (loginFilterArgs.Client.CredentialId == 2)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Prasad" };
                EntityFilter ef = new EntityFilter();
                ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);
            }
            else if (loginFilterArgs.Client.CredentialId == 3)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };
                //Filter f = new Filter();
                EntityFilter ef = new EntityFilter();
                ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);

            }
            else
            {
                //Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Kaushalya" };
                //Filter f = new Filter();
                EntityFilter ef = new EntityFilter();
                //ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);

            }
        }

        public ICollection<Guid> IsAssignedToCredential(LoginFilterArgs filter)
        {
            if (filter.Client.CredentialId == 2)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Prasad" };
                EntityFilter ef = new EntityFilter();
                ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);
            }
            else if (filter.Client.CredentialId == 3)
            {
                Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Asanka" };
                //Filter f = new Filter();
                EntityFilter ef = new EntityFilter();
                ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);

            }
            else
            {
                //Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = "Kaushalya" };
                //Filter f = new Filter();
                EntityFilter ef = new EntityFilter();
                //ef.FilterList.Add(f);

                return _limitOrderManager.ReadIds(ef);

            }
        }

        private AutomaticPushReturnValue OnLimitOrderChange(AutomaticPushArgs automaticPushArgs)
        {
            AutomaticPushReturnValue returnValue = new AutomaticPushReturnValue();

            if (_logAutomaticPushArgs)
            {
                _log.Info("SyncTest", "AutomaticPushArgs.Change", automaticPushArgs.Change.ToString());
                _log.Info("SyncTest", "AutomaticPushArgs.CredentialId", automaticPushArgs.CredentialId.ToString());
                _log.Info("SyncTest", "AutomaticPushArgs.Id", automaticPushArgs.Id.ToString());
                _log.Info("SyncTest", "AutomaticPushArgs.Originator", automaticPushArgs.OriginatorType);

                if ((automaticPushArgs.Change == Operation.Updated || automaticPushArgs.Change == Operation.Deleted) &&
                    automaticPushArgs.OriginatorInstanceId == Application.Instance.InstanceId)
                {
                    _log.Info("SyncTest", "AutomaticPushArgs.Old", automaticPushArgs.Old.ToString());
                }
            }

            if (automaticPushArgs.OriginatorInstanceId == Application.Instance.InstanceId)
            {
                // Handle the case when the change has been done on this server, we can use old and new objects

                // Add information that is needed on other servers when running the delegate
                if (automaticPushArgs.New != null)
                {
                    returnValue.ChangeInfo.Add("Creator", ((LimitOrder)automaticPushArgs.New).Creator);
                }
            }
            else
            {
                // Handle the case when the change has been done on another server
                if (automaticPushArgs.ChangeInfo.ContainsKey("Creator"))
                {
                    string creator = automaticPushArgs.ChangeInfo["Creator"];
                    Console.WriteLine("OnLimitOrderChange, creator = " + creator);
                }
            }

            // Test client push #51
            // Use _syncModule.RegisterAutomaticPush<LimitOrder>(OnLimitOrderChange, Operation.Updated | Operation.Created | Operation.Deleted);
            //if (automaticPushArgs.Change == Operation.Created)
            //{
            //    Thread t = new Thread(o =>
            //        {
            //            LimitOrder limitOrder = (LimitOrder)automaticPushArgs.New;
            //            limitOrder.IsAssigned = !limitOrder.IsAssigned;
            //            _limitOrderManager.Update(limitOrder, new UpdateArgs(GetType().Name, 2));

            //        });
            //    t.Start(automaticPushArgs);
            //}

            // Test Client pull #37
            // Use _syncModule.RegisterAutomaticPush<LimitOrder>(OnLimitOrderChange, Operation.Created);
            //if (automaticPushArgs.Change == Operation.Created && _updateThread == null)
            //{
            //    _updateThread = new Thread(o =>
            //    {
            //        Thread.Sleep(1000);
            //        AutomaticPushArgs args = (AutomaticPushArgs)o;

            //        int i = 1;
            //        while (IsInitialized)
            //        {
            //            i++;
            //            var orders = _limitOrderManager.Read(new Guid[] { args.Id });
            //            if (orders.Count == 0)
            //                break;
            //            LimitOrder limitOrder = (LimitOrder)orders.First();
            //            limitOrder.Description = i.ToString();
            //            _limitOrderManager.Update(limitOrder,
            //                                      new UpdateArgs(GetType().Name, 2));
            //            Thread.Sleep(2);
            //        }
            //    });
            //    _updateThread.Start(automaticPushArgs);
            //}

            var clients = Application.Instance.Communication.GetClients();
            foreach (var client in clients)
            {
                if (client.CredentialId != -1 && !returnValue.CredentialIds.Contains(client.CredentialId))
                {
                    returnValue.CredentialIds.Add(client.CredentialId);
                }
            }
            //Collection<int> users = new Collection<int> { 1, 2, 3 };
            return returnValue;
        }

        private void ReceiveOrders(OrderInfo orderInfo, RequestInfo requestInfo)
        {
            Task.Run(() =>
                {
                    if (orderInfo != null && orderInfo.Action != null)
                    {
                        if (orderInfo.Action == "Delete")
                        {
                            // This simply clear all tables related to sync module 
                            _marketOrderDao.DeleteOrderReords();
                            _marketOrderDao.DeleteSyncPropertyReords();
                            _marketOrderDao.DeleteSyncReords();

                            _limitOrderDao.DeleteOrderReords();
                        }
                        else if (orderInfo.Action == "ServerPush")
                        {
                            //_syncModule.Push<MarketOrder>(orderInfo.OrderFilter, orderInfo.Credentials);
                            List<int> credentials = new List<int>();
                            foreach (int credential in orderInfo.Credentials)
                            {
                                if (Application.Instance.Communication.IsLoggedin(credential))
                                {
                                    credentials.Add(credential);
                                }
                            }

                            _syncModule.Push<LimitOrder>(orderInfo.OrderFilter, credentials);
                        }
                        else if (orderInfo.Action == "ServerPushArgumentTest")
                        {
                            this.ServerPushAPITest();
                        }
                        else if (orderInfo.Action == "Insert")
                        {
                            int numberOfData = orderInfo.NumberOfOrders;
                            String type = orderInfo.OrderType;

                            if (orderInfo.OrderType == "MarketOrder")
                            {
                                for (int i = 0; i < numberOfData; i++)
                                {
                                    MarketOrder order = MarketOrderManager.CreateOrder(GetCreator(i), GetDescription(i));
                                    _marketOrderManager.Create(order, new CreateArgs(GetType().Name));
                                }
                            }

                            if (orderInfo.OrderType == "LimitOrder")
                            {
                                for (int i = 0; i < numberOfData; i++)
                                {
                                    LimitOrder order = LimitOrderManager.CreateOrder(GetCreator(i), GetDescription(i),
                                                                                     orderInfo.IsLarge ? _oneMbData : null);
                                    _limitOrderManager.Create(order, new CreateArgs(GetType().Name));
                                }
                            }
                        }
                        else if (orderInfo.Action == "Update")
                        {
                            if (orderInfo.OrderType == "MarketOrder")
                            {
                                EntityFilter filter = null;
                                List<ISynchronizable> entities =
                                    new List<ISynchronizable>(_marketOrderManager.Read(filter));

                                if (entities.Count > 0)
                                {
                                    for (int i = 0; i < entities.Count && (orderInfo.NumberOfOrders == 0 || i < orderInfo.NumberOfOrders); i++)
                                    {
                                        using (var scope = _syncModule.CreateTransaction(IsolationLevel.ReadCommitted))
                                        {
                                            MarketOrder order = (MarketOrder)entities[i];
                                            order.Description += ".";
                                            order.Date = order.Date.AddDays(1);
                                            _marketOrderManager.Update(order, new UpdateArgs(GetType().Name));

                                            scope.Complete();
                                        }
                                    }
                                }
                            }

                            if (orderInfo.OrderType == "LimitOrder")
                            {
                                EntityFilter filter = null;
                                List<ISynchronizable> entitiess =
                                    new List<ISynchronizable>(_limitOrderManager.Read(filter));

                                List<ISynchronizable> entities = entitiess.OrderByDescending(x => x.Guid).ToList();

                                if (entities.Count > 0)
                                {
                                    for (int i = 0; i < entities.Count && (orderInfo.NumberOfOrders == 0 || i < orderInfo.NumberOfOrders); i++)
                                    {
                                        using (var scope = _syncModule.CreateTransaction(IsolationLevel.ReadCommitted))
                                        {
                                            LimitOrder order = (LimitOrder)entities[i];
                                            //order.Description += ".";
                                            order.Report.Title += "...";
                                            _limitOrderManager.Update(order, new UpdateArgs(GetType().Name));

                                            scope.Complete();
                                            _log.Info("SyncTest", "Update LimitOrder, transaction completed");
                                        }
                                    }
                                }
                            }
                        }
                        else if (orderInfo.Action == "DeleteLast")
                        {
                            if (orderInfo.OrderType == "MarketOrder")
                            {
                                EntityFilter filter = null;
                                var entities = new List<ISynchronizable>(_marketOrderManager.Read(filter));

                                if (entities.Count > 0)
                                {
                                    for (int i = 0; i < entities.Count && (orderInfo.NumberOfOrders == 0 || i < orderInfo.NumberOfOrders); i++)
                                    {
                                        using (var scope = _syncModule.CreateTransaction(IsolationLevel.ReadCommitted))
                                        {
                                            MarketOrder order = (MarketOrder)entities[i];
                                            _marketOrderManager.Delete((order.Guid), new DeleteArgs(GetType().Name));

                                            scope.Complete();
                                        }
                                    }
                                }
                            }

                            if (orderInfo.OrderType == "LimitOrder")
                            {
                                EntityFilter filter = null;
                                var entities = new List<ISynchronizable>(_limitOrderManager.Read(filter));
                                if (entities.Count > 0)
                                {
                                    for (int i = 0; i < entities.Count && (orderInfo.NumberOfOrders == 0 || i < orderInfo.NumberOfOrders); i++)
                                    {
                                        using (var scope = _syncModule.CreateTransaction(IsolationLevel.ReadCommitted))
                                        {
                                            LimitOrder order = (LimitOrder)entities[i];
                                            _limitOrderManager.Delete((order.Guid), new DeleteArgs(GetType().Name));

                                            scope.Complete();
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
        }

        private void ServerPushAPITest()
        {
            List<int> credentials = new List<int>();

            try
            {
                _syncModule.Push<LimitOrder>(null, null);

            }
            catch (Exception e)
            {
                _log.Error("SERVER PUSH API", "", e);
            }

            try
            {
                _syncModule.Push<LimitOrder>(new EntityFilter(), credentials);
            }
            catch (Exception e)
            {
                _log.Error("SERVER PUSH API", "", e);

            }

            try
            {
                credentials.Add(5);
                _syncModule.Push<LimitOrder>(null, credentials);
            }
            catch (Exception e)
            {
                _log.Error("SERVER PUSH API", "", e);
            }

            try
            {
                credentials.Add(5);
                _syncModule.Push<UnregisteredOrder>(new EntityFilter(), credentials);
            }
            catch (Exception e)
            {
                _log.Error("SERVER PUSH API", "", e);

            }
        }

        private String GetCreator(int i)
        {

            if (i % 3 == 1)
            {
                return "Kaushalya";
            }
            else if (i % 3 == 2)
            {
                return "Asanka";
            }
            else if (i % 3 == 0)
            {
                return "Prasad";
            }
            else
            {
                return "Amalka";
            }
        }

        private String GetDescription(int i)
        {

            if (i % 3 == 1)
            {
                return "Computer Parts";
            }
            else if (i % 3 == 2)
            {
                return "Food items";
            }
            else if (i % 3 == 0)
            {
                return "Vehicle Parts";
            }
            else
            {
                return "Books";
            }
        }

        private ISynchronizable ResolveConflict(ConflictInfo<MarketOrder> info)
        {
            MarketOrder hybrid = new MarketOrder();
            hybrid.Guid = info.OldValue.Guid;
            hybrid.Id = info.OldValue.Guid;
            hybrid.Date = info.OldValue.Date;
            hybrid.Creator = "Kamal";
            hybrid.Description = "My name is Kamal";
            hybrid.IsAssigned = false;
            return hybrid;
        }

        private ISynchronizable ResolveConflict(ConflictInfo<LimitOrder> info)
        {
            LimitOrder hybrid = new LimitOrder();
            hybrid.Guid = info.OldValue.Guid;
            hybrid.Id = info.OldValue.Guid;
            hybrid.Date = info.OldValue.Date;
            hybrid.Creator = info.NewValue.Creator;
            hybrid.Description = "Hybrid";// info.OldValue.Description;
            hybrid.Report = info.OldValue.Report; //new Report {Content = info.OldValue.Report.Content, Title = null};//
            hybrid.IsAssigned = false;
            return hybrid;
        }

        /// <summary>
        /// Check if instance is initialized or not.
        /// </summary>
        /// <returns> true if initialized, false otherwise.</returns>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        #endregion Public methods
    }
}
