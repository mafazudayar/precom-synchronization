﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using PreCom.Core;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;
using PreCom.Synchronization.Utils;

namespace SyncServerTestModule
{
    public class MarketOrderDao : IOrderDao<MarketOrder>
    {
        private readonly StorageBase _storage;
        private readonly LogUtil _log;

        public MarketOrderDao(StorageBase storage, LogUtil log)
        {
            _storage = storage;
            _log = log;
        }

        private bool IndexExists(string table, string indexName)
        {
            var cmdText = "SELECT Name FROM sys.indexes WHERE object_id = OBJECT_ID(@table) AND Name = @indexName";

            using (var cmd = _storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(_storage.CreateParameter("@table", table));
                cmd.Parameters.Add(_storage.CreateParameter("@indexName", indexName));

                var result = _storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        public bool CreateOrderTable()
        {
            bool isCreated = false;
            IDbCommand command = null;

            try
            {
                if (!_storage.ObjectExists("SyncTest_MarketOrder", ObjectType.Table))
                {
                    command = _storage.CreateCommand();

                    StringBuilder queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE [SyncTest_MarketOrder]( ");
                    queryString.Append("[AutoId] [bigint] NOT NULL identity (1,1) PRIMARY KEY, ");
                    queryString.Append("[OrderId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[Creator] [nvarchar](50) NOT NULL, ");
                    queryString.Append("[Description] [nvarchar](MAX) NULL, ");
                    queryString.Append("[Date] [datetime] NOT NULL, ");
                    queryString.Append("[IsAssigned] [bit] NOT NULL)");

                    command.CommandText = queryString.ToString();
                    _storage.ExecuteNonCommand(command);

                    // Table creation check
                    if (!_storage.ObjectExists("SyncTest_MarketOrder", ObjectType.Table))
                    {
                        _log.Error("CreateOrderTable()", "Failed to create Table: SyncTest_MarketOrder");
                    }
                    else
                    {
                        isCreated = true;
                    }
                }
                else
                {
                    isCreated = true;
                }

                if (!IndexExists("SyncTest_MarketOrder", "IX_SyncTest_MarketOrder_OrderId"))
                {
                    var cmdText = "CREATE NONCLUSTERED INDEX [IX_SyncTest_MarketOrder_OrderId] ON [SyncTest_MarketOrder] ([OrderId] ASC)";
                    using (var cmd = _storage.CreateCommand(cmdText))
                    {
                        _storage.ExecuteNonCommand(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("CreateTable()", "Failed to create Table: SyncTest_MarketOrder", ex);
                isCreated = false;
            }
            finally
            {
                DisposeCommand(command);
            }

            return isCreated;
        }

        public bool Insert(MarketOrder order)
        {
            IDbCommand insertCommand = null;
            bool inserted = false;

            try
            {
                if (_storage.IsInitialized)
                {
                    const string commandString = "INSERT INTO  [SyncTest_MarketOrder] "
                                                 + " ([OrderId],[Creator],[Description],[Date],[IsAssigned]) "
                                                 + " VALUES (@OrderID, @Creator, @Description, @Date,@IsAssigned ); ";

                    insertCommand = _storage.CreateCommand(commandString);
                    insertCommand.Parameters.Add(
                            _storage.CreateParameter("@OrderId", order.Id));
                    insertCommand.Parameters.Add(
                            _storage.CreateParameter("@Creator", order.Creator));
                    insertCommand.Parameters.Add(
                            _storage.CreateParameter("@Description", order.Description));
                    insertCommand.Parameters.Add(
                            _storage.CreateParameter("@Date", order.Date));
                    insertCommand.Parameters.Add(
                            _storage.CreateParameter("@IsAssigned", order.IsAssigned));

                    int rows = _storage.ExecuteNonCommand(insertCommand);

                    if (rows > 0)
                    {
                        inserted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Insert(Order order)", "Failed to Insert: SyncTest_MarketOrder", ex);
                throw;
            }
            finally
            {
                DisposeCommand(insertCommand);
            }

            return inserted;
        }

        public bool Update(MarketOrder order)
        {
            IDbCommand command = null;
            bool updated = false;

            try
            {
                if (_storage.IsInitialized)
                {
                    const string commandString = "UPDATE [SyncTest_MarketOrder] "
                                                 + " SET [Creator]= @Creator,[Description]=@Description,[Date]=@Date,[IsAssigned]=@IsAssigned "
                                                 + " WHERE [OrderId]= @OrderId; ";

                    command = _storage.CreateCommand(commandString);
                    command.Parameters.Add(
                            _storage.CreateParameter("@OrderId", order.Id));
                    command.Parameters.Add(
                            _storage.CreateParameter("@Creator", order.Creator));
                    command.Parameters.Add(
                            _storage.CreateParameter("@Description", order.Description));
                    command.Parameters.Add(
                                _storage.CreateParameter("@Date", order.Date));
                    command.Parameters.Add(
                                _storage.CreateParameter("@IsAssigned", order.IsAssigned));


                    int rows = _storage.ExecuteNonCommand(command);

                    if (rows > 0)
                    {
                        updated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Update(Order order)", "Failed to Update record: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeCommand(command);
            }

            return updated;
        }

        public bool Delete(Guid orderId)
        {
            return DeleteOrder(orderId);
        }

        public bool DeleteOrder(Guid orderId)
        {
            IDbCommand command = null;
            bool deleted = false;

            try
            {
                if (_storage.IsInitialized)
                {
                    const string commandString = "DELETE FROM [SyncTest_MarketOrder] "
                                                 + " WHERE [OrderId]= @OrderId; ";

                    command = _storage.CreateCommand(commandString);
                    command.Parameters.Add(
                            _storage.CreateParameter("@OrderId", orderId));


                    int rows = _storage.ExecuteNonCommand(command);

                    if (rows > 0)
                    {
                        deleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Delete(Order order)", "Failed to delete order: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeCommand(command);
            }

            return deleted;
        }

        public bool DeleteAll()
        {
            DeleteOrderReords();
            return true;
        }

        public MarketOrder GetOrder(Guid id)
        {
            IDbCommand command = null;
            IDataReader reader = null;
            MarketOrder order = null;

            try
            {
                if (_storage.IsInitialized)
                {
                    const string commandString = " SELECT * from [SyncTest_MarketOrder] "
                                                 + " WHERE [OrderId]= @OrderId; ";

                    command = _storage.CreateCommand(commandString);
                    command.Parameters.Add(
                            _storage.CreateParameter("@OrderId", id));


                    reader = _storage.ExecuteQueryCommand(command);

                    if (reader.Read())
                    {
                        order = new MarketOrder();
                        order.Id = Guid.Parse(Convert.ToString(reader["OrderId"]));
                        order.Creator = Convert.ToString(reader["Creator"]);
                        order.Description = Convert.ToString(reader["Description"]);
                        order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                        order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrder(string id)", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeReader(reader);
                DisposeCommand(command);
            }

            return order;
        }

        public ICollection<MarketOrder> GetOrders()
        {
            IDbCommand command = null;
            IDataReader reader = null;
            List<MarketOrder> orders = new List<MarketOrder>();

            try
            {
                if (_storage.IsInitialized)
                {
                    const string commandString = " SELECT * from [SyncTest_MarketOrder]; ";

                    command = _storage.CreateCommand(commandString);
                    reader = _storage.ExecuteQueryCommand(command);

                    while (reader.Read())
                    {
                        MarketOrder order = new MarketOrder();
                        order.Id = Guid.Parse(Convert.ToString(reader["OrderId"]));
                        order.Creator = Convert.ToString(reader["Creator"]);
                        order.Description = Convert.ToString(reader["Description"]);
                        order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                        order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);

                        orders.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders()", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeReader(reader);
                DisposeCommand(command);
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(ICollection<Guid> entities)
        {
            if (entities != null && entities.Count > 0)
            {
                if (entities.Count == 1)
                {
                    return new Collection<ISynchronizable>()
                    {
                        GetOrder(entities.First())
                    };
                }

                var sb = new StringBuilder();

                foreach (Guid syncEntityInfo in entities)
                {
                    sb.Append("'");
                    sb.Append(syncEntityInfo);
                    sb.Append("',");
                }

                sb.Remove(sb.Length - 1, 1);

                return GetOrders(sb.ToString());
            }

            return null;
        }

        private Collection<ISynchronizable> GetOrders(string commaSeparetedIds)
        {
            IDbCommand command = null;
            IDataReader reader = null;
            Collection<ISynchronizable> orders = new Collection<ISynchronizable>();
            try
            {
                if (_storage.IsInitialized)
                {
                    string commandString = string.Concat("SELECT * from [SyncTest_MarketOrder] WHERE [OrderId] IN (", commaSeparetedIds, "); ");

                    command = _storage.CreateCommand(commandString);
                    reader = _storage.ExecuteQueryCommand(command);

                    while (reader.Read())
                    {
                        var order = new MarketOrder();
                        order.Id = Guid.Parse(Convert.ToString(reader["OrderId"]));
                        order.Creator = Convert.ToString(reader["Creator"]);
                        order.Description = DBNull.Value.Equals(reader["Description"]) ? null : Convert.ToString(reader["Description"]);
                        order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                        order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                        orders.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders(string commaSeparetedIds)", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeReader(reader);
                DisposeCommand(command);
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(EntityFilter filter)
        {
            IDbCommand command = null;
            IDataReader reader = null;
            Collection<ISynchronizable> orders = new Collection<ISynchronizable>();
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * from [SyncTest_MarketOrder] ");

                    if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
                    {
                        sb.Append("WHERE ");
                        int a = 0;
                        foreach (Filter f in filter.FilterList)
                        {
                            a = a + 1;
                            sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                            if (a < filter.FilterList.Count)
                            {
                                sb.Append(" AND ");
                            }
                        }
                    }

                    sb.Append(";");

                    command = _storage.CreateCommand(sb.ToString());
                    reader = _storage.ExecuteQueryCommand(command);

                    while (reader.Read())
                    {
                        MarketOrder order = new MarketOrder();
                        order.Id = Guid.Parse(Convert.ToString(reader["OrderId"]));
                        order.Creator = Convert.ToString(reader["Creator"]);
                        order.Description = Convert.ToString(reader["Description"]);
                        order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                        order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                        orders.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders(EntityFilter filter)", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeReader(reader);
                DisposeCommand(command);
            }

            return orders;
        }

        public ICollection<Guid> GetOrderIds(EntityFilter filter)
        {
            IDbCommand command = null;
            IDataReader reader = null;
            Collection<Guid> orders = new Collection<Guid>();
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT OrderId from [SyncTest_MarketOrder] ");

                    if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
                    {
                        sb.Append("WHERE ");
                        int a = 0;
                        foreach (Filter f in filter.FilterList)
                        {
                            a = a + 1;
                            sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                            if (a < filter.FilterList.Count)
                            {
                                sb.Append(" AND ");
                            }
                        }
                    }

                    sb.Append(";");

                    command = _storage.CreateCommand(sb.ToString());
                    reader = _storage.ExecuteQueryCommand(command);

                    while (reader.Read())
                    {
                        Guid oid = Guid.Parse(reader["OrderId"].ToString());
                        orders.Add(oid);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrderIds(EntityFilter filter)", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }
            finally
            {
                DisposeReader(reader);
                DisposeCommand(command);
            }

            return orders;
        }

        public void DeleteOrderReords()
        {
            IDbCommand command = null;
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM SyncTest_MarketOrder");
                    command = _storage.CreateCommand(sb.ToString());
                    _storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteOrderReords()", "Failed to delete records: SyncTest_MarketOrder", ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }

        }

        public void DeleteSyncReords()
        {
            IDbCommand command = null;
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM PMC_Synchronization_Metadata");
                    command = _storage.CreateCommand(sb.ToString());
                    _storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteSyncReords()", "Failed to delete records: SyncTest_MarketOrder", ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }

        public void DeleteSyncReord(Guid entityId)
        {
            try
            {
                if (_storage.IsInitialized)
                {
                    var cmdText = "DELETE FROM PMC_Synchronization_Metadata WHERE EntityId = @entityId";
                    using (var command = _storage.CreateCommand(cmdText))
                    {
                        command.Parameters.Add(_storage.CreateParameter("@entityId", entityId));
                        _storage.ExecuteNonCommand(command);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteSyncReord(Guid)", "Failed to delete records: SyncTest_MarketOrder", ex);
            }
        }

        public void DeleteSyncPropertyReords()
        {
            IDbCommand command = null;
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM PMC_Synchronization_Property_MetaData");
                    command = _storage.CreateCommand(sb.ToString());
                    _storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteSyncPropertyReords()", "Failed to delete records: SyncTest_MarketOrder", ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }

        public void DeleteSyncPropertyReord(Guid entityId)
        {
            try
            {
                if (_storage.IsInitialized)
                {
                    var cmdText = "DELETE FROM PMC_Synchronization_Property_MetaData WHERE EntityId = @entityId";
                    using (var command = _storage.CreateCommand(cmdText))
                    {
                        command.Parameters.Add(_storage.CreateParameter("@entityId", entityId));
                        _storage.ExecuteNonCommand(command);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteSyncPropertyReord(Guid)", "Failed to delete records: SyncTest_MarketOrder", ex);
            }
        }

        public void DeleteSyncPartialRecords()
        {
            IDbCommand command = null;
            try
            {
                if (_storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM PMC_Synchronization_Partial_Entity");
                    command = _storage.CreateCommand(sb.ToString());
                    _storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteSyncPartialRecords()", "Failed to delete records: PMC_Synchronization_Partial_Entity", ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }

        public string GetValue(string propertyName, object value)
        {
            switch (propertyName)
            {
                case "Id":
                    return "'" + value + "'";
                case "Creator":
                    return "'" + value + "'";
                case "Description":
                    return "'" + value + "'";
                case "IsAssigned":
                    return value.ToString();
                case "Date":
                    return value.ToString();
                default:
                    return value.ToString();
            }
        }

        public string GetTableColumnName(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return "OrderId";
                case "Creator":
                    return "Creator";
                case "Description":
                    return "Description";
                case "IsAssigned":
                    return "IsAssigned";
                case "Date":
                    return "Date";
                default:
                    return "";
            }
        }

        public string GetOperator(FilterOperator filterOperator)
        {
            string op = "=";
            switch (filterOperator)
            {
                case FilterOperator.EqualTo:
                    op = "=";
                    break;
                case FilterOperator.NotEqualTo:
                    op = "!=";
                    break;
                case FilterOperator.GreaterThan:
                    op = ">";
                    break;
                case FilterOperator.LessThan:
                    op = "<";
                    break;
            }
            return op;
        }

        /// <summary>
        /// Disposes the IDbCommand.
        /// </summary>
        /// <param name="command">The IDbCommand object.</param>
        public static void DisposeCommand(IDbCommand command)
        {
            if (command != null)
            {
                command.Dispose();
            }
        }

        /// <summary>
        /// Disposes the IDataReader.
        /// </summary>
        /// <param name="reader">The IDataReader object.</param>
        public void DisposeReader(IDataReader reader)
        {
            if (reader != null)
            {
                reader.Close();
                reader.Dispose();
            }
        }
    }
}
