﻿using System;
using System.Collections.Generic;
using PreCom.Core.Communication;
using PreCom.Synchronization;

namespace SyncTestModule
{
    public class OrderInfo : EntityBase
    {
        public int NumberOfOrders { get; set; }
        public List<int> Credentials { get; set; }
        public EntityFilter OrderFilter { get; set; }
        public String OrderType;
        public bool IsLarge { get; set; }
        public string Action { get; set; }

        public OrderInfo()
        {
            Credentials = new List<int>();
            OrderFilter = new EntityFilter();
        }
    }
}

