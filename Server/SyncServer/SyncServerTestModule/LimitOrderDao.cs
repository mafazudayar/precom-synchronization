﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using PreCom;
using PreCom.Core;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;
using PreCom.Synchronization.Utils;

namespace SyncServerTestModule
{
    public class LimitOrderDao : IOrderDao<LimitOrder>
    {
        private readonly LogUtil _log;

        public LimitOrderDao(LogUtil log)
        {
            _log = log;
        }

        private static bool IndexExists(string table, string indexName)
        {
            var cmdText = "SELECT Name FROM sys.indexes WHERE object_id = OBJECT_ID(@table) AND Name = @indexName";

            using (var cmd = Application.Instance.Storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(Application.Instance.Storage.CreateParameter("@table", table));
                cmd.Parameters.Add(Application.Instance.Storage.CreateParameter("@indexName", indexName));

                var result = Application.Instance.Storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        public bool CreateOrderTable()
        {
            bool isCreated = false;
            using (IDbCommand command = Application.Instance.Storage.CreateCommand())
            {
                try
                {
                    if (!Application.Instance.Storage.ObjectExists("SyncTest_LimitOrder", ObjectType.Table))
                    {
                        StringBuilder queryString = new StringBuilder();
                        queryString.Append("CREATE TABLE [SyncTest_LimitOrder]( ");
                        queryString.Append("[AutoId] [bigint] NOT NULL identity (1,1) PRIMARY KEY, ");
                        queryString.Append("[OrderId] [uniqueidentifier] NOT NULL, ");
                        queryString.Append("[Creator] [nvarchar](50) NULL, ");
                        queryString.Append("[Description] [nvarchar] (max) NULL, ");
                        queryString.Append("[Date] [datetime] NULL, ");
                        queryString.Append("[Image] [image] NULL, ");
                        queryString.Append("[ReportTitle] [nvarchar](50) NULL, ");
                        queryString.Append("[ReportContent] [nvarchar](50) NULL, ");
                        queryString.Append("[ReportId] [int] NULL, ");
                        queryString.Append("[Editors] [nvarchar](50) NULL, ");
                        queryString.Append("[IsAssigned] [bit] NOT NULL)");

                        command.CommandText = queryString.ToString();
                        Application.Instance.Storage.ExecuteNonCommand(command);

                        // Table creation check
                        if (!Application.Instance.Storage.ObjectExists("SyncTest_LimitOrder", ObjectType.Table))
                        {
                            _log.Error("CreateOrderTable()", "Failed to create Table: SyncTest_LimitOrder");
                        }
                        else
                        {
                            isCreated = true;
                        }
                    }
                    else
                    {
                        isCreated = true;
                    }

                    if (!IndexExists("SyncTest_LimitOrder", "IX_SyncTest_LimitOrder_OrderId"))
                    {
                        var cmdText = "CREATE NONCLUSTERED INDEX [IX_SyncTest_LimitOrder_OrderId] ON [SyncTest_LimitOrder] ([OrderId] ASC)";
                        using (var cmd = Application.Instance.Storage.CreateCommand(cmdText))
                        {
                            Application.Instance.Storage.ExecuteNonCommand(cmd);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("CreateTable()", "Failed to create Table: SyncTest_LimitOrder", ex);
                    isCreated = false;
                }
                finally
                {
                    DisposeCommand(command);
                }
            }

            return isCreated;
        }

        public bool Insert(LimitOrder order)
        {
            bool inserted = false;
            const string commandString = "INSERT INTO  [SyncTest_LimitOrder] "
                        + " ([OrderId],[Creator],[Description],[Date],[Image],[ReportTitle],[ReportContent],[ReportId],[Editors],[IsAssigned]) "
                         + " VALUES (@OrderID, @Creator, @Description, @Date,@Image, @ReportTitle, @ReportContent,@ReportId, @Editors,@IsAssigned); ";

            if (Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand insertCommand = Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@OrderId", order.Id));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Creator", order.Creator));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Description", order.Description));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Date", order.Date));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@IsAssigned", order.IsAssigned));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Image", order.Image == null ? null : order.Image.Content));


                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportTitle", order.Report == null ? null : order.Report.Title));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportContent",
                                                                                order.Report == null ? null : order.Report.Content));
                        insertCommand.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportId", order.Report == null ? -1 : order.Report.Id));

                        string editors = null;
                        if (order.Report != null && order.Report.Editors != null)
                        {
                            foreach (Editor editor in order.Report.Editors)
                            {
                                editors = editor.EditorName + "-" + editors;
                            }

                            if (editors != null) editors = editors.TrimEnd('-');
                        }
                        insertCommand.Parameters.Add(
                                Application.Instance.Storage.CreateParameter("@Editors", editors));

                        int rows = Application.Instance.Storage.ExecuteNonCommand(insertCommand);

                        if (rows > 0)
                        {
                            inserted = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Insert(Order order)", "Failed to Insert: SyncTest_LimitOrder", ex);
                    }
                }
            }

            return inserted;
        }

        public bool Update(LimitOrder order)
        {
            bool updated = false;
            const string commandString = "UPDATE [SyncTest_LimitOrder] "
                                         + " SET [Creator]= @Creator,[Description]=@Description,[Date]=@Date,[IsAssigned]=@IsAssigned,[ReportTitle]=@ReportTitle,[ReportContent]=@ReportContent,[ReportId]=@ReportId,[Editors]=@Editors "
                                         + " WHERE [OrderId]= @OrderId; ";
            if (Application.Instance.Storage.IsInitialized)
            {
                using (IDbCommand command = Application.Instance.Storage.CreateCommand(commandString))
                {
                    try
                    {
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@OrderId", order.Id));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Creator", order.Creator));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Description", order.Description));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Date", order.Date));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@IsAssigned", order.IsAssigned));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Image", order.Image == null ? null : order.Image.Content));


                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportTitle", order.Report == null ? null : order.Report.Title));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportContent",
                                                                                order.Report == null ? null : order.Report.Content));
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@ReportId", order.Report == null ? -1 : order.Report.Id));

                        string editors = null;
                        if (order.Report != null && order.Report.Editors != null)
                        {
                            foreach (Editor editor in order.Report.Editors)
                            {
                                editors = editor.EditorName + "-" + editors;
                            }

                            if (editors != null) editors = editors.TrimEnd('-');
                        }
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@Editors", editors));


                        int rows = Application.Instance.Storage.ExecuteNonCommand(command);

                        if (rows > 0)
                        {
                            updated = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        _log.Error("Update(Order order)", "Failed to Update record: SyncTest_LimitOrder", ex);
                    }
                }
            }

            return updated;
        }

        public bool Delete(Guid orderId)
        {
            IDbCommand command = null;
            bool deleted = false;

            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    const string commandString = "DELETE FROM [SyncTest_LimitOrder] "
                                                 + " WHERE [OrderId]= @OrderId; ";

                    command = Application.Instance.Storage.CreateCommand(commandString);
                    command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@OrderId", orderId));

                    int rows = Application.Instance.Storage.ExecuteNonCommand(command);

                    if (rows > 0)
                    {
                        deleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Delete(Order order)", "Failed to delete order: SyncTest_LimitOrder", ex);
            }
            finally
            {
                DisposeCommand(command);
            }

            return deleted;
        }

        public bool DeleteAll()
        {
            DeleteOrderReords();
            return true;
        }

        public LimitOrder GetOrder(Guid id)
        {
            LimitOrder order = null;

            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {

                    const string commandString = " SELECT * from [SyncTest_LimitOrder] "
                                                 + " WITH (UPDLOCK, FORCESEEK) WHERE [OrderId]= @OrderId; ";


                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(commandString))
                    {
                        command.Parameters.Add(
                            Application.Instance.Storage.CreateParameter("@OrderId", id));

                        
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            if (reader.Read())
                            {
                                order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrder(Guid " + id + ")", "Failed to Read Table: SyncTest_LimitOrder", ex);
            }

            return order;
        }

        public ICollection<LimitOrder> GetOrders()
        {
            List<LimitOrder> orders = new List<LimitOrder>();

            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    const string commandString = " SELECT * from [SyncTest_LimitOrder]; ";
                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(commandString))
                    {
                        
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders()", "Failed to Read Table: SyncTest_MarketOrder", ex);
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(ICollection<Guid> entities)
        {
            if (entities != null && entities.Count > 0)
            {
                string ids = string.Empty;

                foreach (Guid syncEntityInfo in entities)
                {
                    ids = ids + "'" + syncEntityInfo + "',";
                }

                ids = ids.Remove(ids.Length - 1, 1);


                return GetOrders(ids);
            }
            else
            {
                return null;
            }
        }

        internal Collection<ISynchronizable> GetOrders(string commaSeparetedIds)
        {
            Collection<ISynchronizable> orders = new Collection<ISynchronizable>();
            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    string commandString = " SELECT * from [SyncTest_LimitOrder] "
                                           + " WHERE [OrderId] IN (" + commaSeparetedIds + "); ";

                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(commandString))
                    {
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders(string commaSeparetedIds)", "Failed to Read Table: SyncTest_LimitOrder", ex);
            }

            return orders;
        }

        public ICollection<ISynchronizable> GetOrders(EntityFilter filter)
        {
            Collection<ISynchronizable> orders = new Collection<ISynchronizable>();
            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * from [SyncTest_LimitOrder] ");
	                

                    if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
                    {
                        int a = 0;
                        sb.Append("WHERE ");
                        foreach (Filter f in filter.FilterList)
                        {
                            a = a + 1;
                            sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                            if (a < filter.FilterList.Count)
                            {
                                sb.Append(" AND ");
                            }
                        }
                    }

                    sb.Append(";");

                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(sb.ToString()))
                    {
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            while (reader.Read())
                            {
                                LimitOrder order = new LimitOrder();
                                order.Report = new Report();
                                order.Id = new Guid(Convert.ToString(reader["OrderId"]));
                                order.Creator = reader["Creator"] == DBNull.Value ? null : Convert.ToString(reader["Creator"]);
                                order.Description = reader["Description"] == DBNull.Value ? null : Convert.ToString(reader["Description"]);
                                order.Date = new DateTime(((DateTime)reader["Date"]).Ticks, DateTimeKind.Utc);
                                order.IsAssigned = Convert.ToBoolean(reader["IsAssigned"]);
                                order.Image = reader["Image"] == DBNull.Value
                                                  ? null
                                                  : new ComparableArray {Content = (byte[]) reader["Image"]};
                                order.Report.Title = reader["ReportTitle"] == DBNull.Value ? null : Convert.ToString(reader["ReportTitle"]);
                                order.Report.Content = reader["ReportContent"] == DBNull.Value ? null : Convert.ToString(reader["ReportContent"]);
                                order.Report.Id = Convert.ToInt32(reader["ReportId"]);

                                string editors = Convert.ToString(reader["Editors"]);
                                List<string> editorList = editors.Split('-').ToList();
                                order.Report.Editors = new List<Editor>();
                                foreach (string s in editorList)
                                {
                                    order.Report.Editors.Add(new Editor() { EditorName = s });
                                }

                                orders.Add(order);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrders(EntityFilter filter)", "Failed to Read Table: SyncTest_LimitOrder", ex);
            }

            return orders;
        }

        public ICollection<Guid> GetOrderIds(EntityFilter filter)
        {
            Collection<Guid> orders = new Collection<Guid>();
            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT OrderId from [SyncTest_LimitOrder] ");

                    if (filter != null && filter.FilterList != null && filter.FilterList.Count > 0)
                    {
                        sb.Append("WHERE ");
                        int a = 0;
                        foreach (Filter f in filter.FilterList)
                        {
                            a = a + 1;
                            sb.Append("[" + GetTableColumnName(f.PropertyName) + "]" + GetOperator(f.Operator) + GetValue(f.PropertyName, f.Value));
                            if (a < filter.FilterList.Count)
                            {
                                sb.Append(" AND ");
                            }
                        }
                    }

                    sb.Append(";");
                    using (IDbCommand command = Application.Instance.Storage.CreateCommand(sb.ToString()))
                    {
                        using (IDataReader reader = Application.Instance.Storage.ExecuteQueryCommand(command))
                        {
                            while (reader.Read())
                            {
                                Guid oid = Guid.Parse(Convert.ToString(reader["OrderId"]));
                                orders.Add(oid);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("GetOrderIds(EntityFilter filter)", "Failed to Read Table: SyncTest_LimitOrder", ex);
            }

            return orders;
        }

        public void DeleteOrderReords()
        {
            IDbCommand command = null;
            try
            {
                if (Application.Instance.Storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM SyncTest_LimitOrder");
                    command = Application.Instance.Storage.CreateCommand(sb.ToString());
                    Application.Instance.Storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                _log.Error("DeleteOrderReords()", "Failed to delete records: SyncTest_LimitOrder", ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }

        }

        private string GetValue(string propertyName, object value)
        {
            switch (propertyName)
            {
                case "Id":
                    return "'" + value + "'";
                case "Creator":
                    return "'" + value + "'";
                case "Description":
                    return "'" + value + "'";
                case "IsAssigned":
                    return value.ToString();
                case "Date":
                    return value.ToString();
                default:
                    return value.ToString();
            }
        }

        private string GetTableColumnName(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return "OrderId";
                case "Creator":
                    return "Creator";
                case "Description":
                    return "Description";
                case "IsAssigned":
                    return "IsAssigned";
                case "Date":
                    return "Date";
                default:
                    return "";
            }
        }

        private string GetOperator(FilterOperator filterOperator)
        {
            string op = "=";
            switch (filterOperator)
            {
                case FilterOperator.EqualTo:
                    op = "=";
                    break;
                case FilterOperator.NotEqualTo:
                    op = "!=";
                    break;
                case FilterOperator.GreaterThan:
                    op = ">";
                    break;
                case FilterOperator.LessThan:
                    op = "<";
                    break;
            }
            return op;
        }

        /// <summary>
        /// Disposes the IDbCommand.
        /// </summary>
        /// <param name="command">The IDbCommand object.</param>
        internal void DisposeCommand(IDbCommand command)
        {
            if (command != null)
                command.Dispose();
        }

        /// <summary>
        /// Disposes the IDataReader.
        /// </summary>
        /// <param name="reader">The IDataReader object.</param>
        internal void DisposeReader(IDataReader reader)
        {
            if (reader != null)
            {
                reader.Close();
                reader.Dispose();
            }
        }
    }
}
