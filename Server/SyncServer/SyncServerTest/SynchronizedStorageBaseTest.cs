﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Namespace1;
using NUnit.Framework;

namespace PreCom.Synchronization
{
    [TestFixture]
    public class SynchronizedStorageBaseTest
    {
        [Test]
        public void EntityChanged_ExceptionInHandler_ShouldNotThrowUnhandledException()
        {
            bool unhandledException = false;
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                unhandledException = true;
            };

            MyTypeStorage storage = new MyTypeStorage();
            storage.EntityChanged += (sender, args) =>
            {
                throw new Exception("Oh no!");
            };
            storage.FireChanged(new ChangeEventArgs());

            // This is not perfect, but since we don't control the worker thread
            // it is hard to know when it is done
            Thread.Sleep(1000);

            Assert.IsFalse(unhandledException);
        }
    }
}
