﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using NUnit.Framework;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Test.Mocks;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test
{
    [TestFixture]
    public class StorageTransactionScopeTest
    {
        public static StorageBase Storage { get; set; }
        public static ILog Log { get; set; }

        public static MarketOrderDao MarketOrderDao { get; set; }
        public static MarketOrderManager MarketOrderStorage { get; set; }
        public List<MarketOrder> Orders { get; set; }

        [TestFixtureSetUp]
        public static void ClassInitialize()
        {
            // SQL Server 2012
            ////Storage = new StorageSql(@"Server=pmcsrvloaddb01;DataBase=SyncUnitTest;Min Pool Size=1;Max Pool Size=100;Pooling=True;user id=sa;password=Krig&Fred");
            // SQL Server 2014
            ////Storage = new StorageSql(@"Data Source=pmcsrvloaddb01,1440;Initial Catalog=SyncUnitTest;Min Pool Size=1;Max Pool Size=100;Pooling=True;user id=sa;password=Krig&Fred");
            Storage = new StorageSql(@"Server=localhost;DataBase=SyncUnitTest;Min Pool Size=1;Max Pool Size=100;Integrated Security=SSPI");

            Storage.Initialize();
            Log = new MockLog();
            MarketOrderDao = new MarketOrderDao(Storage, new LogUtil(new MockModule(), Log));
            MarketOrderStorage = new MarketOrderManager(MarketOrderDao);
            SyncDao.Storage = Storage;
            SyncDao.CreateStorage(Guid.NewGuid().ToString());
            MarketOrderStorage.AcquireUpdateLock = SyncDao.AcquireUpdateLock;
            MarketOrderDao.CreateOrderTable();
        }

        [TestFixtureTearDown]
        public static void ClassCleanup()
        {
            bool cancel;
            Storage.Dispose(out cancel);
        }

        [SetUp]
        public void TestInitialize()
        {
            Orders = new List<MarketOrder>();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                var desc = Guid.NewGuid().ToString();
                for (int i = 0; i < 10; i++)
                {
                    var order = MarketOrder.Create(GetType().Name, desc);
                    MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));
                    Orders.Add(order);
                }

                scope.Complete();
            }

            // This command is normally executed by the SyncModule itself.
            var setAllowSnapshotCmd = Storage.CreateCommand("ALTER DATABASE SyncUnitTest SET ALLOW_SNAPSHOT_ISOLATION ON");
            Storage.ExecuteNonCommand(setAllowSnapshotCmd);
        }

        [TearDown]
        public void TestCleanup()
        {
            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                foreach (var marketOrder in Orders)
                {
                    try
                    {
                        MarketOrderStorage.Delete(marketOrder.Id, new DeleteArgs(GetType().Name));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Error deleting entity, one of the tests might have deleted it. Exception: " + ex);
                    }

                    // Also delete the records from the actual sync module. Otherwise they are only marked as deleted.
                    MarketOrderDao.DeleteSyncReord(marketOrder.Id);
                    MarketOrderDao.DeleteSyncPropertyReord(marketOrder.Id);
                }

                scope.Complete();
            }
        }

        [Test]
        [Explicit("Testing performance of TransactionScope")]
        public void PerformanceTestOfStorageTransactionScope()
        {
            var sw = Stopwatch.StartNew();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                for (int i = 0; i < 10000; i++)
                {
                    MarketOrderStorage.Create(MarketOrder.Create("Test", "TestDesc"), new CreateArgs("TestOriginator"));
                }

                    scope.Complete();
            }

            sw.Stop();
            Debug.WriteLine("Test duration: " + sw.Elapsed);
        }

        [Test]
        public void Complete_WhenNotReadCommitted_ChangesToReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                Assert.AreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                Assert.AreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be snapshot since we specify it in the transaction scope.");
                Assert.AreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            Assert.AreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [Test]
        public void Complete_WhenReadCommitted_RemainsInReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                Assert.AreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                Assert.AreEqual(transactionScopeLevel, IsolationLevel.ReadCommitted, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                Assert.AreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have remain as read committed");
            Assert.AreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [Test]
        public void Dispose_WhenNotReadCommitted_ChangesToReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                Assert.AreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                Assert.AreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be snapshot since we specify it in the transaction scope.");
                Assert.AreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            Assert.AreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [Test]
        public void Dispose_WhenReadCommitted_RemainsInReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                Assert.AreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                Assert.AreEqual(transactionScopeLevel, IsolationLevel.ReadCommitted, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                Assert.AreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have remain as read committed");
            Assert.AreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [Test]
        public void Complete_WhenHavingOuterTransaction_DoesntResetTheConnectionIsolationLevel()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                using (var scope2 = new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
                {
                    short transactionScopeSpid2 = GetSpid();

                    Assert.AreEqual(transactionScopeSpid2, transactionScopeSpid, "Should have same spid in order for test to be reliable");

                    scope2.Complete();
                }

                var dbLevelInTransactionScopeAfter = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                Assert.AreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                Assert.AreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                Assert.AreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
                Assert.AreEqual(dbLevelInTransactionScopeAfter, dbLevelInTransactionScope, "Expected level to be same as before inner scope.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            Assert.AreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [Test]
        public void Complete_WithSerializable_CommitsDataToDb()
        {
            MarketOrder order;

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Serializable))
            {
                order = MarketOrder.Create(GetType().Name, "AutoTest");
                MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));

                var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                Assert.AreEqual(IsolationLevel.Serializable, transactionScopeLevel, "Scope isolation level should be Serializable since we specify it in the constructor.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            var orderFromDb = (MarketOrder)MarketOrderStorage.Read(new[] { order.Id }).First();

            Assert.AreEqual(orderFromDb.Id, order.Id, "Order we just created should be the same we read from database.");
            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");
        }

        [Test]
        public void Constructor_WithRepeatableRead_SetIsolationLevelToRepeatableRead()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            var semaphore = new SemaphoreSlim(0, 1);
            var task = Task.Run(() =>
            {
                using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.RepeatableRead))
                {
                    // Read order which should make it locked in database.
                    MarketOrderStorage.Read(new[] { Orders[0].Id });
                    semaphore.Release();
                    Thread.Sleep(5000);

                    var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                    Assert.AreEqual(transactionScopeLevel, IsolationLevel.RepeatableRead, "Scope isolation level should be RepeatableRead since we specify it in the constructor.");

                    scope.Complete();
                }
            });

            Orders[0].Description = "Updated";

            var sw = new Stopwatch();
            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                // Wait until order has been read by other thread.
                semaphore.Wait();

                sw.Start();

                // Update should be blocked by the previous transaction with RepeatableRead.
                MarketOrderStorage.Update(Orders[0], new UpdateArgs(GetType().Name));
                sw.Stop();

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            Assert.IsTrue(sw.Elapsed >= TimeSpan.FromSeconds(5), "Update should be blocked by the read operation of repeatable read isolation level.");
            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");

            // Wait until complete so we dont affect other tests.
            task.Wait();
        }

        [Test]
        public void Constructor_WithSerializable_RemainsSerializableUntilCompleted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            if (Storage.ObjectExists("TestTable", ObjectType.Table))
            {
                var dropTableCmd1 = Storage.CreateCommand("DROP TABLE TestTable");
                Storage.ExecuteNonCommand(dropTableCmd1);
            }

            var createTableCmd = Storage.CreateCommand("CREATE TABLE TestTable(Id int NOT NULL, MyInt int NOT NULL, CONSTRAINT PK_TestTable PRIMARY KEY CLUSTERED (Id ASC))");
            Storage.ExecuteNonCommand(createTableCmd);

            for (int i = 0; i < 10; i++)
            {
                var insertCmd = Storage.CreateCommand(string.Format("INSERT INTO TestTable (Id, MyInt) VALUES ({0}, {1})", i, i + 100));
                Storage.ExecuteNonCommand(insertCmd);
            }

            var semaphore = new SemaphoreSlim(0, 1);
            var task = Task.Run(() =>
            {
                using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Serializable))
                {
                    // Read row 2 and 4. This should make row 3 locked due to serializable isolation if everything works ok.
                    var readCmd = Storage.CreateCommand("Select * From TestTable Where Id = 2");
                    Storage.ExecuteScalarCommand(readCmd);

                    readCmd = Storage.CreateCommand("Select * From TestTable Where Id = 4");
                    Storage.ExecuteScalarCommand(readCmd);

                    semaphore.Release();
                    Thread.Sleep(5000);

                    readCmd = Storage.CreateCommand("Select MyInt From TestTable Where Id = 2");
                    var value = Storage.ExecuteScalarCommand(readCmd);

                    var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                    Assert.AreEqual(transactionScopeLevel, IsolationLevel.Serializable, "Scope isolation level should be Serializable since we specify it in the constructor.");

                    scope.Complete();
                }
            });

            var sw = new Stopwatch();
            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                // Wait until rows has been read by other thread.
                semaphore.Wait();

                sw.Start();

                var updateCmd = Storage.CreateCommand(string.Format("UPDATE TestTable SET MyInt = 9999 Where Id = 2"));
                Storage.ExecuteNonCommand(updateCmd);

                // Update should be blocked by the previous transaction with Serializable.
                sw.Stop();

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            Assert.IsTrue(sw.Elapsed >= TimeSpan.FromSeconds(5), "Update should be blocked by the read operation of serializable isolation level.");
            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");

            // Wait until complete so we dont affect other tests.
            task.Wait();
        }

        [Test]
        public void Constructor_WithSnapshot_RemainsSnapshotUntilCompleted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            if (Storage.ObjectExists("TestTable", ObjectType.Table))
            {
                var dropTableCmd1 = Storage.CreateCommand("DROP TABLE TestTable");
                Storage.ExecuteNonCommand(dropTableCmd1);
            }

            var createTableCmd = Storage.CreateCommand("CREATE TABLE TestTable(Id int NOT NULL, MyInt int NOT NULL, CONSTRAINT PK_TestTable PRIMARY KEY CLUSTERED (Id ASC))");
            Storage.ExecuteNonCommand(createTableCmd);

            for (int i = 0; i < 10; i++)
            {
                var insertCmd = Storage.CreateCommand(string.Format("INSERT INTO TestTable (Id, MyInt) VALUES ({0}, {1})", i, i + 100));
                Storage.ExecuteNonCommand(insertCmd);
            }

            var semaphore = new SemaphoreSlim(0, 1);
            var task = Task.Run(() =>
            {
                using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
                {
                    // Read row 2 and 4. Update row 4 before reading it in another thread. The update value should not be visible since in snapshot.
                    var readCmd = Storage.CreateCommand("Select MyInt From TestTable Where Id = 2");
                    Storage.ExecuteScalarCommand(readCmd);

                    semaphore.Release();
                    Thread.Sleep(5000);

                    readCmd = Storage.CreateCommand("Select MyInt From TestTable Where Id = 4");
                    var value = Storage.ExecuteScalarCommand(readCmd);

                    var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                    Assert.AreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Scope isolation level should be Snapshot since we specify it in the constructor.");
                    Assert.AreEqual(104, value, "Value should not be changed due to update from another thread.");

                    scope.Complete();
                }
            });

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                semaphore.Wait();

                var updateCmd = Storage.CreateCommand(string.Format("UPDATE TestTable SET MyInt = 9999 Where Id = 4"));
                Storage.ExecuteNonCommand(updateCmd);

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            Assert.AreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");

            // Wait until complete so we dont affect other tests.
            task.Wait();
        }

        [Test]
        public void Dispose_WhenTransactionHasTimeout_ShouldNotThrowException()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            Assert.AreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            Exception exception = null;
            StorageTransactionScope scope = null;
            try
            {
                scope = new StorageTransactionScope(Storage,
                                                    Log,
                                                    IsolationLevel.RepeatableRead,
                                                    TransactionScopeOption.Required,
                                                    TimeSpan.FromSeconds(1));

                Thread.Sleep(1500);
            }
            finally
            {
                try
                {
                    if (scope != null)
                    {
                        scope.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            }

            object time;
            using (var cmd = Storage.CreateCommand("SELECT GetDate()"))
            {
                time = Storage.ExecuteScalarCommand(cmd);
            }

            Assert.AreEqual(exception, null, "No exception should have been throw");
            Assert.IsTrue(time != null, "Time should not be null since database commands should work");
        }

        private short GetSpid()
        {
            using (var cmd = Storage.CreateCommand("SELECT @@SPID"))
            {
                return (short)Storage.ExecuteScalarCommand(cmd);
            }
        }
    }
}
