﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PreCom.Core.Messaging;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    [TestFixture]
    public class PublisherTest
    {
        [Test]
        public void Initialize_NoTopic_TopicCreated()
        {
            var messageBusMock = new Mock<MessageBusBase>();
            messageBusMock.Setup(@base => @base.TopicExists(It.IsAny<string>())).Returns(false);
            messageBusMock.Setup(@base => @base.CreateTopic(It.IsAny<string>()));

            var publisher = new Publisher(messageBusMock.Object);

            publisher.Initialize();

            messageBusMock.Verify(@base => @base.CreateTopic(Publisher.TopicPullInvokeMessage));
            messageBusMock.Verify(@base => @base.CreateTopic(Publisher.TopicAutomaticPushArgsBroadcast));
        }

        [Test]
        public void Initialize_TopicExists_TopicNotCreated()
        {
            var messageBusMock = new Mock<MessageBusBase>();
            messageBusMock.Setup(@base => @base.TopicExists(It.IsAny<string>())).Returns(true);

            var publisher = new Publisher(messageBusMock.Object);

            publisher.Initialize();

            messageBusMock.Verify(@base => @base.CreateTopic(Publisher.TopicPullInvokeMessage), Times.Never);
            messageBusMock.Verify(@base => @base.CreateTopic(Publisher.TopicAutomaticPushArgsBroadcast), Times.Never);
        }

        [Test]
        public void Broadcast_PullInvoke_SendAsyncCalled()
        {
            var messageBusMock = new Mock<MessageBusBase>();
            messageBusMock.Setup(
                @base => @base.SendAsync(Publisher.TopicPullInvokeMessage, It.IsAny<Message<PullInvokeBroadcastMessage>>()))
                .Returns(Task.FromResult(true));

            var publisher = new Publisher(messageBusMock.Object);

            var p = new PullInvokeMessage();
            var credentials = new Collection<int>() { 1, 2 };
            publisher.Broadcast(p, credentials);

            messageBusMock.Verify(@base => @base.SendAsync(Publisher.TopicPullInvokeMessage,
                It.Is<Message<PullInvokeBroadcastMessage>>(
                    message => message.Body.Credentials.SequenceEqual(credentials))));
        }

        [Test]
        public void Broadcast_AutomaticPushArgs_SendAsyncCalled()
        {
            var messageBusMock = new Mock<MessageBusBase>();
            messageBusMock.Setup(
                @base => @base.SendAsync(Publisher.TopicAutomaticPushArgsBroadcast, It.IsAny<Message<PullInvokeBroadcastMessage>>()))
                .Returns(Task.FromResult(true));

            var publisher = new Publisher(messageBusMock.Object);

            var autoPushArgs = new AutomaticPushArgs();
            autoPushArgs.ChangeInfo = new Dictionary<string, string>() { { "Creator", "JohnDoe" } };
            publisher.Broadcast(autoPushArgs);

            messageBusMock.Verify(@base => @base.SendAsync(Publisher.TopicAutomaticPushArgsBroadcast,
                It.Is<Message<AutomaticPushArgsBroadcastMessage>>(
                    message => message.Body.AutomaticPushArgs.ChangeInfo["Creator"] == "JohnDoe")));
        }
    }
}
