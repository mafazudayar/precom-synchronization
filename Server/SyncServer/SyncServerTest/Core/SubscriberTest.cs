﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Moq;
using NUnit.Framework;
using PreCom.Core;
using PreCom.Core.Messaging;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Test.Mocks;

namespace PreCom.Synchronization.Core
{
    [TestFixture]
    public class SubscriberTest
    {
        private const string InstanceName = "MyInstance";
        private Mock<MessageBusBase> _messageBusMock;
        private Mock<ApplicationBase> _applicationMock;
        private Mock<MessageBusReceiverBase> _receiverMockPullInvoke;
        private Mock<MessageBusReceiverBase> _receiverMockAutomaticPushArgs;
        private Mock<CommunicationBase> _communicationMock;
        private MockDispatcher _dispatcherMock;
        private Mock<SynchronizationServerBase> _syncMock;
        private Action<Message<PullInvokeBroadcastMessage>> _receiveActionPullInvoke;
        private Action<Message<AutomaticPushArgsBroadcastMessage>> _receiveActionAutomaticPushArgs;

        [SetUp]
        public void Setup()
        {
            _messageBusMock = new Mock<MessageBusBase>();
            _applicationMock = new Mock<ApplicationBase>();
            _receiverMockPullInvoke = new Mock<MessageBusReceiverBase>();
            _receiverMockAutomaticPushArgs = new Mock<MessageBusReceiverBase>();
            _communicationMock = new Mock<CommunicationBase>();
            _dispatcherMock = new MockDispatcher();
            _syncMock = new Mock<SynchronizationServerBase>();

            _messageBusMock.Setup(@base => @base.DeleteSubscription(It.IsAny<string>(), It.IsAny<string>()));
            _messageBusMock.Setup(@base => @base.SubscriptionExists(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            _messageBusMock.Setup(@base => @base.CreateSubscription(It.IsAny<string>(), It.IsAny<string>(), false));

            _applicationMock.Setup(@base => @base.InstanceId).Returns(InstanceName);

            _receiverMockPullInvoke.Setup(@base => @base.RegisterReceiver(It.IsAny<Action<Message<PullInvokeBroadcastMessage>>>()))
                .Callback<Action<Message<PullInvokeBroadcastMessage>>>(action => _receiveActionPullInvoke = action);
            _receiverMockPullInvoke.Setup(@base => @base.Start());

            _receiverMockAutomaticPushArgs.Setup(@base => @base.RegisterReceiver(It.IsAny<Action<Message<AutomaticPushArgsBroadcastMessage>>>()))
                .Callback<Action<Message<AutomaticPushArgsBroadcastMessage>>>(action => _receiveActionAutomaticPushArgs = action);
            _receiverMockAutomaticPushArgs.Setup(@base => @base.Start());

            _messageBusMock.Setup(@base => @base.CreateReceiver(Publisher.TopicPullInvokeMessage, It.IsAny<string>(), null))
                .Returns(() => _receiverMockPullInvoke.Object);

            _messageBusMock.Setup(@base => @base.CreateReceiver(Publisher.TopicAutomaticPushArgsBroadcast, It.IsAny<string>(), null))
                .Returns(() => _receiverMockAutomaticPushArgs.Object);

            _communicationMock.Setup(@base => @base.IsLoggedin(It.IsInRange(1, 2, Range.Inclusive))).Returns(true);
        }

        [Test]
        public void Initialize_SubscriptionsCreatedAndReceivedStarted()
        {
            // Setup
            var subscriber = new Subscriber(_messageBusMock.Object, _applicationMock.Object, _communicationMock.Object, _dispatcherMock, _syncMock.Object);

            // Act
            subscriber.Initialize();

            // Verify
            _messageBusMock.Verify(@base => @base.CreateSubscription(Publisher.TopicPullInvokeMessage, InstanceName, true));
            _messageBusMock.Verify(@base => @base.CreateSubscription(Publisher.TopicAutomaticPushArgsBroadcast, InstanceName, true));
            _receiverMockPullInvoke.Verify(@base => @base.Start());
            _receiverMockAutomaticPushArgs.Verify(@base => @base.Start());
        }

        [Test]
        public void Dispose_SubscriptionDeleted()
        {
            // Setup
            var subscriber = new Subscriber(_messageBusMock.Object, _applicationMock.Object, _communicationMock.Object, _dispatcherMock, _syncMock.Object);
            subscriber.Initialize();

            // Act
            subscriber.Dispose();

            // Verify
            _messageBusMock.Verify(@base => @base.DeleteSubscription(Publisher.TopicPullInvokeMessage, InstanceName));
            _messageBusMock.Verify(@base => @base.DeleteSubscription(Publisher.TopicAutomaticPushArgsBroadcast, InstanceName));
        }

        [Test]
        public void ReceiveCallbackPullInvoke_MessagesSent()
        {
            // Setup
            var subscriber = new Subscriber(_messageBusMock.Object, _applicationMock.Object, _communicationMock.Object, _dispatcherMock, _syncMock.Object);
            subscriber.Initialize();

            // Act
            var message = new Message<PullInvokeBroadcastMessage>(new PullInvokeBroadcastMessage());
            message.Body.Credentials = new Collection<int>() { 1, 2 };
            message.Body.PullInvokeMessage = new PullInvokeMessage();
            _receiveActionPullInvoke.Invoke(message);

            // Verify
            var pullInvokeMessagesSent = _dispatcherMock.SentItems.FindAll(item => item.Entity.GetType() == typeof(PullInvokeMessage));
            Assert.AreEqual(message.Body.Credentials.Count, pullInvokeMessagesSent.Count);
        }

        [Test]
        public void ReceiveCallbackAutomaticPushArgs_EventRaised()
        {
            AutomaticPushArgs receivedPushArgs = null;

            // Setup
            var subscriber = new Subscriber(_messageBusMock.Object, _applicationMock.Object, _communicationMock.Object, _dispatcherMock, _syncMock.Object);
            subscriber.Initialize();
            subscriber.AutomaticPushReceived += (sender, args) => receivedPushArgs = args;

            // Act
            var message = new Message<AutomaticPushArgsBroadcastMessage>(new AutomaticPushArgsBroadcastMessage());
            message.Body.AutomaticPushArgs = new AutomaticPushArgs();
            message.Body.AutomaticPushArgs.CredentialId = 5;
            message.Body.AutomaticPushArgs.ChangeInfo = new Dictionary<string, string>();
            message.Body.AutomaticPushArgs.ChangeInfo.Add("Creator", "JohnDoe");
            _receiveActionAutomaticPushArgs.Invoke(message);

            // Verify
            Assert.IsNotNull(receivedPushArgs);
            Assert.AreEqual(message.Body.AutomaticPushArgs.CredentialId, receivedPushArgs.CredentialId);
            Assert.IsNotNull(receivedPushArgs.ChangeInfo);
            Assert.IsTrue(receivedPushArgs.ChangeInfo.ContainsKey("Creator"));
            Assert.AreEqual("JohnDoe", receivedPushArgs.ChangeInfo["Creator"]);
        }
    }
}
