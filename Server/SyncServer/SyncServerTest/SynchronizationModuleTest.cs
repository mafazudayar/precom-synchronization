﻿using System;
using System.Collections.Generic;
using Moq;
using Namespace1;
using NUnit.Framework;
using PreCom;
using PreCom.Core;
using PreCom.Core.Messaging;
using PreCom.Core.Modules;
using PreCom.Synchronization;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Test.Mocks;

namespace SyncServerTest
{
    using PreCom.Login;

    /// <summary>
    /// Summary description for SyncServerTest
    /// </summary>
    [TestFixture]
    public class SyncServerTest
    {
        private SynchronizationModule _synchronization;

        [SetUp]
        public void MyTestInitialize()
        {
            var applicationMock = new Mock<ApplicationBase>();
            var settingsMock = new Mock<SettingsBase>();
            var logMock = new Mock<ILog>();
            var storageMock = new Mock<StorageBase>();
            var communicationMock = new Mock<CommunicationBase>();
            var messageBusMock = new Mock<MessageBusBase>();
            var loginMock = new Mock<ILoginStatus>();

            _synchronization = new SynchronizationModule(applicationMock.Object,
                settingsMock.Object,
                logMock.Object,
                storageMock.Object,
                communicationMock.Object,
                messageBusMock.Object, loginMock.Object);
            _synchronization.TypeManager = new TypeManager(_synchronization, logMock.Object);
            _synchronization.Dispatcher = new MockDispatcher();
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RegisterDuplicateTypes()
        {
            _synchronization.RegisterStorage<Namespace1.MyType>(new Namespace1.MyTypeStorage());

            // Shall throw
            _synchronization.RegisterStorage<Namespace2.MyType>(new Namespace2.MyTypeStorage());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterNullStorage()
        {
            _synchronization.RegisterStorage<Namespace1.MyType>(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterAutomaticPushNullPredicate()
        {
            _synchronization.RegisterAutomaticPush<Namespace1.MyType>(null, Operation.Created);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RegisterAutomaticPushNoTypeRegistered()
        {
            _synchronization.RegisterAutomaticPush<Namespace1.MyType>(args => null, Operation.Created);
        }

        [Test]
        public void RegisterAutomaticPush()
        {
            _synchronization.RegisterStorage<Namespace1.MyType>(new Namespace1.MyTypeStorage());
            _synchronization.RegisterAutomaticPush<Namespace1.MyType>(args => null, Operation.Created);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterLoginFilterNullDelegate()
        {
            _synchronization.RegisterLoginFilter<Namespace1.MyType>(null);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RegisterLoginFilterNoTypeRegistered()
        {
            _synchronization.RegisterLoginFilter<Namespace1.MyType>(args => null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PushNullCredentials()
        {
            _synchronization.Push<MyType>(new EntityFilter(), null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void PushEmptyCredentials()
        {
            _synchronization.Push<MyType>(new EntityFilter(), new List<int>());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PushNullFilter()
        {
            _synchronization.Push<MyType>(null, new List<int>() { 1 });
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PushNoTypeRegistered()
        {
            _synchronization.Push<MyType>(new EntityFilter(), new List<int>() { 1 });
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterNullConflictResolutionDelegate()
        {
            _synchronization.RegisterConflictResolver((ConflictResolutionDelegate<MyType>)null);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RegisterConflictResolutionDelegateNoTypeRegistered()
        {
            _synchronization.RegisterConflictResolver<MyType>(info => null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RegisterSyncEvent_NullNotifier_Throws()
        {
            _synchronization.RegisterSyncEventNotifier<MyType>(null);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RegisterSyncEvent_NoTypeRegistered_Throws()
        {
            _synchronization.RegisterSyncEventNotifier<MyType>((id, info) => { });
        }
    }
}

namespace Namespace1
{
    public class MyType : ISynchronizable
    {
        public Guid Guid
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class MyTypeStorage : SynchronizedStorageBase<MyType>
    {
        public void FireChanged(ChangeEventArgs args)
        {
            FireChangedEvent(args);
        }

        protected override void OnCreate(ICollection<ISynchronizable> entity, CreateArgs args)
        {
            throw new NotImplementedException();
        }

        protected override void OnUpdate(ISynchronizable entity, UpdateArgs args)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(Guid id, DeleteArgs args)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<ISynchronizable> OnRead(EntityFilter filter)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<Guid> OnReadIds(EntityFilter filter)
        {
            throw new NotImplementedException();
        }
    }
}

namespace Namespace2
{
    public class MyType : ISynchronizable
    {
        public Guid Guid
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class MyTypeStorage : SynchronizedStorageBase<MyType>
    {
        protected override void OnCreate(ICollection<ISynchronizable> entity, CreateArgs args)
        {
            throw new NotImplementedException();
        }

        protected override void OnUpdate(ISynchronizable entity, UpdateArgs args)
        {
            throw new NotImplementedException();
        }

        protected override void OnDelete(Guid id, DeleteArgs args)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<ISynchronizable> OnRead(EntityFilter filter)
        {
            throw new NotImplementedException();
        }

        protected override ICollection<Guid> OnReadIds(EntityFilter filter)
        {
            throw new NotImplementedException();
        }
    }

}