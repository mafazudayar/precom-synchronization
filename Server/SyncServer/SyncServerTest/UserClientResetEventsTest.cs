﻿using System;
using NUnit.Framework;
using PreCom.Synchronization.Core;

namespace SyncServerTest
{
    [TestFixture]
    public class UserClientResetEventsTest
    {
        private UserClientResetEvents _userClientResetEvents;
        private int _credentialId;
        private string _clientId;


        [SetUp]
        public void Setup()
        {
            _userClientResetEvents = new UserClientResetEvents(0);

            for (int i = 0; i < 1000; i++)
            {
                _userClientResetEvents.Set(i, Guid.NewGuid().ToString());
            }

            for (int i = 1000; i < 2000; i++)
            {
                _userClientResetEvents.Reset(i, Guid.NewGuid().ToString());
            }

            for (int i = 2000; i < 3000; i++)
            {
                _userClientResetEvents.Wait(i);
            }

            _credentialId = 10000;
            _clientId = Guid.NewGuid().ToString();
        }

        [Test]
        public void Wait_After_Set_Returns_True()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsTrue(result);
        }

        [Test]
        public void Wait_After_SetWithWrongCredentialId_Returns_False()
        {
            _userClientResetEvents.Set(_credentialId + 1, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Wait_After_SetWithWrongClientId_Returns_False()
        {
            _userClientResetEvents.Set(_credentialId, Guid.NewGuid().ToString());

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Wait_Returns_False()
        {
            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Wait_After_Reset_Returns_False()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);
            _userClientResetEvents.Reset(_credentialId, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Wait_After_ResetWithWrongCredentialId_Returns_True()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);
            _userClientResetEvents.Reset(_credentialId + 1, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsTrue(result);
        }

        [Test]
        public void Wait_After_ResetWithWrongClientId_Returns_True()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);
            _userClientResetEvents.Reset(_credentialId, _clientId + 1);

            bool result = _userClientResetEvents.Wait(_credentialId, _clientId);

            Assert.IsTrue(result);
        }

        [Test]
        public void WaitByCredentialId_After_Set_Returns_True()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId);

            Assert.IsTrue(result);
        }

        [Test]
        public void WaitByCredentialId_After_SetWithWrongCredentialId_Returns_False()
        {
            _userClientResetEvents.Set(_credentialId + 1, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId);

            Assert.IsFalse(result);
        }

        [Test]
        public void WaitByCredentialId_Returns_False()
        {
            bool result = _userClientResetEvents.Wait(_credentialId);

            Assert.IsFalse(result);
        }

        [Test]
        public void WaitByCredentialId_After_Reset_Returns_False()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);
            _userClientResetEvents.Reset(_credentialId, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId);

            Assert.IsFalse(result);
        }

        [Test]
        public void WaitByCredentialId_After_ResetWithWrongCredentialId_Returns_True()
        {
            _userClientResetEvents.Set(_credentialId, _clientId);
            _userClientResetEvents.Reset(_credentialId + 1, _clientId);

            bool result = _userClientResetEvents.Wait(_credentialId);

            Assert.IsTrue(result);
        }
    }
}
