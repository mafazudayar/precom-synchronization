﻿using System;
using System.Diagnostics;
using PreCom.Core.Log;
using PreCom.Core.Modules;

namespace PreCom.Synchronization.Test.Mocks
{
    internal class MockLog : ILog
    {
        public bool Write(LogItem item)
        {
            var output = string.Format("{0};{1};{2};{3};{4};{5};{6}",
                DateTime.Now,
                item.ID,
                item.Level,
                item.Source == null ? "null" : item.Source.Name,
                item.Header,
                item.Body,
                item.Dump);

            Debug.WriteLine(output);

            return true;
        }

        public IAsyncResult BeginWrite(LogItem item, AsyncCallback callback)
        {
            var func = new Func<LogItem, bool>(Write);

            return func.BeginInvoke(item, callback, null);
        }
    }
}
