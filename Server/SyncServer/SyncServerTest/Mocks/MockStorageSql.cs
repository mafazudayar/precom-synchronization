﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using PreCom.Core;

namespace PreCom.Synchronization.Test.Mocks
{
    internal class StorageSql : StorageBase
    {
        #region Private Members

        private const string DefaultConnectionString = "Server=localhost;DataBase=PreCom;Min Pool Size=1;Max Pool Size=100;Pooling=True;Integrated security=SSPI";

        private bool _isInitialized;
        private string _recoveryModel;
        private string _databaseName;
        private string _masterDatabase;
        private string _connectionString;
        private string _masterConnectionString;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageSql"/> class.
        /// This constructor should be used when the class is tested or mocked.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public StorageSql(string connectionString)
        {
            ConnectionString = connectionString ?? DefaultConnectionString;
        }

        #endregion

        #region Private Methods

        private static void SetIsolationLevel(IDbConnection connection)
        {
            // Set isolation level if running inside TransactionScope.
            // This is a workaround for new behavior in SQL Server 2014 that
            // resets the IsolationLevel when returning a connection to the pool.
            var currentTransaction = System.Transactions.Transaction.Current;
            var sqlConnection = connection as SqlConnection;
            if (currentTransaction != null && sqlConnection != null)
            {
                if (currentTransaction.TransactionInformation.Status == System.Transactions.TransactionStatus.Aborted ||
                    currentTransaction.TransactionInformation.Status == System.Transactions.TransactionStatus.Committed)
                {
                    return;
                }

                var setIsolationLevel = "SET TRANSACTION ISOLATION LEVEL ";

                switch (currentTransaction.IsolationLevel)
                {
                    case System.Transactions.IsolationLevel.Serializable:
                        setIsolationLevel += "SERIALIZABLE";
                        break;

                    case System.Transactions.IsolationLevel.RepeatableRead:
                        setIsolationLevel += "REPEATABLE READ";
                        break;

                    case System.Transactions.IsolationLevel.ReadUncommitted:
                        setIsolationLevel += "READ UNCOMMITTED";
                        break;

                    case System.Transactions.IsolationLevel.Snapshot:
                        setIsolationLevel += "SNAPSHOT";
                        break;

                    // ReSharper disable once RedundantCaseLabel - Reason: want to be extra clear.
                    case System.Transactions.IsolationLevel.ReadCommitted:
                    default:
                        setIsolationLevel += "READ COMMITTED";
                        break;
                }

                var cmd = new SqlCommand(setIsolationLevel, sqlConnection);
                cmd.ExecuteNonQuery();
            }
        }

        private void ReadSettings()
        {
            _recoveryModel = "SIMPLE";
            _masterDatabase = "master";

            _connectionString = ConnectionString;

            var builder = new SqlConnectionStringBuilder(_connectionString);
            _databaseName = builder.InitialCatalog;

            builder.InitialCatalog = _masterDatabase;
            _masterConnectionString = builder.ToString();
        }

        private bool Contains(StorageQueryParameter param)
        {
            string sqlQuery = "SELECT * FROM INFORMATION_SCHEMA.{0} WHERE {1}";
            IDbCommand command;
            object dbResult;

            switch (param.Type)
            {
                case ObjectType.StoredProc:
                    command = CreateCommand(string.Format(sqlQuery, new object[] { "ROUTINES", "ROUTINE_NAME=@RoutineName" }));
                    command.Parameters.Add(CreateParameter("@RoutineName", param.ObjectName));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                case ObjectType.Table:
                    command = CreateCommand(string.Format(sqlQuery, new object[] { "TABLES", "TABLE_NAME=@TableName" }));
                    command.Parameters.Add(CreateParameter("@TableName", param.ObjectName));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                case ObjectType.ForeignKey:
                    command = CreateCommand(string.Format(sqlQuery, new object[] { "TABLE_CONSTRAINTS", "CONSTRAINT_NAME=@ConstraintsName AND CONSTRAINT_TYPE=@ConstraintsType" }));
                    command.Parameters.Add(CreateParameter("@ConstraintsName", param.ObjectName));
                    command.Parameters.Add(CreateParameter("@ConstraintsType", "FOREIGN KEY"));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                case ObjectType.PrimaryKey:
                    command = CreateCommand(string.Format(sqlQuery, new object[] { "TABLE_CONSTRAINTS", "CONSTRAINT_NAME=@ConstraintsName AND CONSTRAINT_TYPE=@ConstraintsType" }));
                    command.Parameters.Add(CreateParameter("@ConstraintsName", param.ObjectName));
                    command.Parameters.Add(CreateParameter("@ConstraintsType", "PRIMARY KEY"));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                case ObjectType.Trigger:
                    command = CreateCommand("SELECT name FROM sys.triggers WHERE name = @TriggerName");
                    command.Parameters.Add(CreateParameter("@TriggerName", param.ObjectName));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                case ObjectType.Column:
                    if (string.IsNullOrEmpty(param.ParentName))
                    {
                        throw new ArgumentException("StorageQueryParameter.ParentName must have a value");
                    }

                    command = CreateCommand(string.Format(sqlQuery, new object[] { "COLUMNS", "TABLE_NAME = @TableName AND COLUMN_NAME = @ColumnName" }));
                    command.Parameters.Add(CreateParameter("@TableName", param.ParentName));
                    command.Parameters.Add(CreateParameter("@ColumnName", param.ObjectName));
                    dbResult = ExecuteScalarCommand(command);
                    break;
                default:
                    throw new ArgumentException("StorageQueryParameter.Type is not valid");
            }

            return dbResult != null;
        }

        private bool CheckProcedureVersion(string procedureName, string newVersion)
        {
            bool isMatch = false;

            const string SqlQuery = "SELECT value  FROM fn_listextendedproperty (NULL,'schema','dbo', {0}, {1}, NULL, NULL)";

            using (IDbCommand command = CreateCommand(string.Format(SqlQuery, "@ObjectType", "@ProcedureName")))
            {
                command.Parameters.Add(CreateParameter("@ProcedureName", procedureName));
                command.Parameters.Add(CreateParameter("@ObjectType", "PROCEDURE"));

                using (IDataReader dbResult = ExecuteQueryCommand(command))
                {
                    if (dbResult != null)
                    {
                        while (dbResult.Read())
                        {
                            string value = dbResult.GetString(0);

                            if (!string.IsNullOrEmpty(value) && value.Equals(newVersion))
                            {
                                isMatch = true;
                            }
                        }
                    }
                }
            }

            return isMatch;
        }

        private IDbCommand GetAddVersionToProcedureCommand(string procedureName, string version)
        {
            const string SqlQuery = "EXECUTE sys.sp_addextendedproperty " +
                      "@name = N'Version'," +
                      "@value = @Version ," +
                      "@level0type = N'SCHEMA', " +
                      "@level0name = N'dbo',  " +
                      "@level1type = @ObjectType," +
                      "@level1name = @ObjectName";

            IDbCommand command = CreateCommand(SqlQuery);
            command.Parameters.Add(CreateParameter("@Version", version));
            command.Parameters.Add(CreateParameter("@ObjectType", "PROCEDURE"));
            command.Parameters.Add(CreateParameter("@ObjectName", procedureName));

            return command;
        }

        #endregion

        #region Internal properties

        protected override string DataBaseName
        {
            get
            {
                return _databaseName;
            }
        }

        #endregion

        #region Internal methods

        internal bool DeleteObject(StorageQueryParameter paramater)
        {
            bool success = true;
            try
            {
                if (ObjectExists(paramater))
                {
                    switch (paramater.Type)
                    {
                        case ObjectType.StoredProc:
                            IDbCommand command = CreateCommand(string.Format("DROP PROCEDURE [dbo].[{0}]", paramater.ObjectName));
                            ExecuteNonCommand(command);
                            break;
                        default:
                            success = false;
                            break;
                    }
                }
            }
            catch
            {
                success = false;
            }

            return success;
        }

        #endregion

        #region Public methods

        public override bool Initialize()
        {
            ReadSettings();

            // Get a connection string to the master db.
            ConnectionString = _masterConnectionString;

            try
            {
                bool exists = true;
                string sql = "sp_databases";

                using (IDbCommand command = CreateCommand(sql))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = ExecuteQueryCommand(command))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                exists = reader[0].ToString().Equals(DataBaseName);
                                if (exists)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!exists)
                {
                    sql = "CREATE DATABASE " + DataBaseName;

                    using (var command = CreateCommand(sql))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET ANSI_NULL_DEFAULT OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET ANSI_NULLS OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET ANSI_PADDING OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET ANSI_WARNINGS OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET ARITHABORT OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET AUTO_CLOSE OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET AUTO_CREATE_STATISTICS ON"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET AUTO_SHRINK OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET AUTO_UPDATE_STATISTICS ON"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET CURSOR_CLOSE_ON_COMMIT OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET CURSOR_DEFAULT  GLOBAL"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET CONCAT_NULL_YIELDS_NULL OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET NUMERIC_ROUNDABORT OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET QUOTED_IDENTIFIER OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET RECURSIVE_TRIGGERS OFF"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET  READ_WRITE"))
                    {
                        ExecuteNonCommand(command);
                    }

                    if (string.IsNullOrEmpty(_recoveryModel))
                    {
                        _recoveryModel = "SIMPLE";
                    }

                    if (_recoveryModel != "SIMPLE" && _recoveryModel != "FULL" && _recoveryModel != "BULK_LOGGED")
                    {
                        _recoveryModel = "SIMPLE";
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET RECOVERY " + _recoveryModel))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] SET  MULTI_USER"))
                    {
                        ExecuteNonCommand(command);
                    }

                    using (var command = CreateCommand("ALTER DATABASE [" + DataBaseName + "] COLLATE SQL_Latin1_General_CP1_CI_AS"))
                    {
                        ExecuteNonCommand(command);
                    }

                    sql = "USE [" + DataBaseName + "] ";

                    using (var command = CreateCommand(sql))
                    {
                        ExecuteNonCommand(command);
                    }
                }

                ConnectionString = _connectionString;

                _isInitialized = true;
            }
            catch (Exception)
            {
                ConnectionString = _connectionString;
                throw;
            }

            return _isInitialized;
        }

        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;
            return true;
        }

        public override object FindParameterValue(IDataParameter parameter)
        {
            return ((SqlParameter)parameter).Value;
        }

        public override bool ObjectExists(StorageQueryParameter paramater)
        {
            if (string.IsNullOrEmpty(paramater.ObjectName))
            {
                throw new ArgumentException("StorageQueryParameter.ObjectName must have a valid value");
            }

            // Database exceptions are handled via ExecuteScalarCommand -> HandleException.
            return Contains(paramater);
        }

        public override bool ObjectExists(string name, ObjectType type)
        {
            // Database exceptions are handled via ExecuteScalarCommand -> HandleException.
            return ObjectExists(new StorageQueryParameter { Type = type, ObjectName = name });
        }

        public override IDbCommand CreateCommand(string input)
        {
            return CreateCommand(input, true);
        }

        public override IDbCommand CreateCommand()
        {
            return CreateCommand(string.Empty);
        }

        public override IDbCommand CreateCommand(string input, bool newConnection)
        {
            if (newConnection)
            {
                return new SqlCommand(input, CreateConnection() as SqlConnection);
            }
            else
            {
                return new SqlCommand(input);
            }
        }

        public override void CreateOrUpdateStoredProcedure(string command, string objectName, string version)
        {
            if (string.IsNullOrEmpty(command) || string.IsNullOrEmpty(objectName) || string.IsNullOrEmpty(version))
            {
                throw new ArgumentException("Argument Exception when creating the Stored Procedure");
            }

            List<IDbCommand> commands = new List<IDbCommand>();
            StorageQueryParameter param = new StorageQueryParameter { Type = ObjectType.StoredProc, ObjectName = objectName };

            try
            {
                if (ObjectExists(param))
                {
                    if (!CheckProcedureVersion(objectName, version))
                    {
                        DeleteObject(param);

                        commands.Add(CreateCommand(command));
                        commands.Add(GetAddVersionToProcedureCommand(objectName, version));
                        ExecuteNonCommands(commands);
                    }
                }
                else
                {
                    commands.Add(CreateCommand(command));
                    commands.Add(GetAddVersionToProcedureCommand(objectName, version));
                    ExecuteNonCommands(commands);
                }
            }
            finally
            {
                if (commands.Count > 0)
                {
                    foreach (IDbCommand com in commands)
                    {
                        com.Dispose();
                    }
                }

                commands.Clear();
            }
        }

        #endregion

        #region Public properties

        public override string Name
        {
            get
            {
                return "StorageSql";
            }
        }

        public override bool IsInitialized
        {
            get { return _isInitialized; }
        }

        #endregion

        #region Protected Methods

        protected override IDataAdapter GetAdapter(IDbCommand com)
        {
            SqlDataAdapter adp = new SqlDataAdapter((SqlCommand)com);
            return adp;
        }

        protected override IDbConnection CreateConnection()
        {
            IDbConnection conn = new SqlConnection(ConnectionString);
            return conn;
        }

        protected override void CheckConnection(IDbConnection connection)
        {
            base.CheckConnection(connection);

            SetIsolationLevel(connection);
        }

        protected override IDataParameter GetParameter(string name, object val)
        {
            if (val == DBNull.Value)
            {
                SqlParameter sqlPara = new SqlParameter(name, SqlDbType.VarChar);
                sqlPara.Value = DBNull.Value;
                return sqlPara;
            }
            else
            {
                return new SqlParameter(name, val);
            }
        }

        #endregion
    }
}
