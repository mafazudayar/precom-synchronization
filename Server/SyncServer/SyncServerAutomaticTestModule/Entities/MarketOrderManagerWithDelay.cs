﻿using System;
using System.Threading;

namespace PreCom.Synchronization.Test.Entities
{
    public class MarketOrderManagerWithDelay : MarketOrderManager
    {
        private readonly TimeSpan _delayWhenUpdating;

        public MarketOrderManagerWithDelay(IOrderDao<MarketOrder> dao, TimeSpan delayWhenUpdating)
            : base(dao)
        {
            _delayWhenUpdating = delayWhenUpdating;
        }

        protected override void OnUpdate(ISynchronizable entity, UpdateArgs args)
        {
            base.OnUpdate(entity, args);
            Thread.Sleep(_delayWhenUpdating);
        }
    }
}
