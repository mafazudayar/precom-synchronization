﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using PreCom.Core.Modules;
using PreCom.Synchronization.Test.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test
{
    public class SynchronizedStorageBaseTest : SyncModuleTestBase
    {
        public SynchronizedStorageBaseTest(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
        }

        [ModuleTest(IsAsync = true)]
        public async Task Read_InSnapshotWithConcurrentUpdate_ShouldNotBeBlocking()
        {
            var updateDelay = TimeSpan.FromSeconds(10);
            var semaphore = new SemaphoreSlim(0, 1);

            // Update Entity A in task 1, Read Entity A in task 2. Task 2 should not be blocked by task 1.
            var updatingTask = Task.Run(() =>
            {
                // Cannot use async with transaction scope since it may continue on another thread than it was created on.
                using (SynchronizationModule.CreateTransaction(IsolationLevel.Serializable))
                {
                    CurrentOrder.Description = "updatingTask";
                    SynchronizationModule.AcquireUpdateLock<MarketOrder>(CurrentOrder.Id);
                    MarketOrderStorage.Update(CurrentOrder, new UpdateArgs(GetType().Name));
                    semaphore.Release();

                    // Wait again until second task has managed to read the data. Short sleep to make sure the other task has started running.
                    Thread.Sleep(100);
                    semaphore.Wait(updateDelay);
                }
            });

            var stopwatch = new Stopwatch();

            // Cannot use async with transaction scope since it may continue on another thread than it was created on.
            using (SynchronizationModule.CreateTransaction(IsolationLevel.Snapshot))
            {
                // Wait to make sure first task has started updating the entity.
                semaphore.Wait(TimeSpan.FromSeconds(10));
                stopwatch.Start();

                MarketOrderStorage.Read(new[] { CurrentOrder.Id });

                stopwatch.Stop();

                // Release so update task can complete.
                semaphore.Release();
            }

            var readDuration = stopwatch.Elapsed;

            AssertIsLessThan(readDuration, updateDelay, "Read should not be blocked by the sleeping update method");

            // await all tasks so we can get the exceptions thrown if any.
            await Task.WhenAll(updatingTask);
        }

        // Task1 locks order, the task2 tries to lock the order concurrently which should result in task2 waiting for task1 to complete.
        [ModuleTest(IsAsync = true)]
        public async Task Update_WhenUpdateLockIsAcquired_RunsAfterTransactionCompleted()
        {
            var task1LockTime = TimeSpan.FromSeconds(2);

            // Semaphores are used to control the sequence of the concurrent tasks so we can verify the results.
            var semaphoreTask1 = new SemaphoreSlim(0, 1);
            var semaphoreTask2 = new SemaphoreSlim(0, 1);

            var task1 = Task.Run(() =>
            {
                // Cannot use await with transaction scope since it may continue on another thread than it was created on.
                using (SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                {
                    SynchronizationModule.AcquireUpdateLock<MarketOrder>(CurrentOrder.Id);
                    semaphoreTask1.Release();

                    // Wait until the delay has elapsed or task2 has acquired the lock which would be an application error
                    // since task2 should be blocked until task1 has committed its transaction.
                    Thread.Sleep(100);
                    semaphoreTask2.Wait(task1LockTime);
                }
            });

            var stopwatch = new Stopwatch();

            // Cannot use await with transaction scope since it may continue on another thread than it was created on.
            using (SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
            {
                // Wait until task1 has acquired the lock.
                semaphoreTask1.Wait(2000);

                stopwatch.Start();

                SynchronizationModule.AcquireUpdateLock<MarketOrder>(CurrentOrder.Id);

                CurrentOrder.Description = "Task2";
                MarketOrderStorage.Update(CurrentOrder, new UpdateArgs(GetType().Name));

                stopwatch.Stop();

                // Release the lock so that task2 can continue.
                semaphoreTask2.Release();
            }

            var updateDuration = stopwatch.Elapsed;
            AssertGreaterThan(updateDuration, task1LockTime, "UpdateDuration should be longer than the time task1 holds its lock");

            await Task.WhenAll(task1);
        }

        [ModuleTest(IsAsync = true)]
        public async Task Update_ConcurrentUpdatesOnDifferentEntities_RunsInParallel()
        {
            // Set the minimum number of threads in order to reduce time waiting for pool to allocate new threads.
            ThreadPool.SetMinThreads(15, 15);
            int numberOfTasks = 10;
            var countdownEvent = new CountdownEvent(numberOfTasks);

            var tasks = new List<Task<Stopwatch>>();

            // Create a bunch of parallel tasks that try to update separate orders, they should not be blocking each other.
            for (int i = 0; i < numberOfTasks; i++)
            {
                int localIndex = i;
                var order = Orders[localIndex];
                tasks.Add(Task.Run(() =>
                {
                    using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                    {
                        SynchronizationModule.AcquireUpdateLock<MarketOrder>(order.Id);
                        Debug.WriteLine("Task" + localIndex + " AcquiredUpdateLock " + Environment.TickCount);

                        // Countdown event makes sure all tasks have acquired update locks before continuing.
                        countdownEvent.Signal();

                        // Long delay is useful when debugging, when test is running normally should not be an issue.
                        if (!countdownEvent.Wait(30000))
                        {
                            throw new Exception("Start wait timeout out");
                        }

                        order.Description = "Task" + localIndex + " " + Environment.TickCount;
                        order.Date = DateTime.Now;

                        var taskUpdateStopwatch = Stopwatch.StartNew();
                        MarketOrderStorage.Update(order, new UpdateArgs(GetType().Name));
                        taskUpdateStopwatch.Stop();

                        // Let each task sleep 1000ms before completing.
                        Thread.Sleep(1000);

                        scope.Complete();

                        return taskUpdateStopwatch;
                    }
                }));
            }

            // Wait for all threads to start before starting timer.
            countdownEvent.Wait(10000);

            var stopwatch = Stopwatch.StartNew();
            await Task.WhenAll(tasks);
            stopwatch.Stop();

            foreach (var task in tasks)
            {
                var taskUpdateTime = task.Result.Elapsed;

                Debug.WriteLine("TaskUpdateTime " + taskUpdateTime);
            }

            var totalUpdateTime = stopwatch.ElapsedMilliseconds;
            AssertIsLessThan(totalUpdateTime, 6000, "Update took long time, blocking might have occurred");
        }

        [ModuleTest(IsAsync = true)]
        public async Task Create_ConcurrentCreateOnDifferentEntities_RunsInParallel()
        {
            var createdOrders = new List<MarketOrder>();

            try
            {
                // Set the minimum number of threads in order to reduce time waiting for pool to allocate new threads.
                ThreadPool.SetMinThreads(15, 15);
                int numberOfTasks = 10;
                var countdownEvent = new CountdownEvent(numberOfTasks);

                var tasks = new List<Task<Stopwatch>>();

                // Create a bunch of parallel tasks that try to update separate orders, they should not be blocking each other.
                for (int i = 0; i < numberOfTasks; i++)
                {
                    int localIndex = i;
                    var order = MarketOrder.Create(GetType().Name, "Task " + localIndex);
                    createdOrders.Add(order);

                    tasks.Add(Task.Run(() =>
                    {
                        using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                        {
                            // Countdown event makes sure all tasks have acquired update locks before continuing.
                            countdownEvent.Signal();

                            // Long delay is useful when debugging, when test is running normally should not be an issue.
                            if (!countdownEvent.Wait(30000))
                            {
                                throw new Exception("Start wait timeout out");
                            }

                            Debug.WriteLine("Task" + localIndex + " AcquiredUpdateLock");

                            order.Description = "Task" + localIndex + " " + Environment.TickCount;
                            order.Date = DateTime.Now;

                            var taskCreateStopwatch = Stopwatch.StartNew();
                            MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));
                            taskCreateStopwatch.Stop();

                            // Let each task sleep 1000ms before completing.
                            Thread.Sleep(1000);

                            scope.Complete();

                            return taskCreateStopwatch;
                        }
                    }));
                }

                // Wait for all threads to start before starting timer.
                countdownEvent.Wait(10000);

                var stopwatch = Stopwatch.StartNew();
                await Task.WhenAll(tasks);
                stopwatch.Stop();

                foreach (var task in tasks)
                {
                    var taskCreateTime = task.Result.Elapsed;

                    Debug.WriteLine("TaskUpdateTime " + taskCreateTime);
                }

                var totalCreateTime = stopwatch.ElapsedMilliseconds;
                AssertIsLessThan(totalCreateTime, 5000, "Creation took long time, blocking might have occurred");
            }
            finally
            {
                using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                {
                    foreach (var order in createdOrders)
                    {
                        MarketOrderDao.DeleteOrder(order.Id);
                        MarketOrderDao.DeleteSyncReord(order.Id);
                        MarketOrderDao.DeleteSyncPropertyReord(order.Id);
                    }

                    scope.Complete();
                }
            }
        }

        [ModuleTest(IsAsync = true)]
        public async Task Delete_ConcurrentDeleteOnDifferentEntities_RunsInParallel()
        {
            // Set the minimum number of threads in order to reduce time waiting for pool to allocate new threads.
            ThreadPool.SetMinThreads(15, 15);
            int numberOfTasks = 10;
            var countdownEvent = new CountdownEvent(numberOfTasks);

            var tasks = new List<Task<Stopwatch>>();

            // Create a bunch of parallel tasks that try to update separate orders, they should not be blocking each other.
            for (int i = 0; i < numberOfTasks; i++)
            {
                int localIndex = i;
                var order = Orders[localIndex];

                tasks.Add(Task.Run(() =>
                {
                    using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                    {
                        SynchronizationModule.AcquireUpdateLock<MarketOrder>(order.Id);
                        Debug.WriteLine("Task" + localIndex + " AcquiredUpdateLock " + Environment.TickCount);

                        // Countdown event makes sure all tasks have acquired update locks before continuing.
                        countdownEvent.Signal();

                        // Long delay is useful when debugging, when test is running normally should not be an issue.
                        if (!countdownEvent.Wait(30000))
                        {
                            throw new Exception("Start wait timeout out");
                        }

                        Debug.WriteLine("Task" + localIndex + " AcquiredUpdateLock");

                        order.Description = "Task" + localIndex + " " + Environment.TickCount;
                        order.Date = DateTime.Now;

                        var taskDeleteStopwatch = Stopwatch.StartNew();
                        MarketOrderStorage.Delete(order.Id, new DeleteArgs(GetType().Name));
                        taskDeleteStopwatch.Stop();

                        // Let each task sleep 1000ms before completing.
                        Thread.Sleep(1000);

                        scope.Complete();

                        return taskDeleteStopwatch;
                    }
                }));
            }

            // Wait for all threads to start before starting timer.
            countdownEvent.Wait(10000);

            var stopwatch = Stopwatch.StartNew();
            await Task.WhenAll(tasks);
            stopwatch.Stop();

            foreach (var task in tasks)
            {
                var taskDeleteTime = task.Result.Elapsed;

                Debug.WriteLine("TaskUpdateTime " + taskDeleteTime);
            }

            var totalDeleteTime = stopwatch.ElapsedMilliseconds;
            AssertIsLessThan(totalDeleteTime, 3000, "Creation took long time, blocking might have occurred");
        }
    }
}
