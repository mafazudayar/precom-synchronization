﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Test.Utils;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test
{
    public class StorageTransactionScopeTest : SyncModuleTestBase
    {
        public StorageBase Storage { get; set; }
        public ILog Log { get; set; }

        public StorageTransactionScopeTest(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
            Storage = Application.Instance.Storage;
            Log = Application.Instance.CoreComponents.Get<ILog>();
        }

        [ModuleTest]
        public void Complete_WhenNotReadCommitted_ChangesToReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                AssertAreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                AssertAreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be snapshot since we specify it in the transaction scope.");
                AssertAreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            AssertAreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [ModuleTest]
        public void Complete_WhenReadCommitted_RemainsInReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                AssertAreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                AssertAreEqual(transactionScopeLevel, IsolationLevel.ReadCommitted, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                AssertAreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have remain as read committed");
            AssertAreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [ModuleTest]
        public void Dispose_WhenNotReadCommitted_ChangesToReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                AssertAreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                AssertAreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be snapshot since we specify it in the transaction scope.");
                AssertAreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            AssertAreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [ModuleTest]
        public void Dispose_WhenReadCommitted_RemainsInReadCommitted()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.ReadCommitted))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                AssertAreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                AssertAreEqual(transactionScopeLevel, IsolationLevel.ReadCommitted, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                AssertAreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have remain as read committed");
            AssertAreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [ModuleTest]
        public void Complete_WhenHavingOuterTransaction_DoesntResetTheConnectionIsolationLevel()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            short initialiSpid = GetSpid();

            using (new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
            {
                var transactionScopeLevel = Transaction.Current.IsolationLevel;
                var dbLevelInTransactionScope = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                short transactionScopeSpid = GetSpid();

                using (var scope2 = new StorageTransactionScope(Storage, Log, IsolationLevel.Snapshot))
                {
                    short transactionScopeSpid2 = GetSpid();

                    AssertAreEqual(transactionScopeSpid2, transactionScopeSpid, "Should have same spid in order for test to be reliable");

                    scope2.Complete();
                }

                var dbLevelInTransactionScopeAfter = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

                AssertAreEqual(initialiSpid, transactionScopeSpid, "Should have same spid in order for test to be reliable");
                AssertAreEqual(transactionScopeLevel, IsolationLevel.Snapshot, "Initial level should be ReadCommitted since we specify it in the transaction scope.");
                AssertAreEqual(transactionScopeLevel, dbLevelInTransactionScope, "Expected level from db and transaction scope to be the same.");
                AssertAreEqual(dbLevelInTransactionScopeAfter, dbLevelInTransactionScope, "Expected level to be same as before inner scope.");
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            short finalSpid = GetSpid();

            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed");
            AssertAreEqual(initialiSpid, finalSpid, "Should have same spid in order for test to be reliable");
        }

        [ModuleTest]
        public void Complete_WithSerializable_CommitsDataToDb()
        {
            MarketOrder order;

            using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.Serializable))
            {
                order = MarketOrder.Create(GetType().Name, "AutoTest");
                MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));

                var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                AssertAreEqual(transactionScopeLevel, IsolationLevel.Serializable, "Scope isolation level should be Serializable since we specify it in the constructor.");

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            var orderFromDb = (MarketOrder)MarketOrderStorage.Read(new[] { order.Id }).First();

            AssertAreEqual(orderFromDb.Id, order.Id, "Order we just created should be the same we read from database.");
            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");
        }

        [ModuleTest]
        public void Constructor_WithRepeatableRead_SetIsolationLevelToRepeatableRead()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            var semaphore = new SemaphoreSlim(0, 1);
            var task = Task.Run(() =>
            {
                using (var scope = new StorageTransactionScope(Storage, Log, IsolationLevel.RepeatableRead))
                {
                    // Read order which should make it locked in database.
                    MarketOrderStorage.Read(new[] { Orders[0].Id });
                    semaphore.Release();
                    Thread.Sleep(5000);

                    var transactionScopeLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
                    AssertAreEqual(transactionScopeLevel, IsolationLevel.RepeatableRead, "Scope isolation level should be RepeatableRead since we specify it in the constructor.");

                    scope.Complete();
                }
            });

            Orders[0].Description = "Updated";

            var sw = new Stopwatch();
            using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
            {
                semaphore.Wait();

                sw.Start();

                // Update should be blocked by the previous transaction with RepeatableRead.
                MarketOrderStorage.Update(Orders[0], new UpdateArgs(GetType().Name));
                sw.Stop();

                scope.Complete();
            }

            var finalLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);

            AssertGreaterThan(sw.Elapsed, TimeSpan.FromSeconds(5), "Update should be blocked by the read operation of repeatable read isolation level.");
            AssertAreEqual(finalLevel, IsolationLevel.ReadCommitted, "Level should have been changed back to read committed.");

            // Wait until complete so we dont affect other tests.
            task.Wait();
        }

        [ModuleTest]
        public void Dispose_WhenTransactionHasTimeout_ShouldNotThrowException()
        {
            var initialLevel = TransactionUtil.GetSqlConnectionIsolationLevel(Storage);
            AssertAreEqual(initialLevel, IsolationLevel.ReadCommitted, "Default should be read committed");

            Exception exception = null;
            StorageTransactionScope scope = null;
            try
            {
                scope = new StorageTransactionScope(Storage,
                                                    Log,
                                                    IsolationLevel.RepeatableRead,
                                                    TransactionScopeOption.Required,
                                                    TimeSpan.FromSeconds(1));

                Thread.Sleep(1500);
            }
            finally
            {
                try
                {
                    if (scope != null)
                    {
                        scope.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            }

            object time;
            using (var cmd = Storage.CreateCommand("SELECT GetDate()"))
            {
                time = Storage.ExecuteScalarCommand(cmd);
            }

            AssertAreEqual(exception, null, "No exception should have been throw");
            AssertIsTrue(time != null, "Time should not be null since database commands should work");
        }

        private short GetSpid()
        {
            using (var cmd = Storage.CreateCommand("SELECT @@SPID"))
            {
                return (short)Storage.ExecuteScalarCommand(cmd);
            }
        }
    }
}
