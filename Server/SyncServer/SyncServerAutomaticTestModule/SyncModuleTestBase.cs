﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Test.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test
{
    public abstract class SyncModuleTestBase : ModuleTestBase
    {
        public SynchronizationServerBase SynchronizationModule { get; set; }
        public MarketOrderManager MarketOrderStorage { get; set; }
        public MarketOrderDao MarketOrderDao { get; set; }
        public ILog Log { get; set; }        

        public MarketOrder CurrentOrder { get; set; }
        public List<MarketOrder> Orders { get; set; }

        protected SyncModuleTestBase(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
        {
            SynchronizationModule = module;
            MarketOrderDao = marketOrderDao;
            MarketOrderStorage = marketOrderStorage;
            Log = log;

            Orders = new List<MarketOrder>();
        }

        protected override void TestInitialize()
        {
            base.TestInitialize();
            Orders.Clear();

            using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
            {
                var order = MarketOrder.Create(GetType().Name, "AutoTestOrder");
                MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));
                CurrentOrder = order;

                var desc = Guid.NewGuid().ToString();
                for (int i = 0; i < 10; i++)
                {
                    var order2 = MarketOrder.Create(GetType().Name, desc);
                    MarketOrderStorage.Create(order2, new CreateArgs(GetType().Name));
                    Orders.Add(order2);
                }

                scope.Complete();
            }
        }

        protected override void TestCleanUp()
        {
            base.TestCleanUp();

            using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
            {
                if (CurrentOrder != null)
                {
                    MarketOrderStorage.Delete(CurrentOrder.Id, new DeleteArgs(GetType().Name));

                    // Also delete the records from the actual sync module. Otherwise they are only marked as deleted.
                    MarketOrderDao.DeleteSyncReord(CurrentOrder.Id);
                    MarketOrderDao.DeleteSyncPropertyReord(CurrentOrder.Id);

                    foreach (var marketOrder in Orders)
                    {
                        try
                        {
                            MarketOrderStorage.Delete(marketOrder.Id, new DeleteArgs(GetType().Name));
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Error deleting entity, one of the tests might have deleted it. Exception: " + ex.ToString());
                        }

                        // Also delete the records from the actual sync module. Otherwise they are only marked as deleted.
                        MarketOrderDao.DeleteSyncReord(marketOrder.Id);
                        MarketOrderDao.DeleteSyncPropertyReord(marketOrder.Id);
                    }
                }

                scope.Complete();
            }
        }

        protected Client CreateNewClient()
        {
            return CreateNewClient(1, "AutoTestClientId", "WindowsMobileOrPC");
        }

        protected Client CreateNewClient(int credentialId, string clientId, string platformType)
        {
            return new Client()
            {
                CredentialId = credentialId,
                ClientID = clientId,
                MetaData = new MetaData() { Platform = new PlatformMetaData() { Type = platformType } }
            };
        }
    }
}
