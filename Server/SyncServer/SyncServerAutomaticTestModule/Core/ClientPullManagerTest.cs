﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Test.Mocks;
using PreCom.Synchronization.Test.Utils;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test.Core
{
    public class ClientPullManagerTest : SyncModuleTestBase
    {
        internal ClientPullManager<MarketOrder> MarketOrderPullManager { get; set; }
        internal MockDispatcher MockDispatcher { get; set; }

        public ClientPullManagerTest(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
            MockDispatcher = new MockDispatcher();

            MarketOrderPullManager = new ClientPullManager<MarketOrder>(MarketOrderStorage,
                MockDispatcher,
                SynchronizationModule as SynchronizationModule,
                1000000,
                Log);
        }

        protected override void TestInitialize()
        {
            base.TestInitialize();
            MockDispatcher.SentItems.Clear();
        }

        [ModuleTest]
        public void PullRequestHandler_WhenDataAvailable_ReturnsMatchingData()
        {
            var description = Guid.NewGuid().ToString();

            RunWithOrders(10,
                i => MarketOrder.Create(GetType().Name, description),
                orders =>
                {
                    var filter = new EntityFilter()
                    {
                        FilterList = { new Filter(FilterOperator.EqualTo, "Description", description) }
                    };

                    var pullRequest = CreateNewMarketOrderPullRequest(string.Empty, filter);
                    var client = CreateNewClient();
                    var requestInfo = new RequestInfo(client, new NameValueCollection(), null);

                    MockDispatcher.ReceivePullRequest(pullRequest, requestInfo);

                    AssertIsTrue(MockDispatcher.SentItems.Count > 0, "Should contain sent data");

                    foreach (SentItem sentItem in MockDispatcher.SentItems)
                    {
                        var pullResponse = (PullResponse)sentItem.Entity;
                        foreach (var synchronizedObject in pullResponse.SynchronizedObjects)
                        {
                            // Verify that we have exactly matching item.
                            AssertIsTrue(
                                orders.Single(m => m.Id == synchronizedObject.Guid) != null,
                                "The list of synchronized object to be sent should contain all orders previously created");
                        }
                    }
                });
        }

        [ModuleTest]
        public void PullRequest_WhenDoingLongRunningUpdate_ShouldNotWaitForUpdateToComplete()
        {
            var description = Guid.NewGuid().ToString();

            RunWithOrders(10,
                i => MarketOrder.Create(GetType().Name, description),
                orders =>
                {
                    var filter = new EntityFilter()
                    {
                        FilterList = { new Filter(FilterOperator.EqualTo, "Description", description) }
                    };

                    var pullRequest = CreateNewMarketOrderPullRequest(string.Empty, filter);
                    var client = CreateNewClient();
                    var requestInfo = new RequestInfo(client, new NameValueCollection(), null);

                    var updateSemaphore = new SemaphoreSlim(0, 1);
                    var exitSemaphore = new SemaphoreSlim(0, 1);

                    var task = Task.Run(() =>
                    {
                        var orderToUpdate = (MarketOrder)orders.First().Clone();
                        using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.RepeatableRead))
                        {
                            SynchronizationModule.AcquireUpdateLock<MarketOrder>(orderToUpdate.Id);
                            orderToUpdate.Description = "Updated";
                            MarketOrderStorage.Update(orderToUpdate, new UpdateArgs(GetType().Name));

                            // Signal to notify that we have started updating so main thread can continue to do pull request.
                            updateSemaphore.Release();

                            // Simulate long running operation.
                            exitSemaphore.Wait(5000);
                            scope.Complete();
                        }
                    });

                    // Wait until the update has started.
                    updateSemaphore.Wait(5000);

                    var stopwatch = Stopwatch.StartNew();
                    MockDispatcher.ReceivePullRequest(pullRequest, requestInfo);
                    stopwatch.Stop();

                    // Release so that update task exits, we have already verified the results here anyway.
                    exitSemaphore.Release();

                    Debug.WriteLine("Waiting for task completion so other tests are not affected.");
                    task.Wait();
                    AssertIsTrue(((PullResponse)MockDispatcher.SentItems[0].Entity).SynchronizedObjects.Count == 10, "PullResponse should match the 10 created orders since we are reading a snapshot");
                    AssertIsTrue(stopwatch.ElapsedMilliseconds < 5000, "Should run faster than the updating task timeperiod, otherwise blocking might have occurred");
                });
        }

        [ModuleTest]
        public void PullRequest_5000ExistingEntitiesWithKnowledge_ShouldReturn5000Entities()
        {
            var description = Guid.NewGuid().ToString();
            RunWithOrders(50,
                i => MarketOrder.Create(GetType().Name, string.Concat(description, " ", i.ToString())),
                orders =>
                {
                    var metadataList = new List<SyncMetadata>();

                    foreach (var order in orders)
                    {
                        metadataList.Add(new SyncMetadata
                        {
                            ChangeId = Guid.NewGuid(),
                            Guid = order.Guid,
                            Status = (int)Operation.Updated,
                            Version = 1000
                        });
                    }

                    string knowledge = XSerializer.SerializeSyncEntity(metadataList, new List<PropertyInfo>());

                    var entityFilter = new EntityFilter();
                    var nonExistingOrderId = Guid.NewGuid();
                    entityFilter.FilterList.Add(new Filter(FilterOperator.NotEqualTo, "Id", nonExistingOrderId));

                    var pullRequest = CreateNewMarketOrderPullRequest(knowledge, entityFilter);
                    var client = CreateNewClient();
                    var requestInfo = new RequestInfo(client, new NameValueCollection(), null);

                    var watch = Stopwatch.StartNew();
                    MockDispatcher.ReceivePullRequest(pullRequest, requestInfo);

                    watch.Stop();
                    Debug.WriteLine("PullRequest elapsed time" + watch.Elapsed.TotalMilliseconds);

                    // Create a list containing all items returned by the ClientPullManager.
                    // Then check that we got back all items that we know are in the collection.
                    var sentItems = new List<SyncEntityData>();

                    foreach (SentItem sentItem in MockDispatcher.SentItems)
                    {
                        var pullResponse = (PullResponse)sentItem.Entity;
                        sentItems.AddRange(pullResponse.SynchronizedObjects);
                    }

                    foreach (var order in orders)
                    {
                        // Verify that we have exactly matching item.
                        AssertIsTrue(
                            sentItems.Single(m => m.Guid == order.Id) != null,
                            "We should at least have gotten a list of all orders we just created.");
                    }
                });
        }

        // Empty filter should pull all items of the type.
        private static PullRequest CreateNewMarketOrderPullRequest(string knowledge, EntityFilter filter)
        {
            return new PullRequest
            {
                Knowledge = knowledge,
                ObjectType = typeof(MarketOrder).Name,
                SessionGuid = Guid.NewGuid(),
                Filter = filter,
                SyncType = 0, // SyncEventType.ClientPullRequest
                PlatformType = "WindowsMobileOrPc" // we need to get the platform type from the framework
            };
        }

        private List<MarketOrder> CreateOrders(int ordersToCreate, Func<int, MarketOrder> orderCreator)
        {
            var createdOrders = new List<MarketOrder>();

            // Initialize the test data.
            using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted, TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
            {
                for (int i = 0; i < ordersToCreate; i++)
                {
                    var order = orderCreator(i);
                    createdOrders.Add(order);
                    MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));
                }

                scope.Complete();
            }

            return createdOrders;
        }

        private void DeleteOrders(IEnumerable<MarketOrder> orders)
        {
            // Clean up the created orders.
            using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted, TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
            {
                foreach (var order in orders)
                {
                    MarketOrderStorage.Delete(order.Id, new DeleteArgs(GetType().Name));
                    MarketOrderDao.DeleteSyncReord(order.Id);
                    MarketOrderDao.DeleteSyncPropertyReord(order.Id);
                }

                scope.Complete();
            }
        }

        // Provides a context with orders for tests to run in to reduce the amount of duplicated code.
        private void RunWithOrders(int ordersToCreate, Func<int, MarketOrder> orderCreator, Action<List<MarketOrder>> runCallback)
        {
            var createdOrders = CreateOrders(ordersToCreate, orderCreator);

            try
            {
                runCallback(createdOrders);
            }
            finally
            {
                DeleteOrders(createdOrders);
            }
        }
    }
}
