﻿using System;
using System.Collections.Generic;
using System.Transactions;
using PreCom.Core.Modules;
using PreCom.Synchronization.Test.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test.Core
{
    public class SynchronizationModuleTest : SyncModuleTestBase
    {
        public SynchronizationModuleTest(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
        }

        [ModuleTest]
        public void AcquireUpdateLock_OnUnlockedEntity_ReturnOne()
        {
            var rowsLocked = SynchronizationModule.AcquireUpdateLock<MarketOrder>(CurrentOrder.Id);

            AssertIsTrue(rowsLocked == 1, "Number of rows locked should be 1");
        }

        [ModuleTest]
        public void AcquireUpdateLock_OnUnlockedEntities_ReturnNumberOfLockedEntities()
        {
            // Rollback changes after test.
            using (SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
            {
                var entityIds = new List<Guid>();
                for (int i = 0; i < 10; i++)
                {
                    var order = MarketOrder.Create(GetType().Name, "AutoTestOrder");
                    MarketOrderStorage.Create(order, new CreateArgs(GetType().Name));

                    entityIds.Add(order.Id);
                }

                var rowsLocked = SynchronizationModule.AcquireUpdateLock<MarketOrder>(entityIds);

                AssertIsTrue(rowsLocked == 10, "Number of rows locked should be 10");
            }
        }
    }
}
