﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Test.Entities;
using PreCom.Synchronization.Test.Mocks;
using PreCom.Synchronization.Test.Utils;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test.Core
{
    public class ClientPushManagerTest : SyncModuleTestBase
    {
        internal MockDispatcher MockDispatcher { get; set; }

        internal ClientPushManager<MarketOrder> MarketOrderPushManager { get; set; }

        public ClientPushManagerTest(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
            MockDispatcher = new MockDispatcher();

            MarketOrderPushManager = new ClientPushManager<MarketOrder>(MarketOrderStorage, MockDispatcher, (SynchronizationModule)SynchronizationModule, 1000000, Log);
            // Register dummy listener to avoid NullReferenceException.
            MarketOrderPushManager.EntitiesChanged += (sender, argses) => { };
        }

        protected override void TestInitialize()
        {
            base.TestInitialize();

            MockDispatcher.SentItems.Clear();
        }

        [ModuleTest]
        public void PushRequestHandler_WhenDataAvailable_UpdatesMatchingData()
        {
            for (int i = 0; i < Orders.Count; i++)
            {
                var order = Orders[i];
                order.Description = "Updated " + i;
                order.Creator = "Test " + Environment.TickCount;
            }

            var client = CreateNewClient();
            var requestInfo = new RequestInfo(client, new NameValueCollection(), null);
            var changedProperties = new List<string>() { "Description" };

            var pushRequest = CreateUpdatePushRequest<MarketOrder>(Orders, changedProperties, 999);

            MockDispatcher.ReceivePushRequest(pushRequest, requestInfo);

            AssertIsTrue(((PushResponse)MockDispatcher.SentItems[0].Entity).AcceptedObjects.Count == Orders.Count, "Should have accepted all data.");

            for (int i = 0; i < Orders.Count; i++)
            {
                var orderFromStorage = (MarketOrder)MarketOrderStorage.Read(new[] { Orders[i].Id }).Single();

                AssertAreEqual(orderFromStorage.Description, "Updated " + i, "Should contain updated value");
            }
        }

        [ModuleTest]
        public void PushRequestHandler_WhenConcurrentPushRequestsOnEntity_RunsInParallel()
        {
            var marketOrderStorage = new MarketOrderManagerWithDelay(MarketOrderDao, TimeSpan.FromSeconds(5));
            marketOrderStorage.AcquireUpdateLock = SynchronizationModule.AcquireUpdateLock<MarketOrder>;
            var mockDispatcher = new MockDispatcher();

            // ReSharper disable once UnusedVariable
            var marketOrderPushManager = new ClientPushManager<MarketOrder>(marketOrderStorage, mockDispatcher, (SynchronizationModule)SynchronizationModule, 1000000, Log);
            // Register dummy listener to avoid NullReferenceException.
            marketOrderPushManager.EntitiesChanged += (sender, argses) => { };

            var order = Orders[0];
            var orderCopy = (MarketOrder)order.Clone();

            order.Description = "Task1";
            orderCopy.Description = "MainThread";

            var client1 = CreateNewClient(1, "AutoTestClientId-1", "WindowsMobileOrPC");
            var client2 = CreateNewClient(2, "AutoTestClientId-2", "WindowsMobileOrPC");

            var requestInfo1 = new RequestInfo(client1, new NameValueCollection(), null);
            var requestInfo2 = new RequestInfo(client2, new NameValueCollection(), null);

            var changedProperties = new List<string>() { "Description" };

            var pushRequest1 = CreateUpdatePushRequest<MarketOrder>(new[] { order }, changedProperties, 999);
            var pushRequest2 = CreateUpdatePushRequest<MarketOrder>(new[] { orderCopy }, changedProperties, 1000);

            var semaphore = new SemaphoreSlim(0, 1);
            var task = Task.Run(() =>
            {
                semaphore.Release();
                mockDispatcher.ReceivePushRequest(pushRequest1, requestInfo1);
            });

            semaphore.Wait(10000);
            var stopwatch = Stopwatch.StartNew();

            mockDispatcher.ReceivePushRequest(pushRequest2, requestInfo2);

            stopwatch.Stop();

            Debug.WriteLine("Waiting for task completion so other tests are not affected.");
            task.Wait();
            AssertIsTrue(stopwatch.Elapsed < TimeSpan.FromSeconds(10000), "Should be less than sum of both update times if ran in parallell");
        }

        [ModuleTest]
        public void PushRequestHandler_PushingOneEntity_SavesEntitiesInStorage()
        {
            PushRequestHandler_PushingXEntities_SavesEntitiesInStorage(1);
        }

        [ModuleTest]
        public void PushRequestHandler_Pushing10000Entities_SavesEntitiesInStorage()
        {
            PushRequestHandler_PushingXEntities_SavesEntitiesInStorage(10000);
        }

        private void PushRequestHandler_PushingXEntities_SavesEntitiesInStorage(int numberOfEntities)
        {
            var createdOrders = new List<MarketOrder>(numberOfEntities);
            var creator = GetType().Name;
            for (int i = 0; i < numberOfEntities; i++)
            {
                var description = Guid.NewGuid().ToString();
                createdOrders.Add(MarketOrder.Create(creator, description));
            }

            try
            {
                var client = CreateNewClient();
                var requestInfo = new RequestInfo(client, new NameValueCollection(), null);

                var pushRequest = CreateCreatedPushRequest(createdOrders);

                var sw = Stopwatch.StartNew();

                MockDispatcher.ReceivePushRequest(pushRequest, requestInfo);

                sw.Stop();
                Debug.WriteLine("Duration of push request processing[sec]: " + sw.Elapsed.TotalSeconds);

                var acceptedEntities = new List<SyncMetadata>(numberOfEntities);

                foreach (var sentItem in MockDispatcher.SentItems)
                {
                    var pushResponse = (PushResponse)sentItem.Entity;
                    acceptedEntities.AddRange(pushResponse.AcceptedObjects);
                }

                AssertAreEqual(acceptedEntities.Count, createdOrders.Count, "Should have accepted all data.");

                foreach (MarketOrder t in createdOrders)
                {
                    var orderFromStorage = (MarketOrder)MarketOrderStorage.Read(new[] { t.Id }).Single();

                    AssertAreEqual(orderFromStorage.Description, t.Description, "Should contain created value");
                }
            }
            finally
            {
                using (var scope = SynchronizationModule.CreateTransaction(IsolationLevel.ReadCommitted))
                {
                    foreach (var order in createdOrders)
                    {
                        MarketOrderDao.DeleteOrder(order.Id);
                        MarketOrderDao.DeleteSyncReord(order.Id);
                        MarketOrderDao.DeleteSyncPropertyReord(order.Id);
                    }

                    scope.Complete();
                }
            }
        }

        private PushRequest CreateUpdatePushRequest<T>(IEnumerable<T> entities, List<string> changedProperties, int version)
            where T : ISynchronizable
        {
            var pushRequest = new PushRequest();

            foreach (var entity in entities)
            {
                var syncEntityData = new SyncEntityData
                {
                    Guid = entity.Guid,
                    Version = version, // Set a high version that the server will not have in its storage.
                    Status = (int)Operation.Updated,
                    ChangeId = Guid.NewGuid()
                };

                foreach (var property in changedProperties)
                {
                    syncEntityData.ChangedPropertyNames.Add(property);
                }

                IEnumerable<PropertyInfo> allProperties = PropertyHelper.GetProperties(typeof(MarketOrder));

                syncEntityData.Object = XSerializer.SerializeSyncEntity(entity, PropertyHelper.GetExcludedProperties(allProperties, changedProperties));

                pushRequest.SynchronizedObjects.Add(syncEntityData);
            }

            pushRequest.SessionGuid = Guid.NewGuid();
            pushRequest.ObjectType = typeof(MarketOrder).Name;
            pushRequest.IsSplit = false;
            pushRequest.TotalNumberOfPackets = 0;
            pushRequest.PacketNumber = 0;

            return pushRequest;
        }

        private PushRequest CreateCreatedPushRequest<T>(IEnumerable<T> entities)
            where T : ISynchronizable
        {
            var pushRequest = new PushRequest();

            foreach (var entity in entities)
            {
                var syncEntityData = new SyncEntityData
                {
                    Guid = entity.Guid,
                    Version = 0, // Set a high version that the server will not have in its storage.
                    Status = (int)Operation.Created,
                    ChangeId = Guid.NewGuid()
                };

                IEnumerable<PropertyInfo> allProperties = PropertyHelper.GetProperties(typeof(MarketOrder)).ToList();

                foreach (var property in allProperties)
                {
                    syncEntityData.ChangedPropertyNames.Add(property.Name);
                }

                // All properties should be serialized since it is created.
                syncEntityData.Object = XSerializer.SerializeSyncEntity(entity, new List<PropertyInfo>());

                pushRequest.SynchronizedObjects.Add(syncEntityData);
            }

            pushRequest.SessionGuid = Guid.NewGuid();
            pushRequest.ObjectType = typeof(MarketOrder).Name;
            pushRequest.IsSplit = false;
            pushRequest.TotalNumberOfPackets = 0;
            pushRequest.PacketNumber = 0;

            return pushRequest;
        }
    }
}
