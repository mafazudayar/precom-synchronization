﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Test.Core;
using PreCom.Synchronization.Test.Data;
using PreCom.Synchronization.Test.Utils;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test
{
    /// <summary>
    /// This class is a substitute to unit tests since PreCom Server currently does not work very well will unit tests.
    /// !! When that has improved we should convert these test cases to proper unit tests !!
    /// </summary>
    public class SyncServerAutomaticTestModule : ModuleBase
    {
        private bool _isInitialized;
        private Application _application;
        private SynchronizationServerBase _syncModule;
        private MarketOrderManager _marketOrderStorage;
        private MarketOrderDao _marketOrderDao;
        private ILog _log;

        public override bool IsInitialized
        {
            get { return _isInitialized; }
        }

        public override bool Initialize()
        {
            _application = Application.Instance;
            _syncModule = _application.Modules.Get<SynchronizationServerBase>();
            _log = _application.CoreComponents.Get<ILog>();
            _marketOrderDao = new MarketOrderDao(_application.Storage, new LogUtil(this, _log));
            _marketOrderDao.CreateOrderTable();
            _marketOrderStorage = new MarketOrderManager(_marketOrderDao);
            _syncModule.RegisterStorage<MarketOrder>(_marketOrderStorage);

            Task.Run(new Func<Task>(RunAutomaticTestsAsync));

            _isInitialized = true;
            return true;
        }

        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;
            return true;
        }

        /// <summary>
        /// This is the launcher of all the automatic tests.
        /// </summary>
        /// <returns>Task that contains information about the execution.</returns>
        public async Task RunAutomaticTestsAsync()
        {
            while (!_application.IsInitialized)
            {
                await Task.Delay(100);
            }

            var tests = new List<ModuleTestBase>();
            tests.Add(new StorageTransactionScopeTest(_syncModule, _marketOrderDao, _marketOrderStorage, _log));
            tests.Add(new SynchronizationModuleTest(_syncModule, _marketOrderDao, _marketOrderStorage, _log));
            tests.Add(new SyncDaoTests(_syncModule, _marketOrderDao, _marketOrderStorage, _log));
            tests.Add(new ClientPullManagerTest(_syncModule, _marketOrderDao, _marketOrderStorage, _log));
            tests.Add(new ClientPushManagerTest(_syncModule, _marketOrderDao, _marketOrderStorage, _log));
            tests.Add(new SynchronizedStorageBaseTest(_syncModule, _marketOrderDao, _marketOrderStorage, _log));

            foreach (var test in tests)
            {
                await test.RunTests();
            }

            var totalTests = 0;
            var passedTests = 0;
            foreach (var test in tests)
            {
                passedTests += test.PassedTests;
                totalTests += test.PassedTests + test.FailedTests;
            }

            Console.WriteLine("Summary: {0} of total {1} passed", passedTests, totalTests);
        }
    }
}
