﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PreCom.Synchronization.Test.Utils
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ModuleTestAttribute : Attribute
    {
        public bool IsAsync { get; set; }
    }

    public abstract class ModuleTestBase
    {
        public int PassedTests { get; set; }
        public int FailedTests { get; set; }

        public virtual async Task RunTests()
        {
            ConsoleWriteLine(ConsoleColor.White, "Running tests for {0}", GetType().Name);

            var testCases = GetType().GetMethods().Where(m => Attribute.IsDefined(m, typeof(ModuleTestAttribute)));

            foreach (var methodInfo in testCases)
            {
                var moduleTest = methodInfo.GetCustomAttributes(typeof(ModuleTestAttribute), true).FirstOrDefault() as ModuleTestAttribute;
                MethodInfo info = methodInfo;

                // ReSharper disable once PossibleNullReferenceException - We have already made sure the methods have this attribute.
                if (!moduleTest.IsAsync)
                {
                    RunTest(info.Name, () => info.Invoke(this, null));
                }
                else
                {
                    await RunTestAsync(info.Name, () => (Task)info.Invoke(this, null));
                }
            }

            ConsoleWriteLine(ConsoleColor.White, "Tests completed for {0}. Passed {1}/{2}.", GetType().Name, PassedTests, PassedTests + FailedTests);
        }

        protected virtual void TestInitialize()
        {
        }

        protected virtual void TestCleanUp()
        {
        }

        protected void RunTest(string methodName, Action testMethod)
        {
            if (testMethod == null)
            {
                throw new ArgumentNullException("testMethod");
            }

            TestRunner(methodName, testMethod, null).Wait();
        }

        protected void RunTest(Action testMethod)
        {
            RunTest(testMethod.Method.Name, testMethod);
        }

        protected async Task RunTestAsync(Func<Task> testMethod)
        {
            await RunTestAsync(testMethod.Method.Name, testMethod);
        }

        protected async Task RunTestAsync(string methodName, Func<Task> testMethod)
        {
            if (testMethod == null)
            {
                throw new ArgumentNullException("testMethod");
            }

            await TestRunner(methodName, null, testMethod);
        }

        private async Task TestRunner(string methodName, Action testMethod, Func<Task> testMethodAsync)
        {
            try
            {
                TestInitialize();
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.Message);
            }

            var sw = new Stopwatch();
            bool testPassed = false;
            string assertMessage = null;
            Exception testException = null;

            try
            {
                if (testMethod != null)
                {
                    Console.WriteLine("Running " + methodName);

                    sw.Start();
                    testMethod();
                }
                else
                {
                    Console.WriteLine("Running " + methodName);

                    sw.Start();
                    await testMethodAsync();
                }

                testPassed = true;
                PassedTests += 1;
                sw.Stop();
            }
            catch (Exception ex)
            {
                sw.Stop();
                FailedTests += 1;

                if (ex is AssertException)
                {
                    assertMessage = ex.Message;
                }
                else if (ex.InnerException is AssertException)
                {
                    assertMessage = ex.InnerException.Message;
                }
                else
                {
                    testException = ex;
                }
            }

            WriteTestResult(testPassed, sw.Elapsed, assertMessage, testException);

            try
            {
                TestCleanUp();
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.Message);
            }
        }

        private void WriteTestResult(bool passed, TimeSpan duration, string assertMessage = null, Exception ex = null)
        {
            Console.Write("Result: ");

            if (passed)
            {
                ConsoleWrite(ConsoleColor.Green, "Passed");
            }
            else
            {
                ConsoleWrite(ConsoleColor.Red, "Failed");
            }

            Console.WriteLine(", Duration: {0:mm}:{0:ss}.{0:ffff}", duration);

            if (!passed)
            {
                if (!string.IsNullOrEmpty(assertMessage))
                {
                    ConsoleWriteLine(ConsoleColor.Cyan, assertMessage);
                }

                if (ex != null)
                {
                    Console.WriteLine("Exception:");
                    ConsoleWriteLine(ConsoleColor.DarkRed, ex.ToString());
                }
            }
        }

        public void ConsoleWrite(ConsoleColor color, string formatString, params object[] arg)
        {
            var defaultColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(formatString, arg);

            Console.ForegroundColor = defaultColor;
        }

        public void ConsoleWriteLine(ConsoleColor color, string formatString, params object[] arg)
        {
            var defaultColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(formatString, arg);

            Console.ForegroundColor = defaultColor;
        }

        protected void AssertIsTrue(bool condition, string message)
        {
            Debug.Assert(condition, message);
            if (!condition)
            {
                throw new AssertException(message);
            }
        }

        protected void AssertAreEqual<T>(T actual, T expected, string message)
        {
            bool result = Equals(expected, actual);

            var newMessage = string.Format("Expected {0}, actual {1}. Message: {2}", expected, actual, message);
            Debug.Assert(result, message);
            if (!result)
            {
                throw new AssertException(newMessage);
            }
        }

        protected void AssertIsLessThan<T>(T actual, T expected, string message)
        {
            var compareable = (IComparable)expected;
            var result = compareable.CompareTo(actual) > 0;

            var newMessage = string.Format("Expected less than {0}, actual {1}. Message: {2}", expected, actual, message);
            Debug.Assert(result, message);
            if (!result)
            {
                throw new AssertException(newMessage);
            }
        }

        protected void AssertGreaterThan<T>(T actual, T expected, string message)
        {
            var compareable = (IComparable)expected;
            var result = compareable.CompareTo(actual) < 0;

            var newMessage = string.Format("Expected greater than {0}, actual {1}. Message: {2}", expected, actual, message);
            Debug.Assert(result, message);
            if (!result)
            {
                throw new AssertException(newMessage);
            }
        }

        protected class AssertException : Exception
        {
            public AssertException(string message)
                : base(message)
            {
            }
        }
    }
}
