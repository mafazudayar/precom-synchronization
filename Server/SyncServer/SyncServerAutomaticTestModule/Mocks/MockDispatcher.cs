﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Login;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Test.Mocks
{
    internal class MockDispatcher : IDispatcher
    {
        public List<SentItem> SentItems { get; private set; }

        private event EventHandler<PullRequestEventArgs> PullRequestReceived;
        private event EventHandler<PushRequestEventArgs> PushRequestReceived;
        private readonly List<ClientEventDelegate> _loginReceivers = new List<ClientEventDelegate>();

        private readonly List<EventHandler<LoginStatusEventArgs>> _loginEventHandlers = new List<EventHandler<LoginStatusEventArgs>>();

        public MockDispatcher()
        {
            SentItems = new List<SentItem>();
        }

        public void Initialize()
        {
        }

        public void Register(EventHandler<PushRequestEventArgs> pushRequestReceiver)
        {
            PushRequestReceived += pushRequestReceiver;
        }

        public void Register(EventHandler<PullRequestEventArgs> pullRequestReceiver)
        {
            PullRequestReceived += pullRequestReceiver;
        }

        public void Register(ClientEventDelegate loginReceiver)
        {
            _loginReceivers.Add(loginReceiver);
        }

        public void Register(EventHandler<LoginStatusEventArgs> loginStatusReceiver)
        {
            _loginEventHandlers.Add(loginStatusReceiver);
        }

        public void ReceivePushRequest(PushRequest pushRequest, RequestInfo requestInfo)
        {
            if (PushRequestReceived != null)
            {
                PushRequestReceived.Invoke(this, new PushRequestEventArgs
                {
                    PushRequest = pushRequest,
                    RequestInfo = requestInfo
                });
            }
        }

        public void ReceivePullRequest(PullRequest pullRequest, RequestInfo requestInfo)
        {
            if (PullRequestReceived != null)
            {
                PullRequestReceived.Invoke(this, new PullRequestEventArgs
                {
                    PullRequest = pullRequest,
                    RequestInfo = requestInfo
                });
            }
        }

        public void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, int credentialId)
        {
            SendMessage(entity, credentialId, syncModule, Persistence.ConnectionSession);
        }

        public void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, Client client)
        {
            SendMessage(entity, client.CredentialId, syncModule, Persistence.ConnectionSession);
        }

        public void SendMessage(EntityBase entity, int credentialId, SynchronizationServerBase syncModule, Persistence persistence)
        {
            var sentItem = new SentItem() { CredentialId = credentialId, Persistence = persistence };

            // Must create a clone otherwise might get error with updating the same reference.
            // Probably we want to fix this in the sync module instead to avoid future confusion and errors.
            var entityType = entity.GetType();
            if (entityType == typeof(PushResponse))
            {
                sentItem.Entity = Clone((PushResponse)entity);
            }
            else if (entityType == typeof(PullResponse))
            {
                sentItem.Entity = Clone((PullResponse)entity);
            }
            else
            {
                sentItem.Entity = entity;
            }

            SentItems.Add(sentItem);
        }

        public void SendMessage(EntityBase entity, Client client, SynchronizationServerBase syncModule)
        {
            SendMessage(entity, client.CredentialId, syncModule, Persistence.ConnectionSession);
        }

        private PushResponse Clone(PushResponse pushResponse)
        {
            var clone = new PushResponse();
            foreach (var item in pushResponse.AcceptedObjects)
            {
                clone.AcceptedObjects.Add(item);
            }

            foreach (var item in pushResponse.ResolvedObjects)
            {
                clone.ResolvedObjects.Add(item);
            }

            clone.IsSplit = pushResponse.IsSplit;
            clone.ObjectType = pushResponse.ObjectType;
            clone.PacketNumber = pushResponse.PacketNumber;
            clone.SessionGuid = pushResponse.SessionGuid;
            clone.TotalNumberOfPackets = pushResponse.TotalNumberOfPackets;

            return clone;
        }

        private PullResponse Clone(PullResponse pullResponse)
        {
            var clone = new PullResponse();
            clone.CurrentEntityCount = pullResponse.CurrentEntityCount;

            foreach (var item in pullResponse.DeletedObjects)
            {
                clone.DeletedObjects.Add(item);
            }

            clone.IsSplit = pullResponse.IsSplit;
            clone.ObjectType = pullResponse.ObjectType;
            clone.PacketNumber = pullResponse.PacketNumber;
            clone.SessionGuid = pullResponse.SessionGuid;

            foreach (var item in pullResponse.SynchronizedObjects)
            {
                clone.SynchronizedObjects.Add(item);
            }

            clone.SyncType = pullResponse.SyncType;
            clone.TotalEntityCount = pullResponse.TotalEntityCount;
            clone.TotalNumberOfPackets = pullResponse.TotalNumberOfPackets;

            return clone;
        }
    }

    [DebuggerDisplay("Entity = {Entity}, CredentialId = {CredentialId}, Persistence = {Persistence}")]
    internal class SentItem
    {
        public EntityBase Entity { get; set; }
        public Persistence Persistence { get; set; }
        public int CredentialId { get; set; }
    }
}
