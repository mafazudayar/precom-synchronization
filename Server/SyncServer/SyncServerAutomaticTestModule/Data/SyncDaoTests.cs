﻿using System;
using System.Linq;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Test.Utils;
using PreCom.Synchronization.Utils;
using SyncServerTestModule;

namespace PreCom.Synchronization.Test.Data
{
    public class SyncDaoTests : SyncModuleTestBase
    {
        public SyncDaoTests(SynchronizationServerBase module, MarketOrderDao marketOrderDao, MarketOrderManager marketOrderStorage, ILog log)
            : base(module, marketOrderDao, marketOrderStorage, log)
        {
        }

        [ModuleTest]
        public void GetMetadata_WhenMetadataAvailable_ReturnsMetadata()
        {
            var entity = Orders[0];
            var meta = SyncDao.GetMetadata(entity.Id, true);

            AssertAreEqual(meta.Guid, entity.Guid, "Should be same");
        }

        [ModuleTest]
        public void GetMetadata_FromListWhenMetadataAvailable_ReturnsMetadata()
        {
            var entity1 = Orders[0];
            var entity2 = Orders[1];

            var metaList = SyncDao.GetMetadata(new[] { entity1.Id, entity2.Id }, true, true);

            AssertIsTrue(metaList.Single(e => e.Guid == entity1.Guid) != null, "Should exist once in collection");
            AssertIsTrue(metaList.Single(e => e.Guid == entity2.Guid) != null, "Should exist once in collection");
        }

        [ModuleTest]
        public void UpdateSyncMetaData_WhenMetadataExists_UpdatesTheMetadata()
        {
            var eventArgs = new ChangeEventArgs() { Id = Orders[0].Id };
            var meta = SyncDao.GetMetadata(Orders[0].Id, true);

            SyncDao.UpdateSyncMetadata(eventArgs, Guid.NewGuid().ToString());

            var metaAfterUpdate = SyncDao.GetMetadata(Orders[0].Id, true);

            AssertAreEqual(metaAfterUpdate.Version, meta.Version + 1, "Should have increased version by one");
        }

        [ModuleTest]
        public void UpdateMetadataWithPropertyMetadata_WhenMetadataExists_UpdatesTheMetadata()
        {
            var eventArgs = new ChangeEventArgs() { Id = Orders[0].Id };
            var meta = SyncDao.GetMetadata(Orders[0].Id, true);

            var propertyMetaList = SyncDao.GetPropertyMetadata(Orders[0].Id, true);

            var properties = PropertyHelper.GetProperties(typeof(MarketOrder)).Select(p => p.Name);

            SyncDao.UpdateMetadataWithPropertyMetadata(eventArgs, properties, Guid.NewGuid().ToString());

            var metaAfterUpdate = SyncDao.GetMetadata(Orders[0].Id, true);
            var propertyMetaAfterUpdateList = SyncDao.GetPropertyMetadata(Orders[0].Id, true);

            AssertAreEqual(metaAfterUpdate.Version, meta.Version + 1, "Should have increased version by one");

            for (int i = 0; i < propertyMetaAfterUpdateList.Count; i++)
            {
                AssertAreEqual(propertyMetaAfterUpdateList[i].Version, propertyMetaList[i].Version + 1, "Should have increased version by one");
                AssertAreEqual(propertyMetaAfterUpdateList[i].Name, propertyMetaList[i].Name, "Name should remain the same");
            }
        }
    }
}
