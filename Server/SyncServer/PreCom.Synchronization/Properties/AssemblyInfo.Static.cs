﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("PreCom.Synchronization.Server")]
[assembly: InternalsVisibleTo("SyncServerTest")]
[assembly: InternalsVisibleTo("SyncServerAutomaticTestModule")]
