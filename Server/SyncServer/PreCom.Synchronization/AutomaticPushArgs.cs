﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains information related to the changed entity and the operation.
    /// </summary>
    public class AutomaticPushArgs
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the old entity.
        /// </summary>
        public ISynchronizable Old { get; set; }

        /// <summary>
        /// Gets or sets the new entity.
        /// </summary>
        public ISynchronizable New { get; set; }

        /// <summary>
        /// Gets or sets the change information that is distributed between server instances.
        /// </summary>
        /// <remarks>
        /// If executed on the same server instance as where the change is was done this
        /// dictionary will be empty.
        /// </remarks>
        public Dictionary<string, string> ChangeInfo { get; set; }

        /// <summary>
        /// Gets or sets the change.
        /// </summary>
        public Operation Change { get; set; }

        /// <summary>
        /// Gets or sets the instance of the caller that created, updated or deleted the entity.
        /// </summary>
        public string OriginatorType { get; set; }

        /// <summary>
        /// Gets or sets the instance id of the server that created, updated or deleted the entity.
        /// </summary>
        public string OriginatorInstanceId { get; set; }

        /// <summary>
        /// Gets or sets the credential id of the user initiated the operation.
        /// </summary>
        public int CredentialId { get; set; }
    }
}
