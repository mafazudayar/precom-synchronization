﻿using System;
using System.Data;
using System.Transactions;
using PreCom.Core;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace PreCom.Synchronization.Utils
{
    /// <summary>
    /// Utilities for working with transactions in a simplified way.
    /// </summary>
    internal static class TransactionUtil
    {
        /// <summary>
        /// Changes the transaction isolation level using the default PreCom database connection.
        /// This should be used to reset the isolation level for a transaction is running in a transaction scope.
        /// Otherwise the connection will remain at the previously specified isolation level if connection pooling is enabled, which is recommended and the default setting.
        /// This is mostly a problem if running in high isolation levels in particular Snapshot, but Serializable and Repeatable read could also cause potential problems.
        /// See: http://support.microsoft.com/kb/972915.
        /// </summary>
        /// <param name="storage">StorageBase implementation.</param>
        /// <param name="isolationLevel">The isolation level to set on the transaction.</param>
        internal static void SetIsolationLevel(StorageBase storage, IsolationLevel isolationLevel)
        {
            var cmdText = "SET TRANSACTION ISOLATION LEVEL ";

            switch (isolationLevel)
            {
                case IsolationLevel.Serializable:
                    cmdText += "SERIALIZABLE";
                    break;
                case IsolationLevel.RepeatableRead:
                    cmdText += "REPEATABLE READ";
                    break;
                case IsolationLevel.ReadCommitted:
                    cmdText += "READ COMMITTED";
                    break;
                case IsolationLevel.ReadUncommitted:
                    cmdText += "READ UNCOMMITTED";
                    break;
                case IsolationLevel.Snapshot:
                    cmdText += "SNAPSHOT";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("isolationLevel");
            }

            using (var cmd = storage.CreateCommand(cmdText))
            {
                cmd.CommandType = CommandType.Text;

                storage.ExecuteNonCommand(cmd);
            }
        }

        /// <summary>
        /// Gets the current isolation level for a transaction.
        /// </summary>
        /// <param name="storage">StorageBase implementation.</param>
        /// <returns>The current isolation level for the connection.</returns>
        internal static IsolationLevel GetSqlConnectionIsolationLevel(StorageBase storage)
        {
            // 0 = Unspecified
            // 1 = ReadUncomitted
            // 2 = ReadCommitted
            // 3 = Repeatable
            // 4 = Serializable
            // 5 = Snapshot
            using (var com = storage.CreateCommand("select transaction_isolation_level from sys.dm_exec_sessions where (session_id = @@SPID)"))
            {
                var level = (short)storage.ExecuteScalarCommand(com);

                switch (level)
                {
                    case 0:
                        return IsolationLevel.Unspecified;

                    case 1:
                        return IsolationLevel.ReadUncommitted;

                    case 2:
                        return IsolationLevel.ReadCommitted;

                    case 3:
                        return IsolationLevel.RepeatableRead;

                    case 4:
                        return IsolationLevel.Serializable;

                    case 5:
                        return IsolationLevel.Snapshot;

                    default:
                        throw new InvalidCastException("Received unexpected isolation level value from SQL Server. Expected 0-5, actual " + level);
                }
            }
        }
    }
}
