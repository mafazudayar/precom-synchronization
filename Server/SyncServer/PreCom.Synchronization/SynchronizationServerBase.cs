﻿using System;
using System.Collections.Generic;
using System.Transactions;
using PreCom.Core;
using PreCom.Login;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Represents the method that determines which credentials will receive a changed object.
    /// </summary>
    /// <param name="automaticPushArgs">The arguments describing an object change.</param>
    /// <returns>
    /// List of credential ids
    /// </returns>
    [Obsolete("Use AutomaticPushDelegate2(AutomaticPushArgs) instead.")]
    public delegate ICollection<int> AutomaticPushDelegate(AutomaticPushArgs automaticPushArgs);

    /// <summary>
    /// Represents the method that determines which credentials will receive a changed object.
    /// </summary>
    /// <param name="automaticPushArgs">The arguments describing an object change.</param>
    /// <returns>
    /// An object that encapsulates the return values
    /// </returns>
    public delegate AutomaticPushReturnValue AutomaticPushDelegate2(AutomaticPushArgs automaticPushArgs);

    /// <summary>
    /// Notify the custom server module about the conflicted entity.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <param name="conflictInfo">The conflict info.</param>
    /// <returns>
    /// <see cref="ISynchronizable"/> Entity
    /// </returns>
    public delegate ISynchronizable ConflictResolutionDelegate<T>(ConflictInfo<T> conflictInfo)
        where T : ISynchronizable;

    /// <summary>
    /// Invoke this delegate when a client is logged in.
    /// </summary>
    /// <param name="loginFilterArgs">The login filter args.</param>
    /// <returns>
    /// List of <see cref="ISynchronizable"/> entity ids
    /// </returns>
    public delegate ICollection<Guid> LoginFilterDelegate(LoginFilterArgs loginFilterArgs);

    /// <summary>
    /// Represents the method that is called to notify about a sync event.
    /// </summary>
    /// <remarks>
    /// Currently the only <see cref="SyncEventReason"/> that is being notified
    /// is <see cref="SyncEventReason.ServerError"/>.
    /// </remarks>
    /// <param name="entityId">The id of the entity involved in the event.</param>
    /// <param name="syncInfo">The sync info which contains information about the event.</param>
    public delegate void SyncEventDelegate(Guid entityId, SyncInfo syncInfo);

    /// <summary>
    /// The base class which use by the synchronization module.
    /// </summary>
    public abstract class SynchronizationServerBase : ModuleBase
    {
        #region Public members

        /// <summary>
        /// The custom module needs to register the storage through this method in order to use the synchronization module.
        /// </summary>
        /// <typeparam name="T">The type of the entity that the custom module is using to synchronize.</typeparam>
        /// <param name="storage">An instance of the storage that the custom module uses.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if storage is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has already been called for T.</exception>
        public abstract void RegisterStorage<T>(SynchronizedStorageBase<T> storage)
            where T : ISynchronizable;

        /// <summary>
        /// Unregisters a storage through this method for disposing registered object.
        /// </summary>
        /// <typeparam name="T">The type of the entity that the custom module is using to synchronize.</typeparam>
        /// <param name="storage">An instance of the storage that the custom module uses.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if storage is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void UnregisterStorage<T>(SynchronizedStorageBase<T> storage)
            where T : ISynchronizable;

        /// <summary>
        /// Pushes the specified type of entities.
        /// </summary>
        /// <remarks>
        /// Note that if filter parameter contains an empty filter list all objects will be pushed.
        /// </remarks>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="filter">The filter that matches the entities to be pushed.</param>
        /// <param name="credentials">The list of credentials , of the users who should be notified.</param>
        /// <exception cref="ArgumentNullException">Thrown if filter or credentials is null.</exception>
        /// <exception cref="ArgumentException">Thrown if credentials is empty.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void Push<T>(EntityFilter filter, ICollection<int> credentials)
            where T : ISynchronizable;

        /// <summary>
        /// Registers a delegate for automatic push of changes.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="automaticPushDelegate">The automatic push delegate.</param>
        /// <param name="changeOperation">The change operation.</param>
        /// <exception cref="ArgumentNullException">Thrown if automaticPushDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        [Obsolete("Use RegisterAutomaticPush2(AutomaticPushDelegate2, Operation)")]
#pragma warning disable 618
        public abstract void RegisterAutomaticPush<T>(AutomaticPushDelegate automaticPushDelegate, Operation changeOperation)
#pragma warning restore 618
            where T : ISynchronizable;

        /// <summary>
        /// Registers a delegate for automatic push of changes.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="automaticPushDelegate">The automatic push delegate.</param>
        /// <param name="changeOperation">The change operation.</param>
        /// <exception cref="ArgumentNullException">Thrown if automaticPushDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        /// <example>
        /// This example shows how a delegate can be implemented when used in a load balanced environment
        /// <code>
        ///        private AutomaticPushReturnValue AutoPushDelegateExample(AutomaticPushArgs automaticPushArgs)
        ///        {
        ///            string creator = "";
        ///            AutomaticPushReturnValue returnValue = new AutomaticPushReturnValue();
        ///            if (automaticPushArgs.OriginatorInstanceId == _application.InstanceId)
        ///            {
        ///                // Handle the case when the change has been done on this server, we can use Old and New objects
        ///
        ///                // Add information that is needed on other servers when running the delegate
        ///                if (automaticPushArgs.New != null)
        ///                {
        ///                    creator = ((LimitOrder)automaticPushArgs.New).Creator;
        ///                    returnValue.ChangeInfo.Add("Creator", creator);
        ///                }
        ///            }
        ///            else
        ///            {
        ///                // Handle the case when the change has been done on another server
        ///                // Old and New objects will be null
        ///                if (automaticPushArgs.ChangeInfo.ContainsKey("Creator"))
        ///                {
        ///                    creator = automaticPushArgs.ChangeInfo["Creator"];
        ///                }
        ///            }
        ///            
        ///            // Add credentials here...
        ///            returnValue.CredentialIds.Add(GetCredentials(creator));
        ///
        ///            return returnValue;
        ///        }
        /// </code>
        /// </example>
        public abstract void RegisterAutomaticPush2<T>(AutomaticPushDelegate2 automaticPushDelegate, Operation changeOperation)
            where T : ISynchronizable;

        /// <summary>
        /// Unregisters a delegate which is register for automatic push of changes.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <exception cref="ArgumentNullException">Thrown if automaticPushDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void UnregisterAutomaticPush2<T>()
            where T : ISynchronizable;

        /// <summary>
        /// Registers the login filter. Specifies the delegate that should be executed when the user logs in.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="loginFilterDelegate">The login filter delegate.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if loginFilterDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void RegisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate)
            where T : ISynchronizable;
        

        /// <summary>
        /// Registers the login filter. Specifies the delegate that should be executed when the user logs in.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="loginFilterDelegate">The login filter delegate.</param>
        /// <param name="trigger">LoginStatus for which the login filter should be invoked.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if loginFilterDelegate is null.</exception>
        /// <exception cref="System.ArgumentException">Thrown if trigger equals <see cref="LoginStatus.InProgress"/> or <see cref="LoginStatus.LoggedOut"/>.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        /// <exception cref="InvalidOperationException">Thrown if there is no module which implements ILoginStatus </exception>
        public abstract void RegisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate, LoginStatus trigger) where T : ISynchronizable;

        /// <summary>
        /// Unregisters the login filter.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="loginFilterDelegate">The login filter delegate.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if loginFilterDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void UnregisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate)
            where T : ISynchronizable;

        /// <summary>
        /// Unregisters the login filter. 
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="trigger">LoginStatus for which the login filter should be invoked.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if loginFilterDelegate is null.</exception>
        /// <exception cref="System.ArgumentException">Thrown if trigger equals <see cref="LoginStatus.InProgress"/> or <see cref="LoginStatus.LoggedOut"/>.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        /// <exception cref="InvalidOperationException">Thrown if there is no module which implements ILoginStatus </exception>
        public abstract void UnregisterLoginFilter<T>(LoginStatus trigger) where T : ISynchronizable;
        
        /// <summary>
        /// Registers the specified conflict resolution delegate.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="conflictResolutionDelegate">The conflict resolution delegate.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if conflictResolutionDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void RegisterConflictResolver<T>(ConflictResolutionDelegate<T> conflictResolutionDelegate)
            where T : ISynchronizable;

        /// <summary>
        /// Unregisters the specified conflict resolution delegate.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>       
        /// <exception cref="System.ArgumentNullException">Thrown if conflictResolutionDelegate is null.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <see cref="RegisterStorage{T}"/> has not been called for T.</exception>
        public abstract void UnregisterConflictResolver<T>()
            where T : ISynchronizable;

        /// <summary>
        /// Registers the sync event notify callback method.
        /// </summary>
        /// <remarks>
        /// Currently the only <see cref="SyncEventReason"/> that is being notified
        /// is <see cref="SyncEventReason.ServerError"/>.
        /// </remarks>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="syncEventNotifier">The sync event notify callback method.</param>
        public abstract void RegisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier) where T : ISynchronizable;

        /// <summary>
        /// Unregisters the sync event notify callback method.
        /// </summary>
        /// <remarks>
        /// Currently the only <see cref="SyncEventReason"/> that is being notified
        /// is <see cref="SyncEventReason.ServerError"/>.
        /// </remarks>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="syncEventNotifier">The sync event notify callback method.</param>
        public abstract void UnregisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier) where T : ISynchronizable;

        /// <summary>
        /// Creates a transaction scope that will reset the connection to read committed before completing or disposing the transaction scope.
        /// Uses <see cref="TransactionScopeOption.Required"/> and timeout value <see cref="TransactionManager.DefaultTimeout"/> for the transaction scope.
        /// </summary>
        /// <param name="isolationLevel">The isolation Level.</param>
        /// <returns>The <see cref="StorageTransactionScope"/>.</returns>
        public abstract StorageTransactionScope CreateTransaction(IsolationLevel isolationLevel);

        /// <summary>
        /// Creates a transaction scope that will reset the connection to read committed before completing or disposing the transaction scope.
        /// </summary>
        /// <param name="isolationLevel">The isolation Level.</param>
        /// <param name="option">The option.</param>
        /// <param name="timeout">The timeout.</param>
        /// <returns>The <see cref="StorageTransactionScope"/>.</returns>
        public abstract StorageTransactionScope CreateTransaction(IsolationLevel isolationLevel, TransactionScopeOption option, TimeSpan timeout);

        /// <summary>
        /// Acquires an update lock on the metadata for the entity with given id.
        /// The method block until the update lock has been acquired and will hold the lock for the duration of the ambient transaction.
        /// Avoid locking on an entity for long time periods as it will affect performance negatively.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="entityId">The entity to acquire update lock for.</param>
        /// <returns>The number of entities found matching the supplied entity id.</returns>
        public abstract int AcquireUpdateLock<T>(Guid entityId)
            where T : ISynchronizable;

        /// <summary>
        /// Acquires an update lock on the metadata for the entities with given ids.
        /// The method block until the update lock has been acquired and will hold the lock for the duration of the ambient transaction.
        /// Avoid locking on multiple entities for long time periods as it will affect performance negatively.
        /// </summary>
        /// <typeparam name="T">The type of the entity.</typeparam>
        /// <param name="entityIds">Collection of entity ids to acquire locks on.</param>
        /// <returns>The number of entities found matching the supplied entity ids.</returns>
        public abstract int AcquireUpdateLock<T>(ICollection<Guid> entityIds)
            where T : ISynchronizable;

        #endregion

        #region Internal members

        internal bool EnablePerformanceLogs { get; set; }

        #endregion
    }
}
