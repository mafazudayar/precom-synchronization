﻿using PreCom.Core;

namespace PreCom.Synchronization
{
	/// <summary>
	/// Contains the information about the logged in client.
	/// </summary>
	public class LoginFilterArgs
	{
		/// <summary>
		/// Gets or sets the client.
		/// </summary>
		public Client Client { get; set; }
	}
}
