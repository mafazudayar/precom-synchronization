﻿namespace PreCom.Synchronization
{
	/// <summary>
	/// Contains information related to a conflicted entity.
	/// </summary>
	public class ConflictInfo<T> where T : ISynchronizable
	{
		/// <summary>
		/// Gets or sets the change of the entity.
		/// </summary>
		public Operation Change { get; set; }

		/// <summary>
		/// Gets or sets the server entity
		/// </summary>
		public T OldValue { get; set; }

		/// <summary>
		/// Gets or sets the client entity.
		/// </summary>
		public T NewValue { get; set; }
	}
}
