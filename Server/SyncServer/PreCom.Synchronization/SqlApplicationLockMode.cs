﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Determines the mode of the SQL application lock
    /// </summary>
    public enum SqlApplicationLockMode
    {
        /// <summary>
        /// Used for read operations that do not change or update data.
        /// </summary>
        Shared,
        /// <summary>
        /// Used for data-modification operations. Ensures that multiple updates cannot be made to the same type at the same time.
        /// </summary>
        Exclusive
    }
}
