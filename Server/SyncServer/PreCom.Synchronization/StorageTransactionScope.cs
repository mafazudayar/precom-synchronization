﻿using System;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Replacement for normal .NET TransactionScope class which resets the connection to read committed before calling Complete or Dispose.
    /// If the transaction scope is nested within another transaction scope the connection will not be reset to read committed.
    /// </summary>
    public class StorageTransactionScope : IDisposable
    {
        private readonly StorageBase _storage;
        private readonly ILog _log;
        private readonly TransactionScope _scope;
        private readonly bool _shouldResetLevel;
        private volatile bool _isLevelReset;

        /// <summary>
        /// Gets the Storage instance.
        /// </summary>
        protected StorageBase Storage
        {
            get { return _storage; }
        }

        /// <summary>
        /// Gets the wrapped TransactionScope.
        /// </summary>
        protected TransactionScope TransactionScope
        {
            get { return _scope; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageTransactionScope"/> class using TransactionManager.DefaultTimeout and TransactionScopeOption.Required.
        /// </summary>
        /// <param name="storage">The storage instance.</param>
        /// <param name="isolationLevel">The isolation level to use for the transaction.</param>
        [Obsolete("Use StorageTransactionScope(StorageBase, ILog, IsolationLevel, TransactionScopeOption, TimeSpan) instead to avoid hiding errors from dispose method.")]
        protected internal StorageTransactionScope(StorageBase storage, IsolationLevel isolationLevel)
            : this(storage, null, isolationLevel, TransactionScopeOption.Required, TransactionManager.DefaultTimeout)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageTransactionScope"/> class.
        /// </summary>
        /// <param name="storage">The storage instance.</param>
        /// <param name="isolationLevel">The isolation level to use for the transaction.</param>
        /// <param name="option">Additional options for creating the transaction scope.</param>
        /// <param name="timeout">The timeout for the transaction scope.</param>
        [Obsolete("Use StorageTransactionScope(StorageBase, ILog, IsolationLevel, TransactionScopeOption, TimeSpan) instead to avoid hiding errors from dispose method.")]
        protected internal StorageTransactionScope(StorageBase storage, IsolationLevel isolationLevel, TransactionScopeOption option, TimeSpan timeout)
            : this(storage, null, isolationLevel, option, timeout)
        {
        }

        internal StorageTransactionScope(StorageBase storage, ILog log, IsolationLevel isolationLevel)
            : this(storage, log, isolationLevel, TransactionScopeOption.Required, TransactionManager.DefaultTimeout)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageTransactionScope"/> class.
        /// </summary>
        /// <param name="storage">The storage instance.</param>
        /// <param name="log">Log implemetation used in case of internal failure in the Dispose method.</param>
        /// <param name="isolationLevel">The isolation level to use for the transaction.</param>
        /// <param name="option">Additional options for creating the transaction scope.</param>
        /// <param name="timeout">The timeout for the transaction scope.</param>
        protected internal StorageTransactionScope(StorageBase storage, ILog log, IsolationLevel isolationLevel, TransactionScopeOption option, TimeSpan timeout)
        {
            if (Transaction.Current == null)
            {
                _shouldResetLevel = true;
            }

            _storage = storage;
            _log = log;

            var transactionOptions = new TransactionOptions()
            {
                IsolationLevel = isolationLevel,
                Timeout = timeout
            };

            _scope = new TransactionScope(option, transactionOptions);
        }

        /// <summary>
        /// Resets the isolation level to read committed if required and then completes the transaction scope to indicate all operations were successful.
        /// </summary>
        public virtual void Complete()
        {
            ResetIsolationLevel();
            _scope.Complete();
        }

        private void ResetIsolationLevel()
        {
            if (_isLevelReset || !_shouldResetLevel)
            {
                return;
            }

            if (Transaction.Current != null && Transaction.Current.IsolationLevel == IsolationLevel.ReadCommitted)
            {
                // Transaction.Current will throw InvalidOperationException if accessed after scope has completed
                // By setting this to true we avoid that.
                _isLevelReset = true;
                return;
            }

            TransactionUtil.SetIsolationLevel(_storage, IsolationLevel.ReadCommitted);
            _isLevelReset = true;
        }

        /// <summary>
        /// Resets the connection and then disposes the wrapped transaction scope.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Resets the connection and then disposes the wrapped transaction scope if disposing set to true.
        /// </summary>
        /// <param name="disposing">True if disposing, else false.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                bool resetIsolationLevel;
                try
                {
                    // Frist try to reset the isolation level when within the transaction scope.
                    // If failing try to reset it for the connection after disposing the scope.
                    // Otherwise it might remain on the same level which can cause unexpected behavior.
                    // The reason we try here first is that we are here more sure that we will get the correct connection when inside the scope.
                    ResetIsolationLevel();
                    resetIsolationLevel = true;
                }
                catch (Exception)
                {
                    resetIsolationLevel = false;
                }

                _scope.Dispose();

                if (resetIsolationLevel)
                {
                    return;
                }

                try
                {
                    ResetIsolationLevel();
                }
                catch (Exception ex)
                {
                    using (new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        var logItem = new Core.Log.LogItem(
                                        null,
                                        LogLevel.Warning,
                                        LogId.SYNC_TSCOPE_E_1.ToString(),
                                        "Failed to reset isolation level",
                                        "Error when trying to reset the connection isolation level to read committed after disposing StorageTransactionScope.", ex.ToString());

                        if (_log != null)
                        {
                            _log.Write(logItem);
                        }
                    }
                }
            }
        }
    }
}
