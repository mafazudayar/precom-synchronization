﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Encapsulates the return value from <see cref="AutomaticPushDelegate2"/>
    /// </summary>
    public class AutomaticPushReturnValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutomaticPushReturnValue"/> class. 
        /// </summary>
        public AutomaticPushReturnValue()
        {
            CredentialIds = new Collection<int>();
            ChangeInfo = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets a list of credential ids that shall receive the changed object.
        /// </summary>
        public ICollection<int> CredentialIds { get; set; }

        /// <summary>
        /// Gets or sets information about the change that shall be distributed when
        /// <see cref="AutomaticPushDelegate2"/> is called on other server instances in a load
        /// balanced environment. This is needed because when the delegate is called on other server
        /// instances than where the change was done then <see cref="AutomaticPushArgs.Old"/> and
        /// <see cref="AutomaticPushArgs.New"/> are null. Any information that is needed on other server
        /// instances should be added by the implementer of the delegate.
        /// </summary>
        public Dictionary<string, string> ChangeInfo { get; set; }
    }
}