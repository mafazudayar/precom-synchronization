﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Thrown when an exclusive sql application lock can not be acquired.
    /// </summary>
    public class SqlApplicationLockException : Exception
    {
        /// <summary>
        /// Gets or sets the error code. See MSDN documentation of <code>sp_getapplock</code> for the details.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlApplicationLockException"/> class. 
        /// </summary>
        /// <param name="errorCode">The error code.
        /// </param>
        /// <param name="message">The message associated with this exception.
        /// </param>
        public SqlApplicationLockException(int errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
