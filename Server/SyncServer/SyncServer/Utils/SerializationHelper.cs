﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace PreCom.Synchronization.Utils
{
    // This class is used to customize the serialization. So that we can
    // restrict the properties that really need to be sent to the client.
    /// <summary>
    /// This class should not be used outside of Synchronization.
    /// </summary>
    [Serializable]
    public class SerializationHelper : ISerializable
    {
        private readonly List<string> _includeProperties;
        private readonly object _originalInput;

        internal SerializationHelper(object instance, List<string> includeProperties)
        {
            _includeProperties = includeProperties;
            _originalInput = instance;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializationHelper" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The context.</param>
        protected SerializationHelper(SerializationInfo info, StreamingContext context)
        {
            // Even though it seems this is not used it is called by .NET when deserializing values.
            // http://msdn.microsoft.com/en-us/library/ty01x675.aspx.
        }

        #region ISerializable Members

        /// <summary>
        /// This is a member of ISerializable interface which allows to add fields that are need to be serialized.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission.</exception>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Type originalType = _originalInput.GetType();
            IEnumerable<PropertyInfo> dataMembers;

            if (_includeProperties != null && _includeProperties.Any())
            {
                dataMembers = from property in originalType.GetProperties()
                              where _includeProperties.Contains(property.Name)
                              select property;
            }
            else
            {
                dataMembers = originalType.GetProperties().ToList();
            }

            if (HasSynchronizableProperties(originalType))
            {
                dataMembers = dataMembers.Where(IsSynchronizable);
            }

            foreach (var dataMember in dataMembers)
            {
                info.AddValue(dataMember.Name, dataMember.GetValue(_originalInput, null));
            }
        }

        #endregion

        private bool IsSynchronizable(PropertyInfo propertyInfo)
        {
            return Attribute.IsDefined(propertyInfo, typeof(SynchronizableAttribute));
        }

        private bool HasSynchronizableProperties(Type originalType)
        {
            return originalType.GetProperties().Any(prop => prop.IsDefined(typeof(SynchronizableAttribute), false));
        }
    }
}
