﻿using System;
using System.Collections.Generic;
using System.Linq;
using PreCom.Synchronization.Core;

namespace PreCom.Synchronization.Utils
{
	internal static class SyncSerializer
	{
		internal static string Serialize(object instance , Type [] knownTypes , List<string> changedProperties, IEnumerable<Type> allProperties , string platform)
		{
			string serializedString = null;

			if (SynchronizationModule.AndroidPlatform == platform)
			{
				SerializationHelper helper = new SerializationHelper(instance, changedProperties);
				serializedString = JsonSerializer.Serialize(helper, allProperties.ToArray());
			}
			else
			{
				serializedString = SyncXmlSerializer.Serialize(instance);
			}

			return serializedString;
		}
	}
}
