﻿using System;

namespace PreCom.Synchronization.Utils
{
	/// <summary>
	/// This class is used to wrap the Id, in order to serialize the list of ids.
	/// </summary>
    /// The issue is, if we send the list of ids without a wrapper at the sql it can only identify the first element only.
	public class IdContainer
	{
		/// <summary>
		/// Gets or sets the id of the searching object.
		/// </summary>
		public Guid EntityId { get; set; }
	}
}
