﻿using System;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;

namespace PreCom.Synchronization.Utils
{
    internal class LogHelper
    {
        private readonly ILog _logger;

        internal LogHelper(ILog log)
        {
            _logger = log;
        }

        private void Log(PreCom.Core.Log.LogItem item)
        {
            _logger.Write(item);
        }

        internal void LogError(PreComBase preComBase, LogId id, string header, string description, string dump = null)
        {
            Log(new PreCom.Core.Log.LogItem(preComBase, LogLevel.Error, id.ToString(), header, description, dump, 0));
        }

        internal void LogError(PreComBase preComBase, Exception exception, LogId id, string header)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _logger.Write(new PreCom.Core.Log.LogItem(preComBase,
                    LogLevel.Error,
                    id.ToString(),
                    header,
                    exception.Message,
                    exception.ToString()));
                transactionScope.Complete();
            }
        }

        internal void LogDebug(PreComBase preComBase, LogId id, string header, string body, string dump = null)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _logger.Write(new PreCom.Core.Log.LogItem(preComBase, LogLevel.Debug, id.ToString(), header, body, dump));
                transactionScope.Complete();
            }
        }

        internal void LogInformation(PreComBase preComBase, LogId id, string header, string body, string dump = null)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _logger.Write(new PreCom.Core.Log.LogItem(preComBase,
                    LogLevel.Information,
                    id.ToString(),
                    header,
                    body,
                    dump));
                transactionScope.Complete();
            }
        }

        internal void LogWarning(PreComBase preComBase, LogId id, string header, string body, string dump = null)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                _logger.Write(new PreCom.Core.Log.LogItem(preComBase,
                    LogLevel.Warning,
                    id.ToString(),
                    header,
                    body,
                    dump));
                transactionScope.Complete();
            }
        }
    }
}
