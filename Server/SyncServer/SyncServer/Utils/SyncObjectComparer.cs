﻿using System;
using System.Collections.Generic;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Utils
{
	internal class SyncObjectComparer : IEqualityComparer<SyncMetadata>
	{
		#region IEqualityComparer<SyncMetaObject> Members

		public bool Equals(SyncMetadata x, SyncMetadata y)
		{
			if (ReferenceEquals(x, y)) return true;

			if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
				return false;

			return x.Guid == y.Guid && x.Version == y.Version && x.Status == y.Status;
		}

		public int GetHashCode(SyncMetadata obj)
		{
			if (ReferenceEquals(obj, null)) return 0;

			int hashGuid = obj.Guid == Guid.Empty ? 0 : obj.Guid.GetHashCode();

			int hashVersion = obj.Version == 0 ? 0 : obj.Version.GetHashCode();

			int hashStatus = obj.Status.GetHashCode();

			return hashGuid ^ hashVersion ^ hashStatus;
		}

		#endregion
	}
}
