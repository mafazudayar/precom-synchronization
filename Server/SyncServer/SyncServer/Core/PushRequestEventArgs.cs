﻿using System;
using PreCom.Core;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class PushRequestEventArgs : EventArgs
    {
        internal PushRequest PushRequest { get; set; }
        internal RequestInfo RequestInfo { get; set; }
    }
}
