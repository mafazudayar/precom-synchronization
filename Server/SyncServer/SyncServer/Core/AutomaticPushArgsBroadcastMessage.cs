﻿using System.Runtime.Serialization;

namespace PreCom.Synchronization.Core
{
    [DataContract]
    internal class AutomaticPushArgsBroadcastMessage
    {
        [DataMember]
        public AutomaticPushArgs AutomaticPushArgs { get; set; }
    }
}