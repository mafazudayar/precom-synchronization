﻿using System;
using System.Linq.Expressions;

namespace PreCom.Synchronization.Core
{
    internal class LoginExpressionFilter<T> where T : ISynchronizable
    {
        internal Expression<Func<T, bool>> Filter { get; set; }
        internal string PlatformType { get; set; }
    }
}
