﻿namespace PreCom.Synchronization.Core
{
    internal class TypeInformation
    {
        internal IClientPushManager PushManager { get; set; }
        internal IClientPullManager PullManager { get; set; }
        internal ILoginManager LoginManager { get; set; }
        internal IEntityUpdateManager EntityUpdateManager { get; set; }
    }
}
