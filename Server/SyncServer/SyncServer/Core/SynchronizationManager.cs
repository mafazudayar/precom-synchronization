﻿using System;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;
using Synchronization.Shared.All.Utils;

namespace PreCom.Synchronization.Core
{
    internal abstract class SynchronizationManager<T> where T : ISynchronizable
    {
        #region Fields and Properties

        private readonly SynchronizedStorageBase<T> _storage;
        private readonly SynchronizationModule _syncModule;
        private readonly int _maxDataPacketSize;
        private readonly IDispatcher _dispatcher;
        private readonly LogHelper _logHelper;

        protected SynchronizedStorageBase<T> Storage
        {
            get { return _storage; }
        }

        protected SynchronizationModule SyncModule
        {
            get { return _syncModule; }
        }

        protected int MaxDataPacketSize
        {
            get { return _maxDataPacketSize; }
        }

        protected IDispatcher Dispatcher
        {
            get { return _dispatcher; }
        }

        protected LogHelper LogHelper
        {
            get { return _logHelper; }
        }

        internal SyncEventDelegate SyncEventNotifier { get; set; }

        #endregion

        #region Constructors

        protected SynchronizationManager(SynchronizedStorageBase<T> storage, SynchronizationModule syncModule,
            int maxDataPacketSize, IDispatcher dispatcher, ILog log)
        {
            _storage = storage;
            _syncModule = syncModule;
            _maxDataPacketSize = maxDataPacketSize;
            _dispatcher = dispatcher;
            _logHelper = new LogHelper(log);
        }

        #endregion

        #region Protected methods

        protected static int GetNumberOfPackets(int innerObjectSize, int maxDataPacketSize)
        {
            int numberOfPackets = innerObjectSize / maxDataPacketSize;

            if ((innerObjectSize % maxDataPacketSize) > 0)
            {
                numberOfPackets++;
            }

            return numberOfPackets;
        }

        protected void SendErrorMessage(string objectType, Guid sessionGuid, Client client, SyncType type, Exception exception)
        {
            SendErrorMessage(objectType, sessionGuid, client, type, exception.ToString());
        }

        protected void SendErrorMessage(string objectType, Guid sessionGuid, Client client, SyncType type, string information)
        {
            var syncInfo = new SyncInfo()
            {
                Information = information,
                Reason = SyncEventReason.ServerError,
                SessionId = sessionGuid,
                SynchronizedEntityCount = 0,
                TotalEntityCount = 0,
                Type = SyncTypeHelper.ToSyncEventType(type)
            };

            var notifier = SyncEventNotifier;
            if (notifier != null)
            {
                try
                {
                    notifier.Invoke(Guid.Empty, syncInfo);
                }
                catch (Exception exception)
                {
                    _logHelper.LogError(SyncModule,
                        exception,
                        LogId.SYNC_SM_E_1,
                        "Exception when calling sync event callback");
                }
            }

            Dispatcher.SendMessageWhenReady(new ErrorMessage
            {
                Information = information,
                ObjectType = objectType,
                SessionId = sessionGuid,
                Type = type,
            }, SyncModule, client);
        }

        #endregion

        protected string[] SplitSerializedEntity(string entity, int allowedMaxDataPacketSize)
        {
            var lengthOfEntity = entity.Length;
            var numberOfSplits = lengthOfEntity / allowedMaxDataPacketSize;

            if (lengthOfEntity % allowedMaxDataPacketSize > 0)
            {
                numberOfSplits += 1;
            }

            var splits = new string[numberOfSplits];

            for (int i = 0; i < numberOfSplits - 1; i++)
            {
                splits[i] = entity.Substring(i * allowedMaxDataPacketSize, allowedMaxDataPacketSize);
            }

            splits[numberOfSplits - 1] = entity.Substring((numberOfSplits - 1) * allowedMaxDataPacketSize,
                                                entity.Length - ((numberOfSplits - 1) * allowedMaxDataPacketSize));

            return splits;
        }
    }
}
