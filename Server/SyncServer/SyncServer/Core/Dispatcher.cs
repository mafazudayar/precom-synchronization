﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Modules;
using PreCom.Login;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class Dispatcher : IDispatcher
    {
        private event EventHandler<PullRequestEventArgs> PullRequestReceived;
        private event EventHandler<PushRequestEventArgs> PushRequestReceived;
        private event EventHandler<LoginStatusEventArgs> LoginStatusChanged;

        private readonly CommunicationBase _communication;
        private readonly SynchronizationServerBase _syncModule;
        private readonly ILoginStatus _loginModule;
        private readonly ApplicationBase _application;
        private readonly LogHelper _logHelper;
        private readonly List<ClientEventDelegate> _loginReceivers = new List<ClientEventDelegate>();
        private UserClientResetEvents _userClientResetEvents;
        private const int WaitTimeoutInSeconds = 30;

        public Dispatcher(ApplicationBase application,
            CommunicationBase communication,
            SynchronizationServerBase syncModule,
            ILoginStatus loginModule,
            ILog log)
        {
            _application = application;
            _communication = communication;
            _syncModule = syncModule;
            _loginModule = loginModule;
            _logHelper = new LogHelper(log);
        }

        public void Initialize()
        {
            _communication.Register<PullRequest>(PullRequestHandler);
            _communication.Register<PushRequest>(PushRequestHandler);

            _userClientResetEvents = new UserClientResetEvents(WaitTimeoutInSeconds);

            if (_loginModule != null)
            {
                _loginModule.LoginStatusChanged += OnModuleLoginStatusChanged;
            }

            _application.Register(ClientEventReason.UserCommunicationReady, LoginHandler);
            _application.Register(ClientEventReason.ClientDisconnect,
                (Client client, ClientEventReason reason, ref ClientEventReply reply) => ResetEvent(client));
            _application.Register(ClientEventReason.UserLogout,
                (Client client, ClientEventReason reason, ref ClientEventReply reply) => ResetEvent(client));
        }

        public void Register(EventHandler<PushRequestEventArgs> pushRequestReceiver)
        {
            PushRequestReceived += pushRequestReceiver;
        }

        public void Register(EventHandler<PullRequestEventArgs> pullRequestReceiver)
        {
            PullRequestReceived += pullRequestReceiver;
        }

        public void Register(ClientEventDelegate loginReceiver)
        {
            _loginReceivers.Add(loginReceiver);
        }

        public void Register(EventHandler<LoginStatusEventArgs> loginStatusReceiver)
        {
            LoginStatusChanged += loginStatusReceiver;
        }

        private void PushRequestHandler(PushRequest pushRequest, RequestInfo requestInfo)
        {
            if (_syncModule.EnablePerformanceLogs)
            {
                _logHelper.LogDebug(
                    _syncModule,
                    LogId.SYNC_COM_D_2,
                    "Performance Logs",
                    string.Format("PushRequest received from CredentialId {0}, {1}", requestInfo.Client.CredentialId, pushRequest.GetDebugString()),
                    XSerializer.Serialize(pushRequest, typeof(PushRequest)));
            }

            if (PushRequestReceived != null)
            {
                // Fixed - PC-2531, Starting a new thread to handle push requests.
                Task.Run(() =>
                         PushRequestReceived.Invoke(this, new PushRequestEventArgs
                             {
                                 PushRequest = pushRequest,
                                 RequestInfo = requestInfo
                             }));
            }
        }

        private void PullRequestHandler(PullRequest pullRequest, RequestInfo requestInfo)
        {
            if (_syncModule.EnablePerformanceLogs)
            {
                _logHelper.LogDebug(_syncModule, LogId.SYNC_COM_D_2, "Performance Logs",
                                   string.Format("PullRequest received from CredentialId {0}, {1}", requestInfo.Client.CredentialId, pullRequest.GetDebugString()),
                                   XSerializer.Serialize(pullRequest, typeof(PullRequest)));
            }

            if (PullRequestReceived != null)
            {
                // Fixed - PC-2531, Starting a new thread to handle pull requests.
                Task.Run(() =>
                         PullRequestReceived.Invoke(this, new PullRequestEventArgs
                             {
                                 PullRequest = pullRequest,
                                 RequestInfo = requestInfo
                             }));
            }
        }

        private void OnModuleLoginStatusChanged(object sender, LoginStatusEventArgs loginStatusEventArgs)
        {
            LoginStatusChanged.Invoke(sender, loginStatusEventArgs);
        }

        private void LoginHandler(Client client, ClientEventReason reason, ref ClientEventReply reply)
        {
            _userClientResetEvents.Set(client.CredentialId, client.ClientID);

            foreach (var loginReceiver in _loginReceivers)
            {
                loginReceiver.Invoke(client, reason, ref reply);
            }
        }

        private void ResetEvent(Client client)
        {
            if (client.CredentialId != -1)
            {
                _userClientResetEvents.Reset(client.CredentialId, client.ClientID);
            }
        }

        public void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, int credentialId)
        {
            SendMessageWhenReady(entity, syncModule, credentialId, null);
        }

        // This will wait for UserCommunicationReady before sending the message. If clientId is null it will wait for the user to be ready
        // on any client. If clientId is set it will wait for the user to be ready on that client.
        public void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, Client client)
        {
            SendMessageWhenReady(entity, syncModule, client.CredentialId, client);
        }

        private void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, int credentialId, Client client)
        {
            // Check if user is logged in. 
            if (_communication.IsLoggedin(credentialId))
            {
                try
                {
                    if (Wait(credentialId, client))
                    {
                        Send(entity, credentialId, client, Persistence.ConnectionSession, syncModule);
                    }
                    else
                    {
                        string error =
                            string.Format(
                                "Timed out waiting for user to become ready to receive data. CredentialId {0}, ClientId {1}, Type {2}",
                                credentialId,
                                client != null ? client.ClientID : "null",
                                entity.GetType().Name);

                        _logHelper.LogError(syncModule,
                            LogId.SYNC_COM_E_2,
                            "Timeout",
                            error,
                            entity is IDebugPrintable ? ((IDebugPrintable)entity).GetDebugString() : null);
                    }
                }
                catch (Exception ex)
                {
                    string error = string.Format(
                        "Exception in SendMessageWhenReady. CredentialId {0},  ClientId {1}, Type {2}",
                        credentialId,
                        client != null ? client.ClientID : "null",
                        entity.GetType().Name);

                    string dump = string.Format("Exception: {0} Entity: {1}",
                        ex,
                        entity is IDebugPrintable ? ((IDebugPrintable)entity).GetDebugString() : null);

                    _logHelper.LogError(syncModule,
                        LogId.SYNC_COM_E_3,
                        "Exception",
                        error,
                        dump);
                }
            }
            else
            {
                string body =
                    string.Format(
                        "Not sending {0} to CredentialId {1} on ClientId {2} since user is not logged in",
                        entity.GetType().Name,
                        credentialId,
                        client != null ? client.ClientID : "null");
                _logHelper.LogDebug(syncModule, LogId.SYNC_COM_D_3, "Dispatcher", body);
            }
        }

        private bool Wait(int credentialId, Client client)
        {
            if (client == null)
            {
                return _userClientResetEvents.Wait(credentialId);
            }

            return _userClientResetEvents.Wait(credentialId, client.ClientID);
        }

        public void SendMessage(EntityBase entity, int credentialId, SynchronizationServerBase syncModule, Persistence persistence)
        {
            bool isLoggedIn = _communication.IsLoggedin(credentialId);
            if (persistence != Persistence.ConnectionSession || isLoggedIn)
            {
                try
                {
                    Send(entity, credentialId, null, persistence, syncModule);
                }
                catch (Exception ex)
                {
                    string error = string.Format(
                        "Exception in SendMessage. CredentialId {0}, Type {1}",
                        credentialId,
                        entity.GetType().Name);

                    string dump = string.Format("Exception: {0} Entity: {1}",
                        ex,
                        entity is IDebugPrintable ? ((IDebugPrintable)entity).GetDebugString() : null);

                    _logHelper.LogError(syncModule,
                        LogId.SYNC_COM_E_1,
                        "Exception",
                        error,
                        dump);
                }
            }
            else
            {
                string body =
                    string.Format(
                        "Not sending ConnectionSession {0} to CredentialId {1} since user is not logged in",
                        entity.GetType().Name,
                        credentialId);
                _logHelper.LogDebug(syncModule, LogId.SYNC_COM_D_4, "Dispatcher", body);
            }
        }

        public void SendMessage(EntityBase entity, Client client, SynchronizationServerBase syncModule)
        {
            try
            {
                Send(entity, client.CredentialId, client, Persistence.ConnectionSession, syncModule);
            }
            catch (Exception ex)
            {
                _logHelper.LogError(syncModule, ex, LogId.SYNC_COM_E_4, "Exception in SendMessage");
            }
        }

        private void Send(EntityBase entity, int credentialId, Client client, Persistence persistence, SynchronizationServerBase syncModule)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                if (client != null)
                {
                    _communication.Send(entity,
                        credentialId,
                        client.ClientID,
                        new RequestArgs(QueuePriority.Low, persistence));
                }
                else
                {
                    _communication.Send(entity, credentialId, new RequestArgs(QueuePriority.Low, persistence));
                }

                transactionScope.Complete();
            }

            if (syncModule.EnablePerformanceLogs)
            {
                IDebugPrintable debugPrintable = entity as IDebugPrintable;
                if (debugPrintable != null)
                {
                    string response = string.Empty;

                    if (entity is PullResponse)
                    {
                        response = XSerializer.Serialize(entity, typeof(PullResponse));
                    }

                    if (entity is PushResponse)
                    {
                        response = XSerializer.Serialize(entity, typeof(PushResponse));
                    }

                    if (entity is PullInvokeMessage)
                    {
                        response = XSerializer.Serialize(entity, typeof(PullInvokeMessage));
                    }

                    string body = string.Format("{0} sent to CredentialId {1}{2}, {3}",
                        entity.GetType().Name,
                        credentialId,
                        client != null ? " on client " + client.ClientID : string.Empty,
                        debugPrintable.GetDebugString());

                    _logHelper.LogDebug(syncModule, LogId.SYNC_COM_D_1, "Performance Logs", body, response);
                }
            }
        }
    }
}
