﻿using System;
using System.Collections.ObjectModel;

namespace PreCom.Synchronization.Core
{
	internal class ResolvedObject
	{
		private Collection<string> _changedPropertyNames;

		internal bool IsServerWin { get; set; }
		internal Guid ObjectId { get; set; }
		internal bool IsDeleted { get; set; }
		internal Collection<string> ChangedPropertyNames
		{
			get
			{
				if (_changedPropertyNames == null)
				{
					_changedPropertyNames = new Collection<string>();
				}

				return _changedPropertyNames;
			}
			set
			{
				_changedPropertyNames = value;
			}
		}
	}
}
