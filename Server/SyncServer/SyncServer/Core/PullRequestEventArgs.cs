﻿using System;
using PreCom.Core;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class PullRequestEventArgs : EventArgs
    {
        internal PullRequest PullRequest { get; set; }
        internal RequestInfo RequestInfo { get; set; }

        internal string GetLogFriendlyString()
        {
            return string.Format("ObjectType: {0}, Sync SessionId: {1}, ClientId: {2}, CredentialId: {3}",
                PullRequest.ObjectType,
                PullRequest.SessionGuid,
                RequestInfo.Client.ClientID,
                RequestInfo.Client.CredentialId);
        }
    }
}
