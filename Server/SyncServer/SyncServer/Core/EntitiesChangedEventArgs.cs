﻿using System.Collections.Generic;
using PreCom.Core;

namespace PreCom.Synchronization.Core
{
    internal class EntitiesChangedEventArgs
    {
        public ICollection<ChangeEventArgs> ChangeArgs { get; set; }
        public Client Client { get; set; }
    }
}