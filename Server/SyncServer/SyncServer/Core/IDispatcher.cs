﻿using System;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Login;

namespace PreCom.Synchronization.Core
{
    internal interface IDispatcher
    {
        void Initialize();

        void Register(EventHandler<PushRequestEventArgs> pushRequestReceiver);

        void Register(EventHandler<PullRequestEventArgs> pullRequestReceiver);

        void Register(ClientEventDelegate loginReceiver);

        void Register(EventHandler<LoginStatusEventArgs> loginStatusReceiver);

        void SendMessage(EntityBase entity,
                         int credentialId,
                         SynchronizationServerBase syncModule,
                         Persistence persistence);

        void SendMessage(EntityBase entity,
                         Client client,
                         SynchronizationServerBase syncModule);

        void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, int credentialId);

        void SendMessageWhenReady(EntityBase entity, SynchronizationServerBase syncModule, Client client);
    }
}
