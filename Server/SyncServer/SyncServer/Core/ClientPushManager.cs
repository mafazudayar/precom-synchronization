﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    using System.Globalization;
    using System.Threading;

    internal class ClientPushManager<T> : SynchronizationManager<T>, IClientPushManager where T : ISynchronizable
    {
        #region Properties

        internal ConflictResolutionDelegate<T> ConflictResolutionDelegate { get; set; }

        #endregion

        #region Constructor

        internal ClientPushManager(SynchronizedStorageBase<T> storage, IDispatcher dispatcher, SynchronizationModule syncModule,
            int maxDataPacketSize, ILog log)
            : base(storage, syncModule, maxDataPacketSize, dispatcher, log)
        {
            Dispatcher.Register(PushRequestHandler);
        }

        #endregion

        #region Private fields

        private int _partialMsgCount;

        #endregion

        #region Private Methods

        private void PushRequestHandler(object sender, PushRequestEventArgs pushRequestArgs)
        {
            PushRequest pushRequest = pushRequestArgs.PushRequest;

            var isSplitRequest = false;

            if (pushRequest.ObjectType != typeof(T).Name)
            {
                return;
            }

            try
            {
                if (pushRequest.SynchronizedObjects == null || pushRequest.SynchronizedObjects.Count < 1)
                {
                    return;
                }

                List<SyncEntityData> objectsListToSync = pushRequest.SynchronizedObjects.ToList();

                if (pushRequest.IsSplit)
                {
                    isSplitRequest = true;

                    #region This path is for handling large objects

                    // We might need to check if the record about to insert doesn't exists in the DB before performing the following statement.
                    if (SyncDao.InsertPartialEntity(pushRequest.SessionGuid, objectsListToSync[0].Guid,
                                                    pushRequest.PacketNumber,
                                                    objectsListToSync[0].Object))
                    {
                        _partialMsgCount = SyncDao.GetPartialEntityCount(pushRequest.SessionGuid,
                                                                                     objectsListToSync[0].Guid);
                    }

                    if (_partialMsgCount == pushRequest.TotalNumberOfPackets)
                    {
                        var partialObjectsList = SyncDao.GetPartialEntities(pushRequest.SessionGuid,
                                                                                     objectsListToSync[0].Guid);

                        var mergedContent = new StringBuilder();

                        foreach (var contentObject in partialObjectsList)
                        {
                            mergedContent.Append(contentObject);
                        }

                        var mergedSyncObject = new SyncEntityData
                            {
                                Guid = objectsListToSync[0].Guid,
                                Status = objectsListToSync[0].Status,
                                Version = objectsListToSync[0].Version,
                                Object = mergedContent.ToString(),
                                ChangeId = objectsListToSync[0].ChangeId
                            };

                        foreach (var property in objectsListToSync[0].ChangedPropertyNames)
                        {
                            mergedSyncObject.ChangedPropertyNames.Add(property);
                        }

                        objectsListToSync.Clear();
                        objectsListToSync.Add(mergedSyncObject);

                        SyncDao.DeletePartialEntity(pushRequest.SessionGuid, mergedSyncObject.Guid);

                        isSplitRequest = false;
                    }

                    #endregion
                }

                if (isSplitRequest)
                {
                    return;
                }

                // Sort the entities on the entity id to minimize the risk for deadlocks.
                List<SyncEntityData> orderedObjectsListToSync = objectsListToSync.OrderBy(x => x.Guid).ToList();

                try
                {
                    ProcessPushRequest(pushRequestArgs, orderedObjectsListToSync);
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 1205)
                    {
                        // Deadlock 
                        LogHelper.LogInformation(
                            SyncModule,
                            LogId.SYNC_PUSH_I_1,
                            "Retrying push request, Attempt: 1",
                            string.Format("Retrying to process the push request due to a SQL deadlock, Thread Id {0}", Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture)),
                            ex.ToString());
                        ProcessPushRequest(pushRequestArgs, orderedObjectsListToSync, 2);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            catch (SynchronizedStorageException storageException)
            {
                LogHelper.LogError(SyncModule, storageException, LogId.SYNC_PUSH_E_1, "Storage exception in push request handler");

                SendErrorMessage(pushRequest.ObjectType, pushRequest.SessionGuid,
                                 pushRequestArgs.RequestInfo.Client, SyncType.ClientPush, storageException);
            }
            catch (Exception exception)
            {
                LogHelper.LogError(SyncModule, exception, LogId.SYNC_PUSH_E_2, "Exception in push request handler");

                SendErrorMessage(pushRequest.ObjectType, pushRequest.SessionGuid,
                                 pushRequestArgs.RequestInfo.Client, SyncType.ClientPush, exception);
            }
        }

        private void ProcessPushRequest(PushRequestEventArgs pushRequestArgs, List<SyncEntityData> objectsListToSync, int retries)
        {
            int attempt = 1;

            do
            {
                try
                {
                    ProcessPushRequest(pushRequestArgs, objectsListToSync);
                    return;
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 1205)
                    {
                        // Deadlock 
                        if (retries <= 0)
                        {
                            LogHelper.LogError(
                                SyncModule,
                                 LogId.SYNC_PUSH_E_4,
                                "Failed to process the push request",
                                string.Format("Deadlock occurred when processing the push request and {0} times of retries have been performed without success, Thread Id {1}", attempt, Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture)),
                                ex.ToString());
                            throw;
                        }
                        else
                        {
                            LogHelper.LogInformation(
                                SyncModule,
                                 LogId.SYNC_PUSH_I_1,
                                "Retrying push request, Attempt: " + (attempt + 1),
                                "Retrying to process the push request due to a SQL deadlock, Thread Id " + Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture),
                                ex.ToString());
                        }
                    }
                    else
                    {
                        throw;
                    }
                }

                attempt++;
            }
            while (retries-- > 0);
        }

        private void ProcessPushRequest(PushRequestEventArgs pushRequestArgs, List<SyncEntityData> objectsListToSync)
        {
            PushRequest pushRequest = pushRequestArgs.PushRequest;
            RequestInfo requestInfo = pushRequestArgs.RequestInfo;

            List<SyncMetadata> changeIdInfoList = new List<SyncMetadata>();
            List<Guid> acceptedIdList = new List<Guid>();
            List<ResolvedObject> resolvedIdList = new List<ResolvedObject>();
            List<Guid> deletedIdList = new List<Guid>();

            List<ChangeEventArgs> changeEventArgsList = new List<ChangeEventArgs>();
            List<PushResponse> pushResponses = new List<PushResponse>();

            using (var transaction = SyncModule.CreateTransaction(IsolationLevel.ReadCommitted,
                                                                  TransactionScopeOption.Required,
                                                                  SyncModule.PushRequestTransactionTimeout))
            {
                if (SyncModule.EnablePerformanceLogs)
                {
                    var header = string.Format("Transaction (IsolationLevel.ReadCommitted) started by thread id: {0}",
                        Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture));
                    var body = string.Format(
                        "Method: ProcessPushRequest, Object Type: {0}, Object Count: {1}, ClientId: {2}, CredentialId: {3}, Sync Session Id: {4}",
                        pushRequest.ObjectType,
                        pushRequest.SynchronizedObjects.Count.ToString(CultureInfo.InvariantCulture),
                        pushRequestArgs.RequestInfo.Client.ClientID,
                        pushRequestArgs.RequestInfo.Client.CredentialId,
                        pushRequest.SessionGuid);
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_7, "Performance Logs", header, body);
                }

                List<Guid> idList = objectsListToSync.Select(x => x.Guid).ToList();
                SyncModule.AcquireUpdateLock<T>(idList);

                foreach (SyncEntityData syncObject in objectsListToSync)
                {
                    if (syncObject.Object != null || syncObject.Status == (int)Operation.Deleted)
                    {
                        changeIdInfoList.Add(new SyncMetadata
                        {
                            Guid = syncObject.Guid,
                            ChangeId = syncObject.ChangeId
                        });

                        Tuple<ResolvedObject, ChangeEventArgs> updateResult = UpdateISynchronizableEntity(pushRequest,
                            syncObject,
                            requestInfo);

                        if (updateResult.Item2 != null)
                        {
                            changeEventArgsList.Add(updateResult.Item2);
                        }

                        ManageResolvedObject(updateResult.Item1, deletedIdList, resolvedIdList, acceptedIdList);
                    }
                    else
                    {
                        string body = string.Format(
                            "Pushed object with Guid {0} and type {1} in sync session {2} is null",
                            syncObject.Guid,
                            pushRequest.ObjectType,
                            pushRequest.SessionGuid);
                        LogHelper.LogError(SyncModule, LogId.SYNC_PUSH_E_2, "Pushed object is null", body);

                        SendErrorMessage(pushRequest.ObjectType, pushRequest.SessionGuid,
                                         pushRequestArgs.RequestInfo.Client, SyncType.ClientPush,
                                         body);
                    }
                }

                PopulateAndSendPushResponse(acceptedIdList, resolvedIdList, deletedIdList, pushRequest,
                                            changeIdInfoList,
                                            requestInfo, pushResponses);
                transaction.Complete();

                if (SyncModule.EnablePerformanceLogs)
                {
                    var header = string.Format(
                        "Transaction (IsolationLevel.ReadCommitted) completed by thread id: {0}",
                        Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture));
                    var body = string.Format(
                        "Method: ProcessPushRequest, Object Type: {0}, ClientId: {1}, CredentialId: {2}, Sync Session Id: {3}",
                        pushRequest.ObjectType,
                        pushRequestArgs.RequestInfo.Client.ClientID,
                        pushRequestArgs.RequestInfo.Client.CredentialId,
                        pushRequest.SessionGuid);
                    LogHelper.LogDebug(SyncModule, LogId.SYNC_PUSH_D_8, "Performance Logs", header, body);
                }
            }

            EntitiesChanged.Invoke(this,
                new EntitiesChangedEventArgs()
                {
                    ChangeArgs = changeEventArgsList,
                    Client = requestInfo.Client
                });

            // Since calling SendMessageWhenReady may block it must be called after the db transaction is complete
            foreach (var pushResponse in pushResponses)
            {
                Dispatcher.SendMessageWhenReady(pushResponse, SyncModule, requestInfo.Client);
            }

            if (SyncModule.EnablePerformanceLogs)
            {
                LogHelper.LogDebug(
                    SyncModule,
                    LogId.SYNC_PUSH_D_6,
                    "Performance Logs",
                    "Push responses sending completed",
                    string.Format(
                        "Method: ProcessPushRequest, Object Type: {0}, ClientId: {1}, CredentialId: {2}, Sync Session Id: {3}",
                        pushRequest.ObjectType,
                        pushRequestArgs.RequestInfo.Client.ClientID,
                        pushRequestArgs.RequestInfo.Client.CredentialId,
                        pushRequest.SessionGuid));
            }
        }

        private Tuple<ResolvedObject, ChangeEventArgs> UpdateISynchronizableEntity(PushRequest pushRequest, SyncEntityData syncEntityData, RequestInfo requestInfo)
        {
            var resolvedObj = new ResolvedObject();
            Type type = typeof(T);

            ISynchronizable customEntity;
            IEnumerable<PropertyInfo> properties;
            EntityMetadata metadata;

            ChangeEventArgs changeEventArgs = null;

            switch (syncEntityData.GetStatus)
            {
                case Operation.Created:

                    if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                    {
                        customEntity = (ISynchronizable)JsonSerializer.Deserialize(syncEntityData.Object, type, null);
                    }
                    else
                    {
                        customEntity = (ISynchronizable)XSerializer.DeserializeSyncEntity(syncEntityData.Object, type);
                    }

                    // Since the ISynchronizableEntity Guid is not marked with the Synchronizable attribute , the client doesn't send it along with the 
                    // object data, but this Guid is in the SyncObject , so before creating the new custom object at the server we need to assign the Guid to the 
                    // custom object. This is not needed at the update since we get the object from storage and fill the needed properties. 
                    customEntity.Guid = syncEntityData.Guid;
                    properties = PropertyHelper.GetProperties(type);

                    Storage.Create(customEntity, new CreateArgs(typeof(SynchronizationModule).Name, requestInfo.Client.CredentialId));
                    resolvedObj = InsertMetaInformation(pushRequest, syncEntityData, properties, requestInfo.Client.ClientID);

                    changeEventArgs = new ChangeEventArgs()
                    {
                        Change = Operation.Created,
                        CredentialId = requestInfo.Client.CredentialId,
                        EntityType = typeof(T).Name,
                        Id = customEntity.Guid,
                        New = customEntity,
                        Old = null,
                        OriginatorType = typeof(SynchronizationModule).Name
                    };

                    break;

                case Operation.Updated:

                    // if(metadata == null) means the metadata related to the given Id existed and be completely deleted from DB using maintenance SP 
                    // or that given Id may never exist.
                    metadata = SyncDao.GetMetadata(syncEntityData.Guid, true);

                    // This is an exceptional scenario where the receiving sync object may contain status = 2 (updated) and the version = 0(created on the client)
                    // so that means this object is created on the client and then updated before pushing to the server. For such objects GetMetadata() returns null. 
                    // With that we can identify that this object should be inserted.
                    if (syncEntityData.Version == 0 && metadata == null)
                    {
                        if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                        {
                            customEntity = (ISynchronizable)JsonSerializer.Deserialize(syncEntityData.Object, type, null);
                        }
                        else
                        {
                            customEntity = (ISynchronizable)XSerializer.DeserializeSyncEntity(syncEntityData.Object, type);
                        }

                        customEntity.Guid = syncEntityData.Guid;
                        properties = PropertyHelper.GetProperties(type);

                        Storage.Create(customEntity, new CreateArgs(typeof(SynchronizationModule).Name, requestInfo.Client.CredentialId));
                        resolvedObj = InsertMetaInformation(pushRequest, syncEntityData, properties, requestInfo.Client.ClientID);

                        changeEventArgs = new ChangeEventArgs()
                        {
                            Change = Operation.Created,
                            CredentialId = requestInfo.Client.CredentialId,
                            EntityType = typeof(T).Name,
                            Id = customEntity.Guid,
                            New = customEntity,
                            Old = null,
                            OriginatorType = typeof(SynchronizationModule).Name
                        };
                    }
                    else
                    {
                        if (metadata == null || metadata.IsDeleted)
                        {
                            // This means the object that the client is trying to update is already deleted at the server.
                            resolvedObj = new ResolvedObject
                                {
                                    ObjectId = syncEntityData.Guid,
                                    IsDeleted = true,
                                    IsServerWin = true
                                };
                        }
                        else
                        {
                            // If the last change is from same device, we don't need to invoke the conflict resolver. 
                            if (metadata.Version > syncEntityData.Version && metadata.LastChangeClientId != requestInfo.Client.ClientID)
                            {
                                var result = ResolveConflict(pushRequest, syncEntityData, requestInfo, type);
                                resolvedObj = result.Item1;

                                changeEventArgs = result.Item2;

                                break;
                            }

                            // Client wins
                            // On this occasion we need to get the existing object before it's updated , since we need to compare for updated properties.
                            ISynchronizable oldEntity = Storage.Read(new List<Guid> { syncEntityData.Guid }).DefaultIfEmpty(null).First();

                            IEnumerable<Type> synchronizablePropertyTypes = PropertyHelper.GetProperties(typeof(T)).Select(prop => prop.PropertyType).ToArray();

                            ISynchronizable merged =
                                 (ISynchronizable)
                                 JsonSerializer.Deserialize(JsonSerializer.Serialize(oldEntity, synchronizablePropertyTypes),
                                                            oldEntity.GetType(), null);
                            ISynchronizable newEntity;

                            if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                            {
                                newEntity =
                                    (ISynchronizable)
                                    JsonSerializer.Deserialize(syncEntityData.Object, type, null);
                            }
                            else
                            {
                                newEntity =
                                    (ISynchronizable)XSerializer.DeserializeSyncEntity(syncEntityData.Object, type);
                            }

                            if (metadata.LastChangeClientId == requestInfo.Client.ClientID)
                            {
                                // If the same is from same client, we don't need to compare the property version. 
                                MergeServerEntity(syncEntityData, merged, newEntity, false);
                            }
                            else
                            {
                                MergeServerEntity(syncEntityData, merged, newEntity, true);
                            }

                            changeEventArgs = new ChangeEventArgs()
                            {
                                Change = Operation.Updated,
                                CredentialId = requestInfo.Client.CredentialId,
                                EntityType = typeof(T).Name,
                                Id = syncEntityData.Guid,
                                New = merged,
                                Old = oldEntity,
                                OriginatorType = typeof(SynchronizationModule).Name
                            };

                            Storage.Update(merged, new UpdateArgs(typeof(SynchronizationModule).Name, requestInfo.Client.CredentialId));

                            UpdateMetaInformation(pushRequest.ObjectType, syncEntityData.Guid, syncEntityData.ChangedPropertyNames, requestInfo.Client.ClientID);

                            resolvedObj = new ResolvedObject
                                {
                                    ObjectId = syncEntityData.Guid,
                                    IsServerWin = false,
                                    IsDeleted = false
                                };
                        }
                    }

                    PopulateChangedProperties(syncEntityData, resolvedObj);
                    break;

                case Operation.Deleted:

                    // if(metadata == null) means the metadata related to the given Id existed and be completely deleted from DB using maintenance SP 
                    // or that given Id may never exist. Here we can decide this entity is already deleted from the DB. 
                    metadata = SyncDao.GetMetadata(syncEntityData.Guid, true);

                    if (metadata == null || metadata.IsDeleted)
                    {
                        resolvedObj = new ResolvedObject { ObjectId = syncEntityData.Guid, IsDeleted = true, IsServerWin = true };
                    }
                    else
                    {
                        // If the last change is from same device, we don't need to resolve the object. Just delete it. 
                        if (metadata.Version > syncEntityData.Version && metadata.LastChangeClientId != requestInfo.Client.ClientID)
                        {
                            // Cannot delete the object since it has a newer version on the server . So we need to send it as a resolved object. 
                            // Server wins here.
                            resolvedObj = new ResolvedObject
                                {
                                    ObjectId = syncEntityData.Guid,
                                    IsServerWin = true,
                                    IsDeleted = false
                                };
                        }
                        else
                        {
                            changeEventArgs = new ChangeEventArgs()
                            {
                                Change = Operation.Deleted,
                                CredentialId = requestInfo.Client.CredentialId,
                                EntityType = typeof(T).Name,
                                Id = syncEntityData.Guid,
                                New = null,
                                Old = Storage.Read(new[] { syncEntityData.Guid }).DefaultIfEmpty(null).First(),
                                OriginatorType = typeof(SynchronizationModule).Name
                            };

                            Storage.Delete(syncEntityData.Guid, new DeleteArgs(typeof(SynchronizationModule).Name, requestInfo.Client.CredentialId));
                            resolvedObj = DeleteMetaInformation(pushRequest, syncEntityData, requestInfo.Client.ClientID);
                        }
                    }

                    break;
            }

            return new Tuple<ResolvedObject, ChangeEventArgs>(resolvedObj, changeEventArgs);
        }

        private Tuple<ResolvedObject, ChangeEventArgs> ResolveConflict(PushRequest pushRequest, SyncEntityData syncEntityData, RequestInfo requestInfo, Type type)
        {
            string platformType = requestInfo.Client.MetaData.Platform.Type;
            int credentialId = requestInfo.Client.CredentialId;

            ISynchronizable oldEntity = Storage.Read(new List<Guid> { syncEntityData.Guid }).DefaultIfEmpty(null).First();

            // Since ISynchronizable is not implementing IClone , its not possible to get a clone of an object.
            ISynchronizable merged;
            ISynchronizable newEntity;

            if (SynchronizationModule.AndroidPlatform == platformType)
            {
                IEnumerable<Type> synchronizablePropertyTypes = PropertyHelper.GetProperties(typeof(T)).Select(prop => prop.PropertyType).ToArray();

                merged =
                    (ISynchronizable)
                    JsonSerializer.Deserialize(JsonSerializer.Serialize(oldEntity, synchronizablePropertyTypes),
                                               oldEntity.GetType(), null);

                newEntity =
                    (ISynchronizable)JsonSerializer.Deserialize(syncEntityData.Object, type, null);
            }
            else
            {
                merged = (ISynchronizable)XSerializer.DeserializeSyncEntity(XSerializer.SerializeSyncEntity(oldEntity), oldEntity.GetType());

                newEntity =
                    (ISynchronizable)XSerializer.DeserializeSyncEntity(syncEntityData.Object, type);
            }

            MergeServerEntity(syncEntityData, merged, newEntity, false);

            ISynchronizable resolved;

            if (ConflictResolutionDelegate != null)
            {
                resolved =
                    ConflictResolutionDelegate.Invoke(new ConflictInfo<T>
                        {
                            OldValue = (T)oldEntity,
                            NewValue = (T)merged,
                            Change = Operation.Updated
                        });

                if (resolved == null)
                {
                    throw new Exception("Return entity of the Conflict Resolution Delegate cannot be null");
                }
            }
            else
            {
                // Since there is no conflict resolver registered, we select merge object as the resolved object.
                resolved = merged;
            }

            // We have to update both server and client
            var changedProperties = new List<string>();
            var allProperties = PropertyHelper.GetProperties(resolved.GetType());

            ResolvedObject resolvedObj = new ResolvedObject { ObjectId = syncEntityData.Guid, IsDeleted = false, IsServerWin = true };

            if (SyncModule.EnablePropertySynchronization)
            {
                List<string> changedOldProperties = GetNotEqualProperties(allProperties, resolved, oldEntity);
                List<string> changedNewProperties = GetNotEqualProperties(allProperties, resolved, merged);

                // Server version has won
                if (!changedOldProperties.Any())
                {
                    PopulateChangedProperties(syncEntityData, resolvedObj);

                    return new Tuple<ResolvedObject, ChangeEventArgs>(resolvedObj, null);
                }

                changedProperties.AddRange(changedOldProperties);

                foreach (var property in changedNewProperties.Where(property => !changedProperties.Contains(property)))
                {
                    changedProperties.Add(property);
                }

                PopulateChangedProperties(syncEntityData, resolvedObj);
            }
            else
            {
                // Since the property synchronization is disabled we send all the properties
                foreach (var property in allProperties)
                {
                    resolvedObj.ChangedPropertyNames.Add(property.Name);
                }

                // and in the below UpdateMetaInformation is updating the properties so that the all properties will get the same version.
            }

            ChangeEventArgs changeEventArgs = new ChangeEventArgs()
            {
                CredentialId = credentialId,
                Change = Operation.Updated,
                EntityType = typeof(T).Name,
                Id = syncEntityData.Guid,
                New = resolved,
                Old = oldEntity,
                OriginatorType = typeof(SynchronizationModule).Name
            };

            Storage.Update(resolved, new UpdateArgs(typeof(SynchronizationModule).Name, credentialId));

            // Here we have to use a new guid for the LastCahngeClientId, because resolve object contains both client and server changes. 
            UpdateMetaInformation(pushRequest.ObjectType, syncEntityData.Guid, changedProperties, Guid.NewGuid().ToString());

            return new Tuple<ResolvedObject, ChangeEventArgs>(resolvedObj, changeEventArgs);
        }

        private void PopulateAndSendPushResponse(ICollection<Guid> acceptedIdList, List<ResolvedObject> resolvedList, IEnumerable<Guid> deletedIdList,
                                         PushRequest pushRequest, List<SyncMetadata> changeIdInfoList, RequestInfo requestInfo, List<PushResponse> pushResponses)
        {
            PushResponse pushResponse = new PushResponse();
            pushResponse.SessionGuid = pushRequest.SessionGuid;
            pushResponse.ObjectType = pushRequest.ObjectType;

            // here we need the metadata related to the deleted objects, pass "true" to GetMetadata()
            List<EntityMetadata> acceptedMetadataList = SyncDao.GetMetadata(acceptedIdList, true, true);

            int currentSize = SendAcceptedEntities(acceptedMetadataList, pushResponse, changeIdInfoList, requestInfo.Client.MetaData.Platform.Type, pushResponses);

            var resolvedSyncObjList = new List<SyncEntityData>();

            // since we don't need metadata related to deleted objects , pass "false" to GetMetadata()
            var resolvedMetadataList = SyncDao.GetMetadata(resolvedList.Select(item => item.ObjectId).ToList(), false, true);

            var requestingObjectIdList =
                resolvedList.Select(item => item.ObjectId).ToList();

            ICollection<ISynchronizable> resolvedObjectList = Storage.Read(requestingObjectIdList);

            PopulateResolvedObjectList(resolvedList, resolvedMetadataList, resolvedObjectList, resolvedSyncObjList, changeIdInfoList, requestInfo.Client.MetaData.Platform.Type);

            // Here we need to add the deleted objects as resolved objects. The scenario is that the client tries to update|delete an object that is already deleted at the 
            // server. So this should be sent as resolved objects, but the info in these objects are the Guids and the statuses only.
            resolvedSyncObjList.AddRange(deletedIdList.Select(deletedObj => new SyncEntityData { Guid = deletedObj, Status = (int)Operation.Deleted, ChangeId = FindChangeId(changeIdInfoList, deletedObj) }));

            for (int r = 0; r < resolvedSyncObjList.Count; r++)
            {
                #region Handling large objects

                int syncObjectSize;

                if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                {
                    syncObjectSize = JsonSerializer.Serialize(resolvedSyncObjList[r], null).Length;
                }
                else
                {
                    syncObjectSize = JsonSerializer.Serialize(resolvedSyncObjList[r], null).Length;
                }

                if (syncObjectSize > MaxDataPacketSize)
                {
                    CreatePartialObjectsForPushResponse(pushResponse, resolvedSyncObjList[r], pushResponses);
                    continue;
                }

                #endregion

                pushResponse.IsSplit = false;
                pushResponse.PacketNumber = 0;
                pushResponse.TotalNumberOfPackets = 0;

                if ((MaxDataPacketSize - currentSize) >= syncObjectSize)
                {
                    pushResponse.ResolvedObjects.Add(resolvedSyncObjList[r]);
                    currentSize += syncObjectSize;
                }
                else
                {
                    r--;
                    pushResponses.Add(Clone(pushResponse));
                    pushResponse.ResolvedObjects.Clear();
                    currentSize = 0;
                }
            }

            if (pushResponse.ResolvedObjects.Any() || pushResponse.AcceptedObjects.Any())
            {
                pushResponses.Add(Clone(pushResponse));
            }
        }

        private void PopulateResolvedObjectList(List<ResolvedObject> resolvedList,
                                                IEnumerable<EntityMetadata> resolvedMetadataList,
                                                IEnumerable<ISynchronizable> resolvedObjectList,
                                                ICollection<SyncEntityData> resolvedSyncObjList,
                                                List<SyncMetadata> changeIdInfoList, string platformType)
        {
            if (resolvedMetadataList != null && resolvedObjectList != null)
            {
                var combinedList = from m in resolvedMetadataList
                                   join b in resolvedObjectList on m.Guid equals b.Guid
                                   select new
                                       {
                                           m.Guid,
                                           m.Version,
                                           Type = m.EntityType,
                                           b
                                       };

                foreach (var mdata in combinedList)
                {
                    ////here we need to get the version of the sent object from client in order to get the updated properties on the server.
                    ResolvedObject resolvedObj = resolvedList.First(item => item.ObjectId == mdata.Guid);

                    SyncEntityData syncEntityData = new SyncEntityData();

                    if (SynchronizationModule.AndroidPlatform == platformType)
                    {
                        var helper = new SerializationHelper(mdata.b, resolvedObj.ChangedPropertyNames.ToList());

                        IEnumerable<Type> synchronizablePropertyTypes =
                            PropertyHelper.GetProperties(typeof(T)).Select(prop => prop.PropertyType).ToArray();
                        syncEntityData.Object = JsonSerializer.Serialize(helper, synchronizablePropertyTypes);
                    }
                    else
                    {
                        syncEntityData.Object = XSerializer.SerializeSyncEntity(mdata.b);
                    }

                    syncEntityData.Version = mdata.Version;
                    syncEntityData.Guid = mdata.Guid;
                    syncEntityData.ChangeId = FindChangeId(changeIdInfoList, mdata.Guid);

                    foreach (var propertyName in resolvedObj.ChangedPropertyNames)
                    {
                        syncEntityData.ChangedPropertyNames.Add(propertyName);
                    }

                    resolvedSyncObjList.Add(syncEntityData);
                }
            }
        }

        private PushResponse Clone(PushResponse pushResponse)
        {
            var clone = new PushResponse();
            foreach (var item in pushResponse.AcceptedObjects)
            {
                clone.AcceptedObjects.Add(Clone(item));
            }

            foreach (var item in pushResponse.ResolvedObjects)
            {
                clone.ResolvedObjects.Add(Clone(item));
            }

            clone.IsSplit = pushResponse.IsSplit;
            clone.ObjectType = pushResponse.ObjectType;
            clone.PacketNumber = pushResponse.PacketNumber;
            clone.SessionGuid = pushResponse.SessionGuid;
            clone.TotalNumberOfPackets = pushResponse.TotalNumberOfPackets;

            return clone;
        }

        private SyncMetadata Clone(SyncMetadata syncMetadata)
        {
            var clone = new SyncMetadata();

            clone.ChangeId = syncMetadata.ChangeId;
            clone.Guid = syncMetadata.Guid;
            clone.Status = syncMetadata.Status;
            clone.Version = syncMetadata.Version;

            return clone;
        }

        private SyncEntityData Clone(SyncEntityData syncEntityData)
        {
            var clone = new SyncEntityData();

            foreach (var name in syncEntityData.ChangedPropertyNames)
            {
                clone.ChangedPropertyNames.Add(name);
            }

            clone.ChangeId = syncEntityData.ChangeId;
            clone.Guid = syncEntityData.Guid;
            clone.Object = syncEntityData.Object;
            clone.Status = syncEntityData.Status;
            clone.Version = syncEntityData.Version;

            return clone;
        }

        private void CreatePartialObjectsForPushResponse(PushResponse pushResponse, SyncEntityData syncEntityData, List<PushResponse> pushResponses)
        {
            // TODO : The code segment below can be replaced by using "SplitSerializedEntity()" method implemented in SynchronizationManager
            int innerObjectSize = syncEntityData.Object.Length;
            var numberOfPackets = GetNumberOfPackets(innerObjectSize, MaxDataPacketSize);

            pushResponse.IsSplit = true;
            pushResponse.TotalNumberOfPackets = numberOfPackets;

            SyncEntityData syncObj = new SyncEntityData { Guid = syncEntityData.Guid, Version = syncEntityData.Version };

            foreach (var property in syncEntityData.ChangedPropertyNames)
            {
                syncObj.ChangedPropertyNames.Add(property);
            }

            for (int p = 0; p < numberOfPackets - 1; p++)
            {
                pushResponse.ResolvedObjects.Clear();
                syncObj.Object = syncEntityData.Object.Substring(p * MaxDataPacketSize, MaxDataPacketSize);
                pushResponse.ResolvedObjects.Add(syncObj);
                pushResponse.PacketNumber = p;

                pushResponses.Add(Clone(pushResponse));
            }

            pushResponse.ResolvedObjects.Clear();

            syncObj.Object = syncEntityData.Object.Substring((numberOfPackets - 1) * MaxDataPacketSize,
                                                         innerObjectSize - ((numberOfPackets - 1) * MaxDataPacketSize));
            pushResponse.ResolvedObjects.Add(syncObj);
            pushResponse.PacketNumber = numberOfPackets - 1;
            pushResponses.Add(Clone(pushResponse));

            pushResponse.ResolvedObjects.Clear();
        }

        private Guid FindChangeId(IEnumerable<SyncMetadata> changeIdInfoList, Guid entityId)
        {
            Guid changeId = Guid.Empty;
            foreach (SyncMetadata syncMetadata in changeIdInfoList)
            {
                if (syncMetadata.Guid == entityId)
                {
                    return syncMetadata.ChangeId;
                }
            }

            return changeId;
        }

        private int SendAcceptedEntities(IList<EntityMetadata> acceptedMetadataList,
                                         PushResponse pushResponse, List<SyncMetadata> changeIdInfoList,
                                         string platformType, List<PushResponse> pushResponses)
        {
            int currentSize = 0;

            for (int n = 0; n < acceptedMetadataList.Count; n++)
            {
                SyncMetadata acceptedObj = new SyncMetadata
                    {
                        Version = acceptedMetadataList[n].Version,

                        // Status is set here in order to identify the deleted objects that has been accepted by the client , in the push response 
                        Status = acceptedMetadataList[n].IsDeleted ? (int)Operation.Deleted : (int)Operation.None,
                        Guid = acceptedMetadataList[n].Guid,
                        ChangeId = FindChangeId(changeIdInfoList, acceptedMetadataList[n].Guid)
                    };

                string serializedString;

                if (SynchronizationModule.AndroidPlatform == platformType)
                {
                    serializedString = JsonSerializer.Serialize(acceptedObj, null);
                }
                else
                {
                    serializedString = XSerializer.SerializeSyncEntity(acceptedObj);
                }

                int stringSize = serializedString.Length;

                if ((MaxDataPacketSize - currentSize) >= stringSize)
                {
                    pushResponse.AcceptedObjects.Add(acceptedObj);
                    currentSize += stringSize;
                }
                else
                {
                    n--;
                    pushResponses.Add(Clone(pushResponse));
                    pushResponse.AcceptedObjects.Clear();
                    currentSize = 0;
                }
            }

            return currentSize;
        }

        private static void ManageResolvedObject(ResolvedObject resolvedObj, List<Guid> deletedIdList, List<ResolvedObject> resolvedIdList,
                                                  ICollection<Guid> acceptedIdList)
        {
            if (resolvedObj.IsServerWin)
            {
                if (resolvedObj.IsDeleted)
                {
                    deletedIdList.Add(resolvedObj.ObjectId);
                }
                else
                {
                    resolvedIdList.Add(resolvedObj);
                }
            }
            else
            {
                acceptedIdList.Add(resolvedObj.ObjectId);
            }
        }

        private ResolvedObject InsertMetaInformation(PushRequest pushRequest, SyncEntityData syncEntityData, IEnumerable<PropertyInfo> properties, string lastChangeClientId)
        {
            SyncDao.InsertSyncMetadata(new ChangeEventArgs
            {
                Id = syncEntityData.Guid,
                Change = Operation.Created,
                EntityType = pushRequest.ObjectType
            }, lastChangeClientId);

            if (SyncModule.EnablePropertySynchronization)
            {
                try
                {
                    SyncDao.InsertPropertyMetadata(properties, syncEntityData.Guid);
                }
                catch (SqlException ex)
                {
                   LogHelper.LogError(
                                SyncModule,
                                 LogId.SYNC_PUSH_E_1,
                                "Failed to insert property meta data",
                                "Exception when trying to insert property meta data, Entity id " + syncEntityData.Guid,
                                ex.ToString());
                    throw;
                }
            }

            ResolvedObject resolvedObj = new ResolvedObject { ObjectId = syncEntityData.Guid, IsDeleted = false, IsServerWin = false };

            return resolvedObj;
        }

        private void UpdateMetaInformation(string objectType, Guid guid, IEnumerable<string> changedProperties, string lastChangeClientId)
        {
            if (SyncModule.EnablePropertySynchronization)
            {
                SyncDao.UpdateMetadataWithPropertyMetadata(new ChangeEventArgs
                {
                    Id = guid,
                    Change = Operation.Updated,
                    EntityType = objectType
                }, changedProperties, lastChangeClientId);
            }
            else
            {
                SyncDao.UpdateSyncMetadata(new ChangeEventArgs
                {
                    Id = guid,
                    Change = Operation.Updated,
                    EntityType = objectType
                }, lastChangeClientId);
            }
        }

        private ResolvedObject DeleteMetaInformation(PushRequest pushRequest, SyncEntityData syncEntityData, string lastChangeClientId)
        {
            SyncDao.DeleteSyncMetadata(new ChangeEventArgs
            {
                Id = syncEntityData.Guid,
                Change = Operation.Deleted,
                EntityType = pushRequest.ObjectType
            }, lastChangeClientId);

            if (SyncModule.EnablePropertySynchronization)
            {
                SyncDao.DeletePropertyMetadata(syncEntityData.Guid);
            }

            ResolvedObject resolvedObj = new ResolvedObject { ObjectId = syncEntityData.Guid, IsDeleted = true, IsServerWin = false };
            return resolvedObj;
        }

        private void MergeServerEntity(SyncEntityData syncEntityData, ISynchronizable server,
                                               ISynchronizable client, bool comparePropertyVersion)
        {
            if (SyncModule.EnablePropertySynchronization)
            {
                if (!comparePropertyVersion)
                {
                    foreach (var propertyName in syncEntityData.ChangedPropertyNames)
                    {
                        PropertyInfo property = server.GetType().GetProperty(propertyName);

                        property.SetValue(server, property.GetValue(client, null), null);
                    }
                }
                else
                {
                    foreach (var propertyName in syncEntityData.ChangedPropertyNames)
                    {
                        PropertyMetadata propertyData = SyncDao.GetPropertyMetadata(syncEntityData.Guid, propertyName);

                        if (syncEntityData.Version >= propertyData.Version)
                        {
                            PropertyInfo property = server.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).First(
                                item => item.Name == propertyName);

                            property.SetValue(server, property.GetValue(client, null), null);
                        }
                    }
                }
            }
            else
            {
                // since we don't need to compare the properties , just assign the client to server entity.
                // server entity will be used as the merged entity.
                foreach (var property in server.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    property.SetValue(server, property.GetValue(client, null), null);
                }
            }
        }

        private static void PopulateChangedProperties(SyncEntityData syncEntityData, ResolvedObject resolvedObj)
        {
            IList<PropertyMetadata> changedProperties = SyncDao.GetNewPropertyMetadata(syncEntityData.Guid, syncEntityData.Version);
            foreach (var property in changedProperties)
            {
                resolvedObj.ChangedPropertyNames.Add(property.Name);
            }

            foreach (
                var sentProperty in
                    syncEntityData.ChangedPropertyNames.Where(sentProperty => !resolvedObj.ChangedPropertyNames.Contains(sentProperty)))
            {
                resolvedObj.ChangedPropertyNames.Add(sentProperty);
            }
        }

        private static List<string> GetNotEqualProperties(IEnumerable<PropertyInfo> allProperties, ISynchronizable x, ISynchronizable y)
        {
            List<string> notEqualProperties = new List<string>();

            foreach (var property in allProperties)
            {
                object xProperty = property.GetValue(x, null);
                object yProperty = property.GetValue(y, null);

                if (xProperty == null && yProperty == null)
                {
                    continue;
                }

                if (xProperty == null || yProperty == null)
                {
                    notEqualProperties.Add(property.Name);
                }
                else
                {
                    if (!property.GetValue(x, null).Equals(property.GetValue(y, null)))
                    {
                        notEqualProperties.Add(property.Name);
                    }
                }
            }

            return notEqualProperties;
        }

        #endregion

        public event EventHandler<EntitiesChangedEventArgs> EntitiesChanged;
    }
}
