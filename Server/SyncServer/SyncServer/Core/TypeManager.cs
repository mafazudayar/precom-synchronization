﻿using System.Collections.Generic;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Login;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
	internal class TypeManager
	{
		#region Fields

	    private readonly Dictionary<string, TypeInformation> _typeDictionary;
	    private readonly PreComBase _syncModule;
	    private readonly LogHelper _logHelper;

		#endregion

		#region Constructors

		public TypeManager(PreComBase syncModule, ILog log)
		{
            _typeDictionary = new Dictionary<string, TypeInformation>();
		    _syncModule = syncModule;
            _logHelper = new LogHelper(log);
		}

		#endregion

        #region Methods

        internal void AddType<T>(TypeInformation typeInformation) where T : ISynchronizable
        {
            _typeDictionary.Add(typeof(T).Name, typeInformation);
        }

        internal void RemoveType<T>() where T : ISynchronizable
        {
            _typeDictionary.Remove(typeof(T).Name);
        }

	    internal bool TypeExists<T>() where T : ISynchronizable
	    {
	        return _typeDictionary.ContainsKey(typeof(T).Name);
	    }

	    internal void AddConflictResolutionDelegate<T>(ConflictResolutionDelegate<T> conflictResolutionDelegate)
	        where T : ISynchronizable
	    {
	        TypeInformation typeInformation;
	        _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

	        if (typeInformation != null)
	        {
	            ((ClientPushManager<T>)typeInformation.PushManager).ConflictResolutionDelegate =
	                conflictResolutionDelegate;
	        }
	    }

        internal void RemoveConflictResolutionDelegate<T>()
           where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((ClientPushManager<T>)typeInformation.PushManager).ConflictResolutionDelegate = null;
            }
        }

	    internal void AddAutomaticPushDelegate<T>(AutomaticPushDelegateInformation info) where T : ISynchronizable
	    {
	        TypeInformation typeInformation;
	        _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

	        if (typeInformation != null)
	        {
	            var entityUpdateManager = (EntityUpdateManager<T>)typeInformation.EntityUpdateManager;
	            if (entityUpdateManager.PushDelegateInformation != null)
	            {
	                var str = string.Format(
	                    "Automatic push delegate for {0} is already registered and will be overwritten",
	                    typeof(T).Name);
	                _logHelper.LogWarning(_syncModule, LogId.SYNC_TM_W_1, "TypeManager", str);
	            }

	            entityUpdateManager.PushDelegateInformation = info;
	        }
	    }

	    internal void RemoveAutomaticPushDelegate<T>() where T : ISynchronizable
	    {
	        TypeInformation typeInformation;
	        _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

	        if (typeInformation != null)
	        {
	            var entityUpdateManager = (EntityUpdateManager<T>)typeInformation.EntityUpdateManager;
	            if (entityUpdateManager.PushDelegateInformation != null)
	            {
	                entityUpdateManager.PushDelegateInformation = null;
	            }
	            else
	            {
	                var str = string.Format(
	                    "Automatic push delegate for {0} is already unregistered.",
	                    typeof(T).Name);
	                _logHelper.LogWarning(_syncModule, LogId.SYNC_TM_W_1, "TypeManager", str);
	            }
	        }
	    }

	    internal void AddLoginDelegate<T>(LoginFilterDelegate filter) where T : ISynchronizable
	    {
	        TypeInformation typeInformation;
	        _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

	        if (typeInformation != null)
	        {
	            ((LoginManager<T>)typeInformation.LoginManager).DelegateFilters.Add(filter);
	        }
	    }

        public void AddLoginDelegate<T>(LoginFilterDelegate filter, LoginStatus trigger) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((LoginManager<T>)typeInformation.LoginManager).AddLoginStatusDelegateFilter(trigger, filter);
            }
        }

        internal void RemoveLoginDelegate<T>(LoginFilterDelegate filter) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((LoginManager<T>)typeInformation.LoginManager).DelegateFilters.Remove(filter);
            }
        }

        public void RemoveLoginDelegate<T>(LoginStatus trigger) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((LoginManager<T>)typeInformation.LoginManager).RemoveLoginStatusDelegateFilter(trigger);
            }
        }

	    public void AddSyncEventNotifier<T>(SyncEventDelegate notifier) where T : ISynchronizable
	    {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
                ((LoginManager<T>)typeInformation.LoginManager).SyncEventNotifier += notifier;
                ((EntityUpdateManager<T>)typeInformation.EntityUpdateManager).SyncEventNotifier += notifier;
                ((ClientPullManager<T>)typeInformation.PullManager).SyncEventNotifier += notifier;
                ((ClientPushManager<T>)typeInformation.PushManager).SyncEventNotifier += notifier;
            }
        }

        public void RemoveSyncEventNotifier<T>(SyncEventDelegate notifier) where T : ISynchronizable
        {
            TypeInformation typeInformation;
            _typeDictionary.TryGetValue(typeof(T).Name, out typeInformation);

            if (typeInformation != null)
            {
// ReSharper disable once DelegateSubtraction
                ((LoginManager<T>)typeInformation.LoginManager).SyncEventNotifier -= notifier;
// ReSharper disable once DelegateSubtraction
                ((EntityUpdateManager<T>)typeInformation.EntityUpdateManager).SyncEventNotifier -= notifier;
// ReSharper disable once DelegateSubtraction
                ((ClientPullManager<T>)typeInformation.PullManager).SyncEventNotifier -= notifier;
                ((ClientPushManager<T>)typeInformation.PushManager).SyncEventNotifier -= notifier;
            }
        }

        #endregion
	}
}
