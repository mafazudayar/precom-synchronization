using System;
using System.Collections.Generic;
using System.Threading;

namespace PreCom.Synchronization.Core
{
    internal class UserClientResetEvents
    {
        private readonly int _waitTimeoutInSeconds;

        private readonly Dictionary<Tuple<int, string>, ManualResetEventSlim> _userClientResetEvents = new Dictionary<Tuple<int, string>, ManualResetEventSlim>();
        private readonly Dictionary<int, UserResetEvent> _userResetEvents = new Dictionary<int, UserResetEvent>();

        public UserClientResetEvents(int waitTimeoutInSeconds)
        {
            _waitTimeoutInSeconds = waitTimeoutInSeconds;
        }

        public void Set(int credentialId, string clientId)
        {
            var userClientResetEvent = GetResetEvent(credentialId, clientId);
            userClientResetEvent.Set();

            var userResetEvent = GetResetEvent(credentialId);
            userResetEvent.Set(clientId);
        }

        public void Reset(int credentialId, string clientId)
        {
            var userClientResetEvent = GetResetEvent(credentialId, clientId);
            userClientResetEvent.Reset();

            var userResetEvent = GetResetEvent(credentialId);
            userResetEvent.Reset(clientId);
        }

        public bool Wait(int credentialId, string clientId)
        {
            var userClientResetEvent = GetResetEvent(credentialId, clientId);
            return userClientResetEvent.Wait(new TimeSpan(0, 0, _waitTimeoutInSeconds));
        }

        public bool Wait(int credentialId)
        {
            var userResetEvent = GetResetEvent(credentialId);
            return userResetEvent.Wait(new TimeSpan(0, 0, _waitTimeoutInSeconds));
        }

        private ManualResetEventSlim GetResetEvent(int credentialId, string clientId)
        {
            ManualResetEventSlim resetEvent;
            lock (_userClientResetEvents)
            {
                var key = new Tuple<int, string>(credentialId, clientId);
                if (!_userClientResetEvents.TryGetValue(key, out resetEvent))
                {
                    resetEvent = new ManualResetEventSlim(false);
                    _userClientResetEvents.Add(key, resetEvent);
                }
            }

            return resetEvent;
        }

        private UserResetEvent GetResetEvent(int credentialId)
        {
            UserResetEvent resetEvent;
            lock (_userResetEvents)
            {
                if (!_userResetEvents.TryGetValue(credentialId, out resetEvent))
                {
                    resetEvent = new UserResetEvent(false);
                    _userResetEvents.Add(credentialId, resetEvent);
                }
            }

            return resetEvent;
        }
    }
}