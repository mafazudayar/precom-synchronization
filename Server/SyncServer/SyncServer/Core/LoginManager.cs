﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Login;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class LoginManager<T> : SynchronizationManager<T>, ILoginManager where T : ISynchronizable
    {
        #region Properties

        internal List<LoginFilterDelegate> DelegateFilters { get; set; }

        private Dictionary<LoginStatus, List<LoginFilterDelegate>> LoginStatusDelegateFilters { get; set; }

        #endregion

        #region Constructor

        internal LoginManager(SynchronizedStorageBase<T> storage, IDispatcher dispatcher, SynchronizationModule syncModule,
            int maxDataPacketSize, ILog log)
            : base(storage, syncModule, maxDataPacketSize, dispatcher, log)
        {
            DelegateFilters = new List<LoginFilterDelegate>();
            LoginStatusDelegateFilters = new Dictionary<LoginStatus, List<LoginFilterDelegate>>();

            dispatcher.Register(OnClientEventReasonChanged);
            dispatcher.Register(OnLoginStatusChanged);
        }

        #endregion

        #region Private Methods

        private void OnClientEventReasonChanged(Client client, ClientEventReason reason, ref ClientEventReply reply)
        {
            // Start from new thread to let login method complete quickly.
            // For Android clients this event is invoked synchronously.
            Task.Run(() => ExecuteLoginDelegate(client));
        }

        private void OnLoginStatusChanged(object sender, LoginStatusEventArgs loginStatusEventArgs)
        {
            Task.Run(() => ExecuteLoginStatusDelegates(loginStatusEventArgs));
        }

        private async Task ExecuteLoginDelegate(Client client)
        {
            try
            {
                Guid sessionGuid = Guid.NewGuid();

                if (DelegateFilters != null && DelegateFilters.Count > 0)
                {
                    foreach (var item in DelegateFilters)
                    {
                        InvokeLoginFilterDelegate(client, item, sessionGuid);
                    }
                }
                else
                {
                    PullInvokeMessage pullInvokeMsg = CreatePullInvokeMessage(typeof(T).Name, client.MetaData.Platform.Type, sessionGuid);
                    Dispatcher.SendMessage(pullInvokeMsg, client, SyncModule);
                }
            }
            catch (SynchronizedStorageException storageException)
            {
                LogHelper.LogError(SyncModule, storageException, LogId.SYNC_LM_E_2, "Storage exception when handling user login");

                SendErrorMessage(typeof(T).Name, Guid.Empty, client, SyncType.ServerPush, storageException);
            }
        }

        private async Task ExecuteLoginStatusDelegates(LoginStatusEventArgs loginStatusEventArgs)
        {
            LoginStatus loginStatus = loginStatusEventArgs.Status;

            if (loginStatus != LoginStatus.LoggedOut)
            {
                List<LoginFilterDelegate> loginStatusFilters;
                LoginStatusDelegateFilters.TryGetValue(loginStatus, out loginStatusFilters);

                var sessionGuid = Guid.NewGuid();

                if (loginStatusFilters != null && loginStatusFilters.Count > 0)
                {
                    var client = loginStatusEventArgs.Client;

                    foreach (var loginFilterDelegate in loginStatusFilters)
                    {
                        InvokeLoginFilterDelegate(client, loginFilterDelegate, sessionGuid);
                    }
                }
            }
        }

        private PullInvokeMessage CreatePullInvokeMessage(string type, string platformType, Guid sessionGuid)
        {
            var pullInvokeMsg = new PullInvokeMessage
            {
                ObjectType = type,
                InvokeReason = (int)PullInvokeReason.ClientLogin,
                SessionGuid = sessionGuid,
                PlatformType = platformType
            };

            return pullInvokeMsg;
        }

        private void InvokeLoginFilterDelegate(Client client, LoginFilterDelegate item, Guid sessionGuid)
        {
            var ids = new List<Guid>();
            string knowledge;

            try
            {
                var returnIds = item.Invoke(new LoginFilterArgs { Client = client });

                if (returnIds != null)
                {
                    if (SyncModule.EnablePerformanceLogs)
                    {
                        var body = string.Format(
                            "LoginFilterDelegate method '{0}' of the entity type {1} returns {2} entities",
                            item.Method.Name,
                            typeof(T).Name,
                            returnIds.Count);
                        var dump = string.Format("Client: {0}, credentialId: {1}, Platform: {2}",
                            client.ClientID,
                            client.CredentialId,
                            client.MetaData.Platform.Type);
                        LogHelper.LogDebug(SyncModule, LogId.SYNC_LM_D_1, "Performance Logs", body, dump);
                    }

                    ids.AddRange(returnIds.Distinct());
                }
                else
                {
                    if (SyncModule.EnablePerformanceLogs)
                    {
                        var body = string.Format(
                            "LoginFilterDelegate method '{0}' of the entity type {1} returns null",
                            item.Method.Name,
                            typeof(T).Name);
                        var dump = string.Format(
                            "Client: {0}, credentialId: {1}, Platform: {2}",
                            client.ClientID,
                            client.CredentialId,
                            client.MetaData.Platform.Type);
                        LogHelper.LogDebug(SyncModule, LogId.SYNC_LM_D_1, "Performance Logs", body, dump);
                    }
                }
            }
            catch (Exception exception)
            {
                LogHelper.LogError(SyncModule, exception, LogId.SYNC_LM_E_1, "Exception in login delegate");

                SendErrorMessage(typeof(T).Name, Guid.Empty, client, SyncType.ServerPush, exception);
            }

            if (SynchronizationModule.AndroidPlatform == client.MetaData.Platform.Type)
            {
                knowledge = JsonSerializer.Serialize(ids, null);
            }
            else
            {
                knowledge = XSerializer.Serialize(ids, typeof(List<Guid>));
            }

            if (knowledge.Length > MaxDataPacketSize)
            {
                // Need to correct the size
                var knowledgeSplits = SplitSerializedEntity(knowledge, MaxDataPacketSize);

                for (var i = 0; i < knowledgeSplits.Length; i++)
                {
                    // Need to create separate instances for PullInvokeMessage, if the send methods calls through separate threads. 
                    PullInvokeMessage pullInvokeMsg = CreatePullInvokeMessage(typeof(T).Name, client.MetaData.Platform.Type, sessionGuid);
                    pullInvokeMsg.Knowledge = knowledgeSplits[i];
                    pullInvokeMsg.IsSplit = true;
                    pullInvokeMsg.PacketNumber = i + 1;
                    pullInvokeMsg.TotalNumberOfPackets = knowledgeSplits.Length;
                    Dispatcher.SendMessage(pullInvokeMsg, client, SyncModule);
                }
            }
            else
            {
                PullInvokeMessage pullInvokeMsg = CreatePullInvokeMessage(typeof(T).Name, client.MetaData.Platform.Type, sessionGuid);
                pullInvokeMsg.Knowledge = knowledge;
                Dispatcher.SendMessage(pullInvokeMsg, client, SyncModule);
            }
        }
        #endregion

        #region Internal Methods

        internal void AddLoginStatusDelegateFilter(LoginStatus trigger, LoginFilterDelegate filter)
        {
            if (!LoginStatusDelegateFilters.ContainsKey(trigger))
            {
                LoginStatusDelegateFilters.Add(trigger, new List<LoginFilterDelegate>());
            }
            else if (LoginStatusDelegateFilters[trigger] == null)
            {
                LoginStatusDelegateFilters[trigger] = new List<LoginFilterDelegate>();
            }

            LoginStatusDelegateFilters[trigger].Add(filter);
        }

        internal void RemoveLoginStatusDelegateFilter(LoginStatus trigger)
        {
            if (LoginStatusDelegateFilters.ContainsKey(trigger))
            {
                LoginStatusDelegateFilters.Remove(trigger);
            }
        }

        #endregion
    }
}
