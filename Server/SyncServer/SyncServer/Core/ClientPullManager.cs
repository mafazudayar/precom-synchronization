﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class ClientPullManager<T> : SynchronizationManager<T>, IClientPullManager where T : ISynchronizable
    {
        #region Constructor

        internal ClientPullManager(SynchronizedStorageBase<T> storage, IDispatcher dispatcher, SynchronizationModule syncModule,
            int maxDataPacketSize, ILog log)
            : base(storage, syncModule, maxDataPacketSize, dispatcher, log)
        {
            Dispatcher.Register(PullRequestHandler);
        }

        #endregion

        #region Private fields

        private int _pullRequestCount;

        #endregion

        #region Private Methods

        private void PullRequestHandler(object sender, PullRequestEventArgs pullRequestArgs)
        {
            var isSplitRequest = false;
            var isDeltaOnly = false;

            var pullRequest = pullRequestArgs.PullRequest;
            var requestInfo = pullRequestArgs.RequestInfo;

            if (pullRequest.ObjectType != typeof(T).Name)
            {
                return;
            }

            var idList = new List<Guid>();
            var deletedObjectList = new List<Guid>();
            IEnumerable<SyncMetadata> difference = new List<SyncMetadata>();
            var serverMetadata = new List<EntityMetadata>();
            ICollection<ISynchronizable> sendObjectList = new ArraySegment<ISynchronizable>();
            var clientMetadata = new List<SyncMetadata>();

            try
            {
                if (pullRequest.IsSplit)
                {
                    isSplitRequest = true;

                    // We might need to check if the record about to insert doesn't exists in the DB before performing the following statement.
                    if (SyncDao.InsertPartialRequest(pullRequest.SessionGuid, pullRequest.PacketNumber,
                                                     pullRequest.Knowledge))
                    {
                        _pullRequestCount = SyncDao.GetPartialRequestCount(pullRequest.SessionGuid);
                    }

                    if (pullRequest.TotalNumberOfPackets == _pullRequestCount)
                    {
                        var pullRequestList = SyncDao.GetPartialRequests(pullRequest.SessionGuid);

                        // Initialize the StringBuilder with proper size.
                        var length = 0;
                        pullRequestList.ForEach(s => length += s.Length);
                        var knowledge = new StringBuilder(length);

                        foreach (var partialRequest in pullRequestList)
                        {
                            knowledge.Append(partialRequest);
                        }

                        if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                        {
                            clientMetadata = (List<SyncMetadata>)JsonSerializer.Deserialize(knowledge.ToString(), typeof(List<SyncMetadata>), null);
                        }
                        else
                        {
                            clientMetadata = (List<SyncMetadata>)XSerializer.DeserializeSyncEntity(knowledge.ToString(), typeof(List<SyncMetadata>));
                        }

                        SyncDao.DeletePartialRequest(pullRequest.SessionGuid);

                        isSplitRequest = false;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(pullRequest.Knowledge))
                    {
                        if (SynchronizationModule.AndroidPlatform == requestInfo.Client.MetaData.Platform.Type)
                        {
                            clientMetadata = (List<SyncMetadata>)JsonSerializer.Deserialize(pullRequest.Knowledge, typeof(List<SyncMetadata>), null);
                        }
                        else
                        {
                            clientMetadata = (List<SyncMetadata>)XSerializer.DeserializeSyncEntity(pullRequest.Knowledge, typeof(List<SyncMetadata>));
                        }
                    }
                }

                if (!isSplitRequest)
                {
                    List<SyncEntityData> metadataCollection;

                    using (var transaction = SyncModule.CreateTransaction(IsolationLevel.Snapshot,
                                                                          TransactionScopeOption.Required,
                                                                          SyncModule.PullRequestTransactionTimeout))
                    {
                        if (SyncModule.EnablePerformanceLogs)
                        {
                            LogHelper.LogDebug(
                                SyncModule,
                                LogId.SYNC_PULL_D_7,
                                "Performance Logs",
                                string.Format("Transaction (IsolationLevel.Snapshot) started by thread id: {0}", Thread.CurrentThread.ManagedThreadId),
                                string.Format("Method: PullRequestHandler, {0}", pullRequestArgs.GetLogFriendlyString()));
                        }

                        if (clientMetadata != null && clientMetadata.Count > 0)
                        {
                            if (pullRequest.Filter != null)
                            {
                                idList = Storage.ReadIds(pullRequest.Filter).ToList();

                                // since we don't need metadata related to deleted objects , pass "false" to GetMetadata()
                                serverMetadata = SyncDao.GetMetadata(idList, false, false);

                                // this is to identify the new objects that match the filter. we need to send them as well 
                                if (serverMetadata != null)
                                {
                                    difference = serverMetadata.Except(clientMetadata, new SyncObjectComparer());
                                }

                                sendObjectList = GetSendingObjectList(difference, serverMetadata, clientMetadata);

                                // here we need to get the deleted objects from the list of ids received by the client 
                                deletedObjectList =
                                    SyncDao.GetMetadataForDeletedObjects(
                                        clientMetadata.Select(syncObject => syncObject.Guid).ToList());
                            }
                            else
                            {
                                foreach (var syncMetaObject in clientMetadata)
                                {
                                    idList.Add(syncMetaObject.Guid);
                                }

                                // since we don't need metadata related to deleted objects , pass "false" to GetMetadata()
                                serverMetadata = SyncDao.GetMetadata(idList, false, false);

                                // Do not delete the objects that exist in server
                                var doNotDeleteGuids = serverMetadata.Select(item => item.Guid);

                                // Do not delete the objects that have changes in client
                                doNotDeleteGuids = doNotDeleteGuids.Concat(clientMetadata.Where(
                                    metadata =>
                                        metadata.GetStatus == Operation.Created ||
                                        metadata.GetStatus == Operation.Updated)
                                    .Select(metadata => metadata.Guid));

                                deletedObjectList = idList.Except(doNotDeleteGuids).ToList();

                                // There are no differences here, since we match the ids, so we are not worrying about the newly added objects. 
                                // Because of that "null" is passed to GetSendingObjectList() 
                                sendObjectList = GetSendingObjectList(null, serverMetadata, clientMetadata);
                            }

                            // Scenario : server sent PullInvokeMessage to a client with the guid of a new entity. Client makes the PullRequest with that guid.
                            // The server should response the client with the whole entity instead of delta entity.This implementation id done for that , 
                            // but this is not the perfect solution. TODO : find a better solution for this.
                            if (clientMetadata.Count() != 1 || clientMetadata[0].Version != 0)
                            {
                                if (SyncModule.EnablePropertySynchronization)
                                {
                                    isDeltaOnly = true;
                                }
                            }

                            // Fixed - PC-1295
                            // Client sends, metadata about deleted objects before a normal pull request. This request contains only the metadata which has
                            // status 4. Then we need to send complete object with all properties with in the pull response. 
                            if (clientMetadata[0].Status == 4)
                            {
                                isDeltaOnly = false;
                            }
                        }
                        else if (pullRequest.Filter != null)
                        {
                            idList = Storage.ReadIds(pullRequest.Filter).ToList();

                            // since we don't need metadata related to deleted objects , pass "false" to GetMetadata()
                            if (idList.Any())
                            {
                                serverMetadata = SyncDao.GetMetadata(idList, false, false);
                                sendObjectList = Storage.Read(pullRequest.Filter).ToList();
                            }
                        }

                        metadataCollection = CreateMetadataCollection(serverMetadata, sendObjectList, clientMetadata, isDeltaOnly, pullRequestArgs);

                        transaction.Complete();

                        if (SyncModule.EnablePerformanceLogs)
                        {
                            LogHelper.LogDebug(
                                SyncModule,
                                LogId.SYNC_PULL_D_7,
                                "Performance Logs",
                                string.Format("Transaction (IsolationLevel.Snapshot) completed by thread id: {0}", Thread.CurrentThread.ManagedThreadId),
                                string.Format("Method: ProcessPullRequest, {0}", pullRequestArgs.GetLogFriendlyString()));
                        }
                    }

                    var pullResponse = new PullResponse
                        {
                            SyncType = pullRequest.SyncType,
                            SessionGuid = pullRequest.SessionGuid,
                            ObjectType = pullRequest.ObjectType,
                            TotalEntityCount = deletedObjectList.Count() + metadataCollection.Count
                        };

                    var currentSize = SendDeletedEntities(deletedObjectList, requestInfo.Client, pullResponse);

                    SendPullResponse(requestInfo.Client, requestInfo.Client.MetaData.Platform.Type,
                                     metadataCollection, pullResponse, currentSize);

                    if (SyncModule.EnablePerformanceLogs)
                    {
                        LogHelper.LogDebug(SyncModule, LogId.SYNC_PULL_D_6, "Performance Logs", "Pull responses sending completed",
                                           string.Format("Method: PullRequestHandler, {0}", pullRequestArgs.GetLogFriendlyString()));
                    }
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("{0} when processing pull request, {1}. Exception: {2}",
                    ex.GetType().Name,
                    pullRequestArgs.GetLogFriendlyString(),
                    ex);

                LogHelper.LogError(SyncModule, LogId.SYNC_PULL_E_1, "Exception in pull request handler", message, ex.ToString());

                SendErrorMessage(pullRequest.ObjectType, pullRequest.SessionGuid, requestInfo.Client, SyncType.ClientPull, ex);
            }
        }

        private void SendPullResponse(Client client, string platformType, List<SyncEntityData> metadataCollection,
                                      PullResponse pullResponse,
                                      int currentSize)
        {
            var noOfEntitiesInPullResponse = 0;

            if (metadataCollection.Count < 1)
            {
                Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
            }
            else
            {
                for (int i = 0; i < metadataCollection.Count; i++)
                {
                    #region Handling large objects

                    int syncObjectSize;

                    if (SynchronizationModule.AndroidPlatform == platformType)
                    {
                        syncObjectSize = JsonSerializer.Serialize(metadataCollection[i], null).Length;
                    }
                    else
                    {
                        syncObjectSize = XSerializer.SerializeSyncEntity(metadataCollection[i]).Length;
                    }

                    if (syncObjectSize > MaxDataPacketSize)
                    {
                        // Send the objects, before handling the large objects
                        if (pullResponse.SynchronizedObjects.Any() || pullResponse.DeletedObjects.Any())
                        {
                            pullResponse.CurrentEntityCount += noOfEntitiesInPullResponse;
                            Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
                            noOfEntitiesInPullResponse = 0;
                            pullResponse.DeletedObjects.Clear();
                            pullResponse.SynchronizedObjects.Clear();
                        }

                        CreatePartialObjectsForPullResponse(client, pullResponse, metadataCollection[i]);
                        continue;
                    }

                    #endregion

                    pullResponse.IsSplit = false;
                    pullResponse.PacketNumber = 0;
                    pullResponse.TotalNumberOfPackets = 0;

                    if ((MaxDataPacketSize - currentSize) >= syncObjectSize)
                    {
                        pullResponse.SynchronizedObjects.Add(metadataCollection[i]);
                        currentSize += syncObjectSize;
                        noOfEntitiesInPullResponse++;
                    }
                    else
                    {
                        i--;
                        pullResponse.CurrentEntityCount += noOfEntitiesInPullResponse;
                        Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
                        pullResponse.SynchronizedObjects.Clear();
                        pullResponse.DeletedObjects.Clear();
                        currentSize = 0;
                        noOfEntitiesInPullResponse = 0;
                    }
                }

                if (pullResponse.SynchronizedObjects.Any())
                {
                    pullResponse.CurrentEntityCount += noOfEntitiesInPullResponse;
                    Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
                }
            }
        }

        private void CreatePartialObjectsForPullResponse(Client client, PullResponse pullResponse, SyncEntityData syncEntityData)
        {
            pullResponse.IsSplit = true;

            var syncObj = new SyncEntityData { Guid = syncEntityData.Guid, Version = syncEntityData.Version };

            foreach (var property in syncEntityData.ChangedPropertyNames)
            {
                syncObj.ChangedPropertyNames.Add(property);
            }

            var objectSplits = SplitSerializedEntity(syncEntityData.Object, MaxDataPacketSize);

            pullResponse.TotalNumberOfPackets = objectSplits.Count();

            for (var p = 0; p < objectSplits.Length; p++)
            {
                pullResponse.SynchronizedObjects.Clear();
                syncObj.Object = objectSplits[p];
                pullResponse.SynchronizedObjects.Add(syncObj);
                pullResponse.PacketNumber = p;

                if (p == objectSplits.Length - 1)
                {
                    pullResponse.CurrentEntityCount = 1; // Only the last split has the CurrentEntityCount set to 1
                }
                else
                {
                    pullResponse.CurrentEntityCount = -1; // We set this to '-1' because we need to identify the split response at client .
                }

                Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
            }

            pullResponse.SynchronizedObjects.Clear();
        }

        private int SendDeletedEntities(IList<Guid> deletedObjectList, Client client, PullResponse pullResponse)
        {
            var currentSize = 0;
            var noOfEntitiesInPullResponse = 0;

            for (var n = 0; n < deletedObjectList.Count; n++)
            {
                var serializedString = JsonSerializer.Serialize(deletedObjectList[n], null);
                var stringSize = serializedString.Length;

                if ((MaxDataPacketSize - currentSize) >= stringSize)
                {
                    pullResponse.DeletedObjects.Add(
                        new SyncMetadata
                        {
                            Guid = deletedObjectList[n]
                        });
                    currentSize += stringSize;
                    noOfEntitiesInPullResponse++;
                    pullResponse.CurrentEntityCount = noOfEntitiesInPullResponse;
                }
                else
                {
                    n--;
                    Dispatcher.SendMessageWhenReady(pullResponse, SyncModule, client);
                    pullResponse.DeletedObjects.Clear();
                    currentSize = 0;
                    noOfEntitiesInPullResponse = 0;
                }
            }

            return currentSize;
        }

        private ICollection<ISynchronizable> GetSendingObjectList(IEnumerable<SyncMetadata> difference,
                                                                  IEnumerable<EntityMetadata> serverMetadata,
                                                                  IEnumerable<SyncMetadata> clientMetadata)
        {
            var requestingObjectIdList = new List<Guid>();

            if (serverMetadata != null)
            {
                var matchedList = from m in serverMetadata
                                  join b in clientMetadata on m.Guid equals b.Guid
                                  where m.Version > b.Version
                                  select m;

                requestingObjectIdList.AddRange(matchedList.Select(item => item.Guid));
            }

            if (difference != null)
            {
                requestingObjectIdList.AddRange(difference.Select(diffObj => diffObj.Guid));
            }

            ICollection<ISynchronizable> readEntities = Storage.Read(requestingObjectIdList);

            return readEntities;
        }

        private List<SyncEntityData> CreateMetadataCollection(List<EntityMetadata> serverMetadata,
                                                              ICollection<ISynchronizable> sendObjectList,
                                                              IEnumerable<SyncMetadata> clientMetadata,
                                                              bool isDeltaOnly,
                                                              PullRequestEventArgs pullRequestArgs)
        {
            var metadataCollection = new List<SyncEntityData>();

            if ((serverMetadata != null && serverMetadata.Any()) && (sendObjectList != null && sendObjectList.Any()))
            {
                var combinedList = from m in serverMetadata
                                   join b in sendObjectList on m.Guid equals b.Guid
                                   select new
                                       {
                                           m.Guid,
                                           m.Version,
                                           Type = m.EntityType,
                                           Synchronizable = b
                                       };

                List<Type> androidSyncPropertyTypes = null;
                List<PropertyInfo> propertiesToSync = PropertyHelper.GetProperties(typeof(T));
                var allProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();
                string platformType = pullRequestArgs.RequestInfo.Client.MetaData.Platform.Type;

                if (SynchronizationModule.AndroidPlatform == platformType)
                {
                    androidSyncPropertyTypes = propertiesToSync.Select(prop => prop.PropertyType).ToList();
                }

                if (isDeltaOnly)
                {
                    // Make a dictionary of the clientMetadata to avoid O(n^2) complexity with many itterations.
                    var clientMetadataDictionary = clientMetadata.ToDictionary(meta => meta.Guid);

                    foreach (var combinedData in combinedList)
                    {
                        var syncEntityData = new SyncEntityData
                            {
                                Version = combinedData.Version,
                                Guid = combinedData.Guid
                            };

                        SyncMetadata entity;
                        int clientVersion = 0;
                        if (clientMetadataDictionary.TryGetValue(combinedData.Guid, out entity))
                        {
                            // Any entities means this entity is existing in the client.
                            clientVersion = entity.Version;
                        }

                        var changedProperties = new List<string>();

                        // If the client version of the entity is 0, this entity is not existing in the client, so we have to send whole object to client.
                        if (clientVersion == 0)
                        {
                            changedProperties.AddRange(propertiesToSync.Select(item => item.Name).ToList());
                        }
                        else
                        {
                            var propertyMetadata = new List<PropertyMetadata>();
                            propertyMetadata.AddRange(SyncDao.GetPropertyMetadata(combinedData.Guid, false));
                            changedProperties = FilterByVersion(propertyMetadata, clientVersion).Select(item => item.Name).ToList();
                        }

                        if (SynchronizationModule.AndroidPlatform == platformType)
                        {
                            var helper = new SerializationHelper(combinedData.Synchronizable, changedProperties);
                            syncEntityData.Object = JsonSerializer.Serialize(helper, androidSyncPropertyTypes);
                        }
                        else
                        {
                            syncEntityData.Object = XSerializer.SerializeSyncEntity(combinedData.Synchronizable,
                                                                                    PropertyHelper.GetExcludedProperties(allProperties, changedProperties));
                        }

                        if (clientVersion != 0)
                        {
                            foreach (var property in changedProperties)
                            {
                                syncEntityData.ChangedPropertyNames.Add(property);
                            }
                        }

                        metadataCollection.Add(syncEntityData);
                    }
                }
                else
                {
                    foreach (var mdata in combinedList)
                    {
                        // Here we have to send whole object, even if the propery sync is enable or not. 
                        var syncEntityData = new SyncEntityData { Version = mdata.Version, Guid = mdata.Guid };
                        var changedProperties = new List<string>();
                        changedProperties.AddRange(propertiesToSync.Select(item => item.Name).ToList());

                        if (SynchronizationModule.AndroidPlatform == platformType)
                        {
                            var helper = new SerializationHelper(mdata.Synchronizable, changedProperties);
                            syncEntityData.Object = JsonSerializer.Serialize(helper, androidSyncPropertyTypes);
                        }
                        else
                        {
                            syncEntityData.Object = XSerializer.SerializeSyncEntity(mdata.Synchronizable, PropertyHelper.GetExcludedProperties(allProperties, changedProperties));
                        }

                        metadataCollection.Add(syncEntityData);
                    }
                }
            }

            return metadataCollection;
        }

        private IEnumerable<PropertyMetadata> FilterByVersion(IEnumerable<PropertyMetadata> propertyList, int version)
        {
            return propertyList.Where(x => x.Version > version);
        }

        #endregion
    }
}
