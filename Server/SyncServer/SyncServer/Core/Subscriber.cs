﻿using System;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Messaging;

namespace PreCom.Synchronization.Core
{
    internal class Subscriber
    {
        private readonly MessageBusBase _messageBus;
        private readonly ApplicationBase _application;
        private readonly IDispatcher _dispatcher;
        private readonly SynchronizationServerBase _module;
        private readonly CommunicationBase _communication;
        private MessageBusReceiverBase _pullInvokeReceiver;
        private MessageBusReceiverBase _automaticPushReceiver;
        private string _subscriptionName;

        public event EventHandler<AutomaticPushArgs> AutomaticPushReceived;

        public Subscriber(MessageBusBase messageBus,
            ApplicationBase application,
            CommunicationBase communication,
            IDispatcher dispatcher,
            SynchronizationServerBase module)
        {
            _messageBus = messageBus;
            _application = application;
            _communication = communication;
            _dispatcher = dispatcher;
            _module = module;
        }

        public void Initialize()
        {
            _subscriptionName = _application.InstanceId.Replace("\\", "-").Replace("//", "-");

            CreateSubcription(Publisher.TopicPullInvokeMessage);
            CreateSubcription(Publisher.TopicAutomaticPushArgsBroadcast);

            _pullInvokeReceiver = _messageBus.CreateReceiver(Publisher.TopicPullInvokeMessage, _subscriptionName);
            _pullInvokeReceiver.RegisterReceiver<PullInvokeBroadcastMessage>(Receive);
            _pullInvokeReceiver.Start();

            _automaticPushReceiver = _messageBus.CreateReceiver(Publisher.TopicAutomaticPushArgsBroadcast, _subscriptionName);
            _automaticPushReceiver.RegisterReceiver<AutomaticPushArgsBroadcastMessage>(Receive);
            _automaticPushReceiver.Start();
        }

        public void Dispose()
        {
            if (_pullInvokeReceiver != null)
            {
                _pullInvokeReceiver.UnregisterReceiver<PullInvokeBroadcastMessage>();
                _pullInvokeReceiver.Dispose();
            }

            if (_automaticPushReceiver != null)
            {
                _automaticPushReceiver.UnregisterReceiver<AutomaticPushArgsBroadcastMessage>();
                _automaticPushReceiver.Dispose();
            }

            if (_subscriptionName != null)
            {
                _messageBus.DeleteSubscription(Publisher.TopicPullInvokeMessage, _subscriptionName);
                _messageBus.DeleteSubscription(Publisher.TopicAutomaticPushArgsBroadcast, _subscriptionName);
            }
        }

        private void Receive(Message<PullInvokeBroadcastMessage> message)
        {
            foreach (int credential in message.Body.Credentials)
            {
                if (_communication.IsLoggedin(credential))
                {
                    _dispatcher.SendMessage(message.Body.PullInvokeMessage,
                        credential,
                        _module,
                        Persistence.ConnectionSession);
                }
            }
        }

        private void Receive(Message<AutomaticPushArgsBroadcastMessage> message)
        {
            var handler = AutomaticPushReceived;
            if (handler != null)
            {
                handler.Invoke(this, message.Body.AutomaticPushArgs);
            }
        }

        private void CreateSubcription(string topic)
        {
            if (_messageBus.SubscriptionExists(topic, _subscriptionName))
            {
                _messageBus.DeleteSubscription(topic, _subscriptionName);
            }

            _messageBus.CreateSubscription(topic, _subscriptionName, true);
        }
    }
}
