using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PreCom.Synchronization.Core
{
    internal class UserResetEvent
    {
        private readonly ManualResetEventSlim _manualResetEvent;
        private readonly List<string> _clientIds;

        public UserResetEvent(bool initialState)
        {
            _manualResetEvent = new ManualResetEventSlim(initialState);
            _clientIds = new List<string>();
        }

        public void Set(string clientId)
        {
            if (!_clientIds.Contains(clientId, StringComparer.InvariantCulture))
            {
                _clientIds.Add(clientId);
            }

            _manualResetEvent.Set();
        }

        public void Reset(string clientId)
        {
            _clientIds.RemoveAll(s => s.Equals(clientId, StringComparison.InvariantCulture));

            if (!_clientIds.Any())
            {
                _manualResetEvent.Reset();
            }
        }

        public bool Wait(TimeSpan timeout)
        {
            return _manualResetEvent.Wait(timeout);
        }
    }
}