﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    [DataContract]
    internal class PullInvokeBroadcastMessage
    {
        [DataMember]
        public PullInvokeMessage PullInvokeMessage { get; set; }

        [DataMember]
        public ICollection<int> Credentials { get; set; }
    }
}
