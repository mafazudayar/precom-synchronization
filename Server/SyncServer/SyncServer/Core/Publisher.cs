﻿using System.Collections.Generic;
using System.Transactions;
using PreCom.Core.Messaging;
using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Core
{
    internal class Publisher
    {
        internal const string TopicPullInvokeMessage = "PreCom-Synchronization-PullInvokeMessage";
        internal const string TopicAutomaticPushArgsBroadcast = "PreCom-Synchronization-AutomaticPushArgs";

        private readonly MessageBusBase _messageBus;

        public Publisher(MessageBusBase messageBus)
        {
            _messageBus = messageBus;
        }

        public void Initialize()
        {
            if (!_messageBus.TopicExists(TopicPullInvokeMessage))
            {
                _messageBus.CreateTopic(TopicPullInvokeMessage);
            }

            if (!_messageBus.TopicExists(TopicAutomaticPushArgsBroadcast))
            {
                _messageBus.CreateTopic(TopicAutomaticPushArgsBroadcast);
            }
        }

        public void Broadcast(PullInvokeMessage pullInvoke, ICollection<int> credentials)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var pullInvokeBroadcast = new PullInvokeBroadcastMessage();
                pullInvokeBroadcast.PullInvokeMessage = pullInvoke;
                pullInvokeBroadcast.Credentials = credentials;

                var task = _messageBus.SendAsync(TopicPullInvokeMessage,
                    new Message<PullInvokeBroadcastMessage>(pullInvokeBroadcast));
                task.Wait();

                transactionScope.Complete();
            }
        }

        public void Broadcast(AutomaticPushArgs automaticPushArgs)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var automaticPushArgsBroadcast = new AutomaticPushArgsBroadcastMessage();
                automaticPushArgsBroadcast.AutomaticPushArgs = automaticPushArgs;

                var task = _messageBus.SendAsync(TopicAutomaticPushArgsBroadcast,
                    new Message<AutomaticPushArgsBroadcastMessage>(automaticPushArgsBroadcast));
                task.Wait();

                transactionScope.Complete();
            }
        }
    }
}
