﻿namespace PreCom.Synchronization.Core
{
	internal class AutomaticPushDelegateInformation
	{
		internal Operation Change { get; set; }
		internal AutomaticPushDelegate Delegate { get; set; }
        internal AutomaticPushDelegate2 Delegate2 { get; set; }
	}
}
