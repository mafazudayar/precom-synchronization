﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    internal class EntityUpdateManager<T> : SynchronizationManager<T>, IEntityUpdateManager where T : ISynchronizable
    {
        #region Fields and Properties

        private readonly string _instanceId;
        private readonly Publisher _publisher;
        private readonly CommunicationBase _communication;

        internal AutomaticPushDelegateInformation PushDelegateInformation { get; set; }

        #endregion

        #region Contructor

        public EntityUpdateManager(SynchronizedStorageBase<T> storage, IDispatcher dispatcher, SynchronizationModule syncModule, IClientPushManager clientPushManager,
            int maxDataPacketSize, string instanceId, Publisher publisher, Subscriber subscriber, ILog log, CommunicationBase communication)
            : base(storage, syncModule, maxDataPacketSize, dispatcher, log)
        {
            storage.InternalEntityChanged += UpdateSyncMetadata;
            clientPushManager.EntitiesChanged += ClientPushManagerEntitiesChanged;
            _instanceId = instanceId;
            _publisher = publisher;
            if (subscriber != null)
            {
                subscriber.AutomaticPushReceived += SubscriberOnAutomaticPushReceived;
            }

            _communication = communication;
        }

        #endregion

        #region Private Methods

        private static IEnumerable<string> GetUpdatedProperties(ISynchronizable oldEntity, ISynchronizable newEntity)
        {
            if (oldEntity == null || newEntity == null)
            {
                throw new ArgumentException("Old Entity or New Entity cannot be null");
            }

            List<PropertyInfo> allSynchronizableProperties = PropertyHelper.GetProperties(oldEntity.GetType()).ToList();

            List<string> selectedProperties = new List<string>();

            foreach (var property in allSynchronizableProperties)
            {
                object newValue = property.GetValue(newEntity, null);
                object oldValue = property.GetValue(oldEntity, null);

                if (!Equals(newValue, oldValue))
                {
                    selectedProperties.Add(property.Name);
                }
            }

            return selectedProperties;
        }

        private void SubscriberOnAutomaticPushReceived(object sender, AutomaticPushArgs automaticPushArgs)
        {
            var returnValue = InvokePushDelegate(automaticPushArgs);
            if (returnValue != null)
            {
                SendPullInvokeMessages(returnValue.CredentialIds, automaticPushArgs.Id);
            }
        }

        private void ClientPushManagerEntitiesChanged(object sender, EntitiesChangedEventArgs eventArgs)
        {
            foreach (var changeEventArgs in eventArgs.ChangeArgs)
            {
                try
                {
                    var returnValue = InvokePushDelegate(changeEventArgs);

                    if (returnValue != null)
                    {
                        Broadcast(changeEventArgs, returnValue.ChangeInfo);
                        SendPullInvokeMessages(returnValue.CredentialIds, changeEventArgs.Id, eventArgs.Client);
                    }
                }
                catch (Exception exception)
                {
                    LogHelper.LogError(SyncModule,
                        exception,
                        LogId.SYNC_EU_E_5,
                        "Exception while handling automatic push");
                }
            }
        }

        private void UpdateSyncMetadata(object sender, ChangeEventArgs eventArgs)
        {
            if (typeof(T).Name == eventArgs.EntityType)
            {
                if (eventArgs.OriginatorType != typeof(SynchronizationModule).Name)
                {
                    switch (eventArgs.Change)
                    {
                        case Operation.Created:
                            {
                                IEnumerable<PropertyInfo> properties = PropertyHelper.GetProperties(typeof(T));

                                // Since we are not sure, who is responsible the change, we have to use a guid as client id. 
                                SyncDao.InsertSyncMetadata(eventArgs, Guid.NewGuid().ToString());

                                if (SyncModule.EnablePropertySynchronization)
                                {
                                    try
                                    {
                                        SyncDao.InsertPropertyMetadata(properties, eventArgs.Id);
                                    }
                                    catch (SqlException ex)
                                    {
                                        LogHelper.LogError(
                                                 SyncModule,
                                                  LogId.SYNC_EU_E_1,
                                                 "Failed to insert property meta data",
                                                 "Exception when trying to insert property meta data, Entity id " + eventArgs.Id,
                                                 ex.ToString());
                                        throw;
                                    }
                                }

                                break;
                            }

                        case Operation.Updated:
                            {
                                if (SyncModule.EnablePropertySynchronization)
                                {
                                    // Since we are not sure, who is responsible the change, we have to use a guid as client id. 
                                    SyncDao.UpdateMetadataWithPropertyMetadata(eventArgs,
                                        GetUpdatedProperties(eventArgs.Old, eventArgs.New), Guid.NewGuid().ToString());
                                }
                                else
                                {
                                    // Since we are not sure, who is responsible the change, we have to use a guid as client id. 
                                    SyncDao.UpdateSyncMetadata(eventArgs, Guid.NewGuid().ToString());
                                }

                                break;
                            }

                        case Operation.Deleted:
                            {
                                // Since we are not sure, who is responsible the change, we have to use a guid as client id. 
                                SyncDao.DeleteSyncMetadata(eventArgs, Guid.NewGuid().ToString());
                                SyncDao.DeletePropertyMetadata(eventArgs.Id);

                                break;
                            }
                    }

                    try
                    {
                        var returnValue = InvokePushDelegate(eventArgs);
                        if (returnValue != null)
                        {
                            Broadcast(eventArgs, returnValue.ChangeInfo);
                            SendPullInvokeMessages(returnValue.CredentialIds, eventArgs.Id);
                        }
                    }
                    catch (Exception exception)
                    {
                        LogHelper.LogError(SyncModule,
                            exception,
                            LogId.SYNC_EU_E_2,
                            "Exception while handling automatic push");
                    }
                }
            }
        }

        private AutomaticPushReturnValue InvokePushDelegate(AutomaticPushArgs automaticPushArgs)
        {
            AutomaticPushReturnValue returnValue = null;

            if (PushDelegateInformation != null && (PushDelegateInformation.Delegate != null || PushDelegateInformation.Delegate2 != null))
            {
                if ((PushDelegateInformation.Change & automaticPushArgs.Change) != Operation.None)
                {
                    if (PushDelegateInformation.Delegate != null)
                    {
                        returnValue = new AutomaticPushReturnValue();
                        returnValue.CredentialIds = PushDelegateInformation.Delegate.Invoke(automaticPushArgs);
                    }
                    else if (PushDelegateInformation.Delegate2 != null)
                    {
                        returnValue = PushDelegateInformation.Delegate2.Invoke(automaticPushArgs);
                    }

                    if (returnValue == null)
                    {
                        if (SyncModule.EnablePerformanceLogs)
                        {
                            string body =
                                string.Format(
                                    "AutomaticPushDelegate method '{0}' of the entity type {1} returns null",
                                    GetAutoPushDelegateName(),
                                    typeof(T).Name);

                            LogHelper.LogDebug(SyncModule,
                                LogId.SYNC_EU_D_1,
                                "Performance Logs",
                                body,
                                "credentialId: " + automaticPushArgs.CredentialId);
                        }
                    }
                    else if (returnValue.CredentialIds != null && returnValue.CredentialIds.Any())
                    {
                        if (SyncModule.EnablePerformanceLogs)
                        {
                            string body =
                                string.Format(
                                    "AutomaticPushDelegate method '{0}' of the entity type {1} returns {2} credentials",
                                    GetAutoPushDelegateName(),
                                    typeof(T).Name,
                                    returnValue.CredentialIds.Count);

                            LogHelper.LogDebug(SyncModule,
                                LogId.SYNC_EU_D_1,
                                "Performance Logs",
                                body,
                                "credentialId: " + automaticPushArgs.CredentialId);
                        }
                    }
                    else if (returnValue.CredentialIds != null && returnValue.CredentialIds.Count == 0)
                    {
                        if (SyncModule.EnablePerformanceLogs)
                        {
                            string body =
                                string.Format(
                                    "AutomaticPushDelegate method '{0}' of the entity type {1} returns empty",
                                    GetAutoPushDelegateName(),
                                    typeof(T).Name);

                            LogHelper.LogDebug(SyncModule,
                                LogId.SYNC_EU_D_1,
                                "Performance Logs",
                                body,
                                "credentialId: " + automaticPushArgs.CredentialId);
                        }
                    }
                    else
                    {
                        if (SyncModule.EnablePerformanceLogs)
                        {
                            string body =
                                string.Format(
                                    "AutomaticPushDelegate method '{0}' of the entity type {1} returns null CredentialIds",
                                    GetAutoPushDelegateName(),
                                    typeof(T).Name);

                            LogHelper.LogDebug(SyncModule,
                                LogId.SYNC_EU_D_1,
                                "Performance Logs",
                                body,
                                "credentialId: " + automaticPushArgs.CredentialId);
                        }
                    }
                }
            }

            return returnValue;
        }

        private AutomaticPushReturnValue InvokePushDelegate(ChangeEventArgs eventArgs)
        {
            return InvokePushDelegate(CreateArgs(eventArgs));
        }

        private void SendPullInvokeMessages(IEnumerable<int> credentialIds, Guid entityId, Client exclude = null)
        {
            foreach (var credentialId in credentialIds)
            {
                PullInvokeMessage pullInvokeMsg = new PullInvokeMessage();

                // Here we do not serialize the id since it is only one and also we cannot find the client platform.
                pullInvokeMsg.Knowledge = entityId.ToString();
                pullInvokeMsg.ObjectType = typeof(T).Name;
                pullInvokeMsg.InvokeReason = (int)PullInvokeReason.AutomaticPush;

                if (exclude != null && credentialId == exclude.CredentialId)
                {
                    // Here we should remove the client that modified the entity.
                    // Because sending a pullinvoke in the middle of the push will make the push not work.
                    var clients = _communication.GetClients(credentialId);

                    foreach (var client in clients)
                    {
                        if (client.ClientID != exclude.ClientID)
                        {
                            // Send the message on a separate thread since it may block if the client is not ready
                            ThreadPool.QueueUserWorkItem(state =>
                            {
                                Client cl = null;
                                try
                                {
                                    cl = (Client)state;
                                    Dispatcher.SendMessageWhenReady(pullInvokeMsg,
                                        SyncModule,
                                        cl);
                                }
                                catch (Exception exception)
                                {
                                    string body = "Exception when sending pull invoke message to " +
                                                  (cl != null ? cl.ClientID : "unknown client");
                                    LogHelper.LogError(SyncModule,
                                        exception,
                                        LogId.SYNC_EU_E_4,
                                        body);
                                }
                            },
                                client);
                        }
                    }
                }
                else
                {
                    // Send the message on a separate thread since it may block if the user is not ready
                    ThreadPool.QueueUserWorkItem(state =>
                    {
                        int credId = 0;
                        try
                        {
                            credId = (int)state;
                            Dispatcher.SendMessageWhenReady(pullInvokeMsg,
                                SyncModule,
                                credId);
                        }
                        catch (Exception exception)
                        {
                            LogHelper.LogError(SyncModule,
                                exception,
                                LogId.SYNC_EU_E_4,
                                "Exception when sending pull invoke message to " + credId);
                        }
                    },
                        credentialId);
                }
            }
        }

        private AutomaticPushArgs CreateArgs(ChangeEventArgs eventArgs)
        {
            return new AutomaticPushArgs
            {
                Id = eventArgs.Id,
                New = eventArgs.Change != Operation.Deleted ? eventArgs.New : null,
                Old =
                    (eventArgs.Change != Operation.Created)
                        ? eventArgs.Old
                        : null,
                Change = eventArgs.Change,
                CredentialId = eventArgs.CredentialId,
                OriginatorType = eventArgs.OriginatorType,
                OriginatorInstanceId = _instanceId,
                ChangeInfo = new Dictionary<string, string>()
            };
        }

        private void Broadcast(ChangeEventArgs eventArgs, Dictionary<string, string> changeInfo)
        {
            if (_publisher != null)
            {
                var automaticPushArgs = CreateArgs(eventArgs);
                automaticPushArgs.New = null;
                automaticPushArgs.Old = null;
                automaticPushArgs.ChangeInfo = changeInfo;
                _publisher.Broadcast(automaticPushArgs);
            }
        }

        private string GetAutoPushDelegateName()
        {
            string name = string.Empty;

            if (PushDelegateInformation != null)
            {
                if (PushDelegateInformation.Delegate != null)
                {
                    name = PushDelegateInformation.Delegate.Method.Name;
                }
                else if (PushDelegateInformation.Delegate2 != null)
                {
                    name = PushDelegateInformation.Delegate2.Method.Name;
                }
            }

            return name;
        }

        #endregion
    }
}
