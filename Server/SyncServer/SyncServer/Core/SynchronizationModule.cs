﻿using System;
using System.Collections.Generic;
using System.Transactions;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Messaging;
using PreCom.Core.Modules;
using PreCom.Login;
using PreCom.Synchronization.Data;
using PreCom.Synchronization.Entity;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Core
{
    /// <summary>
    /// Synchronization module class.
    /// </summary>
    internal class SynchronizationModule : SynchronizationServerBase
    {
        #region Fields

        private const int DefaultSyncPacketSizeInKb = 1024;
        private const int DefaultMaximumSyncPacketSizeInKb = 2048;
        private const int DefaultMinimumSyncPacketSizeInKb = 10;

        private readonly ApplicationBase _application;
        private readonly SettingsBase _settings;
        private readonly ILog _log;
        private readonly StorageBase _storage;
        private readonly CommunicationBase _communication;
        private readonly ILoginStatus _login;
        private readonly MessageBusBase _messageBus;
        private Publisher _publisher;
        private Subscriber _subscriber;
        private LogHelper _logHelper;

        private bool _isInitialized;
        private int _definedSyncPacketSizeInBytes;

        internal static string AndroidPlatform
        {
            get { return "Android"; }
        }

        internal static string WindowsMobile
        {
            get { return "WindowsMobileOrPc"; }
        }

        internal int MaxRetryAttempts { get; set; }
        internal TimeSpan RetrySendDelay { get; set; }
        internal TypeManager TypeManager { get; set; }
        internal IDispatcher Dispatcher { get; set; }
        internal bool EnablePropertySynchronization { get; set; }

        internal bool LoginStatusModuleAvailable { get; set; }

        internal TimeSpan PullRequestTransactionTimeout { get; set; }
        internal TimeSpan PushRequestTransactionTimeout { get; set; }

        internal const int SerializedEmptyPushResponse = 396;
        internal const int SerializedEmptyPullResponse = 425;
        internal const int SerializedEmptyPullInvokeMessage = 456;

        #endregion

        [Inject]
        public SynchronizationModule(ApplicationBase application,
            SettingsBase settings,
            ILog log,
            StorageBase storage,
            CommunicationBase communication,
            MessageBusBase messageBus, ILoginStatus loginStatus)
        {
            _application = application;
            _settings = settings;
            _log = log;
            _storage = storage;
            _communication = communication;
            _messageBus = messageBus;
            _login = loginStatus;
            _logHelper = new LogHelper(log);
        }

        #region ModuleBase Members

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get { return "Synchronization"; }
        }

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">Out parameter. If returned as true dispose was canceled by the application. If returned as false dispose was accepted by the application and is shutting down.</param>
        /// <returns>true if dispose was ok, false otherwise</returns>
        public override bool Dispose(out bool cancel)
        {
            TypeManager = null;

            if (_subscriber != null)
            {
                _subscriber.Dispose();
            }

            cancel = false;
            _isInitialized = false;
            return true;
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>
        /// true if initialization was ok, false otherwise
        /// </returns>
        public override bool Initialize()
        {
            try
            {
                int definedSyncPacketSizeInKb = _settings.Read(this, "MaximumSyncPacketSizeInKb", DefaultSyncPacketSizeInKb, true);
                _definedSyncPacketSizeInBytes = SetSyncPacketSize(definedSyncPacketSizeInKb) * 1024;
                RetrySendDelay = TimeSpan.FromMilliseconds(_settings.Read(this, "RetrySendDelayInMilliseconds", 1000, false));
                MaxRetryAttempts = _settings.Read(this, "MaxRetryAttempts", 10, false);
                EnablePerformanceLogs = _settings.Read(this, "EnablePerformanceLogs", false, false);

                // Removed the possibility to turn off the delta synchronization
                // EnablePropertySynchronization = Application.Instance.Settings.Read(this, "EnablePropertySynchronization", true, true);
                EnablePropertySynchronization = true;

                PullRequestTransactionTimeout = TimeSpan.FromSeconds(_settings.Read(this, "PullRequestTransactionTimeoutInSeconds", 60, true));
                PushRequestTransactionTimeout = TimeSpan.FromSeconds(_settings.Read(this, "PushRequestTransactionTimeoutInSeconds", 60, true));

                TypeManager = new TypeManager(this, _log);

                LoginStatusModuleAvailable = _login != null;

                SyncDao.Storage = _storage;
                SyncDao.CreateStorage(Version);

                SyncDao.SetSnapshotIsolationOn();

                Dispatcher = new Dispatcher(_application, _communication, this, _login, _log);
                Dispatcher.Initialize();

                if (_communication.LoadBalancingEnabled)
                {
                    _publisher = new Publisher(_messageBus);
                    _publisher.Initialize();
                    _subscriber = new Subscriber(_messageBus, _application, _communication, Dispatcher, this);
                    _subscriber.Initialize();
                }

                _isInitialized = true;
            }
            catch (Exception ex)
            {
                _logHelper.LogError(this, ex, LogId.SYNC_E_2, "Error in Initialization");
            }

            return _isInitialized;
        }

        /// <summary>
        /// Check if instance is initialized or not.
        /// </summary>
        /// <returns> true if initialized, false otherwise.</returns>
        public override bool IsInitialized
        {
            get { return _isInitialized; }
        }

        #endregion

        #region Private methods

        private int SetSyncPacketSize(int definedSyncPacketSizeInKb)
        {
            if (definedSyncPacketSizeInKb > DefaultMaximumSyncPacketSizeInKb)
            {
                string logDescription = "The value of the setting \"MaximumSyncPacketSizeInKb\" is ("
                                        + definedSyncPacketSizeInKb + "), Maximum supported value "
                                        + DefaultMaximumSyncPacketSizeInKb + " will be used";
                _logHelper.LogError(this,
                    LogId.SYNC_E_1,
                    "MaximumSyncPacketSizeInKb setting is out of range",
                    logDescription);

                return DefaultMaximumSyncPacketSizeInKb;
            }

            if (definedSyncPacketSizeInKb < DefaultMinimumSyncPacketSizeInKb)
            {
                string logDescription = "The value of the setting \"MaximumSyncPacketSizeInKb\" is ("
                                        + definedSyncPacketSizeInKb + "), Minimum supported value "
                                        + DefaultMinimumSyncPacketSizeInKb + " will be used";
                _logHelper.LogError(this,
                    LogId.SYNC_E_1,
                    "MaximumSyncPacketSizeInKb setting is out of range",
                    logDescription);

                return DefaultMinimumSyncPacketSizeInKb;
            }

            return definedSyncPacketSizeInKb;
        }

        #endregion

        #region Internal methods

        #endregion

        #region Override of SynchronizationServerBase methods

        public override void RegisterStorage<T>(SynchronizedStorageBase<T> storage)
        {
            if (storage == null)
            {
                throw new ArgumentNullException("storage");
            }

            if (TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is already registered");
            }

            storage.AcquireUpdateLock = AcquireUpdateLock<T>;

            var pushManager = new ClientPushManager<T>(storage,
                Dispatcher,
                this,
                _definedSyncPacketSizeInBytes - SerializedEmptyPushResponse,
                _log);

            var pullManager = new ClientPullManager<T>(storage,
                Dispatcher,
                this,
                _definedSyncPacketSizeInBytes - SerializedEmptyPullResponse,
                _log);

            var updateManager = new EntityUpdateManager<T>(storage,
                Dispatcher,
                this,
                pushManager,
                _definedSyncPacketSizeInBytes,
                _application.InstanceId,
                _publisher,
                _subscriber,
                _log,
                _communication);

            var loginManager = new LoginManager<T>(storage,
                Dispatcher,
                this,
                _definedSyncPacketSizeInBytes - SerializedEmptyPullInvokeMessage,
                _log);

            TypeManager.AddType<T>(new TypeInformation
                {
                    PushManager = pushManager,
                    PullManager = pullManager,
                    EntityUpdateManager = updateManager,
                    LoginManager = loginManager
                });
        }

        public override void UnregisterStorage<T>(SynchronizedStorageBase<T> storage)
        {
            if (storage == null)
            {
                throw new ArgumentNullException("storage");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }
           
            TypeManager.RemoveType<T>();
        }

        public override void Push<T>(EntityFilter filter, ICollection<int> credentials)
        {
            if (credentials == null)
            {
                throw new ArgumentNullException("credentials");
            }

            if (credentials.Count < 1)
            {
                throw new ArgumentException("The credentials cannot be empty", "credentials");
            }

            if (filter == null)
            {
                throw new ArgumentNullException("filter");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            PullInvokeMessage pullInvokeMsg = new PullInvokeMessage
                                                  {
                                                      Filter = filter,
                                                      ObjectType = typeof(T).Name,
                                                      InvokeReason = (int)PullInvokeReason.ManualPush
                                                  };

            foreach (int credential in credentials)
            {
                Dispatcher.SendMessage(pullInvokeMsg, credential, this, Persistence.ConnectionSession);
            }

            if (_communication.LoadBalancingEnabled && _publisher != null)
            {
                _publisher.Broadcast(pullInvokeMsg, credentials);
            }
        }

        public override void RegisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate)
        {
            if (loginFilterDelegate == null)
            {
                throw new ArgumentNullException("loginFilterDelegate");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddLoginDelegate<T>(loginFilterDelegate);
        }

        public override void UnregisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate)
        {
            if (loginFilterDelegate == null)
            {
                throw new ArgumentNullException("loginFilterDelegate");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.RemoveLoginDelegate<T>(loginFilterDelegate);
        }

        public override void RegisterLoginFilter<T>(LoginFilterDelegate loginFilterDelegate, LoginStatus trigger)
        {
            if (loginFilterDelegate == null)
            {
                throw new ArgumentNullException("loginFilterDelegate");
            }

            if (trigger == LoginStatus.LoggedOut || trigger == LoginStatus.InProgress)
            {
                throw new ArgumentException("Login filters cannot be registered for Login Status " + trigger);
            }

            if (!LoginStatusModuleAvailable)
            {
                throw new InvalidOperationException("No module found which implements ILoginStatus");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddLoginDelegate<T>(loginFilterDelegate, trigger);
        }

        public override void UnregisterLoginFilter<T>(LoginStatus trigger)
        {
            if (trigger == LoginStatus.LoggedOut || trigger == LoginStatus.InProgress)
            {
                throw new ArgumentException("Login filters cannot be Unregistered for Login Status " + trigger);
            }

            if (!LoginStatusModuleAvailable)
            {
                throw new InvalidOperationException("No module found which implements ILoginStatus");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.RemoveLoginDelegate<T>(trigger);
        }

        public override void RegisterConflictResolver<T>(ConflictResolutionDelegate<T> conflictResolutionDelegate)
        {
            if (conflictResolutionDelegate == null)
            {
                throw new ArgumentNullException("conflictResolutionDelegate");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddConflictResolutionDelegate(conflictResolutionDelegate);
        }

        public override void UnregisterConflictResolver<T>()
        {
            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.RemoveConflictResolutionDelegate<T>();
        }
        [Obsolete("Use RegisterAutomaticPush2(AutomaticPushDelegate2, Operation)")]
        public override void RegisterAutomaticPush<T>(AutomaticPushDelegate automaticPushDelegate, Operation changeOperation)
        {
            if (automaticPushDelegate == null)
            {
                throw new ArgumentNullException("automaticPushDelegate");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddAutomaticPushDelegate<T>(new AutomaticPushDelegateInformation
            {
                Delegate = automaticPushDelegate,
                Change = changeOperation,
            });
        }

        public override void RegisterAutomaticPush2<T>(AutomaticPushDelegate2 automaticPushDelegate, Operation changeOperation)
        {
            if (automaticPushDelegate == null)
            {
                throw new ArgumentNullException("automaticPushDelegate");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddAutomaticPushDelegate<T>(new AutomaticPushDelegateInformation
            {
                Delegate2 = automaticPushDelegate,
                Change = changeOperation,
            });
        }

        public override void UnregisterAutomaticPush2<T>()
        {
            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.RemoveAutomaticPushDelegate<T>();
        }

        public override void RegisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier)
        {
            if (syncEventNotifier == null)
            {
                throw new ArgumentNullException("syncEventNotifier");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.AddSyncEventNotifier<T>(syncEventNotifier);
        }

        public override void UnregisterSyncEventNotifier<T>(SyncEventDelegate syncEventNotifier)
        {
            if (syncEventNotifier == null)
            {
                throw new ArgumentNullException("syncEventNotifier");
            }

            if (!TypeManager.TypeExists<T>())
            {
                throw new InvalidOperationException("Type " + typeof(T).Name + " is not registered");
            }

            TypeManager.RemoveSyncEventNotifier<T>(syncEventNotifier);
        }

        public override StorageTransactionScope CreateTransaction(IsolationLevel isolationLevel)
        {
            return new StorageTransactionScope(_storage,
                                               _log,
                                               isolationLevel,
                                               TransactionScopeOption.Required,
                                               TransactionManager.DefaultTimeout);
        }

        public override StorageTransactionScope CreateTransaction(IsolationLevel isolationLevel, TransactionScopeOption option, TimeSpan timeout)
        {
            return new StorageTransactionScope(_storage,
                                               _log,
                                               isolationLevel,
                                               option,
                                               timeout);
        }

        public override int AcquireUpdateLock<T>(Guid entityId)
        {
            return SyncDao.AcquireUpdateLock(entityId);
        }

        public override int AcquireUpdateLock<T>(ICollection<Guid> entityIds)
        {
            return SyncDao.AcquireUpdateLock(entityIds);
        }

        #endregion
    }
}
