﻿using System;

namespace PreCom.Synchronization.Data
{
    internal class EntityPropertyMetadata : PropertyMetadata
    {
        internal Guid EntityId { get; set; }
    }
}

