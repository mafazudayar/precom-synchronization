﻿namespace PreCom.Synchronization.Data
{
	internal class PropertyMetadata
	{
		internal string Name { get; set; }
		internal int Version { get; set; }
	}
}
