﻿using System;

namespace PreCom.Synchronization.Data
{
    /// <summary>
    /// Exception thrown when an error occurred while initializing the sync module storage.
    /// </summary>
    internal class SyncStorageInitializeException : Exception
    {
        public SyncStorageInitializeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
