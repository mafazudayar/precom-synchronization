﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using PreCom.Core;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Data
{
    using System.Data.SqlClient;

    internal static class SyncDao
    {
        #region Private constants

        // Metadata members
        private const string MetadataTableName = "PMC_Synchronization_Metadata";
        private const string SelectMetadataForDeletedIdsProc = "pr_sync_Select_Metadata_For_Deleted_Entities";

        // This Sp is an exceptional one due to the issue of handling transaction between updating the metadata and metadata properties
        private const string UpdateMetadataAndPropertyMetadataProc = "pr_sync_Update_Metadata_And_Property_Metadata";
        private const string UpdateMetadataProc = "pr_sync_Update_Metadata";
        private const string InsertMetadataProc = "pr_sync_Insert_Metadata";
        private const string DeleteMetadataProc = "pr_sync_Delete_Metadata";
        private const string AcquireMetaUpdateLocksProc = "pr_sync_Acquire_Metadata_UpdateLocks";

        // Partial Entity members
        private const string PartialObjectsTableName = "PMC_Synchronization_Partial_Entity";
        private const string InsertPartialEntityProc = "pr_sync_Insert_Partial_Entity";
        private const string SelectPartialEntityProc = "pr_sync_Select_Partial_Entity";
        private const string SelectPartialEntityCountProc = "pr_sync_Select_Partial_EntityCount";
        private const string DeletePartialEntityProc = "pr_sync_Delete_Partial_Entity";

        // Synchronizable property members
        private const string PropertyMetadataTable = "PMC_Synchronization_Property_Metadata";
        private const string InsertPropertyMetadataProc = "pr_sync_Insert_Property_Metadata";
        private const string DeletePropertyMetadataProc = "pr_sync_Delete_Property_Metadata";
        private const string SelectPropertyMetadataProc = "pr_sync_Select_Property_Metadata";
        private const string SelectPropertyMetadataByNameProc = "pr_sync_Select_Property_Metadata_by_Name";
        private const string MaintenanceMetadataProc = "pr_sync_maintenance_metadata";

        // Partial Request members
        private const string PartialPullRequestsTableName = "PMC_Synchronization_Partial_Pull_Request";
        private const string InsertPartialPullRequestProc = "pr_sync_Insert_Partial_Pull_Request";
        private const string SelectPartialPullRequestProc = "pr_sync_Select_Partial_Pull_Request";
        private const string SelectPartialPullRequestCountProc = "pr_sync_Select_Partial_Pull_RequestCount";
        private const string DeletePartialPullRequestProc = "pr_sync_Delete_Partial_Pull_Request";

        private const string SplitFunction = "fn_sync_Split";

        private const string GetAppLockProc = "sp_getapplock";
        private const string AppLockPrefix = "PreComSynchronization";
        private const string SynchronizableIdsTableType = "SynchronizableIdsTableType";
        private const string SyncPropertyMetaDataTableType = "SyncPropertyMetaDataTableType";

        #endregion

        #region Private fields

        private static StorageBase _storage;

        #endregion

        #region Internal properties

        internal static StorageBase Storage
        {
            get { return _storage; }
            set { _storage = value; }
        }

        #endregion

        #region Internal static methods

        internal static List<EntityMetadata> GetMetadata(ICollection<Guid> idList, bool isIncludeDeletedObjects, bool withUpdLock)
        {
            var metadataList = new List<EntityMetadata>();
            var idCollection = new StringBuilder();

            if (idList == null || !idList.Any())
            {
                return metadataList;
            }

            if (idList.Count == 1)
            {
                // Improve performance if only one entity is requested.
                var meta = GetMetadata(idList.First(), withUpdLock);
                if (meta != null)
                {
                    metadataList.Add(meta);
                }

                return isIncludeDeletedObjects ? metadataList : metadataList.Where(metaObject => metaObject.IsDeleted == false).ToList();
            }

            foreach (var id in idList)
            {
                idCollection.Append("'")
                            .Append(id)
                            .Append("',");
            }

            var ids = idCollection.ToString().Remove(idCollection.ToString().Length - 1, 1);
            string sqlCommand;
            if (withUpdLock)
            {
                sqlCommand = string.Format("SELECT EntityId, EntityVersion, EntityType, DeletionDateUtc FROM {0} WITH (UPDLOCK, FORCESEEK) WHERE EntityId IN ({1})", MetadataTableName, ids);
            }
            else
            {
                sqlCommand = string.Format("SELECT EntityId, EntityVersion, EntityType, DeletionDateUtc FROM {0} WHERE EntityId IN ({1})", MetadataTableName, ids);
            }

            using (var command = _storage.CreateCommand(sqlCommand))
            {
                command.Parameters.Add(_storage.CreateParameter("EntityIds", ids));

                using (var reader = _storage.ExecuteQueryCommand(command))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadataList.Add(new EntityMetadata
                            {
                                Guid = Guid.Parse(reader["EntityId"].ToString()),
                                Version = int.Parse(reader["EntityVersion"].ToString(), CultureInfo.CurrentCulture),
                                EntityType = reader["EntityType"].ToString(),
                                IsDeleted = !DBNull.Value.Equals(reader["DeletionDateUtc"])
                            });
                        }
                    }
                }
            }

            return isIncludeDeletedObjects ? metadataList : metadataList.Where(metaObject => metaObject.IsDeleted == false).ToList();
        }

        internal static EntityMetadata GetMetadata(Guid entityId, bool withUpdLock)
        {
            EntityMetadata metadata = null;

            string sqlCommand;
            if (withUpdLock)
            {
                sqlCommand = string.Format("SELECT EntityId, EntityVersion, EntityType, LastChangeClientId, DeletionDateUtc FROM {0} WITH (UPDLOCK) WHERE EntityId = @EntityId", MetadataTableName);
            }
            else
            {
                sqlCommand = string.Format("SELECT EntityId, EntityVersion, EntityType, LastChangeClientId, DeletionDateUtc FROM {0} WHERE EntityId = @EntityId", MetadataTableName);
            }

            using (var selectCommand = _storage.CreateCommand(sqlCommand))
            {
                selectCommand.CommandType = CommandType.Text;
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));

                using (IDataReader reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadata = new EntityMetadata
                                {
                                    Guid = Guid.Parse(reader["EntityId"].ToString()),
                                    Version = int.Parse(reader["EntityVersion"].ToString(), CultureInfo.CurrentCulture),
                                    EntityType = reader["EntityType"].ToString(),
                                    LastChangeClientId = reader["LastChangeClientId"] == DBNull.Value ? string.Empty : reader["LastChangeClientId"].ToString(),
                                    IsDeleted = !DBNull.Value.Equals(reader["DeletionDateUtc"])
                                };

                            break;
                        }
                    }
                }
            }

            return metadata;
        }

        internal static IList<PropertyMetadata> GetNewPropertyMetadata(Guid entityId, int version)
        {
            var metadata = new List<PropertyMetadata>();

            using (var selectCommand = _storage.CreateCommand(SelectPropertyMetadataProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));
                selectCommand.Parameters.Add(_storage.CreateParameter("Version", version));

                using (IDataReader reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadata.Add(new PropertyMetadata { Name = reader["PropertyName"].ToString() });
                        }
                    }
                }
            }

            return metadata;
        }

        internal static PropertyMetadata GetPropertyMetadata(Guid entityId, string propertyName)
        {
            var metadata = new PropertyMetadata();

            using (var selectCommand = _storage.CreateCommand(SelectPropertyMetadataByNameProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));
                selectCommand.Parameters.Add(_storage.CreateParameter("PropertyName", propertyName));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadata.Version = int.Parse(reader["PropertyVersion"].ToString(), CultureInfo.CurrentCulture);
                        }
                    }
                }
            }

            return metadata;
        }

        internal static List<PropertyMetadata> GetPropertyMetadata(Guid entityId, bool withUpdLock)
        {
            var metadataList = new List<PropertyMetadata>();
            string command;
            if (withUpdLock)
            {
                command =
                    string.Format(
                        "SELECT PropertyName, PropertyVersion FROM {0} WITH (UPDLOCK) WHERE EntityId = @EntityId",
                        PropertyMetadataTable);
            }
            else
            {
                command =
                    string.Format(
                        "SELECT PropertyName, PropertyVersion FROM {0} WHERE EntityId = @EntityId",
                        PropertyMetadataTable);
            }

            using (var selectCommand = _storage.CreateCommand(command))
            {
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            metadataList.Add(
                                new PropertyMetadata
                                {
                                    Version = int.Parse(reader["PropertyVersion"].ToString(), CultureInfo.CurrentCulture),
                                    Name = reader["PropertyName"].ToString()
                                });
                        }
                    }
                }
            }

            return metadataList;
        }

        internal static void SetSnapshotIsolationOn()
        {
            var database = new SqlConnectionStringBuilder(_storage.ConnectionString).InitialCatalog;
            using (var selectCommand = _storage.CreateCommand(string.Format("ALTER DATABASE {0} SET ALLOW_SNAPSHOT_ISOLATION ON", database)))
            {
                _storage.ExecuteNonCommand(selectCommand);
            }
        }

        /// <summary>
        /// This method should only be called from pull manager
        /// </summary>
        /// <param name="entityIdList"></param>
        /// <returns></returns>
        internal static List<Guid> GetMetadataForDeletedObjects(IEnumerable<Guid> entityIdList)
        {
            var metadataList = new List<Guid>();

            var idObjList = entityIdList.Select(id => new IdContainer { EntityId = id }).ToList();

            if (_storage.IsInitialized)
            {
                using (var command = _storage.CreateCommand(SelectMetadataForDeletedIdsProc))
                {
                    using (var ms = new MemoryStream())
                    {
                        new XmlSerializer(typeof(List<IdContainer>)).Serialize(ms, idObjList);

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(_storage.CreateParameter("xmlDoc", Encoding.UTF8.GetString(ms.ToArray())));
                    }

                    using (var reader = _storage.ExecuteQueryCommand(command))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                metadataList.Add(Guid.Parse(reader["EntityId"].ToString()));
                            }
                        }
                    }
                }
            }

            return metadataList;
        }

        internal static List<string> GetPartialEntities(Guid sessionId, Guid entityId)
        {
            var partialEntityList = new List<string>();

            using (var selectCommand = _storage.CreateCommand(SelectPartialEntityProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            partialEntityList.Add(reader["PartialEntity"].ToString());
                        }
                    }
                }
            }

            return partialEntityList;
        }

        internal static int GetPartialEntityCount(Guid sessionId, Guid entityId)
        {
            int entityCount = 0;

            using (var selectCommand = _storage.CreateCommand(SelectPartialEntityCountProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));
                selectCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            entityCount = int.Parse(reader["EntityCount"].ToString());
                        }
                    }
                }
            }

            return entityCount;
        }

        internal static bool InsertPartialEntity(Guid sessionId, Guid entityId, int packetNumber, string serializedObject)
        {
            try
            {
                using (var insertCommand = _storage.CreateCommand(InsertPartialEntityProc))
                {
                    insertCommand.CommandType = CommandType.StoredProcedure;
                    insertCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));
                    insertCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));
                    insertCommand.Parameters.Add(_storage.CreateParameter("PacketNumber", packetNumber));
                    insertCommand.Parameters.Add(_storage.CreateParameter("PartialEntity", serializedObject));

                    _storage.ExecuteNonCommand(insertCommand);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        internal static void DeletePartialEntity(Guid sessionId, Guid entityId)
        {
            using (var deleteCommand = _storage.CreateCommand(DeletePartialEntityProc))
            {
                deleteCommand.CommandType = CommandType.StoredProcedure;
                deleteCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));
                deleteCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));

                _storage.ExecuteNonCommand(deleteCommand);
            }
        }

        internal static void DeleteSyncMetadata(ChangeEventArgs eventArgs, string lastChangeClientId)
        {
            using (IDbCommand deleteCommand = _storage.CreateCommand(DeleteMetadataProc))
            {
                deleteCommand.CommandType = CommandType.StoredProcedure;
                deleteCommand.Parameters.Add(_storage.CreateParameter("EntityId", eventArgs.Id));
                deleteCommand.Parameters.Add(_storage.CreateParameter("LastChangeClientId", lastChangeClientId));

                _storage.ExecuteNonCommand(deleteCommand);
            }
        }

        internal static void InsertSyncMetadata(ChangeEventArgs eventArgs, string lastChangeClientId)
        {
            using (var insertCommand = _storage.CreateCommand(InsertMetadataProc))
            {
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.Add(_storage.CreateParameter("EntityId", eventArgs.Id));
                insertCommand.Parameters.Add(_storage.CreateParameter("EntityType", eventArgs.EntityType));
                insertCommand.Parameters.Add(_storage.CreateParameter("LastChangeClientId", lastChangeClientId));

                // starting version of any object is 1
                insertCommand.Parameters.Add(_storage.CreateParameter("EntityVersion", 1));

                _storage.ExecuteNonCommand(insertCommand);
            }
        }

        internal static void InsertPropertyMetadata(IEnumerable<PropertyInfo> properties, Guid entityId)
        {
            if (properties == null)
            {
                return;
            }

            if (properties.Any())
            {
                var propertiesDataTable = new DataTable { Locale = CultureInfo.InvariantCulture };
                propertiesDataTable.Columns.Add(new DataColumn("EntityId", typeof(Guid)));
                propertiesDataTable.Columns.Add(new DataColumn("PropertyName", typeof(string)));
                propertiesDataTable.Columns.Add(new DataColumn("Version", typeof(int)));

                foreach (var property in properties)
                {
                    propertiesDataTable.Rows.Add(entityId, property.Name, 1);
                }

                using (var cmd = _storage.CreateCommand(InsertPropertyMetadataProc))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    var propertyTableSqlParam = new SqlParameter("@properties", propertiesDataTable)
                    {
                        SqlDbType = SqlDbType.Structured,
                        TypeName = SyncPropertyMetaDataTableType
                    };

                    cmd.Parameters.Add(propertyTableSqlParam);
                    _storage.ExecuteNonCommand(cmd);
                }
            }
        }

        internal static void DeletePropertyMetadata(Guid entityId)
        {
            using (var insertCommand = _storage.CreateCommand(DeletePropertyMetadataProc))
            {
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.Add(_storage.CreateParameter("EntityId", entityId));
                _storage.ExecuteNonCommand(insertCommand);
            }
        }

        internal static void UpdateMetadataWithPropertyMetadata(ChangeEventArgs eventArgs, IEnumerable<string> properties, string lastChangeClientId)
        {
            // We use the same stored procedure to update sync metadata and sync property metadata.
            using (var updateCommand = _storage.CreateCommand(UpdateMetadataAndPropertyMetadataProc))
            {
                updateCommand.CommandType = CommandType.StoredProcedure;
                updateCommand.Parameters.Add(_storage.CreateParameter("EntityId", eventArgs.Id));
                updateCommand.Parameters.Add(_storage.CreateParameter("LastChangeClientId", lastChangeClientId));

                var propertiesList = new StringBuilder();

                foreach (var property in properties)
                {
                    propertiesList.Append(property + ",");
                }

                string propertyListStr = propertiesList.ToString();
                string propertyNameList;

                if (!string.IsNullOrEmpty(propertyListStr))
                {
                    propertyNameList = propertyListStr.Trim(',');
                }
                else
                {
                    propertyNameList = string.Empty;
                }

                updateCommand.Parameters.Add(_storage.CreateParameter("PropertyNames", propertyNameList));

                _storage.ExecuteNonCommand(updateCommand);
            }
        }

        internal static void UpdateSyncMetadata(ChangeEventArgs eventArgs, string lastChangeClientId)
        {
            using (var insertCommand = _storage.CreateCommand(UpdateMetadataProc))
            {
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.Add(_storage.CreateParameter("EntityId", eventArgs.Id));
                insertCommand.Parameters.Add(_storage.CreateParameter("LastChangeClientId", lastChangeClientId));

                _storage.ExecuteNonCommand(insertCommand);
            }
        }

        internal static void GetSqlApplicationLock<T>(int lockTimeoutInSec, SqlApplicationLockMode lockMode) where T : ISynchronizable
        {
            using (var selectCommand = _storage.CreateCommand(GetAppLockProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("Resource ", AppLockPrefix + "-" + typeof(T).Name));
                if (lockMode == SqlApplicationLockMode.Exclusive)
                {
                    selectCommand.Parameters.Add(_storage.CreateParameter("LockMode ", "Exclusive"));
                }
                else
                {
                    selectCommand.Parameters.Add(_storage.CreateParameter("LockMode ", "Shared"));
                }

                selectCommand.Parameters.Add(_storage.CreateParameter("LockTimeout ", Convert.ToString(1000 * lockTimeoutInSec)));
                selectCommand.Parameters.Add(_storage.CreateParameter("LockOwner ", "Transaction"));
                selectCommand.Parameters.Add(_storage.CreateReturnParameter("@ReturnValue", SqlDbType.Int));
                selectCommand.CommandTimeout = lockTimeoutInSec;

                _storage.ExecuteNonCommand(selectCommand);

                var parameter = (IDataParameter)selectCommand.Parameters["@ReturnValue"];
                var returnValue = (int)_storage.FindParameterValue(parameter);

                if (returnValue < 0)
                {
                    string message = string.Format(
                        "Failed to get SQL application lock for PreCom Synchronization, type {0}, error code {1}",
                        typeof(T).Name,
                        returnValue);
                    throw new SqlApplicationLockException(returnValue, message);
                }
            }
        }

        internal static bool InsertPartialRequest(Guid sessionId, int packetNumber, string serializedObject)
        {
            try
            {
                using (var insertCommand = _storage.CreateCommand(InsertPartialPullRequestProc))
                {
                    insertCommand.CommandType = CommandType.StoredProcedure;
                    insertCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));
                    insertCommand.Parameters.Add(_storage.CreateParameter("RequestNumber", packetNumber));
                    insertCommand.Parameters.Add(_storage.CreateParameter("PartialRequest", serializedObject));

                    _storage.ExecuteNonCommand(insertCommand);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        internal static List<string> GetPartialRequests(Guid sessionId)
        {
            var partialEntityList = new List<string>();

            using (var selectCommand = _storage.CreateCommand(SelectPartialPullRequestProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            partialEntityList.Add(reader["PartialRequest"].ToString());
                        }
                    }
                }
            }

            return partialEntityList;
        }

        internal static int GetPartialRequestCount(Guid sessionId)
        {
            var requestCount = 0;

            using (var selectCommand = _storage.CreateCommand(SelectPartialPullRequestCountProc))
            {
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));

                using (var reader = _storage.ExecuteQueryCommand(selectCommand))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            requestCount = int.Parse(reader["RequestCount"].ToString());
                        }
                    }
                }
            }

            return requestCount;
        }

        internal static void DeletePartialRequest(Guid sessionId)
        {
            using (var deleteCommand = _storage.CreateCommand(DeletePartialPullRequestProc))
            {
                deleteCommand.CommandType = CommandType.StoredProcedure;
                deleteCommand.Parameters.Add(_storage.CreateParameter("SessionId", sessionId));

                _storage.ExecuteNonCommand(deleteCommand);
            }
        }

        internal static int AcquireUpdateLock(Guid entityId)
        {
            int rows = 0;

            using (var cmd = _storage.CreateCommand(string.Format("SELECT Count(1) FROM {0} WITH(UPDLOCK) WHERE EntityId = @entityId ", MetadataTableName)))
            {
                cmd.CommandType = CommandType.Text;
                var sqlParam = _storage.CreateParameter("@entityId", entityId);
                sqlParam.DbType = DbType.Guid;
                cmd.Parameters.Add(sqlParam);

                rows = (int)_storage.ExecuteScalarCommand(cmd);
            }

            return rows;
        }

        internal static int AcquireUpdateLock(ICollection<Guid> entityIds)
        {
            if (entityIds == null)
            {
                throw new ArgumentNullException("entityIds");
            }

            if (entityIds.Count < 1)
            {
                throw new ArgumentException("Empty collection of entity ids supplied", "entityIds");
            }

            if (entityIds.Count == 1)
            {
                return AcquireUpdateLock(entityIds.First());
            }

            var entityIdsDataTable = new DataTable { Locale = CultureInfo.InvariantCulture };
            entityIdsDataTable.Columns.Add(new DataColumn("Id", typeof(Guid)));

            foreach (var entityId in entityIds)
            {
                entityIdsDataTable.Rows.Add(entityId);
            }

            using (var cmd = _storage.CreateCommand(AcquireMetaUpdateLocksProc))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                var entityIdsSqlParam = new SqlParameter("@ids", entityIdsDataTable)
                {
                    SqlDbType = SqlDbType.Structured,
                    TypeName = "[dbo].[" + SynchronizableIdsTableType + "]"
                };

                cmd.Parameters.Add(entityIdsSqlParam);

                var rows = (int)_storage.ExecuteScalarCommand(cmd);
                return rows;
            }
        }

        #region Create _storage (Tables/Stored Procedures/functions)

        internal static void CreateStorage(string version)
        {
            #region Create Types

            CreateTypes();

            #endregion

            #region Create tables

            UpdateTableSchemas();

            CreateTables();

            #endregion

            #region Create Stored Procedures

            CreateStoredProcedures(version);

            #endregion

            #region Create Functions

            CreateFunctions();

            #endregion
        }

        #endregion

        #endregion

        #region Private static methods

        private static void CreateFunctions()
        {
            var spQueryStringProcedure = new StringBuilder();

            if (!_storage.ObjectExists(SplitFunction, ObjectType.StoredProc))
            {
                spQueryStringProcedure.Clear();
                spQueryStringProcedure.AppendLine(" CREATE FUNCTION ");
                spQueryStringProcedure.AppendLine(SplitFunction);
                spQueryStringProcedure.AppendLine("(@sInputList VARCHAR(max), ");
                spQueryStringProcedure.AppendLine("@sDelimiter VARCHAR(5)) ");
                spQueryStringProcedure.AppendLine("RETURNS @List TABLE (item VARCHAR(8000)) ");
                spQueryStringProcedure.AppendLine("BEGIN ");
                spQueryStringProcedure.AppendLine("DECLARE @sItem VARCHAR(8000) ");
                spQueryStringProcedure.AppendLine("WHILE CHARINDEX(@sDelimiter, @sInputList, 0) <> 0 ");
                spQueryStringProcedure.AppendLine("BEGIN ");
                spQueryStringProcedure.AppendLine("SELECT @sItem = RTRIM(LTRIM(SUBSTRING(@sInputList,1,CHARINDEX(@sDelimiter, @sInputList, 0)-1))),");
                spQueryStringProcedure.AppendLine("@sInputList=RTRIM(LTRIM(SUBSTRING(@sInputList, CHARINDEX(@sDelimiter, @sInputList, 0) + LEN(@sDelimiter), LEN(@sInputList))))");
                spQueryStringProcedure.AppendLine("IF LEN(@sItem) > 0 ");
                spQueryStringProcedure.AppendLine("INSERT INTO @List SELECT @sItem ");
                spQueryStringProcedure.AppendLine("END ");
                spQueryStringProcedure.AppendLine("IF LEN(@sInputList) > 0 ");
                spQueryStringProcedure.AppendLine("INSERT INTO @List SELECT @sInputList ");
                spQueryStringProcedure.AppendLine("RETURN ");
                spQueryStringProcedure.AppendLine("END ");

                using (IDbCommand createFunction = _storage.CreateCommand(spQueryStringProcedure.ToString()))
                {
                    _storage.ExecuteNonCommand(createFunction);
                }
            }
        }

        private static void CreateTypes()
        {
            if (!TableTypeExists(SynchronizableIdsTableType))
            {
                using (var cmd = _storage.CreateCommand("CREATE TYPE [dbo].[" + SynchronizableIdsTableType + "] AS TABLE([Id] [uniqueidentifier] NULL)"))
                {
                    _storage.ExecuteNonCommand(cmd);
                }
            }

            if (!TableTypeExists(SyncPropertyMetaDataTableType))
            {
                using (var cmd = _storage.CreateCommand(string.Format("CREATE TYPE [dbo].[{0}] AS TABLE([EntityId] [uniqueidentifier] NOT NULL," 
                    + "[PropertyName] [nvarchar](128) NOT NULL," 
                    + "[Version] [int] NOT NULL)", SyncPropertyMetaDataTableType)))
                {
                    _storage.ExecuteNonCommand(cmd);
                }
            }
        }

        private static void CreateStoredProcedures(string version)
        {
            var spQueryStringProcedure = new StringBuilder();

            #region Metadata members

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This SP only calls from pull manager, so updlock is not needed. 
            spQueryStringProcedure.AppendLine(SelectMetadataForDeletedIdsProc);
            spQueryStringProcedure.AppendLine("( @xmlDoc xml)");
            spQueryStringProcedure.AppendLine("AS BEGIN ");
            spQueryStringProcedure.AppendLine("DECLARE @handle INT");
            spQueryStringProcedure.AppendLine("EXEC sp_xml_preparedocument @handle OUTPUT, @xmlDoc");
            spQueryStringProcedure.AppendLine("SELECT EntityId, EntityType, EntityVersion, DeletionDateUtc FROM ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine(" WHERE DeletionDateUtc IS NOT NULL AND");
            spQueryStringProcedure.AppendLine("EntityId  IN (");
            spQueryStringProcedure.AppendLine("SELECT * FROM OPENXML (@handle, '/ArrayOfIdContainer/IdContainer', 2) WITH (EntityId uniqueidentifier) )");
            spQueryStringProcedure.AppendLine("EXEC sp_xml_removedocument @handle ");
            spQueryStringProcedure.AppendLine("end");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectMetadataForDeletedIdsProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to use updlock. 
            spQueryStringProcedure.AppendLine(UpdateMetadataAndPropertyMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @PropertyNames nvarchar(max), @LastChangeClientId nvarchar(256)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("DECLARE @Version int ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("UPDATE ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine("SET @Version = EntityVersion = EntityVersion + 1, LastChangeClientId = @LastChangeClientId ");
            spQueryStringProcedure.AppendLine("WHERE EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("UPDATE ");
            spQueryStringProcedure.AppendLine(PropertyMetadataTable);
            spQueryStringProcedure.AppendLine("SET PropertyVersion = @Version ");
            spQueryStringProcedure.AppendLine("WHERE EntityId = @EntityId AND ");
            spQueryStringProcedure.AppendLine("PropertyName in (SELECT * FROM ");
            spQueryStringProcedure.AppendLine(SplitFunction);
            spQueryStringProcedure.AppendLine("(@PropertyNames,',') as property) ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), UpdateMetadataAndPropertyMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to use updlock. 
            spQueryStringProcedure.AppendLine(UpdateMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @LastChangeClientId nvarchar(256)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("UPDATE ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine("SET EntityVersion = EntityVersion + 1, LastChangeClientId = @LastChangeClientId ");
            spQueryStringProcedure.AppendLine("WHERE EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), UpdateMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to use updlock. 
            spQueryStringProcedure.AppendLine(InsertMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @EntityType nvarchar(512), @EntityVersion int, @LastChangeClientId nvarchar(256)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("DECLARE @count int ");
            spQueryStringProcedure.AppendLine("SET @count = (SELECT COUNT(*) FROM ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine("m1 WITH (UPDLOCK) WHERE m1.EntityId = @EntityId) ");
            spQueryStringProcedure.AppendLine("IF @count = 0 ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("INSERT INTO ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine("(EntityId, EntityType, EntityVersion, DeletionDateUtc, LastChangeClientId) VALUES (@EntityId, @EntityType, @EntityVersion, NULL, @LastChangeClientId) ");
            spQueryStringProcedure.AppendLine("END ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), InsertMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to use updlock. 
            spQueryStringProcedure.AppendLine(DeleteMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @LastChangeClientId nvarchar(256)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("UPDATE ");
            spQueryStringProcedure.AppendLine(MetadataTableName);
            spQueryStringProcedure.AppendLine("SET DeletionDateUtc = getutcdate(), LastChangeClientId = @LastChangeClientId ");
            spQueryStringProcedure.AppendLine("WHERE EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), DeleteMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.Append("CREATE PROCEDURE ");
            spQueryStringProcedure.Append(MaintenanceMetadataProc);
            spQueryStringProcedure.Append("\r\n");
            spQueryStringProcedure.Append("\t@recordsOlderThan datetime = NULL,\r\n ");
            spQueryStringProcedure.Append("\t@rowsToBeRemovedConcurrently int = 10000\r\n");
            spQueryStringProcedure.Append("AS\r\n");
            spQueryStringProcedure.Append("DECLARE @rc int\r\n");
            spQueryStringProcedure.Append("SET @rc = @rowsToBeRemovedConcurrently\r\n");
            spQueryStringProcedure.Append("DECLARE @affectedRows int\r\n");
            spQueryStringProcedure.Append("SET @affectedRows = 0\r\n");
            spQueryStringProcedure.Append("DECLARE @totalRecords int\r\n");
            spQueryStringProcedure.Append("DECLARE @remainingRecords int\r\n");
            spQueryStringProcedure.Append("IF @rowsToBeRemovedConcurrently > 0\r\n");
            spQueryStringProcedure.Append("BEGIN\r\n");
            spQueryStringProcedure.Append("\tSET NOCOUNT ON\r\n");
            spQueryStringProcedure.Append("\tIF @recordsOlderThan IS NULL\r\n");
            spQueryStringProcedure.Append("\tBEGIN\r\n");
            spQueryStringProcedure.Append("\t\tBEGIN TRY\r\n");
            spQueryStringProcedure.Append("\t\t\tWHILE @rc = @rowsToBeRemovedConcurrently\r\n");
            spQueryStringProcedure.Append("\t\t\tBEGIN\r\n");
            spQueryStringProcedure.Append("\t\t\t\tBEGIN TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tDELETE TOP(@rowsToBeRemovedConcurrently) FROM ");
            spQueryStringProcedure.Append(MetadataTableName);
            spQueryStringProcedure.Append(" WHERE [DeletionDateUtc] <= getutcdate()\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tSET @rc = @@ROWCOUNT\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tSET @affectedRows = @affectedRows+ @rc\r\n");
            spQueryStringProcedure.Append("\t\t\t\tCOMMIT TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\tEND\r\n");
            spQueryStringProcedure.Append("\t\tEND TRY\r\n");
            spQueryStringProcedure.Append("\t\tBEGIN CATCH\r\n");
            spQueryStringProcedure.Append("\t\t\tROLLBACK TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\texec pr_pmc_util_rethrow_error\r\n");
            spQueryStringProcedure.Append("\t\tEND CATCH\r\n");
            spQueryStringProcedure.Append("\t\treturn @affectedRows\r\n");
            spQueryStringProcedure.Append("\tEND\r\n");
            spQueryStringProcedure.Append("\tELSE\r\n");
            spQueryStringProcedure.Append("\tBEGIN\r\n");
            spQueryStringProcedure.Append("\t\tBEGIN TRY\r\n");
            spQueryStringProcedure.Append("\t\t\tWHILE @rc = @rowsToBeRemovedConcurrently\r\n");
            spQueryStringProcedure.Append("\t\t\tBEGIN\r\n");
            spQueryStringProcedure.Append("\t\t\t\tBEGIN TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tDELETE TOP(@rowsToBeRemovedConcurrently) FROM ");
            spQueryStringProcedure.Append(MetadataTableName);
            spQueryStringProcedure.Append(" WHERE [DeletionDateUtc] <= @recordsOlderThan\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tSET @rc = @@ROWCOUNT\r\n");
            spQueryStringProcedure.Append("\t\t\t\t\tSET @affectedRows = @affectedRows+ @rc\r\n");
            spQueryStringProcedure.Append("\t\t\t\tCOMMIT TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\tEND\r\n");
            spQueryStringProcedure.Append("\t\tEND TRY\r\n");
            spQueryStringProcedure.Append("\t\tBEGIN CATCH\r\n");
            spQueryStringProcedure.Append("\t\t\tROLLBACK TRANSACTION\r\n");
            spQueryStringProcedure.Append("\t\t\texec pr_pmc_util_rethrow_error\r\n");
            spQueryStringProcedure.Append("\t\tEND CATCH\r\n");
            spQueryStringProcedure.Append("\t\treturn @affectedRows\r\n");
            spQueryStringProcedure.Append("\tEND\r\n");
            spQueryStringProcedure.Append("\tSET NOCOUNT OFF\r\n");
            spQueryStringProcedure.Append("END\r\n");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), MaintenanceMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE");
            spQueryStringProcedure.AppendLine(AcquireMetaUpdateLocksProc);
            spQueryStringProcedure.AppendLine("(@ids dbo." + SynchronizableIdsTableType + " READONLY)");
            spQueryStringProcedure.AppendLine("AS");
            spQueryStringProcedure.AppendLine("BEGIN");
            spQueryStringProcedure.AppendLine("\tSELECT Count(1) FROM " + MetadataTableName + " WITH(UPDLOCK, FORCESEEK)");
            spQueryStringProcedure.AppendLine("\tJOIN @ids as EntityIds on " + MetadataTableName + ".EntityId = EntityIds.Id");
            spQueryStringProcedure.AppendLine("END");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), AcquireMetaUpdateLocksProc, version);

            #endregion

            #region Partial Entity members

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(InsertPartialEntityProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier, @EntityId uniqueidentifier, @PacketNumber int, @PartialEntity nvarchar(max)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("INSERT INTO ");
            spQueryStringProcedure.AppendLine(PartialObjectsTableName);
            spQueryStringProcedure.AppendLine("(SessionId, EntityId, PacketNumber, PartialEntity, CreationDateUtc) VALUES (@SessionId, @EntityId, @PacketNumber, @PartialEntity, getutcdate()) ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), InsertPartialEntityProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(SelectPartialEntityProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier, @EntityId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT PartialEntity FROM ");
            spQueryStringProcedure.AppendLine(PartialObjectsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId AND EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("ORDER BY PacketNumber ASC ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPartialEntityProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(SelectPartialEntityCountProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier, @EntityId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT COUNT(PartialEntity) AS EntityCount FROM ");
            spQueryStringProcedure.AppendLine(PartialObjectsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId AND EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPartialEntityCountProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(DeletePartialEntityProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier, @EntityId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("DELETE FROM ");
            spQueryStringProcedure.AppendLine(PartialObjectsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId AND EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), DeletePartialEntityProc, version);

            #endregion

            #region Metadata Property members

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to used updlock. 
            spQueryStringProcedure.AppendLine(InsertPropertyMetadataProc);
            spQueryStringProcedure.AppendLine("(@properties dbo." + SyncPropertyMetaDataTableType + " READONLY) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("INSERT INTO " + PropertyMetadataTable + " SELECT * FROM @properties ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), InsertPropertyMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to used updlock. 
            spQueryStringProcedure.AppendLine(DeletePropertyMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("DELETE ");
            spQueryStringProcedure.AppendLine(PropertyMetadataTable);
            spQueryStringProcedure.AppendLine("WHERE EntityId = @EntityId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), DeletePropertyMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to used updlock. 
            spQueryStringProcedure.AppendLine(SelectPropertyMetadataProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @Version int) ");
            spQueryStringProcedure.AppendLine("AS BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT PropertyName FROM ");
            spQueryStringProcedure.AppendLine(PropertyMetadataTable);
            spQueryStringProcedure.AppendLine("WITH (UPDLOCK) WHERE ");
            spQueryStringProcedure.AppendLine("EntityId = @EntityId AND PropertyVersion > @Version");
            spQueryStringProcedure.AppendLine("END");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPropertyMetadataProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");

            // This sp is not calling from pull manager, so it is OK to used updlock. 
            spQueryStringProcedure.AppendLine(SelectPropertyMetadataByNameProc);
            spQueryStringProcedure.AppendLine("(@EntityId uniqueidentifier, @PropertyName nvarchar(512)) ");
            spQueryStringProcedure.AppendLine("AS BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT PropertyVersion FROM ");
            spQueryStringProcedure.AppendLine(PropertyMetadataTable);
            spQueryStringProcedure.AppendLine("WITH (UPDLOCK) WHERE ");
            spQueryStringProcedure.AppendLine("EntityId = @EntityId AND PropertyName = @PropertyName");
            spQueryStringProcedure.AppendLine("END");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPropertyMetadataByNameProc, version);

            #endregion

            #region Partial Request members

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(InsertPartialPullRequestProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier, @RequestNumber int, @PartialRequest nvarchar(max)) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("IF NOT EXISTS (SELECT SessionId FROM ");
            spQueryStringProcedure.AppendLine(PartialPullRequestsTableName);
            spQueryStringProcedure.AppendLine(" WHERE SessionId = @SessionId AND RequestNumber=@RequestNumber) ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("INSERT INTO ");
            spQueryStringProcedure.AppendLine(PartialPullRequestsTableName);
            spQueryStringProcedure.AppendLine("(SessionId, RequestNumber, PartialRequest, CreationDateUtc) VALUES (@SessionId, @RequestNumber, @PartialRequest,getutcdate()) ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), InsertPartialPullRequestProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(SelectPartialPullRequestProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT PartialRequest FROM ");
            spQueryStringProcedure.AppendLine(PartialPullRequestsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId");
            spQueryStringProcedure.AppendLine("ORDER BY RequestNumber ASC ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPartialPullRequestProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(SelectPartialPullRequestCountProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("SELECT COUNT(PartialRequest) RequestCount FROM ");
            spQueryStringProcedure.AppendLine(PartialPullRequestsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), SelectPartialPullRequestCountProc, version);

            spQueryStringProcedure.Clear();
            spQueryStringProcedure.AppendLine("CREATE PROCEDURE ");
            spQueryStringProcedure.AppendLine(DeletePartialPullRequestProc);
            spQueryStringProcedure.AppendLine("(@SessionId uniqueidentifier) ");
            spQueryStringProcedure.AppendLine("AS ");
            spQueryStringProcedure.AppendLine("BEGIN ");
            spQueryStringProcedure.AppendLine("DELETE FROM ");
            spQueryStringProcedure.AppendLine(PartialPullRequestsTableName);
            spQueryStringProcedure.AppendLine("WHERE [SessionId]= @SessionId ");
            spQueryStringProcedure.AppendLine("END ");
            _storage.CreateOrUpdateStoredProcedure(spQueryStringProcedure.ToString(), DeletePartialPullRequestProc, version);

            #endregion
        }

        private static void UpdateTableSchemas()
        {
            var cmdText = string.Empty;

            try
            {
                #region PMC_Synchronization_Partial_Entity

                // Updated the PartialObjectsTableName
                if (_storage.ObjectExists(PartialObjectsTableName, ObjectType.Table))
                {
                    // Change column name to be more consisten with naming of other columns in Sync module and PreCom Directory Services. module.
                    if (_storage.ObjectExists(new StorageQueryParameter { ParentName = PartialObjectsTableName, ObjectName = "InsertedTime", Type = ObjectType.Column }))
                    {
                        // TODO: Log with information what we are trying to do.
                        RenameColumn(PartialObjectsTableName, "InsertedTime", "CreationDateUtc");
                    }

                    // Add primarykey to PartialObjectsTableName if missing.
                    if (!IndexExists(PartialObjectsTableName, "PK_" + PartialObjectsTableName))
                    {
                        var cmdTextSb = new StringBuilder();
                        cmdTextSb.Append("ALTER TABLE [dbo].[" + PartialObjectsTableName + "] ADD CONSTRAINT [PK_" + PartialObjectsTableName + "] ");
                        cmdTextSb.Append("PRIMARY KEY CLUSTERED (");
                        cmdTextSb.Append("[SessionId] ASC,");
                        cmdTextSb.Append("[EntityId] ASC,");
                        cmdTextSb.Append("[PacketNumber] ASC)");
                        cmdText = cmdTextSb.ToString();

                        ExecuteNonQuery(cmdText, 30);
                    }
                }

                #endregion

                #region PMC_Synchronization_Property_Metadata

                // Updated the PropertyMetadataTable
                if (_storage.ObjectExists(PropertyMetadataTable, ObjectType.Table))
                {
                    // Change nvachar length from 512 to 128.
                    if (GetColumnMaximumLength(PropertyMetadataTable, "PropertyName") > 128)
                    {
                        cmdText = "ALTER TABLE " + PropertyMetadataTable + " ALTER COLUMN PropertyName NVARCHAR(128) NOT NULL";
                        ExecuteNonQuery(cmdText, 30);
                    }

                    // Remove a previous hotfix index if existing
                    var oldHotfixClusteredIndex = "IX_PMC_Synchronization_Property_Metadata";
                    if (IndexExists(PropertyMetadataTable, oldHotfixClusteredIndex) &&
                        IsClusteredIndex(PropertyMetadataTable, oldHotfixClusteredIndex))
                    {
                        cmdText = "DROP INDEX " + oldHotfixClusteredIndex + " ON " + PropertyMetadataTable;

                        ExecuteNonQuery(cmdText, 30);
                    }

                    // Add primarykey to PropertyMetadataTable if missing.
                    if (!IndexExists(PropertyMetadataTable, "PK_" + PropertyMetadataTable))
                    {
                        var cmdTextSb = new StringBuilder();
                        cmdTextSb.Append("ALTER TABLE [dbo].[" + PropertyMetadataTable + "] ADD  CONSTRAINT [PK_" + PropertyMetadataTable + "] ");
                        cmdTextSb.Append("PRIMARY KEY CLUSTERED (");
                        cmdTextSb.Append("[EntityId] ASC,");
                        cmdTextSb.Append("[PropertyName] ASC)");
                        cmdText = cmdTextSb.ToString();

                        ExecuteNonQuery(cmdText, 30);
                    }
                }

                #endregion

                if (_storage.ObjectExists(MetadataTableName, ObjectType.Table))
                {
                    if (!_storage.ObjectExists(
                            new StorageQueryParameter
                                {
                                    ParentName = MetadataTableName,
                                    ObjectName = "LastChangeClientId",
                                    Type = ObjectType.Column
                                }))
                    {
                        var cmdTextSb = new StringBuilder();
                        cmdTextSb.Append("ALTER TABLE ");
                        cmdTextSb.Append(MetadataTableName);
                        cmdTextSb.Append(" ADD ");
                        cmdTextSb.Append("[LastChangeClientId] ");
                        cmdTextSb.Append("nvarchar(256) NULL ");
                        cmdText = cmdTextSb.ToString();

                        ExecuteNonQuery(cmdText, 30);
                    }
                }
            }
            catch (SyncStorageInitializeException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new SyncStorageInitializeException(
                    string.Format("Failed to update table schema. Run the following query manually to resolve the error: {0}", cmdText),
                    ex);
            }
        }

        private static void CreateTables()
        {
            if (!_storage.ObjectExists(MetadataTableName, ObjectType.Table))
            {
                using (var command = _storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE " + MetadataTableName + " ( ");
                    queryString.Append("[EntityId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[EntityType] nvarchar(512) NOT NULL, ");
                    queryString.Append("[EntityVersion] [int] NOT NULL, ");
                    queryString.Append("[DeletionDateUtc] [datetime] NULL, ");
                    queryString.Append("[LastChangeClientId] nvarchar(256) NOT NULL ");
                    queryString.Append("CONSTRAINT [PK_" + MetadataTableName + "] PRIMARY KEY CLUSTERED ");
                    queryString.Append("( [EntityId] ))");
                    command.CommandText = queryString.ToString();
                    _storage.ExecuteNonCommand(command);
                }
            }

            if (!_storage.ObjectExists(PartialObjectsTableName, ObjectType.Table))
            {
                using (var command = _storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE " + PartialObjectsTableName + " ( ");
                    queryString.Append("[SessionId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[EntityId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[PacketNumber] [int] NOT NULL, ");
                    queryString.Append("[PartialEntity] nvarchar(max) NOT NULL, ");
                    queryString.Append("[CreationDateUtc] DateTime NOT NULL ");
                    queryString.Append("CONSTRAINT [PK_" + PartialObjectsTableName + "] PRIMARY KEY CLUSTERED (");
                    queryString.Append("[SessionId] ASC,");
                    queryString.Append("[EntityId] ASC,");
                    queryString.Append("[PacketNumber] ASC))");
                    command.CommandText = queryString.ToString();
                    _storage.ExecuteNonCommand(command);
                }
            }

            if (!_storage.ObjectExists(PropertyMetadataTable, ObjectType.Table))
            {
                using (var command = _storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE " + PropertyMetadataTable + " ( ");
                    queryString.Append("[EntityId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[PropertyName] nvarchar(128) NOT NULL, ");
                    queryString.Append("[PropertyVersion] int NOT NULL ");
                    queryString.Append("CONSTRAINT [PK_" + PropertyMetadataTable + "] PRIMARY KEY CLUSTERED (");
                    queryString.Append("[EntityId] ASC,");
                    queryString.Append("[PropertyName] ASC))");
                    command.CommandText = queryString.ToString();
                    _storage.ExecuteNonCommand(command);
                }
            }

            if (!_storage.ObjectExists(PartialPullRequestsTableName, ObjectType.Table))
            {
                using (var command = _storage.CreateCommand())
                {
                    var queryString = new StringBuilder();
                    queryString.Append("CREATE TABLE " + PartialPullRequestsTableName + " ( ");
                    queryString.Append("[SessionId] [uniqueidentifier] NOT NULL, ");
                    queryString.Append("[RequestNumber] [int] NOT NULL, ");
                    queryString.Append("[PartialRequest] [nvarchar](max) NOT NULL, ");
                    queryString.Append("[CreationDateUtc] DateTime NOT NULL ");
                    queryString.Append("CONSTRAINT [PK_" + PartialPullRequestsTableName + "] PRIMARY KEY CLUSTERED (");
                    queryString.Append("[SessionId] ASC,");
                    queryString.Append("[RequestNumber] ASC))");
                    command.CommandText = queryString.ToString();
                    _storage.ExecuteNonCommand(command);
                }
            }
        }

        private static int GetColumnMaximumLength(string table, string column)
        {
            var cmdText = "SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.columns WHERE table_name = @table and column_name = @column";
            using (var cmd = _storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(_storage.CreateParameter("@table", table));
                cmd.Parameters.Add(_storage.CreateParameter("@column", column));

                return (int)_storage.ExecuteScalarCommand(cmd);
            }
        }

        private static bool IndexExists(string table, string indexName)
        {
            var cmdText = "SELECT Name FROM sys.indexes WHERE object_id = OBJECT_ID(@table) AND Name = @indexName";

            using (var cmd = _storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(_storage.CreateParameter("@table", table));
                cmd.Parameters.Add(_storage.CreateParameter("@indexName", indexName));

                var result = _storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        private static bool IsClusteredIndex(string table, string indexName)
        {
            var cmdText = "SELECT Name FROM sys.indexes WHERE object_id = OBJECT_ID(@table) AND Name = @indexName AND type_desc = N'CLUSTERED'";
            using (var cmd = _storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(_storage.CreateParameter("@table", table));
                cmd.Parameters.Add(_storage.CreateParameter("@indexName", indexName));

                var result = _storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        private static void RenameColumn(string table, string columnOldName, string columnNewName)
        {
            var cmdText = "sp_rename '" + table + "." + columnOldName + "', '" + columnNewName + "', 'COLUMN'";
            using (var command = _storage.CreateCommand(cmdText))
            {
                _storage.ExecuteNonCommand(command);
            }
        }

        private static void ExecuteNonQuery(string commandText, int extendedTimeoutInSeconds = 0)
        {
            using (var command = _storage.CreateCommand(commandText))
            {
                command.CommandTimeout += extendedTimeoutInSeconds;
                _storage.ExecuteNonCommand(command);
            }
        }

        private static bool TableTypeExists(string tableTypeName)
        {
            var cmdText = "SELECT system_type_id FROM sys.types WHERE name = @tableTypeName AND is_table_type = 1";

            using (var cmd = _storage.CreateCommand(cmdText))
            {
                cmd.Parameters.Add(_storage.CreateParameter("@tableTypeName", tableTypeName));

                var result = _storage.ExecuteScalarCommand(cmd);

                // Index exists if we get a value back that is not null.
                return result != DBNull.Value && result != null;
            }
        }

        #endregion
    }
}
