﻿using PreCom.Synchronization.Entity;

namespace PreCom.Synchronization.Data
{
	internal class EntityMetadata : SyncMetadata
	{
		internal string EntityType { get; set; }
		internal bool IsDeleted { get; set; }
	}
}
