# PreCom Synchronization

## About
This repository contains the PreCom Synchronization module and related source code.
Everyone at PocketMobile has read access to the code, but only Product Development has write access.

## Important notes for usage of the source code
* **If doing a fork of the PreCom source code or a standard module you must take appropriate precautions so that the code does not end up outside PocketMobile.**

* **Be equally careful when handling compiled binaries that have not been obfuscated since they can easily be reverse engineered to source code.**

* **Do not use a custom built version of PreCom or standard modules in customer projects.**

* **Do not ever use a custom built version of PreCom or standard modules in production.**

## Getting started using the synchronization module
**TODO:** Please help filling in a short code sample here


## Getting started developing in the synchronization module

### Configure NuGet

This project uses a [PocketMobile private NuGet feed][1] to obtain dependencies.
Access to this private feed can be managed by [adam.brengesjo@pocketmobile.se][4] or [magnus.bondesson@pocketmobile.se][5] See [PocketMobile Knowledgebase][2] for more information.
For a quick guide on how to configure NuGet, see the [MyGet FAQ][3].

**Tip:** If you get an error with `exit code 1` from NuGet when building in Visual Studio it might be due to your credentials for MyGet have not been cached.
To cache your credentials run the following PowerShell command.

    .\.nuget\NuGet.exe sources add -Name PocketMobile -Source http://www.myget.org/F/pocketmobile -Username yourusername -Password yourpassword

### Get references and submodules
1. Download Android references by calling.

        gs.sh library

2. Download packages for Windows Mobile and packages required for the build script.

        get-references.sh

3. Initialize submodules.

        git submodule init
        git submodule update

[1]: http://www.myget.org/F/pocketmobile
[2]: https://pocketmobile.jira.com/wiki/display/KNOWBASE/Private+NuGet+feed
[3]: http://www.myget.org/site/Faq#title2
[4]: mailto:adam.brengesjo@pocketmobile.se
[5]: mailto:magnus.bondesson@pocketmobile.se