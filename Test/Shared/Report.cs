﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization.Test
{
    public class Report
    {
        public String Title { get; set; }
        public String Content { get; set; }
        public List<Editor> Editors { get; set; }
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            Report r = (Report)obj;

            if (Content == null)
            {
                if (r.Content != null)
                    return false;
            }
            else if (!Content.Equals(r.Content))
                return false;
            if (Title == null)
            {
                if (r.Title != null)
                    return false;
            }
            else if (!Title.Equals(r.Title))
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            result = prime*result + ((Content == null) ? 0 : Content.GetHashCode());
            result = prime*result + ((Title == null) ? 0 : Title.GetHashCode());
            return result;
        }
    }
}
