﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization.Test
{
    /// <summary>
    /// OrderManager implements the SynchronizedStorageBase
    /// </summary>
    public class MarketOrderManager : SynchronizedStorageBase<MarketOrder>
    {
        private readonly IOrderDao<MarketOrder> _dao;

        public MarketOrderManager(IOrderDao<MarketOrder> dao)
        {
            _dao = dao;
        }

        public static MarketOrder CreateOrder(string creater, string description)
        {
            MarketOrder order = new MarketOrder();
            Guid id = Guid.NewGuid();
            order.Id = id;
            order.Creator = creater;
            order.Description = description;
            order.Date = DateTime.Now;
            order.IsAssigned = false;

            return order;
        }

        protected override void OnCreate(ICollection<ISynchronizable> entities, CreateArgs args)
        {
            foreach (ISynchronizable entity in entities)
            {
                MarketOrder order = (MarketOrder)entity;

                if (_dao.GetOrder(order.Guid) == null)
                {
                    _dao.Insert(order);
                }
            }
        }

        protected override void OnUpdate(ISynchronizable entity, UpdateArgs args)
        {
            MarketOrder order = (MarketOrder)entity;
            _dao.Update(order);
        }

        protected override void OnDelete(Guid id, DeleteArgs args)
        {
            _dao.Delete(id);
        }

        protected override ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds)
        {
            return _dao.GetOrders(entityIds);
        }

        protected override ICollection<ISynchronizable> OnRead(EntityFilter filter)
        {
            return _dao.GetOrders(filter);
        }

        protected override ICollection<Guid> OnReadIds(EntityFilter filter)
        {
            return _dao.GetOrderIds(filter);
        }

        public void Delete()
        {
            lock (this)
            {
                _dao.DeleteAll();
            }
        }
    }
}
