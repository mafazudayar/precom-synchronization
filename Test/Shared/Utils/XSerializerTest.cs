﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using NUnit.Framework;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Test.Utils
{
    [TestFixture]
    public class XSerializerTest
    {
        public List<Guid> ListOfGuids { get; set; }

        [SetUp]
        public void TestInitialize()
        {
            // Create some base data we can use to serialize in the tests.
            ListOfGuids = new List<Guid>();
            for (int i = 0; i < 10; i++)
            {
                ListOfGuids.Add(Guid.NewGuid());
            }
        }

        [Test]
        [Explicit("Only needed when experimenting with performance")]
        public void Serialize_PerformanceTest()
        {
            var ids = new List<Guid>();
            for (int i = 0; i < 50; i++)
            {
                ids.Add(Guid.NewGuid());
            }

            string serializedObject = null;
            var type = typeof(List<Guid>);

            // Warm up
            for (int i = 0; i < 100; i++)
            {
                serializedObject = XSerializer.Serialize(ids, type);
                var deserialized = XSerializer.Deserialize(serializedObject, type);
            }

            var w = Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                serializedObject = XSerializer.Serialize(ids, type);
                var deserialized = XSerializer.Deserialize(serializedObject, type);
            }

            w.Stop();

            Debug.WriteLine("Elapsed: " + w.ElapsedMilliseconds);
            Debug.WriteLine("XML Length: " + serializedObject.Length);
        }

        [Test]
        public void Serialize_Entity_ReturnsNonIndentedString()
        {
            var serializedData = XSerializer.Serialize(ListOfGuids, ListOfGuids.GetType());

            // Removed the header since it is known to contain spaces.
            // The other part of the string should not contain any spaces.
            var serializedDataWithoutHeader =
                serializedData.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "")
                    .Replace("<ArrayOfGuid", "")
                    .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                    .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                    .Replace(">", "")
                    .TrimStart();

            Assert.IsTrue(serializedDataWithoutHeader.Length > 0);
            Assert.IsFalse(serializedDataWithoutHeader.Contains(" "));
            Assert.IsFalse(serializedDataWithoutHeader.Contains(Environment.NewLine));
        }

        [Test]
        public void Serialize_WithLegacyDeserializer_ReturnsOriginalData()
        {
            var serializedData = XSerializer.Serialize(ListOfGuids, ListOfGuids.GetType());
            var deserializedData = (List<Guid>)LegacyXSerializer.Deserialize(serializedData, ListOfGuids.GetType());

            Assert.IsTrue(serializedData.Length > 0);
            CollectionAssert.AreEqual(ListOfGuids, deserializedData);
        }

        [Test]
        public void Deserialize_SerializedData_ReturnsOriginalData()
        {
            var serializedData = XSerializer.Serialize(ListOfGuids, ListOfGuids.GetType());
            var deserializedData = (List<Guid>)XSerializer.Deserialize(serializedData, ListOfGuids.GetType());

            Assert.IsTrue(serializedData.Length > 0);
            CollectionAssert.AreEqual(ListOfGuids, deserializedData);
        }

        [Test]
        public void Deserialize_WithLegacySerializer_ReturnsOriginalData()
        {
            var serializedData = LegacyXSerializer.Serialize(ListOfGuids, ListOfGuids.GetType());
            var deserializedData = (List<Guid>)XSerializer.Deserialize(serializedData, ListOfGuids.GetType());

            Assert.IsTrue(serializedData.Length > 0);
            CollectionAssert.AreEqual(ListOfGuids, deserializedData);
        }

        //

        [Test]
        public void SerializeSyncEntity_WithExcludedProperty_ExcludesProperty()
        {
            var order = MarketOrder.Create("test", "testDesc");
            var descriptionProperty = order.GetType().GetProperty("Description");

            var serializedData = XSerializer.SerializeSyncEntity(order, new List<PropertyInfo>() { descriptionProperty });
            var deserializedData = (MarketOrder)LegacyXSerializer.DeserializeSyncEntity(serializedData, typeof(MarketOrder));

            Assert.IsNull(deserializedData.Description);
        }

        [Test]
        public void DeserializeSyncEntity_SerializedData_ReturnsOriginalData()
        {
            var order = MarketOrder.Create("test", "testDesc");
            var descriptionProperty = order.GetType().GetProperty("Description");

            var serializedData = XSerializer.SerializeSyncEntity(order, new List<PropertyInfo>() { descriptionProperty });
            var deserializedData = (MarketOrder)LegacyXSerializer.DeserializeSyncEntity(serializedData, typeof(MarketOrder));

            Assert.IsTrue(serializedData.Length > 0);
            Assert.IsTrue(order.Id == deserializedData.Id &&
                          deserializedData.Description == null &&
                          order.Creator == deserializedData.Creator &&
                          order.Date == deserializedData.Date &&
                          order.IsAssigned == deserializedData.IsAssigned);
        }

        [Test]
        public void DeserializeSyncEntity_WithLegacySerializer_ReturnsOriginalData()
        {
            var order = MarketOrder.Create("test", "testDesc");
            var descriptionProperty = order.GetType().GetProperty("Description");

            var serializedData = LegacyXSerializer.SerializeSyncEntity(order, new List<PropertyInfo>() { descriptionProperty });
            var deserializedData = (MarketOrder)XSerializer.DeserializeSyncEntity(serializedData, typeof(MarketOrder));

            Assert.IsTrue(serializedData.Length > 0);
            Assert.IsTrue(order.Id == deserializedData.Id &&
              deserializedData.Description == null &&
              order.Creator == deserializedData.Creator &&
              order.Date == deserializedData.Date &&
              order.IsAssigned == deserializedData.IsAssigned);
        }
    }
}
