﻿namespace PreCom.Synchronization.Test
{
    public class ComparableArray
    {
        public byte[] Content { get; set; }

        protected bool Equals(ComparableArray other)
        {
            if (Equals(Content, other.Content))
            {
                return true;
            }
            if (Content.Length != other.Content.Length)
            {
                return false;
            }
            for (int i = 0; i < Content.Length; i++)
            {
                if (Content[i] != other.Content[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ComparableArray) obj);
        }

        public override int GetHashCode()
        {
            return (Content != null ? Content.GetHashCode() : 0);
        }
    }
}
