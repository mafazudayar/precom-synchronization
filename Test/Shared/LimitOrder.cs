﻿using System;

namespace PreCom.Synchronization.Test
{
    public class LimitOrder : Order, ISynchronizable
    {
        [Synchronizable]
        public Guid Id { get; set; }

        public bool IsAssigned { get; set; }

        [Synchronizable]
        public ComparableArray Image { get; set; }

        [Synchronizable]
        public DateTime Date { get; set; }

        public Guid Guid
        {
            get { return Id; }
            set { Id = value; }
        }

        [Synchronizable]
        public Report Report { get; set; }
    }
}
