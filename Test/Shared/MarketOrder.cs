﻿using System;

namespace PreCom.Synchronization.Test
{
#if SERVER
    [DebuggerDisplay("Id = {Id}, Date = {Date}")]
#endif
    public class MarketOrder : Order, ISynchronizable, ICloneable
    {
        [Synchronizable]
        public bool IsAssigned { get; set; }

        [Synchronizable]
        public DateTime Date { get; set; }

        [Synchronizable]
        public Guid Id
        {
            get { return Guid; }
            set { Guid = value; }
        }

        #region ISynchronizable Members

        public Guid Guid { get; set; }

        #endregion

        public static MarketOrder Create(string creator, string description)
        {
            var order = new MarketOrder
            {
                Id = Guid.NewGuid(),
                Creator = creator,
                Description = description,
                Date = DateTime.Now,
                IsAssigned = false
            };

            return order;
        }

        public object Clone()
        {
            return new MarketOrder()
            {
                Id = Id,
                Creator = Creator,
                Date = Date,
                Description = Description,
                IsAssigned = IsAssigned,
            };
        }
    }
}
