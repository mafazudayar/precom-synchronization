﻿using System;
using System.Collections.Generic;

namespace PreCom.Synchronization.Test
{
    public interface IOrderDao<T> where T : Order
    {
        bool Insert(T order);
        bool Update(T order);
        bool Delete(Guid orderId);
        bool DeleteAll();
        T GetOrder(Guid id);
        ICollection<T> GetOrders();
        ICollection<ISynchronizable> GetOrders(ICollection<Guid> entities);
        ICollection<ISynchronizable> GetOrders(EntityFilter filter);
        ICollection<Guid> GetOrderIds(EntityFilter filter);
    }
}