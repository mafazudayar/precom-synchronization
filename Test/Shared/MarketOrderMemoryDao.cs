﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PreCom.Synchronization.Test
{
    public class MarketOrderMemoryDao : IOrderDao<MarketOrder>
    {
        private List<MarketOrder> MarketOrderList = new List<MarketOrder>();

        public bool Insert(MarketOrder order)
        {
            MarketOrderList.Add(order);

            return true;
        }

        public bool Update(MarketOrder order)
        {
            var orders = from marketOrder in MarketOrderList where marketOrder.Guid == order.Guid select marketOrder;
            if (orders.Any())
            {
                MarketOrderList.Remove(orders.First());
                MarketOrderList.Add(order);
            }

            return true;
        }

        public bool Delete(Guid orderId)
        {
            var orders = from marketOrder in MarketOrderList where marketOrder.Guid == orderId select marketOrder;
            if (orders.Any())
            {
                MarketOrderList.Remove(orders.First());
            }

            return true;
        }

        public bool DeleteAll()
        {
            MarketOrderList.Clear();

            return true;
        }

        public MarketOrder GetOrder(Guid id)
        {
            return MarketOrderList.Find(order => order.Guid == id);
        }

        public ICollection<MarketOrder> GetOrders()
        {
            return MarketOrderList;
        }

        public ICollection<ISynchronizable> GetOrders(ICollection<Guid> entities)
        {
            return MarketOrderList.FindAll(order => entities.Contains(order.Guid)).ConvertAll(input => (ISynchronizable)input);
        }

        public ICollection<ISynchronizable> GetOrders(EntityFilter filter)
        {
            return MarketOrderList.FindAll(order => DoesMatchFilter(order, filter)).ConvertAll(input => (ISynchronizable)input);
        }

        private bool DoesMatchFilter(MarketOrder order, EntityFilter entityFilter)
        {
            if (entityFilter == null || entityFilter.FilterList == null || entityFilter.FilterList.Count == 0)
            {
                return true;
            }
            foreach (Filter filter in entityFilter.FilterList)
            {
                switch (filter.Operator)
                {
                    case FilterOperator.EqualTo:
                        if (filter.PropertyName == "Creator" && !order.Creator.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Description" && !order.Description.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "IsAssigned" && !order.IsAssigned.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Date" && !order.Date.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Guid" && !order.Guid.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Id" && !order.Id.Equals(filter.Value))
                            return false;
                        break;
                    case FilterOperator.NotEqualTo:
                        if (filter.PropertyName == "Creator" && order.Creator.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Description" && order.Description.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "IsAssigned" && order.IsAssigned.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Date" && order.Date.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Guid" && order.Guid.Equals(filter.Value))
                            return false;
                        else if (filter.PropertyName == "Id" && order.Id.Equals(filter.Value))
                            return false;
                        break;
                    case FilterOperator.GreaterThan:
                    case FilterOperator.LessThan:
                        throw new InvalidOperationException("Filter not supported");
                }
            }
            return true;
        }

        public ICollection<Guid> GetOrderIds(EntityFilter filter)
        {
            return MarketOrderList.FindAll(order => DoesMatchFilter(order, filter)).ConvertAll(input => input.Guid);
        }
    }
}
