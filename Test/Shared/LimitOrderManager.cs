﻿using System;
using System.Collections.Generic;
using PreCom.Core.Modules;

namespace PreCom.Synchronization.Test
{
    public class LimitOrderManager : SynchronizedStorageBase<LimitOrder>
    {
#if PocketPC
        // Set this to true to test exception handling
        private const bool IsThrowing = false;
#else
        // Set this to true to test exception handling
        private const bool IsThrowing = false;
#endif

        private IOrderDao<LimitOrder> _dao;

        public LimitOrderManager(IOrderDao<LimitOrder> dao)
        {
            _dao = dao;
        }

#if PocketPC
        public LimitOrderManager(IOrderDao<LimitOrder> dao, ILog log, SynchronizationClientBase synchronization)
            : base(log, synchronization)
        {
            _dao = dao;
        }
#else
        public LimitOrderManager(IOrderDao<LimitOrder> dao, ILog log, SynchronizationServerBase synchronization)
            : base(log, synchronization)
        {
            _dao = dao;
        }
#endif

        public static LimitOrder CreateOrder(string creater, string description, byte[] isLarge)
        {
            LimitOrder mo = new LimitOrder();

            mo.Guid = Guid.NewGuid();
            mo.IsAssigned = true;
            mo.Date = DateTime.Now;
            mo.Description = description;
            mo.Creator = creater;
            mo.Report = new Report();
            mo.Report.Content = "Report Content";
            mo.Report.Title = "Report Title";
            mo.Image = new ComparableArray {Content = isLarge};
            mo.Report.Id = 1;
            mo.Report.Editors = new List<Editor>();
            mo.Report.Editors.Add(new Editor { EditorName = "AAA" });
            mo.Report.Editors.Add(new Editor { EditorName = "BBB" });
            mo.Report.Editors.Add(new Editor { EditorName = "CCC" });

            return mo;
        }

        protected override void OnCreate(ICollection<ISynchronizable> entities, CreateArgs args)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }


            foreach (ISynchronizable entity in entities)
            {
                LimitOrder order = (LimitOrder)entity;
                if (_dao.GetOrder(order.Guid) == null)
                {
                    _dao.Insert(order);
                }
            }
        }

        protected override void OnUpdate(ISynchronizable entity, UpdateArgs args)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }

            LimitOrder order = (LimitOrder)entity;
            _dao.Update(order);
        }

        protected override void OnDelete(Guid id, DeleteArgs args)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }

            _dao.Delete(id);
        }

        protected override ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }

            var orders = _dao.GetOrders(entityIds);
#if CLIENT
            // Uncomment to test PC-2279
            //orders.Remove(orders.FirstOrDefault());
#endif
            return orders;
        }

        protected override ICollection<ISynchronizable> OnRead(EntityFilter filter)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }

            return _dao.GetOrders(filter);
        }

        protected override ICollection<Guid> OnReadIds(EntityFilter filter)
        {
            if (IsThrowing)
            {
                throw new SynchronizedStorageException("Error occurred in storage");
            }

            return _dao.GetOrderIds(filter);
        }

    }
}
