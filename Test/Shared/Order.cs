﻿namespace PreCom.Synchronization.Test
{
    public class Order
    {
        [Synchronizable]
        public string Creator { get; set; }

        [Synchronizable]
        public string Description { get; set; }
    }
}
