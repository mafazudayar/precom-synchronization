﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using PreCom;
using PreCom.Core;
using PreCom.Core.Modules;
using PreCom.Synchronization;
using PreCom.Synchronization.Test;
using PreCom.Synchronization.Utils;
using Synchronization.LTHttpClient;

namespace SyncServerTestModule
{
    using System.Data;

    using IsolationLevel = System.Transactions.IsolationLevel;

    public class LoadTestServerModule : ModuleBase
    {
        private bool _isInitialized;
        private SynchronizedStorageBase<MarketOrder> _marketOrderManager;
        private SynchronizationServerBase _syncModule;
        private List<int> _stagedCredentialIds = new List<int>();
        private List<Guid> _objectGuids = new List<Guid>();
        private Random _rand = new Random();

        private int _nInitialPush;
        private int _tInitialPush;
        private int _nServerPush;
        private int _nServerPushClients;
        private int _tServerPush;

        private Timer _t;

        private Thread _serverPushThread;
        private bool _serverPushThreadRunning = true;

        private int _loadTestSenario;
        private MarketOrderDao _marketOrderDao;
        private ILog _log;

        //ClientPush = 1
        //ClientPull = 2
        //ServerPush = 3
        //Login Filters = 4
        //Continuous load testing = 5
        //Server crash testing = 
        //Network error testing = 

        public override bool Initialize()
        {
            ReadSettings();
            _log = Application.Instance.CoreComponents.Get<ILog>();
            _marketOrderDao = new MarketOrderDao(Application.Instance.Storage, new LogUtil(this, _log));
            _marketOrderDao.CreateOrderTable();

            _syncModule = Application.Instance.Modules.Get<SynchronizationServerBase>();
            _marketOrderManager = new MarketOrderManager(_marketOrderDao);
            _syncModule.RegisterStorage<MarketOrder>(_marketOrderManager);

            Application.Instance.Communication.Register<TestEntity>(OnReceiveInsertEntity);

            if (_loadTestSenario == 1)
            {
                ClearTables();

                _syncModule.RegisterAutomaticPush<MarketOrder>(AutomaticPushDelegateOnScenario01To03, Operation.Created);
            }

            if (_loadTestSenario == 2)
            {
                ClearTables();

                _syncModule.RegisterAutomaticPush<MarketOrder>(AutomaticPushDelegateOnScenario01To03, Operation.Created);

                CreateInitialOrders(_nInitialPush);

                DeleteSomeOrders();
            }

            if (_loadTestSenario == 3)
            {
                ClearTables();

                _syncModule.RegisterAutomaticPush<MarketOrder>(AutomaticPushDelegateOnScenario01To03, Operation.Created);

                CreateInitialOrders(_nInitialPush);

                Application.Instance.Register(ClientEventReason.UserLogin, OnClientLoginScenario03);
            }

            if (_loadTestSenario == 4)
            {
                ClearTables();

                _syncModule.RegisterAutomaticPush<MarketOrder>(AutomaticPushDelegateOnScenario01To03, Operation.Created);

                CreateInitialOrders(_nInitialPush);

                _syncModule.RegisterLoginFilter<MarketOrder>(LoginFilterScenario04);
            }

            if (_loadTestSenario == 5)
            {
                ClearTables();

                _syncModule.RegisterAutomaticPush<MarketOrder>(AutomaticPushDelegateOnScenario05, Operation.Created | Operation.Updated | Operation.Deleted);

                CreateInitialOrders(_nInitialPush);

                Application.Instance.Register(ClientEventReason.UserLogin, OnClientLoginScenario05);

                _syncModule.RegisterLoginFilter<MarketOrder>(LoginFilterScenario04);

                //_serverPushThread = new Thread(ServerPushThread);
                //_serverPushThread.Start();
            }

            _isInitialized = true;
            return _isInitialized;
        }

        private void OnReceiveInsertEntity(TestEntity entity, RequestInfo requestInfo)
        {
            WriteLog("Age " + entity.Age.ToString());
        }

        private void ClearTables()
        {
            _marketOrderDao.DeleteSyncReords();
            _marketOrderDao.DeleteSyncPropertyReords();
            _marketOrderDao.DeleteOrderReords();
            _marketOrderDao.DeleteSyncPartialRecords();
            DeleteCommQueue();
        }

        public ICollection<Guid> LoginFilterScenario04(LoginFilterArgs args)
        {
            string creator = "Creator_" + args.Client.CredentialId;
            Filter filter = new Filter() { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = creator };
            EntityFilter entityFilter = new EntityFilter();
            entityFilter.FilterList.Add(filter);

            ICollection<Guid> ids = _marketOrderManager.ReadIds(entityFilter);
            WriteLog("Server-Push-" + ids.Count + "-Entities-to-" + args.Client.CredentialId);
            return ids;
        }

        private void OnClientLoginScenario03(Client client, ClientEventReason reason, ref ClientEventReply reply)
        {
            lock (_stagedCredentialIds)
            {
                if (!_stagedCredentialIds.Contains(client.CredentialId))
                {
                    _t = new Timer(state =>
                    {
                        _syncModule.Push<MarketOrder>(new EntityFilter(), new Collection<int>() { client.CredentialId });
                        _stagedCredentialIds.Add(client.CredentialId);
                    }, _t, 2000, Timeout.Infinite);

                    WriteLog("Client-" + client.CredentialId + "logged-in-and-server-push-timer-will-start-soon");
                }
            }
        }

        private void OnClientLoginScenario05(Client client, ClientEventReason reason, ref ClientEventReply reply)
        {
            lock (_stagedCredentialIds)
            {
                if (!_stagedCredentialIds.Contains(client.CredentialId))
                {
                    _stagedCredentialIds.Add(client.CredentialId);
                }
            }
        }

        private void ReadSettings()
        {
            _nInitialPush = Application.Instance.Settings.Read(this, "nInitialPush", 1000, true);
            _tInitialPush = Application.Instance.Settings.Read(this, "tInitialPush", 300, true);
            _nServerPush = Application.Instance.Settings.Read(this, "nServerPush", 100, true);
            _nServerPushClients = Application.Instance.Settings.Read(this, "nServerPushClients", 10, true);
            _tServerPush = Application.Instance.Settings.Read(this, "tServerPush", 30, true);
            _loadTestSenario = PreCom.Application.Instance.Settings.Read(this, "LoadTestScenario", 1, true);
        }

        private ICollection<int> AutomaticPushDelegateOnScenario01To03(AutomaticPushArgs automaticPushArgs)
        {
            WriteLog("Created " + automaticPushArgs.Id.ToString() + " By " + automaticPushArgs.CredentialId);
            Collection<int> users = new Collection<int>();
            return users;
        }

        private ICollection<int> AutomaticPushDelegateOnScenario05(AutomaticPushArgs automaticPushArgs)
        {
            WriteLog(automaticPushArgs.Change + " " + automaticPushArgs.Id.ToString() + " By " + automaticPushArgs.CredentialId);

            MarketOrder order = (MarketOrder)automaticPushArgs.New;
            string creator = order.Creator;
            List<int> credentialsToSendTo = new List<int>();

            try
            {
                int owner = Convert.ToInt32(creator.Split('_')[1]);
                credentialsToSendTo.Add(owner);
                credentialsToSendTo.Add(_stagedCredentialIds[_rand.Next(0, _stagedCredentialIds.Count - 1)]);
                credentialsToSendTo.Add(_stagedCredentialIds[_rand.Next(0, _stagedCredentialIds.Count - 1)]);
            }
            catch
            {
                WriteLog("Error-in-AutomaticPushDelegateOnScenario05");
            }

            return credentialsToSendTo;
        }


        private void ServerPushThread()
        {
            Thread.Sleep(1000 * _tInitialPush);

            while (_serverPushThreadRunning)
            {
                List<Guid> remainingGuids = new List<Guid>(_objectGuids);

                List<Guid> guidsToRead = new List<Guid>();
                for (int i = 0; i < _nServerPush; i++)
                {
                    Guid guid = remainingGuids[_rand.Next(0, remainingGuids.Count - 1)];
                    remainingGuids.Remove(guid);
                    guidsToRead.Add(guid);
                }

                ICollection<ISynchronizable> objects = _marketOrderManager.Read(guidsToRead);
                if (objects != null)
                {
                    foreach (ISynchronizable synchronizable in objects)
                    {
                        MarketOrder marketOrder = (MarketOrder)synchronizable;
                        marketOrder.Description = UpdateDescription(marketOrder.Description);
                        _marketOrderManager.Update(marketOrder, new UpdateArgs(GetType().Name,-1));
                    }
                }

                Thread.Sleep(1000 * _tServerPush);
            }
        }

        private string UpdateDescription(string description)
        {
            try
            {
                string[] splitted = description.Split('_');
                if (splitted.Length == 0)
                {
                    return description + "_2";
                }
                else
                {
                    int version = Convert.ToInt32(splitted[1]);
                    return splitted[0] + "_" + version++;
                }
            }
            catch
            {
                return description;
            }
        }

        private void DeleteCommQueue()
        {
            IDbCommand command = null;
            try
            {
                if (PreCom.Application.Instance.Storage.IsInitialized)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("TRUNCATE TABLE PMC_Communication_Queue_2");
                    command = PreCom.Application.Instance.Storage.CreateCommand(sb.ToString());
                    command.Connection.Open();

                    PreCom.Application.Instance.Storage.ExecuteNonCommand(command);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to delete comm queue: " + ex);
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }


        public override bool Dispose(out bool cancel)
        {
            Console.WriteLine("Entering Dispose()");

            if (_serverPushThread != null)
            {

                _serverPushThreadRunning = false;
                if (!_serverPushThread.Join(1000 * _tServerPush + 60000))
                {
                    Console.WriteLine("Failed to join thread, aborting it");
                    _serverPushThread.Abort();
                }

            }
            Console.WriteLine("Dispose Done");


            cancel = false;
            _isInitialized = false;
            return true;
        }

        public override bool IsInitialized
        {
            get { return _isInitialized; }
        }

        public void CreateInitialOrders(int numOrders)
        {
            for (int i = 0; i < numOrders; i++)
            {
                MarketOrder order = MarketOrderManager.CreateOrder(GetCreator(), GetDescription());
                _marketOrderManager.Create(order, new CreateArgs(GetType().Name));
                _objectGuids.Add(order.Guid);
            }
        }

        public void ReadInitialOrders()
        {
            Filter filter = new Filter
                {
                    Operator = FilterOperator.EqualTo,
                    PropertyName = "Creator",
                    Value = "Prasad"
                };
            EntityFilter entityFilter = new EntityFilter();
            entityFilter.FilterList.Add(filter);
            ICollection<Guid> gcollection = _marketOrderManager.ReadIds(entityFilter);
            foreach (Guid guid in gcollection)
            {
                _objectGuids.Add(guid);
            }
        }

        public void DeleteSomeOrders()
        {
            Filter filter = new Filter
            {
                Operator = FilterOperator.EqualTo,
                PropertyName = "Creator",
                Value = "Creator_20"
            };
            EntityFilter entityFilter = new EntityFilter();
            entityFilter.FilterList.Add(filter);
            ICollection<Guid> gcollection = _marketOrderManager.ReadIds(entityFilter);
            foreach (Guid guid in gcollection)
            {
                using (var scope = _syncModule.CreateTransaction(IsolationLevel.ReadCommitted))
                {

                    _marketOrderManager.Delete(guid, new DeleteArgs(GetType().Name, -1));
                    scope.Complete();
                }
            }
        }

        private String GetCreator()
        {
            string client = "Creator_";

            Random rn = new Random();
            int i = rn.Next(2, 2105);

            return client + i;

        }

        private String GetDescription()
        {
            Random rn = new Random();
            int i = rn.Next(10);

            if (i == 1)
            {
                return "Monitor";
            }

            if (i == 2)
            {
                return "Keyboard";
            }

            if (i == 3)
            {
                return "Mouse";
            }

            if (i == 4)
            {
                return "RAM";
            }

            if (i == 5)
            {
                return "CPU";
            }

            if (i == 6)
            {
                return "Web cam";
            }

            if (i == 7)
            {
                return "Hard disk";
            }

            if (i == 8)
            {
                return "CD rom";
            }

            if (i == 9)
            {
                return "mother board";
            }

            return "Books";
        }

        private string GetFilenameYYYMMDD(string suffix, string extension)
        {
            return string.Format("{0}_{1}.{2}", DateTime.Now.ToString("yyyy-MM-dd"), suffix, extension);
        }

        public void WriteLog(String message)
        {
            //just in case: we protect code with try.
            try
            {
                string filename = AppDomain.CurrentDomain.BaseDirectory +
                                  AppDomain.CurrentDomain.RelativeSearchPath + GetFilenameYYYMMDD("server", "log");

                System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                XElement xmlEntry = new XElement("logEntry",
                                                 new XElement("Date", DateTime.Now.ToString()),
                                                 new XElement("Message", message));
                sw.WriteLine(xmlEntry);
                sw.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}
