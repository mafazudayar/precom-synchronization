﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PreCom.Core.Communication;

namespace Synchronization.LTHttpClient
{
    public class TestEntity: EntityBase
    {
        public int Age { get; set; }
    }
}
