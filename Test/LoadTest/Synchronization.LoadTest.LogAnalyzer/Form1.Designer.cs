﻿namespace Synchronization.LoadTest.LogAnalyzer
{
    partial class FormLogAnalyzer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowsClientLogs = new System.Windows.Forms.Button();
            this.btnAnalyseScenario_01 = new System.Windows.Forms.Button();
            this.btnanalizeScenario_02 = new System.Windows.Forms.Button();
            this.txtResults = new System.Windows.Forms.RichTextBox();
            this.btnanalizeScenario_03 = new System.Windows.Forms.Button();
            this.btnanalizeScenario_04 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBrowsClientLogs
            // 
            this.btnBrowsClientLogs.Location = new System.Drawing.Point(18, 21);
            this.btnBrowsClientLogs.Name = "btnBrowsClientLogs";
            this.btnBrowsClientLogs.Size = new System.Drawing.Size(169, 23);
            this.btnBrowsClientLogs.TabIndex = 0;
            this.btnBrowsClientLogs.Text = "Select Client/Server Logs";
            this.btnBrowsClientLogs.UseVisualStyleBackColor = true;
            this.btnBrowsClientLogs.Click += new System.EventHandler(this.BtnBrowsLogsClick);
            // 
            // btnAnalyseScenario_01
            // 
            this.btnAnalyseScenario_01.Location = new System.Drawing.Point(447, 21);
            this.btnAnalyseScenario_01.Name = "btnAnalyseScenario_01";
            this.btnAnalyseScenario_01.Size = new System.Drawing.Size(315, 23);
            this.btnAnalyseScenario_01.TabIndex = 3;
            this.btnAnalyseScenario_01.Text = "Analize Logs for client push scenario ( 01 )";
            this.btnAnalyseScenario_01.UseVisualStyleBackColor = true;
            this.btnAnalyseScenario_01.Click += new System.EventHandler(this.BtnAnalyzeScenario01Click);
            // 
            // btnanalizeScenario_02
            // 
            this.btnanalizeScenario_02.Location = new System.Drawing.Point(447, 59);
            this.btnanalizeScenario_02.Name = "btnanalizeScenario_02";
            this.btnanalizeScenario_02.Size = new System.Drawing.Size(315, 23);
            this.btnanalizeScenario_02.TabIndex = 4;
            this.btnanalizeScenario_02.Text = "Analize Logs for client pull scenario  ( 02 )";
            this.btnanalizeScenario_02.UseVisualStyleBackColor = true;
            this.btnanalizeScenario_02.Click += new System.EventHandler(this.BtnAnalyzeScenario02Click);
            // 
            // txtResults
            // 
            this.txtResults.Location = new System.Drawing.Point(12, 194);
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.txtResults.Size = new System.Drawing.Size(750, 367);
            this.txtResults.TabIndex = 5;
            this.txtResults.Text = "";
            // 
            // btnanalizeScenario_03
            // 
            this.btnanalizeScenario_03.Location = new System.Drawing.Point(447, 99);
            this.btnanalizeScenario_03.Name = "btnanalizeScenario_03";
            this.btnanalizeScenario_03.Size = new System.Drawing.Size(315, 23);
            this.btnanalizeScenario_03.TabIndex = 6;
            this.btnanalizeScenario_03.Text = "Analize Logs for Server push scenario  ( 03 )";
            this.btnanalizeScenario_03.UseVisualStyleBackColor = true;
            this.btnanalizeScenario_03.Click += new System.EventHandler(this.BtnAnalizeScenario03Click);
            // 
            // btnanalizeScenario_04
            // 
            this.btnanalizeScenario_04.Location = new System.Drawing.Point(447, 137);
            this.btnanalizeScenario_04.Name = "btnanalizeScenario_04";
            this.btnanalizeScenario_04.Size = new System.Drawing.Size(315, 23);
            this.btnanalizeScenario_04.TabIndex = 7;
            this.btnanalizeScenario_04.Text = "Analize Logs for login filter scenario  ( 04 )";
            this.btnanalizeScenario_04.UseVisualStyleBackColor = true;
            this.btnanalizeScenario_04.Click += new System.EventHandler(this.BtnAnalizeScenario04Click);
            // 
            // FormLogAnalyzer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 573);
            this.Controls.Add(this.btnanalizeScenario_04);
            this.Controls.Add(this.btnanalizeScenario_03);
            this.Controls.Add(this.txtResults);
            this.Controls.Add(this.btnBrowsClientLogs);
            this.Controls.Add(this.btnanalizeScenario_02);
            this.Controls.Add(this.btnAnalyseScenario_01);
            this.Name = "FormLogAnalyzer";
            this.Text = "Log Analyzer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBrowsClientLogs;
        private System.Windows.Forms.Button btnAnalyseScenario_01;
        private System.Windows.Forms.Button btnanalizeScenario_02;
        private System.Windows.Forms.RichTextBox txtResults;
        private System.Windows.Forms.Button btnanalizeScenario_03;
        private System.Windows.Forms.Button btnanalizeScenario_04;
    }
}

