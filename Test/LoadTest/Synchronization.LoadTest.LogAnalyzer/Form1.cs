﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Synchronization.LoadTest.LogAnalyzer
{
    public partial class FormLogAnalyzer : Form
    {
        private List<string> _clientLogFiles;
        private List<string> _serverLogFiles;

        List<SyncdData> _clientEntityList;
        List<SyncdData> _serverEntityList;

        private bool fullResults = false;

        public FormLogAnalyzer()
        {
            InitializeComponent();
            _clientEntityList = new List<SyncdData>();
            _serverEntityList = new List<SyncdData>();
            _clientLogFiles = new List<string>();
            _serverLogFiles = new List<string>();
        }

        private void BtnBrowsLogsClick(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                DialogResult result = dialog.ShowDialog();
                string[] logFiles = Directory.GetFiles(dialog.SelectedPath);

                foreach (var file in logFiles)
                {

                    string fileName = (file.Split('.')[0]).Split('_')[1];

                    if (fileName == "client")
                    {
                        _clientLogFiles.Add(file);
                    }
                    else if (fileName == "server")
                    {
                        _serverLogFiles.Add(file);
                    }
                }

                InvokeTextBoxResults(_clientLogFiles.Count + " Client logs selected");
                InvokeTextBoxResults(_serverLogFiles.Count + " Server logs selected");
            }
            catch (Exception ex)
            {
                InvokeTextBoxResults(ex.StackTrace);
            }
        }

        private SyncdData ParseClientLogMessage(string xmlNode)
        {
            string[] ids = xmlNode.Split(' ');

            if (ids[0]== "Pushed")
            {
                SyncdData data = new SyncdData(ids[1], ids[3]);
                return data;
            }

            if (ids[0] == "Created")
            {
                SyncdData data = new SyncdData(ids[2], "0");
                return data;
            }

            if (ids[0] == "CreatedAndAutoPush")
            {
                SyncdData data = new SyncdData(ids[2], "0");
                return data;
            }

            return null;
        }

        private SyncdData ParseServerLogMessage(string xmlNode)
        {
            string[] ids = xmlNode.Split(' ');

            if (ids[0] == "Created")
            {
                SyncdData data = new SyncdData(ids[1], ids[3]);
                return data;
            }

            return null;
        }

        private void AnalyzeScenario01()
        {
            if (_clientLogFiles != null)
            {
                foreach (string clientLog in _clientLogFiles)
                {
                    TextReader sm = new StreamReader(clientLog);
                    string s = "<logs>" + sm.ReadToEnd() + "</logs>";
                    int pushedCount = 0;
                    string client = string.Empty;
                    XDocument source = XDocument.Parse(s);
                    if (source.Root != null)
                    {
                        XName itemName = XName.Get("Message", source.Root.Name.NamespaceName);
                        foreach (XElement item in source.Descendants(itemName))
                        {
                            SyncdData syncdData = ParseClientLogMessage(item.Value);
                            if (syncdData!=null)
                            {
                                _clientEntityList.Add(syncdData);
                                pushedCount++;
                            }
                        }

                        client = (clientLog.Split('_')[0]).Split('-')[3];
                        InvokeTextBoxResults(client + " Pushed " + pushedCount + " entities");
                    }
                }
            }

            if (_serverLogFiles != null)
            {
                foreach (string serverLog in _serverLogFiles)
                {
                    TextReader sm = new StreamReader(serverLog);
                    string s = "<logs>" + sm.ReadToEnd() + "</logs>";

                    XDocument source = XDocument.Parse(s);
                    if (source.Root != null)
                    {
                        XName itemName = XName.Get("Message", source.Root.Name.NamespaceName);
                        foreach (XElement item in source.Descendants(itemName))
                        {
                            SyncdData syncdData = ParseServerLogMessage(item.Value);
                            if (syncdData != null)
                            {
                                _serverEntityList.Add(syncdData);
                            }
                        }
                    }
                }

                InvokeTextBoxResults("All Clients pushed " + _clientEntityList.Count() + " entities");
                InvokeTextBoxResults("Server received " + _serverEntityList.Count() + " entities");
            }

            if (_clientEntityList.Count() == _serverEntityList.Count())
            {
                string mess = "Server and Client entity count is matched";
                InvokeTextBoxResults(mess, Color.Green);
            }
            else
            {
                string mess = "Server and Client entity count is not matched";
                InvokeTextBoxResults(mess, Color.Red);
            }

            if (fullResults)
            {
                foreach (SyncdData clientGuid in _clientEntityList)
                {
                    foreach (SyncdData serverGuid in _serverEntityList)
                    {
                        if (clientGuid.ID == serverGuid.ID)
                        {
                            clientGuid.Matched = true;
                            serverGuid.Matched = true;
                        }
                    }
                }

                foreach (SyncdData syncdData in _clientEntityList)
                {
                    if (syncdData.Matched == false)
                    {
                        string mess = "Entity " + syncdData.ID + " is not pushed to server";
                        InvokeTextBoxResults(mess, Color.Red);
                    }
                }

                foreach (SyncdData syncdData in _serverEntityList)
                {
                    if (syncdData.Matched == false)
                    {
                        string mess = "Entity " + syncdData.ID + " is not pushed from any of the connected clients";
                        InvokeTextBoxResults(mess, Color.Red);
                    }
                }
            }
        }

        private void BtnAnalyzeScenario01Click(object sender, EventArgs e)
        {
            try
            {
                Thread t = new Thread(AnalyzeScenario01);
                t.Start();
            }
            catch (Exception)
            {
                
            }
        }

        private void InvokeTextBoxResults(string message, Color color)
        {
            txtResults.Invoke(new Action(() => AppendText(txtResults, message + Environment.NewLine,color)));
        }

        private void InvokeTextBoxResults(string message)
        {
            txtResults.Invoke(new Action(() => AppendText(txtResults, message + Environment.NewLine, Color.Black)));
        }

        private void AppendText(RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

        private void AnalyzeScenario02()
        {
            if (_serverLogFiles != null)
            {
                foreach (string serverLog in _serverLogFiles)
                {
                    TextReader sm = new StreamReader(serverLog);
                    string s = "<logs>" + sm.ReadToEnd() + "</logs>";

                    XDocument source = XDocument.Parse(s);
                    if (source.Root != null)
                    {
                        XName itemName = XName.Get("Message", source.Root.Name.NamespaceName);
                        foreach (XElement item in source.Descendants(itemName))
                        {
                            SyncdData syncdData = ParseServerLogMessage(item.Value);
                            if (syncdData != null)
                            {
                                _serverEntityList.Add(syncdData);
                            }
                        }
                    }
                }

                InvokeTextBoxResults("Server has " + _serverEntityList.Count() + " entities");
            }

            if (_clientLogFiles != null)
            {
                foreach (string clientLog in _clientLogFiles)
                {
                    _clientEntityList.Clear();
                    TextReader sm = new StreamReader(clientLog);
                    string s = "<logs>" + sm.ReadToEnd() + "</logs>";
                    int pulledCount = 0;
                    string client = string.Empty;
                    XDocument source = XDocument.Parse(s);
                    if (source.Root != null)
                    {
                        XName itemName = XName.Get("Message", source.Root.Name.NamespaceName);
                        foreach (XElement item in source.Descendants(itemName))
                        {
                            SyncdData syncdData = ParseClientLogMessage(item.Value);
                            if (syncdData != null)
                            {
                                pulledCount++;
                                _clientEntityList.Add(syncdData);
                            }
                        }

                        client = (clientLog.Split('_')[0]).Split('-')[3];
                        InvokeTextBoxResults(client + " Pulled " + pulledCount + " entities");

                        if (_clientEntityList.Count() == _serverEntityList.Count())
                        {
                            string mess = "Server and Client entity count is matched for the client " + client;
                            InvokeTextBoxResults(mess,Color.Green);
                        }
                        else
                        {
                            string mess = "Server and Client entity count is not matched for the client " + client;
                            InvokeTextBoxResults(mess,Color.Red);
                        }

                        if (fullResults)
                        {
                            List<SyncdData> copyOfserverEntityList = new List<SyncdData>(_serverEntityList.Count);
                            _serverEntityList.ForEach((item) =>
                                {
                                    copyOfserverEntityList.Add(new SyncdData(item));
                                });

                            foreach (SyncdData clientGuid in _clientEntityList)
                            {
                                foreach (SyncdData serverGuid in copyOfserverEntityList)
                                {
                                    if (clientGuid.ID == serverGuid.ID)
                                    {
                                        clientGuid.Matched = true;
                                        serverGuid.Matched = true;
                                    }
                                }
                            }

                            foreach (SyncdData syncdData in _clientEntityList)
                            {
                                if (syncdData.Matched == false)
                                {
                                    string mess = "Entity " + syncdData.ID + " of the client " + client +
                                                  " is not in the server";
                                    InvokeTextBoxResults(mess, Color.Red);
                                }
                            }

                            foreach (SyncdData syncdData in copyOfserverEntityList)
                            {
                                if (syncdData.Matched == false)
                                {
                                    string mess = "Entity " + syncdData.ID + " is not pulled from the server to client " +
                                                  client;
                                    InvokeTextBoxResults(mess, Color.Red);
                                }
                            }
                        }
                    }
                }
            }

            InvokeTextBoxResults("Done");
        }

        private void BtnAnalyzeScenario02Click(object sender, EventArgs e)
        {
            try
            {
                Thread t = new Thread(AnalyzeScenario02);
                t.Start();
            }
            catch (Exception)
            {
                
            }
        }

        private void BtnAnalizeScenario03Click(object sender, EventArgs e)
        {
            try
            {
                //same as AnalyzeScenario02
                Thread t = new Thread(AnalyzeScenario02);
                t.Start();
            }
            catch (Exception)
            {

            }
        }

        private void BtnAnalizeScenario04Click(object sender, EventArgs e)
        {
            try
            {
                //same as AnalyzeScenario02
                Thread t = new Thread(AnalyzeScenario02);
                t.Start();
            }
            catch (Exception)
            {

            }
        }
    }
}
