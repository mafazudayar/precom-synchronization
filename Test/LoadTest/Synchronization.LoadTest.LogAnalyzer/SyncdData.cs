﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchronization.LoadTest.LogAnalyzer
{
    internal class SyncdData
    {
        internal string ID { get; set; }
        internal string Client { get; set; }
        internal bool Matched { get; set; }

        public SyncdData(SyncdData previousSyncdData)
        {
            ID = previousSyncdData.ID;
            Client = previousSyncdData.Client;
            Matched = previousSyncdData.Matched;
        }

        public SyncdData(string id, string client)
        {
            ID = id;
            Client = client;
            Matched = false;
        }
    }
}
