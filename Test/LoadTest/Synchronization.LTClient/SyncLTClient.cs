﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Xml.Linq;
using PreCom.Core;
using PreComLT.LoadTest.Client;
using SyncClientTestModule;

namespace PreCom.Synchronization.Core
{
    public class SyncLTClient : ClientLTBase
    {
        private readonly int _numMessages;
        private readonly int _clientPush;
        private readonly object _timerLock = new object();
        private readonly object _updateTimerLock = new object();

        private readonly SynchronizationModule _synchronization;
        private readonly SyncCustomModule _customModule;
        private readonly List<Guid> _temporaryGuidListForUpdate;
        private readonly int _ltScenario;

        private int _initialDelay;

        private Timer _timer;

        private Timer _updateTimer;

        //private Timer _deleteTimer;
        //private object _deleteTimerLock = new object();

        private bool _disposed;

        public int Scenario
        {
            get { return _ltScenario; }
        }

        public SyncLTClient(string clientId, string protocol, string ip, int pspPort, int httpPort, List<KeyValuePair<string, string>> credentials, int numMessages, int delay, int initialDelay, int ltScenario, bool deltaSync)
            : base(clientId, protocol, ip, pspPort, httpPort, credentials)
        {
            _numMessages = numMessages;

            _ltScenario = ltScenario;
            _clientPush = delay;
            _initialDelay = initialDelay;

            Communication.LoginStatus += Communication_LoginStatus;

            _synchronization = new SynchronizationModule(Communication, IncreaseSentMessageCount, IncreaseReceivedMessageCount, deltaSync);
            _synchronization.Initialize();
            _customModule = new SyncCustomModule(_synchronization, this);
            _temporaryGuidListForUpdate = new List<Guid>();

            //Continuous load testing = 5
            if (_ltScenario == 5)
            {
                _customModule.RegisterForAutoPush();
            }
        }

        public void IncreaseSentMessageCount()
        {
            MessagesSent = MessagesSent + 1;
        }

        public void IncreaseReceivedMessageCount()
        {
            MessagesReceived = MessagesReceived + 1;
        }

        private void Communication_LoginStatus(LoginEventArgs e)
        {
            switch (e.Status)
            {
                case LoginStatusInfo.LoginRequest:
                    // Create a copy of the login credentials because they can be cleared if login fails.
                    this.Login();
                    break;

                case LoginStatusInfo.LogoutRequest:
                    break;

                case LoginStatusInfo.LoginOK:
                    WriteLog("Login-Success-Credential " + Communication.GetClient().CredentialId);

                    lock (_timerLock)
                    {
                        if (_timer != null)
                        {
                            _initialDelay = 30;
                            _timer.Dispose();
                        }

                        //ClientPush = 1
                        if (_ltScenario == 1)
                        {
                            _timer = new Timer(TimerCallbackScenario1, _timer, _initialDelay * 1000, _clientPush * 1000);
                        }

                        //ClientPull = 2
                        if (_ltScenario == 2)
                        {
                            _timer = new Timer(TimerCallbackScenario2, _timer, _initialDelay * 1000, Timeout.Infinite);
                        }

                        //ServerPush = 3
                        if (_ltScenario == 3)
                        {
                            //Nothing to do
                        }


                        //Login Filters = 4
                        if (_ltScenario == 4)
                        {
                            //Nothing to do
                        }


                        //Continuous load testing = 5
                        if (_ltScenario == 5)
                        {
                            _timer = new Timer(TimerCallbackScenario5Create, _timer, _initialDelay * 1000, _clientPush * 1000 );
                        }
                    }

                    //Continuous load testing = 5
                    if (_ltScenario == 5)
                    {
                        lock (_updateTimerLock)
                        {
                            if (_updateTimer != null)
                            {
                                _initialDelay = 30;
                                _updateTimer.Dispose();
                            }

                            _updateTimer = new Timer(TimerCallbackScenario5Update, _updateTimer, _initialDelay * 1000, _clientPush * 1000);
                        }
                    }

                    break;

                case LoginStatusInfo.LogoutOK:
                    WriteLog("Logout-Success-Credential " + Communication.GetClient().CredentialId);
                    break;

                case LoginStatusInfo.LoginError:

                    WriteLog("Login-error");

                    //_sending = false;
                    Thread.Sleep(2000);
                    this.Login();
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    if (!_disposed)
                    {
                        Communication.LoginStatus -= new LoginStatusDelegate(Communication_LoginStatus);

                        if (_timer != null)
                        {
                            _timer.Dispose();
                        }

                        if (_updateTimer != null)
                        {
                            _updateTimer.Dispose();
                        }

                        //if (_deleteTimer != null)
                        //{
                        //    _deleteTimer.Dispose();
                        //}

                        _disposed = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Client: " + ClientId + ". Exception: " + ex.ToString());
                }
            }

            bool cancel;
            _synchronization.Dispose(out cancel);
            base.Dispose(disposing);
        }

        private void TimerCallbackScenario1(object state)
        {
            if (Monitor.TryEnter(_timerLock, 1000))
            {
                try
                {
                    if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
                    {
                        try
                        {
                            _customModule.CreateMarketOrders(_numMessages, ClientId);
                            PushResult pushResult = _customModule.PushAllOrders();
                            foreach (Guid guid in pushResult.PushedEntityIds)
                            {
                                WriteLog("Pushed " + guid.ToString() + " from " + ClientId);
                            }
                        }
                        catch (NotLoggedInExeption ee)
                        {
                            WriteLog(ee.StackTrace);
                        }
                        catch (Exception ex)
                        {
                            WriteLog(ex.StackTrace);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(_timerLock);
                }
            }
        }

        private void TimerCallbackScenario2(object state)
        {
            if (Monitor.TryEnter(_timerLock, 1000))
            {
                try
                {
                    if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
                    {
                        try
                        {
                            WriteLog("Going-To-Pull");
                            _customModule.PullAllOrders();
                        }
                        catch (NotLoggedInExeption ee)
                        {
                            WriteLog(ee.StackTrace);
                        }
                        catch (Exception ex)
                        {
                            WriteLog(ex.StackTrace);
                        }
                    }
                    else
                    {
                        WriteLog("Connection-not-ready-for-client" + Communication.GetClient().CredentialId);
                        RetryPull();
                    }
                }
                finally
                {
                    Monitor.Exit(_timerLock);
                }
            }
        }

        private void RetryPull()
        {
            if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
            {
                try
                {
                    _customModule.PullAllOrders();
                }
                catch (NotLoggedInExeption ee)
                {
                    WriteLog(ee.StackTrace);
                }
                catch (Exception ex)
                {
                    WriteLog(ex.StackTrace);
                }
            }
            else
            {
                WriteLog("Connection-not-ready-for-client" + Communication.GetClient().CredentialId);
                RetryPull();
            }
        }

        private void TimerCallbackScenario5Create(object state)
        {
            if (Monitor.TryEnter(_timerLock, 1000))
            {
                try
                {
                    if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
                    {
                        try
                        {
                            Guid id = _customModule.CreateMarketOrder();
                            //AddToTemporaryGuidList(id);
                        }
                        catch (NotLoggedInExeption ee)
                        {
                            WriteLog(ee.StackTrace);
                        }
                        catch (Exception ex)
                        {
                            WriteLog(ex.StackTrace);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(_timerLock);
                }
            }
        }

        private void TimerCallbackScenario5Update(object state)
        {
            if (Monitor.TryEnter(_updateTimerLock, 1000))
            {
                try
                {
                    if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
                    {
                        try
                        {
                            Guid guid = SelectRandomIdFromTemporaryGuidList();
                            if (guid != Guid.Empty)
                            {
                                _customModule.UpdateMarketOrder(guid);
                            }
                        }
                        catch (NotLoggedInExeption ee)
                        {
                            WriteLog(ee.StackTrace);
                        }
                        catch (Exception ex)
                        {
                            WriteLog(ex.StackTrace);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(_updateTimerLock);
                }
            }
        }

        //private void TimerCallbackScenario5Delete(object state)
        //{
        //    if (Monitor.TryEnter(_deleteTimerLock, 1000))
        //    {
        //        try
        //        {
        //            if (!Paused && Communication.IsLoggedIn && Communication.IsConnected)
        //            {
        //                try
        //                {
        //                    Guid guid = RemoveRandomIdFromTemporaryGuidList();
        //                    if (guid != Guid.Empty)
        //                    {
        //                        _customModule.DeleteMarketOrder(guid);
        //                    }
        //                }
        //                catch (NotLoggedInExeption ee)
        //                {
        //                    WriteLog(ee.StackTrace);
        //                }
        //                catch (Exception ex)
        //                {
        //                    WriteLog(ex.StackTrace);
        //                }
        //            }
        //        }
        //        finally
        //        {
        //            Monitor.Exit(_deleteTimerLock);
        //        }
        //    }
        //}

        public void AddToTemporaryGuidList(Guid id)
        {
            Random rd = new Random();
            //int i = rd.Next(5);
            //if (i == 3)
            //{
            lock (_temporaryGuidListForUpdate)
            {
                _temporaryGuidListForUpdate.Add(id);
                WriteLog("Added-to-temGuID-" + id);
            }
            //}
        }

        private Guid SelectRandomIdFromTemporaryGuidList()
        {
            Random rd = new Random();
            Guid id = Guid.Empty;

            lock (_temporaryGuidListForUpdate)
            {
                if (_temporaryGuidListForUpdate.Count >= 2)
                {
                    int i = rd.Next(_temporaryGuidListForUpdate.Count - 1);
                    id = _temporaryGuidListForUpdate[i];
                }
                else if (_temporaryGuidListForUpdate.Count == 1)
                {
                    id = _temporaryGuidListForUpdate[0];
                }
            }

            return id;
        }

        public void RemoveFromTemporaryGuidList(Guid id)
        {
            lock (_temporaryGuidListForUpdate)
            {
                _temporaryGuidListForUpdate.Remove(id);
            }
        }

        public override void OnStart()
        {
            Connect();
        }

        public override void OnStop()
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }

        private string GetFilenameYYYMMDD(string suffix, string extension)
        {
            return string.Format("{0}-{1:D4}_{2}.{3}", DateTime.Now.ToString("yyyy-MM-dd"), Communication.GetClient().CredentialId, suffix, extension);
        }

        public void WriteLog(String message)
        {
            //just in case: we protect code with try.
            try
            {
                string filename = AppDomain.CurrentDomain.BaseDirectory +
                                  AppDomain.CurrentDomain.RelativeSearchPath + GetFilenameYYYMMDD("client", "log");

                System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                XElement xmlEntry = new XElement("logEntry",
                                                 new XElement("Date", DateTime.Now.ToString()),
                                                 new XElement("Message", message));
                sw.WriteLine(xmlEntry);
                sw.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}
