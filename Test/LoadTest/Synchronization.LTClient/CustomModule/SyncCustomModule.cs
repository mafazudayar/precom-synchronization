﻿using System;
using System.Collections.Generic;
using PreCom.Synchronization;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Test;

namespace SyncClientTestModule
{
    public class SyncCustomModule : PreCom.Core.ModuleBase
    {
        #region Private fields

        /// <summary>
        /// Indicates if the module has been initialized or not.
        /// </summary>
        private bool _isInitialized;

        internal SynchronizationModule SynchronizationModule;

        internal MarketOrderManager MarketOrderManager;

        internal SyncLTClient SyncLTClient;

        public SyncCustomModule(SynchronizationModule synchronizationModule, SyncLTClient syncLTClient)
        {
            SyncLTClient = syncLTClient;
            SynchronizationModule = synchronizationModule;

            MarketOrderMemoryDao marketOrderMemoryDao = new MarketOrderMemoryDao();
            MarketOrderManager = new MarketOrderManager(marketOrderMemoryDao);
            SynchronizationModule.RegisterStorage<MarketOrder>(MarketOrderManager);

            MarketOrderManager.EntityChanged += delegate(object sender, ChangeEventArgs args)
                {
                    if (args.OriginatorType != GetType().Name)
                    {
                        switch (args.Change)
                        {
                            case Operation.Created:
                                SyncLTClient.WriteLog("Created entity " + args.Id + " by " + args.CredentialId);
                                syncLTClient.AddToTemporaryGuidList(args.Id);
                                break;
                            case Operation.Updated:
                                SyncLTClient.WriteLog("Updated entity " + args.Id + " by " + args.CredentialId);
                                break;
                            case Operation.Deleted:
                                SyncLTClient.WriteLog("Deleted entity " + args.Id + " by " + args.CredentialId);
                                break;
                        }
                    }

                    if (args.OriginatorType == GetType().Name && syncLTClient.Scenario == 5)
                    {
                        switch (args.Change)
                        {
                            case Operation.Created:
                                SyncLTClient.WriteLog("CreatedAndAutoPush entity " + args.Id + " by " + args.CredentialId);
                                break;
                            case Operation.Updated:
                                SyncLTClient.WriteLog("UpdatedAndAutoPush entity " + args.Id + " by " + args.CredentialId);
                                break;
                            case Operation.Deleted:
                                SyncLTClient.WriteLog("DeletedAndAutoPush entity " + args.Id + " by " + args.CredentialId);
                                break;
                        }
                    }
                };
        }

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// Gets the name of the PreCom module.
        /// </summary>
        /// <value>The name of the module.</value>
        public override string Name
        {
            get
            {
                // TODO: Change module name.
                return "SyncTest";
            }
        }

        #endregion Public properties

        #region ModuleBase members

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        /// <param name="cancel">Cancel the dispose of the module</param>
        /// <returns>
        /// true if disposing was OK, false otherwise.
        /// </returns>
        public override bool Dispose(out bool cancel)
        {
            cancel = false;
            _isInitialized = false;

            return true;
        }

        /// <summary>
        /// Initializes the instance.
        /// </summary>
        /// <returns>
        /// true if initializing was OK, false otherwise.
        /// </returns>
        public override bool Initialize()
        {
            _isInitialized = true;
            return true;
        }

        /// <summary>
        /// Check if instance is initialized or not.
        /// </summary>
        /// <value>True if initialized, else false</value>
        public override bool IsInitialized
        {
            get
            {
                return _isInitialized;
            }
        }

        #endregion ModuleBase members

        public void CreateMarketOrders(int nOrders, string clientId)
        {
            for (int i = 0; i < nOrders; i++)
            {
                MarketOrder order = CreateMarketOrder(GetDescription());
                MarketOrderManager.Create(order, new CreateArgs(GetType().Name));
            }
        }

        public Guid CreateMarketOrder()
        {
            MarketOrder order = CreateMarketOrder(GetDescription());
            MarketOrderManager.Create(order, new CreateArgs(GetType().Name));
            return order.Guid;
        }

        public void UpdateMarketOrder(Guid guid)
        {
            Filter f = new Filter { Operator = FilterOperator.EqualTo, PropertyName = "Id", Value = guid };
            EntityFilter ef = new EntityFilter();
            ef.FilterList.Add(f);
            ICollection<ISynchronizable> orders = MarketOrderManager.Read(ef);
            string description = string.Empty;

            foreach (ISynchronizable synchronizable in orders)
            {
                MarketOrder order = (MarketOrder)synchronizable;

                description = UpdateDescription(order.Description);

                if (description=="Delete")
                {
                    SyncLTClient.RemoveFromTemporaryGuidList(order.Guid);
                    DeleteMarketOrder(order.Guid);
                }
                else
                {
                    MarketOrder newOrder = new MarketOrder()
                    {
                        Id = order.Id,
                        Creator = order.Creator,
                        Date = order.Date,
                        Description = description,
                        Guid = order.Guid,
                        IsAssigned = order.IsAssigned
                    };


                    MarketOrderManager.Update(newOrder, new UpdateArgs(GetType().Name));
                }
            }
        }

        public void DeleteMarketOrder(Guid guid)
        {
            MarketOrderManager.Delete(guid, new DeleteArgs(GetType().Name));
        }

        private string UpdateDescription(string description)
        {
            try
            {
                string[] splitted = description.Split('_');
                if (splitted.Length == 1)
                {
                    return description + "_2";
                }
                if (splitted.Length > 1)
                {
                    int version = Convert.ToInt32(splitted[1]);
                    if (version < 200)
                    {
                        return splitted[0] + "_" + ++version;
                    }

                    description = "Delete";
                }

                return description;
            }
            catch
            {
                return description;
            }
        }

        public PushResult PushAllOrders()
        {
            return SynchronizationModule.Push<MarketOrder>();
        }

        public PullResult PullAllOrders()
        {
            string creator = GetCreator();
            Filter filter = new Filter() { Operator = FilterOperator.EqualTo, PropertyName = "Creator", Value = creator };
            EntityFilter filters = new EntityFilter();
            filters.FilterList.Add(filter);
            PullArgs args = new PullArgs(filters);

            PullResult results = SynchronizationModule.Pull<MarketOrder>(args);
            SyncLTClient.WriteLog("Pulling-For-" + creator + "-SyncSession:-" + results.SyncSession);
            return results;
        }

        private string GetCreator()
        {
            string client = "Creator_";

            Random rn = new Random();
            int i = rn.Next(2, 101);

            return client + i;
        }

        public void RegisterForAutoPush()
        {
            SynchronizationModule.RegisterAutomaticPush<MarketOrder>(Operation.Created | Operation.Updated | Operation.Deleted);
        }

        private MarketOrder CreateMarketOrder(string description)
        {
            Random rn = new Random();
            int a = rn.Next(1, 2005);
            MarketOrder order = new MarketOrder();
            Guid id = Guid.NewGuid();
            order.Guid = id;
            order.Creator = "Creator_"+a;
            order.Description = description;
            order.Date = DateTime.Now;
            order.IsAssigned = true;

            return order;
        }

        private String GetDescription()
        {
            Random rn = new Random();
            int i = rn.Next(10);

            if (i == 1)
            {
                return "Monitor";
            }

            if (i == 2)
            {
                return "Keyboard";
            }

            if (i == 3)
            {
                return "Mouse";
            }

            if (i == 4)
            {
                return "RAM";
            }

            if (i == 5)
            {
                return "CPU";
            }

            if (i == 6)
            {
                return "Web cam";
            }

            if (i == 7)
            {
                return "Hard disk";
            }

            if (i == 8)
            {
                return "CD rom";
            }

            if (i == 9)
            {
                return "mother board";
            }

            return "Books";
        }
    }
}
