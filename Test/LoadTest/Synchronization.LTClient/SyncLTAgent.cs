﻿using System.Collections.Generic;
using System.Globalization;
using PreCom.Synchronization.Core;
using PreComLT.LoadTest.Client;
using PreComLT.LoadTest.Util;

namespace Synchronization.LTClient
{
    public class SyncLTAgent : AgentBase<SyncLTClient>
    {
        private const string ProtocolSettingKey = "Protocol";
        private const string ProtocolSettingValue = "psp";

        private const string ClientPushSettingKey = "tClientPush";
        private const string ClientPushSettingValue = "180";

        private const string InitialDelaySettingKey = "tInitialDelay";
        private const string InitialDelaySettingValue = "300";

        private const string NumMessagesSettingKey = "NumMessages";
        private const string NumMessagesSettingValue = "1";

        private const string LoadTestScenarioSettingKey = "LoadTestScenario";
        private const string LoadTestScenarioSettingValue = "1";

        private const string DeltaSyncSettingKey = "DeltaSync";
        private const string DeltaSyncSettingValue = "true";

        public SyncLTAgent()
        {
            InitializeSettings();
        }

         protected override SyncLTClient CreateClient(string serverHost, int pspPort, int httpPort, int index)
        {
            var protocol = Settings[ProtocolSettingKey];

            var clientId = "SyncLT_" + index.ToString(CultureInfo.InvariantCulture);
            var credentialsList = new List<KeyValuePair<string, string>>()
            { 
                new KeyValuePair<string, string>("Username", index.ToString(CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("Password", index.ToString(CultureInfo.InvariantCulture))
            };

            SyncLTClient syncLTClient = new SyncLTClient(clientId, protocol, serverHost, pspPort, httpPort, credentialsList,
                                int.Parse(Settings[NumMessagesSettingKey]),
                                int.Parse(Settings[ClientPushSettingKey]),
                                int.Parse(Settings[InitialDelaySettingKey]),
                                int.Parse(Settings[LoadTestScenarioSettingKey]),
                                bool.Parse(Settings[DeltaSyncSettingKey]));

            return syncLTClient;
        }

        private void InitializeSettings()
        {
            Settings.Add(ProtocolSettingKey, AppSettingHelper.Read(ProtocolSettingKey, ProtocolSettingValue));
            Settings.Add(ClientPushSettingKey, AppSettingHelper.Read(ClientPushSettingKey, ClientPushSettingValue));
            Settings.Add(InitialDelaySettingKey, AppSettingHelper.Read(InitialDelaySettingKey, InitialDelaySettingValue));
            Settings.Add(NumMessagesSettingKey, AppSettingHelper.Read(NumMessagesSettingKey, NumMessagesSettingValue));
            Settings.Add(LoadTestScenarioSettingKey, AppSettingHelper.Read(LoadTestScenarioSettingKey, LoadTestScenarioSettingValue));
            Settings.Add(DeltaSyncSettingKey, AppSettingHelper.Read(DeltaSyncSettingKey, DeltaSyncSettingValue));
        }
    }
}
