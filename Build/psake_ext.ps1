function Get-TeamCityBuildNumber {
    return (Get-Int32OrDefault "$env:BUILD_NUMBER")
}

function Get-BambooBuildNumber {
    return (Get-Int32OrDefault "$env:bamboo_BuildNumber")
}

function Get-BuildNumberFromCISystem {
    $buildNumber = Get-TeamCityBuildNumber
    if ($buildNumber -gt 0) { return $buildNumber }
    
    $buildNumber = Get-BambooBuildNumber
    if ($buildNumber -gt 0) { return $buildNumber }

    return 0
}

function Get-BuildNameFromCISystem {
    return $env:bamboo_buildKey
}

function Get-VersionFromCISystemOrFileOrGitTag {
    $customVersion = $env:bamboo_version
    if (!$customVersion) {
        return Get-VersionFromVersionFileOrGitTag
    } else {
        return $customVersion
    }
}

function Get-Version
{
    return Get-VersionFromCISystemOrFileOrGitTag
}

function Get-ShortVersion
{
param(
    [string]$version
)

    return $version.Trim(".0")
}

function Add-BuildNumberToVersion
{
param(
    [string]$version,
    [int]$buildNumber
)

    $pos = 0
    $newVersion = ""

    $tokens = $version.Split(".")
    if ($tokens.length -ne 4) {
        throw "Invalid version $version"
    }

    $tokens | ForEach {
        $pos++
        if ($pos -lt 4) {
            $newVersion += "$_."
        } else {
            $newVersion += "$buildNumber"
        }
    }
    return $newVersion
}

function Get-Int32OrDefault($s) {
    [int]$result = $null
    $ignored = [System.Int32]::TryParse($s, [ref]$result)
    return $result # if we failed, $result will be 0
}

function Get-IsGitPath
{
param(
    [string]$path
)

    try {
        & $path --version > $null
    } catch {
        return $false
    }
    return $true
}

function Invoke-Git
{
param(
    [string[]]$cmdArgs
)

    $gitExe = "git"
    $test = Get-IsGitPath($gitExe)
    if ($test -eq $false) {
        $gitExe = "C:\Program Files (x86)\git\bin\git"
        $test = Get-IsGitPath($gitExe)
        if ($test -eq $false) {
            throw "Can not find git.exe, please install git and make sure it is in the PATH"
        }
    }

    Exec { & $gitExe $cmdArgs }
}

function Get-GitDescribe
{
    try {
        return (Invoke-Git @("describe", "--always")) 2>$null
    } catch {
        return Get-GitCommit
    }
}

function Get-GitBranch
{
    try {
        return ((Invoke-Git @("symbolic-ref", "HEAD")).Replace("refs/heads/", "")) 2>$null
    } catch {
        return "detached"
    }
}

function Get-GitCommit
{
    return (Invoke-Git @("log", "--pretty=format:%h", "-1")) 2>$null
}

function Get-VersionFromGitTag
{
    try {
      $version = (Invoke-Git @("describe", "--tags", "--abbrev=0")) 2>$null
      return Get-ParsedVersionOrDefault $version
    } catch {
        return "0.0.0.0"
    }
}

function Get-VersionFromVersionFileOrGitTag {
    $version = Get-VersionInfoFromFile
    if (!$version) {
        $version = Get-VersionFromGitTag
    }

    return Get-ParsedVersionOrDefault $version
}

function Get-VersionFromVersionFile
{
    $version = Get-VersionInfoFromFile
    return Get-ParsedVersionOrDefault $version
}

function Get-VersionInfoFromFile
{
    if (Test-Path "VERSION") {
        return gc "VERSION"
    } elseif (Test-Path "..\VERSION") {
        return gc "..\VERSION"
    } elseif (Test-Path "VERSION.txt") {
        return gc "VERSION.txt"
    } elseif (Test-Path "..\VERSION.txt") {
        return gc "..\VERSION.txt"
    }

    return $null
}

function Get-ParsedVersionOrDefault($s) {
    if ($version -match "v?([0-9]+(\.[0-9]+(\.[0-9]+(\.[0-9]+)?)?)).*") {
        return $matches[1]
    } else {
        return "0.0.0.0"
    }
}

function Get-ExpandedVersion
{
param(
    [string]$s,
    [int]$ReplaceMajor = -1,
    [int]$ReplaceMinor = -1,
    [int]$ReplaceBuild = -1,
    [int]$ReplaceRevision = -1
)

    $version = New-Object -type System.Version $s
    $major = if ($version.Major -gt 0) { $version.Major } else { 0 }
    $minor = if ($version.Minor -gt 0) { $version.Minor } else { 0 }
    $build = if ($version.Build -gt 0) { $version.Build } else { 0 }
    $revision = if ($version.Revision -gt 0) { $version.Revision } else { 0 }

    if ($ReplaceMajor -gt 0) { $major = $ReplaceMajor }
    if ($ReplaceMinor -gt 0) { $minor = $ReplaceMajor }
    if ($ReplaceBuild -gt 0) { $build = $ReplaceBuild }
    if ($ReplaceRevision -gt 0) { $revision = $ReplaceRevision }

    return "$major.$minor.$build.$revision"
}

function Get-PathRooted
{
param(
    [string]$path
)
    if (![System.IO.Path]::IsPathRooted($path)) {
        $path = [System.IO.Path]::Combine((Get-Location), $path)
    }

    return $path
}

function Delete-DirectoryIfExists
{
param(
    [string]$Path
)

    $Path = Get-PathRooted $Path
    if ([System.IO.Directory]::Exists($Path)) {
        Remove-Item -Recurse -Force $Path
    }
}

function Delete-ItemIfExists
{
param(
    [string]$Path
)

    if (Test-Path $Path) {
        Remove-Item $Path -Force
    }
}

function Copy-ItemIfExists
{
param(
    [string]$Path,
    [string]$Destination,
    [switch]$Recurse
)

    if (Test-Path $Path) {
        if ($Recurse) {
            Copy-Item $Path $Destination -Recurse
        } else {
            Copy-Item $Path $Destination
        }
    }
}

function Create-DirectoryIfNotExists
{
param(
    [string]$Path
)

    $Path = Get-PathRooted $Path
    if (![System.IO.Directory]::Exists($Path)) {
        [System.IO.Directory]::CreateDirectory($Path)
    }
}

function Create-ParentDirectoryIfNotExists
{
param(
    [string]$Path
)

    $parentDirectory = [System.IO.Path]::GetDirectoryName($Path)
    Create-DirectoryIfNotExists $parentDirectory
}

function Get-InformationalVersion
{
param(
    [string]$Version,
    [string]$BuildName,
    [int]$BuildNumber,
    [switch]$NoGitDescribe,
    [switch]$NoVersionFromFile
)

    $version = if ($Version) { $Version } else { Get-Version }
    $buildNumber = if ($BuildNumber) { $BuildNumber } else { Get-BuildNumberFromCISystem }
    $buildName = if ($BuildName) { $BuildName } else { Get-BuildNameFromCISystem }
    $buildInformation = if ($buildNumber -gt 0 -and $buildName) { "#$buildNumber $buildName" } else { "Local build" }
    $versionFileContents = ""
    if ($NoVersionFromFile -eq $false) {
        $versionFileContents = Get-VersionInfoFromFile
    }
    $gitDescribe = Get-GitDescribe
    if ($NoGitDescribe -eq $true) {
        $gitDescribe = Get-GitCommit
    }
    $gitBranch = Get-GitBranch
    $userName = [System.Environment]::UserName
    $machineName = [System.Environment]::MachineName
    $timestamp = (get-date).ToString("o")
    
    return "$version ($buildInformation) $versionFileContents $gitDescribe-$gitBranch $timestamp $userName@$machineName"
}

function Generate-AssemblyInfo
{
param(
    [string]$title,
    [string]$description,
    [string]$company = "PocketMobile Communications AB",
    [string]$product,
    [string]$copyright = "Copyright (c) PocketMobile Communications AB $((get-date).ToString('yyyy'))",
    [string]$version,
    [string]$informationalVersion,
    [string]$configuration,
    [string]$culture,
    [string]$trademark,
    [switch]$CLSCompliant = $false,
    [string]$file = $(throw "file is a required parameter.")
)

    Assert (Test-Path $file) "Cannot generate assembly info, file not found: $file"
    
    $CLSCompliantValue = if ($CLSCompliant -eq $true) { "true" } else { "false" }

    $asmInfo = "/*
 * This file was generated by psake_ext.ps1 on $((get-date).ToString('o'))
 *
 * Any modifications to this file will be overwritten by build tools when updating version numbers.
 *
 * IF YOU NEED TO EDIT THESE VALUES, PLEASE SEE THE PSAKE BUILD FILE 'default.ps1'
 *
 * If you need to add additional assembly attributes, add another .cs file to your project and
 * add the attributes in that file, AssemblyInfo.Static.cs for example.
 */
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


[assembly: CLSCompliant($CLSCompliantValue)]
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute(""$title"")]
[assembly: AssemblyDescriptionAttribute(""$description"")]
[assembly: AssemblyCompanyAttribute(""$company"")]
[assembly: AssemblyProductAttribute(""$product"")]
[assembly: AssemblyCopyrightAttribute(""$copyright"")]
[assembly: AssemblyVersionAttribute(""$version"")]
[assembly: AssemblyConfiguration(""$configuration"")]
[assembly: AssemblyTrademark(""$trademark"")]
[assembly: AssemblyCulture(""$culture"")]
#if !PocketPC
[assembly: AssemblyFileVersionAttribute(""$version"")]
#endif
#pragma warning disable 1607
[assembly: AssemblyInformationalVersionAttribute(""$informationalVersion"")]
#pragma warning restore 1607
"

    $dir = [System.IO.Path]::GetDirectoryName($file)
    if ([System.IO.Directory]::Exists($dir) -eq $false)
    {
        Write-Host "Creating directory $dir"
        [System.IO.Directory]::CreateDirectory($dir)
    }

    Write-Host "Generating assembly info file: $file"
    Set-Content $file $asmInfo -Encoding String
}

function Generate-VersionAssemblyInfo
{
param(
    [string]$version,
    [string]$informationalVersion,
    [string]$file = $(throw "file is a required parameter.")
)

  $asmInfo = "/*
 * This file was generated by psake_ext.ps1 on $((get-date).ToString('o'))
 *
 * Any modifications to this file will be overwritten by build tools when updating version numbers.
 *
 * If you need to add additional assembly attributes, add another .cs file to your project and
 * add the attributes in that file, AssemblyInfo.Static.cs for example.
 */
using System.Reflection;

[assembly: AssemblyVersionAttribute(""$version"")]
#if !PocketPC
[assembly: AssemblyFileVersionAttribute(""$version"")]
#endif
#pragma warning disable 1607
[assembly: AssemblyInformationalVersionAttribute(""$informationalVersion"")]
#pragma warning restore 1607
"

    $dir = [System.IO.Path]::GetDirectoryName($file)
    if ([System.IO.Directory]::Exists($dir) -eq $false)
    {
        Write-Host "Creating directory $dir"
        [System.IO.Directory]::CreateDirectory($dir)
    }

    Write-Host "Generating assembly info file: $file"
    $Utf8WithoutByteOrderMark = New-Object System.Text.UTF8Encoding($false)
    [System.IO.File]::WriteAllLines($file, $asmInfo, $Utf8WithoutByteOrderMark)
}

function Update-Win32RCFile
{
param(
    [string]$title,
    [string]$description,
    [string]$company,
    [string]$product,
    [string]$copyright,
    [string]$version,
    [string]$informationalVersion,
    [string]$configuration,
    [string]$culture,
    [string]$trademark,
    [string]$originalFileName,
    [string]$file = $(throw "file is a required parameter.")
)

    $contents = Get-Content $file
    $parsedVersion = new-object -type System.Version (Get-ExpandedVersion $version)
    
    $major = $parsedVersion.Major
    $minor = $parsedVersion.Minor
    $build = $parsedVersion.Build
    $revision = $parsedVersion.Revision

    $contents = ($contents -replace "^ (FILEVERSION|PRODUCTVERSION) ([0-9]+),([0-9]+),([0-9]+),([0-9]+)$", (' $1 '+"$major,$minor,$build,$revision"))
    $contents = ($contents -replace "^((\s*)VALUE ""CompanyName"",) ""[^""]+""$", ('$1'+" ""$company"""))
    $contents = ($contents -replace "^((\s*)VALUE ""FileDescription"",) ""[^""]+""$", ('$1'+" ""$description"""))
    $contents = ($contents -replace "^((\s*)VALUE ""FileVersion"",) ""[^""]+""$", ('$1'+" ""$major, $minor, $build, $revision"""))
    $contents = ($contents -replace "^((\s*)VALUE ""InternalName"",) ""[^""]+""$", ('$1'+" ""$title"""))
    $contents = ($contents -replace "^((\s*)VALUE ""LegalCopyright"",) ""[^""]+""$", ('$1'+" ""$copyright"""))
    $contents = ($contents -replace "^((\s*)VALUE ""OriginalFileName"",) ""[^""]+""$", ('$1'+" ""$originalFileName"""))
    $contents = ($contents -replace "^((\s*)VALUE ""ProductName"",) ""[^""]+""$", ('$1'+" ""$product"""))
    $contents = ($contents -replace "^((\s*)VALUE ""ProductVersion"",) ""[^""]+""$", ('$1'+" ""$informationalVersion"""))

    Write-Host "Generating Win32 *.rc file: $file"
    Set-Content $file $contents -Encoding ASCII
}

function Update-BuildGradleFile
{
param(
    [string]$version,
    [string]$file = $(throw "file is a required parameter.")
)

    $contents = Get-Content $file
    $contents = ($contents -replace "version(\s*)=(\s*)'([0-9]+).([0-9]+).([0-9]+).([0-9]+)'$", "version = '$version'")

    Write-Host "Generating gradle file: $file"
    Set-Content $file $contents -Encoding ASCII
}

function Get-AndroidVersionCode
{
param(
    [string]$version
)

    $tokens = $version.Split(".")
    if ($tokens.length -ne 4) {
        throw "Invalid version $version"
    }

    $pos = 0
    $tokens | ForEach {
        $pos++
        if ($pos -eq 1) {
            $versionCode = $_
        } else {
            $versionCode += $_.PadLeft(2, "0")
        }
    }

    return "$versionCode"
}

function Update-AndroidManifest
{
param(
    [string]$version,
    [string]$file = $(throw "file is a required parameter.")
)

    [xml]$manifest = Get-Content $file
    $versionCode = Get-AndroidVersionCode $version
    Write-Host "Android version code: $versionCode"
    $manifest.manifest.versionCode = "$versionCode"
    $manifest.manifest.versionName = "$version"

    # If I saved the file with $writer.Save($file) it ended up having a UTF-8 BOM which android tools didn't like
    $settings = New-Object System.Xml.XmlWriterSettings
    $settings.Encoding = New-Object System.Text.UTF8Encoding($False)
    $writer = [System.Xml.XmlWriter]::Create($file, $settings)
    $manifest.WriteTo($writer)
    $writer.Close()
}

function Update-JavaReleaseInformation
{
param(
    [string]$version,
    [string]$packageName,
    [string]$file = $(throw "file is a required parameter.")
)

    $date = Get-Date
    $calendar =  "{0}, {1}, {2}, 12, 0, 0" -f $date.Year, ($date.Month - 1), $date.Day
    $versionCode = Get-AndroidVersionCode $version
    $content = "/*
 * This file was generated by psake_ext.ps1 on $((get-date).ToString('o'))
 *
 * Any modifications to this file will be overwritten by build tools when updating version numbers.
 */
package $packageName;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ReleaseInformation {
    static {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(""UTC""));
        // The java calendar starts with month 0 = January.
        cal.set($calendar);
        RELEASE_DATE = cal.getTime();
    }

    public static final int VERSION_CODE = $versionCode;
    public static final String VERSION_NAME = ""$version"";
    public static final Date RELEASE_DATE;
}
"
    
    Write-Host "Generating java release info file: $file"
    Set-Content $file $content -Encoding ASCII
}

function Dotfuscator
{
param(
    [string]$AssemblyFileName,
    [string]$dotfuscator = "C:\Program Files (x86)\PreEmptive Solutions\Dotfuscator Professional Edition 4.10\dotfuscator.exe"
)

    Assert (Test-Path $AssemblyFileName) "Cannot obfuscate assembly, file not found: $AssemblyFileName"

    $assemblyName = [System.IO.Path]::GetFileNameWithoutExtension($AssemblyFileName)
    $directory = [System.IO.Path]::GetDirectoryName($AssemblyFileName)
    $mapFile = [System.IO.Path]::Combine($directory, $assemblyName + ".dotfuscationmap.xml")

    Exec { & $dotfuscator -in:$AssemblyFileName -out:$directory -debug:on -mapout:$mapFile -prune:off }
}

function SignAssembly
{
param(
    [string]$AssemblyFileName,
    [string]$CertificateFileName = $env:bamboo_signCertificateFileName,
    [string]$CertificatePassword = $env:bamboo_signCertificatePassword,
    [string]$signtool = "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\signtool.exe"
)

    Exec { & $signtool sign /t http://timestamp.digicert.com/ /f $CertificateFileName /p $CertificatePassword $AssemblyFileName }
}

function Get-PathOrTryOneLevelUp
{
param(
    [string]$path
)
    if (![System.IO.Path]::IsPathRooted($path)) {
        $rootedPath = [System.IO.Path]::GetFullPath("$(get-location)\$path")
    } else {
        $rootedPath = $path
    }

    if (![System.IO.File]::Exists($rootedPath) -and $rootedPath -ne $path) {
        $oneLevelUp = "$(get-location)\..\" + $path
        $oneLevelUp = [System.IO.Path]::GetFullPath($oneLevelUp)
        if ([System.IO.File]::Exists($oneLevelUp)) {
            $rootedPath = $oneLevelUp
        }
    }

    return $rootedPath
}

function Copy-Recursive
{
param(
    [string]$Source,
    [string]$Output,
    [string]$Exclude
)

    if ($Exclude) {
        ROBOCOPY /e $Source $Output "/XD" $Exclude
    } else {
        ROBOCOPY /e $Source $Output $Exclude
    }

    # ROBOCOPY does not confirm to standard CLI convention...
    # See http://support.microsoft.com/kb/954404 for the list of ROBOCOPYs exit codes
    # Greater than 5 means that ROBOCOPY has encountered errors
    if ($LastExitCode -gt 5) {
        throw "ROBOCOPY expected to return with exit code 1, but returned with $LastExitCode"
    }
}

function Trim-VersionFromChildDirectories {
param(
    [string]$Path,
    [string]$Filter,
    [switch]$Rename
)

    Get-ChildItem $Path -Filter $Filter | % {
        if ($Rename) {
            Trim-VersionFromDirectory $_.FullName -Rename
        } else {
            Trim-VersionFromDirectory $_.FullName
        }
    }
}

function Trim-VersionFromDirectory {
param(
    [string]$Path,
    [switch]$Rename
)

    if (![System.IO.Directory]::Exists($Path)) {
        return
    }

    # Example "A\Path\MyPackage-1.2.3.4" to "A\Path\MyPackage"
    $directory = [System.IO.Path]::GetDirectoryName($Path)
    $fileName = [System.IO.Path]::GetFileName($Path)
    if ($fileName -match "((-|\.)([0-9]+(\.[0-9]+(\.[0-9]+(\.[0-9]+)?)?))(-[0-9A-Za-z\-]+(\.[0-9A-Za-z\-]+)*)?(\+[0-9A-Za-z\-]+(\.[0-9A-Za-z\-]+)*)?)$") {
        $fileNameWithoutVersion = $fileName.Replace($matches[1], "")
        $pathWithoutVersion = [System.IO.Path]::Combine($directory, $fileNameWithoutVersion)

        Write-Host "Trim-VersionFromDirectory: $Path -> $pathWithoutVersion"

        Delete-DirectoryIfExists $pathWithoutVersion
        if ($Rename) {
            Move-Item $Path $pathWithoutVersion
        } else {
            Copy-Recursive $Path $pathWithoutVersion
        }
    }
}

function OpenCover-GenerateReport
{
param(
    [string]$Reports = "$here\pkg\TestResults\OpenCover",
    [string]$Filter = "*.opencover.xml",
    [string]$Output = "$here\pkg\TestResults\CodeCoverageReport",
    [string]$ReportGeneratorExe = "$here\packages\ReportGenerator.1.8.1.0\ReportGenerator.exe"
)
    $Reports = [System.IO.Path]::Combine($Reports, $Filter)
    Exec { & $ReportGeneratorExe -reports:$Reports -targetdir:$Output }
}

function OpenCover-MSTest
{
param(
    [string]$TestContainer,
    [string]$TestResults = ".\TestResults\MSTest",
    [string]$Output = ".\TestResults\OpenCover",
    [string]$Filter = '+[*]* -[Moq]* -[Newtonsoft.Json.Compact]* -[Microsoft.*]* -[*Test*]* -[*.Mock.*]*',
    [string]$Register = 'user',
    [string]$MSTestExecutable = "C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\MSTest.exe",
    [string]$OpenCoverExe = ".\packages\OpenCover.4.5.1403\OpenCover.Console.exe"
)

    $assemblyName = [System.IO.Path]::GetFileNameWithoutExtension($TestContainer)
    $targetDir = [System.IO.Path]::GetDirectoryName($TestContainer)
    $fileName = [System.IO.Path]::GetFileName($TestContainer)

    $TestResults = Get-PathRooted $TestResults
    $Output = Get-PathRooted $Output

    $testResultsFile = [System.IO.Path]::Combine($TestResults, $assemblyName + ".trx")
    $outputFileName = [System.IO.Path]::Combine($Output, $assemblyName + ".opencover.xml")

    $OpenCoverExe = Get-PathOrTryOneLevelUp $OpenCoverExe

    Create-DirectoryIfNotExists $TestResults
    Create-DirectoryIfNotExists $Output
    
    # MSTest requires that the test results file doesn't exists when the tests begins... Delete it.
    if ([System.IO.File]::Exists($testResultsFile)) {
        [System.IO.File]::Delete($testResultsFile)
    }

    $registerSwitch = "-register:$Register"
    $filterSwitch = "-filter:$Filter"
    $targetSwitch

    Exec {
        & $OpenCoverExe "-register:$Register" "-filter:$Filter" "-target:$MSTestExecutable" `
            -targetargs:"""/testcontainer:$fileName"" /nologo /resultsfile:""$testResultsFile""" `
            -targetdir:"$targetDir" `
            -output:"$outputFileName" `
            -returntargetcode
    }
}

function Invoke-OpenCover {
param(
    [string]$Target,
    [string]$TargetArgs,
    [string]$TargetDir,
    [string]$Output,
    [switch]$ReturnTargetCode,
    [string]$Register = 'user',
    [string[]]$Filter = @('+[*]*', '-[Moq]*', '-[Newtonsoft.Json.Compact]*', '-[Microsoft.*]*', ' -[*Test*]*', '-[*.Mock.*]*', '-[FluentAssertions]*'),
    [string[]]$Exclude,
    [string[]]$Include,
    [string]$Path = ".\..\packages\OpenCover.4.5.1403\OpenCover.Console.exe"
)

    #$OpenCoverExe = Get-PathOrTryOneLevelUp $OpenCoverExe

    if (!$Filter) {
        $Filter = @()
    }

    if ($Include) {
        $Include | % {
            $Filter += "-$Include"
        }
    }

    if ($Exclude) {
        $Exclude | % {
            $Filter += "-$Exclude"
        }
    }

    $options = @()

    if ($Register) {
        $options += "-register:$Register"
    }
    if ($Filter) {
        $options += "-filter:$Filter"
    }
    if ($Target) {
        $options += "-target:$Target"
    }
    if ($TargetArgs) {
        $options += "-targetargs:$TargetArgs"
    }
    if ($TargetDir) {
        $options += "-targetdir:$TargetDir"
    }
    if ($Output) {
        Create-ParentDirectoryIfNotExists $Output
        $options += "-output:$Output"
    }
    if ($ReturnTargetCode) {
        $options += "-returntargetcode"
    }

    $options

    Exec { & "$Path" $options }
}

function Invoke-NUnit {
param(
    [string]$TestContainer,
    [string]$Include,
    [string]$Exclude,
    [switch]$NoLogo,
    [switch]$Cleanup,
    [switch]$Labels,
    [switch]$OpenCover,
    [string]$OpenCoverOutput = "$here\pkg\TestResults\OpenCover",
    [string[]]$OpenCoverFilter,
    [string[]]$OpenCoverExclude,
    [string[]]$OpenCoverInclude,
    [string]$TestResults = "$here\pkg\TestResults\NUnit",
    [string]$NUnitRunner = "$here\packages\NUnit.Runners.2.6.2\tools\nunit-console.exe"
)
    $assemblyName = [System.IO.Path]::GetFileNameWithoutExtension($TestContainer)
    $testResultsFile = [System.IO.Path]::Combine($TestResults, $assemblyName + ".xml")
    Create-ParentDirectoryIfNotExists $testResultsFile

    Write-Host "Invoke-NUnit: $TestContainer" -ForegroundColor Green

    $options = @($TestContainer, "/xml:""$testResultsFile""")
    
    if ($Include) {
        $options += "/include:$Include"
    }
    if ($Exclude) {
        $options += "/exclude:$Exclude"
    }
    if ($NoLogo) {
        $options += "/nologo"
    }
    if ($Cleanup) {
        $options += "/cleanup"
    }
    if ($Labels) {
        $options += "/labels"
    }

    if (-not $OpenCover) {
        Exec { & $NUnitRunner $options }
    } else {
        if (![System.IO.File]::Exists($OpenCoverOutput)) {
            $OpenCoverOutput = [System.IO.Path]::Combine($OpenCoverOutput, $assemblyName + ".opencover.xml")
        }

        $TestContainerDirectory = [System.IO.Path]::GetDirectoryName($TestContainer)
        Invoke-OpenCover `
            -Target $NUnitRunner `
            -TargetArgs $options `
            -TargetDir $TestContainerDirectory `
            -ReturnTargetCode `
            -Filter $OpenCoverFilter `
            -Exclude $OpenCoverExclude `
            -Include $OpenCoverInclude `
            -Output $OpenCoverOutput
    }
}

function NuGetHackToSetCredentials {
    $feedName = "PocketMobile"
    $feedUrl = "https://www.myget.org/F/pocketmobile"
    $feedUsername = "pocketmobilemyget"
    $feedPassword = "Krig&Fred"
    $feedApiKey = "e070ef68-19ce-4e9c-9e79-3a2796811ab5"

    if ($env:bamboo_nugetPocketMobileUrl) {
        $feedUrl = $env:bamboo_nugetPocketMobileUrl
    }
    if ($env:bamboo_nugetPocketMobileUsername) {
        $feedUsername = $env:bamboo_nugetPocketMobileUsername
    }
    if ($env:bamboo_nugetPocketMobilePassword) {
        $feedPassword = $env:bamboo_nugetPocketMobilePassword
    }
    if ($env:bamboo_nugetPocketMobileApiKeyPassword) {
        $feedApiKey = $env:bamboo_nugetPocketMobileApiKeyPassword
    }

    if (Test-Path ".\NuGet.exe") {
        $nuget = ".\NuGet.exe"
    }

    if (([System.String]::IsNullOrEmpty($feedUrl) -eq $false)) {
        Write-Host "Re-create NuGet source '$feedName' to work around the fact that I have not been able to create and save a proper NuGet.config file on the build image."
        & $nuget sources remove -name $feedName
        & $nuget setapikey $feedApiKey -source $feedUrl
        & $nuget sources add -name $feedName -source $feedUrl -Username $feedUsername -Password $feedPassword
    }
}

function DownloadZipFilesFromBitBucket {
param(
    [string]$bash = "C:\Program Files (x86)\Git\bin\bash.exe"
)
    $username = $env:bamboo_libraryUsername
    $password = $env:bamboo_libraryPassword

    if ($username -and $password) {
        $username = "--username=$username"
        $password = "--password=$password"
    }

    Exec { & $bash "--login" "-i" "gs.sh" "library" $username $password }
}

function Exec-TRXMerge
{
param(
    $Include = ".\TestResults\MSTest",
    $Output = ".\TestResults\TestResults.trx",
    $Exclude,
    $TrxFiles,
    $TrxMerge = ".\trxmerge.exe"
)

    Write-Host "Merging MSTest results to $Output"

    if (!$TrxFiles) {
        $TrxFiles = (Get-ChildItem $Include -Filter "*.trx" | foreach {
            if ($_.Name -ne $Exclude) {
                $_.FullName
                Write-Host "  Including: $($_.FullName)"
            } else {
                Write-Host "! Excluding: $($_.FullName)"
            }
        })
    }

    Write-Host "Merging..."
    Exec { & $TrxMerge $TrxFiles $Output }
    Assert (Test-Path $Output) "$Output does not exists. Perhaps one of the input .trx files is corrupt?"
    Write-Host "Merge was successful!"
}

function Compress-PackageOutput
{
param(
    [string]$PackageName,
    [string]$PackageOutput,
    [string]$ArchiveName,
    [string]$7za,
    [string]$Version
)
    $here = get-location
    if ([System.IO.Path]::GetFileName($here) -eq "build") {
        $here = [System.IO.Path]::Combine($here, "..")
        $here = [System.IO.Path]::GetFullPath($here)
    }

    if ($PackageName) {
        $packageName = $PackageName
    }

    if (!$PackageOutput) {
        $packageOutput = "$here\pkg"
    } else {
        $packageOutput = $PackageOutput
    }

    if (!$ArchiveName) {
        $buildName = Get-BuildNameFromCISystem
        $buildNumber = Get-BuildNumberFromCISystem
        if (!$Version) {
            $Version = Get-Version
        }
        if ($BuildName -and $BuildNumber) {
            $archiveName = "$buildName-$buildNumber-$Version"
        } else {
            $archiveName = "LocalBuild-$Version-$(Get-GitCommit)"
        }
        if ($packageName) {
            $archiveName = "$packageName-$archiveName"
        }
    } else {
        $archiveName = $ArchiveName
    }

    if (![System.IO.Directory]::Exists($packageOutput)) {
        throw "Not a directory: $packageOutput"
    }

    Create-ZipArchive "$packageOutput\$archiveName.zip" "$packageOutput\*"
}

function Create-ZipArchive
{
param(
    [string]$archiveFile,
    [string]$include,
    [string]$exclude,
    [string]$excludeFilelist
)

    if ($exclude) {
        $exclude = "-x!$exclude"
    }

    if ($excludeFilelist) {
        $excludeFilelist = "-xr@$excludeFilelist"
    }

    Invoke-7Zip @("a", "-tzip", $archiveFile, $include, $exclude, $excludeFilelist)
}

function Invoke-7Zip
{
param(
    [string[]]$cmdArgs
)

    if (!$7za) {
        if (Test-Path "$here\7za.exe") {
            $7za = "$here\7za.exe"
        } elseif (Test-Path "$here\..\7za.exe") {
            $7za = "$here\..\7za.exe"
        } elseif (Test-Path "$here\build\7za.exe") {
            $7za = "$here\build\7za.exe"
        } elseif (Test-Path "C:\Program Files\7-Zip\7z.exe") {
            $7za = "C:\Program Files\7-Zip\7z.exe"
        } elseif (Test-Path "C:\Program Files (x86)\7-Zip\7z.exe") {
            $7za = "C:\Program Files (x86)\7-Zip\7z.exe"
        }
    }

    Exec { & $7za $cmdArgs }
}

function Generate-TransformXmlPropertiesFile {
param(
    [hashtable]$Parameters,
    [string]$ParametersFile
)

    $xml = [xml]"<parameters><param name='foo' value='bar' /></parameters>"
    $Parameters.Keys | % {
        $param = $xml.CreateElement("param")
        $param.SetAttribute("name", $_)
        $param.SetAttribute("value", $Parameters[$_])
        $v = $xml.parameters.AppendChild($param)
    }
    $xml.Save($ParametersFile)
}

function TransformXml {
param(
    [string]$Source,
    [string]$Transform,
    [string]$Destination,
    [string]$Target,
    [hashtable]$Parameters,
    [string]$ParametersFile,
    [string]$Encoding = "UTF-8",
    [switch]$Verbose
)

    if (!$Source -and !$Destination -and $Target) {
        $Source = $Target
        $Destination = $Target
    }

    if ($ParametersFile) {
        if ($Parameters) {
            Generate-TransformXmlPropertiesFile $Parameters $ParametersFile
        }
        $params = "pf:"+'"'+$ParametersFile+'"'
    } elseif ($Parameters -and $Parameters.Count -gt 0) {
        $params = "p:"
        $Parameters.Keys | % {
            $params += [String]::Format('{0}:"{1}"', $_, $Parameters[$_])
        }
    }

    if ($Encoding) {
        $Encoding = "encoding:$Encoding"
    }

    $verbosity = if ($Verbose) { "verbose" } else { "" }  

    Write-Host "Transforming $Source -> $Destination"
    Write-Host "  Source file.....: $Source"
    Write-Host "  Transform file..: $Transform"
    Write-Host "  Destination file: $Destination"

    if (!(Test-Path $Source)) {
        throw "Source file does not exists: $Source"
    }
    if (!(Test-Path $Transform)) {
        throw "Transform file does not exists: $Transform"
    }

    Exec {
        .\ctt.exe `
            source:"$Source" `
            transform:"$Transform" `
            destination:"$Destination" `
            $Encoding `
            $params `
            $verbosity `
            preservewhitespace `
            indent
    }
}

function Invoke-Gradle {
param(
    [string]$GradleWrapper,
    [string]$Task = "build",
    [switch]$IgnoreStdErr = $false
)
    if ([System.IO.Directory]::Exists($GradleWrapper)) {
        $GradleDirectory = $GradleWrapper
        $GradleWrapper = [System.IO.Path]::Combine($GradleWrapper, "gradlew.bat")
    } else {
        $GradleDirectory = [System.IO.Path]::GetDirectoryName($GradleWrapper)
    }

    if (![System.IO.File]::Exists($GradleWrapper)) {
        throw "Cannot find file or part of path: $GradleWrapper"
    }

    function CheckEnvironmentVariableOrAskUser([string]$VariableName) {
        $value = [Environment]::GetEnvironmentVariable($VariableName)
        if (!$value) {
            # If running locally, we can ask the user for input
            if ($BuildName -eq $null) {
                Write-Host "You have not configured the $VariableName environment variable." -ForegroundColor red
                Write-Host "Please supply a value, or use CTRL+C to abort the build."

                $directoryExists = $false
                do {
                    $value = Read-Host "$VariableName value"

                    if ([System.IO.Directory]::Exists($value)) {
                        $directoryExists = $true
                    } else {
                        Write-Host "Path or directory does not exists: $value" -ForegroundColor red
                    }

                } while (!$directoryExists)

                Write-Host "Sets environment variable $VariableName (this may take a while) ... " -nonewline
                [Environment]::SetEnvironmentVariable($VariableName, $value, "Process") # Save to process so the value is passed to the next process
                [Environment]::SetEnvironmentVariable($VariableName, $value, "User") # Save to User so that we don't have to enter it again...
                Write-Host "OK" -ForegroundColor Green

            } else {
                Write-Host "WARNING: $VariableName environment variable is not configured." -Foreground Yellow
            }
        }
    }

    CheckEnvironmentVariableOrAskUser "ANDROID_HOME"
    CheckEnvironmentVariableOrAskUser "JAVA_HOME"

    $StdErrToStdOut = if ($IgnoreStdErr) { "2>&1" } else { "" }

    $originalLocation = get-location
    try {
        Write-Host "Entering directory: $GradleDirectory"
        set-location $GradleDirectory
        Exec { & cmd /c "$GradleWrapper $Task $StdErrToStdOut" }
    } finally {
        set-location $originalLocation
    }
}

Function CreateNuspecXml()
{
param(
    [string]$packageId = "",
    [string]$version = "",
    [string]$title = "",
    [string]$description = "",
    [string]$files = "",
    [string]$dependenciesNode = ""
)

    $nuspec = [xml]@"
<?xml version="1.0"?>
<package >
  <metadata>
    <id>TO BE FILLED OUT LATER</id>
    <version>`$version$`</version>
    <title>`$title$`</title>
    <authors>PocketMobile Communications AB</authors>
    <owners>PocketMobile Communications AB</owners>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>`$description$`</description>
    <copyright></copyright>
    $dependenciesNode
  </metadata>
  $files
</package>
"@

    $metadata = $nuspec.package.metadata

    if (![System.String]::IsNullOrEmpty($packageId)) {
        $metadata.id = $packageId
    }
    if (![System.String]::IsNullOrEmpty($version)) {
        $metadata.version = $version
    }
    if (![System.String]::IsNullOrEmpty($title)) {
        $metadata.title = $title
    }
    if (![System.String]::IsNullOrEmpty($description)) {
        $metadata.description = $description
    }

    $metadata.copyright = "Copyright (c) PocketMobile Communications $((get-date).ToString('yyyy'))"

    return $nuspec
}

Function CreateNuGetFromProject()
{
<#  
.EXAMPLE
     CreateNuGetFromProject `
        -nugetExe nuget.exe `
        -projectFileFullPath "$here\Server\SyncServer\SyncServer\Synchronization.Server.csproj" `
        -configuration $Configuration `
        -outputPath "$here\Server\SyncServer\bin\$Configuration\" `
        -packageId "PreCom.Synchronization.Server" `
        -dependencies @(@{id = "PreCom.Server"; version = "3.8"})
#>
param(
    [string]$nugetExe,
    [string]$projectFileFullPath,
    [string]$configuration,
    [string]$outputPath,
    [string]$packageId,
    [string]$description = "",
    [string]$dependenciesNode = ""
)

    $nuspec = CreateNuspecXml `
        -packageId $packageId `
        -description $description `
        -files "<files></files>" `
        -dependenciesNode $dependenciesNode

    $nuspecFile = $projectFileFullPath.Replace("csproj", "nuspec")
    $nuspec.Save($nuspecFile)

    Exec { & $nugetExe pack $projectFileFullPath -OutputDirectory $outputPath  -Properties "Platform=AnyCPU;Configuration=$configuration" -IncludeReferencedProjects }
}

Function CreateNuGetFromFiles()
{
<#
.EXAMPLE
    CreateNuGetFromFiles `
         -nugetExe $nuget `
         -projectOutputPath "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\" `
         -files @{`
            "net35-cf" = @("PreCom.ISynchronization.Client.dll", "PreCom.ISynchronization.Client.xml", "PreCom.Synchronization.Client.dll", "PreCom.Synchronization.Client.XML"); `
            "net45" = @("PreCom.ISynchronization.Client.dll", "PreCom.ISynchronization.Client.xml", "PreCom.Synchronization.Client.dll", "PreCom.Synchronization.Client.XML")} `
         -packageId "PreCom.Synchronization.Client" `
         -version $Version `
         -title "Synchronization Client Module" `
         -description "Synchronization Client Module" `
         -dependenciesNode @"
<dependencies>
    <group targetFramework=".NETFramework3.5-CompactFramework">
        <dependency id="PreCom.Mobile" version="3.8" />
    </group>
    <group targetFramework=".NETFramework4.5">
        <dependency id="PreCom.PC" version="3.8" />
    </group>
</dependencies>
"@
#>
param(
    [string]$nugetExe,
    [string]$projectOutputPath,
    [hashtable]$files,
    [string]$packageId,
    [string]$version,
    [string]$title,
    [string]$description,
    [string]$dependenciesNode = ""
)
    $nuspec = CreateNuspecXml `
        -packageId $packageId `
        -version $version `
        -title $title `
        -description $description `
        -dependenciesNode $dependenciesNode
    
    $nuspecFile = [System.IO.Path]::Combine($projectOutputPath, "$packageId.nuspec")
    $nuspec.Save($nuspecFile)

    $inputpath = [System.IO.Path]::Combine($projectOutputPath, "nuget")
    $libPath = [System.IO.Path]::Combine($inputpath, "lib")
    
    
    if (Test-Path $inputpath) {
        Remove-Item -Path $inputpath -Recurse
    }

    $files.GetEnumerator() | % {
        $frameworkPath = [System.IO.Path]::Combine($libPath, $($_.key))
        New-Item $frameworkPath -type Directory
        foreach ($file in $($_.value)) {
            $fullPathToFile = [System.IO.Path]::Combine($projectOutputPath, $file)
            Copy-Item -Path $fullPathToFile -Destination $frameworkPath
        }

    }

    Exec { & $nugetExe pack $nuspecFile -OutputDirectory $projectOutputPath -BasePath $inputpath }

    Remove-Item -Path $inputpath -Recurse
    Remove-Item -Path $nuspecFile
}

function GenerateAndRunSonar
{
param(
    [string]$projectKey = $(throw "projectKey is a required parameter."),
    [string]$projectName = $(throw "projectName is a required parameter."),
    [string]$projectVersion = $(throw "projectVersion is a required parameter."),
	$language = $(throw "language is a required parameter."),
    $solutionFile,
    $projectNameInSolution,
	[string]$assembly,
    [string]$folder = $(throw "folder is a required parameter."),
	[string]$fxCopDependenciesDirectories,
    [System.Collections.Hashtable]$modules,
    $folderNamesForProject
)
	# Note that path separators shall be / here.
	# fxCopDependenciesDirectories is a comma separed list of directories.

    $originalLocation = get-location
    try {
        Write-Host "Entering directory: $folder"
        set-location $folder
		Generate-SonarProjectProperties -projectKey $projectKey -projectName $projectName -projectVersion $projectVersion -language $language -solutionFile $solutionFile -projectNameInSolution $projectNameInSolution -assembly $assembly -folder $folder -fxCopDependenciesDirectories $fxCopDependenciesDirectories -modules $modules -folderNamesForProject $folderNamesForProject
        Exec { & sonar-runner -e}
    } finally {
        set-location $originalLocation
    }
}


function Generate-SonarProjectProperties
{
param(
    [string]$projectKey = $(throw "projectKey is a required parameter."),
    [string]$projectName = $(throw "projectName is a required parameter."),
    [string]$projectVersion = $(throw "projectVersion is a required parameter."),
	$language = $(throw "language is a required parameter."),
    $solutionFile,
    $projectNameInSolution,
	[string]$assembly,
    [string]$folder = $(throw "folder is a required parameter."),
	[string]$fxCopDependenciesDirectories,
    [System.Collections.Hashtable]$modules,
    $folderNamesForProject
)
 
    $projectPropertiesInfoGeneric = "#
 # This file was generated by psake_ext.ps1 on $((get-date).ToString('o'))
 #
 # Any modifications to this file will be overwritten by build tools when updating version numbers.
 #
 # IF YOU NEED TO EDIT THESE VALUES, PLEASE SEE THE PSAKE BUILD FILE 'default.ps1'
 #

 # Required metadata
sonar.projectKey=$projectKey
sonar.projectName=$projectName
sonar.projectVersion=$projectVersion
 
# Path to the parent source code directory.

 
# Encoding of the source code
sonar.sourceEncoding=UTF-8
"

	$projectPropertiesInfoJava = " 
    

"

	$projectPropertiesInfoCS = " 

"

	$file = $folder + "sonar-project.properties"
    Write-Host "Generating sonar-project.properties file: $file"
	$projectPropertiesCombined = $projectPropertiesInfoGeneric

    if ($projectNameInSolution -is [System.Array]) {
        if ($projectNameInSolution.Length -ne $language.Length) {
            throw "All the project names provided should have a language"
        }
        
        if ($projectNameInSolution.Length -ne $solutionFile.Length) {
            throw "All projects must have a solution file or source code directory"
        }

        if ($projectNameInSolution.Length -ne $folderNamesForProject.Length) {
            throw "All projects must have a project folder name"
        }

        $projectPropertiesCombined += "sonar.modules="
        for ($i=0; $i -lt $projectNameInSolution.Length; $i++) {
            $projectPropertiesCombined += $projectNameInSolution[$i] + ","
        }

        $projectPropertiesCombined = $projectPropertiesCombined.Substring(0, $projectPropertiesCombined.Length-1)


        for ($i=0; $i -lt $projectNameInSolution.Length; $i++) {
	        $project = $projectNameInSolution[$i]
            $projectLanguage = $language[$i]
            $projectSolutionFile = $solutionFile[$i]
            $projectFolder = $folderNamesForProject[$i]
            
            if ($projectLanguage -eq "cs") {
                $projectPropertiesInfoCS += $project + ".sonar.projectBaseDir=" + $folder + $projectFolder
                $projectPropertiesInfoCS += " `r`n"
                if ($modules) {
                    $projectPropertiesInfoCS += Get-sonarModules -modules $modules.Item($project) -projectName $project
                    $projectPropertiesInfoCS += " `r`n"
                    $projectPropertiesInfoCS += Get-sonarAssemblies -modules $modules.Item($project) -projectName $project
                }
                $projectPropertiesInfoCS += " `r`n"
                $projectPropertiesInfoCS += $project + ".sonar.resharper.projectName=$project"
                $projectPropertiesInfoCS += " `r`n"
                $projectPropertiesInfoCS += $project + ".sonar.resharper.solutionFile=$projectSolutionFile"
                $projectPropertiesInfoCS += " `r`n"
                $projectPropertiesInfoCS += $project + ".sonar.sources=."
                $projectPropertiesInfoCS += " `r`n"
		        $projectPropertiesCombined += $projectPropertiesInfoCS
                $projectPropertiesInfoCS += " `r`n"
                
	        } elseif ($projectLanguage -eq "java") {
                $projectPropertiesInfoJava += $project + ".sonar.projectBaseDir=$projectSolutionFile"
                $projectPropertiesInfoJava += " `r`n"
                #$projectPropertiesInfoJava += $project + ".project.home=$projectSolutionFile"
                #$projectPropertiesInfoJava += " `r`n"
                if ($modules -and ($modules.Item($project) -is [System.Collections.Hashtable])) {
                    $projectPropertiesInfoJava += Get-sonarModules -modules $modules.Item($project) -projectName $project
                }
                $projectPropertiesInfoJava += " `r`n"
                $projectPropertiesInfoJava += $project + ".sonar.language=java"
                $projectPropertiesInfoJava += " `r`n"
                
                $projectPropertiesInfoJava += $project + ".sonar.sources=."
                $projectPropertiesInfoJava += " `r`n"
        
		        $projectPropertiesCombined += $projectPropertiesInfoJava
	        }
        }
    
    } elseif ($language -eq "cs") {
		#$projectPropertiesInfoCS += "sonar.projectBaseDir=" + $folder
		#$projectPropertiesInfoCS += " `r`n"
        $projectPropertiesInfoCS += "sonar.resharper.projectName=$projectNameInSolution"
        $projectPropertiesInfoCS += " `r`n"
        $projectPropertiesInfoCS += "sonar.resharper.solutionFile=$solutionFile"
        $projectPropertiesInfoCS += " `r`n"
        $projectPropertiesInfoCS += "sonar.sources=."
        $projectPropertiesInfoCS += " `r`n"
        if ($modules) {
            $projectPropertiesInfoCS += Get-sonarModules -modules $modules
            $projectPropertiesInfoCS += " `r`n"
            $projectPropertiesInfoCS += Get-sonarAssemblies -modules $modules
        }
        else {
            $projectPropertiesInfoCS += "sonar.cs.fxcop.assembly=$assembly"
            $projectPropertiesInfoCS += " `r`n"
            $projectPropertiesInfoCS += "sonar.fxcop.assemblyDependencyDirectories=$fxCopDependenciesDirectories"
            $projectPropertiesInfoCS += " `r`n"
            
        }
        
		$projectPropertiesCombined = $projectPropertiesCombined + $projectPropertiesInfoCS
	} elseif ($language -eq "java") {
        $projectPropertiesInfoJava += "sonar.language=java"
        $projectPropertiesInfoJava += " `r`n"
        $projectPropertiesInfoJava += "project.home=$folder"
        $projectPropertiesInfoJava += " `r`n"
        $projectPropertiesInfoJava += "sonar.sources=."
		$projectPropertiesInfoJava += " `r`n"
        if ($modules) {
            $projectPropertiesInfoJava += Get-sonarModules -modules $modules
        }
        
		$projectPropertiesCombined = $projectPropertiesCombined + $projectPropertiesInfoJava
	}
    Set-Content $file $projectPropertiesCombined -Encoding String
}

function Get-sonarModules {
param (
    [System.Collections.Hashtable]$modules = $(throw "modules is a required parameter"),
    $projectName
)
    if ($projectName){
        $sonarModules = $projectName + ".sonar.modules="
    } else {
        $sonarModules = "sonar.modules="
    }
    
    $modules.Keys | % {
        if ($_ -notmatch "nomodule") {
            $sonarModules += $_ + ","
        }
        
    }

    $sonarModules = $sonarModules.Substring(0, $sonarModules.Length-1)

    $modules.Keys | % {
        if ($modules.Item($_) -is [System.Collections.Hashtable]) {
            $module = $_
            Write-Host "Adding sub modules for $module"

            if ($projectName) {
                $sonarSubModules = $projectName + "." + $module + ".sonar.modules="
            } else {
                $sonarSubModules = $module + ".sonar.modules="
            }
            
            $subModules = $modules.Item($module)
            $subModules.Keys | % {
                if ($_ -notmatch "nomodule") {
                    $sonarSubModules += $_ + ","
                }
            }
            $sonarSubModules = $sonarSubModules.Substring(0, $sonarSubModules.Length-1)
            $sonarModules += " `r`n"
            $sonarModules += $sonarSubModules
        }
    }

    return $sonarModules
}

function Get-sonarAssemblies {
param(
    [System.Collections.Hashtable]$modules = $(throw "modules is a required parameter"),
    $projectName
)
    $assemblies = ""
    $modules.Keys | % {
        if ($modules.Item($_) -is [System.Collections.Hashtable]) {
            $module = $_
            $subModules = $modules.Item($module)
            $subModules.Keys | % {
                if ($projectName) {
                    if ($_ -match "nomodule") {
                        $assemblies += $projectName + "." + $module + "." + "sonar.cs.fxcop.assembly=" +  $subModules.Item($_) + " `r`n"
                    } else {
                        $assemblies += $projectName + "." + $module + "." + $_ + "." + "sonar.cs.fxcop.assembly=" +  $subModules.Item($_) + " `r`n"
                    }
                    
                } else {
                    if ($_ -match "nomodule") {
                        $assemblies += $module + "." + "sonar.cs.fxcop.assembly=" +  $subModules.Item($_) + " `r`n"
                    } else {
                        $assemblies += $module + "." + $_ + "." + "sonar.cs.fxcop.assembly=" +  $subModules.Item($_) + " `r`n"
                    }
                    
                }
                
            }
            
        } else {
            if ($projectName) {
                if ($_ -match "nomodule") {
                    $assemblies += $projectName + "." + "sonar.cs.fxcop.assembly=" +  $modules.Item($_) + " `r`n"
                } else {
                    $assemblies += $projectName + "." + $_ + "." + "sonar.cs.fxcop.assembly=" +  $modules.Item($_) + " `r`n"
                }
                
            } else {
                if ($_ -match "nomodule") {
                    $assemblies += "sonar.cs.fxcop.assembly=" +  $modules.Item($_) + " `r`n"
                } else {
                    $assemblies += $_ + "." + "sonar.cs.fxcop.assembly=" +  $modules.Item($_) + " `r`n"
                }
                
            } 
            
        }
    }

    return $assemblies
}
