﻿properties {
    $here = "$(Get-Location)\.."

    $PackageName = "PreCom Synchronization"

    $Configuration = "Release"
    $BuildName = Get-BuildNameFromCISystem
    $BuildNumber = Get-BuildNumberFromCISystem
    $Version = Get-Version


    # NuGet.exe is by default included in this Build folder, if you have enabled NuGet Package
    # Restore in your Visual Studio projects, you may already have checked in a .nuget folder
    # and the NuGet.exe file. You can change the path to NuGet.exe by changing this variable.
    $nuget = "$here\.nuget\nuget.exe"
    
    $NuGetOutput = "$here\packages"
    $PackageOutput = "$here\pkg"
    $MSTestResults = "$PackageOutput\TestResults\MSTest"
    $OpenCoverReports = "$PackageOutput\TestResults\OpenCover"
    $CodeCoverageReport = "$PackageOutput\TestResults\CodeCoverageReport"
	$PreComFrameworkVersion = "3.8"

	if ($BuildName -and $BuildNumber) {
		Write-Host "Considered to be build server environment since BuildName = $BuildName and BuildNumber = $BuildNumber" 
		$OnBuildServer = $true
	 }
	 else {
		Write-Host "Considered to be local build environment since BuildName = $BuildName and BuildNumber = $BuildNumber" 
		$OnBuildServer = $false
	 }

    if ($OnBuildServer -eq $true) {
        $Version = Add-BuildNumberToVersion $Version $BuildNumber
    }

    # Uncomment the following line to inject the build number in the assembly/file versions.
    ##$Version = Get-ExpandedVersion $Version -ReplaceBuild $BuildNumber

    $InformationalVersion = Get-InformationalVersion $Version -NoGitDescribe -NoVersionFromFile
	
	$branchesForSonar = "PC-SYNC-JOB1"
}

Framework "4.0x86"

. .\psake_ext.ps1

task default -depends PrintMetadata,Init,Compile,Test,Obfuscate,SignAssemblies,CreateNuGetPackage,Package,Reset-AssemblyInfo
task Init -depends Clean,Generate-AssemblyInfo,RestoreDependecies


task PrintMetadata {
    echo "InformationalVersion: $InformationalVersion"
    echo "Version:              $Version"
    echo "BuildNumber:          $BuildNumber"
    echo "BuildName:            $BuildName"
    echo "Configuration:        $Configuration"
}

task Clean {
    Delete-DirectoryIfExists $PackageOutput
    Delete-DirectoryIfExists $NuGetOutput
    Delete-DirectoryIfExists "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration"
    Delete-DirectoryIfExists "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration"
    Delete-DirectoryIfExists "$here\Server\SyncServer\bin\$Configuration"
	Delete-DirectoryIfExists "$here\Server\SyncServer\SyncServerTestModule\bin\$Configuration"
	Delete-DirectoryIfExists "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\bin\$Configuration"
	

    Exec { msbuild /p:Configuration=$Configuration /t:Clean "$here\PreCom.Synchronization.VS2012.sln" }
    Exec { msbuild /p:Configuration=$Configuration /t:Clean "$here\PreCom.Synchronization.VS2008.sln" }

    Exec { ant clean -f "$here\Client\Android\sync module\javadoc.xml" }
	#Exec { ant clean -f "$here\Client\Android\Sync Test Module\javadoc.xml" }
}

task RestoreDependecies -depends RestoreNuGetPackages,DownloadZipFilesFromBitBucket

task DownloadZipFilesFromBitBucket {
    Exec {
        .\PsGet.ps1 `
            -Username $env:bamboo_libraryUsername `
            -Password $env:bamboo_libraryPassword `
            -InputFile "$here\references.cfg" `
            -Output "$here\ext"
    }
}

task Generate-AssemblyInfo  {
    # Define some common shared values here...
    $product = "PreCom Synchronization"
    $trademark = ""

    Generate-AssemblyInfo `
        -file "$here\Server\SyncServer\PreCom.Synchronization\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization Server Interface" `
        -version $Version `
        -product "Synchronization Server" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Generate-AssemblyInfo `
        -file "$here\Server\SyncServer\SyncServer\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization Server Module" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Generate-AssemblyInfo `
        -file "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.ISynchronization.Client\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization Client Interface" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Generate-AssemblyInfo `
        -file "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization Client Module" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Generate-AssemblyInfo `
        -file "$here\Client\LT\PreCom.ISynchronization.LT\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization LT Interface" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Generate-AssemblyInfo `
        -file "$here\Client\LT\PreCom.Synchronization.LT\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization LT" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion
		
	Generate-AssemblyInfo `
        -file "$here\Server\SyncServer\SyncServerTestModule\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization SyncServerTestModule" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion
		
	Generate-AssemblyInfo `
        -file "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\Properties\AssemblyInfo.cs" `
        -description "" `
        -title "Synchronization SyncClientTestModule" `
        -version $Version `
        -product "PreCom Synchronization" `
        -trademark $trademark `
        -informationalVersion $InformationalVersion

    Update-BuildGradleFile `
        -file "$here\Client\Android\build.gradle" `
        -version $Version

    Update-AndroidManifest `
        -file "$here\Client\Android\sync module\AndroidManifest.xml" `
        -version $Version
		
	Update-AndroidManifest `
        -file "$here\Client\Android\Sync Test Module\AndroidManifest.xml" `
        -version $Version

    Update-JavaReleaseInformation `
        -file "$here\Client\Android\sync module\src\se\precom\synchronization\ReleaseInformation.java" `
        -version $Version `
        -packageName "se.precom.synchronization"
	
	Update-JavaReleaseInformation `
        -file "$here\Client\Android\Sync Test Module\src\se\precom\synctestmodule\ReleaseInformation.java" `
        -version $Version `
        -packageName "se.precom.synctestmodule"
}

task Reset-AssemblyInfo -precondition { $OnBuildServer -eq $false } {
    $(Get-ChildItem $here -Recurse -Filter AssemblyInfo.cs | where {$_.FullName -notmatch "precom-load-test"} | foreach { 
        Invoke-Git @("checkout", "HEAD", "--", $_.FullName)
    })

    $(Get-ChildItem $here -Recurse -Filter ReleaseInformation.java| foreach { 
        Invoke-Git @("checkout", "HEAD", "--", $_.FullName)
    })

    Invoke-Git @("checkout", "HEAD", "--", "$here\Client\Android\build.gradle")
    Invoke-Git @("checkout", "HEAD", "--", "$here\Client\Android\sync module\AndroidManifest.xml")
	Invoke-Git @("checkout", "HEAD", "--", "$here\Client\Android\Sync Test Module\AndroidManifest.xml")
}

task RestoreNuGetPackages {
    NuGetHackToSetCredentials
    Exec { & $nuget install .\packages.config -o $NuGetOutput }
}

task Compile -depends Init {
    Exec { msbuild /p:Configuration=$Configuration /t:Build "$here\PreCom.Synchronization.VS2012.sln" }
    Exec { msbuild /p:Configuration=$Configuration /t:Build "$here\PreCom.Synchronization.VS2008.sln" }

    Invoke-Gradle "$here\Client\Android" "clean build uploadArchives" -IgnoreStdErr $true
    Exec { ant jarjavadoc -f "$here\Client\Android\sync module\javadoc.xml" } 
	#Exec { ant jarjavadoc -f "$here\Client\Android\Sync Test Module\javadoc.xml" } 
	Create-Proguard-Folder
    Create-ZipArchive -archiveFile "$here\Client\Android\sync module\build\proguard.zip" -include "$here\Client\Android\sync module\build\proguard\*"
	Create-ZipArchive -archiveFile "$here\Client\Android\Sync Test Module\build\proguard.zip" -include "$here\Client\Android\Sync Test Module\build\proguard\*"
}

function Create-Proguard-Folder
{
	Create-DirectoryIfNotExists "$here\Client\Android\sync module\build\proguard"
    Copy-Item "$here\Client\Android\sync module\build\outputs\mapping\release\dump.txt" "$here\Client\Android\sync module\build\proguard"
	Copy-Item "$here\Client\Android\sync module\build\outputs\mapping\release\seeds.txt" "$here\Client\Android\sync module\build\proguard"
	Copy-Item "$here\Client\Android\sync module\build\intermediates\proguard-rules\release\aapt_rules.txt" "$here\Client\Android\sync module\build\proguard"
    Copy-Item "$here\Client\Android\sync module\proguard\sync_mapping.txt" "$here\Client\Android\sync module\build\proguard"
	
	Create-DirectoryIfNotExists "$here\Client\Android\Sync Test Module\build\proguard"
    Copy-Item "$here\Client\Android\Sync Test Module\build\outputs\mapping\release\dump.txt" "$here\Client\Android\Sync Test Module\build\proguard"
	Copy-Item "$here\Client\Android\Sync Test Module\build\outputs\mapping\release\seeds.txt" "$here\Client\Android\Sync Test Module\build\proguard"
	Copy-Item "$here\Client\Android\Sync Test Module\build\intermediates\proguard-rules\release\aapt_rules.txt" "$here\Client\Android\Sync Test Module\build\proguard"
    Copy-Item "$here\Client\Android\Sync Test Module\proguard\sync_mapping.txt" "$here\Client\Android\Sync Test Module\build\proguard"
}

task Test -depends RunFastTests,RunSlowTests,MergeTestResults,Generate-OpenCoverReport,RunSonar

task RunFastTests {
    # Only one test so copy directly to TestResults.trx and don't merge
    #OpenCover-MSTest -TestResults "$PackageOutput\TestResults\TestResults.trx" -Output $OpenCoverReports -TestContainer "$here\Server\SyncServer\SyncServerTest\bin\Release\SyncServerTest.dll"

    Invoke-NUnit `
        -OpenCover `
        "$here\Server\SyncServer\SyncServerTest\bin\Release\SyncServerTest.dll" `
        -TestResults "$here\pkg\TestResults\NUnit" `
        -OpenCoverOutput "$here\pkg\TestResults\OpenCover" `
}

# TODO: Change the match criteria to the plan of the nightly build
# The match criteria is a regex pattern, and should match the build key (PROJ-PLAN-JOB).
task RunSlowTests -precondition { $BuildName -match "^TODO-CHANGEME-.*" } {
    echo "No slow tests are defined. Edit the default.ps1 file and task RunSlowTests to configure slow test runs."
    echo "This task should only be executed in the nightly build."
}

task Generate-OpenCoverReport {
    OpenCover-GenerateReport -Reports $OpenCoverReports -Output $CodeCoverageReport
}

task MergeTestResults {
    #Exec-TRXMerge -Include $MSTestResults -Exclude "VS2008Tests.trx" -Output "$PackageOutput\TestResults\TestResults.trx"
}

task Obfuscate -precondition { $OnBuildServer }  {	
    Dotfuscator "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll"	
    Dotfuscator "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll"	
    Dotfuscator "$here\Server\SyncServer\bin\$Configuration\PreCom.ISynchronization.Server.dll"
    Dotfuscator "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.dll"
    Dotfuscator "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.ISynchronization.LT.dll"
    Dotfuscator "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.dll"
	Dotfuscator "$here\Server\SyncServer\SyncServerTestModule\bin\$Configuration\SyncServerTestModule.dll"
	Dotfuscator "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\bin\$Configuration\SyncClientTestModule.dll"
}


task SignAssemblies -precondition { $OnBuildServer } {
	# Copy unsigned version for WM since Signing SHA2 is not supported
	Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll" "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll.unsigned"
    SignAssembly "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll"
	# Copy unsigned version for WM since Signing SHA2 is not supported
	Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll" "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll.unsigned"
    SignAssembly "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll"
    SignAssembly "$here\Server\SyncServer\bin\$Configuration\PreCom.ISynchronization.Server.dll"
    SignAssembly "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.dll"
    SignAssembly "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.ISynchronization.LT.dll"
    SignAssembly "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.dll"
	SignAssembly "$here\Server\SyncServer\SyncServerTestModule\bin\$Configuration\SyncServerTestModule.dll"
	SignAssembly "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\bin\$Configuration\SyncClientTestModule.dll"
}

task Package -depends CopyOutput,CreateReleaseZips,CreateZipPackage

task CopyOutput {
    Create-DirectoryIfNotExists "$PackageOutput\Client"
    Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll" "$PackageOutput\Client"
    Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll" "$PackageOutput\Client"
    Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.xml" "$PackageOutput\Client"
    Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.XML" "$PackageOutput\Client"
    Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.*.nupkg" "$PackageOutput\Client"
    Copy-ItemIfExists "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dotfuscationmap.xml" "$PackageOutput\Client"
    Copy-ItemIfExists "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dotfuscationmap.xml" "$PackageOutput\Client"

	# Copy unsigned version for WM since Signing SHA2 is not supported
	Create-DirectoryIfNotExists "$PackageOutput\Client\Unsigned"
	Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.ISynchronization.Client.dll.unsigned" "$PackageOutput\Client\Unsigned\PreCom.ISynchronization.Client.dll"
	Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\PreCom.Synchronization.Client.dll.unsigned" "$PackageOutput\Client\Unsigned\PreCom.Synchronization.Client.dll"
	
    Create-DirectoryIfNotExists "$PackageOutput\Server"
    Copy-Item "$here\Server\SyncServer\bin\$Configuration\PreCom.ISynchronization.Server.dll" "$PackageOutput\Server"
    Copy-Item "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.dll" "$PackageOutput\Server"
    Copy-Item "$here\Server\SyncServer\bin\$Configuration\PreCom.ISynchronization.Server.XML" "$PackageOutput\Server"
    Copy-Item "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.XML" "$PackageOutput\Server"
    Copy-Item "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.*.nupkg" "$PackageOutput\Server"
	Copy-Item "$here\Server\SyncServer\SyncServerTest\bin\$Configuration\SyncServerTest.dll" "$PackageOutput\Server"
    Copy-ItemIfExists "$here\Server\SyncServer\bin\$Configuration\PreCom.ISynchronization.Server.dotfuscationmap.xml" "$PackageOutput\Server"
    Copy-ItemIfExists "$here\Server\SyncServer\bin\$Configuration\PreCom.Synchronization.Server.dotfuscationmap.xml" "$PackageOutput\Server"

	#Since the SyncServerAutomaticTestModule is included in server solution , we have to copy the client dll to client folder from the server folder
	Copy-Item "$here\Server\SyncServer\SyncServerAutomaticTestModule\bin\$Configuration\SyncServerAutomaticTestModule.dll" "$PackageOutput\Client"
	
	Create-DirectoryIfNotExists "$PackageOutput\TestModule\Server"
	Copy-Item "$here\Server\SyncServer\SyncServerTestModule\bin\$Configuration\SyncServerTestModule.dll" "$PackageOutput\TestModule\Server"	
	Copy-ItemIfExists "$here\Server\SyncServer\SyncServerTestModule\bin\$Configuration\SyncServerTestModule.dotfuscationmap.xml" "$PackageOutput\TestModule\Server"
	
	Create-DirectoryIfNotExists "$PackageOutput\TestModule\Client"
	Copy-Item "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\bin\$Configuration\SyncClientTestModule.dll" "$PackageOutput\TestModule\Client"	
	Copy-ItemIfExists "$here\Client\WMandPC\PreCom.Synchronization.Client\SyncClientTestModule\bin\$Configuration\SyncClientTestModule.dotfuscationmap.xml" "$PackageOutput\TestModule\Client"

    Create-DirectoryIfNotExists "$PackageOutput\LT"
    Copy-Item "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.ISynchronization.LT.dll" "$PackageOutput\LT"
    Copy-Item "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.dll" "$PackageOutput\LT"
    Copy-Item "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.ISynchronization.LT.XML" "$PackageOutput\LT"
    Copy-Item "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.XML" "$PackageOutput\LT"
    Copy-Item "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.*.nupkg" "$PackageOutput\LT"
    Copy-ItemIfExists "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.ISynchronization.LT.dotfuscationmap.xml" "$PackageOutput\LT"
    Copy-ItemIfExists "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\PreCom.Synchronization.LT.dotfuscationmap.xml" "$PackageOutput\LT"

    Create-DirectoryIfNotExists "$PackageOutput\Android"
    Invoke-7Zip @("e", "-y", "-o$PackageOutput\Android", "$PackageOutput\Android\maven\se\precom\precom-synchronization-android\$Version\precom-synchronization-android-$Version.aar", "classes.jar")
    Delete-ItemIfExists "$PackageOutput\Android\precom-synchronization-android-$Version.jar"
    Rename-Item "$PackageOutput\Android\classes.jar" "precom-synchronization-android-$Version.jar"
    Set-Content -Path "$PackageOutput\Android\precom-synchronization-android-$Version.jar.properties" -Value "doc=../doc/precom-synchronization-android-$Version-javadoc/"
    Create-DirectoryIfNotExists "$PackageOutput\Android\doc\precom-synchronization-android-$Version-javadoc"
	copy-item "$here\client\android\sync module\doc\*" "$packageoutput\android\doc\precom-synchronization-android-$version-javadoc"
    copy-item "$here\client\android\sync module\javadoc.jar" "$packageoutput\android\doc\precom-synchronization-android-$version-javadoc.jar"
    Copy-Item "$here\Client\Android\sync module\build\proguard.zip" "$PackageOutput\Android"
}

task CreateReleaseZips {
    Create-DirectoryIfNotExists "$PackageOutput\Zip"
    Create-ZipArchive -archiveFile "$PackageOutput\Zip\PreCom-Synchronization-Client-$Version.zip" -include "$PackageOutput\Client\*.dll" -exclude "SyncServerAutomaticTestModule.dll"
    Create-ZipArchive -archiveFile "$PackageOutput\Zip\PreCom-Synchronization-Client-$Version.zip" -include "$PackageOutput\Client\*.xml" -exclude "*dotfuscationmap*"
    Create-ZipArchive -archiveFile "$PackageOutput\Zip\PreCom-Synchronization-Server-$Version.zip" -include "$PackageOutput\Server\*.dll"
    Create-ZipArchive -archiveFile "$PackageOutput\Zip\PreCom-Synchronization-Server-$Version.zip" -include "$PackageOutput\Server\*.xml" -exclude "*dotfuscationmap*"
    Create-ZipArchive -archiveFile "$PackageOutput\Zip\PreCom-Synchronization-Android-$Version.zip" -include "$PackageOutput\Android\*" -exclude "proguard.zip"
}

task CreateZipPackage {
    Compress-PackageOutput -PackageName $PackageName -Version $Version
}

task CreateNuGetPackage {
    
    CreateNuGetFromProject `
        -nugetExe $nuget `
        -projectFileFullPath "$here\Server\SyncServer\SyncServer\Synchronization.Server.csproj" `
        -configuration $Configuration `
        -outputPath "$here\Server\SyncServer\bin\$Configuration\" `
        -packageId "PreCom.Synchronization.Server" `
        -description "-" `
        -dependenciesNode @"
<dependencies>
    <dependency id="PreCom.Server" version="3.11.2" />
</dependencies>
"@

    CreateNuGetFromFiles `
         -nugetExe $nuget `
         -projectOutputPath "$here\Client\WMandPC\PreCom.Synchronization.Client\PreCom.Synchronization.Client\bin\$Configuration\" `
         -files @{`
            "net35-cf" = @("PreCom.ISynchronization.Client.dll", "PreCom.ISynchronization.Client.xml", "PreCom.Synchronization.Client.dll", "PreCom.Synchronization.Client.XML"); `
            "net45" = @("PreCom.ISynchronization.Client.dll", "PreCom.ISynchronization.Client.xml", "PreCom.Synchronization.Client.dll", "PreCom.Synchronization.Client.XML")} `
         -packageId "PreCom.Synchronization.Client" `
         -version $Version `
         -title "Synchronization Client Module" `
         -description "-" `
         -dependenciesNode @"
<dependencies>
    <group targetFramework=".NETFramework3.5-CompactFramework">
        <dependency id="PreCom.Mobile" version="3.9" />
    </group>
    <group targetFramework=".NETFramework4.5">
        <dependency id="PreCom.PC" version="3.9" />
    </group>
</dependencies>
"@

    CreateNuGetFromProject `
        -nugetExe $nuget `
        -projectFileFullPath "$here\Client\LT\PreCom.Synchronization.LT\Synchronization.LT.csproj" `
        -configuration $Configuration `
        -outputPath "$here\Client\LT\PreCom.Synchronization.LT\bin\$Configuration\" `
        -packageId "PreCom.Synchronization.LT" `
        -description "-" `
        -dependenciesNode @"
<dependencies>
    <dependency id="PreCom.LT" version="3.9" />
</dependencies>
"@
}

task RunSonar  -precondition { $branchesForSonar -contains $BuildName }  {
	# Note that path separators shall be / here.
	$hereWithForwardSlash = $here -replace "\\", "/"
    
    
    #Client-LT    
    $clientLtModules = @{}
    $clientLtModules.Add("PreCom.ISynchronization.LT", "Client/LT/PreCom.ISynchronization.LT/bin/$Configuration/" + "PreCom.ISynchronization.LT.dll")
    $clientLtModules.Add("PreCom.Synchronization.LT", "Client/LT/PreCom.Synchronization.LT/bin/$Configuration/" + "PreCom.Synchronization.LT.dll")
    $clientLtModules.Add("PreCom.Synchronization.Test.LT", "Client/LT/PreCom.Synchronization.Test.LT/bin/$Configuration/" + "PreCom.Synchronization.Test.LT.dll")
	
	#Client-WMPC    
    #$clientWMPCModules = @{}
    #$clientWMPCModules.Add("PreCom.ISynchronization.Client", "Client/WMandPC/PreCom.Synchronization.Client/PreCom.ISynchronization.Client/bin/$Configuration/" + "PreCom.ISynchronization.Client.dll")
    #$clientWMPCModules.Add("PreCom.Synchronization.Client", "Client/WMandPC/PreCom.Synchronization.Client/PreCom.Synchronization.Client/bin/$Configuration/" + "PreCom.Synchronization.Client.dll")
    #$clientWMPCModules.Add("PreCom.Synchronization.Client.Test", "Client/WMandPC/PreCom.Synchronization.Client/PreCom.Synchronization.Client.Test/bin/$Configuration/" + "ClientUnitTestProject1.dll")
	#$clientWMPCModules.Add("PreCom.Synchronization.Client.Test.WM", "Client/WMandPC/PreCom.Synchronization.Client/PreCom.Synchronization.Client.Test.WM/bin/$Configuration/" + "SyncClientTestModule.dll")
    #$clientWMPCModules.Add("SyncClientTestModule", "Client/WMandPC/PreCom.Synchronization.Client/SyncClientTestModule/bin/$Configuration/" + "SyncClientTestModule.dll")
    
    #Client-Android
    $androidModules = @{}
    
    #Server
    $serverSubModules = @{}
    $serverSubModules.Add("PreCom.Synchronization", "Server/SyncServer/PreCom.Synchronization/bin/$Configuration/" + "PreCom.ISynchronization.Server.dll")    
    $serverSubModules.Add("SyncServer", "Server/SyncServer/bin/$Configuration/" + "PreCom.Synchronization.Server.dll")
    $serverSubModules.Add("SyncServerAutomaticTestModule", "Server/SyncServer/SyncServerAutomaticTestModule/bin/$Configuration/" + "SyncServerAutomaticTestModule.dll")
    $serverSubModules.Add("SyncServerTestModule", "Server/SyncServer/SyncServerTestModule/bin/$Configuration/" + "SyncServerTestModule.dll")
    $serverModules = @{}
    $serverModules.Add("SyncServer", $serverSubModules)     
    
    $projectNames = @(
       "Synchronization.LT", 
        "Android",
        "Synchronization.Server")
		#"Synchronization.Client")
    
    $projectLanguages = @(
        "cs", 
        "java",
        "cs")
		#"cs"
		
    $folderNamesForProject = @(
        "Client/LT", 
        "Client/Android",
        "Server/SyncServer")
		#"Client/WMandPC/PreCom.Synchronization.Client"
    
    $solutionFiles = @(
        "$hereWithForwardSlash/PreCom.Synchronization.VS2012.sln", 
        "$hereWithForwardSlash/Client/Android",
        "$hereWithForwardSlash/PreCom.Synchronization.VS2012.sln")
		#"$hereWithForwardSlash/PreCom.Synchronization.VS2008.sln"
    
    $modules = @{}
    $modules.Add("Synchronization.LT", $clientLtModules)
    $modules.Add("Android", $androidModules)
    $modules.Add("Synchronization.Server", $serverSubModules)
	#$modules.Add("Synchronization.Client", $clientWMPCModules)
    

	 GenerateAndRunSonar `
	 -projectKey "precom3-sync" `
	 -projectName "PreCom3 Synchronization" `
	 -projectVersion $Version `
	 -language $projectLanguages `
	 -solutionFile $solutionFiles `
	 -projectNameInSolution $projectNames `
	 -folder "$hereWithForwardSlash/" `
     -folderNamesForProject $folderNamesForProject	`
     -modules $modules `

}