﻿namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Represents the synchronization type. 
    /// </summary>
    public enum SyncType
    {
        /// <summary>
        /// Client initiated pull request.
        /// </summary>
        ClientPull,
        /// <summary>
        /// Client initiated push request.
        /// </summary>
        ClientPush,
        /// <summary>
        /// Server initiated push request.
        /// </summary>
        ServerPush
    }
}
