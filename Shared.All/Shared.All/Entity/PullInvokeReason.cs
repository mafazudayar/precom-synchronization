﻿namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Represents the reason for the server push.
    /// </summary>
    public enum PullInvokeReason
    {
        /// <summary>
        /// Server initiates a push due to a client login.
        /// </summary>
        ClientLogin,
        /// <summary>
        /// Server initiates a push due to a automatic push.
        /// </summary>
        AutomaticPush,
        /// <summary>
        /// Server initiates a push due to a manual push.
        /// </summary>
        ManualPush
    }
}
