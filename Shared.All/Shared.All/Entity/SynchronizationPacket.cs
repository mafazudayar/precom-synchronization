﻿using System;
using PreCom.Core.Communication;

namespace PreCom.Synchronization.Entity
{
	/// <summary>
    /// Contains the basic information required for the synchronization process.
    /// </summary>
    public class SynchronizationPacket : EntityBase, IDebugPrintable
    {
        /// <summary>
        /// Gets or sets the session GUID.
        /// </summary>
        public Guid SessionGuid { get; set; }

        /// <summary>
        /// Gets or sets the total number of packets,
        /// if the same object is broken into several parts.
        /// </summary>
        public int TotalNumberOfPackets { get; set; }

        /// <summary>
        /// Gets or sets the packet number.
        /// part number of the large object.
        /// </summary>
        public int PacketNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a data 
        /// packet of a large object that is split.
        /// </summary>
        public bool IsSplit { get; set; }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the SynchronizationPacket.</returns>
        public virtual string GetDebugString()
        {
            return string.Format("SessionGuid: {0}, TotalNumberOfPackets: {1}, PacketNumber: {2}, ObjectType: {3}, IsSplit: {4}", SessionGuid, TotalNumberOfPackets, PacketNumber, ObjectType, IsSplit);
        }
    }
}
