﻿using System.Collections.ObjectModel;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Contains information related to the entity that is to be synchronized.
    /// </summary>
    public class SyncEntityData : SyncMetadata
    {
        private readonly Collection<string> _changedPropertyNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncEntityData"/> class.
        /// </summary>
        public SyncEntityData()
        {
            _changedPropertyNames = new Collection<string>();
        }

        /// <summary>
        /// Gets or sets the serialized object.
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Gets the changed property names.
        /// </summary>
        /// <value>
        /// The changed property names.
        /// </value>
        public Collection<string> ChangedPropertyNames
        {
            get { return _changedPropertyNames; }
        }
    }
}
