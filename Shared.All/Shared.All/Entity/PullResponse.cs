﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// The response sent by the sync server for pull request.
    /// </summary>
    public class PullResponse : SynchronizationPacket
    {
        private readonly Collection<SyncEntityData> _synchronizedObjects;
        private readonly Collection<SyncMetadata> _deletedObjects;

        /// <summary>
        /// Initializes a new instance of the <see cref="PullResponse"/> class.
        /// </summary>
        public PullResponse()
        {
            _synchronizedObjects = new Collection<SyncEntityData>();
            _deletedObjects = new Collection<SyncMetadata>();
        }

        /// <summary>
        /// Gets or sets the type of the synchronization.
        /// </summary>
        public int SyncType { get; set; }

        /// <summary>
        /// Gets the synchronized objects.
        /// </summary>
        public Collection<SyncEntityData> SynchronizedObjects
        {
            get { return _synchronizedObjects; }
        }

        /// <summary>
        /// Gets the deleted objects.
        /// </summary>
        public Collection<SyncMetadata> DeletedObjects
        {
            get { return _deletedObjects; }
        }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the PullResponse.</returns>
        public override string GetDebugString()
        {
            try
            {
                return base.GetDebugString() +
                       string.Format(
                           ", SynchronizedObjects(Count): {0}, DeletedObjects(Count): {1}, TotalEntityCount: {2}, CurrentEntityCount: {3}, SyncType: {4}, SynchronizedObjects(Ids): {5}, DeletedObjects(Ids): {6}",
                           SynchronizedObjects != null ? SynchronizedObjects.Count.ToString(CultureInfo.InvariantCulture) : "0",
                           DeletedObjects != null ? DeletedObjects.Count.ToString(CultureInfo.InvariantCulture) : "0",
                           TotalEntityCount.ToString(CultureInfo.InvariantCulture),
                           CurrentEntityCount.ToString(CultureInfo.InvariantCulture),
                           SyncType, SynchronizedObjectsToString(), DeletedObjectsToString());
            }
            catch
            {
                return "Error in GetDebugString method";
            }
        }

        /// <summary>
        /// Gets or sets the number of entities the client should receive against the given pull request
        /// </summary>
        public int TotalEntityCount { get; set; }

        /// <summary>
        /// Gets or sets the total number of entities contains in a given pull response
        /// </summary>
        public int CurrentEntityCount { get; set; }

        private string SynchronizedObjectsToString()
        {
            if (SynchronizedObjects != null)
            {
                if (SynchronizedObjects.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SyncEntityData syncEntityData in SynchronizedObjects)
                    {
                        stringBuilder.Append(syncEntityData.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            return "0";
        }

        private string DeletedObjectsToString()
        {
            if (DeletedObjects != null)
            {
                if (DeletedObjects.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SyncMetadata syncMetadata in DeletedObjects)
                    {
                        stringBuilder.Append(syncMetadata.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            return "0";
        }
    }
}
