﻿using System;
using System.Diagnostics;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Contains metadata related to an entity.
    /// </summary>
#if SERVER
    [DebuggerDisplay("Guid = {Guid}, Version = {Version}, Status = {Status}, ChangeId = {ChangeId}, LastChangeClientId = {LastChangeClientId}")]
#endif
    public class SyncMetadata
    {
        private int _status;

        /// <summary>
        /// Gets or sets the unique id of the entity.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the change id.
        /// </summary>
        /// <value>The change id.</value>
        public Guid ChangeId { get; set; }

        /// <summary>
        /// Gets or sets the last change client id.
        /// </summary>
        /// <value>The Last change client id</value>
        public string LastChangeClientId { get; set; }

        /// <summary>
        /// Gets or sets the version of the entity.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets the status of the entity.
        /// </summary>
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        internal Operation GetStatus
        {
            get { return (Operation)_status; }
        }

        internal bool IsValid()
        {
            return Version != -1 &&
                   Status != -1;
        }
    }
}
