﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Request sent by the sync client when client push process is initiated.
    /// </summary>
    public class PushRequest : SynchronizationPacket
    {
        private readonly Collection<SyncEntityData> _synchronizedObjects;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushRequest"/> class.
        /// </summary>
        public PushRequest()
        {
            _synchronizedObjects = new Collection<SyncEntityData>();
        }

        /// <summary>
        /// Gets the synchronized objects.
        /// </summary>
        public Collection<SyncEntityData> SynchronizedObjects
        {
            get { return _synchronizedObjects; }
        }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the PushRequest.</returns>
        public override string GetDebugString()
        {
            try
            {
                return base.GetDebugString() +
                       string.Format(", SynchronizedObjects(Count): {0}, SynchronizedObjects(Ids): {1}",
                                     SynchronizedObjects != null ? SynchronizedObjects.Count.ToString(CultureInfo.InvariantCulture) : "0", SynchronizedObjectsToString());
            }
            catch
            {
                return "Error in GetDebugString method";
            }
        }

        private string SynchronizedObjectsToString()
        {
            if (SynchronizedObjects != null)
            {
                if (SynchronizedObjects.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SyncEntityData syncEntityData in SynchronizedObjects)
                    {
                        stringBuilder.Append(syncEntityData.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            return "0";
        }
    }
}
