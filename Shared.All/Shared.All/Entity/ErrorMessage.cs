﻿using System;
using PreCom.Core.Communication;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Message sent from server when an error has occurred
    /// </summary>
    public class ErrorMessage : EntityBase, IDebugPrintable
    {
        /// <summary>
        /// Gets or sets the object type
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the entity id
        /// </summary>
        public Guid EntityId { get; set; }

        /// <summary>
        /// Gets or sets the type
        /// </summary>
        public SyncType Type { get; set; }

        /// <summary>
        /// Gets or sets the session id
        /// </summary>
        public Guid SessionId { get; set; }

        /// <summary>
        /// Gets or sets information related to the error
        /// </summary>
        public string Information { get; set; }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>
        /// The debug string
        /// </returns>
        public string GetDebugString()
        {
            return string.Format("ObjectType: {0}, EntityId: {1}, Type: {2}, SessionId: {3}, Information: {4}", ObjectType, EntityId, Type, SessionId, Information);
        }
    }
}
