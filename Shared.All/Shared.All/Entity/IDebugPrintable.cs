﻿namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Interface to be implemented by the entities which need detailed debug string.
    /// </summary>
    interface IDebugPrintable
    {
        /// <summary>
        /// Gets the debug string.
        /// </summary>
        /// <returns></returns>
        string GetDebugString();
    }
}
