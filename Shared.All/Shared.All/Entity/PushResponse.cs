﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// The response sent by the sync server for push request.
    /// </summary>
#if SERVER
    [DebuggerDisplay("AcceptedObjects = {AcceptedObjects.Count}, ResolvedObjects = {ResolvedObjects.Count}")]
#endif
    public class PushResponse : SynchronizationPacket
    {
        private readonly Collection<SyncMetadata> _acceptedObjects;
        private readonly Collection<SyncEntityData> _resolvedObjects;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushResponse"/> class.
        /// </summary>
        public PushResponse()
        {
            _acceptedObjects = new Collection<SyncMetadata>();
            _resolvedObjects = new Collection<SyncEntityData>();
        }

        /// <summary>
        /// Gets the accepted objects.
        /// </summary>
        public Collection<SyncMetadata> AcceptedObjects
        {
            get { return _acceptedObjects; }
        }

        /// <summary>
        /// Gets the resolved objects.
        /// </summary>
        public Collection<SyncEntityData> ResolvedObjects
        {
            get { return _resolvedObjects; }
        }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the PushResponse.</returns>
        public override string GetDebugString()
        {
            try
            {
                return base.GetDebugString() + string.Format(", AcceptedObjects(Count): {0}, ResolvedObjects(Count): {1}, AcceptedObjects(Ids): {2}, ResolvedObjects(Ids): {3}",
                                                             AcceptedObjects != null ? AcceptedObjects.Count.ToString(CultureInfo.InvariantCulture) : "0",
                                                             ResolvedObjects != null ? ResolvedObjects.Count.ToString(CultureInfo.InvariantCulture) : "0", AcceptedObjectsToString(), ResolvedObjectsToString());
            }
            catch
            {
                return "Error in GetDebugString method";
            }
        }

        private string AcceptedObjectsToString()
        {
            if (AcceptedObjects != null)
            {
                if (AcceptedObjects.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SyncMetadata syncMetadata in AcceptedObjects)
                    {
                        stringBuilder.Append(syncMetadata.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            return "0";
        }

        private string ResolvedObjectsToString()
        {
            if (ResolvedObjects != null)
            {
                if (ResolvedObjects.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SyncEntityData syncEntityData in ResolvedObjects)
                    {
                        stringBuilder.Append(syncEntityData.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            return "0";
        }
    }
}
