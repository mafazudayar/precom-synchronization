﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// Request sent by the sync client when client pull process is initiated.
    /// </summary>
    public class PullRequest : SynchronizationPacket
    {
        private string _knowledge;
        private List<SyncMetadata> _syncMetadataList;

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public EntityFilter Filter { get; set; }

        /// <summary>
        /// Gets or sets the type of the synchronization.
        /// </summary>
        public int SyncType { get; set; }

        /// <summary>
        /// Gets or sets the knowledge.
        /// This contains the serialized string of object ids and their versions that matches the filter in client.
        /// </summary>
        public string Knowledge
        {
            get { return _knowledge ?? (_knowledge = string.Empty); }
            set { _knowledge = value; }
        }

        /// <summary>
        /// Gets or sets the platform type of the client.
        /// </summary>
        public string PlatformType { get; set; }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the PullRequest.</returns>
        public override string GetDebugString()
        {
            try
            {
                if (_syncMetadataList == null)
                {
                    if (!string.IsNullOrEmpty(Knowledge) && !IsSplit)
                    {
#if SERVER
                        if (Core.SynchronizationModule.AndroidPlatform == PlatformType)
                        {
                            _syncMetadataList = (List<SyncMetadata>)JsonSerializer.Deserialize(Knowledge, typeof(List<SyncMetadata>), null);
                        }
                        else
                        {
                            _syncMetadataList = (List<SyncMetadata>)XSerializer.DeserializeSyncEntity(Knowledge, typeof(List<SyncMetadata>));
                        }
#else
                        _syncMetadataList = (List<SyncMetadata>)XSerializer.DeserializeSyncEntity(Knowledge, typeof(List<SyncMetadata>));
#endif
                    }
                }

                return base.GetDebugString() +
                    string.Format(", Filter(Count): {0}, SyncType: {1}, Knowledge(Count): {2}, Knowledge(Ids): {3}",
                                                             Filter != null && Filter.FilterList != null ? Filter.FilterList.Count.ToString(CultureInfo.InvariantCulture) : "0",
                                                             SyncType, _syncMetadataList != null ? _syncMetadataList.Count.ToString(CultureInfo.InvariantCulture) : "N/A", KnowledgeToString());
            }
            catch
            {
                return "Error in GetDebugString method";
            }
        }

        private string KnowledgeToString()
        {
            if (_syncMetadataList != null)
            {
                if (_syncMetadataList.Count <= 100)
                {
                    var stringBuilder = new StringBuilder();
                    foreach (SyncMetadata syncMetadata in _syncMetadataList)
                    {
                        stringBuilder.Append(syncMetadata.Guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            if (IsSplit)
            {
                return "Ids will not be logged because they are split";
            }

            return "0";
        }
    }
}
