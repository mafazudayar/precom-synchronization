﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using PreCom.Synchronization.Core;
using PreCom.Synchronization.Utils;

namespace PreCom.Synchronization.Entity
{
    /// <summary>
    /// The entity which is used to initiate the server push process.
    /// </summary>
    public class PullInvokeMessage : SynchronizationPacket
    {
        private string _knowledge;
        private List<Guid> _idList;

        /// <summary>
        /// Gets or sets the invoke reason.
        /// </summary>
        /// <value>The invoke reason.</value>
        public int InvokeReason { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public EntityFilter Filter { get; set; }

        /// <summary>
        /// Gets or sets the knowledge. 
        /// This contains the serialized string of object ids that matches the filter.
        /// </summary>
        public string Knowledge
        {
            get { return _knowledge ?? (_knowledge = string.Empty); }
            set { _knowledge = value; }
        }

        /// <summary>
        /// Gets or sets the platform type of the client.
        /// </summary>
        public string PlatformType { get; set; }

        /// <summary>
        /// Gets a string for debug logging
        /// </summary>
        /// <returns>Debug friendly string of the PullInvokeMessage.</returns>
        public override string GetDebugString()
        {
            try
            {
                if (_idList == null)
                {
                    if (!string.IsNullOrEmpty(Knowledge) && !IsSplit)
                    {
                        switch (InvokeReason)
                        {
                            case (int)PullInvokeReason.ClientLogin:

#if SERVER
                                if (PlatformType == SynchronizationModule.AndroidPlatform)
                                {
                                    _idList = (List<Guid>)JsonSerializer.Deserialize(Knowledge, typeof(List<Guid>), null);
                                }
                                else
                                {
                                    _idList = (List<Guid>)XSerializer.Deserialize(Knowledge, typeof(List<Guid>));
                                }

#endif
#if PocketPC
                                _idList = (List<Guid>)XSerializer.Deserialize(Knowledge, typeof(List<Guid>));
#endif

                                break;
                            case (int)PullInvokeReason.AutomaticPush:

                                _idList = new List<Guid> { new Guid(Knowledge) };
                                break;
                        }
                    }
                }

                return base.GetDebugString() +
                    string.Format(", Knowledge(Count): {0}, Filter(Count): {1}, ObjectType: {2}, Knowledge(Ids): {3} ",
                                     _idList != null ? _idList.Count.ToString(CultureInfo.InvariantCulture) : "N/A",
                                     Filter != null && Filter.FilterList != null ? Filter.FilterList.Count.ToString(CultureInfo.InvariantCulture) : "0",
                                     ObjectType, KnowledgeToString());
            }
            catch
            {
                return "Error in GetDebugString method";
            }
        }

        private string KnowledgeToString()
        {
            if (_idList != null)
            {
                if (_idList.Count <= 100)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (Guid guid in _idList)
                    {
                        stringBuilder.Append(guid + ",");
                    }

                    string ids = stringBuilder.ToString().TrimEnd(',');
                    return ids;
                }

                return "Ids will not be logged because the count is higher than 100";
            }

            if (IsSplit)
            {
                return "Ids will not be logged because they are split";
            }

            return "0";
        }
    }
}
