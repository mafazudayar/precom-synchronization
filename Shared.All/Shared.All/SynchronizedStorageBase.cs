﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
#if SERVER
using System.Transactions;
#endif
using PreCom.Core.Log;
using PreCom.Core.Modules;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Base class for custom storage implementations. This class performs CRUD operations.
    /// It contains a number of abstract methods that the subclass needs to implement where the actual CRUD operations
    /// of the storage will be performed.
    /// </summary>
    /// <typeparam name="T">Type that this storage handles</typeparam>
    public abstract class SynchronizedStorageBase<T> where T : ISynchronizable
    {
#if !LT
        private readonly ILog _log;

#if CLIENT
        private readonly SynchronizationClientBase _synchronization;

        /// <summary>
        /// Initializes a new instance of the <see><cref>SynchronizedStorageBase</cref></see> class.
        /// </summary>
        [Obsolete("Use SynchronizedStorageBase(ILog, SynchronizationClientBase) instead.")]
        protected SynchronizedStorageBase()
        {
            _log = Application.Instance.CoreComponents.Get<ILog>();
            _synchronization = Application.Instance.Modules.Get<SynchronizationClientBase>();
        }

        /// <summary>
        /// Initializes a new instance of the <see><cref>SynchronizedStorageBase</cref></see> class.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="synchronization"></param>
        protected SynchronizedStorageBase(ILog log, SynchronizationClientBase synchronization)
        {
            _log = log;
            _synchronization = synchronization;
        }
#endif
#if SERVER
        private readonly SynchronizationServerBase _synchronization;

        /// <summary>
        /// Initializes a new instance of the <see><cref>SynchronizedStorageBase</cref></see> class.
        /// </summary>
        [Obsolete("Use SynchronizedStorageBase(ILog, SynchronizationClientBase) instead.")]
        protected SynchronizedStorageBase()
        {
            _log = Application.Instance.CoreComponents.Get<ILog>();
            _synchronization = Application.Instance.Modules.Get<SynchronizationServerBase>();
        }

        /// <summary>
        /// Initializes a new instance of the <see><cref>SynchronizedStorageBase</cref></see> class.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="synchronization"></param>
        protected SynchronizedStorageBase(ILog log, SynchronizationServerBase synchronization)
        {
            _log = log;
            _synchronization = synchronization;
        }
#endif
#endif

#if CLIENT

        internal object ThisLock { get; set; }

        /// <summary>
        /// Creates entities of a specific type.
        /// </summary>
        /// <param name="entities">A collection of entities to create.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entities.</exception>
        public void Create(ICollection<ISynchronizable> entities, CreateArgs args)
        {
            this.RunWithLock(() =>
            {
                OnCreate(entities, args);
                foreach (ISynchronizable entity in entities)
                {
                    FireChangedEvent(entity.Guid, Operation.Created, args.OriginatorType, args.CredentialId, null, entity);
                }
            }
);
        }

        /// <summary>
        /// Creates an entity of a specific type.
        /// </summary>
        /// <param name="entity">The entity to create.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entity.</exception>
        public void Create(ISynchronizable entity, CreateArgs args)
        {
            Create(new List<ISynchronizable>() { entity }, args);
        }

        /// <summary>
        /// Updates the entity of a specified type.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="args">The argument containing information about the modification.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when updating the entity.</exception>
        public void Update(ISynchronizable entity, UpdateArgs args)
        {
            this.RunWithLock(() =>
            {
                ISynchronizable oldEntity = GetOldEntity(entity.Guid);

                OnUpdate(entity, args);
                FireChangedEvent(entity.Guid, Operation.Updated, args.OriginatorType, args.CredentialId, oldEntity, entity);
            }
);
        }

        /// <summary>
        /// Deletes the entity by the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="args">The argument containing information about the deletion.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when deleting the entity.</exception>
        public void Delete(Guid id, DeleteArgs args)
        {
            this.RunWithLock(() =>
            {
                ISynchronizable oldEntity = GetOldEntity(id);

                OnDelete(id, args);
                FireChangedEvent(id, Operation.Deleted, args.OriginatorType, args.CredentialId, oldEntity, null);
            }
);
        }

        /// <summary>
        /// Reads the specified entities.
        /// </summary>
        /// <param name="entityIds">The list of entity ids.</param>
        /// <returns>
        /// Collection of <see cref="ISynchronizable"/> Entities.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading entities.</exception>
        public ICollection<ISynchronizable> Read(ICollection<Guid> entityIds)
        {
            return RunWithLock(() => OnRead(entityIds));
        }

        /// <summary>
        /// Reads the specified entities for the given filter.
        /// If the EntityFilter is empty, should return all entities. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// Collection of <see cref="ISynchronizable"/> Entities.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading entities.</exception>
        public ICollection<ISynchronizable> Read(EntityFilter filter)
        {
            return RunWithLock(() => OnRead(filter));
        }

        /// <summary>
        /// Reads the entity ids for the given filter.
        /// If the EntityFilter is empty, should return all entity ids. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// Collection of Guids.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading ids.</exception>
        public ICollection<Guid> ReadIds(EntityFilter filter)
        {
            return RunWithLock(() => OnReadIds(filter));
        }

        private void RunWithLock(Action action)
        {
            RunWithLock(() =>
            {
                action();
                return true;
            });
        }

        private TResult RunWithLock<TResult>(Func<TResult> function)
        {
            try
            {
                if (ThisLock != null)
                {
                    Monitor.Enter(ThisLock);
                }

                return function();
            }
            finally
            {
                if (ThisLock != null)
                {
                    Monitor.Exit(ThisLock);
                }
            }
        }
#endif

#if SERVER
        internal delegate int AcquireUpdateLockDelegate<TU>(Guid entityId) where TU : ISynchronizable;
        internal AcquireUpdateLockDelegate<T> AcquireUpdateLock;

        /// <summary>
        /// Creates entities of a specific type.
        /// </summary>
        /// <param name="entities">A collection of entities to create.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entities.</exception>
        public void Create(ICollection<ISynchronizable> entities, CreateArgs args)
        {
            OnCreate(entities, args);
            foreach (ISynchronizable entity in entities)
            {
                FireChangedEvent(
                    entity.Guid, Operation.Created, args.OriginatorType, args.CredentialId, null, entity);
            }
        }

        /// <summary>
        /// Creates an entity of a specific type.
        /// </summary>
        /// <param name="entity">The entity to create.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entity.</exception>
        public void Create(ISynchronizable entity, CreateArgs args)
        {
            Create(new List<ISynchronizable>() { entity }, args);
        }

        /// <summary>
        /// Updates the entity of a specified type.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="args">The argument containing information about the modification.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when updating the entity.</exception>
        public void Update(ISynchronizable entity, UpdateArgs args)
        {
            if (Transaction.Current != null)
            {
                AcquireUpdateLock(entity.Guid);
                ISynchronizable oldEntity = GetOldEntity(entity.Guid);

                OnUpdate(entity, args);
                FireChangedEvent(entity.Guid, Operation.Updated, args.OriginatorType, args.CredentialId, oldEntity, entity);
            }
            else
            {
                throw new SynchronizedStorageException("Operation is not valid without a transaction");
            }
        }

        /// <summary>
        /// Deletes the entity by the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="args">The argument containing information about the deletion.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when deleting the entity.</exception>
        public void Delete(Guid id, DeleteArgs args)
        {
            if (Transaction.Current != null)
            {
                AcquireUpdateLock(id);
                ISynchronizable oldEntity = GetOldEntity(id);

                OnDelete(id, args);
                FireChangedEvent(id, Operation.Deleted, args.OriginatorType, args.CredentialId, oldEntity, null);
            }
            else
            {
                throw new SynchronizedStorageException("Operation is not valid without a transaction");
            }
        }

        /// <summary>
        /// Reads the specified entities.
        /// </summary>
        /// <param name="entityIds">The list of entity ids.</param>
        /// <returns>
        /// Collection of <see cref="ISynchronizable"/> Entities.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading entities.</exception>
        public ICollection<ISynchronizable> Read(ICollection<Guid> entityIds)
        {
            return OnRead(entityIds);
        }

        /// <summary>
        /// Reads the specified entities for the given filter.
        /// If the EntityFilter is empty, should return all entities. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// Collection of <see cref="ISynchronizable"/> Entities.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading entities.</exception>
        public ICollection<ISynchronizable> Read(EntityFilter filter)
        {
            return OnRead(filter);
        }

        /// <summary>
        /// Reads the entity ids for the given filter.
        /// If the EntityFilter is empty, should return all entity ids. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// Collection of Guids.
        /// </returns>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when reading ids.</exception>
        public ICollection<Guid> ReadIds(EntityFilter filter)
        {
            return OnReadIds(filter);
        }

#endif

        /// <summary>
        /// Fired when an entity is changed by create, update or delete.
        /// </summary>
        public event EventHandler<ChangeEventArgs> EntityChanged;

        internal event EventHandler<ChangeEventArgs> InternalEntityChanged;

        /// <summary>
        /// Called by the base class when objects shall be created. Implement the create functionality in this method.
        /// </summary>
        /// <param name="entities">The entities to be created.</param>
        /// <param name="args">The argument containing information of creation.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when creating the entities.</exception>
        protected abstract void OnCreate(ICollection<ISynchronizable> entities, CreateArgs args);

        /// <summary>
        /// Called by the base class when an object shall be updated. Implement the update functionality in this method.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        /// <param name="args">The argument containing information about the modification.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when updating the entity.</exception>
        protected abstract void OnUpdate(ISynchronizable entity, UpdateArgs args);

        /// <summary>
        /// Called by the base class when an object shall be deleted. Implement the delete functionality in this method.
        /// </summary>
        /// <param name="id">The id of the entity to be deleted.</param>
        /// <param name="args">The argument containing information about the deletion.</param>
        /// <exception cref="SynchronizedStorageException">Thrown when an error occurs when deleting the entity.</exception>
        protected abstract void OnDelete(Guid id, DeleteArgs args);

        /// <summary>
        /// Called by the base class when entities should be read given a list of entity Ids. Implement the read functionality in this method.
        /// </summary>
        /// <param name="entityIds">The collection of entity ids</param>
        /// <returns>Collection of <see cref="ISynchronizable"/> entities.</returns>
        protected abstract ICollection<ISynchronizable> OnRead(ICollection<Guid> entityIds);

        /// <summary>
        /// Called by the base class when entities should be read given a filter. Implement the read functionality in this method.
        /// If the EntityFilter is empty, should return all entities. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Collection of <see cref="ISynchronizable"/> entities.</returns>
        protected abstract ICollection<ISynchronizable> OnRead(EntityFilter filter);

        /// <summary>
        /// Called by the base class when entity ids should be read given a filter. Implement the read functionality in this method.
        /// If the EntityFilter is empty, should return all entity ids. 
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Collection of ids selected by the filter</returns>
        protected abstract ICollection<Guid> OnReadIds(EntityFilter filter);

        /// <summary>
        /// Fires the change event for an entity. This should normally not be called by a sub class except for exceptional cases
        /// when the actual storage is not handled with the protected abstract On... methods of this class.
        /// </summary>
        /// <param name="args">Change event arguments</param>
        protected void FireChangedEvent(ChangeEventArgs args)
        {
            var internalHandler = InternalEntityChanged;
            if (internalHandler != null)
            {
                internalHandler.Invoke(this, args);
            }

            ThreadPool.QueueUserWorkItem(state =>
            {
                try
                {
                    var handler = EntityChanged;
                    if (handler != null)
                    {
                        handler.Invoke(this, args);
                    }
                }
                catch (Exception exception)
                {
#if !LT
                    if (_log != null && _synchronization != null)
                    {
                        var logItem = new LogItem(_synchronization,
                            LogLevel.Error,
                            "SYNC_STOR_E_1",
                            "Exception",
                            "Exception when invoking change event listeners: " + exception.Message,
                            exception.ToString());
                        _log.Write(logItem);
                    }
#endif
                }
            });
        }

        private void FireChangedEvent(Guid id,
            Operation operation,
            string originatorType,
            int credentialId,
            ISynchronizable oldEntity,
            ISynchronizable newEntity)
        {
            ChangeEventArgs args = new ChangeEventArgs();
            args.Change = operation;
            args.Id = id;
            args.New = newEntity;
            args.Old = oldEntity;
            args.EntityType = typeof(T).Name;
            args.OriginatorType = originatorType;
            args.CredentialId = credentialId;

            FireChangedEvent(args);
        }

        private ISynchronizable GetOldEntity(Guid entityId)
        {
            ICollection<ISynchronizable> result = Read(new[] { entityId });

            if (result == null || result.Count == 0)
            {
                throw new SynchronizedStorageException(string.Format("No object with id {0} was found", entityId));
            }

            if (result.Count != 1)
            {
                throw new SynchronizedStorageException(string.Format("{0} objects with id {1} was found, expecting one object", result.Count, entityId));
            }

            return result.First();
        }
    }
}
