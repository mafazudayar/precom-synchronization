﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// The interface which an object should implement in order to support the synchronization process.
    /// </summary>
    public interface ISynchronizable
    {
        /// <summary>
        /// Gets or sets the GUID which is used for the synchronization process. 
        /// This GUID should be unique for each entity.
        /// </summary>
        Guid Guid { get; set; }
    }
}
