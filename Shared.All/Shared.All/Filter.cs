﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains information about a given property, which is used to build the filter query.
    /// </summary>
    public class Filter
    {
        /// <summary>
        /// Gets or sets the value for the property .
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// Gets or sets the operator which is used for the comparison.
        /// </summary>
        public FilterOperator Operator { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Filter"/> class.
        /// </summary>
        public Filter(){}

        /// <summary>
        /// Initializes a new instance of the <see cref="Filter"/> class.
        /// </summary>
        /// <param name="filterOperator">The filter operator.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        public Filter(FilterOperator filterOperator, string propertyName, object value)
        {
            Operator = filterOperator;
            PropertyName = propertyName;
            Value = value;
        }
    }
}
