﻿using System;
using PreCom.Synchronization;
using PreCom.Synchronization.Entity;

namespace Synchronization.Shared.All.Utils
{
    internal static class SyncTypeHelper
    {
        public static SyncEventType ToSyncEventType(SyncType syncType)
        {
            SyncEventType syncEventType;
            switch (syncType)
            {
                case SyncType.ClientPull:
                    syncEventType = SyncEventType.ClientPull;
                    break;
                case SyncType.ClientPush:
                    syncEventType = SyncEventType.ClientPush;
                    break;
                case SyncType.ServerPush:
                    syncEventType = SyncEventType.ServerPush;
                    break;
                default:
                    throw new ArgumentException("No matching value", "syncType");
            }
            return syncEventType;
        }
    }
}
