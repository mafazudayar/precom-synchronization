﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace PreCom.Synchronization.Utils
{
    internal class XSerializer
    {
        private static readonly Dictionary<string, XmlSerializer> Serializers = new Dictionary<string, XmlSerializer>();

        private static readonly object SerializeLock = new object();
        private static readonly XmlSerializerNamespaces XmlNs = new XmlSerializerNamespaces();

        static XSerializer()
        {
            XmlNs.Add(string.Empty, string.Empty);
        }

        public static string SerializeSyncEntity(object o, IList<PropertyInfo> propertiesToExclude)
        {
            string myStr;

            Type objectType = o.GetType();
            using (var memoryStream = new MemoryStream())
            {
                XmlSerializer serializer = GetSerializer(objectType, propertiesToExclude);

                var serializerSettings = new XmlWriterSettings
                {
                    Indent = false,
                    Encoding = Encoding.UTF8
                };

                using (var xmlWriter = XmlWriter.Create(memoryStream, serializerSettings))
                {
                    serializer.Serialize(xmlWriter, o, XmlNs);
                }

                memoryStream.Position = 0;
                using (var sr = new StreamReader(memoryStream))
                {
                    myStr = sr.ReadToEnd();
                }
            }

            return myStr;
        }

        public static string SerializeSyncEntity(object o)
        {
            return SerializeSyncEntity(o, new List<PropertyInfo>());
        }

        public static string Serialize(object o, Type type)
        {
            string serializedString;

            using (var memoryStream = new MemoryStream())
            {
                var serializer = new XmlSerializer(type);
                var serializerSettings = new XmlWriterSettings
                {
                    Indent = false,
                    Encoding = Encoding.UTF8
                };

                using (var xmlWriter = XmlWriter.Create(memoryStream, serializerSettings))
                {
                    serializer.Serialize(xmlWriter, o, XmlNs);
                }

                memoryStream.Position = 0;
                using (var sr = new StreamReader(memoryStream))
                {
                    serializedString = sr.ReadToEnd();
                }
            }

            return serializedString;
        }

        public static object DeserializeSyncEntity(string serializedObject, Type type)
        {
            object deserializedObject;

            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(serializedObject)))
            {
                XmlSerializer serializer = GetSerializer(type, new List<PropertyInfo>());
                deserializedObject = serializer.Deserialize(memoryStream);
            }

            return deserializedObject;
        }

        private static XmlSerializer GetSerializer(Type type, IList<PropertyInfo> propertiesToExclude)
        {
            XmlSerializer serializer;

            // We need to cache these serializers to avoid memory leaks. 
            // See the "Dynamically generated assemblies" section in the following url: 
            // http://msdn.microsoft.com/en-us/library/system.xml.serialization.xmlserializer%28v=VS.100%29.aspx

            List<string> excludedProperties = new List<string>();

            foreach (PropertyInfo propertyInfo in propertiesToExclude)
            {
                excludedProperties.Add(propertyInfo.Name);
            }
            excludedProperties.Sort();

            // Create a unique string for each combination of type and excluded properties.
            // The string will always start with the qualified name of the object type
            // and then the excluded property names will follow in alphabetical order.
            string key = string.Format("{0}{1}{2}",
                                       type.AssemblyQualifiedName,
                                       excludedProperties.Count > 0 ? "-" : "",
                                       string.Join(":", excludedProperties.ToArray()));

            lock (SerializeLock)
            {
                if (!Serializers.TryGetValue(key, out serializer))
                {
                    XmlAttributes attrs = new XmlAttributes { XmlIgnore = true };
                    XmlAttributeOverrides attrOverrides = new XmlAttributeOverrides();
                    foreach (PropertyInfo propertyInfo in propertiesToExclude)
                    {
                        if (propertyInfo.DeclaringType != null)
                        {
                            attrOverrides.Add(propertyInfo.DeclaringType, propertyInfo.Name, attrs);
                        }
                    }
                    serializer = new XmlSerializer(type, attrOverrides);
                    Serializers.Add(key, serializer);
                }
            }

            return serializer;
        }

        public static object Deserialize(string serializedObject, Type type)
        {
            object deserializedObject;

            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(serializedObject)))
            {
                XmlSerializer serializer = new XmlSerializer(type);
                deserializedObject = serializer.Deserialize(memoryStream);
            }

            return deserializedObject;
        }
    }
}
