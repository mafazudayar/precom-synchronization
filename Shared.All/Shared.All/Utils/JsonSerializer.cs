﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Json;
using System;
using System.Text;

namespace PreCom.Synchronization.Utils
{
	internal static class JsonSerializer
	{
		internal static object Deserialize(string data, Type dataType, IEnumerable<Type> knownTypes)
		{
			object deserializedObject;

			DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
			settings.DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("U", CultureInfo.InvariantCulture.DateTimeFormat);
			settings.KnownTypes = knownTypes;

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(dataType, knownTypes);

			using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
			{
				deserializedObject = serializer.ReadObject(memoryStream);
			}

			return deserializedObject;
		}

		internal static string Serialize(object o, IEnumerable<Type> knownTypes)
		{
			string serializedString;

			Type objectType = o.GetType();

			DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
			settings.DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("U", CultureInfo.InvariantCulture.DateTimeFormat);
			settings.KnownTypes = knownTypes;

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(objectType, knownTypes);
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, o);
				memoryStream.Position = 0;
				StreamReader stringReader = new StreamReader(memoryStream);
				serializedString = stringReader.ReadToEnd();
			}
			return serializedString;
		}
	}
}
