﻿namespace PreCom.Synchronization.Utils
{
    // ReSharper disable InconsistentNaming
    internal enum LogId
    {
        /// <summary>
        /// Communication exception when sending requests. 
        /// </summary>
        SYNC_COM_E_1,
        /// <summary>
        /// Timed out waiting for user to become ready to receive data.
        /// </summary>
        SYNC_COM_E_2,
        /// <summary>
        /// Communication exception when sending requests. 
        /// </summary>
        SYNC_COM_E_3,
        /// <summary>
        /// Communication exception when sending requests. 
        /// </summary>
        SYNC_COM_E_4,
        /// <summary>
        /// Message sent.
        /// </summary>
        SYNC_COM_D_1,
        /// <summary>
        /// Message received. 
        /// </summary>
        SYNC_COM_D_2,
        /// <summary>
        /// User not logged in. 
        /// </summary>
        SYNC_COM_D_3,
        /// <summary>
        /// User not logged in. 
        /// </summary>
        SYNC_COM_D_4,
        /// <summary>
        /// Push request initiated.
        /// </summary>
        SYNC_PUSH_D_1,
        /// <summary>
        /// Calling custom storage.
        /// </summary>
        SYNC_PUSH_D_2,
        /// <summary>
        /// Custom storage responded.
        /// </summary>
        SYNC_PUSH_D_3,
        /// <summary>
        /// Push requests sent.
        /// </summary>
        SYNC_PUSH_D_4,
        /// <summary>
        /// Push response received.
        /// </summary>
        SYNC_PUSH_D_5,
        /// <summary>
        /// Push response completed.
        /// </summary>
        SYNC_PUSH_D_6,
        /// <summary>
        /// Transaction (IsolationLevel.ReadCommitted) started.
        /// </summary>
        SYNC_PUSH_D_7,
        /// <summary>
        /// Transaction (IsolationLevel.ReadCommitted) completed.
        /// </summary>
        SYNC_PUSH_D_8,
        /// <summary>
        /// SQL deadlock.
        /// </summary>
        SYNC_PUSH_I_1,
        /// <summary>
        /// Storage exception in push manager. 
        /// </summary>
        SYNC_PUSH_E_1,
        /// <summary>
        /// Exception in push manager. 
        /// </summary>
        SYNC_PUSH_E_2,
        /// <summary>
        /// Object not found when pushing.
        /// </summary>
        SYNC_PUSH_E_3,
        /// <summary>
        /// SQL deadlock.
        /// </summary>
        SYNC_PUSH_E_4,
        /// <summary>
        /// Storage exception in entity update manager.
        /// </summary>
        SYNC_EU_E_1,
        /// <summary>
        /// Exception in entity update manager.
        /// </summary>
        SYNC_EU_E_2,
        /// <summary>
        /// Entity change not specified.
        /// </summary>
        SYNC_EU_E_3,
        /// <summary>
        /// Exception when sending pull invoke.
        /// </summary>
        SYNC_EU_E_4,
        /// <summary>
        /// Exception handling automatic push.
        /// </summary>
        SYNC_EU_E_5,
        /// <summary>
        /// Exception when an entity is deleted locally.
        /// </summary>
        SYNC_EU_E_6,
        /// <summary>
        /// AutomaticPushDelegate method invoked.
        /// </summary>
        SYNC_EU_D_1,
        /// <summary>
        /// Pull request initiated.
        /// </summary>
        SYNC_PULL_D_1,
        /// <summary>
        /// Calling custom storage.
        /// </summary>
        SYNC_PULL_D_2,
        /// <summary>
        /// Custom storage responded.
        /// </summary>
        SYNC_PULL_D_3,
        /// <summary>
        /// Pull requests sent.
        /// </summary>
        SYNC_PULL_D_4,
        /// <summary>
        /// Pull response received.
        /// </summary>
        SYNC_PULL_D_5,
        /// <summary>
        /// Pull response completed.
        /// </summary>
        SYNC_PULL_D_6,
        /// <summary>
        /// Transaction (IsolationLevel.Snapshot) started.
        /// </summary>
        SYNC_PULL_D_7,
        /// <summary>
        /// Transaction (IsolationLevel.Snapshot) completed.
        /// </summary>
        SYNC_PULL_D_8,
        /// <summary>
        /// Storage exception in pull manager. 
        /// </summary>
        SYNC_PULL_E_1,
        /// <summary>
        /// Exception in pull manager. 
        /// </summary>
        SYNC_PULL_E_2,
        /// <summary>
        /// Exception in synchronization manager when calling event notify callback.
        /// </summary>
        SYNC_SM_E_1,
        /// <summary>
        /// Storage exception in synchronization manager.
        /// </summary>
        SYNC_SM_E_2,
        /// <summary>
        /// Maximum sync packet size setting is out of range.
        /// </summary>
        SYNC_E_1,
        /// <summary>
        /// Error in Initialization.
        /// </summary>
        SYNC_E_2,
        /// <summary>
        /// Exception in login manager.
        /// </summary>
        SYNC_LM_E_1,
        /// <summary>
        /// Storage exception in login manager.
        /// </summary>
        SYNC_LM_E_2,
        /// <summary>
        /// LoginFilterDelegate method invoked.
        /// </summary>
        SYNC_LM_D_1,
        /// <summary>
        /// Exception trying to reset storage transaction scope in dispose.
        /// </summary>
        SYNC_TSCOPE_E_1,
        /// <summary>
        /// Delegate already registered
        /// </summary>
        SYNC_TM_W_1
    }
    // ReSharper restore InconsistentNaming
}
