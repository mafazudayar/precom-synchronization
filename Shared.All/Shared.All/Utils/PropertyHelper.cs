﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PreCom.Synchronization.Utils
{
    internal class PropertyHelper
    {
        public static List<PropertyInfo> GetExcludedProperties(IEnumerable<PropertyInfo> properties, List<string> changedPropertyNames)
        {
            return properties.Where(propertyInfo => !changedPropertyNames.Contains(propertyInfo.Name)).ToList();
        }

        public static List<PropertyInfo> GetProperties(Type type)
        {
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();
            var synchronizableProperties = new List<PropertyInfo>();

            foreach (var propertyInfo in properties)
            {
                if (Attribute.IsDefined(propertyInfo, typeof(SynchronizableAttribute)))
                {
                    synchronizableProperties.Add(propertyInfo);
                }
            }

            // If we cant find any SynchronizableAttribute all the properties should be synchronized.
            return synchronizableProperties.Any() ? synchronizableProperties : properties;
        }
    }
}
