﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Represents the type of the synchronization process.
    /// </summary>
    public enum SyncEventType
    {
        /// <summary>
        /// Client initiated pull.
        /// </summary>
        ClientPull,

        /// <summary>
        /// Client initiated push.
        /// </summary>
        ClientPush,

        /// <summary>
        /// Server initiated push.
        /// </summary>
        ServerPush
    }
}
