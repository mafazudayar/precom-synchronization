﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Attribute provided to mark the properties that should be synchronized.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SynchronizableAttribute : Attribute
    {
    }
}
