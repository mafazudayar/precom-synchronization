﻿using System;

namespace PreCom.Synchronization.Core
{
    internal interface IClientPushManager
    {
#if SERVER
        event EventHandler<EntitiesChangedEventArgs> EntitiesChanged;
#endif
    }
}
