﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Thrown when an error happens in an implementation of <see><cref>SynchronizedStorageBase</cref></see>
    /// </summary>
    [Serializable]
    public class SynchronizedStorageException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizedStorageException"/> class. 
        /// </summary>
        public SynchronizedStorageException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizedStorageException"/> class. 
        /// </summary>
        /// <param name="message">
        /// Message to use.
        /// </param>
        public SynchronizedStorageException(string message) 
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizedStorageException"/> class. 
        /// </summary>
        /// <param name="message">
        /// Message to use.
        /// </param>
        /// <param name="innerException">
        /// Inner exception to use.
        /// </param>
        public SynchronizedStorageException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
    }
}
