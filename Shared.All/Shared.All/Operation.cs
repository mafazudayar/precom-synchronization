﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Represents the status of the entity.
    /// </summary>
    [Flags]
    public enum Operation
    {
        /// <summary>
        /// None.
        /// </summary>
        None = 0,
        /// <summary>
        /// Entity is Created. 
        /// </summary>
        Created = 0x01,
        /// <summary>
        /// Entity is Updated. 
        /// </summary>
        Updated = 0x02,
        /// <summary>
        /// Entity is Deleted.
        /// </summary>
        Deleted = 0x04
    }
}
