﻿using System;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Used to send information about the synchronization process to SyncEvents
    /// </summary>
    public class SyncInfo
    {
        /// <summary>
        /// Gets or sets the sync event reason.
        /// </summary>
        /// <value>The reason.</value>
        public SyncEventReason Reason { get; set; }

        /// <summary>
        /// Gets or sets the sync event type.
        /// </summary>
        /// <value>The type.</value>
        public SyncEventType Type { get; set; }

        /// <summary>
        /// Gets or sets the session id.
        /// </summary>
        /// <value>The session id.</value>
        public Guid SessionId { get; set; }

        /// <summary>
        /// Gets or sets the information.
        /// </summary>
        /// <value>The information.</value>
        public string Information { get; set; }

        /// <summary>
        /// Gets or set the total entity count to be received
        /// </summary>
        public int TotalEntityCount { get; set; }

        /// <summary>
        /// Gets or set the synchronized entity count
        /// </summary>
        public int SynchronizedEntityCount { get; set; }
    }
}
