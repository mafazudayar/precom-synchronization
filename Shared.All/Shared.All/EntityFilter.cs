﻿using System.Collections.ObjectModel;

namespace PreCom.Synchronization
{
    /// <summary>
    /// Contains a list of filters that is used to create the filter query.
    /// </summary>
    public class EntityFilter
    {
        private Collection<Filter> _filterList;

        /// <summary>
        /// Gets the Collection of filters.
        /// </summary>
        public Collection<Filter> FilterList
        {
            get
            {
                if (_filterList == null)
                {
                    _filterList = new Collection<Filter>();
                }
                return _filterList;
            }
        }
    }
}
