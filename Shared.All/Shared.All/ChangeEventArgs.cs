﻿using System;

namespace PreCom.Synchronization
{
	/// <summary>
	/// Event Argument of the entity changed event.
	/// </summary>
	public class ChangeEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the id of the changed entity.
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Gets or sets the status of the changed entity.
		/// </summary>
		public Operation Change { get; set; }

		/// <summary>
		/// Gets or sets the type of the changed entity.
		/// </summary>
		public string EntityType { get; set; }

		/// <summary>
		/// Gets or sets the old entity.
		/// </summary>
		public ISynchronizable Old { get; set; }

		/// <summary>
		/// Gets or sets the new entity.
		/// </summary>
		public ISynchronizable New { get; set; }

		/// <summary>
		/// Gets or sets the type of the caller that created, updated or deleted the entity.
		/// </summary>
		/// <value>
		/// The originator type.
		/// </value>
		public string OriginatorType { get; set; }

		/// <summary>
		/// Gets or sets the credential id of the user initiated the operation.
		/// </summary>
		/// <value>
		/// The credential id.
		/// </value>
		public int CredentialId { get; set; }
	}
}
