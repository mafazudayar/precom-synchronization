﻿namespace PreCom.Synchronization
{
    /// <summary>
    ///Represents the status of the synchronization process.
    /// </summary>
    public enum SyncEventReason
    {
        /// <summary>
        /// Synchronization process is started.
        /// </summary>
        Started,

        /// <summary>
        /// Entity is synchronized.
        /// </summary>
        Synchronized,

        /// <summary>
        /// Synchronization process is aborted.
        /// </summary>
        Aborted,

        /// <summary>
        /// Error occurred in the synchronization process.
        /// </summary>
        Error,

        /// <summary>
        /// An error occurred in server in synchronization process.
        /// </summary>
        ServerError,

        /// <summary>
        /// Synchronization process is Completed.
        /// </summary>
        Completed
    }
}
