﻿namespace PreCom.Synchronization
{
    /// <summary>
    ///  Contains information related to the deleted entity.
    /// </summary>
    public class DeleteArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteArgs"/> class.
        /// </summary>
        /// <param name="originatorType">The originator.</param>
        public DeleteArgs(string originatorType)
        {
            OriginatorType = originatorType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteArgs"/> class.
        /// </summary>
        /// <param name="originatorType">The originator.</param>
        /// <param name="credentialId">The credential id.</param>
        public DeleteArgs(string originatorType, int credentialId)
        {
            OriginatorType = originatorType;
            CredentialId = credentialId;
        }

        /// <summary>
        /// Gets or sets the originator type.
        /// </summary>
        /// <value>
        /// The type of the caller.
        /// </value>
        public string OriginatorType { get; set; }

        /// <summary>
        /// Gets or sets the credential id.
        /// </summary>
        /// <value>
        /// The credential id of the user responsible for the modification. 
        /// </value>
        public int CredentialId { get; set; }
    }
}
