﻿namespace PreCom.Synchronization
{
    /// <summary>
    /// Represents the operators used to compare the property with the value when building the filter query.
    /// </summary>
    public enum FilterOperator
    {
        /// <summary>
        /// Property equals to the value.
        /// </summary>
        EqualTo = 0,
        /// <summary>
        /// Property not equals to the value.
        /// </summary>
        NotEqualTo = 1,
        /// <summary>
        /// Property is greater than the value.
        /// </summary>
        GreaterThan = 2,
        /// <summary>
        /// Property is less than the value.
        /// </summary>
        LessThan = 3,
    }
}
